/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.parallel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import argo.saj.InvalidSyntaxException;

public class ParallelTextAnnotatorTest {

  private ParallelTextAnnotator parallel;

  private String text;

  @Before public void bringUp() {
    parallel = (ParallelTextAnnotator) ParallelTextAnnotator.getInstance();
    text = "Barack Obama is the new president of the United States.";
  }

  /**
   * Test to check that a text can be submitted to extractiv annotator service
   * 
   * @throws InvalidSyntaxException
   * @throws ConfigurationException
   */
  @Test public void testSubmitToAnnotator() throws InvalidSyntaxException, ConfigurationException {
    setNormalConfiguration();

    // check the timeout and the annotation pool has been specified in the configuration
    Assert.assertNotNull(parallel.getConfig().getSpecificParameters().get("timeout"));
    // check the annotator pool has been correctly initialized
    Assert.assertTrue(parallel.getAnnotatorPool().size() > 0);

    // check that the text can be submitted to the annotator and no exceptions are thrown
    final String response = parallel.submitToAnnotator(text);

    Assert.assertNotNull(response);
    Assert.assertTrue(response.length() > 0);

    // check the response contains as many @ as the number of annotators in the pool
    Assert.assertTrue(parallel.getAnnotatorPool().size() == response.split("~@~@~@~").length);
  }

  /**
   * Test to verify the entities retrieved from extractiv web service
   * 
   * @throws ConfigurationException
   */
  @Test public void testAnnotateEntities() throws ConfigurationException {
    setNormalConfiguration();
    final Set<Annotation> annotations = parallel.annotateEntity(text);
    Assert.assertNotNull(annotations);
    Assert.assertTrue(annotations.size() > 0);
  }

  /**
   * Test to verify that no entitie ares retrieved from extractiv web service with an senseless text
   * 
   * @throws ConfigurationException
   */
  @Test public void testAnnotateNoEntities() throws ConfigurationException {
    setNormalConfiguration();
    text = "cfrgfvdfgbfdgdgdfgdfgdgdg";

    final Set<Annotation> annotations = parallel.annotateEntity(text);
    Assert.assertNotNull(annotations);
    Assert.assertTrue(annotations.size() == 0);
  }

  /**
   * Check that if in the configuration file the annotator pool is not specified an exception is thrown
   * 
   * @throws ConfigurationException
   * @throws IOException
   */
  @Test(expected = AnnotatorException.class) public void testInvalidConfigurationFile() throws ConfigurationException,
      IOException {
    final File f = new File("wrong_config");
    if (!f.exists()) {
      f.createNewFile();
    }
    // write an empty xml file
    final BufferedWriter bw = new BufferedWriter(new FileWriter(f));
    bw.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?><diadem xml:space=\"preserve\"></diadem>");
    bw.flush();
    bw.close();
    parallel.setAnnotatorPoolFromConfigurationFile(new XMLConfiguration(f), null);
    f.delete();
  }

  /**
   * Check that if the annotators pool is empty no annotations are returned
   */
  @Test public void testAnnotateWithoutAnnotators() {
    parallel.setAnnotatorPool(new HashSet<Annotator>());
    final Set<Annotation> empty_annotations = parallel.annotateEntity(text);

    Assert.assertNotNull(empty_annotations);
    Assert.assertTrue(empty_annotations.size() == 0);
  }

  /**
   * Utiliy method to set the annotators pool from the configuration file and the timeout to 3 minutes
   * 
   * @throws ConfigurationException
   */
  private void setNormalConfiguration() throws ConfigurationException {
    parallel.setAnnotatorPoolFromConfigurationFile(new XMLConfiguration("conf/TextannotatorConfiguration.xml"),
        "parallel_annotator");
    parallel.setTimeout(180000);
  }

  @After public void deleteFiles() {
    final File f = new File("wrong_config");
    if (f.exists()) {
      f.delete();
    }

  }

}
