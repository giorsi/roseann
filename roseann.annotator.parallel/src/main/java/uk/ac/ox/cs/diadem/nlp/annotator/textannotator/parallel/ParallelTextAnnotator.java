/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.parallel;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.tree.ConfigurationNode;
import org.apache.commons.configuration.tree.xpath.XPathExpressionEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Sets;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;

/**
 * Class to run in parallel the annotators. Every annotator has its own thread and can complite its job in at most
 * waiting_time milliseconds. In the configuration file you can specify the annotator class to be invoked and waiting_time.
 * We can assign a text to be analyzed by the annotator, each thread submit text to the specific annotator
 * In order to add a new annotator class, the class must implement the Annotator interface and define a static method
 * getInstance() that returns an instance of the class
 * 
 * @author Stefano Ortona (stefano.ortona@gmail.com) Oxford University,Department of Computer Science
 */
public class ParallelTextAnnotator extends AnnotatorAdapter{

	/**
	 * Logger
	 */
	static final Logger LOGGER = LoggerFactory.getLogger(ParallelTextAnnotator.class);

	/**
	 * List of the annotators to be invoked
	 */
	private Set<Annotator> pool;

	/**
	 * Time in milliseconds to wait for every annotator, after that time the thread responsible of the annotator is killed
	 */
	private int waiting_time;

	/**
	 * singleton instance
	 */
	private static ParallelTextAnnotator INSTANCE;


	/**
	 * Public method to set the annotator pool from a configuration file.
	 * The configuration file must contain the name of the class of each annotator to be invoked 
	 * under the path nlp/textannotator/annotator[@name=annotator_name]/annotation_pool/annotator
	 * 
	 * @param conf An XMLConfiguration object representing an xml configuration file
	 * @param annotator_name The name of the annotator specified in the configuration file
	 * 
	 */
	public void setAnnotatorPoolFromConfigurationFile(XMLConfiguration conf,final String annotator_name){

		conf.setExpressionEngine(new XPathExpressionEngine());

		this.pool.clear();

		//read the configuration file
		try{
			HierarchicalConfiguration pr=conf.configurationAt("nlp/textannotator/annotator" +
					"[@name='"+getAnnotatorName()+"']/annotation_pool");
			@SuppressWarnings("unchecked")
			final List<ConfigurationNode> parameters=pr.getRootNode().getChildren();
			for(ConfigurationNode node:parameters){
				final String name=node.getName();
				if(!name.equals("annotator"))
					continue;
				String annotator_class=node.getValue().toString();
				LOGGER.info("Try to get the instance of the class {}",annotator_class);

				//for each annotator name class, try to get the instance. If not able to get an instance, log the exception and skip the annotator
				try{
					Class<?> c = Class.forName(annotator_class);
					Method m = c.getDeclaredMethod("getInstance",(Class<?>[]) null);
					Annotator annotator = (Annotator)m.invoke((Object[])null,(Object[])null);
					
					//set the same configuration of the current parallel annotator
					AnnotatorConfiguration individual_conf = new AnnotatorConfiguration();
					individual_conf.initializeConfiguration(annotator.getAnnotatorName(),getConfig().getConfigurationFileLocation());
					annotator.setConfig(individual_conf);
					pool.add(annotator);
				}
				catch(final Exception e){
					LOGGER.error("Not able to instantiate the annotator class {}",annotator_class,e);

				}
				LOGGER.info("The annotator class {} has been instantiated correctly",annotator_class);
			}
		}
		catch(final Exception e){
			LOGGER.error("Not able to load the configuration file {}. " +
					"The configuration file must contain the name of the class of each annotator to be invoked " +
					"under the path nlp/textannotator/annotator[@name=annotator_name]/annotation_pool/annotator",conf.getBasePath(),e);
			throw new AnnotatorException("Not able to load the configuration file "+conf.getBasePath()+
					". The configuration file must contain the name of the class of each annotator to be invoked " +
					"under the path nlp/textannotator/annotator[@name=annotator_name]/annotation_pool/annotator", LOGGER);
		}

		if(pool.size()==0)
			LOGGER.warn("No annotators have been specified " +
					"for the annotation pool in the configuration file {}",conf.getBasePath());
		else
			LOGGER.info("Annotation pool set correctly with {} annotators",pool.size());

	}

	/**
	 * Public method to set the annotator pool to be invoked
	 * @param pool	A set of annotator objects
	 */
	public void setAnnotatorPool(Set<Annotator> pool){
		this.pool.clear();
		this.pool.addAll(pool);
		if(pool.size()==0)
			LOGGER.warn("No annotators have been specified " +
					"for the annotation pool");
		else
			LOGGER.info("Annotation pool set correctly with {} annotators",pool.size());
	}

	/**
	 * private constructor to build the singleton instance
	 */
	private ParallelTextAnnotator() {

		ConfigurationFacility.getConfiguration();
		setAnnotatorName("parallel_annotator");

		//initialize the parametrs and the annotators pool
		AnnotatorConfiguration configuration = new AnnotatorConfiguration();
		configuration.initializeConfiguration(getAnnotatorName());
		setConfig(configuration);
		
		this.initializeResources();

	}
	
	@Override
	public void setConfig(AnnotatorConfiguration config){
		super.setConfig(config);
		this.initializeResources();
	}

	private void initializeResources() {

		//initialize the annotatos pool
		pool = Sets.newLinkedHashSet();
		try{
			setAnnotatorPoolFromConfigurationFile(new XMLConfiguration(getConfig().getConfigurationFileLocation()),
					getAnnotatorName());
		}
		catch(Exception e){
			LOGGER.warn("Not able to read the annotators pool from the configuration file {}," +
					" the pool is empty at the moment",getConfig().getConfigurationFileLocation());
		}

		// read from the configuration file the time_out value
		try {
			waiting_time = Integer.parseInt(getConfig().getSpecificParameters().get("timeout"));
			LOGGER.info("Timeout correctly set to {} milliseconds",waiting_time);
		} catch (Exception e) {
			LOGGER.warn("Missing timeout parameter in configuration file. Time out set to infinite.");
			waiting_time = 0;
		}

	}

	/**
	 * method to get the singleton instance
	 * 
	 * @return singleton instance
	 */
	public static synchronized Annotator getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ParallelTextAnnotator();
		}
		return INSTANCE;
	}

	/**
	 * Get the timeout
	 * @return	the timeout (milliseconds)
	 */
	public long getTimeout() {
		return waiting_time;
	}

	/**
	 * Set the timeout
	 * @param timeout	the timeout to be set (in milliseconds)
	 */
	public void setTimeout(final int timeout) {
		waiting_time = timeout;
	}

	/**
	 * If the timeout parameter has been specified in the configuration, then it will be used
	 */
	@Override 
	public Set<Annotation> annotateEntity(final String text) {

		LOGGER.info("Invoking parallel annotator on: '{}'", text);
		Set<Annotation> annotations = invokeAnnotators(text,(int)this.waiting_time);
		final int num_annotations = annotations.size();
		if (num_annotations == 0) {
			LOGGER.warn("No annotations produced for the following text: '{}'",text);
		}
		LOGGER.info("Parallel annotator process completed with {} annotations created", num_annotations);
		return annotations;
	}

	/**
	 * In this case the parameter timeout overrides the one written in the configuration file
	 */
	@Override 
	public Set<Annotation> annotateEntity(final String text,int timeout) {

		LOGGER.info("Invoking parallel annotator on: '{}'", text);
		Set<Annotation> annotations = invokeAnnotators(text,timeout);
		final int num_annotations = annotations.size();
		if (num_annotations == 0) {
			LOGGER.warn("No annotations produced for the following text: '{}'",text);
		}
		LOGGER.info("Parallel annotator process completed with {} annotations created", num_annotations);
		return annotations;
	}

	/**
	 * Main method to invoke all the annotators in parallel way, every annotator has its own thread 
	 * If there's a problem of any kind with one annotator, it just skips it and doesn't collect the answer
	 * 
	 * @param text
	 *          the text to be annotated
	 * @return A set of found annotations by invoking each annotator in the pool annotators
	 */
	private Set<Annotation> invokeAnnotators(final String text,int timeout) {
		final Set<Annotation> found_annotations=new HashSet<Annotation>();

		// list of thread,each thread take care of one specific annotator
		final List<Thread> threads = new LinkedList<Thread>();
		// map to associate to each thread an annotator_name
		final Map<Long, String> thread2annotator = new HashMap<Long, String>();
		int i = 0;

		for (final Annotator annotator : pool) {

			final Thread current_thread = new Thread(new InvokeAnnotator(annotator, text, found_annotations,timeout), 
					annotator.getClass().getName());
			thread2annotator.put(current_thread.getId(), annotator.getAnnotatorName());
			threads.add(current_thread);
			// start the thread and invoke the annotator
			try {
				LOGGER.info("Invoking {} annotator", annotator.getAnnotatorName());
				current_thread.start();
			}
			// if thread is not able to finish its job, just continue and save
			// into the log the problem
			catch (final IllegalThreadStateException e) {
				LOGGER.error("Thread with id {} encountered a problem.", threads.get(i).getId(), e);

				continue;
			} finally {
				i++;
			}
		}
		// wait every thread to finish their job
		long start, end = 0; // to calculate time for every thread
		for (final Thread t : threads) {
			start = System.currentTimeMillis();
			try {
				t.join(0);
				end = System.currentTimeMillis();
				final String annotatorName = thread2annotator.get(t.getId());

				LOGGER.info("Annotator {} completed in {} milliseconds.", annotatorName,end-start);

			} catch (final InterruptedException e) {
				LOGGER.error("Annotator {} was unable to complete its job.", thread2annotator.get(t.getId()));

				continue;
			}
		}

		return found_annotations;
	}

	/**
	 * private class to implement the thread's behavior
	 */
	private static class InvokeAnnotator implements Runnable {

		/**
		 * Thread takes care of one annotator
		 */
		private final Annotator annotator;

		/**
		 * Text to be analyzed
		 */
		private final String text;

		/**
		 * The set where to save the retrieved annotations
		 */
		private final Set<Annotation> found_annotations;

		/**
		 * The timeout to wait
		 */
		private int timeout;

		/**
		 * Constructor to initialize the thread job
		 * 
		 * @param annotator
		 * @param docId
		 * @param text
		 * @param inputModel
		 * @param outputModel
		 */
		public InvokeAnnotator(final Annotator annotator, final String text,Set<Annotation> found_annotations,int timeout) {
			this.annotator = annotator;
			this.text = text;
			this.found_annotations=found_annotations;
			this.timeout=timeout;

		}

		@Override public void run() {
			final long thread_id = Thread.currentThread().getId();
			final String annotator_class = annotator.getClass().getCanonicalName();
			LOGGER.debug("Thread with id {} takes care of {} class starts the process.", thread_id, annotator_class);
			try {
				final Set<Annotation> annotations = annotator.annotateEntity(text,timeout);
				if (annotations==null || annotations.size() == 0) {
					// save the failure in the mode
					LOGGER.warn("The annotator {} completed its job without finding any annotation",
							this.annotator.getAnnotatorName());

				}
				else{
					this.found_annotations.addAll(annotations);
					LOGGER.info("The annotator {} completed its job with {} annotations found",
							this.annotator.getAnnotatorName(),annotations.size());
				}

			} catch (final Exception e) {
				LOGGER.error("{} was not able to complet its job.", annotator.getAnnotatorName(), e);
			}

			LOGGER.debug("Thread with id {} takes care of {} class process done.", thread_id, annotator_class);
		}

	}

	/**
	 * In this case the submitToAnnotator method will invoke all the annotators in sequence and returns a string that is
	 * the concatenation of all the individual responses, separated by the @ character
	 */
	@Override
	public String submitToAnnotator(String freeText) {
		return this.submitToAnnotator(freeText, 0);
	}

	/**
	 * In this case the submitToAnnotator method will invoke all the annotators in sequence and returns a string that is
	 * the concatenation of all the individual responses, separated by the @ character
	 */
	@Override
	public String submitToAnnotator(String freeText,int timeout) {
		StringBuilder response_builder=new StringBuilder();
		for(Annotator annotator:this.pool){
			try{
				final String response = annotator.submitToAnnotator(freeText,timeout);
				response_builder.append(response);
				response_builder.append("~@~@~@~");
			}
			catch(Exception e){
				LOGGER.warn("Annotator {} was not able to produce the string response",annotator.getAnnotatorName(),e);
			}
		}

		return response_builder.toString();
	}

	/**
	 * Get the annotator pool
	 * @return	Set of annotators pool
	 */
	public Set<Annotator> getAnnotatorPool(){
		return this.pool;
	}

}
