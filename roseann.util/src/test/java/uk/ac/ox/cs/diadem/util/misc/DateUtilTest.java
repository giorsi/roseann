package uk.ac.ox.cs.diadem.util.misc;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;
import java.text.ParseException;

import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;
import uk.ac.ox.cs.diadem.util.misc.DateUtil;

public class DateUtilTest extends StandardTestcase {
	
	@Test
	public void testParseDate() throws ParseException {
		assertEquals(DateUtil.create(2011, 6, 18, 21, 32, 15),
				DateUtil.parseTimestamp("2011-06-18-21-32-15"));
	}

	
	@Test(expected=java.text.ParseException.class)
	public void testParseMissingSecond() throws ParseException {
		DateUtil.parseTimestamp("2011-06-18-21-32-");
	}

	@Test(expected=java.text.ParseException.class)
	public void testParseMissingTooLargeSecond() throws ParseException {
		DateUtil.parseTimestamp("2011-06-18-21-32-60");
	}

	@Test(expected=java.text.ParseException.class)
	public void testParseMissingTooManyFebruaries() throws ParseException {
		DateUtil.parseTimestamp("2011-02-32-21-32-59");
	}

	@Test
	public void testParseMissingManyFebruaries() throws ParseException {
		assertEquals(DateUtil.create(2012, 2, 28, 21, 32, 59),
				DateUtil.parseTimestamp("2012-02-28-21-32-59"));
		DateUtil.parseTimestamp("2012-02-28-21-32-59");
	}

	@Test
	public void testPrintTimestamp() throws ParseException, IOException {
		String DATE_STRINGS[]={"2012-02-28-21-32-59","2011-01-01-01-01-01"};
		for(String dateString : DATE_STRINGS) {
			StringWriter writer=new StringWriter();
			Date date=DateUtil.parseTimestamp(dateString);
			DateUtil.printTimestamp(date, writer);
			assertEquals(dateString,writer.toString());
		}
	}

	
	// TODO many cases missing
}
