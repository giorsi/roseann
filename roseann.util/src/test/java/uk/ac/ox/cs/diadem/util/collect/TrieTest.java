/**
 * 
 */
package uk.ac.ox.cs.diadem.util.collect;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.StringWriter;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;

import com.google.common.collect.Sets;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
public abstract class TrieTest extends StandardTestcase {

  class IntegerKeyMask extends SingletonKeyMask<Integer> {
    IntegerKeyMask(final int integer) {
      super(integer);
    }
  }

  protected abstract <T, V> Trie<T, V> getTrie();

  protected abstract <T, V> Trie<T, V> getTrie(boolean identityBased);

  private Trie<Integer, String> getSmallTrie() {
    final Trie<Integer, String> trie = getTrie();
    trie.putOverwrite("a");
    trie.putOverwrite("b", 1);
    trie.putOverwrite("c", 2);
    trie.putOverwrite("d", 1, 20);
    return trie;
  }

  private static class UncachedInteger {
    int i;

    UncachedInteger(final int i) {
      this.i = i;
    }

    @Override
    public boolean equals(final Object other) {
      if (other instanceof UncachedInteger) {
        final UncachedInteger that = (UncachedInteger) other;
        return that.i == i;
      }
      return false;
    }

    @Override
    public int hashCode() {
      return i;
    }
  }

  private static final UncachedInteger[] integers = { new UncachedInteger(0), new UncachedInteger(1),
      new UncachedInteger(2) };

  private Trie<UncachedInteger, String> getMediumIdentityTrie(final boolean identityBased) {
    final Trie<UncachedInteger, String> trie = getTrie(identityBased);
    int size = 0;
    assertEquals(trie.size(), 0);
    for (int i = 0; i < 3; ++i) {
      trie.putOverwrite(new Integer(i).toString(), integers[i]);
      ++size;
      assertEquals(trie.size(), size);
      for (int j = 0; j < 3; ++j) {
        trie.putOverwrite(new Integer(i).toString() + j, integers[i], integers[j]);
        ++size;
        assertEquals(trie.size(), size);
      }
    }
    return trie;
  }

  private Trie<Integer, String> getMediumTrie() { // somewhat bloody
    final Trie<Integer, String> trie = getTrie();
    int size = 0;
    assertEquals(trie.size(), 0);
    for (int i = 0; i < 3; ++i) {
      trie.putOverwrite(new Integer(i).toString(), i);
      ++size;
      assertEquals(trie.size(), size);
      for (int j = 0; j < 3; ++j) {
        trie.putOverwrite(new Integer(i).toString() + j, i, j);
        ++size;
        assertEquals(trie.size(), size);
      }
    }
    return trie;
  }

  private Trie<Integer, String> getLargeTrie() {
    return getLargeTrie(false);
  }

  private Trie<Integer, String> getLargeTrie(final boolean identityBased) {
    final Trie<Integer, String> trie = getTrie(identityBased);
    int size = 0;
    assertEquals(trie.size(), 0);
    for (int i = 0; i < 10; ++i) {
      trie.putOverwrite(new Integer(i).toString(), i);
      ++size;
      assertEquals(trie.size(), size);
      for (int j = 0; j < 10; ++j) {
        trie.putOverwrite(new Integer(i).toString() + j, i, j);
        ++size;
        assertEquals(trie.size(), size);
        for (int k = 0; k < 10; ++k) {
          trie.putOverwrite((new Integer(i).toString() + j) + k, i, j, k);
          ++size;
          assertEquals(trie.size(), size);
          for (int l = 0; l < 10; ++l) {
            trie.putOverwrite(((new Integer(i).toString() + j) + k) + l, i, j, k, l);
            ++size;
            assertEquals(trie.size(), size);
          }
        }
      }
    }
    return trie;
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.util.collect.TrieImplementation#Trie()}.
   */
  @Test
  public void testTrie() {
    final Trie<Integer, String> trie = getTrie();
    assertTrue(trie.isEmpty());
    assertEquals(trie.get(), null);
    assertEquals(trie.get(1), null);

    assertEquals(trie.putOverwrite("hu"), "hu");
    assertFalse(trie.isEmpty());
    assertEquals(trie.get(), "hu");

    trie.remove();
    assertTrue(trie.isEmpty());
    assertEquals(trie.get(), null);

    assertEquals(trie.putOverwrite("a"), "a");
    assertEquals(trie.putOverwrite("b", 1), "b");
    assertEquals(trie.putOverwrite("c", 2), "c");
    assertEquals(trie.putOverwrite("d", 1, 20), "d");
    assertFalse(trie.isEmpty());
    assertEquals(trie.get(), "a");
    assertEquals(trie.get(1), "b");
    assertEquals(trie.get(2), "c");
    assertEquals(trie.get(1, 20), "d");
    assertEquals(trie.get(1, 21), null);

    trie.remove(1);
    assertFalse(trie.isEmpty());
    assertEquals(trie.get(), "a");
    assertEquals(trie.get(1), null);
    assertEquals(trie.get(2), "c");
    assertEquals(trie.get(1, 20), "d");

    assertEquals(trie.putOverwrite("dd", 1, 20), "dd");
    assertFalse(trie.isEmpty());
    assertEquals(trie.get(), "a");
    assertEquals(trie.get(1), null);
    assertEquals(trie.get(2), "c");
    assertEquals(trie.get(1, 20), "dd");

  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.util.collect.TrieImplementation#Trie()}.
   */
  @Test
  public void testPutOverwriteTentatively() {
    final Trie<Integer, String> trie = getSmallTrie();
    // trie.putOverwrite("a");
    // trie.putOverwrite("b",1);
    // trie.putOverwrite("c",2);
    // trie.putOverwrite("d",1,20);
    assertEquals(trie.putOverwrite("aa"), "aa");
    assertEquals(trie.size(), 4);
    assertEquals(trie.putTentatively("a"), "aa");
    assertEquals(trie.size(), 4);
    assertEquals(trie.putTentatively("aa"), "aa");
    assertEquals(trie.size(), 4);
    assertEquals(trie.putTentatively("a", 5), "a");
    assertEquals(trie.size(), 5);
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.util.collect.TrieImplementation#clear()}.
   */
  @Test
  public void testClear() {
    final Trie<Integer, String> trie = getSmallTrie();
    assertFalse(trie.isEmpty());
    trie.clear();
    assertTrue(trie.isEmpty());
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.util.collect.TrieImplementation#Trie()}.
   */
  @Test
  public void testLargeTrieConstructionOnly() {
    getLargeTrie();
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.util.collect.TrieImplementation#Trie()}.
   */
  @Test
  public void testLargeTrie() {
    final Trie<Integer, String> trie = getLargeTrie();
    int size = 0;
    for (int i = 0; i < 10; ++i) {
      ++size;
      assertEquals(trie.get(i), new Integer(i).toString());
      for (int j = 0; j < 10; ++j) {
        ++size;
        assertEquals(trie.get(i, j), new Integer(i).toString() + j);
        for (int k = 0; k < 10; ++k) {
          ++size;
          assertEquals(trie.get(i, j, k), (new Integer(i).toString() + j) + k);
          for (int l = 0; l < 10; ++l) {
            ++size;
            assertEquals(trie.get(i, j, k, l), ((new Integer(i).toString() + j) + k) + l);
          }
        }
      }
    }

    for (int i = 0; i < 10; ++i) {
      trie.remove(i);
      --size;
      assertEquals(trie.size(), size);
      for (int j = 0; j < 10; ++j) {
        trie.remove(i, j);
        --size;
        assertEquals(trie.size(), size);
        for (int k = 0; k < 10; ++k) {
          trie.remove(i, j, k);
          --size;
          assertEquals(trie.size(), size);
          for (int l = 0; l < 10; ++l) {
            trie.remove(i, j, k, l);
            --size;
            assertEquals(trie.size(), size);
          }
        }
      }
    }

  }

  @Test
  public void testIterator() {
    final Trie<Integer, String> trie = getMediumTrie();

    final Set<String> expectedValues = new HashSet<String>();
    for (int i = 0; i < 3; ++i) {
      expectedValues.add(new Integer(i).toString());
      for (int j = 0; j < 3; ++j) {
        expectedValues.add(new Integer(i).toString() + j);
      }
    }

    final Set<String> actualValues = new HashSet<String>();
    for (final String string : trie) {
      actualValues.add(string);
    }

    assertEquals(expectedValues, actualValues);
  }

  @Test
  public void testIteratorLargeTrie() {
    final Trie<Integer, String> trie = getLargeTrie();

    final Set<String> expectedValues = new HashSet<String>();
    for (int i = 0; i < 10; ++i) {
      expectedValues.add(new Integer(i).toString());
      for (int j = 0; j < 10; ++j) {
        expectedValues.add(new Integer(i).toString() + j);
        for (int k = 0; k < 10; ++k) {
          expectedValues.add((new Integer(i).toString() + j) + k);
          for (int l = 0; l < 10; ++l) {
            expectedValues.add(((new Integer(i).toString() + j) + k) + l);
          }
        }
      }
    }

    final Set<String> actualValues = new HashSet<String>();
    for (final String string : trie) {
      actualValues.add(string);
    }

    assertEquals(expectedValues, actualValues);
  }

  @Test
  public void testIteratorLargeTrie2() {
    final Trie<Integer, String> trie = getLargeTrie();

    final Set<String> expectedValues = new HashSet<String>();
    for (int i = 0; i < 10; ++i) {
      expectedValues.add(new Integer(i).toString());
      for (int j = 0; j < 10; ++j) {
        expectedValues.add(new Integer(i).toString() + j);
        for (int k = 0; k < 10; ++k) {
          expectedValues.add((new Integer(i).toString() + j) + k);
          for (int l = 0; l < 10; ++l) {
            expectedValues.add(((new Integer(i).toString() + j) + k) + l);
          }
        }
      }
    }

    final Set<String> actualValues = new HashSet<String>();
    for (final String string : trie.iterator()) {
      actualValues.add(string);
    }

    assertEquals(expectedValues, actualValues);
  }

  @Test
  public void testEmptyIterator() {
    Trie<Integer, String> trie = getMediumTrie();
    assertFalse(trie.isEmpty());
    assertTrue(trie.iterator().hasNext());
    trie.clear();
    assertTrue(trie.isEmpty());
    assertFalse(trie.iterator().hasNext());

    trie = getTrie();
    assertTrue(trie.isEmpty());
    assertFalse(trie.iterator().hasNext());
  }

  @Test
  public void testIteratorForKeys() {
    Trie<Integer, String> trie = getMediumTrie();

    IterableIterator<String> iter = trie.iterator();
    assertHaveSameValues(iter, "0", "1", "2", "00", "01", "02", "10", "11", "12", "20", "21", "22");

    iter = trie.iteratorForKeys(1);
    assertHaveSameValues(iter, "1");

    iter = trie.iteratorForKeys(1, null);
    assertHaveSameValues(iter, "10", "11", "12");

    iter = trie.iteratorForKeys(null, null);
    assertHaveSameValues(iter, "00", "01", "02", "10", "11", "12", "20", "21", "22");

    iter = trie.iteratorForKeys(null, 2);
    assertHaveSameValues(iter, "02", "12", "22");

    iter = trie.iteratorForKeys(null, null, null);
    assertHaveSameValues(iter);

    trie.clear();
    assertTrue(trie.isEmpty());
    assertFalse(trie.iteratorForKeys(1).hasNext());
    assertFalse(trie.iteratorForKeys(1, null).hasNext());
    assertFalse(trie.iteratorForKeys(null, 1).hasNext());

    trie = getLargeTrie();
    iter = trie.iteratorForKeys(1, null, 3);
    assertHaveSameValues(iter, "103", "113", "123", "133", "143", "153", "163", "173", "183", "193");
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testIteratorForKeyMask() {
    Trie<Integer, String> trie = getMediumTrie();

    IterableIterator<String> iter = trie.iterator();
    assertHaveSameValues(iter, "0", "1", "2", "00", "01", "02", "10", "11", "12", "20", "21", "22");

    iter = trie.iteratorForMasks(new IntegerKeyMask(1));
    assertHaveSameValues(iter, "1");

    iter = trie.iteratorForMasks(new IntegerKeyMask(1), null);
    assertHaveSameValues(iter, "10", "11", "12");

    iter = trie.iteratorForMasks(null, null);
    assertHaveSameValues(iter, "00", "01", "02", "10", "11", "12", "20", "21", "22");

    iter = trie.iteratorForMasks(null, new IntegerKeyMask(2));
    assertHaveSameValues(iter, "02", "12", "22");

    iter = trie.iteratorForMasks(null, null, null);
    assertHaveSameValues(iter);

    iter = trie.iteratorForMasks(new NegatedKeyMask<Integer>(new IntegerKeyMask(2)), null);
    assertHaveSameValues(iter, "00", "01", "02", "10", "11", "12");

    iter = trie.iteratorForMasks(new NegatedKeyMask<Integer>(new IntegerKeyMask(2)), new NegatedKeyMask<Integer>(
        new IntegerKeyMask(1)));
    assertHaveSameValues(iter, "00", "02", "10", "12");

    iter = trie.iteratorForMasks(new NegatedKeyMask<Integer>(new IntegerKeyMask(2)), new IntegerKeyMask(0));
    assertHaveSameValues(iter, "00", "00", "10", "10");

    trie.clear();
    assertTrue(trie.isEmpty());
    assertFalse(trie.iteratorForMasks(new IntegerKeyMask(1)).hasNext());
    assertFalse(trie.iteratorForMasks(new IntegerKeyMask(1), null).hasNext());
    assertFalse(trie.iteratorForMasks(null, new IntegerKeyMask(1)).hasNext());

    trie = getLargeTrie();
    iter = trie.iteratorForMasks(new IntegerKeyMask(1), null, new IntegerKeyMask(3));
    assertHaveSameValues(iter, "103", "113", "123", "133", "143", "153", "163", "173", "183", "193");

  }

  @SuppressWarnings("unchecked")
  private static <V> void assertHaveSameValues(final IterableIterator<V> iter, final V... values) {
    final Set<V> valueSet = Sets.newHashSet(values);
    while (iter.hasNext()) {
      final V value = iter.next();
      assertTrue(valueSet.contains(value));
      valueSet.remove(value);
    }
    assertEquals(valueSet.size(), 0);
  }

  private static <V> String iteratorValuestoString(final IterableIterator<V> iter) {
    final SortedSet<V> valueSet = new TreeSet<V>();
    while (iter.hasNext()) {
      final V value = iter.next();
      valueSet.add(value);
    }

    final StringWriter writer = new StringWriter();
    for (final V value : valueSet) {
      writer.append(value.toString());
      writer.append(' ');
    }
    return writer.toString();
  }

  @Test
  public void testIdentityBasedTrie() {
    final Trie<UncachedInteger, String> trie = getMediumIdentityTrie(true);

    for (int i = 0; i < 3; ++i) {
      assertTrue(trie.get(integers[i]) != null);
      for (int j = 0; j < 3; ++j) {
        assertTrue(trie.get(integers[i], integers[j]) != null);
      }
    }

    for (int i = 0; i < 3; ++i) {
      assertFalse(trie.get(new UncachedInteger(i)) != null);
      for (int j = 0; j < 3; ++j) {
        assertFalse(trie.get(new UncachedInteger(i), new UncachedInteger(j)) != null);
      }
    }
  }

  @Test
  public void testIteratorLargeTrieIdentityBased() {
    final Trie<Integer, String> trie = getLargeTrie(true);

    final Set<String> expectedValues = new HashSet<String>();
    for (int i = 0; i < 10; ++i) {
      expectedValues.add(new Integer(i).toString());
      for (int j = 0; j < 10; ++j) {
        expectedValues.add(new Integer(i).toString() + j);
        for (int k = 0; k < 10; ++k) {
          expectedValues.add((new Integer(i).toString() + j) + k);
          for (int l = 0; l < 10; ++l) {
            expectedValues.add(((new Integer(i).toString() + j) + k) + l);
          }
        }
      }
    }

    final Set<String> actualValues = new HashSet<String>();
    for (final String string : trie) {
      actualValues.add(string);
    }

    assertEquals(expectedValues, actualValues);
  }

  @Test
  // repeated to see how the second call realtes to the first one timewise
  public void testIteratorLargeTrieIdentityBased2() {
    final Trie<Integer, String> trie = getLargeTrie(true);

    final Set<String> expectedValues = new HashSet<String>();
    for (int i = 0; i < 10; ++i) {
      expectedValues.add(new Integer(i).toString());
      for (int j = 0; j < 10; ++j) {
        expectedValues.add(new Integer(i).toString() + j);
        for (int k = 0; k < 10; ++k) {
          expectedValues.add((new Integer(i).toString() + j) + k);
          for (int l = 0; l < 10; ++l) {
            expectedValues.add(((new Integer(i).toString() + j) + k) + l);
          }
        }
      }
    }

    final Set<String> actualValues = new HashSet<String>();
    for (final String string : trie) {
      actualValues.add(string);
    }

    assertEquals(expectedValues, actualValues);
  }

  @Test
  public void testClone() throws CloneNotSupportedException {
    final Trie<Integer, String> trie = getMediumTrie();

    for (int i = 0; i < 3; ++i) {
      assertEquals(trie.get(i), new Integer(i).toString());
      for (int j = 0; j < 3; ++j) {
        assertEquals(trie.get(i, j), new Integer(i).toString() + j);
      }
    }

    final Trie<Integer, String> otherTrie = trie.clone();

    for (int i = 0; i < 3; ++i) {
      assertTrue(trie.get(i) == otherTrie.get(i));
      for (int j = 0; j < 3; ++j) {
        assertTrue(trie.get(i, j) == otherTrie.get(i, j));
      }
    }

    trie.clear();

    for (int i = 0; i < 3; ++i) {
      assertEquals(otherTrie.get(i), new Integer(i).toString());
      for (int j = 0; j < 3; ++j) {
        assertEquals(otherTrie.get(i, j), new Integer(i).toString() + j);
      }
    }
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testPrefixIteration() {
    Trie<Integer, String> trie = getMediumTrie();

    IterableIterator<String> iter = trie.iteratorForPrefixKeys(new Integer(1));
    assertHaveSameValues(iter, "1", "10", "11", "12");

    iter = trie.iteratorForPrefixKeys(new Integer(1), null);
    assertHaveSameValues(iter, "10", "11", "12");

    iter = trie.iteratorForPrefixKeys(null, null);
    assertHaveSameValues(iter, "00", "01", "02", "10", "11", "12", "20", "21", "22");

    iter = trie.iteratorForPrefixKeys(new Integer[] { null });
    assertHaveSameValues(iter, "0", "1", "2", "00", "01", "02", "10", "11", "12", "20", "21", "22");

    trie = getLargeTrie();
    iter = trie.iteratorForPrefixKeys(new Integer(1), null);
    assertTrue(database.check("largePrefix-1-n", iteratorValuestoString(iter)));

    trie = getLargeTrie();
    iter = trie.iteratorForPrefixKeys(new Integer(2), null, new Integer(3));
    assertTrue(database.check("largePrefix-2-n-3", iteratorValuestoString(iter)));

    trie = getLargeTrie();
    iter = trie.iteratorForPrefixMasks(new IntegerKeyMask(2), new IntegerKeyMask(6), new NegatedKeyMask<Integer>(
        new IntegerKeyMask(5)));
    assertTrue(database.check("largePrefix-2-6-n5", iteratorValuestoString(iter)));
  }
}
