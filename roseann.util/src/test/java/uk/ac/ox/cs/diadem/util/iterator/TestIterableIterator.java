package uk.ac.ox.cs.diadem.util.iterator;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;
import uk.ac.ox.cs.diadem.util.collect.IterableIterator;
import uk.ac.ox.cs.diadem.util.collect.WrappingIterableIterator;

import com.google.common.base.Predicates;
import com.google.common.collect.Lists;

public class TestIterableIterator extends StandardTestcase {

  @Test
  @Ignore
  public void testIterable() {
    // we test that two following for loop produce the same result
    final Iterable<Integer> list = Lists.newArrayList(1, 2);

    Integer count = 0;

    for (@SuppressWarnings("unused")
    final Integer x : list) {
      count++;
    }

    for (@SuppressWarnings("unused")
    final Integer x : list) {
      count++;
    }

    // using the WrappingIterableIterator
    final IterableIterator<Integer> iterator = new WrappingIterableIterator<Integer>(list.iterator(),
        Predicates.<Integer> alwaysTrue());

    int otherCount = 0;
    for (@SuppressWarnings("unused")
    final Integer x : iterator) {
      otherCount++;
    }
    for (@SuppressWarnings("unused")
    final Integer x : iterator) {
      otherCount++;
    }

    assertEquals("Same count", Integer.valueOf(count), Integer.valueOf(otherCount));

  }

}
