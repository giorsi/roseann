/**
 * 
 */
package uk.ac.ox.cs.diadem.util.collect;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
public class TrieImplementationTest extends TrieTest {

  @Override
  protected <T, V> Trie<T, V> getTrie() {
    return new TrieImplementation<T, V>();
  }

  @Override
  protected <T, V> Trie<T, V> getTrie(boolean identityBased) {
    return new TrieImplementation<T, V>(identityBased);
  }
}
