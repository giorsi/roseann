package uk.ac.ox.cs.diadem.util.reflection;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.ParameterizedType;

import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;

public class ReflectionUtilTest extends StandardTestcase {

  static private interface Root<Z> {
  }

  static private interface Inter<Y, Z> extends Root<Z> {
  }

  static private class Base<X> {
  }

  static private class Derived<X, Y, Z> extends Base<X> implements Inter<Y, Z> {
  }

  static private class Derived2 extends Derived<Integer, Double, String> {
  }

  static private class Derived3 extends Derived2 {
  }

  class XX<T> extends Base<Integer> {
    public Class<?> returnedClass() {
      ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
      return (Class<?>) parameterizedType.getActualTypeArguments()[0];
    }
  }

  @Test
  public void testGetDirectTypeParameter() {
    // no type information on arguments recoverable, as the type parameters are not determined within the hierarchy
    Derived<Integer, Double, String> d = new Derived<Integer, Double, String>();
    assertEquals(ReflectionUtil.getTypeArgument(d.getClass(), Derived.class, 0), null);
    assertEquals(ReflectionUtil.getTypeArgument(d.getClass(), Derived.class, 1), null);
    assertEquals(ReflectionUtil.getTypeArgument(d.getClass(), Derived.class, 2), null);
    assertEquals(ReflectionUtil.getTypeArgument(d.getClass(), Inter.class, 0), null);
  }

  @Test
  public void testGetIndirectTypeParameter() {
    Derived2 d3 = new Derived3();
    assertEquals(ReflectionUtil.getTypeArgument(d3.getClass(), Inter.class, 0), Double.class);
    assertEquals(ReflectionUtil.getTypeArgument(d3.getClass(), Inter.class, 1), String.class);
    assertEquals(ReflectionUtil.getTypeArgument(d3.getClass(), Derived.class, 0), Integer.class);
    assertEquals(ReflectionUtil.getTypeArgument(d3.getClass(), Derived.class, 1), Double.class);
    assertEquals(ReflectionUtil.getTypeArgument(d3.getClass(), Derived.class, 2), String.class);
    assertEquals(ReflectionUtil.getTypeArgument(d3.getClass(), Base.class, 0), Integer.class);
    assertEquals(ReflectionUtil.getTypeArgument(d3.getClass(), Inter.class, 0), Double.class);
    assertEquals(ReflectionUtil.getTypeArgument(d3.getClass(), Inter.class, 1), String.class);
    assertEquals(ReflectionUtil.getTypeArgument(d3.getClass(), Root.class, 0), String.class);

    assertEquals(ReflectionUtil.getTypeArgument(Derived3.class, Inter.class, 0), Double.class);
  }

  @Test(expected = AssertionError.class)
  public void testGetTypeParameterOutOfBound() {
    Derived2 d3 = new Derived3();
    assertEquals(ReflectionUtil.getTypeArgument(d3.getClass(), Inter.class, 0), Double.class);
    assertEquals(ReflectionUtil.getTypeArgument(d3.getClass(), Inter.class, 1), Double.class);
    assertEquals(ReflectionUtil.getTypeArgument(d3.getClass(), Inter.class, 2), Double.class);
  }

}
