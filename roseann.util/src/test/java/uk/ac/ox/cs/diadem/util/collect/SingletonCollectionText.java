package uk.ac.ox.cs.diadem.util.collect;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;

public class SingletonCollectionText extends StandardTestcase {
  @Test
  public void testScratch() {
    database.setMethodKeyPrefix();

    final Collection<Integer> collection = SingletonCollection.create(5);
    assertTrue(database.check("5", collection.toString()));

    Integer[] arr = collection.toArray(new Integer[1]);
    for (final Integer i : arr) {
      assertTrue(database.check("5/1", i.toString()));
    }

    arr = collection.toArray(new Integer[0]);
    for (final Integer i : arr) {
      assertTrue(database.check("5/1", i.toString()));
    }

  }
}
