/**
 * 
 */
package uk.ac.ox.cs.diadem.util.misc;

import static org.junit.Assert.assertEquals;
import static uk.ac.ox.cs.diadem.util.misc.StreamTokenizer.State.CHAR;
import static uk.ac.ox.cs.diadem.util.misc.StreamTokenizer.State.EOF;
import static uk.ac.ox.cs.diadem.util.misc.StreamTokenizer.State.LONG;
import static uk.ac.ox.cs.diadem.util.misc.StreamTokenizer.State.STRING;
import static uk.ac.ox.cs.diadem.util.misc.StreamTokenizer.State.WORD;

import java.io.IOException;
import java.io.StringReader;

import org.junit.Test;

import uk.ac.ox.cs.diadem.env.ParserException;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
public class StreamTokenizerTest {

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.util.misc.StreamTokenizer#StreamTokenizer(java.io.Reader)}.
   * 
   * @throws IOException
   * @throws ParseErrorException
   */
  @Test
  public final void testStreamTokenizer() throws IOException, ParserException {
    // final StringReader reader = new StringReader("two ids. and a \"dot \" \" \\\"\"");
    // disabled above version because we are not dealing with double quotes inside strings (as DLV does not).
    final StringReader reader = new StringReader("two ids. and a \"dot \"  ");
    final StreamTokenizer tokenizer = new StreamTokenizer(reader);

    assertEquals(tokenizer.nextToken(), WORD);
    assertEquals(tokenizer.getWord(), "two");
    assertEquals(tokenizer.getTokenAsString(), "two");

    assertEquals(tokenizer.nextToken(), WORD);
    assertEquals(tokenizer.getWord(), "ids");
    assertEquals(tokenizer.getTokenAsString(), "ids");

    assertEquals(tokenizer.nextToken(), CHAR);
    assertEquals(tokenizer.getChar(), '.');
    assertEquals(tokenizer.getTokenAsString(), ".");

    assertEquals(tokenizer.nextToken(), WORD);
    assertEquals(tokenizer.getWord(), "and");
    assertEquals(tokenizer.getTokenAsString(), "and");

    assertEquals(tokenizer.nextToken(), WORD);
    assertEquals(tokenizer.getWord(), "a");
    assertEquals(tokenizer.getTokenAsString(), "a");

    assertEquals(tokenizer.nextToken(), STRING);
    assertEquals(tokenizer.getString(), "dot ");
    assertEquals(tokenizer.getTokenAsString(), "dot ");

    // assertFalse(tokenizer.isEmpty());
    // assertEquals(tokenizer.nextToken(), STRING);
    // assertEquals(tokenizer.getString(), " \\");
    // assertEquals(tokenizer.getTokenAsString(), " \\");

    assertEquals(tokenizer.nextToken(), EOF);
    assertEquals(tokenizer.getTokenAsString(), "EOF");
  }

  @Test
  public final void testInteger() throws IOException, ParserException {
    final StringReader reader = new StringReader("1 123 -123 -1");
    final StreamTokenizer tokenizer = new StreamTokenizer(reader);

    assertEquals(tokenizer.nextToken(), LONG);
    assertEquals(tokenizer.getLong(), 1);
    assertEquals(tokenizer.getTokenAsString(), "1");

    assertEquals(tokenizer.nextToken(), LONG);
    assertEquals(tokenizer.getLong(), 123);
    assertEquals(tokenizer.getTokenAsString(), "123");

    assertEquals(tokenizer.nextToken(), LONG);
    assertEquals(tokenizer.getLong(), -123);
    assertEquals(tokenizer.getTokenAsString(), "-123");

    assertEquals(tokenizer.nextToken(), LONG);
    assertEquals(tokenizer.getLong(), -1);
    assertEquals(tokenizer.getTokenAsString(), "-1");

    assertEquals(tokenizer.nextToken(), EOF);
    assertEquals(tokenizer.getTokenAsString(), "EOF");
  }
}
