package uk.ac.ox.cs.diadem.util.collect;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;

public class CollectionHelperTest extends StandardTestcase {

  @Test
  public void testReverse() {
    database.setMethodKeyPrefix();

    final Integer[] sizes = { 0, 1, 2, 3, 10, 20 };
    for (final int size : sizes) {
      final ArrayList<Integer> list = new ArrayList<Integer>();
      for (int i = 0; i < size; ++i) {
        list.add(i);
      }
      assertTrue(database.check(Integer.toString(size), list.toString()));
      assertTrue(database.check(Integer.toString(size) + "R", CollectionHelpers.reverse(list).toString()));
    }
  }
}
