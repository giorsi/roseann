package uk.ac.ox.cs.diadem.util.reflection;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class ReflectionUtil {
  /**
   * Get the underlying class for a type, or null if the type is a variable type.
   * 
   * @param type
   *          the type
   * @return the underlying class
   */
  public static Class<?> getClass(Type type) {
    if (type instanceof Class) {
      @SuppressWarnings("rawtypes")
      final Class result = (Class) type;
      return result;
    } else if (type instanceof ParameterizedType)
      return getClass(((ParameterizedType) type).getRawType());
    else if (type instanceof GenericArrayType) {
      Type componentType = ((GenericArrayType) type).getGenericComponentType();
      Class<?> componentClass = getClass(componentType);
      if (componentClass != null)
        return Array.newInstance(componentClass, 0).getClass();
      else
        return null;
    } else
      return null;
  }

  /**
   * Get the actual type arguments a child class has used to extend a generic base class. The method does not allow to
   * determine type parameters which are determined WITHIN THE type hierarchy. For example, for
   * {@code A<Integer> ainteger} , we get {@code getTypeArgument(ainteger.getClass(),A.class,0)==null} but for
   * {@code B inherits A<Integer>}, we get {@code getTypeArgument(B.class, A.class,0)==Integer.class}.
   * 
   * @param childClass
   *          the child class
   * @param baseClass
   *          the base class
   * @param index
   *          the number of the type argument to obtain
   * 
   * @return the index-th type argument of childClass at ancestor class baseClass.
   * 
   * @throws IndexOutOfBoundsException
   *           if the index
   */
  public static Class<?> getTypeArgument(Class<?> childClass, Class<?> baseClass, int index) {
    assert index >= 0;
    assert childClass != null;
    assert baseClass != null;

    Map<Type, Type> resolvedTypes = new HashMap<Type, Type>();
    Type targetType = null;

    Stack<Type> stack = new Stack<Type>();
    stack.push(childClass);

    while (!stack.empty()) {
      Type type = stack.pop();

      if (type instanceof ParameterizedType) {
        ParameterizedType parameterizedType = (ParameterizedType) type;
        Class<?> rawType = (Class<?>) parameterizedType.getRawType();
        TypeVariable<?>[] typeParameters = rawType.getTypeParameters();
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();

        for (int i = 0; i < actualTypeArguments.length; ++i)
          resolvedTypes.put(typeParameters[i], actualTypeArguments[i]);
      }

      if (getClass(type).equals(baseClass)) {
        targetType = type;
        break;
      }

      if (getClass(type) != null) {
        if (getClass(type).getGenericSuperclass() != null)
          stack.push(getClass(type).getGenericSuperclass());
        for (Type inter : getClass(type).getGenericInterfaces())
          stack.push(inter);
      }
    }

    if (targetType == null)
      return null;

    if (targetType instanceof ParameterizedType) {
      ParameterizedType parameterizedType = (ParameterizedType) targetType;
      assert index < parameterizedType.getActualTypeArguments().length : "Type '" + parameterizedType
          + "' has no type parameter of index '" + index + "'.";
      Type targetArg = parameterizedType.getActualTypeArguments()[index];

      for (; resolvedTypes.containsKey(targetArg); targetArg = resolvedTypes.get(targetArg))
        ;

      return getClass(targetArg);
    }
    return null;
  }

  public static String getMethodName() {
    return Thread.currentThread().getStackTrace()[2].getMethodName();
  }
}
