package uk.ac.ox.cs.diadem.util.collect;

import java.util.Iterator;


public class IteratorOfIterators<E> implements IterableIterator<E> {
	private Iterator<Iterator<E>> masterIterator;
	private Iterator<E> currentIterator=null;
	private Iterator<E> lastIterator;
	
	public IteratorOfIterators(Iterator<Iterator<E>> masterIterator) {
		assert masterIterator!=null;
		this.masterIterator=masterIterator;
		advance();
	}

	private final void advance() {
		lastIterator=currentIterator;
		while(masterIterator.hasNext()) {
			currentIterator=masterIterator.next();
			if(currentIterator.hasNext()) return;
		}
		currentIterator=null;
	}
	
	@Override
	public Iterator<E> iterator() { return this; }

	@Override
	public boolean hasNext() { return currentIterator!=null; }

	@Override
	public E next() {
		assert currentIterator!=null : "Invalid Protocol";
		final E result=currentIterator.next();
		if(!currentIterator.hasNext()) advance();
		else lastIterator=null;
		return result;
	}

	@Override
	public void remove() {
		if(lastIterator==null) currentIterator.remove(); else lastIterator.remove();
	}
}
