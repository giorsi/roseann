/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.util.misc;

import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;

/**
 * Contains a collection of methods that are necessary due to defective interfaces and should be replaced over time.
 */
public class EvilHelper {
  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(EvilHelper.class);
  @SuppressWarnings("unused")
  private static final Configuration config = ConfigurationFacility.getConfiguration();

  /**
   * Returns a URL for the given string catching any {@link MalformedURLException} and converting them into
   * EvilHelperException, a type of {@link RuntimeException}.
   * 
   * @param s
   * @return
   */
  public static URL asUrlUnchecked(final String s) {
    try {
      return new URL(s);
    } catch (final MalformedURLException e) {
      throw new EvilHelperException(e);
    }
  }

}
