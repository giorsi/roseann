package uk.ac.ox.cs.diadem.util.collect;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
public class NegatedKeyMask<Key> implements KeyMask<Key> {
  final private KeyMask<Key> keyMask;

  public NegatedKeyMask(KeyMask<Key> keyMask) {
    assert keyMask != null : "Invalid Argument.";
    this.keyMask = keyMask;
  }

  @Override
  public boolean matchKeyByIdentity(Key key) {
    assert key != null : "Invalid Argument.";
    return !keyMask.matchKeyByIdentity(key);
  }

  @Override
  public Key singleyKeyMatch() {
    return null;
  }

  @Override
  public boolean matchKeyByEquality(Key key) {
    assert key != null : "Invalid Argument.";
    return !keyMask.matchKeyByIdentity(key);
  }

  @Override
  public String toString() {
    return "<NOT " + keyMask + " >";
  }
}
