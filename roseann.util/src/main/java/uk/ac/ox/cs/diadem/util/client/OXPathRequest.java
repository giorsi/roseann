/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.util.client;

/**
 * @author timfu
 */
public class OXPathRequest {
  private String id;
  private String expression;

  /**
   * Sets the id of this GeneratorServerPayload.
   */
  public void setId(final String id) {
    this.id = id;
  }

  /**
   * Returns the id associated with this GeneratorServerPayload.
   */
  public String getId() {
    return id;
  }

  /**
   * Returns the expression associated with this OXPathRequest.
   */
  public String getExpression() {
    return expression;
  }

  /**
   * Sets the expression of this OXPathRequest.
   */
  public void setExpression(final String expression) {
    this.expression = expression;
  }

}
