package uk.ac.ox.cs.diadem.util.misc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

/**
 * Some handy methods for writing and reading files with proper encoding/decoding.
 */
public class CharsetFileUtils {
  public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

  public static CharsetDecoder getDefaultDecoder() {
    return DEFAULT_CHARSET.newDecoder();
  }

  public static CharsetEncoder getDefaultEncoder() {
    return DEFAULT_CHARSET.newEncoder();
  }

  /**
   * Retrieve a writer with the given encoding for the given file. Warning: unbuffered.
   * @throws FileNotFoundException
   */
  public static Writer getFileWriterWithDefaultEncoding(final File f) throws FileNotFoundException {
    return new OutputStreamWriter(new FileOutputStream(f), getDefaultEncoder());
  }

  /**
   * Retrieve a reader with the given encoding for the given file. Warning: unbuffered.
   * @throws FileNotFoundException
   */
  public static Reader getFileReaderWithDefaultEncoding(final File f) throws FileNotFoundException {
    return new InputStreamReader(new FileInputStream(f), getDefaultDecoder());
  }

  /**
   * Return a reader with the given encoding reading from the given stream. Warning: unbuffered.
   */
  public static InputStreamReader getInputStreamReaderWithDefaultEncoding(final InputStream iStream) {
    return new InputStreamReader(iStream, getDefaultDecoder());
  }
}
