package uk.ac.ox.cs.diadem.util.functions;

import uk.ac.ox.cs.diadem.util.misc.EscapingUtils;

import com.google.common.base.Function;

public class FunctionUtils {

  private FunctionUtils() {
  }

  /**
   * 
   * 
   * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
   */
  public static enum CapitalizeFunction implements Function<String, String> {
    INSTANCE;

    CapitalizeFunction() {
    }

    @Override
    public String apply(final String input) {
      return input.toUpperCase();
    }

  }

  /**
   * 
   * 
   * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
   */
  public static enum UncapitalizeFunction implements Function<String, String> {
    INSTANCE;
    UncapitalizeFunction() {
    }

    @Override
    public String apply(final String input) {
      return input.toLowerCase();
    }
  }

  public static enum DoubleQuoteFunction implements Function<String, String> {
    INSTANCE;

    DoubleQuoteFunction() {
    }

    @Override
    public String apply(final String input) {
      return EscapingUtils.quoteUnquotedString(input);
    }

  }

}
