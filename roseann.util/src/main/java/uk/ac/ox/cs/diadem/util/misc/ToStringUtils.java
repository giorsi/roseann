package uk.ac.ox.cs.diadem.util.misc;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.log4j.lf5.util.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.DiademRuntimeException;

/**
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public class ToStringUtils {

  private static final Logger LOGGER = LoggerFactory.getLogger(ToStringUtils.class);

  private final String separator;
  private final String arrow;

  private static final ToStringUtils instance = new ToStringUtils(System.getProperty("line.separator"), "->");

  public static ToStringUtils getInstance() {
    return instance;
  }

  public String toString(final InputStream inputStream) {
    final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    try {
      StreamUtils.copy(inputStream, outputStream);
    } catch (final IOException e) {
      LOGGER.error("failed toString from inputStream: '{}'", e.getMessage());
      throw new DiademRuntimeException("failed toString from inputStream", e, LOGGER);
    }
    return outputStream.toString();
  }

  private ToStringUtils(final String separator, final String arrow) {
    this.separator = separator;
    this.arrow = arrow;
  }

  public String toString(final List<?> l) {
    final StringBuilder sb = new StringBuilder("(");
    String sep = "";
    for (final Object object : l) {
      sb.append(sep).append(object.toString());
      sep = separator;
    }
    return sb.append(")").toString();
  }

  public String toString(final Map<?, ?> m) {
    final StringBuilder sb = new StringBuilder("[");
    String sep = "";
    for (final Object object : m.keySet()) {
      sb.append(sep).append(object.toString()).append(arrow).append(m.get(object).toString());
      sep = separator;
    }
    return sb.append("]").toString();
  }

  public String toString(final Set<?> s) {
    final StringBuilder sb = new StringBuilder("{");
    String sep = "";
    for (final Object object : s) {
      sb.append(sep).append(object.toString());
      sep = separator;
    }
    return sb.append("}").toString();
  }

}
