/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.util.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.DiademRuntimeException;
import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * Retrieves enum from enum constants and interface classes.
 */
public class ReflectionEnumHelper {
  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(ReflectionEnumHelper.class);
  @SuppressWarnings("unused")
  private static final Configuration config = ConfigurationFacility.getConfiguration();

  public static final String DEFAULT_TYPE_PACKAGE = "uk.ac.ox.cs.diadem.dom.shadow.pagemodel.types";

  private static final LoadingCache<String, Reflections> cache = CacheBuilder.newBuilder().maximumSize(10)
      .build(new CacheLoader<String, Reflections>() {
        @Override
        public Reflections load(final String packAge) {
          return new Reflections(packAge);
        }
      });

  /**
   * Gets the enum for the given string searching in all enums of the given interface class in the given package,
   * possibly uppercasing the given value.
   */
  @SuppressWarnings("unchecked")
  public static <T> T enumFromString(final String enumValue, final Class<T> interFace, final String packAge,
      final boolean upperCase) {
    final T value = enumFromStringHelper(enumValue, interFace, packAge, upperCase);
    if (value == null) {
      logger.warn("No enum found for {}", enumValue);
    }
    return value;
  }

  private static <T> T enumFromStringHelper(final String enumValue, final Class<T> interFace, final String packAge,
      final boolean upperCase) {
    Reflections reflections;
    try {
      reflections = cache.get(packAge);
    } catch (final ExecutionException e1) {
      throw new DiademRuntimeException("Retrieval of reflection for package " + packAge + " failed.", e1, logger);
    }

    T value = null;
    final Set<Class<? extends T>> subTypes = reflections.getSubTypesOf(interFace);
    for (final Class<? extends T> type : subTypes) {
      logger.trace("Type: {}", type);
      try {
        final Method method = type.getMethod("valueOf", String.class);
        if (upperCase)
          return (T) method.invoke(null, enumValue.toUpperCase());
        return (T) method.invoke(null, enumValue);
      } catch (final NoSuchMethodException e) {
        // Just ignore
      } catch (final SecurityException e) {
        // Just ignore
      } catch (final IllegalAccessException e) {
        // Just ignore
      } catch (final IllegalArgumentException e) {
        // Just ignore
      } catch (final InvocationTargetException e) {
        // Just ignore
      }
      value = enumFromStringHelper(enumValue, type, packAge, upperCase);
      if (value != null)
        return value;
    }
    return value;
  }
}
