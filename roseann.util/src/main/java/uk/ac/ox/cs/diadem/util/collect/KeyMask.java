/**
 * 
 */
package uk.ac.ox.cs.diadem.util.collect;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
public interface KeyMask<Key> {
  boolean matchKeyByIdentity(Key key);

  boolean matchKeyByEquality(Key key);

  /**
   * 
   * @return
   */
  Key singleyKeyMatch();
}
