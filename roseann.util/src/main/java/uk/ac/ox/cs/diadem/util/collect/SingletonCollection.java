package uk.ac.ox.cs.diadem.util.collect;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;

public final class SingletonCollection<E> implements Collection<E> {

  private final E element;

  public static <X> SingletonCollection<X> create(final X element) {
    return new SingletonCollection<X>(element);
  }

  public SingletonCollection(final E element) {
    assert element != null;
    this.element = element;
  }

  @Override
  public boolean add(final E e) {
    throw new IllegalStateException("SingletonCollection is unmodifiable.");
  }

  @Override
  public boolean addAll(final Collection<? extends E> c) {
    throw new IllegalStateException("SingletonCollection is unmodifiable.");
  }

  @Override
  public void clear() {
    throw new IllegalStateException("SingletonCollection is unmodifiable.");
  }

  @Override
  public boolean contains(final Object o) {
    return o == element;
  }

  @Override
  public boolean containsAll(final Collection<?> c) {
    assert c != null;
    // c can have duplicates
    for (final Object e : c) {
      if (e != element)
        return false;
    }
    return true;
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public Iterator<E> iterator() {
    return SingletonIterator.create(element);
  }

  @Override
  public boolean remove(final Object o) {
    throw new IllegalStateException("SingletonCollection is unmodifiable.");
  }

  @Override
  public boolean removeAll(final Collection<?> c) {
    throw new IllegalStateException("SingletonCollection is unmodifiable.");
  }

  @Override
  public boolean retainAll(final Collection<?> c) {
    throw new IllegalStateException("SingletonCollection is unmodifiable.");
  }

  @Override
  public int size() {
    return 1;
  }

  @Override
  public Object[] toArray() {
    final Object[] result = { element };
    return result;
  }

  @SuppressWarnings("unchecked")
  @Override
  public <T> T[] toArray(final T[] a) {
    if (a.length == 1) {
      a[0] = (T) element;
      return a;
    }
    if (a.length > 1) {
      a[0] = (T) element;
      a[1] = null;
      return a;
    }
    final T[] result = (T[]) Array.newInstance(a.getClass().getComponentType(), 1);
    result[0] = (T) element;
    return result;
  }

  void write(final Writer writer) throws IOException {
    writer.append("[ ");
    writer.append(element.toString());
    writer.append(" ]");
  }

  @Override
  public String toString() {
    final StringWriter writer = new StringWriter();
    try {
      write(writer);
    } catch (final IOException e) {
      assert false : "Should be able to write into RAM at any time.";
      e.printStackTrace();
    }
    return writer.toString();
  }
}
