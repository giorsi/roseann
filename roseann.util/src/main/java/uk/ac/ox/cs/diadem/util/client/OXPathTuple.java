/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.util.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;

/**
 * Represents a raw output tuple from OXPath. E.g., (0, -1, "record", null), (1, 0, "attr1", "value1") represents the
 * XML fragment <record><attr1>value1</attr1></record>.
 */
public class OXPathTuple {
  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(OXPathTuple.class);
  @SuppressWarnings("unused")
  private static final Configuration config = ConfigurationFacility.getConfiguration();

  String id;

  String parentId;

  String name;

  String value;

  /**
   * Returns the id associated with this OXPathTuple.
   */
  public String getId() {
    return id;
  }

  /**
   * Sets the id of this OXPathTuple.
   */
  public void setId(final String id) {
    this.id = id;
  }

  /**
   * Returns the parentId associated with this OXPathTuple.
   */
  public String getParentId() {
    return parentId;
  }

  /**
   * Sets the parentId of this OXPathTuple.
   */
  public void setParentId(final String parentId) {
    this.parentId = parentId;
  }

  /**
   * Returns the name associated with this OXPathTuple.
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name of this OXPathTuple.
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   * Returns the value associated with this OXPathTuple.
   */
  public String getValue() {
    return value;
  }

  /**
   * Sets the value of this OXPathTuple.
   */
  public void setValue(final String value) {
    this.value = value;
  }

}
