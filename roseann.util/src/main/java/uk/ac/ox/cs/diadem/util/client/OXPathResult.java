/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.util.client;

/**
 * @author timfu
 */
public class OXPathResult extends OXPathRequest {

  public enum Status {
    RUNNING, TERMINATED, FAILURE, CANCELLED;
  }

  Status status;

  OXPathTuple[] results;

  String[] logLines;

  String startTime;

  String endTime;

  /**
   * Returns the status associated with this OXPathResult.
   */
  public Status getStatus() {
    return status;
  }

  /**
   * Sets the status of this OXPathResult.
   */
  public void setStatus(final Status status) {
    this.status = status;
  }

  /**
   * Returns the results associated with this OXPathResult.
   */
  public OXPathTuple[] getResults() {
    return results;
  }

  /**
   * Sets the results of this OXPathResult.
   */
  public void setResults(final OXPathTuple[] results) {
    this.results = results;
  }

  /**
   * Returns the logLines associated with this OXPathResult.
   */
  public String[] getLogLines() {
    return logLines;
  }

  /**
   * Sets the logLines of this OXPathResult.
   */
  public void setLogLines(final String[] logLines) {
    this.logLines = logLines;
  }

  /**
   * Returns the startTime associated with this OXPathResult.
   */
  public String getStartTime() {
    return startTime;
  }

  /**
   * Sets the startTime of this OXPathResult.
   */
  public void setStartTime(final String startTime) {
    this.startTime = startTime;
  }

  /**
   * Returns the endTime associated with this OXPathResult.
   */
  public String getEndTime() {
    return endTime;
  }

  /**
   * Sets the endTime of this OXPathResult.
   */
  public void setEndTime(final String endTime) {
    this.endTime = endTime;
  }

}
