package uk.ac.ox.cs.diadem.util.misc;

import static java.lang.Math.*;

import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.io.Writer;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.time.FastDateFormat;

public class DateUtil {

  public static Date create(final int year, final int month, final int day, final int hour, final int minute,
      final int second) {
    final Calendar calendar = new GregorianCalendar();
    calendar.setLenient(false);
    calendar.set(Calendar.MILLISECOND, 0);
    try {
      calendar.set(year, month, day, hour, minute, second);
      return calendar.getTime();
    } catch (final Exception e) {
      throw new IllegalArgumentException("Inconsistent Date '" + year + "-" + month + "-" + day + "-" + hour + "-"
          + minute + "-" + second + "'");
    }
  }

  public static Date parseTimestamp(final String str) throws ParseException {
    final StreamTokenizer tokenizer = new StreamTokenizer(new StringReader(str));
    tokenizer.ordinaryChar('-');
    final int nums[] = new int[6];

    try {
      for (int i = 0; i < 5; ++i) {
        if (tokenizer.nextToken() != StreamTokenizer.TT_NUMBER) {
          throw new ParseException("Malformed date '" + str + "' -- expected number.", 0);
        }
        nums[i] = (int) tokenizer.nval;
        if (tokenizer.nextToken() != '-') {
          throw new ParseException("Malformed date '" + str + "' -- expected '-'.", 0);
        }
      }
      if (tokenizer.nextToken() != StreamTokenizer.TT_NUMBER) {
        throw new ParseException("Malformed date '" + str + "' -- expected number.", 0);
      }
      nums[5] = (int) tokenizer.nval;
    } catch (final IOException e) {
      throw new ParseException("Malformed date '" + str + "'.", 0);
    }
    final Calendar calendar = new GregorianCalendar();
    calendar.set(Calendar.MILLISECOND, 0);
    calendar.setLenient(false);
    try {
      calendar.set(nums[0], nums[1], nums[2], nums[3], nums[4], nums[5]);
      return calendar.getTime();
    } catch (final Exception e) {
      throw new ParseException("Inconsistent Date '" + nums[0] + "-" + nums[1] + "-" + nums[2] + "-" + nums[3] + "-"
          + nums[4] + "-" + nums[5] + "'", 0);
    }
  }

  private static void printInteger(final int integer, final Writer writer, final int digits) throws IOException {
    assert integer >= 0;
    assert digits > 0;
    assert writer != null;
    int upper = (int) pow(10, digits - 1);
    if (integer < upper) {
      writer.append("0");
      upper /= 10;
    }
    writer.append(new Integer(integer).toString());
  }

  // TODO disgustingly awkward
  public static void printTimestamp(final Date date, final Writer writer) throws IOException {
    final Calendar calendar = new GregorianCalendar();
    calendar.setTime(date);
    writer.append(new Integer(calendar.get(Calendar.YEAR)).toString() + "-");
    printInteger(calendar.get(Calendar.MONTH), writer, 2);
    writer.append("-");
    printInteger(calendar.get(Calendar.DAY_OF_MONTH), writer, 2);
    writer.append("-");
    printInteger(calendar.get(Calendar.HOUR_OF_DAY), writer, 2);
    writer.append("-");
    printInteger(calendar.get(Calendar.MINUTE), writer, 2);
    writer.append("-");
    printInteger(calendar.get(Calendar.SECOND), writer, 2);
  }

  public static String getIdentifierTimeStamp() {
    final String s = FastDateFormat.getDateTimeInstance(FastDateFormat.SHORT, FastDateFormat.SHORT).format(new Date());
    return s.replaceAll("\\W", "");
  }
}
