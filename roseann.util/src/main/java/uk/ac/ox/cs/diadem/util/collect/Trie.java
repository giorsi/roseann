/**
 * 
 */
package uk.ac.ox.cs.diadem.util.collect;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 * @param <Key>
 * @param <Value>
 */
public interface Trie<Key, Value> extends Iterable<Value>, Cloneable {
  Value get(Key... keys);

  Value putOverwrite(Value value, Key... keys);

  Value putTentatively(Value value, Key... keys);

  void remove(Key... keys);

  boolean isEmpty();

  void clear();

  int size();

  @Override
  IterableIterator<Value> iterator();

  IterableIterator<Value> iteratorForMasks(KeyMask<Key>... keyMasks);

  IterableIterator<Value> iteratorForKeys(Key... keys);

  IterableIterator<Value> iteratorForPrefixMasks(KeyMask<Key>... keyMasks);

  IterableIterator<Value> iteratorForPrefixKeys(Key... keys);

  Trie<Key, Value> clone();
}