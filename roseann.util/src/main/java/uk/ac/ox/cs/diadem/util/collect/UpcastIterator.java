package uk.ac.ox.cs.diadem.util.collect;

import java.util.Iterator;

/**
 * Converts an Iterator<S> to Iterator<T> if S extends T.
 * 
 * @author Christian Schallhart <christian@schallhart.net>
 * 
 * @param <T>
 * @param <S>
 */
public class UpcastIterator<T, S extends T> implements Iterator<T> {

  private final Iterator<S> sourceIterator;

  private UpcastIterator(final Iterator<S> sourceIterator) {
    this.sourceIterator = sourceIterator;
  }

  public static <T, S extends T> Iterator<T> create(final Iterator<S> sourceIterator) {
    return new UpcastIterator<T, S>(sourceIterator);
    // TODO is casting safe?
  }

  @Override
  public boolean hasNext() {
    return sourceIterator.hasNext();
  }

  @Override
  public T next() {
    return sourceIterator.next();
  }

  @Override
  public void remove() {
    sourceIterator.remove();
  }

}
