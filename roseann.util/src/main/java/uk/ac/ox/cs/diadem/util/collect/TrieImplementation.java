/**
 * 
 */
package uk.ac.ox.cs.diadem.util.collect;

import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Stack;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 *         TODO We could improve the performance by avoid creating and recreating iterators all the time but instead
 *         reuse already created iterators which we re-initialize for each new use.
 */
public class TrieImplementation<Key, Value> implements Trie<Key, Value> {

  // //////////////////////////

  private static final class Node<Key, Value> {
    Value value = null;
    Map<Key, Node<Key, Value>> map = null;

    public Node() {
    }

    public Node(final Node<Key, Value> other, boolean identityBased) {
      this.value = other.value;
      if (other.map != null) {
        if (identityBased)
          this.map = new IdentityHashMap<Key, Node<Key, Value>>();
        else
          this.map = new HashMap<Key, Node<Key, Value>>();
        for (Entry<Key, Node<Key, Value>> childEntry : other.map.entrySet())
          this.map.put(childEntry.getKey(), new Node<Key, Value>(childEntry.getValue(), identityBased));
      }
    }

    public Value get(final int index, final Key... keys) {
      if (keys.length == index)
        return value;
      if (map == null)
        return null;
      final Node<Key, Value> nextTrie = map.get(keys[index]);
      if (nextTrie == null)
        return null;
      return nextTrie.get(index + 1, keys);
    }

    public boolean putOverwrite(boolean identityBased, final Value value, final int index, final Key... keys) {
      if (keys.length == index)
        if (this.value == null) {
          this.value = value;
          return true;
        } else {
          this.value = value;
          return false;
        }
      final Key nextKey = keys[index];
      Node<Key, Value> nextTrie;
      if (map == null) {
        if (identityBased)
          map = new IdentityHashMap<Key, Node<Key, Value>>();
        else
          map = new HashMap<Key, Node<Key, Value>>();
        map.put(nextKey, nextTrie = new Node<Key, Value>());
      } else {
        nextTrie = map.get(nextKey);
        if (nextTrie == null)
          map.put(nextKey, nextTrie = new Node<Key, Value>());
      }
      return nextTrie.putOverwrite(identityBased, value, index + 1, keys);
    }

    public Value putTentatively(boolean identityBased, final Value value, final int index, final Key... keys) {
      if (keys.length == index)
        if (this.value == null) {
          this.value = value;
          return null;
        } else
          return this.value;
      final Key nextKey = keys[index];
      Node<Key, Value> nextTrie;
      if (map == null) {
        if (identityBased)
          map = new IdentityHashMap<Key, Node<Key, Value>>();
        else
          map = new HashMap<Key, Node<Key, Value>>();
        map.put(nextKey, nextTrie = new Node<Key, Value>());
      } else {
        nextTrie = map.get(nextKey);
        if (nextTrie == null)
          map.put(nextKey, nextTrie = new Node<Key, Value>());
      }
      return nextTrie.putTentatively(identityBased, value, index + 1, keys);
    }

    public boolean remove(final int index, final Key... keys) {
      if (keys.length == index) {
        if (value == null)
          return false;
        value = null;
        return true;
      }
      final Key nextKey = keys[index];
      if (map == null)
        return false;
      final Node<Key, Value> nextTrie = map.get(nextKey);
      if (nextTrie == null)
        return false;
      if (nextTrie.remove(index + 1, keys)) {
        if (nextTrie.isEmpty())
          map.remove(nextKey);
        // semantically, we could comment in the next line;
        // but I think it's better to keep it there, since we
        // either have a dynamically used trie, then it might
        // re-allocated later, or we have a static once, then
        // this remove is rarely used anyhow.
        // if(map.isEmpty()) map=null;
        return true;
      }
      return false;
    }

    public boolean isEmpty() {
      return (value == null) && ((map == null) || map.isEmpty());
    }

    public void clear() {
      value = null;
      map = null;
    }
  }

  // //////////////////////////////////////////////////////////////

  private static final class TrieIterator<Key, Value> implements IterableIterator<Value> {

    // Invariant: The stack always points to node, to be taken by next.

    final private Stack<Iterator<Node<Key, Value>>> position = new Stack<Iterator<Node<Key, Value>>>();
    boolean hasNext;

    /**
     * @param root
     */
    public TrieIterator(final Node<Key, Value> root) {
      hasNext = !root.isEmpty();
      if (hasNext)
        position.push(SingletonIterator.create(root));
    }

    @Override
    public boolean hasNext() {
      return hasNext;
    }

    @Override
    public Value next() throws NoSuchElementException {
      if (!hasNext)
        throw new NoSuchElementException();
      assert position.peek().hasNext() : "Violated Invaricance";

      // fetch the next node
      final Node<Key, Value> nextNode = position.peek().next();
      final Value result = nextNode.value;

      if ((nextNode.map != null) && !nextNode.map.isEmpty())
        // If existing, push the children of nextNode onto the stack
        position.push(nextNode.map.values().iterator());
      else
        // otherwise, unwind the stack
        while (!position.peek().hasNext()) {
          position.pop();
          if (position.isEmpty()) {
            // if the stack becomes empty, then there are no
            // more elements
            hasNext = false;
            break;
          }
        }

      if (result != null)
        return result;

      // if there was no value in the current node,
      // we look for the next one (after updating the stack)
      assert hasNext : "Violated Invariance: hasNext was true on method entry, although no more elements exist.";
      return next();
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException("TrieIterator does not support remove.");
    }

    @Override
    public Iterator<Value> iterator() {
      return this;
    }
  }

  // //////////////////////////////////////

  private static final class MaskingTrieIterator<Key, Value> implements IterableIterator<Value> {

    final private Stack<Iterator<Node<Key, Value>>> position = new Stack<Iterator<Node<Key, Value>>>();
    boolean hasNext;
    private Value nextValue;
    final private KeyMask<Key>[] keyMasks;
    boolean identityBased;

    public MaskingTrieIterator(final boolean identityBased, final Node<Key, Value> root, KeyMask<Key>... keyMasks) {
      if (!root.isEmpty())
        position.push(SingletonIterator.create(root));
      this.keyMasks = keyMasks;
      this.identityBased = identityBased;
      advanceToNextMatch();
    }

    @Override
    public boolean hasNext() {
      return hasNext;
    }

    private void advanceToNextMatch() {
      hasNext = false;

      if (position.isEmpty())
        return;

      while (!hasNext) {
        assert position.size() > 0 : "Violated Invariance.";
        assert (position.size() - 1) <= keyMasks.length : "Violated Invariance.";

        // fetch the next node
        final Node<Key, Value> nextNode = position.peek().next();

        if ((position.size() - 1) < keyMasks.length) {
          // If we are at some inner node before reaching the mask length
          if ((nextNode.map != null) && !nextNode.map.isEmpty()) {
            // and if there are children to descend deeper in the trie
            final KeyMask<Key> maskToMatchNext = keyMasks[position.size() - 1];
            if (maskToMatchNext == null) {
              // If there is a null-mask (takes all), we push all children
              position.push(nextNode.map.values().iterator());
              continue;
            } else {
              final Key keyToMatchNext = maskToMatchNext.singleyKeyMatch();
              if (keyToMatchNext != null) {
                // If there is a single key to match, search for the right branch
                final Node<Key, Value> child = nextNode.map.get(keyToMatchNext);
                // and add it to the stack, if existing.
                if (child != null) {
                  position.push(SingletonIterator.create(child));
                  continue;
                }
              } else {
                // If there are multiple keys to match, search for the right branches
                // TODO check clean with identity base?
                Set<Node<Key, Value>> matchingChildren = new HashSet<Node<Key, Value>>();
                if (identityBased) {
                  for (Entry<Key, Node<Key, Value>> child : nextNode.map.entrySet())
                    if (maskToMatchNext.matchKeyByIdentity(child.getKey()))
                      matchingChildren.add(child.getValue());
                } else
                  for (Entry<Key, Node<Key, Value>> child : nextNode.map.entrySet())
                    if (maskToMatchNext.matchKeyByEquality(child.getKey()))
                      matchingChildren.add(child.getValue());
                // and add them to the stack, if existing.
                if (!matchingChildren.isEmpty()) {
                  position.push(matchingChildren.iterator());
                  continue;
                }
              }
            }
          }
        } else // If we are the right level
        if (nextNode.value != null) {
          // and there is an element, we take it.
          hasNext = true;
          nextValue = nextNode.value;
        }

        // We reach this point if we could not go on deeper,
        // i.e., we possibly have to unwind.

        while (!position.peek().hasNext()) {
          position.pop();
          if (position.isEmpty())
            // if the stack becomes empty, then there are no
            // more elements, and we are done
            return;
        }
      }
    }

    @Override
    public Value next() throws NoSuchElementException {
      if (!hasNext)
        throw new NoSuchElementException();
      final Value result = nextValue;
      advanceToNextMatch();
      return result;
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException("TrieIterator does not support remove.");
    }

    @Override
    public Iterator<Value> iterator() {
      return this;
    }
  }

  // ////////////////////////////////////////////////////////////////

  private static final class PrefixMaskingTrieIterator<Key, Value> implements IterableIterator<Value> {

    final private Stack<Iterator<Node<Key, Value>>> position = new Stack<Iterator<Node<Key, Value>>>();
    private TrieIterator<Key, Value> leafIterator = null;
    boolean hasNext;
    private Value nextValue;
    final private KeyMask<Key>[] keyMasks;
    boolean identityBased;

    public PrefixMaskingTrieIterator(final boolean identityBased, final Node<Key, Value> root, KeyMask<Key>... keyMasks) {
      if (!root.isEmpty())
        position.push(SingletonIterator.create(root));
      this.keyMasks = keyMasks;
      this.identityBased = identityBased;
      advanceToNextMatch();
    }

    @Override
    public boolean hasNext() {
      return hasNext;
    }

    private void advanceToNextMatch() {
      // If we are in a leaf iteration
      if (leafIterator != null)
        if (leafIterator.hasNext()) {
          hasNext = true;
          nextValue = leafIterator.next();
          return;
        } else
          leafIterator = null;

      assert leafIterator == null;

      hasNext = false;

      if (position.isEmpty())
        return;

      while (!hasNext) {
        assert position.size() > 0 : "Violated Invariance.";
        assert (position.size() - 1) <= keyMasks.length : "Violated Invariance.";

        // fetch the next node
        final Node<Key, Value> nextNode = position.peek().next();

        if ((position.size() - 1) < keyMasks.length) {
          // If we are at some inner node before reaching the mask length
          if ((nextNode.map != null) && !nextNode.map.isEmpty()) {
            // and if there are children to descend deeper in the trie
            final KeyMask<Key> maskToMatchNext = keyMasks[position.size() - 1];
            if (maskToMatchNext == null) {
              // If there is a null-mask (takes all), we push all children
              position.push(nextNode.map.values().iterator());
              continue;
            } else {
              final Key keyToMatchNext = maskToMatchNext.singleyKeyMatch();
              if (keyToMatchNext != null) {
                // If there is a single key to match, search for the right branch
                final Node<Key, Value> child = nextNode.map.get(keyToMatchNext);
                // and add it to the stack, if existing.
                if (child != null) {
                  position.push(SingletonIterator.create(child));
                  continue;
                }
              } else {
                // If there are multiple keys to match, search for the right branches
                // TODO check clean with identity base?
                Set<Node<Key, Value>> matchingChildren = new HashSet<Node<Key, Value>>();
                if (identityBased) {
                  for (Entry<Key, Node<Key, Value>> child : nextNode.map.entrySet())
                    if (maskToMatchNext.matchKeyByIdentity(child.getKey()))
                      matchingChildren.add(child.getValue());
                } else
                  for (Entry<Key, Node<Key, Value>> child : nextNode.map.entrySet())
                    if (maskToMatchNext.matchKeyByEquality(child.getKey()))
                      matchingChildren.add(child.getValue());
                // and add them to the stack, if existing.
                if (!matchingChildren.isEmpty()) {
                  position.push(matchingChildren.iterator());
                  continue;
                }
              }
            }
          }
        } else {
          // If we are the right level
          assert leafIterator == null : "Violated Invariance";
          leafIterator = new TrieIterator<Key, Value>(nextNode);
          if (leafIterator.hasNext()) {
            // if the subtrie contains nodes
            hasNext = true;
            nextValue = leafIterator.next();
          } else
            // if the subtrie is empty
            leafIterator = null;
        }

        // We reach this point if we could not go on deeper,
        // i.e., we possibly have to unwind.

        while (!position.peek().hasNext()) {
          position.pop();
          if (position.isEmpty())
            // if the stack becomes empty, then there are no
            // more elements, and we are done
            return;
        }
      }
    }

    @Override
    public Value next() throws NoSuchElementException {
      if (!hasNext)
        throw new NoSuchElementException();
      final Value result = nextValue;
      advanceToNextMatch();
      return result;
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException("TrieIterator does not support remove.");
    }

    @Override
    public Iterator<Value> iterator() {
      return this;
    }
  }

  // ////////////////////////////////////////////////////////////////

  private final Node<Key, Value> root;
  private int size;
  private final boolean identityBased;

  public TrieImplementation() {
    this.root = new Node<Key, Value>();
    this.size = 0;
    this.identityBased = false;
  }

  public TrieImplementation(final boolean identityBased) {
    this.root = new Node<Key, Value>();
    this.size = 0;
    this.identityBased = identityBased;
  }

  protected TrieImplementation(TrieImplementation<Key, Value> other) {
    this.root = new Node<Key, Value>(other.root, other.identityBased);
    this.size = other.size;
    this.identityBased = other.identityBased;
  }

  @Override
  public TrieImplementation<Key, Value> clone() {
    return new TrieImplementation<Key, Value>(this);
  }

  @Override
  public Value get(Key... keys) {
    return root.get(0, keys);
  }

  @Override
  public Value putOverwrite(Value value, Key... keys) {
    assert value != null : "Invalid Argument.";
    if (root.putOverwrite(identityBased, value, 0, keys)) 
      ++size;
    return value;
  }

  @Override
  public Value putTentatively(Value value, Key... keys) {
    assert value != null : "Invalid Argument.";
    final Value oldValue = root.putTentatively(identityBased, value, 0, keys);
    if (oldValue == null) {
      ++size;
      return value;
    }
    return oldValue;
  }

  @Override
  public void remove(Key... keys) {
    if (root.remove(0, keys))
      --size;
    assert size >= 0 : "Violated Invariance: negative size";
  }

  @Override
  public boolean isEmpty() {
    return root.isEmpty();
  }

  @Override
  public void clear() {
    size = 0;
    root.clear();
  }

  @Override
  public int size() {
    return size;
  }

  @Override
  public IterableIterator<Value> iterator() {
    return new TrieIterator<Key, Value>(root);
  }

  @Override
  public IterableIterator<Value> iteratorForMasks(KeyMask<Key>... keyMasks) {
    return new MaskingTrieIterator<Key, Value>(identityBased, root, keyMasks);
  }

  // TODO to be replaced if the Trie interface remains stable
  @Override
  public IterableIterator<Value> iteratorForKeys(Key... keys) {
    @SuppressWarnings("unchecked")
    final KeyMask<Key> keyMasks[] = new KeyMask[keys.length];
    for (int i = 0; i < keys.length; ++i)
      if (keys[i] != null)
        keyMasks[i] = new SingletonKeyMask<Key>(keys[i]);
      else
        keyMasks[i] = null;
    return new MaskingTrieIterator<Key, Value>(identityBased, root, keyMasks);
  }

  @Override
  public IterableIterator<Value> iteratorForPrefixMasks(KeyMask<Key>... keyMasks) {
    return new PrefixMaskingTrieIterator<Key, Value>(identityBased, root, keyMasks);
  }

  // TODO to be replaced if the Trie interface remains stable
  @Override
  public IterableIterator<Value> iteratorForPrefixKeys(Key... keys) {
    @SuppressWarnings("unchecked")
    final KeyMask<Key> keyMasks[] = new KeyMask[keys.length];
    for (int i = 0; i < keys.length; ++i)
      if (keys[i] != null)
        keyMasks[i] = new SingletonKeyMask<Key>(keys[i]);
      else
        keyMasks[i] = null;
    return new PrefixMaskingTrieIterator<Key, Value>(identityBased, root, keyMasks);
  }

}
