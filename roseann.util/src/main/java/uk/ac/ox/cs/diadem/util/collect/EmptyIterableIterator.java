
package uk.ac.ox.cs.diadem.util.collect;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class EmptyIterableIterator<E> implements IterableIterator<E> {
	@Override
	public boolean hasNext() {
		return false;
	}
	@Override
	public E next() {
		throw new NoSuchElementException("Empty iterator has no elements.");
	}

	@Override
	public void remove() {
		throw new IllegalStateException("Empty iterator has nothing to remove.");
	}

	@Override
	public Iterator<E> iterator() {
		return this;
	} 
}
