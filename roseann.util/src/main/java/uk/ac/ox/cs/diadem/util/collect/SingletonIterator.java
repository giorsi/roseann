package uk.ac.ox.cs.diadem.util.collect;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SingletonIterator<T> implements Iterator<T> {

	private T element;
	private boolean atFirst=true;

	public SingletonIterator(T element) {
		this.element=element;
	}
	
	@Override
	public boolean hasNext() {		
		return atFirst;
	}

	@Override
	public T next() {
		if(!atFirst) throw new NoSuchElementException();
		atFirst=false;
		return element;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	static public <T> SingletonIterator<T> create(T element) {
		return new SingletonIterator<T>(element);
	}
	
}
