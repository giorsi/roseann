package uk.ac.ox.cs.diadem.util.collect;

import java.util.ArrayList;

public class CollectionHelpers {

  private CollectionHelpers() {
  }

  /**
   * Reverses the order in an array list -- returning the same, now changed, list.
   * @param list
   * @return
   */
  public static <X> ArrayList<X> reverse(final ArrayList<X> list) {
    final int size = list.size();
    for (int i = 0, j = size - 1; i < j; ++i, --j) {
      final X tmp = list.get(i);
      list.set(i, list.get(j));
      list.set(j, tmp);
    }
    return list;
  }

}
