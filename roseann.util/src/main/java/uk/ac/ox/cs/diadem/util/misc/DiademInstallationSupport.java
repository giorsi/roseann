/**
 * Header
 */
package uk.ac.ox.cs.diadem.util.misc;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.jar.JarFile;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.DiademRuntimeException;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;

/**
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public class DiademInstallationSupport {

  static final Logger logger = LoggerFactory.getLogger(DiademInstallationSupport.class);

  private DiademInstallationSupport() {
    // prevent instantiation
  }

  /**
   * Returns the DIADEM home folder where to install third-parties tools
   * @return the DIADEM home folder where to install third-parties tools
   */
  public static String getDiademHome() {
    return FilenameUtils.separatorsToSystem(System.getProperty("user.home") + File.separator
        + ConfigurationFacility.getConfiguration().getString("installation-path.home"));
  }

  /**
   * Given a {@link Class} object, it extracts the containing jar file into the Diadem home
   * @param clazz a class object from which to retrieve the jar to install
   */
  public static void installFromClass(final Class<?> clazz) {
    DiademInstallationSupport.extractPackageOfMarker(clazz, DiademInstallationSupport.getDiademHome());
  }

  private static String JAR_BANG_SEPARATOR = "!";
  private static String JAR_URI_PREFIX = "jar:file:";
  private static String FILE_URI_PREFIX = "file:";

  private static void extractPackageOfMarker(final Class<?> clazz, final String destDir) {
    logger.info("Trying to locate JAR for {}", clazz);
    final URL location = clazz.getResource("");
    logger.info("... via get resource: {}", location);

    // If it's a JAR
    final String url = location.toString();
    if (url.startsWith(JAR_URI_PREFIX) && url.contains(JAR_BANG_SEPARATOR)) {
      final int bang = url.indexOf(JAR_BANG_SEPARATOR);
      final String jarFile = url.substring(JAR_URI_PREFIX.length(), bang);
      String packageDir = url.substring(bang + 1, url.length());
      if (packageDir.startsWith("/")) {
        packageDir = packageDir.substring(1, packageDir.length());
      }
      if (packageDir.endsWith("/")) {
        packageDir = packageDir.substring(0, packageDir.length() - 1);
      }
      final String altPackageDir = clazz.getPackage().getName().replace(".", "/");
      logger.info("... extracting package {} from jar {}", packageDir + " (" + altPackageDir + ")", jarFile);
      try {
        JarUtils.copyResourcesToDirectory(new JarFile(jarFile, false), packageDir, destDir, true);
      } catch (final IOException e) {
        throw new DiademRuntimeException("Can't create environment from this location " + location
            + ", where marker class " + clazz + " is located.", e, logger);
      }
    } else if (url.startsWith(FILE_URI_PREFIX)) {
      final File f = new File(EscapingUtils.urlToUri(location));
      final String finalDest = destDir + "/" + clazz.getPackage().getName().replace(".", "/");
      logger.info("... copying {} into {}", f, finalDest);
      if (f.exists() && f.isDirectory()) {
        try {
          final File destFile = new File(finalDest);
          destFile.mkdirs();
          FileUtils.copyDirectory(f, destFile);
        } catch (final IOException e) {
          throw new DiademRuntimeException("Can't create environment from this location " + location
              + ", where marker class " + clazz + " is located.", e, logger);
        }
      }
    } else
      throw new DiademRuntimeException("Can't create environment from this location " + location
          + ", where marker class " + clazz + " is located.", logger);

    // // final jar file to unjar
    // String jar = null;
    // // try to get the path of the jar containing the clazz
    // final String path = clazz.getProtectionDomain().getCodeSource().getLocation().getPath();
    // // if not failed
    // if ((path != null) && (path.length() > 0)) {
    // jar = FilenameUtils.separatorsToSystem(path);
    // } else {// else try other way to get the path
    // final URL resource = clazz.getResource("platform.ini");
    // String url_resurce = resource.toString();
    // // if is a jar:file protocol
    // if (url_resurce.startsWith("jar:file:")) {
    // url_resurce = url_resurce.replaceAll(" ", "%20").replaceFirst("jar:file:", "");
    // } else // else is is a bundleresource procotol
    // if (url_resurce.startsWith("bundleresource")) {
    // try {
    // // try to resolve using Eclipse classes if available
    // final Class<?> p = Class.forName("org.eclipse.core.runtime.Platform");
    // final Method m = p.getMethod("resolve", URL.class);
    // // the resolved url
    // final URL resourceEntry = (URL) m.invoke(null, resource);
    // url_resurce = resourceEntry.toString().replaceAll(" ", "%20");
    // url_resurce = url_resurce.replaceFirst("jar:file:", "");
    // } catch (final Exception e) {
    // // do nothing as we are not embedding in eclipse
    // }
    // }
    // if (url_resurce == null) {
    // logger.error("Cannot locate jar for class {} ", clazz.getName());
    // throw new DiademRuntimeException("Cannot locate jar for class" + clazz.getName(), logger);
    // }
    // final String pathWithSeparators = FilenameUtils.separatorsToSystem(url_resurce);
    // // cut last part relative to the file retrieved
    // jar = pathWithSeparators.substring(0, url_resurce.indexOf("!"));
    // }
    // // in case there are spaces in the path
    // jar = jar.replaceAll("%20", " ");
    // // only in Windows we need to treat this case
    // if (SystemUtils.IS_OS_WINDOWS)
    // if (jar.startsWith("/") || jar.startsWith("\\")) {
    // jar = jar.substring(1);
    // }
    // logger.info("Identified <{}> as Jar for {}", jar, clazz);
    //
    // return jar;

  }

  // private static void extractJarTo(final String jar, final String destDir) {
  // InputStream in;
  // try {
  // in = new FileInputStream(jar);
  // JarUtils.unjar(in, new File(destDir));
  // } catch (final FileNotFoundException e) {
  // logger.error("Cannot extract jar {} ", jar);
  // throw new DiademRuntimeException("Cannot extract Jar file", e, logger);
  // } catch (final IOException e) {
  // logger.error("Cannot extract jar {} ", jar);
  // throw new DiademRuntimeException("Cannot extract Jar file", e, logger);
  // }
  // }

}
