/**
 * 
 */
package uk.ac.ox.cs.diadem.util.collect;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
public class SingletonKeyMask<Key> implements KeyMask<Key> {
  private final Key key;

  public SingletonKeyMask(final Key key) {
    this.key = key;
  }

  @Override
  public boolean matchKeyByIdentity(Key key) {
    return this.key == key;
  }

  @Override
  public boolean matchKeyByEquality(Key key) {
    return this.key.equals(key);
  }

  @Override
  public Key singleyKeyMatch() {
    return key;
  }

  @Override
  public String toString() {
    return "<VALUEMATCH " + key + " >";
  }

}
