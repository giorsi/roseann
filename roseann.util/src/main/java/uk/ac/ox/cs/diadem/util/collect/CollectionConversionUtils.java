/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.util.collect;

import java.util.Collection;
import java.util.Set;
import java.util.SortedSet;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;

import com.google.common.collect.Sets;

/**
 * Converts various representations of collections and arrays.
 */
public class CollectionConversionUtils {
  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(CollectionConversionUtils.class);
  @SuppressWarnings("unused")
  private static final Configuration config = ConfigurationFacility.getConfiguration();

  /**
   * Returns a new set with all the elements of the given set and the additional T elements.
   */
  public static <T> Set<T> fromSetAndArray(final Collection<T> s, final T... rest) {
    final Set<T> snew = Sets.newHashSet(rest);
    snew.addAll(s);
    return snew;
  }

  /**
   * Returns a new sorted set with all the elements of the given set and the additional T elements.
   */
  public static <T extends Comparable<T>> SortedSet<T> fromSetAndArray(final Collection<T> s, final T... rest) {
    final SortedSet<T> snew = Sets.newTreeSet(s);
    CollectionUtils.addAll(snew, rest);
    return snew;
  }

}
