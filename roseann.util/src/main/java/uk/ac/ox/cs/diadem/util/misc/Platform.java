package uk.ac.ox.cs.diadem.util.misc;


public class Platform{
	 
	public enum OSType {Linux, Unix, OSX, Windows, Unknown};
	
	public static void main(String[] args)
	{
		if(isWindows()){
			System.out.println("This is Windows");
		}else if(isMac()){
			System.out.println("This is Mac");
		}else if(isLinux()){
			System.out.println("This is Linux");
		}else if(isUnix()){
			System.out.println("This is Unix");
		}else{
			System.out.println("Your OS is not supported!!");
		}
	}
 
	public static boolean isWindows(){
		String os = System.getProperty("os.name").toLowerCase();
	    return (os.indexOf( "win" ) >= 0); 
	}
 
	public static boolean isMac(){
		String os = System.getProperty("os.name").toLowerCase();
	    return (os.indexOf( "mac" ) >= 0); 
	}
 
	public static boolean isLinux(){
		String os = System.getProperty("os.name").toLowerCase();
	    return (os.indexOf( "nix") >=0 || os.indexOf( "nux") >=0);
	}
	
	public static boolean isUnix(){
		String os = System.getProperty("os.name").toLowerCase();
	    return (os.indexOf( "unix") >=0);
	}

	public static OSType osType() {
		if(isWindows()) return OSType.Windows;
		if(isMac()) return OSType.OSX;
		if(isLinux()) return OSType.Linux;
		if(isUnix()) return OSType.Unix;
		return OSType.Unknown;
	}
	
}
