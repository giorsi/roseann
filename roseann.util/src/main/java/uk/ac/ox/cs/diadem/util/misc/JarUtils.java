/*
 * JBoss, the OpenSource J2EE webOS
 *
 * Distributable under LGPL license.
 * See terms of license at gnu.org.
 */
package uk.ac.ox.cs.diadem.util.misc;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * A utility class for dealing with Jar files.
 * @author Scott.Stark@jboss.org
 * @version $Revision: 1958 $
 */
public final class JarUtils {

  /**
   * Hide the constructor
   */
  private JarUtils() {

  }

  /**
   * Returns the jar file used to load class clazz, or defaultJar if clazz was not loaded from a jar. (from Android,
   * https://code.google.com/p/doclava/source/browse/trunk/src/com/google/doclava/JarUtils.java?r=326)
   */
  public static JarFile jarForClass(final Class<?> clazz, final JarFile defaultJar) {
    final String path = "/" + clazz.getName().replace('.', '/') + ".class";
    final URL jarUrl = clazz.getResource(path);
    if (jarUrl == null)
      return defaultJar;

    final String url = jarUrl.toString();
    final int bang = url.indexOf("!");
    final String JAR_URI_PREFIX = "jar:file:";
    if (url.startsWith(JAR_URI_PREFIX) && (bang != -1)) {
      try {
        return new JarFile(url.substring(JAR_URI_PREFIX.length(), bang));
      } catch (final IOException e) {
        throw new IllegalStateException("Error loading jar file.", e);
      }
    } else
      return defaultJar;
  }

  /**
   * Copies a directory from a jar file to an external directory. (from Android,
   * https://code.google.com/p/doclava/source/browse/trunk/src/com/google/doclava/JarUtils.java?r=326) jarDir must not
   * start or end with a /.
   */
  public static void copyResourcesToDirectory(final JarFile fromJar, final String jarDir, final String destDir,
      final boolean preserveDirectoryStructure) throws IOException {
    for (final Enumeration<JarEntry> entries = fromJar.entries(); entries.hasMoreElements();) {
      final JarEntry entry = entries.nextElement();
      if (entry.getName().startsWith(jarDir + "/") && !entry.isDirectory()) {
        final String destFile = preserveDirectoryStructure ? entry.getName() : entry.getName().substring(
            jarDir.length() + 1);
        final File dest = new File(destDir + "/" + destFile);
        final File parent = dest.getParentFile();
        if (parent != null) {
          parent.mkdirs();
        }

        final FileOutputStream out = new FileOutputStream(dest);
        final InputStream in = fromJar.getInputStream(entry);

        try {
          final byte[] buffer = new byte[8 * 1024];

          int s = 0;
          while ((s = in.read(buffer)) > 0) {
            out.write(buffer, 0, s);
          }
        } catch (final IOException e) {
          final IOException ioException = new IOException("Could not copy asset from jar file");
          ioException.initCause(e);
          throw ioException;
        } finally {
          try {
            in.close();
          } catch (final IOException ignored) {
          }
          try {
            out.close();
          } catch (final IOException ignored) {
          }
        }
      }
    }

  }

  /**
   * <P>
   * This function will create a Jar archive containing the src file/directory. The archive will be written to the
   * specified OutputStream.
   * </P>
   * <P>
   * This is a shortcut for<br>
   * <code>jar(out, new File[] { src }, null, null, null);</code>
   * </P>
   * @param out The output stream to which the generated Jar archive is written.
   * @param src The file or directory to jar up. Directories will be processed recursively.
   */
  static void jar(final OutputStream out, final File src) throws IOException {

    jar(out, new File[] { src }, null, null, null);
  }

  /**
   * <P>
   * This function will create a Jar archive containing the src file/directory. The archive will be written to the
   * specified OutputStream.
   * </P>
   * <P>
   * This is a shortcut for<br>
   * <code>jar(out, src, null, null, null);</code>
   * </P>
   * @param out The output stream to which the generated Jar archive is written.
   * @param src The file or directory to jar up. Directories will be processed recursively.
   */
  static void jar(final OutputStream out, final File[] src) throws IOException {

    jar(out, src, null, null, null);
  }

  /**
   * <P>
   * This function will create a Jar archive containing the src file/directory. The archive will be written to the
   * specified OutputStream. Directories are processed recursively, applying the specified filter if it exists.
   * <P>
   * This is a shortcut for<br>
   * <code>jar(out, src, filter, null, null);</code>
   * </P>
   * @param out The output stream to which the generated Jar archive is written.
   * @param src The file or directory to jar up. Directories will be processed recursively.
   * @param filter The filter to use while processing directories. Only those files matching will be included in the jar
   *          archive. If null, then all files are included.
   */
  static void jar(final OutputStream out, final File[] src, final FileFilter filter) throws IOException {

    jar(out, src, filter, null, null);
  }

  /**
   * <P>
   * This function will create a Jar archive containing the src file/directory. The archive will be written to the
   * specified OutputStream. Directories are processed recursively, applying the specified filter if it exists.
   * @param out The output stream to which the generated Jar archive is written.
   * @param src The file or directory to jar up. Directories will be processed recursively.
   * @param filter The filter to use while processing directories. Only those files matching will be included in the jar
   *          archive. If null, then all files are included.
   * @param prefix The name of an arbitrary directory that will precede all entries in the jar archive. If null, then no
   *          prefix will be used.
   * @param man The manifest to use for the Jar archive. If null, then no manifest will be included.
   */
  static void jar(final OutputStream out, final File[] src, final FileFilter filter, String prefix, final Manifest man)
      throws IOException {

    for (int i = 0; i < src.length; i++)
      if (!src[i].exists())
        throw new FileNotFoundException(src.toString());
    JarOutputStream jout;
    if (man == null) {
      jout = new JarOutputStream(out);
    } else {
      jout = new JarOutputStream(out, man);
    }
    if ((prefix != null) && (prefix.length() > 0) && !prefix.equals("/")) {
      // strip leading '/'
      if (prefix.charAt(0) == '/') {
        prefix = prefix.substring(1);
      }
      // ensure trailing '/'
      if (prefix.charAt(prefix.length() - 1) != '/') {
        prefix = prefix + "/";
      }
    } else {
      prefix = "";
    }
    final JarInfo info = new JarInfo(jout, filter);
    for (final File element : src) {
      jar(element, prefix, info);
    }
    jout.close();
  }

  /**
   * This simple convenience class is used by the jar method to reduce the number of arguments needed. It holds all
   * non-changing attributes needed for the recursive jar method.
   */
  private static class JarInfo {

    public JarOutputStream out;
    public FileFilter filter;
    public byte[] buffer;

    public JarInfo(final JarOutputStream out, final FileFilter filter) {

      this.out = out;
      this.filter = filter;
      buffer = new byte[1024];
    }
  }

  /**
   * This recursive method writes all matching files and directories to the jar output stream.
   */
  private static void jar(final File src, String prefix, final JarInfo info) throws IOException {

    final JarOutputStream jout = info.out;
    if (src.isDirectory()) {
      // create / init the zip entry
      prefix = prefix + src.getName() + "/";
      final ZipEntry entry = new ZipEntry(prefix);
      entry.setTime(src.lastModified());
      entry.setMethod(ZipOutputStream.STORED);
      entry.setSize(0L);
      entry.setCrc(0L);
      jout.putNextEntry(entry);
      jout.closeEntry();
      // process the sub-directories
      final File[] files = src.listFiles(info.filter);
      for (final File file : files) {
        jar(file, prefix, info);
      }
    } else if (src.isFile()) {
      // get the required info objects
      final byte[] buffer = info.buffer;
      // create / init the zip entry
      final ZipEntry entry = new ZipEntry(prefix + src.getName());
      entry.setTime(src.lastModified());
      jout.putNextEntry(entry);
      // dump the file
      final FileInputStream in = new FileInputStream(src);
      int len;
      while ((len = in.read(buffer, 0, buffer.length)) != -1) {
        jout.write(buffer, 0, len);
      }
      in.close();
      jout.closeEntry();
    }
  }

  /**
   * Dump the contents of a JarArchive to the dpecified destination.
   */
  public static void unjar(final InputStream in, final File dest) throws IOException {

    if (!dest.exists()) {
      dest.mkdirs();
    }
    if (!dest.isDirectory())
      throw new IOException("Destination must be a directory.");
    final JarInputStream jin = new JarInputStream(in);
    final byte[] buffer = new byte[1024];
    ZipEntry entry = jin.getNextEntry();
    while (entry != null) {
      String fileName = entry.getName();
      if (fileName.charAt(fileName.length() - 1) == '/') {
        fileName = fileName.substring(0, fileName.length() - 1);
      }
      if (fileName.charAt(0) == '/') {
        fileName = fileName.substring(1);
      }
      if (File.separatorChar != '/') {
        fileName = fileName.replace('/', File.separatorChar);
      }
      final File file = new File(dest, fileName);
      if (entry.isDirectory()) {
        // make sure the directory exists
        file.mkdirs();
        jin.closeEntry();
      } else {
        // make sure the directory exists
        final File parent = file.getParentFile();
        if ((parent != null) && !parent.exists()) {
          parent.mkdirs();
        }
        // dump the file
        final OutputStream out = new FileOutputStream(file);
        int len = 0;
        while ((len = jin.read(buffer, 0, buffer.length)) != -1) {
          out.write(buffer, 0, len);
        }
        out.flush();
        out.close();
        jin.closeEntry();
        file.setLastModified(entry.getTime());
      }
      entry = jin.getNextEntry();
    }
    /*
     * Explicity write out the META-INF/MANIFEST.MF so that any headers such as the Class-Path are see for the
     * unpackaged jar
     */
    final Manifest mf = jin.getManifest();
    if (mf != null) {
      final File file = new File(dest, "META-INF/MANIFEST.MF");
      final File parent = file.getParentFile();
      if (parent.exists() == false) {
        parent.mkdirs();
      }
      final OutputStream out = new FileOutputStream(file);
      mf.write(out);
      out.flush();
      out.close();
    }
    jin.close();
  }

  /**
   * Given a URL check if its a jar url(jar:<url>!/archive) and if it is, extract the archive entry into the given dest
   * directory and return a file URL to its location. If jarURL is not a jar url then it is simply returned as the URL
   * for the jar.
   * @param jarURL the URL to validate and extract the referenced entry if its a jar protocol URL
   * @param dest the directory into which the nested jar will be extracted.
   * @return the file: URL for the jar referenced by the jarURL parameter.
   */
  static URL extractNestedJar(final URL jarURL, final File dest) throws IOException {

    // This may not be a jar URL so validate the protocol
    if (jarURL.getProtocol().equals("jar") == false)
      return jarURL;
    final String destPath = dest.getAbsolutePath();
    final URLConnection urlConn = jarURL.openConnection();
    final JarURLConnection jarConn = (JarURLConnection) urlConn;
    // Extract the archive to dest/jarName-contents/archive
    String parentArchiveName = jarConn.getJarFile().getName();
    // Find the longest common prefix between destPath and parentArchiveName
    final int length = Math.min(destPath.length(), parentArchiveName.length());
    int n = 0;
    while (n < length) {
      final char a = destPath.charAt(n);
      final char b = parentArchiveName.charAt(n);
      if (a != b) {
        break;
      }
      n++;
    }
    // Remove any common prefix from parentArchiveName
    parentArchiveName = parentArchiveName.substring(n);
    final File archiveDir = new File(dest, parentArchiveName + "-contents");
    if ((archiveDir.exists() == false) && (archiveDir.mkdirs() == false))
      throw new IOException("Failed to create contents directory for archive, path=" + archiveDir.getAbsolutePath());
    final String archiveName = jarConn.getEntryName();
    final File archiveFile = new File(archiveDir, archiveName);
    final File archiveParentDir = archiveFile.getParentFile();
    if ((archiveParentDir.exists() == false) && (archiveParentDir.mkdirs() == false))
      throw new IOException("Failed to create parent directory for archive, path=" + archiveParentDir.getAbsolutePath());
    final InputStream archiveIS = jarConn.getInputStream();
    final FileOutputStream fos = new FileOutputStream(archiveFile);
    final BufferedOutputStream bos = new BufferedOutputStream(fos);
    final byte[] buffer = new byte[4096];
    int read;
    while ((read = archiveIS.read(buffer)) > 0) {
      bos.write(buffer, 0, read);
    }
    archiveIS.close();
    bos.close();
    // Return the file url to the extracted jar
    // return archiveFile.toURL();
    return archiveFile.toURI().toURL();
  }

  // /**
  // * A simple jar-like tool used for testing. It's actually faster than jar,
  // * though doesn't sipport as many options.
  // */
  // public static void main(final String[] args) throws Exception {
  //
  // if (args.length == 0) {
  // System.out.println("usage: <x or c> <jar-archive> <files...>");
  // System.exit(0);
  // }
  // if (args[0].equals("x")) {
  // final BufferedInputStream in = new BufferedInputStream(new
  // FileInputStream(args[1]));
  // final File dest = new File(args[2]);
  // unjar(in, dest);
  // } else if (args[0].equals("c")) {
  // final BufferedOutputStream out = new BufferedOutputStream(new
  // FileOutputStream(args[1]));
  // final File[] src = new File[args.length - 2];
  // for (int i = 0; i < src.length; i++) {
  // src[i] = new File(args[2 + i]);
  // }
  // jar(out, src);
  // } else {
  // System.out.println("Need x or c as first argument");
  // }
  // }
}