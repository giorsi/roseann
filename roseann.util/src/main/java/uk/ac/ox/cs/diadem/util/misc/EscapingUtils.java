/*
 * Copyright (c)2011, DIADEM Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the DIADEM team nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DIADEM Team BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package uk.ac.ox.cs.diadem.util.misc;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.DiademRuntimeException;
import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;

import com.google.common.base.CharMatcher;

/**
 * A collection of methods for escaping especially useful for dlv
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public class EscapingUtils {
  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(EscapingUtils.class);
  @SuppressWarnings("unused")
  private static final Configuration config = ConfigurationFacility.getConfiguration();

  /**
   * Produces a string with the indentation level, according to the configuration "general.indent"
   * @param level the level of indentation desired
   * @return a string with the indentation level, according to the configuration "general.indent"
   */
  public static String indent(final int level) {
    final Configuration config = ConfigurationFacility.getConfiguration();
    final StringBuilder sb = new StringBuilder();
    for (int i = 0; i < (level * config.getInt("general.indent")); i++) {
      sb.append(" ");
    }
    return sb.toString();
  }

  /**
   * Turns a {@link URL} into a {@link URI}, turning any exceptions into runtime exceptions. NEVER USE
   * {@link URL#toURI()} as that method fails if the URL contains unencoded, unsafe characters such as | which are
   * allowed in java's URL implementation but not in URI.
   */
  public static URI urlToUri(final URL url) {
    try {
      return new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(),
          url.getRef());
    } catch (final URISyntaxException e) {
      throw new DiademRuntimeException("URL " + url + " can not be parsed as URI!", e, logger);
    }
  }

  /**
   * Turns a {@link URI} into a {@link URL}.
   */
  public static URL uriToUrl(final URI uri) {
    try {
      return uri.toURL();
    } catch (final MalformedURLException e) {
      throw new DiademRuntimeException("URI " + uri + " can not be parsed as URL!", e, logger);
    }
  }

  /**
   * Turns a {@link String} into a {@link URI}.
   */
  public static URI stringToUri(final String urlString) {
    try {
      final URL url = stringToUrl(urlString);
      return new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(),
          url.getRef());
    } catch (final URISyntaxException e) {
      throw new DiademRuntimeException("String " + urlString + " can not be parsed as URI!", e, logger);
    }
  }

  /**
   * Compares two URLs ignoring # or / at the end.
   */
  public static boolean sameURL(final URL url, final URL ourl) {
    final String u = stripEmptyFragment(url.toString());
    final String v = stripEmptyFragment(ourl.toString());
    return u.equals(v);
  }

  private static String stripEmptyFragment(final String string) {
    while (string.endsWith("#") || string.endsWith("/")) {
      return string.substring(0, string.length() - 1);
    }
    return string;
  }

  /**
   * Turns a {@link String} into a {@link URL}.
   */
  public static URL stringToUrl(final String urlString) {
    try {
      return new URL(urlString);
    } catch (final MalformedURLException e) {
      throw new DiademRuntimeException("Can not parse " + urlString + " as URL.", e, logger);
    }
  }

  /**
   * Quotes a string if not a representation of an integer number >= 0 (dlv does not support it).
   * @param value the string to quote
   * @return a string if not a representation of an integer number >= 0 (dlv does not support it).
   */
  public static String quoteIfNotNumberGreaterThenZero(final String value) {

    try {
      if (Integer.parseInt(value) < 0) {
        return "\"" + value + "\"";
      }
      return value;
    } catch (final NumberFormatException e) {
      return "\"" + EscapingUtils.escapeStringContentForDLV(value) + "\"";
    }
  }

  public static boolean isGarbageText(final String value) {
    return value.matches("[\\s]+") || EscapingUtils.isNonBreakingSpace(value);
    // return CharMatcher.WHITESPACE.matchesAllOf(value);
  }

  /**
   * Escapes the given string for inclusion in DLV. If the string is a number or a DLV constant it will be returned
   * unchanged, otherwise it will be quoted and all quotes in the string will be double quoted.
   */
  public static String escapeForDLV(final String value) {
    assert value != null;
    // Numbers are ok
    if (EscapingUtils.isConstantNumber(value)) {
      return value;
    }
    // If it is a lower case letter followed by any word character its a constant and ok.
    if (value.matches("[a-z]\\w*")) {
      return value;
    }
    // If they are already quoted, no need to quote again.
    if (EscapingUtils.isQuotedString(value)) {
      return value;
    }
    return EscapingUtils.quoteAndEscapeString(value);
  }

  /**
   * Adapts a constant to be represented in DLV
   * @param c the constant to escape
   * @return a constant in DLV representation
   */
  public static String escapeForDLVConstants(String c) {

    if (c == null) {
      throw new RuntimeException("Null values cannot be escaped");
    }
    c = c.replaceAll("\\W", "_");
    // }
    if (c.startsWith("_")) {
      c = "tag" + c;
    }
    // if is not a integer number, cannot start with a number
    if (!EscapingUtils.isConstantNumber(c) && EscapingUtils.isConstantNumber(c.substring(0, 1))) {
      c = "tag" + c;
    }
    // Not is an illegal constant in DLV
    if (c.toLowerCase().equals("not")) {
      c = "tag" + c;
    }
    return c.toLowerCase();
  }

  /**
   * Returns true is the given string is a constant number
   * @param string the input string
   * @return true is the given string is a constant number, false otherwise
   */
  public static boolean isConstantNumber(final String string) {

    return string.matches("^\\d+$");
  }

  /**
   * Replaces any NON-word character (not in [a-zA-Z_0-9] ) with empty string
   * @param val the input string
   * @return a new string in which any non-work char is replaced by empty string
   */
  public static String replaceNonWordsWithEmptyString(String val) {

    if (val == null) {
      throw new RuntimeException("Null values cannot be escaped");
    }
    val = val.replaceAll("\\W", "");
    // }
    return val;
  }

  /**
   * Escapes a quoted string to be represented in DLV. In particular, inner double quotes (") are replaced by '\u0003'.
   * @param val the value to escape
   * @return an escaped string to be represented inside double quotes in DLV
   */
  public static String escapeStringContentForDLV(final String val) {

    return StringUtils.replaceChars(val, EscapingUtils.DOUBLE_QUOTE_CHAR, EscapingUtils.SPECIAL_CHAR);
  }

  /**
   * Unescape DLV string terms (the inverse operation of {@link EscapingUtils#escapeStringContentForDLV(String)})
   * @param context the value to unescape
   * @return
   */
  public static String unescapeDLVStringContent(final String context) {

    return StringUtils.replaceChars(context, EscapingUtils.SPECIAL_CHAR, EscapingUtils.DOUBLE_QUOTE_CHAR);
  }

  /**
   * Escapes the given string as a DLV string and quotes it.
   */
  public static String quoteAndEscapeString(final String s) {
    return quoteUnquotedString(escapeStringContentForDLV(s));
  }

  // private static String replaceNBSWithASCIISpace(final String context) {
  // return StringUtils.replaceChars(context, EscapingUtils.NON_BREAKING_SPACE, " ");
  // }

  /**
   * Wraps a string into double quotes (producing "unquotedString")
   * @param unquotedString the value to quote
   * @return the string "unquotedString"
   */
  public static String quoteUnquotedString(final String unquotedString) {

    return EscapingUtils.DOUBLE_QUOTE_AS_A_STRING + unquotedString + EscapingUtils.DOUBLE_QUOTE_AS_A_STRING;
  }

  /**
   * Removes double quotes from a quoted string.
   * @param quotedString
   * @return the given string but unquoted
   */
  public static String stripQuotesFromQuotedString(final String quotedString) {

    // AVOID reference to strings bigger than needed
    return new String(EscapingUtils.selectUnquotedPortionFromQuotedString(quotedString));
  }

  private static String selectUnquotedPortionFromQuotedString(final String quotedStringValue) {

    assert EscapingUtils.isQuotedString(quotedStringValue) : String.format(
        "A quoted string value must start AND end with double quotes, %s is not a good quoted string in this respect",
        quotedStringValue);
    final String substring = quotedStringValue.substring(1, quotedStringValue.length() - 1);
    return substring;
  }

  /**
   * Checks whether a string is properly quoted (e.g, "quotedStringValue")
   * @param quotedStringValue
   * @return <code>true</code> if quotedStringValue is properly quoted (e.g, "quotedStringValue"), <code>false</code>
   *         otherwise
   */
  public static boolean isQuotedString(final String quotedStringValue) {

    return quotedStringValue.startsWith(EscapingUtils.DOUBLE_QUOTE_AS_A_STRING)
        && quotedStringValue.endsWith(EscapingUtils.DOUBLE_QUOTE_AS_A_STRING);
  }

  /**
   * Checks if the given string matches the regex [\u00A0] for a non-breaking-space
   * @param nodeValue the string to check
   * @return <code>true</code> if the given string matches the regex [\u00A0], <code>false</code> otherwise
   */
  public static boolean isNonBreakingSpace(final String nodeValue) {

    return nodeValue.matches("[" + EscapingUtils.NON_BREAKING_SPACE + "]+");
  }

  public static final char SPECIAL_CHAR = '\u0003';
  public static final char DOUBLE_QUOTE_CHAR = '\"';
  private static final String DOUBLE_QUOTE_AS_A_STRING = "\"";
  public static final String NON_BREAKING_SPACE = "\u00A0";

  public static String normalizeTextNodes(final String nodeValue) {
    // TODO should use CharMatcher.WHITSPACE or something similar.
    return CharMatcher.anyOf("\u00A0\t\f\n\r ").trimAndCollapseFrom(nodeValue, ' ');
  }

  private EscapingUtils() {
  }

}
