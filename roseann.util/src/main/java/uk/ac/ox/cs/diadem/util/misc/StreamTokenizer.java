/**
 * 
 */
package uk.ac.ox.cs.diadem.util.misc;

import java.io.IOException;
import java.io.Reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.ParserException;
import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;

/**
 * 
 * TODO we do not have an initial state, leaving a huge door open for errors
 * 
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 */
public class StreamTokenizer {
  private static final Logger LOGGER = LoggerFactory.getLogger(StreamTokenizer.class);
  private static final Configuration CONFIG = ConfigurationFacility.getConfiguration("util.streamTokenizer");

  private long longVal;
  private String sval;

  public enum State {
    EOF, LONG, WORD, STRING, CHAR
  };

  private State state;
  private Reader reader = null;
  private boolean pushedBack = false;
  private boolean charPushedBack = false;
  private int lastChar = -2;

  private static final int TOKEN_BUFFER_SIZE = CONFIG.getInt("tokenBufferSize");
  private static final int READ_BUFFER_SIZE = CONFIG.getInt("readBufferSize");
  private static final int TOKEN_BUFFER_SIZE_MULTIPLIER = CONFIG.getInt("tokenBufferSizeMultiplier");
  private static int tokenBufferSize = TOKEN_BUFFER_SIZE;
  private char[] tokenBuffer = new char[tokenBufferSize];
  private final char[] buffer = new char[READ_BUFFER_SIZE];
  private int bufferSize = 0;
  private int bufferPos = 0;

  public StreamTokenizer(final Reader reader) {
    assert reader != null;
    this.reader = reader;
  }

  public StreamTokenizer() {
  }

  public void init(final Reader reader) {
    assert reader != null;
    this.reader = reader;
    pushedBack = false;
    charPushedBack = false;
    lastChar = -2;
    bufferSize = 0;
    bufferPos = 0;
  }

  public String getContentAsString() {
    assert lastChar != -2 : "Invalid Protocol.";
    switch (state) {
    case CHAR:
      return "CHAR:" + ((char) lastChar);
    case EOF:
      return "EOF:";
    case LONG:
      return "LONG:" + Long.toString(longVal);
    case WORD:
      return "WORD: '" + sval + "'";
    case STRING:
      return "STRING: \"" + sval + "\"";
    default:
      assert false;
      return "";
    }
  }

  public char getChar() {
    assert lastChar != -2 : "Invalid Protocol.";
    assert state == State.CHAR : "Invalid Protocol.";
    return (char) lastChar;
  }

  public long getLong() {
    assert lastChar != -2 : "Invalid Protocol";
    assert state == State.LONG : "Invalid Protocol: state='" + state.toString() + "'";
    return longVal;
  }

  public String getWord() {
    assert lastChar != -2 : "Invalid Protocol";
    assert state == State.WORD : "Invalid Protocol: state='" + state.toString() + "'";
    return sval;
  }

  public String getString() {
    assert lastChar != -2 : "Invalid Protocol";
    assert state == State.STRING : "Invalid Protocol: state='" + state.toString() + "'";
    return sval;
  }

  public State getState() {
    assert lastChar != -2 : "Invalid Protocol";
    return state;
  }

  public void pushBack() {
    assert lastChar != -2 : "Invalid Protocol: Nothing to pushBack";
    assert !pushedBack : "Invalid Protocol: Only one pushBack possible.";
    pushedBack = true;
  }

  final private int nextChar() throws IOException {
    if (charPushedBack) {
      charPushedBack = false;
      return lastChar;
    }
    assert bufferSize >= bufferPos : "Violated Invariance.";
    if (bufferPos == bufferSize) {
      bufferSize = reader.read(buffer, 0, READ_BUFFER_SIZE);
      if (bufferSize == -1)
        return lastChar = -1;
      assert bufferSize > 0 : "Violated Invariance.";
      --bufferSize;
      bufferPos = 0;
      return lastChar = buffer[0];
    } else
      return lastChar = buffer[++bufferPos];
  }

  final private int nextCharWithoutPushBackManagement() throws IOException {
    assert !charPushedBack : "Violated Invariance";

    assert bufferSize >= bufferPos : "Violated Invariance.";
    if (bufferPos == bufferSize) {
      bufferSize = reader.read(buffer, 0, READ_BUFFER_SIZE);
      if (bufferSize == -1)
        return lastChar = -1;
      assert bufferSize > 0 : "Violated Invariance.";
      --bufferSize;
      bufferPos = 0;
      return lastChar = buffer[0];
    } else
      return lastChar = buffer[++bufferPos];
  }

  final private void pushBackChar() {
    assert lastChar != -2 : "Invalid Protocol: Nothing to pushBack";
    assert !charPushedBack : "Invalid Protocol: Only one pushBack possible.";
    charPushedBack = true;
  }

  final private int addToTokenBuffer(final int character, final int tokenBufferPos) {
    if (tokenBufferPos == tokenBufferSize) {
      final int newTokenBufferSize = tokenBufferSize * TOKEN_BUFFER_SIZE_MULTIPLIER;
      final char[] newTokenBuffer = new char[newTokenBufferSize];
      for (int i = 0; i < tokenBufferPos; ++i) {
        newTokenBuffer[i] = tokenBuffer[i];
      }
      tokenBuffer = newTokenBuffer;
      tokenBufferSize = newTokenBufferSize;
    }
    tokenBuffer[tokenBufferPos] = (char) character;
    return tokenBufferPos + 1;
  }

  final public State nextToken() throws IOException, ParserException {
    if (pushedBack) {
      pushedBack = false;
      return state;
    }

    int character = nextChar();
    // skip initial white space
    for (; character >= 0; character = nextCharWithoutPushBackManagement())
      if (!Character.isWhitespace(character)) {
        break;
      }

    if ((('a' <= character) && (character <= 'z')) || (('A' <= character) && (character <= 'Z')) || (character == '_')) {
      state = State.WORD;
      tokenBuffer[0] = (char) character;
      int tokenBufferPos = 1;
      for (character = nextCharWithoutPushBackManagement(); character >= 0; character = nextCharWithoutPushBackManagement()) {
        if ((('a' > character) || (character > 'z')) && (('A' > character) || (character > 'Z'))
            && (('0' > character) || (character > '9')) && (character != '_')) {
          break;
        }
        tokenBufferPos = addToTokenBuffer(character, tokenBufferPos);
        // tokenBuffer[tokenBufferPos] = (char) character;
        // ++tokenBufferPos;
      }
      pushBackChar();
      sval = new String(tokenBuffer, 0, tokenBufferPos);
      return State.WORD;
    }

    if (('0' <= character) && (character <= '9')) {
      state = State.LONG;
      // tokenBuffer[tokenBufferPos]=(char) character;
      // ++tokenBufferPos;
      longVal = (char) character - '0';
      for (character = nextCharWithoutPushBackManagement(); character >= 0; character = nextCharWithoutPushBackManagement()) {
        if ((('0' > character) || (character > '9'))) {
          break;
        }
        longVal = ((longVal * 10) + (char) character) - '0';
        // tokenBuffer[tokenBufferPos]=(char) character;
        // ++tokenBufferPos;
      }
      pushBackChar();
      return State.LONG;
    }

    if (character == '-') {
      state = State.LONG;
      // tokenBuffer[tokenBufferPos]=(char) character;
      // ++tokenBufferPos;
      longVal = 0;
      for (character = nextCharWithoutPushBackManagement(); character >= 0; character = nextCharWithoutPushBackManagement()) {
        if ((('0' > character) || (character > '9'))) {
          break;
        }
        longVal = ((longVal * 10) + (char) character) - '0';
        // tokenBuffer[tokenBufferPos]=(char) character;
        // ++tokenBufferPos;
      }
      pushBackChar();
      longVal = -longVal;
      // integerVal=Integer.parseInt(new String(tokenBuffer,0,tokenBufferPos));
      return State.LONG;
    }

    if ('"' == character) {
      int tokenBufferPos = 0;
      state = State.STRING;
      for (character = nextCharWithoutPushBackManagement(); character >= 0; character = nextCharWithoutPushBackManagement()) {
        // if (character == '\\') {
        // tokenBuffer[tokenBufferPos] = (char) character;
        // ++tokenBufferPos;
        // character = nextChar();
        // if (character == -1)
        // throw new ParserException("Malformed String ending in '\\'.", logger);
        // tokenBuffer[tokenBufferPos] = (char) character;
        // ++tokenBufferPos;
        // } else {
        if (character == '"') {
          break;
        }
        tokenBufferPos = addToTokenBuffer(character, tokenBufferPos);
        // tokenBuffer[tokenBufferPos] = (char) character;
        // ++tokenBufferPos;
        // }
      }
      if (character == -1)
        throw new ParserException("Malformed String '\"" + new String(tokenBuffer, 0, tokenBufferPos)
            + "'ending not in '\"'.", LOGGER);
      sval = new String(tokenBuffer, 0, tokenBufferPos);
      return State.STRING;
    }

    if (character == -1)
      return state = State.EOF;

    return state = State.CHAR;
  }

  public String getTokenAsString() {
    switch (state) {
    case EOF:
      return "EOF";
    case LONG:
      return Long.toString(getLong());
    case WORD:
      return getWord();
    case STRING:
      return getString();
    case CHAR:
      return Character.toString(getChar());
    default:
      assert false;
      return "";
    }
  }
}
