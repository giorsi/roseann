/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.util.client;

import uk.ac.ox.cs.diadem.env.configuration.Configuration;

/**
 * @author timfu
 */
public interface OXPathClient {

  /**
   * Initialises the client and sets the IP and port of the OXPath server.
   */
  void initialize(String hostIP, int hostPort);

  OXPathServerHandle submitExpression(String OXPath, Configuration config, OXPathVariableBindings bindings);

  OXPathExecutionInfo retrieveStatus(OXPathServerHandle handle);

  OXPathExecutionInfo terminateExecution(OXPathServerHandle handle);

  OXPathServerHandle[] listExecutions(OXPathExecutionInfo.ExecutionStatus status);

}
