package uk.ac.ox.cs.diadem.util.reflection;

import java.util.Map;

public class StackUtil {
	
	public static String getMainclassName() {
		final Map<Thread, StackTraceElement[]> traces=Thread.getAllStackTraces();
		for(Map.Entry<Thread,StackTraceElement[]> entry : traces.entrySet()) {
			if(entry.getKey().getName().equals("main")) 
				return entry.getValue()[entry.getValue().length-1].getClassName();
		}
		assert false : "Violated Invariance.";
		return null;
	}
}
