/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.extractiv;

import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonNodeDoesNotMatchJsonNodeSelectorException;
import argo.jdom.JsonRootNode;
import argo.jdom.JsonStringNode;
import argo.saj.InvalidSyntaxException;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.AnnotationAttribute;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.util.Offset;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.util.QueryDbpediaEndpoint;

/**
 *
 *
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk) Oxford University,
 *         Department of Computer Science
 *
 *         The adapter submit the text to Extractiv, take the response as String
 *         representing JSon Object, builds a JSon Object and select the
 *         annotations to be returned. We can retrieve entity annotations as
 *         well as relation annotations
 *
 *         TO DO: Relation Annotations
 */

public class ExtractivAnnotator extends AnnotatorAdapter {

  static final Logger logger = LoggerFactory.getLogger(ExtractivAnnotator.class);

  /**
   * counter to increment the annotation id
   */
  private long id_counter;
  /**
   * The source text submitted to Extractiv We need to store this information
   * because Extractiv modify the text in the answer
   */
  private String sourceText;

  /**
   * singleton instance
   */
  private static ExtractivAnnotator INSTANCE;

  /**
   * map to store for every dbpedia resource a set of types for that resource
   * extractiv indeed provides for an annotation the dbpedia resource link
   */
  private Map<String, Set<String>> resource2types;

  /**
   * map to store the last index of a given instance
   */
  private final Map<String, Integer> instance2offset;

  /**
   * The service to be invoked
   */
  private final ExtractivService service;

  /**
   * private constructor to build the singleton instance
   */
  private ExtractivAnnotator() {
    ConfigurationFacility.getConfiguration();
    setAnnotatorName("extractiv");

    // start the counter from 0
    id_counter = 0;

    instance2offset = new HashMap<String, Integer>();

    // initialize the parametrs
    final AnnotatorConfiguration configuration = new AnnotatorConfiguration();
    configuration.initializeConfiguration(getAnnotatorName());
    setConfig(configuration);

    service = new ExtractivService();

  }

  /**
   * method to get the singleton instance
   *
   * @return singleton instance
   */
  public static synchronized Annotator getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new ExtractivAnnotator();
    }
    return INSTANCE;
  }

  /*
   * (non-Javadoc)
   *
   * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.
   * AnnotatorAdapter#annotateEntity(java.lang.String)
   */
  @Override
  public Set<Annotation> annotateEntity(final String text) {
    sourceText = text;

    final String response = this.submitToAnnotator(text);

    // save the annotations and increment the id counter
    final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(response, id_counter);
    final int num_annotations = entityAnnotations.size();
    id_counter += num_annotations;
    return entityAnnotations;
  }

  /*
   * (non-Javadoc)
   *
   * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.
   * AnnotatorAdapter#annotateEntity(java.lang.String, int)
   */
  @Override
  public Set<Annotation> annotateEntity(final String text, final int timeout) {
    sourceText = text;

    final String response = this.submitToAnnotator(text, timeout);
    if (response == null)
      return new HashSet<Annotation>();

    // save the annotations and increment the id counter
    final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(response, id_counter);
    final int num_annotations = entityAnnotations.size();
    id_counter += num_annotations;
    return entityAnnotations;
  }

  /*
   * (non-Javadoc)
   *
   * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.
   * AnnotatorAdapter#annotateRelation(java.lang.String)
   */
  @Override
  public Set<Annotation> annotateRelation(final String text) {
    // TO DO
    return null;
  }

  /*
   * (non-Javadoc)
   *
   * @see
   * uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator#
   * submitToAnnotator(java.lang.String)
   */
  @Override
  public String submitToAnnotator(final String text) {
    return invokeService(text, 0);
  }

  /*
   * (non-Javadoc)
   *
   * @see
   * uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator#
   * submitToAnnotator(java.lang.String, int)
   */
  @Override
  public String submitToAnnotator(final String text, final int timeout) {
    return invokeService(text, timeout);
  }

  private String invokeService(String text, final int timeout) {
    if ((text == null) || (text.length() < 1)) {
      logger.error("Trying to submit an empty text to Extractiv");
      throw new AnnotatorException("Trying to submit an empty text to Extractiv", logger);
    }

    String res = null;

    try {

      logger.debug("Submitting document to Extractiv service.");

      // eliminate from the original text special characters
      text = text.replaceAll("\u0003", " ");

      res = service.invokeService(text, getConfig(), timeout);
      logger.debug("Extractiv Process done and response received.");

      return res;

    }
    // check if the timeout has expired
    catch (final SocketTimeoutException e) {
      logger.warn("{} was not able to receive the web response within timeout {} milliseconds", getAnnotatorName(),
          timeout);
      return null;

    } catch (final Exception e) {
      logger.error("Error submitting text to Extractiv");
      throw new AnnotatorException("Error submitting text to Extractiv", e, logger);
    }
  }

  private Set<Annotation> retrieveEntityAnnotations(final String json_response, final long progressive) {

    final Set<Annotation> entityAnnotation = new HashSet<Annotation>();
    JsonRootNode doc;

    logger.debug("Processing the Extractiv response.");
    try {
      // create a JSon node parsing the text response
      doc = new JdomParser().parse(json_response);

    } catch (final InvalidSyntaxException e) {
      logger.error("The String response from Extractiv can't be parse as JSonNode");
      throw new AnnotatorException("The String response from Extractiv " + "can't be parse as JSonNode", e, logger);
    }

    // select only the entity annotations
    final List<JsonNode> selected_annotations = selectAnnotations(doc, "entities");

    if (selected_annotations == null) {
      logger.debug("Extractiv service did not find any entity annotations.");
      return entityAnnotation;
    }

    // retrieve all dbpedia types
    setDbpediaResourceType(selected_annotations);

    // to count the annotation
    int count = 0;

    // iterate over all annotation
    for (final JsonNode annotation : selected_annotations) {

      // increment the annotation id and save the annotations found
      final Set<Annotation> found_annotations = buildAnnotationFromJson(annotation, count + progressive);
      count += found_annotations.size();
      entityAnnotation.addAll(found_annotations);
    }
    logger.debug("Response processed and entity annotations saved.");

    // reset the context
    resetContext();

    return entityAnnotation;

  }

  /**
   * Select in the annotated document only the annotation of a certain type For
   * every annotation select only one instance
   *
   * @param root
   *          the JSon node representing the annotated document
   * @param type
   *          type of the annotations to be selected
   * @return a Map where the key is the text annotated and value is list of
   *         annotation object which annotates that text
   */
  private List<JsonNode> selectAnnotations(final JsonNode root, final String type) {

    List<JsonNode> selected_annotations;
    try {
      selected_annotations = root.getArrayNode(type);
    } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
      // if there are no annotation of a certain type return null
      return null;
    }

    return selected_annotations;

  }

  /**
   * Construct the annotation and attribute objects from a JSon representation
   * Currently Extractiv provides just links as attributes
   *
   * @param json_annotation
   *          JSon object representing the instance of annotation
   * @param annotation_id
   *          Annotation_id for the given annotation
   * @return the set of found annotation
   */

  private Set<Annotation> buildAnnotationFromJson(final JsonNode json_annotation, final long annotation_id) {

    final Set<Annotation> annotations_found = new HashSet<Annotation>();

    // retrieve all the information for the annotation instance

    // the annotator type
    final String annotator_term = json_annotation.getStringValue("type");

    final String annotator_name = getAnnotatorName();

    // source of knowledge for Extractiv are their own Gazzetter
    String source = "extractiv_gazzetter";

    // entity class is always an instance
    final AnnotationClass entity_class = AnnotationClass.INSTANCE;

    // confidence given by the annotator
    // for those entities without a confidence value, put null
    Double confidence = null;
    try {
      confidence = Double.parseDouble(json_annotation.getNumberValue("confidence"));
    } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {

    }

    final String annotated_text = json_annotation.getStringValue("text");

    // get the initial position to calculate the offset
    Integer offset_to_start = instance2offset.get(annotated_text.toLowerCase());

    if (offset_to_start == null) {
      try {
        offset_to_start = Integer.parseInt(json_annotation.getNumberValue("offset"));
      } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
        offset_to_start = 0;
      }

    }

    // compute start-end for the instance
    final Map<Integer, Integer> start2end = Offset.getInstancePosition(1, sourceText.substring(offset_to_start),
        annotated_text);

    if (start2end == null) {
      logger.debug("Not able to find offset of annotated text {}", annotated_text);
      return annotations_found;
    }

    int start = start2end.keySet().iterator().next();
    final int end = start2end.get(start) + offset_to_start;
    start += offset_to_start;

    instance2offset.put(annotated_text.toLowerCase(), end);

    // retrieve the attribute for the annotation
    final Map<String, List<String>> attribute2value = getAnnotationAttribute(json_annotation);

    final Set<String> types = new HashSet<String>();
    types.add(annotator_term);

    // retrieve all dbpedia types only if type is equal to LINKED_OTHER
    if ((attribute2value != null) && annotator_term.equals("LINKED_OTHER") && (resource2types != null)) {
      for (final String link : attribute2value.get("link")) {

        final Set<String> link_types = resource2types.get(link);
        if (link_types != null) {
          types.addAll(link_types);
        }
      }
    }

    // if found dbpedia types, delete linked_other as types and keep only
    // dbpedia ones
    // set dbpedia as source
    if (types.size() > 1) {
      source = "dbpedia_ontology";
      types.remove("LINKED_OTHER");

    }

    // iterate over all types and create a
    // fact for each of them

    for (final String type : types) {

      final String id = getAnnotatorName() + "_" + (annotation_id + annotations_found.size());

      // create the new entity annotation
      final EntityAnnotation annotation = new EntityAnnotation();

      annotation.setAnnotationClass(entity_class);
      annotation.setAnnotationSource(source);
      annotation.setId(id);
      annotation.setConcept(type);
      annotation.setOriginAnnotator(annotator_name);
      annotation.setStart(start);
      annotation.setEnd(end);
      if (confidence != null) {
        annotation.setConfidence(confidence);
      }

      if (attribute2value != null) {
        // for every attribute create a object
        for (final String attribute_name : attribute2value.keySet()) {
          for (final String value : attribute2value.get(attribute_name)) {

            final AnnotationAttribute attribute = new AnnotationAttribute(attribute_name, value);
            annotation.addAttribute(attribute);
          }
        }
      }
      annotations_found.add(annotation);
    }

    return annotations_found;

  }

  /**
   * Utility method to retrieve attributes for a certain annotation Currently
   * Extractiv provides only links attribute
   *
   * @param annotation
   *          JSon object representing the instance of annotation
   * @return map where key is the name of the attribute, value is a list of all
   *         values for that attribute
   */
  private Map<String, List<String>> getAnnotationAttribute(final JsonNode annotation) {
    final Map<String, List<String>> attribute2value = new HashMap<String, List<String>>();
    final List<String> values = new LinkedList<String>();

    try {
      final List<JsonNode> links = annotation.getArrayNode("links");
      for (final JsonNode j : links) {
        values.add(((JsonStringNode) j).getText());
      }
      attribute2value.put("link", values);
    } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
      // if not able to find links attribute, simply continue
    }
    return attribute2value.size() > 0 ? attribute2value : null;
  }

  /**
   * Utility method to retrieve all dbpedia onotlogy types for a given list of
   * resource link
   *
   * @param annotations
   *          list of annotations every annotation can contain one or more link
   *          to dbpedia resource
   */
  private void setDbpediaResourceType(final List<JsonNode> annotations) {
    final Set<String> resources = new HashSet<String>();

    // get dbpedia resource link, if found, for every annotation
    for (final JsonNode annotation : annotations) {
      if (annotation.getStringValue("type").equals("LINKED_OTHER")) {
        try {
          final List<JsonNode> links = annotation.getArrayNode("links");

          for (final JsonNode link : links) {
            final String resource = link.getText();
            if (resource.contains("http://dbpedia.org/resource/")) {
              resources.add(resource);
            }
          }

        } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
          // if not able to find links attribute, simply continue
        }
      }
    }
    try {
      resource2types = QueryDbpediaEndpoint.getMultipleTypes(new LinkedList<String>(resources));
    }

    catch (final Exception e) {
      logger.error("Error trying to query the dbpedia SPARQL endpoint to retrieve all types for linked other labels");
    }
  }

  public String getSourceText() {
    return sourceText;
  }

  public void setSourceText(final String sourceText) {
    this.sourceText = sourceText;
  }

  private void resetContext() {
    sourceText = null;
    if (resource2types != null) {
      resource2types.clear();
    }
    instance2offset.clear();

  }

  public void resetId() {
    id_counter = 0;
  }

}
