/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.extractiv;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;

/**
 * 
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk) Oxford University, Department of Computer Science
 * 
 *         Use this class to send a request to Extractiv web service and get a response as a string.
 * 
 */
class ExtractivService {

  /**
   * Method to invoke the extractiv service with the input text and the parameters
   * 
   * @param text
   *          Text to analyze
   * @param parameters
   *          The AnnotatorConfiguration object that contains the parameters for extractiv service. If the output_mode
   *          parameter has not been specified (or it has an unknown value), the default output format is Json
   * @return The response from Extractiv as a string according to the set format
   * @throws IOException
   *           If something goes wrong during the invocation of the service
   * @throws IllegalArgumentException
   *           If one of the mandatory parameter has not been specified or it has been badly specified
   */
  public String invokeService(final String text, final AnnotatorConfiguration parameters, final int timeout)
      throws IOException, IllegalArgumentException {

    if (null == text || text.length() < 1)
      throw new IllegalArgumentException("Enter some text to analyze.");

    final String encoded_params = buildParameterString(text, parameters);

    final URL endpoint = parameters.getURlendpoint();
    if (endpoint == null)
      throw new IllegalArgumentException("The endpoint url has not been specified");

    return postRequest(encoded_params, endpoint, timeout);
  }

  /**
   * Send the post request
   * 
   * @param params
   * @param endpoint
   * @return
   * @throws IOException
   */
  private String postRequest(final String params, final URL endpoint, final int timeout) throws IOException {

    final HttpURLConnection handle = (HttpURLConnection) endpoint.openConnection();

    handle.setDoOutput(true);
    handle.setRequestMethod("POST");
    handle.setConnectTimeout(timeout);

    final StringBuilder data = new StringBuilder();

    data.append(params);
    handle.addRequestProperty("Content-Length", Integer.toString(data.length()));

    final DataOutputStream ostream = new DataOutputStream(handle.getOutputStream());
    ostream.write(data.toString().getBytes());
    ostream.close();

    // get the response.
    final BufferedReader rd = new BufferedReader(new InputStreamReader(handle.getInputStream()));
    String line, result = "";

    while ((line = rd.readLine()) != null) {
      result += line;
    }
    handle.disconnect();

    return result;
  }

  /**
   * Build the parameter string to send along with the request
   * 
   * @param text
   * @param configuration
   * @return
   * @throws IllegalArgumentException
   * @throws UnsupportedEncodingException
   */
  private String buildParameterString(final String text, final AnnotatorConfiguration configuration)
      throws IllegalArgumentException, UnsupportedEncodingException {

    final StringBuilder sb = new StringBuilder();
    String output_mode = configuration.getSpecificParameters().get("output_format");
    // if the output mode has not been specified or it has an unknown value the output mode is set to json
    if (output_mode == null || output_mode.equals("json") && output_mode.equals("rdf") && output_mode.equals("xml")) {
      output_mode = "json";
    }

    sb.append(URLEncoder.encode("output_format", "UTF-8") + "=" + output_mode);

    for (final String parameter_name : configuration.getSpecificParameters().keySet()) {
      sb.append("&" + parameter_name + "="
          + URLEncoder.encode(configuration.getSpecificParameters().get(parameter_name), "UTF-8"));

    }

    sb.append("&content" + "=" + URLEncoder.encode(text, "UTF-8"));

    return sb.toString();

  }

}