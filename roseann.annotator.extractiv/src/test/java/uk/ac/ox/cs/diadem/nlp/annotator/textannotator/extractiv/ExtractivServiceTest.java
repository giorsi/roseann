/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.extractiv;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;

public class ExtractivServiceTest {

  /**
   * The url endpoint
   */
  final private static String endpoint = "http://demo.languagecomputer.com/entity/extended/";

  /**
   * Test to inovoke the service and check that a json response is returned, with specific elements
   * 
   * @throws IOException
   * @throws IllegalArgumentException
   * @throws InvalidSyntaxException
   */
  @Test public void invokeServiceTest() throws IllegalArgumentException, IOException, InvalidSyntaxException {

    final String text = "Barack Obama is the new president of United States of America.";

    // initialize the configuration
    final AnnotatorConfiguration config = new AnnotatorConfiguration();

    // set the url endpoint
    config.setUrlendpoint(new URL(endpoint));

    final ExtractivService service = new ExtractivService();
    final String response = service.invokeService(text, config, 0);

    Assert.assertNotNull(response);
    Assert.assertTrue(response.length() > 0);

    // parse the response as json object and verify the presence of some json elements
    final JsonRootNode doc = new JdomParser().parse(response);

    final List<JsonNode> entities = doc.getArrayNode("entities");
    Assert.assertNotNull(entities);
    Assert.assertTrue(entities.size() > 0);

    final List<JsonNode> relations = doc.getArrayNode("relations");
    Assert.assertNotNull(relations);
    Assert.assertTrue(relations.size() > 0);
  }

  /**
   * Check that when no url-ednpoint has been specified the service cannot be invoked
   * 
   * @throws IOException
   * @throws IllegalArgumentException
   */
  @Test(expected = IllegalArgumentException.class) public void invokeServiceTestWithoutParameter()
      throws IllegalArgumentException, IOException {
    final String text = "Barack Obama is the new president of United States of America.";

    // initialize the configuration
    final AnnotatorConfiguration config = new AnnotatorConfiguration();

    final ExtractivService service = new ExtractivService();
    service.invokeService(text, config, 0);

  }

}
