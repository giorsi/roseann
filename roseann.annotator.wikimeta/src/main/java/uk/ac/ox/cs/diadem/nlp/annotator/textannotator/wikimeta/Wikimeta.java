/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.wikimeta;

import java.io.*;
import java.net.*;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;


/**
 *
 * @author descl
 * 
 * Improvements by eric / eric.charton@polymtl.ca
 * 
 */
class Wikimeta {

	public enum Format {
		XML("xml"),
		JSON("json");

		private final String value;

		Format(String value) {this.value = value;}

		public String getValue() {return this.value;}
	}

	/*
	 * Define API URL by default
	 * 
	 */
	private  String apiurl = ("http://www.wikimeta.com/wapi/service");
	/**
	 * api key
	 */
	private  String api_key;

	/**
	 * construct a default loader
	 */
	private Wikimeta() { // default constructor - use default simplenlg.lexicon

	}

	/**
	 * constructor with an API URL passed 
	 */
	private Wikimeta(String ParametrizedUrl) { // default constructor - use default simplenlg.lexicon
		this.setApiurl(ParametrizedUrl);
	}

	/*
	 * Full method
	 * 
	 */
	public  String getResult(String apiKey, Format format, String content, String treshold, String span, 
			String lng, String semtag, String stats,int timeout) {    
		String result = "";
		String callFormat = format.value;

		if(treshold==null) treshold="10";
		if(span==null) span="100";
		if(lng==null) lng="EN";
		if(semtag==null) semtag="1";
		if(stats==null) stats="0";

		try {
			URL url = new URL(getApiurl());
			HttpURLConnection server = (HttpURLConnection)url.openConnection();
			server.setConnectTimeout(timeout);
			server.setDoInput(true);
			server.setDoOutput(true);
			server.setRequestMethod("POST");
			server.setRequestProperty("Accept", callFormat );
			server.connect();

			BufferedWriter bw = new BufferedWriter(
					new OutputStreamWriter(
							server.getOutputStream()));


			String request = "treshold="+treshold+"&span="+span+"&lng="+lng+"&semtag="+semtag+"&api="+apiKey+"&textmining="+stats+"&contenu="+content;
			// String request = "treshold="+treshold+"&span="+span+"&lng="+lng+"&semtag="+semtagS+"&textmining="+stats+"&contenu="+content;

			bw.write(request, 0, request.length());
			bw.flush();
			bw.close();

			BufferedReader reader = new BufferedReader(new InputStreamReader(server.getInputStream()));

			String ligne;
			while ((ligne = reader.readLine()) != null) {
				result += ligne+"\n";
			}

			reader.close();
			server.disconnect();
		}
		//check if the timeout has expired
		catch(final SocketTimeoutException e) {
			return "timeout";
		}

		catch (Exception e)
		{
			e.printStackTrace();
			Logger.getLogger(Wikimeta.class.getName()).log(Level.SEVERE, null, e);
		}
		return result;
	}

	/**
	 * @param apiurl the apiurl to set
	 */
	public void setApiurl(String apiurl) {
		this.apiurl = apiurl;
	}

	/**
	 * @return the apiurl
	 */
	public  String getApiurl() {
		return apiurl;
	}

	public  String getApi_key() {
		return api_key;
	}

	public  void setApi_key(String api_key) {
		this.api_key = api_key;
	}

	public static Wikimeta GetInstanceFromConfig(final AnnotatorConfiguration config) {
		Wikimeta api = new Wikimeta();
		final String key =config.getApiKey();
		if(key==null)
			throw new IllegalArgumentException("The api key has not been specified");

		api.setApi_key(key);
		final URL url = config.getURlendpoint();
		if(url!=null){
			api.setApiurl(url.toString());
		}

		return api;

	}

	public String invokeNEService(String freeText, AnnotatorConfiguration config,int timeout) {
		final Map<String, String> parameters = config.getSpecificParameters();
		String response=null;
		if(parameters!=null){
			response = getResult(api_key, Wikimeta.Format.JSON, freeText, 
					parameters.get("treshold"),parameters.get("span"),parameters.get("lng"),
					parameters.get("semtag"),parameters.get("textmining"),timeout);
		}
		return response;
	}


}
