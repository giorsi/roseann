/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.wikimeta;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;

/**
 * @author Stefano Ortona, Luying Chen (name.surname@cs.ox.ac.uk) Oxford
 *         University, Department of Computer Science The wrapper submit the
 *         text to Wikimeta service, take the response as String representing
 *         JSon Object, builds a JSon Object and select only the entity
 *         annotations to be saved in the model.
 */

public class WikimetaAnnotator extends AnnotatorAdapter {

  static final Logger logger = LoggerFactory.getLogger(WikimetaAnnotator.class);

  /**
   * to count the annotation
   */
  private long id_counter;
  private Wikimeta wikimeta;

  /**
   * Singleton instance
   */
  private static WikimetaAnnotator INSTANCE;

  /**
   * private constructor to build the instance
   */
  private WikimetaAnnotator() {
    ConfigurationFacility.getConfiguration();
    setAnnotatorName("wikimeta");

    // start the id from 0
    id_counter = 0;

    // initialize the parametrs
    final AnnotatorConfiguration configuration = new AnnotatorConfiguration();
    configuration.initializeConfiguration(getAnnotatorName());
    setConfig(configuration);

  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.
   * AnnotatorAdapter
   * #setConfig(uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.
   * AnnotatorConfiguration)
   */
  @Override
  public void setConfig(final AnnotatorConfiguration config) {
    // TODO Auto-generated method stub
    super.setConfig(config);
    // Instantiate the alchemyAPI with the new configuration

    wikimeta = Wikimeta.GetInstanceFromConfig(config);

  }

  /**
   * method to get the singleton instance
   * 
   * @return singleton instance
   */
  public static synchronized Annotator getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new WikimetaAnnotator();
    }
    return INSTANCE;
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.
   * AnnotatorAdapter#annotateEntity(java.lang.String)
   */
  @Override
  public Set<Annotation> annotateEntity(final String text) {
    final String response = submitToAnnotator(text);
    Set<Annotation> entities = null;
    if (response != null) {
      entities = retrieveEntityAnnotations(response, text);
    }
    return entities;
  }

  @Override
  public Set<Annotation> annotateEntity(final String text, final int timeout) {
    final String response = submitToAnnotator(text, timeout);
    if (response == null)
      return new HashSet<Annotation>();
    Set<Annotation> entities = null;
    if (response != null) {
      entities = retrieveEntityAnnotations(response, text);
    }
    return entities;
  }

  /**
   * An utility method to harvest entity annotations from annotator response
   * 
   * @param text
   *          Text to be annotated
   * @param response
   *          The response from the service
   * @return Set<Annotation> Set of entity annotations
   */

  private Set<Annotation> retrieveEntityAnnotations(final String response, final String orgText) {
    JsonRootNode doc;
    logger.debug("Processing the Wikimeta response.");
    try {
      // create a JSon node parsing the text response
      doc = new JdomParser().parse(response);
    } catch (final InvalidSyntaxException e) {
      logger.error("The String response '{}' from Wikimeta can't be parse as JSonNode", response);
      throw new AnnotatorException("The String response from wikimetaAnnotator " + "can't be parse as JSonNode", e,
          logger);
    }

    final Set<Annotation> entities = new HashSet<Annotation>();
    try {
      final List<JsonNode> docs = doc.getArrayNode("document");
      final List<JsonNode> selected_annotations = docs.get(2).getArrayNode("Named Entities");
      if (selected_annotations == null)
        // logger.debug("wikimetaAnnotator service did not find any entity
        // annotations.");
        return entities;
      int searchBeginAt = 0;

      // iterate over all annotation
      for (final JsonNode annotation : selected_annotations) {
        searchBeginAt = constructIndividualNE(annotation, orgText, searchBeginAt, entities);

      }
    } catch (final Exception e) {
      logger.error("The String response from Wikimeta can't be processed");
      throw new AnnotatorException("The String response from Wikimeta " + "can't be processed", e, logger);

    }
    logger.debug("Response processed and annotations saved.");
    return entities;

  }

  /**
   * Utility method to construct an annotation object from a json node.
   * 
   * @param annotation
   *          The json node with the information of an annotation
   * @param orgText
   *          The orginal text to be annotated
   * @param searchBeginAt
   *          The starting point for next search of anchor text matching (no
   *          offset is provided in the response)
   * @param entities
   *          The entity set to be returned
   * @return int The new starting point
   */
  private int constructIndividualNE(final JsonNode annotation, final String orgText, final int searchBeginAt,
      final Set<Annotation> entities) {

    final String textAnchor = annotation.getStringValue("EN").trim();
    final String origLabel = annotation.getStringValue("type").trim();
    // transform the concept dlv compatible
    // origLabel = AnnotationUtil.modifyConcept(origLabel);

    // find the snippet's offset by string matching from the given starting
    // point
    int matchAt = -1;
    matchAt = orgText.indexOf(textAnchor, searchBeginAt);
    if (matchAt >= 0) {// find a match, add the atom to the model
      // create the new entity annotation
      final Annotation oneAnnotation = new EntityAnnotation(annotatorName + "_" + (id_counter++), origLabel, matchAt,
          matchAt + textAnchor.length(), annotatorName, null, null, AnnotationClass.INSTANCE);

      entities.add(oneAnnotation);
    }
    return matchAt < 0 ? searchBeginAt : matchAt + textAnchor.length();

  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator#
   * submitToAnnotator(java.lang.String)
   */

  public String submitToAnnotator(final String freeText) {
    return this.submitToAnnotator(freeText, 0);
  }

  public String submitToAnnotator(String freeText, final int timeout) {
    if ((freeText == null) || (freeText.length() < 1)) {
      logger.error("Trying to submit an empty text to Zemanta");
      throw new AnnotatorException("Trying to submit an empty text to Zemanta", logger);
    }

    logger.debug("Submitting document to Zemanta service.");

    // eliminate from the original text special characters
    freeText = freeText.replaceAll("\u0003", " ");

    final String res = wikimeta.invokeNEService(freeText, config, timeout);

    // check the timeout has not expired
    if (res.equals("timeout")) {
      logger.warn("{} was not able to receive the web response within timeout {} milliseconds", getAnnotatorName(),
          timeout);
      return null;
    }

    logger.debug("Wikimeta Process done and response received.");

    return res;
  }

}
