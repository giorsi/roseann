/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.saplo;

import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;
import junit.framework.Assert;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;

public class SaploAnnotatorTest {

  private Annotator saplo;

  private String text;

  @Before
  public void bringUp() {
    saplo = SaploAnnotator.getInstance();
    text = "Barack Obama is the new president of the United States.";
  }

  /**
   * Test to check that a text can be submitted to saplo annotator service
   *
   * @throws InvalidSyntaxException
   */
  @Test
  public void testSubmitToAnnotator() throws InvalidSyntaxException {

    // check the url-ednpoint has been specified in the configuration
    Assert.assertNotNull(saplo.getConfig().getURlendpoint());

    // check the secret and api-key have been specified
    Assert.assertNotNull(saplo.getConfig().getSpecificParameters().get("secret"));
    Assert.assertNotNull(saplo.getConfig().getApiKey());

    // check that the text can be submitted to the annotator and no exceptions
    // are thrown
    final String response = saplo.submitToAnnotator(text);

    System.out.println(response);
    Assert.assertNotNull(response);
    Assert.assertTrue(response.length() > 0);

    // check the response can be parse as json node and contains an element
    // entities and an element relations
    final JsonRootNode doc = new JdomParser().parse(response);

    final List<JsonNode> entities = doc.getArrayNode("tags");
    Assert.assertNotNull(entities);
    Assert.assertTrue(entities.size() > 0);

    // check that one annotation has the desired json elements
    final JsonNode annotation = entities.iterator().next();
    Assert.assertNotNull(annotation.getStringValue("category"));
    Assert.assertNotNull(annotation.getStringValue("tag"));
    Assert.assertNotNull(annotation.getNumberValue("relevance"));
  }

  /**
   * Test to verify the entities retrieved from saplo web service
   */
  @Test
  public void testAnnotateEntities() {
    final Set<Annotation> annotations = saplo.annotateEntity(text);
    Assert.assertNotNull(annotations);
    Assert.assertTrue(annotations.size() > 0);
  }

  /**
   * Test to verify that no entitie ares retrieved from extractiv web service
   * with an senseless text
   */
  @Test
  public void testAnnotateNoEntities() {
    text = "cfrgfvdfgbfdgdgdfgdfgdgdg";

    final Set<Annotation> annotations = saplo.annotateEntity(text);
    Assert.assertNotNull(annotations);
    Assert.assertTrue(annotations.size() == 0);
  }

}
