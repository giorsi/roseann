/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.saplo;

import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.util.Offset;
import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;

import com.saplo.api.client.SaploClient;
import com.saplo.api.client.SaploClientException;
import com.saplo.api.client.entity.SaploCollection;
import com.saplo.api.client.entity.SaploCollection.Language;
import com.saplo.api.client.entity.SaploTag;
import com.saplo.api.client.entity.SaploText;
import com.saplo.api.client.manager.SaploCollectionManager;
import com.saplo.api.client.manager.SaploTextManager;

/**
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk) Oxford University, Department of Computer Science The wrapper
 *         submit the text to Saplo, take the response as String representing JSon Object, builds a JSon Object and
 *         select only the entity annotations to be returned.
 */
public class SaploAnnotator extends AnnotatorAdapter {
  /**
   * The logger
   */
  static final Logger logger = LoggerFactory.getLogger(SaploAnnotator.class);

  /**
   * id counter to increment the annotation id
   */
  private long id_counter;

  /**
   * Singleton instance
   */
  private static SaploAnnotator INSTANCE;

  /**
   * private constructor to build the instance
   */
  private SaploAnnotator() {
    ConfigurationFacility.getConfiguration();
    setAnnotatorName("saplo");

    // start the id from 0
    id_counter = 0;
    final AnnotatorConfiguration conf = new AnnotatorConfiguration();
    conf.initializeConfiguration(getAnnotatorName());
    setConfig(conf);
  }

  /**
   * method to get the singleton instance
   * 
   * @return singleton instance
   */
  public static synchronized Annotator getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new SaploAnnotator();
    }
    return INSTANCE;
  }

  @Override public Set<Annotation> annotateEntity(final String text) {

    final String response = this.submitToAnnotator(text);

    if (response == null)
      return new HashSet<Annotation>();

    // save the annotations and increment the id counter
    final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(response, id_counter, text);
    final int num_annotations = entityAnnotations.size();
    id_counter += num_annotations;
    return entityAnnotations;
  }

  @Override public Set<Annotation> annotateEntity(final String text, final int timeout) {

    final String response = this.submitToAnnotator(text, timeout);

    if (response == null)
      return new HashSet<Annotation>();

    // save the annotations and increment the id counter
    final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(response, id_counter, text);
    final int num_annotations = entityAnnotations.size();
    id_counter += num_annotations;
    return entityAnnotations;
  }

  /**
   * Given a json response from the Saplo service, construct the entity annotations
   * 
   * @param response
   *          json response
   * @param progressive
   *          a counter where to start the incremental id for the annotations
   * @param original_text
   *          the original text annotated
   * @return The set of entity annotations returned by Saplo service
   */
  private Set<Annotation> retrieveEntityAnnotations(final String response, final long progressive,
      final String original_text) {

    final Set<Annotation> entityAnnotation = new HashSet<Annotation>();
    JsonRootNode doc;

    logger.debug("Processing the Extractiv response.");
    try {
      // create a JSon node parsing the text response
      doc = new JdomParser().parse(response);

    } catch (final InvalidSyntaxException e) {
      logger.error("The String response from Extractiv can't be parse as JSonNode");
      throw new AnnotatorException("The String response from Extractiv " + "can't be parse as JSonNode", e, logger);
    }

    List<JsonNode> selected_annotations = null;

    try {
      // select only the entity annotations
      selected_annotations = doc.getArrayNode("tags");
    } catch (final IllegalArgumentException e) {
      logger.warn("There is no an element tags in the Json response");
      return entityAnnotation;
    }

    if (selected_annotations == null) {
      logger.debug("Extractiv service did not find any entity annotations.");
      return entityAnnotation;
    }

    // to count the annotation
    int count = 0;

    // iterate over all annotation
    for (final JsonNode annotation : selected_annotations) {

      // increment the annotation id and save the annotations found
      final Set<Annotation> found_annotations = buildAnnotationFromJson(original_text, annotation, count + progressive);
      count += found_annotations.size();
      entityAnnotation.addAll(found_annotations);
    }
    logger.debug("Response processed and entity annotations saved.");

    return entityAnnotation;
  }

  @Override public String submitToAnnotator(final String text) {
    return this.submitToAnnotator(text, 0);
  }

  /**
   * Currently Saplo allows the response to be only in a Json format, therefore the method will return always a String
   * representing a Json object
   */
  @Override public String submitToAnnotator(String text, final int timeout) {

    // the response to be returned
    String response = null;

    if (text == null || text.length() < 1) {
      logger.error("Trying to submit an empty text to Saplo");
      throw new AnnotatorException("Trying to submit an empty text to Saplo", logger);
    }

    // get the api and secret key. If those have not been specified, throws an exception
    final String api_key = getConfig().getApiKey();
    final String secret_key = getConfig().getSpecificParameters().get("secret");

    if (api_key == null || secret_key == null)
      throw new AnnotatorException("The api key or the secret api key have not been specified for" + " Saplo service",
          logger);

    logger.debug("Submitting document to Saplo service.");

    // get the url endpoint
    final URL url_endpoint = getConfig().getURlendpoint();
    if (url_endpoint == null)
      throw new AnnotatorException("The url endpoint has not been specified for" + " Saplo service", logger);

    // eliminate from the original text special characters
    text = text.replaceAll("\u0003", " ");

    try {
      final SaploClient client = new SaploClient(api_key, secret_key, url_endpoint.toString());

      // Create a manager to work with collections
      final SaploCollectionManager collectionMgr = new SaploCollectionManager(client);

      // Get a list of all your collections in a List
      final List<SaploCollection> collectionList = collectionMgr.list();

      // get the collection name
      String collection_name = getConfig().getSpecificParameters().get("collection_name");

      SaploCollection collection = null;

      // if there are no previous collections, create one
      if (collectionList == null || collectionList.size() == 0) {
        if (collection_name == null || collection_name.length() == 0) {
          logger.warn("The collection name has not been specified and there are no collections"
              + " available, the default name Saplo Connection will be used");
          collection_name = "Saplo Connection";
        }
        collection = new SaploCollection(collection_name, Language.en);
        // Save the created collection in the API using the manager
        collectionMgr.create(collection);
      }

      // if there are previous collection
      else {
        // pick the one with the specified name
        if (collection_name != null && collection_name.length() > 0) {
          for (final SaploCollection coll : collectionList) {
            if (coll.getName().equals(collection_name)) {
              collection = coll;
            }
          }
        }
        // if there is no with specified name
        if (collection == null) {

          // create one with the specified name
          if (collection_name != null && collection_name.length() > 0) {
            collection = new SaploCollection(collection_name, Language.en);
            // Save the created collection in the API using the manager

            try {
              collectionMgr.create(collection);
            } catch (final Exception e) {
              throw new AnnotatorException("Too many collections created for Saplo service, try"
                  + " to specify in the Configuration file the name of an already existing collection"
                  + " or leave this field empty", e, logger);
            }
          }
          // or pick the first one if no name is specified
          else {
            collection = collectionList.iterator().next();
          }
        }
      }
      // Create a manager to work with text
      final SaploTextManager textMgr = new SaploTextManager(client);
      // Then create a text object in your collection
      final SaploText saplo_text = new SaploText(collection, text);

      // get additional parameters
      final String text_author = getConfig().getSpecificParameters().get("author");
      final String text_headline = getConfig().getSpecificParameters().get("headline");
      if (text_author != null) {
        saplo_text.setAuthors(text_author);
      }
      if (text_headline != null) {
        saplo_text.setHeadline(text_headline);
      }
      boolean skip_categorization = false;
      final String skip_cat_param = getConfig().getSpecificParameters().get("skip_categorization");
      if (skip_cat_param != null) {
        skip_categorization = Boolean.parseBoolean(skip_cat_param);
      }

      int wait = 0;
      final String wait_param = getConfig().getSpecificParameters().get("wait");
      if (wait_param != null) {
        wait = Integer.parseInt(wait_param);
      }
      if (timeout != 0 || wait_param == null) {
        wait = timeout / 1000;
      }

      // Then save your text using the manager
      textMgr.create(saplo_text);

      final JSONObject json_response = textMgr.invokeJsonService(saplo_text, wait, skip_categorization);

      response = json_response.toString();
    }

    // check if the timeout has expired
    catch (final SaploClientException e) {
      logger.warn("The was a problem trying to submit to the {} web service", getAnnotatorName(), e);
      return null;
    }

    catch (final Exception e) {
      logger.error("Error submitting text to Saplo");
      throw new AnnotatorException("Error submitting text to Saplo", e, logger);
    }

    return response;
  }

  /**
   * Create the annotations from the Saplo JSon annotation object
   * 
   * @param source_text
   *          the original annotated text
   * @param json_annotation
   *          the json object representing one annotation
   * @param progressive
   *          the incremental id counter
   * @return the instance of the annotation found
   */

  private Set<Annotation> buildAnnotationFromJson(final String source_text, final JsonNode json_annotation,
      final long progressive) {

    final Set<Annotation> annotations_found = new HashSet<Annotation>();

    // retrieve all the information for the annotation instance

    String annotator_term = null;
    String tagged_text = null;
    try {
      // the annotator type
      annotator_term = SaploTag.TagCategory.valueOf(json_annotation.getStringValue("category").toUpperCase())
          .toString();

      tagged_text = json_annotation.getStringValue("tag");

    } catch (final Exception e) {
      logger.warn("There is no element tag and/or category in the Json response", e);
      return annotations_found;
    }

    if (tagged_text == null)
      return annotations_found;

    // get start-end for every instance of the given annotation
    final Map<Integer, Integer> instances = Offset.getInstancePosition(-1, source_text, tagged_text);

    // if the system is not able to identify instances over the text just
    // skip the annotation
    if (instances == null) {
      logger.debug("The system is not able to find the instances of {} annotated with type {}", tagged_text,
          annotator_term);
      return annotations_found;

    }

    // to count the annotation
    int count = 0;

    long start;
    long end;

    // confidence given by the annotator, if not specified leave 0 as default value
    double confidence_double = 0.0;
    try {
      confidence_double = Double.parseDouble(json_annotation.getNumberValue("relevance"));
    } catch (final Exception e) {
      logger.warn("There is no element relevance in the Json response", e);
    }

    // iterate over all instances and create a fact for every instance
    for (final int key : instances.keySet()) {
      // retrieve all the information for the annotation instance and create the annotation

      // id of annotation: annotatorId_counter
      final String annotation_istance_id = getAnnotatorName() + "_" + (count + progressive);
      start = key;
      end = instances.get(key);

      final Annotation annotation = new EntityAnnotation();
      annotation.setId(annotation_istance_id);
      annotation.setAnnotationClass(AnnotationClass.INSTANCE);
      annotation.setConcept(annotator_term);
      annotation.setStart(start);
      annotation.setEnd(end);
      annotation.setOriginAnnotator(getAnnotatorName());
      annotation.setConfidence(confidence_double);

      annotations_found.add(annotation);

      // increment the id
      count++;
    }

    // return the annotations number saved
    return annotations_found;
  }

  public void resetId() {
    id_counter = 0;
  }

}
