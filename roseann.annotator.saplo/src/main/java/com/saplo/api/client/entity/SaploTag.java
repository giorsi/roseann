/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package com.saplo.api.client.entity;

import org.json.JSONObject;

import com.saplo.api.client.util.ClientUtil;

/**
 * A Saplo SaploTag representation
 * 
 * @author progre55
 */
public class SaploTag {
	
	public enum TagCategory {
		PERSON, ORGANIZATION, LOCATION, UNKNOWN, URL
	}
	
	private String tagWord;
	private TagCategory category;
	private double relevance;
	
	/**
	 * An empty constructor
	 */
	public SaploTag() {
		tagWord = ClientUtil.NULL_STRING;
		category = TagCategory.UNKNOWN;
		relevance = 0;
	}

	/**
	 * @return the tagWord
	 */
	public String getTagWord() {
		return tagWord;
	}

	/**
	 * @param tagWord the tagWord to set
	 */
	public void setTagWord(String tagWord) {
		this.tagWord = tagWord;
	}

	/**
	 * @return the category
	 */
	public TagCategory getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(TagCategory category) {
		this.category = category;
	}

	/**
	 * @return the relevance
	 */
	public double getRelevance() {
		return relevance;
	}

	/**
	 * @param relevance the relevance to set
	 */
	public void setRelevance(double relevance) {
		this.relevance = relevance;
	}
	
	/**
	 * Convert a given {@link JSONObject} object to a {@link SaploTag} object
	 * 
	 * @param json - the {@link JSONObject} to convert
	 * @return tag - the {@link SaploTag} representation of the json object
	 */
	public static SaploTag convertFromJSONToTag(JSONObject json) {
		SaploTag saploTag = new SaploTag();
		
		if(json.has("tag"))
			saploTag.setTagWord(json.optString("tag"));
		if(json.has("category"))
			saploTag.setCategory(SaploTag.TagCategory.valueOf((json.optString("category").toUpperCase())));
		if(json.has("relevance"))
			saploTag.setRelevance(json.optDouble("relevance"));

		return saploTag;
	}
}
