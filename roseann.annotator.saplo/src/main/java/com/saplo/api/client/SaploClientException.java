/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */package com.saplo.api.client;

import java.util.IllegalFormatException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author progre55
 *
 */
public class SaploClientException extends Exception {

	private static final long serialVersionUID = -5262362627936830619L;
	private static Logger logger = LoggerFactory.getLogger(SaploClientException.class);
	
	protected int errorCode;
	protected Throwable cause;
	
	/**
	 * @param message - API-Exception message
	 */
	public SaploClientException(String message) {
		super(message);
	}
	
	/**
	 * @param code - API-Exception code
	 */
	public SaploClientException(int code) {
		super();
		this.errorCode = code;
	}

	/**
	 * @param message - API-Exception message
	 * @param code - API-Exception code
	 */
	public SaploClientException(String message, int code) {
		super(message);
		this.errorCode = code;
	}
	
	public SaploClientException(String message, int errorCode, String... args) {
		super(format(message, args));
		this.errorCode = errorCode;
	}

	public SaploClientException(String message, int errorCode, Integer... args) {
		super(format(message, args));
		this.errorCode = errorCode;
	}

	/**
	 * 
	 * @param message - API-Exception message
	 * @param code - API-Exception code
	 * @param t - API-Exception cause
	 */
	public SaploClientException(String message, int code, Throwable t) {
		super(message);
		this.cause = t;
		this.errorCode = code;
	}

	/**
	 * 
	 * @param code - Client exception code
	 * @param t - cause
	 */
	public SaploClientException(int code, Throwable t) {
		super(t.getMessage());
		this.cause = t;
		this.errorCode = code;
	}
	
	/**
	 * @param t - API-Exception cause
	 */
	public SaploClientException(Throwable t) {
		super(t.getMessage());
		this.cause = t;
		this.errorCode = -1;
	}

	/**
	 * Get the API-Exception code
	 * @return errorCode 
	 */
	public int getErrorCode() {
		return errorCode;
	}

	/**
	 * {@see super#getCause()}
	 */
	public Throwable getCause() {
		return this.cause;
	}

	private static String format(String message, Integer... fields) {
		String msg = "";
		try {
			msg = String.format(message, (Object[]) fields);
		} catch (IllegalFormatException ife) {
			logger.error("Format Exception (msg: " + message + ", flds: "
					+ fields.length + "): " + ife.getMessage());
			msg = message;
		}
		return msg;
	}
	
	private static String format(String message, String... fields) {
		String msg = "";
		try {
			msg = String.format(message, (Object[]) fields);
		} catch (IllegalFormatException ife) {
			logger.error("Format Exception (msg: " + message + ", flds: "
					+ fields.length + "): " + ife.getMessage());
			msg = message;
		}
		return msg;
	}

}
