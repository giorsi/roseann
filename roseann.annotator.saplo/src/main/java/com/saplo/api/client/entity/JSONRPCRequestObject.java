/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */package com.saplo.api.client.entity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A wrapper for JSON-RPC v2.0 JSONRPC-Request
 * as specified here http://groups.google.com/group/json-rpc/web/json-rpc-2-0
 * 
 * @author progre55
 */
public class JSONRPCRequestObject {
	
	private Integer id;
	private String method;
	private Object params;
	private final String version = "2.0";
	
	public JSONRPCRequestObject(Integer id) {
		this.id = id;
	}
	
	public JSONRPCRequestObject(Integer id, String method) {
		this.id = id;
		this.method = method;
	}
	
	public JSONRPCRequestObject(Integer id, String method, JSONObject params) {
		this.id = id;
		this.method = method;
		this.params = params;
	}

	public JSONRPCRequestObject(Integer id, String method, JSONArray params) {
		this.id = id;
		this.method = method;
		this.params = params;
	}

	/**
	 * Constructs a JSONObject from the given wrapper and returns
	 * 
	 * @return the JSONObject
	 */
	public JSONObject getJSONObject() {
		JSONObject object = new JSONObject();
		
		try {
			object.put("jsonrpc", version);
			object.put("method", method);
			object.put("params", params);
			object.put("id", id);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return object;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * @param method the method to set
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * @return the params
	 */
	public Object getParams() {
		return params;
	}

	/**
	 * Set params of type JSONObject
	 * 
	 * @param params - JSONObject params
	 */
	public void setParams(JSONObject params) {
		this.params = params;
	}
	
	/**
	 * Set params of type JSONArray
	 * 
	 * @param params - JSONArray params
	 */
	public void setParams(JSONArray params) {
		this.params = params;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getJSONObject().toString();
	}

	/**
	 * toString() with indentation
	 *  
	 * @param indent
	 * @return 
	 */
	public String toString(int indent) {
		try {
			return this.getJSONObject().toString(indent);
		} catch (JSONException e) {
			e.printStackTrace();
			return toString();
		}
	}
}
