/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package com.saplo.api.client.entity;

import static com.saplo.api.client.ResponseCodes.CODE_STILL_PROCESSING;
import static com.saplo.api.client.ResponseCodes.CODE_UNKNOWN_EXCEPTION;
import static com.saplo.api.client.ResponseCodes.MSG_STILL_PROCESSING;
import static com.saplo.api.client.ResponseCodes.MSG_UNKNOWN_EXCEPTION;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.saplo.api.client.SaploClientException;

/**
 * A class to wrap around {@link Future} objects and convert {@link ExecutionException}s to {@link SaploClientException}
 * 
 * @author progre55
 */
public class SaploFuture<V> {

	private Future<V> future;
	
	public SaploFuture(Future<V> future) {
		this.future = future;
	}
	
	/**
	 * @see Future#get()
	 * 
	 * @return
	 * @throws SaploClientException - converts the {@link Future#get()} exceptions into a SaploClientException
	 */
	public V get() throws SaploClientException {
		try {
			return future.get();
		} catch (ExecutionException ee) {
			if(ee.getCause() instanceof SaploClientException)
				throw (SaploClientException)ee.getCause();
			else
				throw new SaploClientException(MSG_UNKNOWN_EXCEPTION, CODE_UNKNOWN_EXCEPTION, ee);
		} catch (InterruptedException ie) {
			throw new SaploClientException(MSG_UNKNOWN_EXCEPTION, CODE_UNKNOWN_EXCEPTION, ie);
		}
	}
	
	/**
	 * @see Future#get(long, TimeUnit)
	 * 
	 * @param timeout
	 * @param unit
	 * @return
	 * @throws SaploClientException - converts the {@link Future#get(long, TimeUnit)} exceptions into a SaploClientException
	 */
	public V get(long timeout, TimeUnit unit) throws SaploClientException {
		try {
			return future.get(timeout, unit);
		} catch (ExecutionException ee) {
			if(ee.getCause() instanceof SaploClientException)
				throw (SaploClientException)ee.getCause();
			else
				throw new SaploClientException(MSG_UNKNOWN_EXCEPTION, CODE_UNKNOWN_EXCEPTION, ee);
		} catch (InterruptedException ie) {
			throw new SaploClientException(MSG_UNKNOWN_EXCEPTION, CODE_UNKNOWN_EXCEPTION, ie);
		} catch (TimeoutException te) {
			throw new SaploClientException(MSG_STILL_PROCESSING, CODE_STILL_PROCESSING, te);
		}
	}
	
	/**
	 * @see Future#cancel(boolean)
	 * @param mayInterruptIfRunning
	 */
	public void cancel(boolean mayInterruptIfRunning) {
		future.cancel(mayInterruptIfRunning);
	}
	
	/**
	 * @see Future#isCancelled()
	 * @return
	 */
	public boolean isCancelled() {
		return future.isCancelled();
	}

	/**
	 * @see Future#isDone()
	 * @return
	 */
	public boolean isDone() {
		return future.isDone();
	}
	
	
}
