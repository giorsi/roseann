/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package com.saplo.api.client.manager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.saplo.api.client.ResponseCodes;
import com.saplo.api.client.SaploClient;
import com.saplo.api.client.SaploClientException;
import com.saplo.api.client.entity.JSONRPCRequestObject;

/**
 * A manager class for Authorization methods
 * 
 * @author progre55
 *
 */
public class SaploAuthManager {

	private SaploClient client;
	
	public SaploAuthManager(SaploClient clientToUse) {
		this.client = clientToUse;
	}
	
	public String accessToken(String apiKey, String secretKey) throws SaploClientException {
		
		String accessToken;
		JSONObject params = new JSONObject();
		try {
		params.put("api_key", apiKey);
		params.put("secret_key", secretKey);
		} catch(JSONException je) {
			throw new SaploClientException(ResponseCodes.CODE_JSON_EXCEPTION, je);
		}
		
		JSONRPCRequestObject message = new JSONRPCRequestObject(client.getNextId(), "auth.accessToken", params);
		Object rawResult = client.sendAndReceiveAndParseResponse(message);
		
//		Object rawResult = client.parseResponse(responseMessage);
//		Object rawResult = client.sendAndReceiveAndParseResponse(message);
		if(rawResult instanceof JSONObject)
			accessToken = ((JSONObject) rawResult).opt("access_token").toString();
		else
			accessToken = rawResult.toString();
		
		return accessToken;
	}
	
	public boolean invalidateToken() throws SaploClientException {
		JSONArray params = new JSONArray();

		client.sendAndReceive(new JSONRPCRequestObject(client.getNextId(), "auth.invalidateToken", params));
		
		return true;
	}
}
