/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */package com.saplo.api.client.entity;

import org.json.JSONObject;

import com.saplo.api.client.util.ClientUtil;

/**
 * @author progre55
 *
 */
public class SaploCollection {

	public enum Language {en, sv}
	
	public enum Permission {read, write, none}
	
	private int id = ClientUtil.NULL_INT;
	private String name = ClientUtil.NULL_STRING; // required
	private String description = ClientUtil.NULL_STRING;;
	private Language language; // required
	private Permission permission;
	private int nextId = ClientUtil.NULL_INT;
	
	/**
	 * An empty constructor. 
	 * Make sure you set the {@link #setName(String)} 
	 * and {@link #setLanguage(Language)} parameters, as they are required.
	 *  
	 */
	public SaploCollection() {
		this.language = null;
	}
	
	/**
	 * A constructor with the collection Id
	 * 
	 * @param id
	 */
	public SaploCollection(int id) {
		this.id = id;
	}
	
	/**
	 * A constructor with required parameters
	 * 
	 * @param collectionName
	 * @param lang
	 */
	public SaploCollection(String collectionName, Language lang) {
		this.name = collectionName;
		this.language = lang;
	}
		
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the language
	 */
	public Language getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(Language language) {
		this.language = language;
	}

	/**
	 * @return the permission
	 */
	public Permission getPermission() {
		return permission;
	}

	/**
	 * @param permission the permission to set
	 */
	public void setPermission(Permission permission) {
		this.permission = permission;
	}
	
	/**
	 * @return the nextId
	 */
	public int getNextId() {
		return nextId;
	}

	/**
	 * @param nextId the nextId to set
	 */
	public void setNextId(int nextId) {
		this.nextId = nextId;
	}

	/**
	 * Update a given {@link SaploCollection} object with the given {@link JSONObject} object
	 * 
	 * @param json - the {@link JSONObject} to parse
	 * @param saploCollection - the {@link SaploCollection} object to write the conversion results to
	 */
	public static void convertFromJSONToCollection(JSONObject json, SaploCollection saploCollection) {
		
		if(json.has("collection_id"))
			saploCollection.setId(json.optInt("collection_id"));
		if(json.has("name"))
			saploCollection.setName(json.optString("name"));
		if(json.has("language"))
			saploCollection.setLanguage(SaploCollection.Language.valueOf(json.optString("language")));
		if(json.has("description"))
			saploCollection.setDescription(json.optString("description"));
		if(json.has("permission"))
			saploCollection.setPermission(SaploCollection.Permission.valueOf(json.optString("permission")));
		if(json.has("next_id"))
			saploCollection.setNextId(json.optInt("next_id"));
	}
	
	/**
	 * Convert a given {@link JSONObject} object to a {@link SaploCollection} object
	 * 
	 * @param json - the {@link JSONObject} to convert
	 * @return collection - the {@link SaploCollection} representation of the json object
	 */
	public static SaploCollection convertFromJSONToCollection(JSONObject json)  {
		SaploCollection saploCollection = new SaploCollection();
		convertFromJSONToCollection(json, saploCollection);
		return saploCollection;
	}
}
