/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */package com.saplo.api.client.entity;

import org.json.JSONObject;

/**
 * A wrapper for JSON-RPC v2.0 JSONRPC-Response
 * as specified here http://groups.google.com/group/json-rpc/web/json-rpc-2-0
 * 
 * @author progre55
 */
public class JSONRPCResponseObject {

	private Integer id;
	private Object result;
	private JSONRPCErrorObject error;
	private String version;
	private boolean success = false;
	private String rawMessage;
	
	/**
	 * A constructor that takes in a JSON-RPC response and parses the fields
	 * 
	 * @param response - a JSON-RPC response object, MUST have a "result"
	 *  parameter on SUCCESS or "error" on FAILURE
	 */
	public JSONRPCResponseObject(JSONObject response) {
		if(response.has("result")) {
			result = response.opt("result");
			success = true;
			if(result == null) {
				error = handleError(response);
				success = false;
			}
		} else {
			error = handleError(response);
			success = false;
		}
		id = response.optInt("id");
		version = response.optString("jsonrpc");
		rawMessage = response.toString();
	}
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the result
	 */
	public Object getResult() {
		return result;
	}

	/**
	 * @return the error
	 */
	public JSONRPCErrorObject getError() {
		return error;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @return the status
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * Returns the raw JSON message received from the server.
	 */
	public String toString() {
		return rawMessage;
	}

	/*
	 * tries to parse an error object in a given response object 
	 */
	private JSONRPCErrorObject handleError(JSONObject response) {
		JSONObject err;
		if(response.has("error")) {
			err = response.optJSONObject("error");
			return new JSONRPCErrorObject(err);
		}
		return null;
	}
}
