/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */package com.saplo.api.client.entity;

import org.json.JSONException;
import org.json.JSONObject;

import com.saplo.api.client.ResponseCodes;
import com.saplo.api.client.SaploClientException;

/**
 * A wrapper for JSON-RPC v2.0 JSONRPC-Error object
 * as specified here http://groups.google.com/group/json-rpc/web/json-rpc-2-0
 * 
 * @author progre55
 *
 */
public class JSONRPCErrorObject {

    private Integer code;
    private String message;
    private Object data;
    private String rawErrorMessage;
    private SaploClientException clientException;

    /**
     * A constructor that takes in an "error" object that 
     * MUST contain an error code of type int
     * 
     * @param error - and error object received from an rpc-response
     */
    public JSONRPCErrorObject(JSONObject error) {
	try {
		code = error.getInt("code");
	} catch (JSONException e) {
		code = ResponseCodes.CODE_MALFORMED_RESPONSE;
	}

	message = error.optString("msg");
	
	if(error.has("data")) {
	    data = error.opt("data");
	    clientException = new SaploClientException(message, code, new Throwable(data.toString()));
	} else {
	    clientException = new SaploClientException(message, code);
	}

	rawErrorMessage = error.toString();



    }

    /**
     * @return the code
     */
    public int getCode() {
	return code;
    }

    /**
     * @return the message
     */
    public String getMessage() {
	return message;
    }

    /**
     * @return the data
     */
    public Object getData() {
	if(data != null)
	    return data.toString();

	return "";
    }

    /**
     * Returns the raw JSON error message received from the server.
     */
    public String toString() {
	return rawErrorMessage;
    }
    
    /**
     * Get the clientException to throw it
     * @return clientException
     */
    public SaploClientException getClientException() {
	return clientException;
    }
}
