/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package com.saplo.api.client.session;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import com.saplo.api.client.ClientError;
import com.saplo.api.client.ClientProxy;
import com.saplo.api.client.session.impl.HTTPSessionApache;

/**
 * A registry of transports serving JSON-RPC-Client
 */
public class TransportRegistry {

	/**
	 * A static class holder for TransportRegistry to hold only
	 * a single instance per class-loader. Faster than having
	 * a synchronized method.
	 * 
	 * @author progre55
	 *
	 */
	private static class TransportRegistryHolder {
		private static final TransportRegistry singleton = new TransportRegistry();
	}

	/**
	 * @param registerDefault
	 *            'true' if to register default transports
	 */
	public TransportRegistry(boolean registerDefault) {
		if (registerDefault) {
			HTTPSessionApache.register(this);
		}
	}

	public TransportRegistry() {
		this(true);
	}

	/**
	 * @return singleton instance of the class, created if necessary.
	 */
	public static TransportRegistry getTransportRegistryInstance() {
		return TransportRegistryHolder.singleton;
	}

	private HashMap<String, SessionFactory> registry = new HashMap<String, SessionFactory>();

	public void registerTransport(String scheme, SessionFactory factory) {
		registry.put(scheme, factory);
	}

	public void deregisterTransport(String scheme) {
		registry.remove(scheme);
	}

	public Session createSession(String uriString, String params, ClientProxy proxy, Map<String, Object> httpParams) {
		try {
			URI uri = new URI(uriString);
			SessionFactory found = registry.get(uri.getScheme());
			if (found == null)
				throw new ClientError("Could not open URI '" + uriString
						+ "'. Unknown scheme - '" + uri.getScheme() + "'." +
				"Make sure you have registered your SessionFactory with this transport.");
			return found.newSession(uri, params, proxy, httpParams);
		} catch (URISyntaxException e) {
			throw new ClientError(e);
		}
	}

	public interface SessionFactory {
		/**
		 * @param uri - URI used to open this session
		 * @param params - jsessionid or access_token param
		 */
		Session newSession(URI uri, String params, ClientProxy proxy, Map<String, Object> httpParams);
	}

}
