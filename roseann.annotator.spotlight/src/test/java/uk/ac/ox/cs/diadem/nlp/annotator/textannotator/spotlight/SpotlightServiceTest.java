/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.spotlight;

import java.util.List;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import junit.framework.Assert;

import org.junit.Test;
import org.xml.sax.SAXException;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;

public class SpotlightServiceTest {

	
	
	/**
	 * Test whether a json response is returned from the service
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws InvalidSyntaxException
	 * @throws XPathExpressionException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	@Test
	public void invokeServiceTest() throws IllegalArgumentException, IOException, InvalidSyntaxException, XPathExpressionException, SAXException, ParserConfigurationException{

		String text="Barack Obama is the new president of United States of America.";

		//initialize the configuration
		AnnotatorConfiguration config=new AnnotatorConfiguration();
		//set the url endpoint
		//config.setUrlendpoint(new URL(endpoint));

		Spotlight service = Spotlight.GetInstanceFromConfig(config);
		String response = service.invokeNEService(text,config,0);

		Assert.assertNotNull(response);
		Assert.assertTrue(response.length()>0);

		//parse the response as json object and verify the presence of some json elements
		JsonRootNode doc = new JdomParser().parse(response);

		List<JsonNode> entities = doc.getArrayNode("Resources");
		Assert.assertNotNull(entities);
		Assert.assertTrue(entities.size()>0);

		
	}

	/**
	 * @throws IOException 
	 */
	@Test
	public void invokeServiceTestWithoutParameter() throws IOException {
		String text="Barack Obama is the new president of United States of America.";

		//initialize the configuration
		AnnotatorConfiguration config=new AnnotatorConfiguration();

		Spotlight service = Spotlight.GetInstanceFromConfig(config);;
		service.invokeNEService(text, config,0);

	}

}
