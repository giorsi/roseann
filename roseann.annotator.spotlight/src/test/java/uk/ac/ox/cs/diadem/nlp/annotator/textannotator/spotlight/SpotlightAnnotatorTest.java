/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.spotlight;

import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;

public class SpotlightAnnotatorTest {
	
	private Annotator spotlight;
	
	private String text;
	
	@Before
	public void bringUp(){
		spotlight=SpotlightAnnotator.getInstance();
		text="Barack Obama is the new president of the United States.";
	}

	/**
	 * Test to check that a text can be submitted to spotlight annotator service
	 * @throws InvalidSyntaxException
	 */
	@Test
	public void testSubmitToAnnotator() throws InvalidSyntaxException{

		//check the KEY has been specified in the configuration
	//	Assert.assertNotNull(spotlight.getConfig().getApiKey());

		//check that the text can be submitted to the annotator and no exceptions are thrown
		String response = spotlight.submitToAnnotator(text);

		Assert.assertNotNull(response);
		Assert.assertTrue(response.length()>0);

		//check the response can be parse as json node and contains an element entities and an element relations
		JsonRootNode doc = new JdomParser().parse(response);

		List<JsonNode> entities = doc.getArrayNode("Resources");
		Assert.assertNotNull(entities);
		Assert.assertTrue(entities.size()>0);

	/*	List<JsonNode> relations = doc.getArrayNode("relations");
		Assert.assertNotNull(relations);
		Assert.assertTrue(relations.size()>0);	*/
	}

	/**
	 * Test to verify the entities retrieved from spotlight web service
	 */
	@Test
	public void testAnnotateEntities(){
		Set<Annotation> annotations = spotlight.annotateEntity(text);
		Assert.assertNotNull(annotations);
		Assert.assertTrue(annotations.size()>0);
	}
	
	/**
	 * Test to verify that no spotlight ares retrieved from spotlight web service with an senseless text
	 */
	@Test
	public void testAnnotateNoEntities(){
		text="cfrgfvdfgbfdgdgdfgdfgdgdg";

		Set<Annotation> annotations =spotlight.annotateEntity(text);
		Assert.assertNotNull(annotations);
		Assert.assertTrue(annotations.size()==0);
	}

}
