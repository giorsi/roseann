/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.spotlight;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author Stefano Ortona (stefano.ortona@gmail.com) Oxford University,
 *         Department of Computer Science
 * 
 *         Use this class to set the Parameters for DBPedia Spotlight
 *         web-service invocation. Default value for confidence and support are
 *         0.0 and 20 By default common words are omitted from the analysis and
 *         the disambiguator process is the document one You can set parameters
 *         either to query the answer with a SPARQL query and type restriction
 * 
 */
class Spotlight_Params {

	public static final String CO_OCCURENCE_SELECTOR = "CoOccurrenceBasedSelector";
	public static final String LING_PIPE = "LingPipeSpotter";
	public static final String WIKI_SPOTTER = "WikiMarkupSpotter";
	public static final String AT_LEAST_ONE = "AtLeastOneNounSelector";
	public static final String NE = "NESpotter";
	public static final String OPEN_NLPL = "OpenNLPNGramSpotter";
	public static final String CHUNKER = "OpenNLPChunkerSpotter";
	public static final String KEA = "KeaSpotter";
	
	public static final String DOCUMENT_DISAMBIGUATOR = "Document";

	private String text;
	private String additionalParameters;
	private Double confidence;
	private Long support;
	private List<String> types_restriction;
	private String query_sparql;
	private String spotter;
	private String disambiguator;
	private Boolean coreferenceResolution;

	public double getConfidence() {
		return this.confidence;
	}

	public void setConfidence(double confidence) {
		if (!(confidence >= 0 && confidence <= 1)) {
			throw new IllegalArgumentException("Invalid setting for confidence: " + confidence + ". It must "
			        + "belong to [0,1]");
		}
		this.confidence = confidence;
	}

	public long getSupport() {
		return this.support;
	}

	public void setSupport(long support) {
		if (support <= 0) {
			throw new IllegalArgumentException("Invalid setting for support: " + support + ". It must "
			        + "be higher than 0");
		}
		this.support = support;

	}

	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<String> getTypesRestriction() {
		return this.types_restriction;
	}

	public void setTypeRestrictions(List<String> types) {
		this.types_restriction = types;
	}

	public String getQuerySparql() {
		return this.query_sparql;
	}

	public void setQuerySparql(String query) {
		this.query_sparql = query;
	}

	public String getDisambiguator() {
		return disambiguator;
	}
	
	public boolean coreferenceResolution(){
		return this.coreferenceResolution;
	}
	
	public void setCoreferenceResolution(final boolean coreferenceResolution){
		this.coreferenceResolution=coreferenceResolution;
	}

	public void setDisambiguator(String disambiguator) {
		if (!disambiguator.equals(DOCUMENT_DISAMBIGUATOR)) {
			throw new IllegalArgumentException("Uknown value for Disambiguator.");
		}
		this.disambiguator = disambiguator;
	}

	public String getSpotter() {
		return disambiguator;
	}

	public void setSpotter(String spotter) {
		if (!spotter.equals(CO_OCCURENCE_SELECTOR)&&!spotter.equals(LING_PIPE)
				&&!spotter.equals(WIKI_SPOTTER)&&!spotter.equals(AT_LEAST_ONE)
				&&!spotter.equals(NE)&&!spotter.equals(OPEN_NLPL)
				&&!spotter.equals(CHUNKER)&&!spotter.equals(KEA)) {
			throw new IllegalArgumentException("Uknown value for Spotter.");
		}
		this.spotter = spotter;
	}

	public String getAdditionalParameters() {
		return this.additionalParameters;
	}

	public void setAdditionalParameters(final Map<String,String> additionalParameters) {
		StringBuilder moreParameters = new StringBuilder();
		try {
			for (String name:additionalParameters.keySet()) {
				moreParameters.append('&').append(name);
				moreParameters.append('=').append(URLEncoder.encode(additionalParameters.get(name), "UTF8"));
			}
		} catch (UnsupportedEncodingException e) {
			this.additionalParameters = "";
			return;
		}
		if(moreParameters.length()>1){
			this.additionalParameters = moreParameters.toString().substring(1);
		}else{
			this.additionalParameters = "";
		}
		
	}

	public String getParameter() {
		String parameters;
		StringBuilder sb = new StringBuilder();
		
		if (this.confidence != null) {
			sb.append("&confidence=" + this.confidence);
		}
		if (this.support != null) {
			sb.append("&support=" + this.support);
		}
		if (this.coreferenceResolution != null) {
			sb.append("&coreferenceResolution=" + this.coreferenceResolution);
		}
		if (this.spotter != null) {
			sb.append("&spotter=" + this.spotter);
		}
		if (this.disambiguator != null) {
			sb.append("&disambiguator=" + this.disambiguator);
		}
		if (this.types_restriction != null) {
			sb.append("&types=" + this.types_restriction.get(0));
			for (int i = 1; i < this.types_restriction.size(); i++) {
				sb.append("," + this.types_restriction.get(i));
			}
		}
		try {
			if (this.query_sparql != null)
				sb.append("&sparql=" + URLEncoder.encode(this.query_sparql, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new IllegalArgumentException("Query SPARQL can't be encoded as UTF-8.");
		}
		if (additionalParameters != null)
			sb.append("&"+additionalParameters);
		try {
			if (this.text != null)
				sb.append("&text=" + URLEncoder.encode(text, "UTF-8"));
		} catch (Exception e) {
			throw new IllegalArgumentException("Text can't be encoded as UTF-8.");
		}
		parameters = sb.toString().substring(1);
		return parameters;
	}
}
