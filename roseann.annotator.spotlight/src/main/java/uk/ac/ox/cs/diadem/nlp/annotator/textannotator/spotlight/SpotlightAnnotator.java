/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.spotlight;

import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonNodeDoesNotMatchJsonNodeSelectorException;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.AnnotationAttribute;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.util.QueryDbpediaEndpoint;

/**
 * @author Stefano Ortona , Luying Chen The wrapper submit the text to DBpedia
 *         Spotlight, take the response as String representing JSon Object,
 *         builds a JSon Object and select only the entity annotations to be
 *         saved in the model.
 */
public class SpotlightAnnotator extends AnnotatorAdapter {

  static final Logger logger = LoggerFactory.getLogger(SpotlightAnnotator.class);

  private Spotlight spotlight;
  /**
   * counter to increment the annotation id
   */
  private long id_counter;

  /**
   * Map to associate to every dbpedia resource link a list of dbpedia ontology
   * types
   */
  private Map<String, List<String>> resource2types;

  /**
   * singleton instance
   */
  private static SpotlightAnnotator INSTANCE;

  /**
   * private constructor to build the singleton instance
   */
  private SpotlightAnnotator() {
    ConfigurationFacility.getConfiguration();
    annotatorName = "dbpediaSpotlight";

    // start the counter from 0
    id_counter = 0;
    // initialize the parametrs
    final AnnotatorConfiguration configuration = new AnnotatorConfiguration();
    configuration.initializeConfiguration(getAnnotatorName());
    setConfig(configuration);
  }

  /**
   * static method to get the singleton instance
   *
   * @return the singleton instance
   */
  public static synchronized Annotator getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new SpotlightAnnotator();
    }
    return INSTANCE;
  }

  /*
   * (non-Javadoc)
   *
   * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.
   * AnnotatorAdapter
   * #setConfig(uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.
   * AnnotatorConfiguration)
   */
  @Override
  public void setConfig(final AnnotatorConfiguration config) {
    // TODO Auto-generated method stub
    super.setConfig(config);
    // Instantiate the alchemyAPI with the new configuration

    spotlight = Spotlight.GetInstanceFromConfig(config);

  }

  /*
   * (non-Javadoc)
   *
   * @see
   * uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator#
   * submitToAnnotator(java.lang.String)
   */
  @Override
  public String submitToAnnotator(final String freeText) {

    return this.submitToAnnotator(freeText, 0);

  }

  @Override
  public String submitToAnnotator(String freeText, final int timeout) {

    if ((freeText == null) || (freeText.length() < 1)) {
      logger.error("Trying to submit an empty text to DBpedia Spotlight");
      throw new AnnotatorException("Trying to submit an empty text to DBpedia Spotlight", logger);
    }

    String res = null;

    try {

      logger.debug("Submitting document to DBpedia Spotlight service.");

      // eliminate from the original text special characters
      freeText = freeText.replaceAll("\u0003", " ");

      // invoke the service and set JSon as response
      res = spotlight.invokeNEService(freeText, config, timeout);

    } // check if the timeout has expired
    catch (final SocketTimeoutException e) {
      logger.warn("{} was not able to receive the web response within timeout {} milliseconds", getAnnotatorName(),
          timeout);
      return null;
    } catch (final Exception e) {
      logger.error("Error submitting text to DBpedia Spotlight");
      throw new AnnotatorException("Error submitting text to DBpedia Spotlight", e, logger);

    }
    logger.debug("DBPedia Spotlight Process done and response received.");
    return res;

  }

  /*
   * (non-Javadoc)
   *
   * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.
   * AnnotatorAdapter#annotateEntity(java.lang.String)
   */
  @Override
  public Set<Annotation> annotateEntity(final String text) {
    final String response = this.submitToAnnotator(text);

    // save the annotations and increment the id counter
    final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(response, id_counter, false);
    final int num_annotations = entityAnnotations.size();
    id_counter += num_annotations;
    return entityAnnotations;
  }

  @Override
  public Set<Annotation> annotateEntity(final String text, final int timeout) {
    final String response = this.submitToAnnotator(text, timeout);
    if (response == null)
      return new HashSet<Annotation>();

    // save the annotations and increment the id counter
    final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(response, id_counter, false);
    final int num_annotations = entityAnnotations.size();
    id_counter += num_annotations;
    return entityAnnotations;
  }

  /**
   * An utility method to harvest entity annotations from annotator response
   *
   * @param response
   *          Response to be interpreted
   * @param id_counter
   * @param needDeepest
   *          The flag determines whether to retrieve the most specific entity
   * @return Set<Annotation> Set of entity annotations
   */
  public Set<Annotation> retrieveEntityAnnotations(final String response, final long id_counter,
      final boolean needDeepest) {

    JsonRootNode doc;

    logger.debug("Processing the DBpedia Spotlight response.");
    try {
      // create a JSon node parsing the text response
      doc = new JdomParser().parse(response);
    } catch (final InvalidSyntaxException e) {
      logger.error("The String response from DBpedia Spotlight can't be parse as JSonNode");
      throw new AnnotatorException("The String response from DBpedia Spotlight " + "can't be parse as JSonNode", e,
          logger);
    }
    final Set<Annotation> entityAnnotation = new HashSet<Annotation>();
    // select only the entity annotations
    final List<JsonNode> selected_annotations = selectAnnotations(doc, "Resources");

    if (selected_annotations == null) {
      logger.debug("DBpedia Sptolight service did not find any entity annotations.");
      return entityAnnotation;
    }

    // to count the annotation
    int count = 0;

    for (final JsonNode annotation : selected_annotations) {
      final Set<Annotation> subSet = buildAnnotationFromJson(annotation, count + id_counter, annotatorName,
          needDeepest);
      if (subSet != null) {
        count += subSet.size();
        entityAnnotation.addAll(subSet);
      }

    }
    logger.debug("Response processed and annotations saved.");

    // reset the context
    resetContext();

    return entityAnnotation;
  }

  /**
   * Select in the annotated document only the annotation of a certain type
   *
   * @param root
   *          the JSon node representing the annotated document
   * @param type
   *          type of the annotations to be selected
   * @return a list of selected annotations represented as JSon node
   */
  private List<JsonNode> selectAnnotations(final JsonNode root, final String type) {
    List<JsonNode> selected_annotations = new LinkedList<JsonNode>();
    try {
      selected_annotations = root.getArrayNode(type);
    } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
      // if there are no annotation of a certain type return null
      return null;

    }
    return selected_annotations.size() > 0 ? selected_annotations : null;
  }

  /**
   * Construct annotation for every instance of the annotation, one fact for
   * every attribute of the instance Currently DBpedia Spotlight doesn't provide
   * any attribute for the annotation
   *
   * @param annotation
   *          JSon object representing the instance of annotation
   * @param annotator_id
   *          the annotator id
   * @param annotation_id
   *          Annotation_id for the given annotation
   * @param needDeepest
   *          The flag determines whether to retrieve the most specific entity
   */

  private Set<Annotation> buildAnnotationFromJson(final JsonNode annotation, final long annotation_id,
      final String annotator_id, final boolean needDeepest) {
    final Set<Annotation> annotations_found = new HashSet<Annotation>();
    // retrieve all the information for the annotation

    // the annotator type
    final String annotator_resource = annotation.getStringValue("@URI");

    // retrieve all the types for that resource
    List<String> types = null;
    // if (resource2types != null) {
    // types = this.resource2types.get(annotator_resource);
    // }

    try {
      final String annotation_types = annotation.getStringValue("@types");
      types = getTypes(annotation_types, needDeepest);
    } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
      // if not able to find a field types, just continue
      logger.debug("The annotation resource {} doesn't contatin a filed @types", annotator_resource);
    }

    // if there are no dbpedia types for a certain annotations, just mark it
    // as thing
    if ((types == null) || (types.size() == 0)) {
      types = new LinkedList<String>();
      types.add("Thing");
    }

    // to count the annotation
    long progressive = 0;

    long start;
    long end;
    final String annotator_name = annotatorName;

    // confidence given by the annotator
    final Double confidence = Double.parseDouble(annotation.getStringValue("@support"));

    // source for the main type is DBPedia, but can change for the subtypes
    final String source = "dbpedia";

    // start-end for the annotation
    start = Long.parseLong(annotation.getStringValue("@offset"));

    try {
      end = start + annotation.getStringValue("@surfaceForm").length();
    } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {

      // if not able to find the piece of text annotated is beacuse
      // Spotlight found a null value
      end = start + "null".length();
    }

    // retrieve all the attributes and create a fact for every attribute
    final Map<String, Set<String>> attribute2value = getAttributes(annotation);

    // create a new fact for every instance annotation, for every type

    for (final String type : types) {

      // transform the concept dlv compatible
      // type = AnnotationUtil.modifyConcept(type);

      progressive++;
      // id of annotation: annotatorId_annotationId
      final String id = annotator_id + "_" + (annotation_id + progressive);

      // create the new entity annotation
      final Annotation oneAnnotation = new EntityAnnotation(id, type, start, end, annotator_name, source, confidence,
          AnnotationClass.INSTANCE);
      annotations_found.add(oneAnnotation);
      // create a fact for every attribute
      for (final String attribute : attribute2value.keySet()) {
        String attrVal = "";
        final int index = 0;
        for (final String value : attribute2value.get(attribute))
          if (index > 1) {
            attrVal += "," + value;
          } else {
            attrVal += value;
          }
        final AnnotationAttribute oneattribute = new AnnotationAttribute(attribute, attrVal);
        oneAnnotation.addAttribute(oneattribute);

      }
    }

    return annotations_found;

  }

  /**
   * Utility method to get all the dbpedia deepest types for a given annotation
   * Currently the attribute '@types' of Spotlight response contains a string
   * with following pattern: [source:type,]*, where the type is a deepest type
   * with all its superclasses The method retrieve only the deepest type
   *
   * @param subTypes
   *          string containing source and subtypes for a given annotation
   * @return map where key is a source, value a list of all subtypes found by
   *         that source
   */
  private List<String> getTypes(final String subTypes, final boolean needDeepest) {

    final List<String> types = new LinkedList<String>();

    final String[] all_types = subTypes.split(",");

    for (final String type : all_types) {
      if (type.contains("DBpedia") && !type.contains("TopicalConcept")) {

        // particular case if the dbpedia type is thing
        if (type.equals("DBpedia:Http://www.w3.org/2002/07/owl#Thing")) {
          types.add("Thing");
        } else {
          types.add(type.split(":")[1]);
        }
      }

    }
    if (!needDeepest)
      return types;

    try {
      if (types.size() > 1) {
        // retrieve all the supertypes querying the dbpedia Ontology
        final Set<String> supertypes = new HashSet<String>();
        for (final String type : types) {
          supertypes.addAll(QueryDbpediaEndpoint.getSupertypes(type));
        }

        // remove the supertypes from the type
        types.removeAll(supertypes);
      }
    } catch (final Exception e) {
      logger.error(
          "Error trying to query the dbpedia SPARQL endpoint to retrieve all the supertypes for the annotated types");
    }

    return types.size() > 0 ? types : null;
  }

  /**
   * Utility method to retrieve all the attributes for a given annotation
   * Currently Spotlight provides only 2 attributes, similarityScore and
   * percentageOfSecondRank
   *
   * @param annotation
   *          JsonNode representing the annotation
   * @return map where key is the name of the attribute, value is the value of
   *         the attribute
   */
  private Map<String, Set<String>> getAttributes(final JsonNode annotation) {
    final Map<String, Set<String>> attribute2value = new HashMap<String, Set<String>>();
    String value = null;
    Set<String> values = new HashSet<String>();
    try {
      value = annotation.getStringValue("@percentageOfSecondRank");
      if ((value != null) && (value.length() > 0)) {
        values.add(value);
        attribute2value.put("percentageOfSecondRank", values);
        values = new HashSet<String>();
      }
      value = annotation.getStringValue("@similarityScore");
      if ((value != null) && (value.length() > 0)) {
        values.add(value);
        attribute2value.put("similarityScore", values);
        values = new HashSet<String>();
      }

      // get the link to dbpedia and freebase
      value = annotation.getStringValue("@types");

      for (final String link : value.split(",")) {
        if (link.contains("Freebase")) {
          values.add("http://www.freebase.com" + link.replace("Freebase:", ""));
        }
        if (link.contains("DBpedia")) {
          values.add("http://mappings.dbpedia.org/server/ontology/classes/" + link.replace("DBpedia:", ""));
        }

      }
      attribute2value.put("class_uri", values);
    } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
      // if not able to find the attribute, simply continue
    }

    return attribute2value.size() > 0 ? attribute2value : null;
  }

  private void resetContext() {
    // TODO Auto-generated method stub
    if (resource2types != null) {
      resource2types.clear();
    }

  }

  public void resetId() {
    id_counter = 0;
  }
}