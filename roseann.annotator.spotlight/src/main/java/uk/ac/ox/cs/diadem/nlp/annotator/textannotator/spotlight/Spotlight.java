/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.spotlight;

import java.io.*;
import java.net.*;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;


/**
 * 
 * @author Stefano Ortona 
 * @author Luying Chen
 * 
 *         Use this class to send a request to DBpedia Spotlight web service and
 *         get a response as a string. You can set the response format,
 *         currently supported are json, html, xml and rdf. There are 3 kind of
 *         services: annotate, disambiguate and get best candidates
 * 
 */
class Spotlight {

	private String _requestUri = "http://spotlight.dbpedia.org/rest/";

	/**
	 * @return the _requestUri
	 */
	 String get_requestUri() {
		return _requestUri;
	}

	/**
	 * @param _requestUri the _requestUri to set
	 */
	void set_requestUri(String _requestUri) {
		this._requestUri = _requestUri;
	}
	public final static String JSON_OUTPUT = "application/json";
	public final static String HTML_OUTPUT = "text/html";
	public final static String XML_OUTPUT = "text/xml";
	public final static String RDF_OUTPUT = "application/xhtml+xml";

	
	private String getAnnotatedText(final String text, final Spotlight_Params parameters, final String response,int timeout)
	        throws IOException {

		CheckText(text);

		parameters.setText(text);
		return POST(parameters, "annotate", response,timeout);
	}

	
	private void CheckText(String text) {
		if (null == text || text.length() < 2)
			throw new IllegalArgumentException("Enter some text to analyze.");
	}

	private String POST(final Spotlight_Params parameters, final String service, final String response,int timeout)
	        throws IOException {

		URL url = new URL(_requestUri + service);

		HttpURLConnection handle = (HttpURLConnection) url.openConnection();
		handle.setDoOutput(true);
		handle.setConnectTimeout(timeout);

		handle.setRequestMethod("POST");

		if (response != null && !response.equals(JSON_OUTPUT) && !response.equals(XML_OUTPUT)
		        && !response.equals(HTML_OUTPUT) && !response.equals(RDF_OUTPUT)) {
			throw new IllegalArgumentException("The output format specified is not supported.");
		}
		if (response != null) {
			handle.addRequestProperty("Accept", response);
		}
		
		final String request_parameters=parameters.getParameter();
		handle.addRequestProperty("content-type", "application/x-www-form-urlencoded");
		handle.addRequestProperty("Content-Length", Integer.toString(request_parameters.length()));

		DataOutputStream outputstream = new DataOutputStream(handle.getOutputStream());
		outputstream.write(request_parameters.getBytes());
		outputstream.close();

		return doRequest(handle);
	}

	private String doRequest(HttpURLConnection handle) throws IOException {
		DataInputStream istream = new DataInputStream(handle.getInputStream());

		StringBuilder sb = new StringBuilder();
		BufferedReader br = new BufferedReader(new InputStreamReader(istream));

		String line;
		while ((line = br.readLine()) != null) {
			sb.append(line);
		}
		istream.close();

		br.close();
		handle.disconnect();

		return sb.toString();
	}
	/**
	 * Get Spotlight instance from configuration object(No key is required for this service).
	 * @param config
	 * 			Input configuration object of this Spotlight instance
	 * @return Spotlight
	 * 			The Spotlight instance with the specific configuration
	 */
	static public Spotlight GetInstanceFromConfig(AnnotatorConfiguration config) {
		Spotlight api = new Spotlight();
		final URL url = config.getURlendpoint();
		if(url!=null){
			api.set_requestUri(url.toString());
		}

		return api;
	}
	/**
	 * Utility method to invoke NE service and return the response string.
	 * @param text
	 * 			The source text to be annotated
	 * @param config
	 * 			The input configration object
	 * @return String
	 * 			The response string of the service
	 * @throws XPathExpressionException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	public String invokeNEService(String freeText, AnnotatorConfiguration config,int timeout) throws IOException {
		final Spotlight_Params myParams = getParamsFromConfigurationFile(config);
		
		return getAnnotatedText(freeText, myParams, Spotlight.JSON_OUTPUT,timeout);
	}
	/**
	 * Utility method to assign the configuration items from annotator configuration object
	 * @param config
	 * 			The annotator configuration object
	 * @return Spotlight_Params
	 * 			Spotlight parameters
	 */
	private Spotlight_Params getParamsFromConfigurationFile(
			final AnnotatorConfiguration config) {
		final Spotlight_Params params= new Spotlight_Params();
		final Map<String, String> myParams = config.getSpecificParameters();
		if(myParams==null){
			return params;
		}
		params.setAdditionalParameters(myParams);
		
		return params;
	}


}
