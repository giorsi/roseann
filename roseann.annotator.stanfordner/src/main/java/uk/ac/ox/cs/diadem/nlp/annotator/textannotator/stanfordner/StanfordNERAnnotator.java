/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.stanfordner;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;
import uk.ac.ox.cs.diadem.stanford_env.StanfordMarkerInterface;
import uk.ac.ox.cs.diadem.util.misc.DiademInstallationSupport;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreLabel;

/**
 * @author Stefano Ortona (stefnao.ortona at cs.ox.ac.uk), Oxford University, Department of Computer Science 
 * 		   The wrapper submit the text to StanfordNER,
 *         take the response string in local format from Stanford service and then parse it to harvest entity annotations into model
 */
public class StanfordNERAnnotator extends AnnotatorAdapter {
	static final Logger logger = LoggerFactory.getLogger(StanfordNERAnnotator.class);

	static String serializedClassifier;
	static AbstractSequenceClassifier<CoreLabel> classifier;
	
	private boolean resources_initalized;

	/**
	 * counter to increment the annotation id
	 */
	private long id_counter;

	/**
	 * private constructor to build the singleton instance
	 */
	private StanfordNERAnnotator() {
		ConfigurationFacility.getConfiguration();

		setAnnotatorName("stanfordNER");
		id_counter = 0;

		//get the configuration initialized
		AnnotatorConfiguration config=new AnnotatorConfiguration();
		this.setAnnotatorName("stanfordNER");
		config.initializeConfiguration(getAnnotatorName());
		setConfig(config);
		
		this.resources_initalized=false;

	}

	private void initializeResources(){
		
		if(this.resources_initalized)
			return;

		final String classifier_name = config.getSpecificParameters().get("classifier_name");
		final String classifier_location = config.getSpecificParameters().get("classifier_location");

		if(classifier_name==null||classifier_location==null)
			throw new AnnotatorException("Stanford annotator could not find the location of the classifier," +
					" parameters must be specified in the TextannotatorConfiguration.xml file", logger);

		// get the classifier location
		serializedClassifier = DiademInstallationSupport
				.getDiademHome() + File.separator + classifier_location+File.separator+classifier_name;

		logger.info("Installing Stanford environment.");
		if(!new File(serializedClassifier).exists()){
			DiademInstallationSupport.installFromClass(StanfordMarkerInterface.class);
			logger.info("...done.");
		}
		else
			logger.info("...skipped as already installed.");
		classifier = CRFClassifier.getClassifierNoExceptions(serializedClassifier);
		
		this.resources_initalized=true;

	}

	/**
	 * singleton instance
	 */
	private static StanfordNERAnnotator INSTANCE;

	/**
	 * static method to get the singleton instance
	 * @return the singleton instance
	 */
	public static synchronized Annotator getInstance() {

		if (INSTANCE == null) {
			INSTANCE = new StanfordNERAnnotator();
		}
		return INSTANCE;
	}

	@Override
	public Set<Annotation> annotateEntity(String text) {

		final String response = this.submitToAnnotator(text);

		// save the annotations and increment the id counter
		final Set<Annotation> entityAnnotations=retrieveEntityAnnotations(response);
		final int num_annotations=entityAnnotations.size();
		id_counter += num_annotations;
		return entityAnnotations;
	}

	/**
	 * StanfordNer is not a webservice, therefore the timeout parameter is ignored 
	 */
	@Override
	public Set<Annotation> annotateEntity(String text,int timeout) {

		return this.annotateEntity(text);
	}

	/**
	 * Given the response from StanfordNER annotator create the annotation objects
	 * @param response
	 * @return	set of annotations objects
	 */
	private Set<Annotation> retrieveEntityAnnotations(String response) {

		//the annotations to be returned
		Set<Annotation> found_annotations=new HashSet<Annotation>();

		logger.debug("Processing the StanfordNER response.");
		int startFrom = 0;
		int dirtyCount = 0;
		int endTag = 0;
		int annotationCount = 0;
		while (startFrom < (response.length() - 1)) {
			// find opening tag "<"
			startFrom = response.indexOf('<', startFrom);
			if (startFrom < 0) {
				break;
			}
			// find closing tag ">"
			endTag = response.indexOf('>', startFrom);
			String label = response.substring(startFrom + 1, endTag);
			// transform the concept dlv compatible
			//label = AnnotationUtil.modifyConcept(label);

			final int spanBegin = startFrom - dirtyCount;
			startFrom = response.indexOf("</", endTag);
			// String snippet = response.substring(endTag+1, startFrom);
			final int spanEnd = ((startFrom - endTag) + spanBegin) - 1;

			startFrom += 2 + label.length() + 1;
			dirtyCount += 2 + label.length() + 2 + label.length() + 1;

			//save the annotation object
			Annotation annotation = new EntityAnnotation();
			annotation.setId(getAnnotatorName() + "_" + (id_counter + annotationCount));
			annotation.setConcept(label);
			annotation.setAnnotationClass(AnnotationClass.INSTANCE);
			annotation.setStart(spanBegin);
			annotation.setEnd(spanEnd);
			annotation.setOriginAnnotator(getAnnotatorName());
			annotation.setAnnotationSource(getAnnotatorName());

			annotationCount++;
			found_annotations.add(annotation);

		}

		logger.debug("Response processed and annotations saved");
		return found_annotations;
	}

	public String submitToAnnotator(String text) {
		
		this.initializeResources();

		logger.debug("Submitting text to StanfordNER");
		// eliminate from the original text special characters
		text = text.replaceAll("\u0003", " ");
		text = text.replace('<', '[');
		text = text.replace('>', ']');
		String response=null;
		try{
			response=classifier.classifyWithInlineXML(text);
		}
		catch(final Exception e){
			logger.error("Unable to submit text to StanfordNER annotator.");
			throw new AnnotatorException("Unable to submit text to StanfordNER annotator.", e,
					logger);

		}
		logger.debug("StanfordNER process done and response received.");
		return response;

	}

	/**
	 * StanfordNer is not a webservice, therefore the timeout parameter is ignored 
	 */
	@Override
	public String submitToAnnotator(String text,int timeout) {

		return this.submitToAnnotator(text);

	}

}
