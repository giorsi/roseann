/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.stanfordner;

import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;

public class StanfordNERAnnotatorTest {
	
	private Annotator stanford;
	
	private String text;
	
	@Before
	public void bringUp(){
		stanford=StanfordNERAnnotator.getInstance();
		text="Barack Obama is the new president of the United States.";
	}

	/**
	 * Test to check that a text can be submitted to extractiv annotator service
	 * @throws InvalidSyntaxException
	 */
	@Test
	public void testSubmitToAnnotator(){

		//check the classifier name and location has been specified in the configuration
		Assert.assertNotNull(stanford.getConfig().getSpecificParameters().get("classifier_name"));
		Assert.assertNotNull(stanford.getConfig().getSpecificParameters().get("classifier_location"));

		//check that the text can be submitted to the annotator and no exceptions are thrown
		String response = stanford.submitToAnnotator(text);

		Assert.assertNotNull(response);
		Assert.assertTrue(response.length()>0);

		//check the response is in a desidered format
		Assert.assertTrue(response.equals("" +
				"<PERSON>Barack Obama</PERSON> is the new president of the <LOCATION>United States</LOCATION>."));
	}

	/**
	 * Test to verify the entities retrieved from stanfordNER annotator
	 */
	@Test
	public void testAnnotateEntities(){
		Set<Annotation> annotations = stanford.annotateEntity(text);
		Assert.assertNotNull(annotations);
		Assert.assertTrue(annotations.size()>0);
		
		//check the returned annotations contain the desidered annotations
		final Set<Annotation> expected_annotations=new HashSet<Annotation>();
		Annotation a1=new EntityAnnotation();
		a1.setId("stanfordNER_0");
		a1.setConcept("PERSON");
		a1.setAnnotationClass(AnnotationClass.INSTANCE);
		a1.setStart(0);
		a1.setEnd(12);
		a1.setOriginAnnotator("stanfordNER");
		a1.setAnnotationSource("stanfordNER");
		expected_annotations.add(a1);
		
		Annotation a2=new EntityAnnotation();
		a2.setId("stanfordNER_1");
		a2.setConcept("LOCATION");
		a2.setAnnotationClass(AnnotationClass.INSTANCE);
		a2.setStart(41);
		a2.setEnd(54);
		a2.setOriginAnnotator("stanfordNER");
		a2.setAnnotationSource("stanfordNER");
		expected_annotations.add(a2);
		
		Assert.assertEquals(annotations, expected_annotations);
	}
	
	/**
	 * Test to verify that no entitie ares retrieved from stanfordNER annotator with an senseless text
	 */
	@Test
	public void testAnnotateNoEntities(){
		text="cfrgfvdfgbfdgdgdfgdfgdgdg";

		Set<Annotation> annotations = stanford.annotateEntity(text);
		Assert.assertNotNull(annotations);
		Assert.assertTrue(annotations.size()==0);
	}

}
