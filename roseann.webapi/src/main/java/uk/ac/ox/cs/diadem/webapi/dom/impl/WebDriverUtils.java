/*
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science,
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.webapi.dom.impl;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.util.misc.DiademInstallationSupport;
import uk.ac.ox.cs.diadem.webapi.WebAPIRuntimeException;
import uk.ac.ox.cs.diadem.webdriver_env.WebDriverMarker;

/**
 * @author timfu
 */
public class WebDriverUtils {
  private static final Logger logger = LoggerFactory.getLogger(WebDriverUtils.class);
  @SuppressWarnings("unused")
  private static final Configuration config = ConfigurationFacility.getConfiguration();

  public static String getExtractionPath() {
    return FilenameUtils.separatorsToSystem(DiademInstallationSupport.getDiademHome() + File.separator
        + ConfigurationFacility.getConfiguration().getString("installation-path.webdriver.path"));
  }

  static String getDisplaySizeFilePath() {
    return FilenameUtils.separatorsToSystem(DiademInstallationSupport.getDiademHome() + File.separator
        + ConfigurationFacility.getConfiguration().getString("installation-path.webdriver.path") + File.separator
        + "firefox28/display_size");
  }

  private static Class<?> getMarkerClass() {
    return WebDriverMarker.class;
  }

  public static void installFirefoxEnvironment() {
    final String path = getExtractionPath();
    try {
      installEnvironment(getMarkerClass(), path);
      setExecutablePermissionOnFiles();
    } catch (final WebAPIRuntimeException e) {
      logger.error("Cannot install Firefox environment in {}", path);
      throw new WebAPIBrowserBinaryNotFound("Problem in finding browser binary. Error " + e.getMessage(), e, logger);
    }
  }

  private static void setExecutablePermissionOnFiles() {
    final List<String> list = ConfigurationFacility.getConfiguration().getList("installation-path.webdriver.files");
    for (final String file : list) {
      final String abFile = FilenameUtils.separatorsToSystem(DiademInstallationSupport.getDiademHome() + File.separator
          + file);
      logger.debug("Setting executable permissions on file: '{}'", abFile);
      final File toExe = new File(abFile);
      if (!toExe.exists())
        throw new WebAPIRuntimeException("File " + abFile + " does not exists", logger);
      if (!toExe.canExecute()) {
        final boolean done = toExe.setExecutable(true);
        if (!done)
          throw new WebAPIRuntimeException("Cannot set executable flag on" + abFile, logger);
      }
    }
  }

  private static void installEnvironment(final Class<?> marker, final String path) {
    logger.debug("installing environment for {} ", marker);
    // check if override is wanted
    final String override = System.getProperty(ConfigurationFacility.getConfiguration().getString(
        "override-property-name"));
    boolean forceOverride = false;
    if (override != null) {
      forceOverride = Boolean.parseBoolean(override);
      if (forceOverride) {
        logger.debug("forced override of environmnent for {} ", marker);
      }
    }
    final boolean skip = (new File(path).exists()) && !forceOverride;
    if (skip) {
      logger.debug("...skipped as already existing, using environment in {}", path);
    } else {
      DiademInstallationSupport.installFromClass(marker);
    }
    logger.debug("...environment {} installed in {}", marker, path);
  }

}
