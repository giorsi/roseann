/*
 * Copyright (c)2011, DIADEM Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the DIADEM team nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DIADEM Team BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package uk.ac.ox.cs.diadem.webapi.dom.impl;

import java.awt.Dimension;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.css.sac.CSSException;
import org.w3c.css.sac.CSSParseException;
import org.w3c.css.sac.ErrorHandler;
import org.w3c.dom.Node;

import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.webapi.DialogsService;
import uk.ac.ox.cs.diadem.webapi.WebAPIRuntimeException;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.css.StyledOverlayBuilder;
import uk.ac.ox.cs.diadem.webapi.dom.DOMBoundingClientRect;
import uk.ac.ox.cs.diadem.webapi.dom.DOMCSS2Properties.CssProperty;
import uk.ac.ox.cs.diadem.webapi.dom.DOMCSSStyleDeclaration;
import uk.ac.ox.cs.diadem.webapi.dom.DOMDocument;
import uk.ac.ox.cs.diadem.webapi.dom.DOMElement;
import uk.ac.ox.cs.diadem.webapi.dom.DOMHTMLForm;
import uk.ac.ox.cs.diadem.webapi.dom.DOMHTMLInputElement;
import uk.ac.ox.cs.diadem.webapi.dom.DOMHTMLOptionElement;
import uk.ac.ox.cs.diadem.webapi.dom.DOMHTMLOptionsCollection;
import uk.ac.ox.cs.diadem.webapi.dom.DOMHTMLSelect;
import uk.ac.ox.cs.diadem.webapi.dom.DOMHTMLTextAreaElement;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNamedNodeMap;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode.Type;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNodeList;
import uk.ac.ox.cs.diadem.webapi.dom.DOMRange;
import uk.ac.ox.cs.diadem.webapi.dom.DOMTypeableElement;
import uk.ac.ox.cs.diadem.webapi.dom.DOMWindow;
import uk.ac.ox.cs.diadem.webapi.dom.DOmElementOnJS;
import uk.ac.ox.cs.diadem.webapi.dom.HTMLUtil;
import uk.ac.ox.cs.diadem.webapi.dom.event.DOMEventListener;
import uk.ac.ox.cs.diadem.webapi.dom.event.DOMFocusEvent;
import uk.ac.ox.cs.diadem.webapi.dom.event.DOMKeyboardEvent;
import uk.ac.ox.cs.diadem.webapi.dom.event.DOMKeyboardEvent.Key;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;
import uk.ac.ox.cs.diadem.webapi.dom.mutation.CSSMutationObserver;
import uk.ac.ox.cs.diadem.webapi.dom.xpath.DOMXPathEvaluator;
import uk.ac.ox.cs.diadem.webapi.dom.xpath.DOMXPathException;
import uk.ac.ox.cs.diadem.webapi.dom.xpath.DOMXPathExpression;
import uk.ac.ox.cs.diadem.webapi.dom.xpath.DOMXPathNSResolver;
import uk.ac.ox.cs.diadem.webapi.dom.xpath.DOMXPathResult;
import uk.ac.ox.cs.diadem.webapi.interaction.AdviceProcessor;
import uk.ac.ox.cs.diadem.webapi.interaction.WebActionExecutor;
import uk.ac.ox.cs.diadem.webapi.interaction.impl.WebAdviceProcessor;
import uk.ac.ox.cs.diadem.webapi.listener.BrowserLocationListener;
import uk.ac.ox.cs.diadem.webapi.listener.BrowserProgressListener;
import uk.ac.ox.cs.diadem.webapi.listener.BrowserStatusTextListener;
import uk.ac.ox.cs.diadem.webapi.listener.BrowserTitleListener;
import uk.ac.ox.cs.diadem.webapi.listener.OpenNewWindowListener;

import com.gargoylesoftware.htmlunit.AlertHandler;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.ConfirmHandler;
import com.gargoylesoftware.htmlunit.DefaultCssErrorHandler;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.IncorrectnessListener;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.PromptHandler;
import com.gargoylesoftware.htmlunit.ScriptException;
import com.gargoylesoftware.htmlunit.ScriptResult;
import com.gargoylesoftware.htmlunit.StatusHandler;
import com.gargoylesoftware.htmlunit.TopLevelWindow;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebWindow;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.FrameWindow;
import com.gargoylesoftware.htmlunit.html.HTMLParserListener;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.gargoylesoftware.htmlunit.html.HtmlTextArea;
import com.gargoylesoftware.htmlunit.html.impl.SelectableTextInput;
import com.gargoylesoftware.htmlunit.javascript.JavaScriptErrorListener;
import com.gargoylesoftware.htmlunit.javascript.host.KeyboardEvent;
import com.gargoylesoftware.htmlunit.javascript.host.css.ComputedCSSStyleDeclaration;
import com.gargoylesoftware.htmlunit.javascript.host.html.HTMLElement;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;

/**
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
class HTMLUnitBrowser212 extends AbstractWebBrowser<WebClient> {

  private static final Logger LOGGER = LoggerFactory.getLogger(HTMLUnitBrowser212.class);
  private static final int DELAY_MILLIS = 500;
  private WebClient client;

  static final BiMap<Short, Type> noeTypes;

  static {
    noeTypes = HashBiMap.create();
    noeTypes.put(Node.ATTRIBUTE_NODE, Type.ATTRIBUTE);
    noeTypes.put(Node.CDATA_SECTION_NODE, Type.CDATA_SECTION);
    noeTypes.put(Node.COMMENT_NODE, Type.COMMENT);
    noeTypes.put(Node.DOCUMENT_NODE, Type.DOCUMENT);
    noeTypes.put(Node.DOCUMENT_FRAGMENT_NODE, Type.DOCUMENT_FRAGMENT);
    noeTypes.put(Node.DOCUMENT_TYPE_NODE, Type.DOCUMENT_TYPE);
    noeTypes.put(Node.ELEMENT_NODE, Type.ELEMENT);
    noeTypes.put(Node.ENTITY_NODE, Type.ENTITY);
    noeTypes.put(Node.ENTITY_REFERENCE_NODE, Type.ENTITY_REFERENCE);
    noeTypes.put(Node.NOTATION_NODE, Type.NOTATION);
    noeTypes.put(Node.PROCESSING_INSTRUCTION_NODE, Type.PROCESSING_INSTRUCTION);
    noeTypes.put(Node.TEXT_NODE, Type.TEXT);

  }

  private final Configuration configuration;
  private int lastNavigationStatusCode = -1;

  /**
   * @param visible
   */
  HTMLUnitBrowser212() {
    super();
    configuration = ConfigurationFacility.getConfiguration();

    LOGGER.debug("Inizializing HTMLUnit Browser on as Firefox 17");
    final BrowserVersion firefoxVersion = BrowserVersion.FIREFOX_24;
    LOGGER.debug("Removing plugins form browser");
    firefoxVersion.getPlugins().clear();
    firefoxVersion.setUserAgent(configuration.getString("webapi.useragent", firefoxVersion.getUserAgent()));
    LOGGER.debug("Setting user Agent '{}'", firefoxVersion.getUserAgent());

    // http://stackoverflow.com/questions/3600557/turning-htmlunit-warnings-off
    LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
    java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);
    java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);
    client = new WebClient(firefoxVersion);
    shutupLogging(client);
    // LOGGER.debug("Setting NicelyResynchronizingAjaxController");
    // client.setAjaxController(new NicelyResynchronizingAjaxController());
    LOGGER.debug("clearing cache and coockies ");
    client.getCache().clear();
    client.getCookieManager().clearCookies();
    LOGGER.debug("setting cache to to max 250 files");
    client.getCache().setMaxSize(250);

    // no throw to prevent error on missing JS files
    client.getOptions().setThrowExceptionOnFailingStatusCode(false);

    LOGGER.debug("Enabling javascript");
    client.getOptions().setJavaScriptEnabled(true);
    LOGGER.debug("disabling ThrowExceptionOnScriptError");
    client.getOptions().setThrowExceptionOnScriptError(false);
    final int sec = ConfigurationFacility.getConfiguration().getInt("webdriver.options.timeouts.page-load-sec");
    LOGGER.debug("setting page loading timeout to <{}>ms", sec);
    client.getOptions().setTimeout(1000 * sec);

    LOGGER.debug("enabled popup blocker");
    client.getOptions().setPopupBlockerEnabled(true);
    LOGGER.debug("setting a custom  AlertHandler");
    client.setAlertHandler(new AlertHandler() {

      @Override
      public void handleAlert(final Page page, final String message) {
        LOGGER.info("Alert with message '{}' has been collected", message);
        return;
      }
    });
    LOGGER.debug("setting a custom  ConfirmHandler to press always OK");
    client.setConfirmHandler(new ConfirmHandler() {

      @Override
      public boolean handleConfirm(final Page page, final String message) {
        LOGGER.info("Confirm Dialogue with message '{}', clicking on the OK button", message);
        return true;
      }
    });
    LOGGER.debug("setting a custom  PromptHandler to press always CANCEL");
    client.setPromptHandler(new PromptHandler() {

      @Override
      public String handlePrompt(final Page page, final String message) {
        LOGGER.info("Prompt handles with message '{}', clicking on the CANCEL button", message);
        return null;
      }
    });
    LOGGER.debug("setting a custom  StatusHandler to log messages");
    client.setStatusHandler(new StatusHandler() {

      @Override
      public void statusMessageChanged(final Page page, final String message) {
        LOGGER.debug("Page {}, status Message Changed to '{}'", page.getUrl(), message);

      }
    });

    // client.setCssErrorHandler(new ErrorHandlerImplementation());
    // client.setHTMLParserListener(new LoggingHTMLParserListener());
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.ox.cs.diadem.webapi.MiniBrowser#getActionExecutor()
   */
  @Override
  public WebActionExecutor getActionExecutor() {
    LOGGER.warn("getActionExecutor() not available for HTMLUnit Browser");
    return null;
  }

  private void shutupLogging(final WebClient webClient) {
    // LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log",
    // "org.apache.commons.logging.impl.NoOpLog");
    //
    // java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);
    // java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);

    webClient.setIncorrectnessListener(new IncorrectnessListener() {

      @Override
      public void notify(final String arg0, final Object arg1) {
        // TODO Auto-generated method stub

      }
    });
    webClient.setCssErrorHandler(new ErrorHandler() {

      @Override
      public void warning(final CSSParseException exception) throws CSSException {
        // TODO Auto-generated method stub

      }

      @Override
      public void fatalError(final CSSParseException exception) throws CSSException {
        // TODO Auto-generated method stub

      }

      @Override
      public void error(final CSSParseException exception) throws CSSException {
        // TODO Auto-generated method stub

      }
    });
    webClient.setJavaScriptErrorListener(new JavaScriptErrorListener() {

      @Override
      public void timeoutError(final HtmlPage arg0, final long arg1, final long arg2) {
        // TODO Auto-generated method stub

      }

      @Override
      public void malformedScriptURL(final HtmlPage arg0, final String arg1, final MalformedURLException arg2) {
        // TODO Auto-generated method stub

      }

      @Override
      public void loadScriptError(final HtmlPage arg0, final URL arg1, final Exception arg2) {
        // TODO Auto-generated method stub

      }

      @Override
      public void scriptException(final HtmlPage htmlPage, final ScriptException scriptException) {
        // TODO Auto-generated method stub

      }
    });
    webClient.setHTMLParserListener(new HTMLParserListener() {

      @Override
      public void error(final String message, final URL url, final String html, final int line, final int column,
          final String key) {
        // TODO Auto-generated method stub

      }

      @Override
      public void warning(final String message, final URL url, final String html, final int line, final int column,
          final String key) {
        // TODO Auto-generated method stub

      }
    });

  }

  @Override
  public String toString() {
    return Engine.HTMLUNIT.name() + "@" + Integer.toHexString(hashCode());
  }

  @Override
  public Engine getEngine() {
    return Engine.HTMLUNIT;
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.ox.cs.diadem.webapi.WebBrowser#getContentDOMWindow()
   */
  @Override
  public DOMWindow getContentDOMWindow() {

    return new DOMWindowWrapHtmlUnit(getWebBrowserCurrentTopWindow());
  }

  private DOMWindow loadUriAndGetWindow(final String URI, final boolean wait, final boolean throwOnError) {

    try {
      client.getOptions().setCssEnabled(false);
      LOGGER.debug("Navigating to page {}", URI);
      // final Page page = client.getPage(URI);
      lastNavigationStatusCode = client.getPage(URI).getWebResponse().getStatusCode();
      pages++;
      LOGGER
          .debug(
              "wait until all background JavaScript tasks scheduled to start executing before {}ms have finished executing.",
              DELAY_MILLIS);

      // client.waitForBackgroundJavaScript(timeoutMillis)
      // client.waitForBackgroundJavaScriptStartingBefore(DELAY_MILLIS);

      locationEmpty = false;
      return new DOMWindowWrapHtmlUnit(getWebBrowserCurrentTopWindow());

    } catch (final FailingHttpStatusCodeException e) {
      LOGGER.error("Failing loading {}, status code {}", URI, e.getStatusCode());
      lastNavigationStatusCode = e.getStatusCode();
      if (throwOnError)
        throw new WebAPIRuntimeException(e.getMessage(), e, LOGGER);
      else
        return new DOMWindowWrapHtmlUnit(getWebBrowserCurrentTopWindow());
    } catch (final MalformedURLException e) {
      LOGGER.error("Malformed URL {}", URI);
      if (throwOnError)
        throw new WebAPIRuntimeException(e.getMessage(), e, LOGGER);
    } catch (final IOException e) {
      LOGGER.error("IO error loading  {}", URI);
      if (throwOnError)
        throw new WebAPIRuntimeException(e.getMessage(), e, LOGGER);
    }
    // here means there was an exception not thrown
    lastNavigationStatusCode = 404;
    LOGGER.warn("Cannot get status code for <{}>, due to errors. Reporting status code <{}>", URI,
        lastNavigationStatusCode);
    return new DOMWindowWrapHtmlUnit(getWebBrowserCurrentTopWindow());
  }

  @Override
  public void navigate(final String URI) {
    loadUriAndGetWindow(URI, true, true);
    // walkTree();

  }

  @Override
  public int navigateAndStatus(final URI uri) {
    loadUriAndGetWindow(uri.toString(), true, false);
    return lastNavigationStatusCode;
  }

  @Override
  public void navigate(final URI uri) {
    navigate(uri.toString());

  }

  // private void walkTree() {
  // final int[] c = new int[] { 0 };
  // final Stopwatch w = new Stopwatch();
  // w.start();
  //
  // walk(getContentDOMWindow().document().getDocumentElement(), c, 0);
  // w.stop();
  // System.out.println("count:" + c[0]);
  // System.out.println("time inner:" + w.elapsedMillis());
  // w.reset();
  // c[0] = 0;
  // w.start();
  // walk(getContentDOMWindow().document().getDocumentElement(), c, 0);
  // w.stop();
  // System.out.println("count:" + c[0]);
  // System.out.println("time inner:" + w.elapsedMillis());
  // }
  //
  // private void walk(final DOMNode domElement, final int[] c, final int level) {
  //
  // // System.out.println(node.getNodeName());
  // // if (level == 15)
  // // System.out.println(domElement.getNodeName() + " --" + level);
  // for (final DOMNode element : domElement.getChildNodes()) {
  // if (element == null)
  // c[0]++;
  // else
  // c[1 - 1]++;
  //
  // walk(element, c, level + 1);
  // }
  //
  // }

  @Override
  public Object getWindowFrame() {
    // walkTree();
    return client;
  }

  @Override
  public void close() {
    LOGGER.info("closing window {}", client.getCurrentWindow());
    if (client.getCurrentWindow() != null) {
      ((TopLevelWindow) client.getCurrentWindow().getTopWindow()).close();
    }
    if (client.getWebWindows().size() == 0) {
      LOGGER.info("No more windows opened, shutdown the browser");
      shutdown();
    }
  }

  @Override
  protected WebClient getBrowser() {
    return client;
  }

  @Override
  public String getLocationURL() {
    if (locationEmpty)
      return null;
    return getWebBrowserCurrentTopWindow().getEnclosedPage().getUrl().toString();
  }

  private WebWindow getWebBrowserCurrentTopWindow() {
    return client.getCurrentWindow().getTopWindow();
  }

  @Override
  public void back(final boolean waitUntilLoaded) {
    back();
  }

  /*
   * (non-Javadoc)
   *
   * @see uk.ac.ox.cs.diadem.webapi.WebBrowser#back()
   */
  @Override
  public void back() {
    LOGGER.debug("Going back ");
    try {
      getWebBrowserCurrentTopWindow().getHistory().back();
      LOGGER.info("after back: current page is '{}': ", getWebBrowserCurrentTopWindow().getEnclosedPage().getUrl());
    } catch (final IOException e) {
      LOGGER.debug("Failed Going back ");
      throw new WebAPIRuntimeException("Error going back", e, LOGGER);
    }

  }

  @Override
  public void enableFeatures(final FeatureType... features) {
    for (final FeatureType featureType : features) {
      if (featureType == FeatureType.JAVASCRIPT) {
        LOGGER.debug("Disabling Javascript emulation");
        client.getOptions().setJavaScriptEnabled(true);
      } else {
        LOGGER.debug("HtmlUnit does not download images and no manages flash at all");
      }
    }

  }

  @Override
  public void disableFeatures(final FeatureType... features) {
    for (final FeatureType featureType : features) {
      if (featureType == FeatureType.JAVASCRIPT) {
        LOGGER.debug("Disabling javascript");
        client.getOptions().setJavaScriptEnabled(false);
      } else {
        LOGGER.debug("HtmlUnit does not download images and no manages flash at all");
      }
    }

  }

  @Override
  public DOMXPathEvaluator getOXPathEvaluator() {
    return new DOMXPathEvaluatorOnHTMLUnitImpl();
  }

  @Override
  public void addProgressListener(final BrowserProgressListener listener) {
    throw new WebAPIRuntimeException("Not supported yet", LOGGER);

  }

  @Override
  public void removeProgressListener(final BrowserProgressListener listener) {
    throw new WebAPIRuntimeException("Not supported yet", LOGGER);

  }

  @Override
  public void addLocationListener(final BrowserLocationListener listener) {
    throw new WebAPIRuntimeException("Not supported yet", LOGGER);

  }

  @Override
  public void removeLocationListener(final BrowserLocationListener listener) {
    throw new WebAPIRuntimeException("Not supported yet", LOGGER);

  }

  @Override
  public void addTitleListener(final BrowserTitleListener listener) {
    throw new WebAPIRuntimeException("Not supported yet", LOGGER);

  }

  @Override
  public void removeTitleListener(final BrowserTitleListener listener) {
    throw new WebAPIRuntimeException("Not supported yet", LOGGER);

  }

  @Override
  public void addStatusTextListener(final BrowserStatusTextListener listener) {
    throw new WebAPIRuntimeException("Not supported yet", LOGGER);

  }

  @Override
  public void removeStatusTextListener(final BrowserStatusTextListener listener) {
    throw new WebAPIRuntimeException("Not supported yet", LOGGER);

  }

  @Override
  public void navigate(final String uRI, final boolean waitUntilLoaded) {
    navigate(uRI);

  }

  @Override
  public void forward(final boolean waitUntilLoaded) {
    LOGGER.debug("Going forward");
    try {
      getWebBrowserCurrentTopWindow().getHistory().forward();
      LOGGER.info("after forward: current page is '{}': ", getWebBrowserCurrentTopWindow().getEnclosedPage().getUrl());
    } catch (final IOException e) {
      LOGGER.debug("Failed Going forward ");
      throw new WebAPIRuntimeException("Error going back", e, LOGGER);
    }
  }

  @Override
  public void refresh() {
    LOGGER.debug("Refreshing page");
    if (getWebBrowserCurrentTopWindow() instanceof HtmlPage) {
      try {
        ((HtmlPage) getWebBrowserCurrentTopWindow()).refresh();
      } catch (final IOException e) {
        throw new WebAPIRuntimeException("Error refreshing page", e, LOGGER);
      }
    }
  }

  @Override
  public void stop() {
    LOGGER.debug("Stop not supportedin HTMLUNIT");

  }

  @Override
  public boolean isBackEnabled() {
    final int index = getWebBrowserCurrentTopWindow().getHistory().getIndex();
    return getWebBrowserCurrentTopWindow().getHistory().getUrl(index - 1) != null;
  }

  @Override
  public boolean isForwardEnabled() {
    final int index = getWebBrowserCurrentTopWindow().getHistory().getIndex();
    return getWebBrowserCurrentTopWindow().getHistory().getUrl(index + 1) != null;
  }

  @Override
  public Object evaluate(final String script) {
    LOGGER.debug("Evaluating javascript: '{}'" + script);
    final Page result = getWebBrowserCurrentTopWindow().getEnclosedPage();
    if (result instanceof HtmlPage)
      return ((HtmlPage) result).executeJavaScript("javascript:" + script);
    else {
      LOGGER.error("Javascript only enabled on HTML Pages. cannot execute '{}'" + script);
      throw new WebAPIRuntimeException("Javascript only enabled on HTML Pages", LOGGER);
    }
  }

  @Override
  public void setDialogsService(final DialogsService service) {
    LOGGER.debug("DialogsService unsupported on HTMLUNIT. By default, popups are muted");
  }

  @Override
  public void setZoom(final int zoomRatio) {
    LOGGER.error("setZoom unsupported on HTMLUNIT. ");
  }

  @Override
  public void setWindowSize(final int x, final int y) {
    LOGGER.error("setWindowSize unsupported on HTMLUNIT. ");
  }

  @Override
  public void addOpenNewWindowListener(final OpenNewWindowListener openNewWindowListener) {
    throw new WebAPIRuntimeException("not implemented yet", LOGGER);
  }

  @Override
  public void removeOpenNewWindowListener(final OpenNewWindowListener listener) {
    throw new WebAPIRuntimeException("not implemented yet", LOGGER);
  }

  @Override
  public void cleanCache() {
    LOGGER.debug("Clearing cache");
    client.getCache().clear();

  }

  @Override
  public void removeAllCookies() {
    LOGGER.debug("Removing Cookies");
    client.getCookieManager().clearCookies();

  }

  @SuppressWarnings("unused")
  private final class LoggingHTMLParserListener implements HTMLParserListener {

    @Override
    public void error(final String message, final URL url, final String html, final int line, final int column,
        final String key) {

      HTMLParserListener.LOG_REPORTER.error(message, url, html, line, column, key);
      LOGGER.debug(format(message, url, html, line, column));
    }

    @Override
    public void warning(final String message, final URL url, final String html, final int line, final int column,
        final String key) {
      HTMLParserListener.LOG_REPORTER.warning(message, url, html, line, column, key);
      LOGGER.debug(format(message, url, html, line, column));
    }

    private String format(final String message, final URL url, final String html, final int line, final int column) {
      final StringBuilder buffer = new StringBuilder(message);
      buffer.append(" (");
      buffer.append(url.toExternalForm());
      buffer.append(" ");
      buffer.append(line);
      buffer.append(":");
      buffer.append(column);
      buffer.append(" - ");
      buffer.append(html);
      buffer.append(")");
      return buffer.toString();
    }

  }

  @SuppressWarnings("unused")
  private final class ErrorHandlerImplementation extends DefaultCssErrorHandler {
    public ErrorHandlerImplementation() {
      super();
    }

    @Override
    public void warning(final CSSParseException arg0) throws CSSException {
      super.warning(arg0);
      LOGGER.debug("HTMLUnit CSS warning:: {}", buildMessage(arg0));

    }

    @Override
    public void fatalError(final CSSParseException arg0) throws CSSException {
      super.warning(arg0);
      LOGGER.debug("HTMLUnit CSS fatal error:: {}", buildMessage(arg0));

    }

    @Override
    public void error(final CSSParseException arg0) throws CSSException {
      super.warning(arg0);
      LOGGER.debug("HTMLUnit CSS error:: {}", buildMessage(arg0));

    }

    private String buildMessage(final CSSParseException exception) {
      final String uri = exception.getURI();
      final int line = exception.getLineNumber();
      final int col = exception.getColumnNumber();

      if (null == uri)
        return "[" + line + ":" + col + "] " + exception.getMessage();
      return "'" + uri + "' [" + line + ":" + col + "] " + exception.getMessage();
    }
  }

  class DOMWindowWrapHtmlUnit implements DOMWindow {

    private final WebWindow window;

    public DOMWindowWrapHtmlUnit(final WebWindow window) {
      // LOGGER.debug("DOMWindowWrapHtmlUnit of instance '{}'", window);
      this.window = window;
    }

    @Override
    public String getContentAsString() {
      return window.getEnclosedPage().getWebResponse().getContentAsString();
    }

    @Override
    public DOMDocument getDocument() {
      final Page page = window.getEnclosedPage();
      if (page instanceof HtmlPage) {
        final HtmlPage htmlPage = ((HtmlPage) page);
        return new DOMDocumentWrapHtmlUnit(htmlPage);
      }

      LOGGER.error("HTMLUnit does not support non-html documents");
      throw new WebAPIRuntimeException("Unsupported page content " + page.getClass().getCanonicalName(), LOGGER);

    }

    WebWindow getWrappedObject() {
      return window;
    }

    @Override
    public String getTitle() {
      Page page = window.getEnclosedPage();
      if (window instanceof FrameWindow) {
        page = ((FrameWindow) window).getTopWindow().getEnclosedPage();
      }

      return ((HtmlPage) page).getTitleText();
    }

    @Override
    public String getName() {
      return window.getName();
    }

    @Override
    public void setName(final String name) {
      window.setName(name);
    }

    @Override
    public void close() {
      if (window.getTopWindow() != null) {
        LOGGER.debug("closing window {}", window.getName());
        ((TopLevelWindow) window.getTopWindow()).close();
      }

    }

    @Override
    public String toString() {
      return super.toString() + "\nWindow :" + window.getEnclosedPage().getUrl();
    }

    @Override
    public void addEventListener(final String type, final DOMEventListener listener, final boolean useCapture) {
      throw new WebAPIRuntimeException("not implemented yet", LOGGER);

    }

    @Override
    public void removeEventListener(final String type, final DOMEventListener listener, final boolean useCapture) {
      throw new WebAPIRuntimeException("not implemented yet", LOGGER);

    }

    @Override
    public WebBrowser getBrowser() {
      return HTMLUnitBrowser212.this;
    }

    @Override
    public int getScrollX() {
      LOGGER.error("HTMLUnit does not support scrolling");
      return -1;
    }

    @Override
    public int getScrollY() {
      LOGGER.error("HTMLUnit does not support scrolling");
      return -1;
    }

    @Override
    public DOMDocument document() {
      return getDocument();
    }

    @Override
    public boolean isJustOpened() {
      LOGGER.debug("Multiple windows not yet supported");
      return false;
    }

    @Override
    public StyledOverlayBuilder getOverlayBuilder() {
      LOGGER.debug("HTMLUnit does not support visualization, return null");
      return null;
    }

    @Override
    public DOMDocument getDocument(final boolean checkIfStale) {

      return getDocument();
    }

    @Override
    public Dimension getDimension() {
      throw new WebAPIRuntimeException("not implemented yet", LOGGER);
    }

  }

  class DOMHTMLOptionWrapHtmlUnit<N extends HtmlOption> extends DOMElementWrapHtmlUnit<HtmlOption> implements
      DOMHTMLOptionElement {

    private final HtmlOption option;

    public DOMHTMLOptionWrapHtmlUnit(final HtmlOption option) {
      super(option);
      this.option = option;
    }

    @Override
    public boolean getDefaultSelected() {
      return option.isDefaultSelected();
    }

    @Override
    public String getText() {
      return option.getText();
    }

    @Override
    public int getIndex() {
      return option.getIndex();
    }

    @Override
    public boolean getDisabled() {
      return option.isDisabled();
    }

    @Override
    public String getLabel() {
      return option.getLabelAttribute();
    }

    @Override
    public boolean getSelected() {
      return option.isSelected();
    }

    @Override
    public void setSelected(final boolean aSelected) {
      option.setSelected(aSelected);

    }

    @Override
    public String getValue() {
      return option.getValueAttribute();
    }

    @Override
    public void setValue(final String aValue) {
      option.setValueAttribute(aValue);
    }

    @Override
    HtmlOption getWrappedNode() {
      return this.option;
    }

  }

  class DOMHTMLTextareaWrapHtmlUnit<N extends HtmlTextArea> extends DOMElementWrapHtmlUnit<HtmlTextArea> implements
      DOMHTMLTextAreaElement {

    private final HtmlTextArea textarea;

    public DOMHTMLTextareaWrapHtmlUnit(final HtmlTextArea textarea) {
      super(textarea);
      this.textarea = textarea;
    }

    @Override
    HtmlTextArea getWrappedNode() {
      return textarea;
    }

    @Override
    public boolean getDisabled() {
      return getWrappedNode().isDisabled();
    }

    @Override
    public void setValue(final String aValue) {
      getWrappedNode().setAttribute("value", aValue);

    }

    @Override
    public Object getWrappedElement() {
      return getWrappedNode();
    }

    @Override
    public String getDefaultValue() {
      return getWrappedNode().getDefaultValue();
    }

    @Override
    public void select() {
      if (getWrappedNode() instanceof SelectableTextInput) {
        final SelectableTextInput s = getWrappedNode();
        s.select();
      }
    }

    @Override
    public String getValue() {
      return getWrappedNode().getAttribute("value");
    }

    @Override
    public boolean willValidate() {

      return getWrappedNode().hasAttribute("willValidate");
    }

  }

  class DOMHTMLInputElementWrapHtmlUnit<N extends HtmlInput> extends DOMElementWrapHtmlUnit<HtmlInput> implements
      DOMHTMLInputElement {

    private final HtmlInput htmlInput;

    public DOMHTMLInputElementWrapHtmlUnit(final HtmlInput htmlInput) {
      super(htmlInput);
      this.htmlInput = htmlInput;
    }

    @Override
    HtmlInput getWrappedNode() {
      return this.htmlInput;
    }

    @Override
    public String getDefaultValue() {
      return getWrappedNode().getDefaultValue();
    }

    @Override
    public void setDefaultValue(final String aDefaultValue) {
      getWrappedNode().setDefaultValue(aDefaultValue);
    }

    @Override
    public boolean getDefaultChecked() {

      return getWrappedNode().isDefaultChecked();
    }

    @Override
    public void setDefaultChecked(final boolean aDefaultChecked) {
      getWrappedNode().setDefaultChecked(aDefaultChecked);
    }

    @Override
    public String getAccept() {
      return getWrappedNode().getAcceptAttribute();
    }

    @Override
    public void setAccept(final String aAccept) {
      getWrappedNode().setAttribute("accept", aAccept);

    }

    @Override
    public String getAccessKey() {
      return getWrappedNode().getAccessKeyAttribute();
    }

    @Override
    public void setAccessKey(final String aAccessKey) {
      getWrappedNode().setAttribute("accesskey", aAccessKey);

    }

    @Override
    public String getAlign() {
      return getWrappedNode().getAlignAttribute();
    }

    @Override
    public void setAlign(final String aAlign) {
      getWrappedNode().setAttribute("align", aAlign);
    }

    @Override
    public String getAlt() {
      return getWrappedNode().getAltAttribute();
    }

    @Override
    public void setAlt(final String aAlt) {
      getWrappedNode().setAttribute("alt", aAlt);

    }

    @Override
    public boolean getChecked() {
      return getWrappedNode().isChecked();
    }

    @Override
    public void setChecked(final boolean aChecked) {
      getWrappedNode().setChecked(aChecked);

    }

    @Override
    public boolean getDisabled() {
      return getWrappedNode().isDisabled();
    }

    @Override
    public void setDisabled(final boolean aDisabled) {
      getWrappedNode().setAttribute("disabled", "true");
    }

    @Override
    public int getMaxLength() {
      return Integer.parseInt(getWrappedNode().getMaxLengthAttribute());
    }

    @Override
    public void setMaxLength(final int aMaxLength) {
      getWrappedNode().setAttribute("maxlength", aMaxLength + "");
    }

    @Override
    public String getName() {
      return getWrappedNode().getNameAttribute();
    }

    @Override
    public void setName(final String aName) {
      getWrappedNode().setAttribute("name", aName);
    }

    @Override
    public boolean getReadOnly() {
      return Boolean.parseBoolean(getWrappedNode().getReadOnlyAttribute());
    }

    @Override
    public void setReadOnly(final boolean aReadOnly) {
      getWrappedNode().setAttribute("readonly", aReadOnly + "");

    }

    @Override
    public long getSize() {
      return Long.parseLong(getWrappedNode().getSizeAttribute());
    }

    @Override
    public void setSize(final long aSize) {
      getWrappedNode().setAttribute("size", aSize + "");

    }

    @Override
    public String getSrc() {
      return getWrappedNode().getNameAttribute();
    }

    @Override
    public void setSrc(final String aSrc) {
      getWrappedNode().setAttribute("src", aSrc);

    }

    @Override
    public int getTabIndex() {
      return Integer.parseInt(getWrappedNode().getTabIndexAttribute());
    }

    @Override
    public void setTabIndex(final int aTabIndex) {
      getWrappedNode().setAttribute("tabindex", aTabIndex + "");

    }

    @Override
    public String getType() {
      return getWrappedNode().getTypeAttribute();
    }

    @Override
    public void setType(final String aType) {
      getWrappedNode().setAttribute("type", aType);

    }

    @Override
    public String getUseMap() {
      return getWrappedNode().getUseMapAttribute();
    }

    @Override
    public void setUseMap(final String aUseMap) {
      getWrappedNode().setAttribute("usemap", aUseMap);

    }

    @Override
    public String getValue() {
      return getWrappedNode().getValueAttribute();
    }

    @Override
    public void setValue(final String aValue) {
      getWrappedNode().setAttribute("value", aValue);

    }

    @Override
    public void blur() {
      getWrappedNode().blur();
    }

    @Override
    public void select() {
      if (getWrappedNode() instanceof SelectableTextInput) {
        final SelectableTextInput s = (SelectableTextInput) getWrappedNode();
        s.select();
      }
    }

    @Override
    public Object getWrappedElement() {
      return getWrappedNode();
    }
  }

  class DOMHTMLSelectElementWrapHtmlUnit<N extends HtmlSelect> extends DOMElementWrapHtmlUnit<HtmlSelect> implements
      DOMHTMLSelect {

    private final HtmlSelect htmlSelect;

    public DOMHTMLSelectElementWrapHtmlUnit(final HtmlSelect htmlSelect) {
      super(htmlSelect);
      this.htmlSelect = htmlSelect;
    }

    @Override
    public void selectAllOptions() {
      throw new WebAPIRuntimeException("Not implemented", LOG);
    }

    @Override
    public DOMWindow selectByClick(final int index) {
      try {
        final Page page = getWrappedNode().getOption(index).click();
        LOGGER.debug("Clicking on {}, option index {}", this, index);
        return new DOMWindowWrapHtmlUnit(page.getEnclosingWindow());
      } catch (final Exception e) {
        throw new WebAPIRuntimeException("IO error occurred during click", e, LOGGER);
      }
    }

    @Override
    public DOMWindow selectByClick(final String optiontext) {
      return selectByClick(getWrappedNode().getOptionByText(optiontext).getIndex());
    }

    @Override
    public int getSelectedIndex() {
      return getWrappedNode().getSelectedOptions().get(0).getIndex();
    }

    @Override
    public String getValue() {
      return getWrappedNode().getSelectedOptions().get(0).getValueAttribute();
    }

    @Override
    public void setValue(final String aValue) {
      getWrappedNode().setSelectedAttribute(aValue, true);
    }

    @Override
    public long getLength() {
      return getWrappedNode().getOptionSize();
    }

    @Override
    public DOMHTMLOptionsCollection getOptions() {
      return new DOMHTMLOptionsCollection() {

        @Override
        /*
         * This method retrieves a {@link DOMHTMLOptionElement} using a name. It first searches for a node with a
         * matching id attribute. If it doesn't find one, it then searches for a Node with a matching name attribute,
         * but only on those elements that are allowed a name attribute. This method is case insensitive in HTML
         * documents and case sensitive in XHTML documents.
         */
        public DOMHTMLOptionElement namedItem(final String name) {

          final List<HtmlOption> options = getWrappedNode().getOptions();
          for (final HtmlOption o : options) {
            if ((o.getAttribute("id") != null) && o.getAttribute("id").equalsIgnoreCase(name))
              return new DOMHTMLOptionWrapHtmlUnit<HtmlOption>(o);
          }
          for (final HtmlOption o : options) {
            if ((o.getAttribute("name") != null) && o.getAttribute("name").equalsIgnoreCase(name))
              return new DOMHTMLOptionWrapHtmlUnit<HtmlOption>(o);
          }
          return null;
        }

        @Override
        public DOMHTMLOptionElement item(final long index) {
          return new DOMHTMLOptionWrapHtmlUnit<HtmlOption>(getWrappedNode().getOption((int) index));
        }

        @Override
        public long getLength() {

          return getWrappedNode().getOptionSize();
        }

        @Override
        public DOMHTMLOptionElement itemByValue(final String value) {
          try {
            return new DOMHTMLOptionWrapHtmlUnit<HtmlOption>(getWrappedNode().getOptionByValue(value));
          } catch (final ElementNotFoundException e) {
            // expected
          }
          return null;
        }

        @Override
        public DOMHTMLOptionElement itemByText(final String text) {
          try {
            return new DOMHTMLOptionWrapHtmlUnit<HtmlOption>(getWrappedNode().getOptionByText(text));
          } catch (final ElementNotFoundException e) {
            // expected
          }
          return null;
        }

      };
    }

    @Override
    public boolean getDisabled() {
      return getWrappedNode().isDisabled();
    }

    @Override
    public boolean getMultiple() {
      return getWrappedNode().isMultipleSelectEnabled();
    }

    @Override
    HtmlSelect getWrappedNode() {
      return this.htmlSelect;
    }

    @Override
    public void setSelectedByText(final String optiontext) {

      getWrappedNode().getOptionByText(optiontext).setSelected(true);

    }

    @Override
    public void setSelectedIndex(final int aSelectedIndex) {

      getWrappedNode().getOption(aSelectedIndex).setSelected(true);

    }

    @Override
    public DOMWindow selectOptionByText(final String optiontext) {

      try {
        final Page page = getWrappedNode().getOptionByText(optiontext).setSelected(true);
        LOGGER.debug("Selecting on {}, text  {}", this, optiontext);
        return new DOMWindowWrapHtmlUnit(page.getEnclosingWindow());
      } catch (final Exception e) {
        throw new WebAPIRuntimeException("IO error occurred during selecting on a SELECT", e, LOGGER);
      }

    }

    @Override
    public DOMWindow selectOptionIndex(final int aSelectedIndex) {

      try {
        final Page page = getWrappedNode().getOption(aSelectedIndex).setSelected(true);
        LOGGER.debug("Selecting on {}, option index {}", this, aSelectedIndex);
        return new DOMWindowWrapHtmlUnit(page.getEnclosingWindow());
      } catch (final Exception e) {
        throw new WebAPIRuntimeException("IO error occurred during selecting on a SELECT", e, LOGGER);
      }

    }

    @Override
    public String getType() {
      if (getWrappedNode().isMultipleSelectEnabled())
        return "select-multiple";
      return "select-one";
    }

    @Override
    public String getName() {
      return getWrappedNode().getAttribute("name");
    }

    @Override
    public void setName(final String aName) {
      getWrappedNode().setAttribute("name", aName);

    }

    @Override
    public int getSize() {
      return Integer.parseInt(getWrappedNode().getSizeAttribute());
    }

    @Override
    public void setSize(final int aSize) {
      getWrappedNode().setAttribute("size", aSize + "");
    }

    @Override
    public int getTabIndex() {

      return getWrappedNode().getTabIndex();

    }

    @Override
    public void setTabIndex(final int aTabIndex) {
      getWrappedNode().setAttribute("tabindex", aTabIndex + "");

    }

  }

  class DOMNodeWrapHtmlUnit<N extends DomNode> extends AbstractNodeWrapper<N> {
    protected final N wrappedNode;

    @Override
    public String toPrettyString() {

      return wrappedNode.getNodeName();
    }

    @Override
    public WebBrowser getBrowser() {
      return HTMLUnitBrowser212.this;
    }

    @Override
    public String toString() {
      return wrappedNode.getNodeName();
    }

    protected DOMNodeWrapHtmlUnit(final N node) {
      if (node == null)
        throw new WebAPIRuntimeException("Wrapped node null", LOGGER);
      this.wrappedNode = node;
    }

    @Override
    public Type getNodeType() {
      return noeTypes.get(wrappedNode.getNodeType());
    }

    /*
     * (non-Javadoc)
     *
     * @see uk.ac.ox.cs.diadem.webapi.dom.DOMNode#getDOMPropery(java.lang.String)
     */
    @Override
    public String getDOMProperty(final String domProperty) {
      LOGGER.debug("getDOMProperty not supported");
      throw new WebAPIRuntimeException("getDOMProperty not supported ", LOGGER);
    }

    @Override
    public DOMNodeList getChildNodes() {
      return new DOMNodeList() {

        @Override
        public Iterator<DOMNode> iterator() {
          return Iterators.transform(wrappedNode.getChildNodes().iterator(), new Function<DomNode, DOMNode>() {

            @Override
            public DOMNode apply(final DomNode input) {

              return wrapProperObject(input);
            }
          });

        }

        @Override
        public DOMNode item(final long index) {
          final DomNode item = Iterables.get(wrappedNode.getChildNodes(), (int) index);
          final DOMNode result = wrapProperObject(item);
          return result;
        }

        @Override
        public long getLength() {
          return wrappedNode.getChildNodes().size();
        }
      };
    }

    @Override
    public String getNodeValue() {
      return wrappedNode.getNodeValue();
    }

    @Override
    public DOMNode getParentNode() {
      return wrapProperObject(wrappedNode.getParentNode());
      // return new
      // DOMNodeWrapHtmlUnit<DomNode>(wrappedNode.getParentNode());
    }

    @Override
    public String getLocalName() {
      return wrappedNode.getLocalName();
    }

    @Override
    public DOMNamedNodeMap<DOMNode> getAttributes() {
      if (wrappedNode.getAttributes() == null)
        return null;
      return new DOMNamedNodeMap<DOMNode>() {

        @Override
        public DOMNode item(final int i) {
          final Node item = wrappedNode.getAttributes().item(i);
          return wrapProperObject(((DomNode) item));
        }

        @Override
        public DOMNode getNamedItem(final String name) {

          final Node namedItem = wrappedNode.getAttributes().getNamedItem(name);
          return wrapProperObject(((DomNode) namedItem));
        }

        @Override
        public long getLength() {
          return wrappedNode.getAttributes().getLength();
        }
      };
    }

    @Override
    public String getNodeName() {
      return wrappedNode.getNodeName();
    }

    @Override
    N getWrappedNode() {
      return wrappedNode;
    }

    @Override
    public String getTextContent() {
      return getWrappedNode().getTextContent();
    }

    @Override
    public DOMXPathEvaluator getXPathEvaluator() {
      return new DOMXPathEvaluatorOnHTMLUnitImpl();
    }

    @Override
    public short compareDocumentPosition(final DOMNode other) {
      // final DomNode n1 = ((DOMNodeWrapHtmlUnit<DomNode>) this).getWrappedNode();
      // final DomNode n2 = ((DOMNodeWrapHtmlUnit<DomNode>) other).getWrappedNode();
      return getWrappedNode().compareDocumentPosition(castToImplAndGetWrappedNode(other));
    }

    @SuppressWarnings("unchecked")
    private Node castToImplAndGetWrappedNode(final DOMNode other) {

      return ((DOMNodeWrapHtmlUnit<DomNode>) other).getWrappedNode();
    }

    @Override
    public DOMDocument getOwnerDocument() {
      return new DOMDocumentWrapHtmlUnit((HtmlPage) getWrappedNode().getOwnerDocument());
    }

    @Override
    public boolean isSameNode(final DOMNode other) {
      return getWrappedNode().isSameNode(castToImplAndGetWrappedNode(other));
    }

    @Override
    public void addEventListener(final String type, final DOMEventListener listener, final boolean useCapture) {
      throw new WebAPIRuntimeException("not implemented yet", LOGGER);

    }

    @Override
    public void removeEventListener(final String type, final DOMEventListener listener, final boolean useCapture) {
      throw new WebAPIRuntimeException("not implemented yet", LOGGER);

    }

    @Override
    public DOMNode appendChild(final DOMNode newChild) {
      LOGGER.debug("Appending Child node {} to {}", newChild, this);
      return wrapProperObject(getWrappedNode().appendChild(castToImplAndGetWrappedNode(newChild)));
    }

    @Override
    public DOMNode removeChild(final DOMNode child) {
      LOGGER.debug("Removing Child node {} from {}", child, this);
      return wrapProperObject(getWrappedNode().appendChild(castToImplAndGetWrappedNode(child)));
    }

    @SuppressWarnings("unchecked")
    @Override
    public DOMNode replaceChild(final DOMNode newChild, final DOMNode oldChild) {
      LOGGER.debug("Replacing Child node {} with {} ", oldChild, newChild);
      return wrapProperObject((N) getWrappedNode().replaceChild(castToImplAndGetWrappedNode(newChild),
          castToImplAndGetWrappedNode(oldChild)));
    }

    @SuppressWarnings("unchecked")
    @Override
    public DOMNode insertBefore(final DOMNode newChild, final DOMNode refChild) {
      LOGGER.debug("insert Child node {} before {} ", newChild, refChild);
      return wrapProperObject((N) getWrappedNode().insertBefore(castToImplAndGetWrappedNode(newChild),
          castToImplAndGetWrappedNode(refChild)));
    }

    @Override
    public void setTextContent(final String text) {
      getWrappedNode().setTextContent(text);
    }

    @Override
    public DOMNode getPreviousSibling() {
      return wrapProperObject(getWrappedNode().getPreviousSibling());
    }

    @Override
    public DOMNode getNextSibling() {
      return wrapProperObject(getWrappedNode().getNextSibling());
    }

    @Override
    public DOMNode getLastChild() {
      return wrapProperObject(getWrappedNode().getLastChild());
    }

    @Override
    public DOMNode getFirstChild() {
      return wrapProperObject(getWrappedNode().getFirstChild());
    }

    @Override
    public boolean isEqualNode(final DOMNode other) {
      return getWrappedNode().isEqualNode(castToImplAndGetWrappedNode(other));
    }

    // @Override
    // public DOMCSSStyleDeclaration getComputedStyle() {
    // LOGGER.debug("accessing internal HTMLUnit API for CSS ");
    //
    // return new DOMCSSStyleDeclarationWrapHtmlUnit(getHtmlUnitStyle());
    //
    // }
    //
    // protected ComputedCSSStyleDeclaration getHtmlUnitStyle() {
    // HTMLElement el = ((HTMLElement) wrappedNode.getScriptObject());
    //
    // ComputedCSSStyleDeclaration style = el.jsxGet_currentStyle();
    // return style;
    // }
    //
    // @Override
    // public DOMClientRect getClientRect() {
    // final int top = getHtmlUnitStyle().getTop(false, false, false);
    // final int left = getHtmlUnitStyle().getLeft(false, false, false);
    // final int width = getHtmlUnitStyle().getCalculatedWidth(false,
    // false);
    // final int height = getHtmlUnitStyle().getCalculatedHeight(false,
    // false);
    // return new DOMClientRectImplementation(top, width, height, left);
    //
    // }

  }

  class DOMElementWrapHtmlUnit<N extends DomElement> extends DOMNodeWrapHtmlUnit<DomElement> implements DOMElement {

    // private final DOMDocument doc_owner;

    public DOMElementWrapHtmlUnit(final N htmlElement, final DOMDocument documentWrapHtmlUnit) {
      super(htmlElement);
    }

    public DOMElementWrapHtmlUnit(final N htmlElement) {
      this(htmlElement, null);
    }

    @Override
    public DOMCSSStyleDeclaration getComputedStyle() {

      return new DOMCSSStyleDeclarationWrapHtmlUnit(getHtmlUnitStyle());

    }

    @Override
    public CSSMutationObserver observeCSSProperties(final CssProperty... properties) {
      LOGGER.debug("observeCSSProperties not supported");
      throw new WebAPIRuntimeException("observeCSSProperties not supported ", LOGGER);
    }

    @Override
    public List<DOMElement> getNeighbourhood(final int radius) {
      LOGGER.debug("getNeighbourhood not supported");
      throw new WebAPIRuntimeException("getNeighbourhood not supported ", LOGGER);
    }

    @Override
    HtmlElement getWrappedNode() {
      return (HtmlElement) wrappedNode;
    }

    protected ComputedCSSStyleDeclaration getHtmlUnitStyle() {
      LOGGER.debug("accessing internal HTMLUnit API for CSS ");
      final HTMLElement el = getScriptedObject();
      final ComputedCSSStyleDeclaration style = el.getCurrentStyle();
      return style;
    }

    private HTMLElement getScriptedObject() {
      LOGGER.debug("accessing scripted object ");
      final HTMLElement el = ((HTMLElement) getWrappedNode().getScriptObject());
      return el;
    }

    @Override
    public DOMBoundingClientRect getBoundingClientRect() {
      final ComputedCSSStyleDeclaration htmlUnitStyle = getHtmlUnitStyle();
      final int top = htmlUnitStyle.getTop(false, false, false);
      final int left = htmlUnitStyle.getLeft(false, false, false);
      final int width = htmlUnitStyle.getCalculatedWidth(false, false);
      final int height = htmlUnitStyle.getCalculatedHeight(false, false);
      return new DOMClientRectImplementation(top, width, height, left);

    }

    private final class DOMClientRectImplementation implements DOMBoundingClientRect {
      private final int top;
      private final int width;
      private final int height;
      private final int left;

      private DOMClientRectImplementation(final int top, final int width, final int height, final int left) {
        this.top = top;
        this.width = width;
        this.height = height;
        this.left = left;
      }

      @Override
      public float getWidth() {
        return width;
      }

      @Override
      public float getTop() {
        return top;
      }

      @Override
      public float getRight() {
        return left + width;
      }

      @Override
      public float getLeft() {
        return left;
      }

      @Override
      public float getHeight() {
        return height;
      }

      @Override
      public float getBottom() {
        return top + height;
      }
    }

    @Override
    public void setAttribute(final String name, final String value) {
      LOGGER.debug("setting attribute '{}' of value '{}': ", name, value);
      getWrappedNode().setAttribute(name, value);
    }

    @Override
    public String getAttribute(final String name) {
      return getWrappedNode().getAttribute(name);
    }

    @Override
    public String getInnerHTML() {
      return getScriptedObject().getInnerHTML();
    }

    @Override
    public String getOuterHTML() {

      return getScriptedObject().getOuterHTML();
    }

    @Override
    public boolean isEnabled() {

      return !wrappedNode.hasAttribute("disabled");
    }

    @Override
    public DOMWindow click() {
      try {
        LOGGER.debug("clicking on '{}': ", this);

        final String urlBefore = getWebBrowserCurrentTopWindow().getEnclosedPage().getUrl().toString();
        final Page page = getWrappedNode().click();
        if (!page.getUrl().toString().equals(urlBefore)) {
          pages++;
        }

        LOGGER.debug("clicking migh have opened another page '{}', but we stick with the current top level: ",
            page.getUrl());
        LOGGER
            .debug(
                "wait until all background JavaScript tasks scheduled to start executing before {}ms have finished executing.",
                DELAY_MILLIS);

        client.waitForBackgroundJavaScriptStartingBefore(DELAY_MILLIS);
        LOGGER.info("after click, current page is '{}' in window '{}': ", getWebBrowserCurrentTopWindow()
            .getEnclosedPage().getUrl(), getWebBrowserCurrentTopWindow());

        return new DOMWindowWrapHtmlUnit(getWebBrowserCurrentTopWindow());
      } catch (final IOException e) {
        LOGGER.debug("Error Clicking on '{}': ", this);
        throw new WebAPIRuntimeException("Problem while clicking on the element ", e, LOGGER);
      } catch (final FailingHttpStatusCodeException e) {
        LOGGER.error("Click action error: {}", e.getStatusMessage());
        throw new WebAPIRuntimeException(e.getMessage(), e, LOGGER);
      }
    }

    @Override
    public DOMWindow moveToFrame() {

      boolean nodeCheck = false;
      final String nodeName = getWrappedNode().getNodeName();
      if (nodeName.equalsIgnoreCase("iframe")) {
        nodeCheck = true;
      }
      if (nodeName.equalsIgnoreCase("frame")) {
        nodeCheck = true;
      }

      if (!nodeCheck) {
        LOGGER.error("MovetoFrame() only allowed on Iframe and Frame elements. Invalid current element {}", nodeName);
        throw new WebAPIRuntimeException(
            "MovetoFrame() only allowed on Iframe and Frame elements. Invalid current element " + nodeName, LOGGER);
      }
      final HtmlPage enclosedPage = (HtmlPage) getWebBrowserCurrentTopWindow().getEnclosedPage();
      final List<FrameWindow> frames = enclosedPage.getFrames();
      final Optional<FrameWindow> frameWindow = Iterables.tryFind(frames, new Predicate<FrameWindow>() {

        @Override
        public boolean apply(final FrameWindow input) {
          return input.getFrameElement().equals(getWrappedNode());
        }
      });

      if (!frameWindow.isPresent()) {
        LOGGER.error("Cannot retrieve the framewindow associated to this element {}", this);
        throw new WebAPIRuntimeException("Cannot retrieve the framewindow associated to this element " + this, LOGGER);
      }

      final HtmlPage frame = (HtmlPage) frameWindow.get().getEnclosedPage();
      // set the new page in the current window, this will update history too
      getWebBrowserCurrentTopWindow().setEnclosedPage(frame);
      LOGGER.info("after moving to the frame window, current page is '{}' in window '{}': ",
          getWebBrowserCurrentTopWindow().getEnclosedPage().getUrl());

      return new DOMWindowWrapHtmlUnit(getWebBrowserCurrentTopWindow());
    }

    @Override
    public DOMWindow moveToHREF() {

      final String url = this.getAttribute("href");
      if (url == null) {
        LOGGER.error("Cannot retrieve url from href attribute for element ", this);
        throw new WebAPIRuntimeException("Cannot retrieve url from href atytribtue for element " + this, LOGGER);
      }
      LOGGER.info("Navigating to url {}", url);
      // navigate to the iframe
      navigate(url);
      return new DOMWindowWrapHtmlUnit(getWebBrowserCurrentTopWindow());

    }

    @Override
    public boolean click(final boolean waitUntilLoaded) {
      // here there is no difference
      click();
      return true;
    }

    @Override
    public DOMWindow fireMouseEvent(final String event) {
      LOGGER.debug("fireMouseEvent {} on '{}': ", event, getNodeName());
      final ScriptResult fireEvent = getWrappedNode().fireEvent(event);
      LOGGER
          .debug(
              "wait until all background JavaScript tasks scheduled to start executing before {}ms have finished executing.",
              DELAY_MILLIS);
      client.waitForBackgroundJavaScriptStartingBefore(DELAY_MILLIS);
      LOGGER.debug("fireMouseEvent returned '{}': ", fireEvent);

      return new DOMWindowWrapHtmlUnit(getWebBrowserCurrentTopWindow());
    }

    @Override
    public DOMWindow fireFocusEvent(final String event) {
      LOGGER.debug("fireFocusEvent {} on '{}': ", event, getNodeName());
      final ScriptResult fireEvent = getWrappedNode().fireEvent(event);
      LOGGER
          .debug(
              "wait until all background JavaScript tasks scheduled to start executing before {}ms have finished executing.",
              DELAY_MILLIS);
      client.waitForBackgroundJavaScriptStartingBefore(DELAY_MILLIS);
      LOGGER.debug("fireFocusEvent returned '{}': ", fireEvent);
      return new DOMWindowWrapHtmlUnit(getWebBrowserCurrentTopWindow());
    }

    @Override
    public DOMWindow fireKeyboardEvent(final String event, final char key) {
      LOGGER.debug("fireKeyboardEvent {} on '{}': ", event, getNodeName());
      final boolean prevented = getScriptedObject().dispatchEvent(
          new KeyboardEvent(getWrappedNode(), event, key, false, false, false));
      LOGGER
          .debug(
              "wait until all background JavaScript tasks scheduled to start executing before {}ms have finished executing.",
              DELAY_MILLIS);
      client.waitForBackgroundJavaScriptStartingBefore(DELAY_MILLIS);
      LOGGER.debug("fireKeyboardEvent prevented '{}': ", prevented);
      return new DOMWindowWrapHtmlUnit(getWebBrowserCurrentTopWindow());
    }

    @Override
    public DOMWindow type(final String content) {
      try {
        LOGGER.debug("typying {} on '{}': ", content, getNodeName());
        getWrappedNode().type(content);
        LOGGER
            .debug(
                "wait until all background JavaScript tasks scheduled to start executing before {}ms have finished executing.",
                DELAY_MILLIS);
        client.waitForBackgroundJavaScriptStartingBefore(DELAY_MILLIS);
      } catch (final IOException e) {
        throw new WebAPIRuntimeException("Problem while typing on the element ", e, LOGGER);
      } catch (final FailingHttpStatusCodeException e) {
        LOGGER.error("Type action error: {}", e.getStatusMessage());
        throw new WebAPIRuntimeException(e.getMessage(), e, LOGGER);
      }
      return new DOMWindowWrapHtmlUnit(getWebBrowserCurrentTopWindow());
    }

    @Override
    public DOMWindow typeAndEnter(final String content) {
      try {
        LOGGER.debug("typying {} and return, on '{}': ", content, getNodeName());
        getWrappedNode().type(content + "\n");
        LOGGER
            .debug(
                "wait until all background JavaScript tasks scheduled to start executing before {}ms have finished executing.",
                DELAY_MILLIS);
        client.waitForBackgroundJavaScriptStartingBefore(DELAY_MILLIS);
      } catch (final IOException e) {
        throw new WebAPIRuntimeException("Problem while typing and pressing return on the element ", e, LOGGER);
      } catch (final FailingHttpStatusCodeException e) {
        LOGGER.error("typeAndEnter action error: {}", e.getStatusMessage());
        throw new WebAPIRuntimeException(e.getMessage(), e, LOGGER);
      }
      return new DOMWindowWrapHtmlUnit(getWebBrowserCurrentTopWindow());
    }

    @Override
    public DOMElement querySelector(final String selectors) {
      LOGGER.debug("Css querySelector {}", selectors);
      final DomNode querySelector = getWrappedNode().querySelector(selectors);
      return (DOMElement) wrapProperObject(querySelector);
    }

    @Override
    public DOMNodeList querySelectorAll(final String selectors) {
      LOGGER.debug("Css querySelectorAll {}", selectors);
      final DomNodeList<DomNode> list = getWrappedNode().querySelectorAll(selectors);
      return new DOMNodeList() {

        @Override
        public Iterator<DOMNode> iterator() {
          return Iterators.transform(list.iterator(), new Function<DomNode, DOMNode>() {

            @Override
            public DOMNode apply(final DomNode input) {
              return wrapProperObject(input);
            }

          });
        }

        @Override
        public DOMNode item(final long index) {
          final DomNode domNode = list.get((int) index);
          return wrapProperObject(domNode);
        }

        @Override
        public long getLength() {
          return list.size();
        }

      };
    }

    @Override
    public HTMLUtil htmlUtil() {
      return new HTMLUtil() {

        @Override
        public DOMTypeableElement asTypeableElement() {
          final DOMHTMLInputElement element = asHTMLInputElement();

          return element == null ? asHTMLTextArea() : element;
        }

        @Override
        public DOMHTMLSelect asHTMLSelect() {
          final HtmlElement element = getWrappedNode();
          if (element instanceof HtmlSelect) {
            final HtmlSelect select = (HtmlSelect) element;

            return new DOMHTMLSelectElementWrapHtmlUnit<HtmlSelect>(select);
          }
          LOGGER.error("Cannot cast {} to HtmlSelect", element);
          throw new WebAPIRuntimeException("Cannot cast to HtmlSelect", LOGGER);
        }

        @Override
        public DOMHTMLForm asHTMLForm() {
          LOGGER.error("asHTMLForm not implemented yet");
          throw new WebAPIRuntimeException("asHTMLForm not implemented yet", LOGGER);
        }

        @Override
        public DOMHTMLInputElement asHTMLInputElement() {
          final HtmlElement element = getWrappedNode();
          if (element instanceof HtmlInput) {
            final HtmlInput select = (HtmlInput) element;

            return new DOMHTMLInputElementWrapHtmlUnit<HtmlInput>(select);
          }
          return null;
        }

        @Override
        public DOMHTMLTextAreaElement asHTMLTextArea() {
          final HtmlElement element = getWrappedNode();
          if (element instanceof HtmlTextArea) {
            final HtmlTextArea select = (HtmlTextArea) element;

            return new DOMHTMLTextareaWrapHtmlUnit<HtmlTextArea>(select);
          }
          return null;
        }
      };
    }

    @Override
    public DOMWindow mouseover() {
      LOGGER.debug("mouseover on '{}': ", getNodeName());
      final Page page = getWrappedNode().mouseOver();
      LOGGER
          .debug(
              "wait until all background JavaScript tasks scheduled to start executing before {}ms have finished executing.",
              DELAY_MILLIS);
      client.waitForBackgroundJavaScriptStartingBefore(DELAY_MILLIS);
      LOGGER.debug("mouseover opened another window '{}', but we go on with the current top level", page.getUrl());
      LOGGER.info("after mouseover, current page is '{}' in window '{}' ", getWebBrowserCurrentTopWindow()
          .getEnclosedPage().getUrl(), getWebBrowserCurrentTopWindow());
      return new DOMWindowWrapHtmlUnit(getWebBrowserCurrentTopWindow());
    }

    @Override
    public DOMWindow focus() {
      LOGGER.debug("focus on '{}': ", getNodeName());
      final Page page = getWrappedNode().fireEvent(DOMFocusEvent.focus).getNewPage();
      LOGGER
          .debug(
              "wait until all background JavaScript tasks scheduled to start executing before {}ms have finished executing.",
              DELAY_MILLIS);
      client.waitForBackgroundJavaScriptStartingBefore(DELAY_MILLIS);
      LOGGER.debug("focus opened another window '{}', but we go on with the current top level", page.getUrl());
      LOGGER.info("after focus, current page is '{}' in window '{}' ", getWebBrowserCurrentTopWindow()
          .getEnclosedPage().getUrl(), getWebBrowserCurrentTopWindow());
      return new DOMWindowWrapHtmlUnit(getWebBrowserCurrentTopWindow());
    }

    @Override
    public DOMWindow keypress(final Key content) {
      LOGGER.debug("keypress '{}' on '{}': ", content, getNodeName());
      LOGGER
          .debug(
              "wait until all background JavaScript tasks scheduled to start executing before {}ms have finished executing.",
              DELAY_MILLIS);
      client.waitForBackgroundJavaScriptStartingBefore(DELAY_MILLIS);
      return this.fireKeyboardEvent(DOMKeyboardEvent.keypress, (char) content.getCode());
    }

    @Override
    public DOMWindow sendClick(final float x, final float y) {
      LOGGER.debug("sendClick not implemented");
      throw new WebAPIRuntimeException("not implemented ", LOGGER);
    }

    @Override
    public void removeAttribute(final String name) {
      LOGGER.debug("removeAttribute {} on {}", name, getNodeName());
      getWrappedNode().removeAttribute(name);

    }

    @Override
    public DOmElementOnJS js() {
      return null;
    }

  }

  class DOMDocumentWrapHtmlUnit extends DOMNodeWrapHtmlUnit<HtmlPage> implements DOMDocument {

    public DOMDocumentWrapHtmlUnit(final HtmlPage htmlPage) {
      super(htmlPage);
    }

    @Override
    public DOMRange createRange(final DOMNode startNode, final int startOffset, final DOMNode endNode,
        final int endOffSet) {
      LOGGER.debug("createRange not supported");
      throw new WebAPIRuntimeException("not supported ", LOGGER);
    }

    @Override
    public DOMElement getDocumentElement() {
      return new DOMElementWrapHtmlUnit<HtmlElement>(getWrappedNode().getDocumentElement(), this);
    }

    @Override
    public DOMNodeList getElementsByName(final String string) {
      return buildDOMNodeList(getWrappedNode().getElementsByName(string));
    }

    protected DOMNodeList buildDOMNodeList(final List<DomElement> list) {
      return new DOMNodeList() {

        @Override
        public Iterator<DOMNode> iterator() {
          return Iterators.transform(list.iterator(), new Function<DomElement, DOMNode>() {

            @Override
            public DOMNode apply(final DomElement input) {

              return new DOMElementWrapHtmlUnit<DomElement>(input);
            }

          });
        }

        @Override
        public DOMNode item(final long index) {
          return new DOMElementWrapHtmlUnit<DomElement>(list.get((int) index));
        }

        @Override
        public long getLength() {
          return list.size();
        }

      };
    }

    @Override
    public DOMNodeList getElementsByTagName(final String string) {
      return buildDOMNodeList(getWrappedNode().getElementsByTagName(string));
    }

    @Override
    public DOMElement getElementById(final String string) {
      return new DOMElementWrapHtmlUnit<DomElement>(getWrappedNode().getElementById(string));
    }

    @Override
    public DOMElement elementByPosition(final int x, final int y) {
      throw new WebAPIRuntimeException("elementByPosition not supported", LOGGER);
    }

    @Override
    public DOMWindow getEnclosingWindow() {
      return new DOMWindowWrapHtmlUnit(getWrappedNode().getEnclosingWindow());
    }

    @Override
    public DOMElement createElement(final String tagName) {
      throw new WebAPIRuntimeException("createElement not implemented yet", LOGGER);
    }

    @Override
    public DOMElement querySelector(final String selectors) {
      LOGGER.debug("Css querySelector {}", selectors);
      final DomNode querySelector = getWrappedNode().querySelector(selectors);
      return (DOMElement) wrapProperObject(querySelector);
    }

    @Override
    public DOMNodeList querySelectorAll(final String selectors) {
      LOGGER.debug("Css querySelectorAll {}", selectors);
      final DomNodeList<DomNode> list = getWrappedNode().querySelectorAll(selectors);
      return new DOMNodeList() {

        @Override
        public Iterator<DOMNode> iterator() {
          return Iterators.transform(list.iterator(), new Function<DomNode, DOMNode>() {

            @Override
            public DOMNode apply(final DomNode input) {
              return wrapProperObject(input);
            }

          });
        }

        @Override
        public DOMNode item(final long index) {
          final DomNode domNode = list.get((int) index);
          return wrapProperObject(domNode);
        }

        @Override
        public long getLength() {
          return list.size();
        }

      };
    }

    @Override
    public DOMElement selectElementBy(final DOMDocument.CRITERIA critera, final String expr) {
      throw new WebAPIRuntimeException("selectElementBy not supported yet on HTMLUnit", LOGGER);
    }

    @Override
    public Dimension getDimension() {
      throw new WebAPIRuntimeException("getDimension not supported yet on HTMLUnit", LOGGER);
    }

  }

  class DOMCSSStyleDeclarationWrapHtmlUnit implements DOMCSSStyleDeclaration {

    private final ComputedCSSStyleDeclaration style;

    public DOMCSSStyleDeclarationWrapHtmlUnit(final ComputedCSSStyleDeclaration style) {
      this.style = style;
    }

    @Override
    public String getPropertyValue(final String string) {
      return style.getPropertyValue(string);
    }

    @Override
    public void setProperty(final String name, final String value) {
      throw new WebAPIRuntimeException("not supported yet", LOGGER);

    }

    @Override
    public void setProperties(final Map<String, String> values) {
      final Set<String> keySet = values.keySet();
      for (final String name : keySet) {
        setProperty(name, values.get(name));
      }

    }

  }

  protected DOMNode wrapProperObject(final DomNode item) {
    try {
      if (item == null)
        return null;
      if (item.getNodeType() == Node.ELEMENT_NODE)
        return new DOMElementWrapHtmlUnit<HtmlElement>((HtmlElement) item);
      else if (item.getNodeType() == Node.DOCUMENT_NODE)
        return new DOMDocumentWrapHtmlUnit((HtmlPage) item.getPage());

      return new DOMNodeWrapHtmlUnit<DomNode>(item);
    } catch (final ClassCastException e) {
      throw new WebAPIRuntimeException("Unsupported element " + item.getClass().getSimpleName(), e, LOGGER);
    }
  }

  private final class DOMXPathEvaluatorOnHTMLUnitImpl implements DOMXPathEvaluator {
    @SuppressWarnings("unchecked")
    @Override
    public DOMXPathResult evaluate(final String expression, final DOMNode contextNode,
        final DOMXPathNSResolver resolver, final short type, final Object result) throws DOMXPathException {
      // LOGGER.debug("Evaluating XPath '{}' on context node '{}'", expression, contextNode.getNodeName());

      List<Object> res = null;
      try {
        res = (List<Object>) ((DOMNodeWrapHtmlUnit<DomNode>) contextNode).getWrappedNode().getByXPath(expression);
      } catch (final Throwable e) {
        // HTMLUnit can badly fail on XPath
        LOGGER.error("Failed getByXPath, returning empty result set. <'{}'>", ExceptionUtils.getMessage(e));
        res = Lists.newArrayList();

      }

      final List<Object> finalRes = res;
      // LOGGER.debug("xPath evaluation returned '{}' elemens", res.size());
      final Iterator<Object> resIterator = finalRes.iterator();
      return new DOMXPathResult() {

        @Override
        public DOMNode snapshotItem(final int index) throws DOMXPathException {

          return wrapProperObject((DomNode) finalRes.get(index));
        }

        @Override
        public DOMNode iterateNext() throws DOMXPathException {
          return wrapProperObject((DomNode) resIterator.next());
        }

        @Override
        public String getStringValue() throws DOMXPathException {
          return (String) finalRes.get(0);
        }

        @Override
        public long getSnapshotLength() throws DOMXPathException {
          return finalRes.size();
        }

        @Override
        public DOMNode getSingleNodeValue() throws DOMXPathException {
          return wrapProperObject((DomNode) finalRes.get(0));
        }

        @Override
        public short getResultType() {
          if (finalRes.isEmpty())
            return DOMXPathResult.UNORDERED_NODE_SNAPSHOT_TYPE;
          final Object object = finalRes.get(0);
          if (object instanceof Boolean)
            return DOMXPathResult.BOOLEAN_TYPE;
          if (object instanceof String)
            return DOMXPathResult.STRING_TYPE;
          if (object instanceof Double)
            return DOMXPathResult.NUMBER_TYPE;

          return DOMXPathResult.UNORDERED_NODE_SNAPSHOT_TYPE;
        }

        @Override
        public double getNumberValue() throws DOMXPathException {
          return (Double) finalRes.get(0);
        }

        @Override
        public boolean getInvalidIteratorState() {
          return false;
        }

        @Override
        public Boolean getBooleanValue() throws DOMXPathException {
          return (Boolean) finalRes.get(0);
        }
      };
    }

    @Override
    public DOMXPathNSResolver createNSResolver(final DOMNode nodeResolver) {
      return new DOMXPathNSResolver() {

        @Override
        public String lookupNamespaceURI(final String prefix) {
          throw new WebAPIRuntimeException("not supported", LOGGER);
        }
      };
    }

    @Override
    public DOMXPathExpression createExpression(final String expression, final DOMXPathNSResolver resolver)
        throws DOMXPathException {
      return new DOMXPathExpression() {

        @SuppressWarnings("unchecked")
        @Override
        public Object evaluate(final DOMNode contextNode, final short type, final Object result)
            throws DOMXPathException {

          return ((DOMNodeWrapHtmlUnit<DomNode>) contextNode).getWrappedNode().getByXPath(expression);
        }
      };
    }

    @Override
    public Table<DOMNode, String, List<DOMNode>> evaluateBulk(final DOMNode context, final String pathToAnchorNode,
        final Set<String> contextualAttributeNodes) {
      LOGGER.error("NOT implemented yet");
      throw new WebAPIRuntimeException("non supported yet", LOGGER);

    }

  }

  @Override
  public void shutdown() {

    if (client != null) {
      LOGGER.debug("shutdown closes all windows");
      client.closeAllWindows();
      client = null;
    }

  }

  @Override
  public boolean executeJavaScript(final String script) {
    LOGGER.error("NON implemented yet");
    throw new WebAPIRuntimeException("non unsupported yet", LOGGER);
  }

  @Override
  public void saveDocument(final String name) {
    LOGGER.error("NON implemented yet");
    throw new WebAPIRuntimeException("non unsupported yet", LOGGER);

  }

  @Override
  public void setWindowPosition(final int i, final int j) {
    LOGGER.error("setWindowPosition unsupported on HTMLUNIT. ");
  }

  @Override
  public Options manageOptions() {
    return new Options() {

      @Override
      public void enableOXPathOptimization(final boolean enabled) {
        // FIXME do nothing
      }

      @Override
      public void configureXPathLocatorHeuristics(final boolean useIdAttributeForXPathLocator,
          final boolean useClassAttributeForXPathLocator) {
        // FIXME do nothing

      }

      @Override
      public Boolean useIdAttributeForXPathLocator() {
        return false;
      }

      @Override
      public Boolean useClassAttributeForXPathLocator() {
        return false;
      }

      @Override
      public Boolean fallBackToJSExecutionOnNotInteractableElements() {
        // FIXME
        return false;
      }

      @Override
      public void setFallBackToJSExecutionOnNotInteractableElements(final boolean enableJSAsFallBack) {
        // FIXME do nothing

      }
    };
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.ox.cs.diadem.webapi.WebBrowser#switchToDefaultContent()
   */
  @Override
  public DOMWindow switchToDefaultContent() {
    LOGGER.error("NON implemented yet");
    throw new WebAPIRuntimeException("non unsupported yet", LOGGER);
  }

  /*
   * (non-Javadoc)
   * 
   * @see uk.ac.ox.cs.diadem.webapi.WebBrowser#switchToFrame(uk.ac.ox.cs.diadem.webapi.dom.DOMElement)
   */
  @Override
  public DOMWindow switchToFrame(final DOMElement frameElement) {
    LOGGER.error("NON implemented yet");
    throw new WebAPIRuntimeException("non unsupported yet", LOGGER);
  }

  /* (non-Javadoc)
   * @see uk.ac.ox.cs.diadem.webapi.MiniBrowser#getAdviceExecutor()
   */
  @Override
  public AdviceProcessor getAdviceProcessor() {
	  LOGGER.warn("getAdviceProcessor() not available for HTMLUnit Browser");
	  return new WebAdviceProcessor();
  }

}
