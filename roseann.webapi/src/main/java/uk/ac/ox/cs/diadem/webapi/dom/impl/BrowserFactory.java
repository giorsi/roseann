/*
 * Copyright (c)2011, DIADEM Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the DIADEM team nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DIADEM Team BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package uk.ac.ox.cs.diadem.webapi.dom.impl;

import java.util.Arrays;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.WebBrowser.ContentType;
import uk.ac.ox.cs.diadem.webapi.WebBrowser.FeatureType;

import com.google.common.base.Optional;
import com.google.common.collect.Sets;

/**
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Deparment of Computer Science
 */
public final class BrowserFactory {

  static final Logger logger = LoggerFactory.getLogger(BrowserFactory.class);

  /**
   * @author giog
   */
  public static enum Engine {
    /**
     * Htmlunit
     *
     * @deprecated
     */
    HTMLUNIT,
    /**
     * WebDriver on Firefox
     */
    WEBDRIVER_FF;
    ;

  }

  private BrowserFactory() {

    // prevent instantiation
  }

  /**
   * Create a new browser with defaul options. Use WebBrowserBuilder to configure the browser options.
   *
   * @param kind
   *          the kind of browser to use.
   * @return a new browser instance.
   */
  public static WebBrowser newWebBrowser(final Engine kind) {

    return new WebBrowserBuilder(kind).newWebBrowser();
  }

  public static WebBrowserBuilder newBuilder(final Engine kind) {
    return new WebBrowserBuilder(kind);
  }

  /**
   * Create a new browser
   *
   * @param kind
   *          the kind of browser to use.
   * @param visible
   *          this parameter has not effect
   * @return a new browser instance.
   * @deprecated use WebBrowserBuilder instead
   */
  @Deprecated
  public static WebBrowser newWebBrowser(final Engine kind, final Boolean visible) {

    return new WebBrowserBuilder(kind).newWebBrowser();
  }

  /**
   * @param enabledFeatures
   * @see #newWebBrowser(Engine, Boolean)
   * @deprecated use builder WebBrowserBuilder
   */
  @Deprecated
  public static WebBrowser newWebBrowser(final Engine kind, final Boolean visible,
      final Set<WebBrowser.FeatureType> disabledFeatures, final Set<FeatureType> enabledFeatures) {
    return new WebBrowserBuilder(kind).disable(disabledFeatures.toArray(new WebBrowser.FeatureType[0]))
        .enable(enabledFeatures.toArray(new WebBrowser.FeatureType[0])).newWebBrowser();
  }

  /**
   * @param enabledFeatures
   * @see #newWebBrowser(Engine, Boolean)
   * @deprecated use builder WebBrowserBuilder
   */
  @Deprecated
  public static WebBrowser newWebBrowser(final Engine kind, final String binary, final Boolean visible,
      final Set<WebBrowser.FeatureType> disabledFeatures, final Set<FeatureType> enabledFeatures) {
    return new WebBrowserBuilder(kind).useBinary(binary)
        .disable(disabledFeatures.toArray(new WebBrowser.FeatureType[0]))
        .enable(enabledFeatures.toArray(new WebBrowser.FeatureType[0])).newWebBrowser();

  }

  /**
   * Create a new browser that uses the given parentFrame as window. Only {@link Engine#SWT_MOZILLA} is supported, for
   * integration in Eclipse plugins for instance
   *
   * @param kind
   * @param visible
   * @param parentFrame
   * @return a new browser instance.
   * @deprecated
   */
  @Deprecated
  public static WebBrowser newWebBrowser(final Engine kind, final Boolean visible, final Object parentFrame) {

    return new WebBrowserBuilder(kind).newWebBrowser();

  }

  /**
   * @deprecated use {@link #newBuilder(Engine)}
   * @return
   */
  @Deprecated
  public static WebBrowserBuilder newBuilder() {
    return new WebBrowserBuilder();
  }

  public static class WebBrowserBuilder {

    Set<WebBrowser.FeatureType> disabledFeatures = Sets.newHashSet();
    Set<WebBrowser.FeatureType> enabledFeatures = Sets.newHashSet();
    private String displayNumber;
    private String binaryPath;
    private Engine kind;
    private boolean useXvfb;
    private boolean pluginsEnabled;
    Set<WebBrowser.ContentType> blockedContentTypesOnlyThirdParties = Sets.newHashSet();
    Set<WebBrowser.ContentType> blockedContentTypesAll = Sets.newHashSet();
    private Pair<String, String> location;

    @Deprecated
    public WebBrowserBuilder() {
      // Nothing
    }

    public WebBrowserBuilder(final Engine kind) {
      this.kind = kind;
    }

    public WebBrowserBuilder disable(final WebBrowser.FeatureType... features) {
      disabledFeatures.addAll(Arrays.asList(features));
      return this;
    }

    public WebBrowserBuilder enable(final WebBrowser.FeatureType... features) {
      enabledFeatures.addAll(Arrays.asList(features));
      return this;
    }

    /**
     * Indicates which display number to use
     *
     * @param displayNumber
     * @return
     */
    public WebBrowserBuilder useDisplay(final String displayNumber) {

      this.displayNumber = displayNumber;
      return this;
    }

    /**
     * Forces the usage of the provided firefox binary (effective only for {@link Engine#WEBDRIVER_FF})
     *
     * @param binaryPath
     * @return
     */
    public WebBrowserBuilder useBinary(final String binaryPath) {

      this.binaryPath = binaryPath;
      return this;
    }

    public WebBrowserBuilder useXvfb(final boolean useXvfb) {

      this.useXvfb = useXvfb;
      return this;
    }

    /**
     * Set activation of plugins
     *
     * @param pluginsEnabled
     * @return
     */
    public WebBrowserBuilder pluginsEnabled(final boolean pluginsEnabled) {
      this.pluginsEnabled = pluginsEnabled;
      return this;
    }

    // public WebBrowserBuilder useLocation(final String lat, final String lng) {
    // location = Pair.of(lat, lng);
    // return this;
    // }

    /**
     * Blocks the loading of any additional resource of the given content types coming from third-parties (external
     * websites)
     *
     * @param blockedThirdPartiesContentTypes
     * @return
     */
    public WebBrowserBuilder doNotLoadThirdPartyContent(final ContentType... blockedThirdPartiesContentTypes) {
      blockedContentTypesOnlyThirdParties.addAll(Arrays.asList(blockedThirdPartiesContentTypes));

      return this;
    }

    /**
     * Shortcut for {@link #doNotLoadThirdPartyContent(WebBrowser.ContentType.values())}
     * 
     * @return
     */
    public WebBrowserBuilder doNotLoadAnyThirdPartyContent() {
      blockedContentTypesOnlyThirdParties.addAll(Arrays.asList(WebBrowser.ContentType.values()));

      return this;
    }

    /**
     * Blocks the loading of any additional resource of the given content types
     *
     * @param blockedContentTypes
     * @return
     */
    public WebBrowserBuilder doNotLoadContent(final ContentType... blockedContentTypes) {
      blockedContentTypesAll.addAll(Arrays.asList(blockedContentTypes));
      return this;
    }

    /**
     * This is a shortcut for the invocatipon {@link #doNotLoadContent(WebBrowser.ContentType.values()))}. This will
     * block the loading of any additional resource resulting in only the raw html of the page.
     *
     * @return
     */
    public WebBrowserBuilder loadOnlyRawHtml() {
      return doNotLoadContent(WebBrowser.ContentType.values());
    }

    /**
     * Creates the browser with the provided information
     *
     * @return
     */
    public WebBrowser newWebBrowser() {
      final BrowserConfiguration browserConf = new BrowserConfiguration(Optional.fromNullable(binaryPath),
          Optional.fromNullable(displayNumber), useXvfb, disabledFeatures, enabledFeatures, pluginsEnabled,
          blockedContentTypesAll, blockedContentTypesOnlyThirdParties, Optional.fromNullable(location));
      return BrowserFactory.newWebBrowser(kind, browserConf);

    }

    /**
     * Creates the browser
     *
     * @param kind
     * @param visible
     *          i
     * @return
     * @deprecated use WebBrowserBuilder#
     */
    @Deprecated
    public WebBrowser newWebBrowser(final Engine kind, final Boolean visible) {
      return BrowserFactory.newWebBrowser(kind, false, disabledFeatures, enabledFeatures);
    }

    /**
     *
     * @param kind
     * @param binary
     * @param visible
     * @return
     * @deprecated use WebBrowserBuilder#
     */
    @Deprecated
    public WebBrowser newWebBrowser(final Engine kind, final String binary, final Boolean visible) {
      return BrowserFactory.newWebBrowser(kind, binary, false, disabledFeatures, enabledFeatures);
    }

  }

  private static WebBrowser newWebBrowser(final Engine kind, final BrowserConfiguration browserConf) {
    WebBrowser b = null;
    switch (kind) {
    case HTMLUNIT:
      b = new HTMLUnitBrowser212();
      for (final WebBrowser.FeatureType f : browserConf.disabledFeatures) {
        b.disableFeatures(f);
      }
      return b;
    case WEBDRIVER_FF:

      return new WebDriverBrowserImpl(browserConf);

    default:
      logger.error("Unexpected web browser type {}", kind);
      return null;
    }
  }

  static class BrowserConfiguration {
    Optional<String> binary;
    Optional<String> displayNumber;
    boolean useXvfb;
    Set<FeatureType> disabledFeatures;
    Set<FeatureType> enabledFeatures;
    boolean pluginsEnabled;
    Set<ContentType> blockedContentTypesAll;
    Set<ContentType> blockedContentTypesOnlyThirdParties;
    Optional<Pair<String, String>> location;

    BrowserConfiguration() {

    }

    BrowserConfiguration(final BrowserConfiguration toClone) {

      binary = toClone.binary;
      displayNumber = toClone.displayNumber;
      useXvfb = toClone.useXvfb;
      disabledFeatures = Sets.newHashSet(toClone.disabledFeatures);
      enabledFeatures = Sets.newHashSet(toClone.enabledFeatures);
      pluginsEnabled = toClone.pluginsEnabled;
      blockedContentTypesAll = Sets.newHashSet(toClone.blockedContentTypesAll);
      blockedContentTypesOnlyThirdParties = Sets.newHashSet(toClone.blockedContentTypesOnlyThirdParties);
      location = toClone.location;

    }

    BrowserConfiguration(final Optional<String> binary, final Optional<String> displayNumber, final boolean useXvfb,
        final Set<FeatureType> disabledFeatures, final Set<FeatureType> enabledFeatures, final boolean pluginsEnabled,
        final Set<ContentType> blockedContentTypesAll, final Set<ContentType> blockedContentTypesOnlyThirdParties,
        final Optional<Pair<String, String>> location) {

      this.binary = binary;
      this.displayNumber = displayNumber;
      this.useXvfb = useXvfb;
      this.disabledFeatures = disabledFeatures;
      this.enabledFeatures = enabledFeatures;
      this.pluginsEnabled = pluginsEnabled;
      this.blockedContentTypesAll = blockedContentTypesAll;
      this.blockedContentTypesOnlyThirdParties = blockedContentTypesOnlyThirdParties;
      this.location = location;

    }
  }

}
