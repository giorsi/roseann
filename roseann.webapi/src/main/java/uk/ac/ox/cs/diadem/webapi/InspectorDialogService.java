/*
 * Copyright (c)2011, DIADEM Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the DIADEM team nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DIADEM Team BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package uk.ac.ox.cs.diadem.webapi;

import java.util.Arrays;

import uk.ac.ox.cs.diadem.util.misc.ToStringUtils;
import uk.ac.ox.cs.diadem.webapi.dom.DOMWindow;

import com.google.common.base.Objects;
import com.google.common.base.Objects.ToStringHelper;

/**
 * Implementation of {@link DialogsService} that simply prints on System.out the information of each dialogs' component.
 *
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University Department of Computer Science
 */
public class InspectorDialogService implements DialogsService {

  @Override
  public boolean select(final DOMWindow aParent, final String aDialogTitle, final String aText, final long aCount,
      final String[] aSelectList, final int[] aOutSelection) {

    final ToStringHelper stringHelper = Objects.toStringHelper("select").add("title", aDialogTitle).add("Text", aText)
        .add("List box", string(aSelectList));
    System.out.println(stringHelper.toString());
    return false;
  }

  private String string(final String[] aSelectList) {

    return ToStringUtils.getInstance().toString(Arrays.asList(aSelectList));
  }

  @Override
  public boolean promptUsernameAndPassword(final DOMWindow aParent, final String aDialogTitle, final String aText,
      final String[] aUsername, final String[] aPassword, final String aCheckMsg, final boolean[] aCheckState) {

    final ToStringHelper stringHelper = Objects.toStringHelper("promptUsernameAndPassword").add("title", aDialogTitle)
        .add("Text", aText).add("username", string(aUsername)).add("password", string(aPassword))
        .add("checkbox msg", aCheckMsg).add("checked status", aCheckState[0]);
    System.out.println(stringHelper.toString());
    return false;
  }

  @Override
  public boolean promptPassword(final DOMWindow aParent, final String aDialogTitle, final String aText,
      final String[] aPassword, final String aCheckMsg, final boolean[] aCheckState) {

    final ToStringHelper stringHelper = Objects.toStringHelper("promptPassword").add("title", aDialogTitle)
        .add("Text", aText).add("password", string(aPassword)).add("checkbox msg", aCheckMsg)
        .add("checked status", aCheckState[0]);
    System.out.println(stringHelper.toString());
    return false;
  }

  @Override
  public boolean prompt(final DOMWindow aParent, final String aDialogTitle, final String aText, final String[] aValue,
      final String aCheckMsg, final boolean[] aCheckState) {

    final ToStringHelper stringHelper = Objects.toStringHelper("prompt").add("title", aDialogTitle).add("Text", aText)
        .add("defalut value", string(aValue)).add("checkbox msg", aCheckMsg).add("checked status", aCheckState[0]);
    System.out.println(stringHelper.toString());
    return false;
  }

  @Override
  public int confirmEx(final DOMWindow aParent, final String aDialogTitle, final String aText, final long aButtonFlags,
      final String aButton0Title, final String aButton1Title, final String aButton2Title, final String aCheckMsg,
      final boolean[] aCheckState) {

    final ToStringHelper stringHelper = Objects.toStringHelper("confirmEx").add("title", aDialogTitle)
        .add("Text", aText).add("Button 0 title", aButton0Title).add("Button 1 title", aButton1Title)
        .add("Button 2 title", aButton2Title).add("checkbox msg", aCheckMsg).add("checked status", aCheckState[0]);
    System.out.println(stringHelper.toString());
    return 0;
  }

  @Override
  public boolean confirmCheck(final DOMWindow aParent, final String aDialogTitle, final String aText,
      final String aCheckMsg, final boolean[] aCheckState) {

    final ToStringHelper stringHelper = Objects.toStringHelper("confirmCheck").add("title", aDialogTitle)
        .add("Text", aText).add("checkbox msg", aCheckMsg).add("checked status", aCheckState[0]);
    System.out.println(stringHelper.toString());
    return false;
  }

  @Override
  public boolean confirm(final DOMWindow aParent, final String aDialogTitle, final String aText) {

    final ToStringHelper stringHelper = Objects.toStringHelper("confirm").add("title", aDialogTitle).add("Text", aText);
    System.out.println(stringHelper.toString());
    return false;
  }

  @Override
  public void alertCheck(final DOMWindow aParent, final String aDialogTitle, final String aText,
      final String aCheckMsg, final boolean[] aCheckState) {

    final ToStringHelper stringHelper = Objects.toStringHelper("alertCheck").add("title", aDialogTitle)
        .add("Text", aText).add("checkbox msg", aCheckMsg).add("checked status", aCheckState[0]);
    System.out.println(stringHelper.toString());
  }

  @Override
  public void alert(final DOMWindow aParent, final String aDialogTitle, final String aText) {

    final ToStringHelper stringHelper = Objects.toStringHelper("alert").add("title", aDialogTitle).add("Text", aText);
    System.out.println(stringHelper.toString());
  }
}