package uk.ac.ox.cs.diadem.webapi;

import org.slf4j.Logger;

import uk.ac.ox.cs.diadem.env.DiademException;

public class WebAPIException extends DiademException {
  public WebAPIException(final String message, final Logger logger) {
    super(message, logger);
  }

  public WebAPIException(final String message, final Throwable cause, final Logger logger) {
    super(message, cause, logger);
  }

  private static final long serialVersionUID = 1L; // TODO

}