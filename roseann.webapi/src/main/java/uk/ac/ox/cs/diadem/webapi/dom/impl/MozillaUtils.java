/**
 * Copyright (c)2011, DIADEM Team

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
   This source code contains partially replicated parts from the work XPCOMUtils within the JBrowser project.
 */
package uk.ac.ox.cs.diadem.webapi.dom.impl;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.DiademRuntimeException;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.util.misc.DiademInstallationSupport;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.DOMDocument;
import uk.ac.ox.cs.diadem.webapi.dom.DOMElement;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode;
import uk.ac.ox.cs.diadem.webapi.utils.XPathUtil;

/**
 * Utilities for Mozilla.
 */
public class MozillaUtils {

  static final Logger logger = LoggerFactory.getLogger(MozillaUtils.class);

  /**
   * The user agent string to use
   */
  // private static final String MOZILLA_UA_LINUX =
  // ConfigurationFacility.getConfiguration().getString("webapi.useragent");

  // "Mozilla/5.0 (X11; U; Linux x86_64; en-GB; rv:1.9.2.17) Gecko/20110422 Ubuntu/10.10 (maverick) Firefox/3.6.17";

  static String getExtractionPath() {
    if (SystemUtils.IS_OS_LINUX && SystemUtils.OS_ARCH.contains("64"))
      return FilenameUtils.separatorsToSystem(DiademInstallationSupport.getDiademHome() + File.separator
          + ConfigurationFacility.getConfiguration().getString("installation-path.xulrunner.linux64"));

    if (SystemUtils.IS_OS_LINUX && SystemUtils.OS_ARCH.contains("32"))
      return FilenameUtils.separatorsToSystem(DiademInstallationSupport.getDiademHome() + File.separator
          + ConfigurationFacility.getConfiguration().getString("installation-path.xulrunner.linux32"));

    if (SystemUtils.IS_OS_WINDOWS)
      return FilenameUtils.separatorsToSystem(DiademInstallationSupport.getDiademHome() + File.separator
          + ConfigurationFacility.getConfiguration().getString("installation-path.xulrunner.win32"));

    throw new DiademRuntimeException("Unsupported platform for browser", logger);
  }

  public static void addDemo(final DOMDocument document) {
    final DOMNode head = document.getElementsByTagName("head").item(0);
    final DOMElement script1 = document.createElement("script");
    script1.setAttribute("src", "https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery.min.js");
    script1.setAttribute("type", "text/javascript");
    script1.setAttribute("charset", "utf-8");
    head.appendChild(script1);

    // final DOMElement script1 = document.createElement("script");
    // script1.setAttribute("src", "http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js");
    // script1.setAttribute("type", "text/javascript");
    // script1.setAttribute("charset", "utf-8");
    // head.appendChild(script1);

  }

  // static Class<?> getMarkerClass() {
  // if (SystemUtils.IS_OS_LINUX && SystemUtils.OS_ARCH.contains("64"))
  // return XulrunnerLinux64.class;
  //
  // if (SystemUtils.IS_OS_LINUX && SystemUtils.OS_ARCH.contains("32"))
  // return XulrunnerLinux32.class;
  //
  // if (SystemUtils.IS_OS_WINDOWS)
  // return XulrunnerWin32.class;
  //
  // throw new DiademRuntimeException("Unsupported platform for browser", logger);
  // }

  // /**
  // * Installs Xulrunner if necessary and setd the org.eclipse.swt.browser.XULRunnerPath property accordingly
  // */
  // public static void installXULRunnerEnvironment() {
  // final String path = getExtractionPath();
  // installEnvironment(getMarkerClass(), path);
  // System.setProperty("org.eclipse.swt.browser.XULRunnerPath", path);
  // }

  protected static void installEnvironment(final Class<?> marker, final String path) {
    MozillaUtils.logger.debug("installing environment for {} ", marker);
    // check if override is wanted
    final String override = System.getProperty(ConfigurationFacility.getConfiguration().getString(
        "override-property-name"));
    boolean forceOverride = false;
    if (override != null) {
      forceOverride = Boolean.parseBoolean(override);
      if (forceOverride) {
        logger.debug("forced override of environmnent for {} ", marker);
      }
    }
    final boolean skip = (new File(path).exists()) && !forceOverride;
    if (skip) {
      MozillaUtils.logger.debug("...skipped as already existing, using environment in {}", path);
    } else {
      DiademInstallationSupport.installFromClass(marker);
    }
    MozillaUtils.logger.debug("...environment {} installed in {}", marker, path);
  }

  // private static String guessIID(final Class<?> c) {
  //
  // try {
  // final String name = c.getName();
  // final String baseName = c.getSimpleName();
  // final String iidFieldName;
  // if (name.startsWith("org.mozilla.interfaces.ns")) {
  //        iidFieldName = String.format("NS_%s_IID", baseName.substring(2).toUpperCase()); //$NON-NLS-1$
  // } else {
  //        iidFieldName = String.format("%s_IID", baseName.toUpperCase()); //$NON-NLS-1$
  // }
  // final Field f = c.getDeclaredField(iidFieldName);
  // final String iid = (String) f.get(c);
  // return iid;
  // } catch (final Throwable e) {
  //      MozillaUtils.logger.error("failed to resolve IID of an XPCOM interface", e); //$NON-NLS-1$
  // return null;
  // }
  // }

  // private static String guessCID(final Class<?> c) {
  //
  // try {
  // final String baseName = c.getSimpleName();
  //      final String iidFieldName = String.format("%s_CID", baseName.toUpperCase()); //$NON-NLS-1$
  // final Field f = c.getDeclaredField(iidFieldName);
  // final String cid = (String) f.get(c);
  // return cid;
  // } catch (final Throwable e) {
  //      MozillaUtils.logger.error("failed to resolve CID of an XPCOM component", e); //$NON-NLS-1$
  // return null;
  // }
  // }

  /**
   * Creates an XPCOM proxy for the given XPCOM object. Methods of the proxy can be then safely called from non-Mozilla
   * threads.
   * <p>
   * This method creates a synchronous proxy. Therefore, calling a method on the proxy will block the calling thread
   * until the method finishes.
   * 
   * @param <T>
   *          - an XPCOM interface (org.mozilla.interfaces)
   * @param obj
   *          - XPCOM object
   * @param c
   *          - interface of the XPCOM proxy to be returned
   * @return synchronous XPCOM proxy
   */
  // public static <T extends nsISupports> T proxy(final nsISupports obj, final Class<T> c) {
  //
  // try {
  // final Mozilla moz = Mozilla.getInstance();
  // final String iid = MozillaUtils.guessIID(c);
  // final nsIServiceManager sm = moz.getServiceManager();
  // final nsIThreadManager tm = (nsIThreadManager) sm.getServiceByContractID(
  //          "@mozilla.org/thread-manager;1", nsIThreadManager.NS_ITHREADMANAGER_IID); //$NON-NLS-1$
  // final nsIThread mt = tm.getMainThread();
  // final nsIProxyObjectManager pm = (nsIProxyObjectManager) sm.getService(
  //          "{eea90d41-b059-11d2-915e-c12b696c9333}", nsIProxyObjectManager.NS_IPROXYOBJECTMANAGER_IID); //$NON-NLS-1$
  // final T t = (T) pm.getProxyForObject(mt, iid, obj, nsIProxyObjectManager.INVOKE_SYNC);
  // return t;
  // } catch (final Throwable e) {
  //      MozillaUtils.logger.error("failed to create XPCOM proxy", e); //$NON-NLS-1$
  // return null;
  // }
  // }

  /**
   * Creates an XPCOM proxy for the given XPCOM object. Methods of the proxy can be then safely called from non-Mozilla
   * threads.
   * <p>
   * This method creates an asynchronous proxy. Therefore, a call of a method on the proxy will return immediately. The
   * body of the method will be executed later on the Mozilla thread.
   * 
   * @param <T>
   *          - an XPCOM interface (org.mozilla.interfaces)
   * @param obj
   *          - XPCOM object
   * @param c
   *          - interface of the XPCOM proxy to be returned
   * @return asynchronous XPCOM proxy
   */
  // public static <T extends nsISupports> T asyncProxy(final nsISupports obj, final Class<T> c) {
  //
  // try {
  // final Mozilla moz = Mozilla.getInstance();
  // final String iid = MozillaUtils.guessIID(c);
  // final nsIServiceManager sm = moz.getServiceManager();
  // final nsIThreadManager tm = (nsIThreadManager) sm.getServiceByContractID(
  //          "@mozilla.org/thread-manager;1", nsIThreadManager.NS_ITHREADMANAGER_IID); //$NON-NLS-1$
  // final nsIThread mt = tm.getMainThread();
  // final nsIProxyObjectManager pm = (nsIProxyObjectManager) sm.getService(
  //          "{eea90d41-b059-11d2-915e-c12b696c9333}", nsIProxyObjectManager.NS_IPROXYOBJECTMANAGER_IID); //$NON-NLS-1$
  // final T t = (T) pm.getProxyForObject(mt, iid, obj, nsIProxyObjectManager.INVOKE_ASYNC);
  // return t;
  // } catch (final Throwable e) {
  //      MozillaUtils.logger.error("failed to create XPCOM proxy", e); //$NON-NLS-1$
  // return null;
  // }
  // }

  /**
   * Creates a new XPCOM object with the given contract ID. This method is an equivalent of the
   * <tt>do_CreateInstance()</tt> macro in mozilla source code. For example,
   * 
   * <pre>
   * create(&quot;@mozilla.org/timer;1&quot;, nsITimer.class)
   * </pre>
   * 
   * creates a new XPCOM timer object and returns its <tt>nsITimer</tt> interface. Similarly,
   * 
   * <pre>
   * create(&quot;@mozilla.org/timer;1&quot;, nsISupports.class)
   * </pre>
   * 
   * creates an XPCOM timer object and returns its <tt>nsISuppors</tt> interface.
   * 
   * @param <T>
   *          - an XPCOM interface (org.mozilla.interfaces)
   * @param contractID
   *          - identifier of the XPCOM object to create
   * @param c
   *          - interface of the new XPCOM object to be returned
   * @return newly created XPCOM object
   */
  // public static <T extends nsISupports> T create(final String contractID, final Class<T> c) {
  //
  // try {
  // final Mozilla moz = Mozilla.getInstance();
  // final String iid = MozillaUtils.guessIID(c);
  // final nsIComponentManager componentManager = moz.getComponentManager();
  // final T t = (T) componentManager.createInstanceByContractID(contractID, null, iid);
  // return t;
  // } catch (final Throwable e) {
  //      MozillaUtils.logger.error("failed to create XPCOM object", e); //$NON-NLS-1$
  // return null;
  // }
  // }

  /**
   * Creates a new XPCOM object with the given contract ID. See {@link MozillaUtils#create(String, Class) create()} for
   * more details.
   * <p>
   * Moreover, this method can be called from non-Mozilla threads and returns a <i>proxy</i> of the XPCOM object.
   * Methods of the <i>proxy</i> can be then safely called from non-Mozilla threads.
   * 
   * @param <T>
   *          - an XPCOM interface (org.mozilla.interfaces)
   * @param contractID
   *          - identifier of the XPCOM object to create
   * @param c
   *          - interface of the new XPCOM object to be returned
   * @return proxy of newly created XPCOM object
   */
  // public static <T extends nsISupports> T createProxy(final String contractID, final Class<T> c) {
  //
  // try {
  // final Mozilla moz = Mozilla.getInstance();
  // final String iid = MozillaUtils.guessIID(c);
  // final nsIComponentManager componentManager = MozillaUtils.proxy(moz.getComponentManager(),
  // nsIComponentManager.class);
  // final T t1 = (T) componentManager.createInstanceByContractID(contractID, null, iid);
  // final T t2 = MozillaUtils.proxy(t1, c);
  // return t2;
  // } catch (final Throwable e) {
  //      MozillaUtils.logger.error("failed to create XPCOM object", e); //$NON-NLS-1$
  // return null;
  // }
  // }

  /**
   * Returns XPCOM service with the given contract ID. This method is an equivalent of the <tt>do_GetService()</tt>
   * macro in mozilla source code. For example,
   * 
   * <pre>
   * getService(&quot;@mozilla.org/cookiemanager;1&quot;, nsICookieManager.class)
   * </pre>
   * 
   * returns the <tt>nsICookieManager</tt> interface of the Mozilla's cookie service. Similarly,
   * 
   * <pre>
   * getService(&quot;@mozilla.org/cookiemanager;1&quot;, nsICookieService.class)
   * </pre>
   * 
   * returns the <tt>nsICookieService</tt> interface of the cookie service.
   * 
   * @param <T>
   *          - an XPCOM interface (org.mozilla.interfaces)
   * @param contractID
   *          - identifier of the XPCOM service
   * @param c
   *          - interface of the XPCOM service to be returned
   * @return XPCOM service
   */
  // public static <T extends nsISupports> T getService(final String contractID, final Class<T> c) {
  //
  // try {
  // final Mozilla moz = Mozilla.getInstance();
  // final String iid = MozillaUtils.guessIID(c);
  // final nsIServiceManager serviceManager = moz.getServiceManager();
  // final T t = (T) serviceManager.getServiceByContractID(contractID, iid);
  // return t;
  // } catch (final Throwable e) {
  //      MozillaUtils.logger.error("failed to create XPCOM service", e); //$NON-NLS-1$
  // return null;
  // }
  // }

  /**
   * Returns XPCOM service with the given contract ID. See {@link MozillaUtils#getService(String, Class) getService} for
   * more details.
   * <p>
   * Moreover, this method can be called from non-Mozilla threads and returns a <i>proxy</i> of the XPCOM service.
   * Methods of the <i>proxy</i> can be then safely called from non-Mozilla threads.
   * 
   * @param <T>
   *          - an XPCOM interface (org.mozilla.interfaces)
   * @param contractID
   *          - identifier of the XPCOM service
   * @param c
   *          - interface of the XPCOM service to be returned
   * @return proxy of XPCOM service
   */
  // public static <T extends nsISupports> T getServiceProxy(final String contractID, final Class<T> c) {
  //
  // try {
  // final Mozilla moz = Mozilla.getInstance();
  // final String iid = MozillaUtils.guessIID(c);
  // final nsIServiceManager serviceManager = MozillaUtils.proxy(moz.getServiceManager(), nsIServiceManager.class);
  // final T t1 = (T) serviceManager.getServiceByContractID(contractID, iid);
  // final T t2 = MozillaUtils.proxy(t1, c);
  // return t2;
  // } catch (final Throwable e) {
  //      MozillaUtils.logger.error("failed to create XPCOM service", e); //$NON-NLS-1$
  // return null;
  // }
  // }

  /**
   * Returns a different interface of the given XPCOM object. This method is an equivalent of the
   * <tt>do_QueryInterface()</tt> macro in mozilla source code.
   * <p>
   * For example, the Mozilla's cookie service implements the following interfaces
   * 
   * <pre>
   *   nsICookieService
   *   nsICookieManager
   *   nsICookieManager2
   *   nsIObserver
   *   nsISupportsWeakReference
   * </pre>
   * 
   * Obtaining another interfaces of an XPCOM object is done using the following code snippet
   * 
   * <pre>
   *   nsICookieManager cm = ....
   *   nsIObserver os = qi(cm, nsIObserver.class);
   * </pre>
   * 
   * @param <T>
   *          - an XPCOM interface (org.mozilla.interfaces)
   * @param obj
   *          - an XPCOM object
   * @param c
   *          - interface of the XPCOM object to be returned
   * @return XPCOM object
   */
  // public static <T extends nsISupports> T qi(final nsISupports obj, final Class<T> c) {
  //
  // try {
  // if (obj == null)
  // return null;
  // final String iid = MozillaUtils.guessIID(c);
  // final T t = (T) obj.queryInterface(iid);
  // return t;
  // } catch (final XPCOMException e) {
  // // do not print an error if
  // // obj does not implement the interface
  // if (e.errorcode != IXPCOMError.NS_ERROR_NO_INTERFACE) {
  //        MozillaUtils.logger.error("failed to query-interface an XPCOM object", e); //$NON-NLS-1$
  // }
  // return null;
  // } catch (final Throwable e) {
  //      MozillaUtils.logger.error("failed to query-interface an XPCOM object", e); //$NON-NLS-1$
  // return null;
  // }
  // }

  /**
   * Registers a new factory for the XPCOM objects and services.
   * <p>
   * For example, this code registers a new factory for the timer XPCOM object.
   * 
   * <pre>
   *   nsIFactory myTimerFactory = ...
   *   register("@mozilla.org/timer;1", myTimerFactory);
   * </pre>
   * <p>
   * Note, some XPCOM objects (such as XPCOM services) are singletons. Therefore, if a custom factory is registered when
   * the singleton already exists, the method {@link nsIFactory#createInstance(nsISupports, String) createInstance()} of
   * the factory will be never called.
   * 
   * @param contractID
   *          - identifier of the XPCOM objects this factory creates
   * @param factory
   *          - factory for XPCOM objects
   */
  // public static void register(final String contractID, final nsIFactory factory) {
  //
  // final Mozilla moz = Mozilla.getInstance();
  // final nsIComponentRegistrar componentRegistrar = moz.getComponentRegistrar();
  // final String name = factory.getClass().getSimpleName();
  // final String cid = MozillaUtils.guessCID(factory.getClass());
  // componentRegistrar.registerFactory(cid, name, contractID, factory);
  // }

  // /**
  // * Execute a callable within the display thread
  // *
  // * @param <T>
  // * The type of return value
  // * @param display
  // * The display to use for executing the callable
  // * @param callable
  // * The callable to execute
  // * @return Returns the return value from {@code Callable#call()}
  // * @throws Exception
  // * If {@code Callable#call()} throws an exception
  // */
  // public static <T> T syncExec(final Display display, final Callable<T> callable) {
  //
  // final Result<T> result = new Result<T>();
  // if (display.isDisposed())
  // return result.result;
  // display.syncExec(new Runnable() {
  //
  // @Override
  // public void run() {
  //
  // try {
  // result.result = callable.call();
  // } catch (final Exception e) {
  // result.exception = e;
  // }
  // }
  // });
  // if (result.exception != null)
  // throw new RuntimeException(result.exception);
  // return result.result;
  // }

  // private static class Result<T> {
  //
  // T result;
  // Exception exception;
  // }

  /**
   * Registers a new component in the browser to deal with dialogs
   * 
   * @param service
   * @param wrapperFactory
   */
  // public static void registerNewPrompt(final DialogsService service, final WrapperDOMCallbackFactory wrapperFactory)
  // {
  //
  // // nsIServiceManager sm = Mozilla.getInstance().getServiceManager();
  // final nsIComponentRegistrar registrar = Mozilla.getInstance().getComponentRegistrar();
  // final String NS_PROMPTSERVICE_CID = "a2112d6a-0e28-421f-b46a-25c0b308cbd0";
  // final String NS_PROMPTSERVICE_CONTRACTID = "@mozilla.org/embedcomp/prompt-service;1";
  // registrar.registerFactory(NS_PROMPTSERVICE_CID, "Prompt", NS_PROMPTSERVICE_CONTRACTID, new nsIFactory() {
  //
  // @Override
  // public nsISupports queryInterface(final String uuid) {
  //
  // if (uuid.equals(nsIFactory.NS_IFACTORY_IID) || uuid.equals(nsISupports.NS_ISUPPORTS_IID))
  // return this;
  // return null;
  // }
  //
  // @Override
  // public nsISupports createInstance(final nsISupports outer, final String iid) {
  //
  // return MozillaUtils.createPromptService(service, wrapperFactory);
  // }
  //
  // @Override
  // public void lockFactory(final boolean lock) {
  //
  // }
  // });
  // // nsIComponentRegistrar cr =
  // // Mozilla.getInstance().getComponentRegistrar();
  // // nsIFactory myfactory = new MyPromptServiceFactory();
  // //
  // // cr.registerFactory("a2112d6a-0e28-421f-b46a-25c0b308cbd0",
  // // "My Prompt Service",
  // // "@mozilla.org/embedcomp/prompt-service;1",
  // // myfactory);
  // }

  /**
   * Creates a new {@link nsIPromptService} delegating to the given {@link DialogsService}
   * 
   * @param service
   *          the service to delegate to
   * @param w
   *          the wrapper factory
   * @return a new {@link nsIPromptService} delegating to the given {@link DialogsService}
   */
  // static nsIPromptService createPromptService(final DialogsService service, final WrapperDOMCallbackFactory w) {
  //
  // return new nsIPromptService() {
  //
  // @Override
  // public nsISupports queryInterface(final String arg0) {
  //
  // if (arg0.equals(nsIPromptService.NS_IPROMPTSERVICE_IID) || arg0.equals(nsISupports.NS_ISUPPORTS_IID))
  // return this;
  // return null;
  // }
  //
  // @Override
  // public void alert(final nsIDOMWindow aParent, final String aDialogTitle, final String aText) {
  //
  // service.alert(w.wrapWindow(aParent), aDialogTitle, aText);
  // }
  //
  // @Override
  // public void alertCheck(final nsIDOMWindow aParent, final String aDialogTitle, final String aText,
  // final String aCheckMsg, final boolean[] aCheckState) {
  //
  // service.alertCheck(w.wrapWindow(aParent), aDialogTitle, aText, aCheckMsg, aCheckState);
  // }
  //
  // @Override
  // public boolean confirm(final nsIDOMWindow aParent, final String aDialogTitle, final String aText) {
  //
  // return service.confirm(w.wrapWindow(aParent), aDialogTitle, aText);
  // }
  //
  // @Override
  // public boolean confirmCheck(final nsIDOMWindow aParent, final String aDialogTitle, final String aText,
  // final String aCheckMsg, final boolean[] aCheckState) {
  //
  // return service.confirmCheck(w.wrapWindow(aParent), aDialogTitle, aText, aCheckMsg, aCheckState);
  // }
  //
  // @Override
  // public int confirmEx(final nsIDOMWindow aParent, final String aDialogTitle, final String aText,
  // final long aButtonFlags, final String aButton0Title, final String aButton1Title, final String aButton2Title,
  // final String aCheckMsg, final boolean[] aCheckState) {
  //
  // return service.confirmEx(w.wrapWindow(aParent), aDialogTitle, aText, aButtonFlags, aButton0Title,
  // aButton1Title, aButton2Title, aCheckMsg, aCheckState);
  // }
  //
  // @Override
  // public boolean prompt(final nsIDOMWindow aParent, final String aDialogTitle, final String aText,
  // final String[] aValue, final String aCheckMsg, final boolean[] aCheckState) {
  //
  // return service.prompt(w.wrapWindow(aParent), aDialogTitle, aText, aValue, aCheckMsg, aCheckState);
  // }
  //
  // @Override
  // public boolean promptUsernameAndPassword(final nsIDOMWindow aParent, final String aDialogTitle,
  // final String aText, final String[] aUsername, final String[] aPassword, final String aCheckMsg,
  // final boolean[] aCheckState) {
  //
  // return service.promptUsernameAndPassword(w.wrapWindow(aParent), aDialogTitle, aText, aUsername, aPassword,
  // aCheckMsg, aCheckState);
  // }
  //
  // @Override
  // public boolean promptPassword(final nsIDOMWindow aParent, final String aDialogTitle, final String aText,
  // final String[] aPassword, final String aCheckMsg, final boolean[] aCheckState) {
  //
  // return service.promptPassword(w.wrapWindow(aParent), aDialogTitle, aText, aPassword, aCheckMsg, aCheckState);
  // }
  //
  // @Override
  // public boolean select(final nsIDOMWindow aParent, final String aDialogTitle, final String aText,
  // final long aCount, final String[] aSelectList, final int[] aOutSelection) {
  //
  // return service.select(w.wrapWindow(aParent), aDialogTitle, aText, aCount, aSelectList, aOutSelection);
  // }
  // };
  // }
  //
  // /**
  // * Sets a new user agent for the borwser
  // */
  // public static void setNewUserAgent() {
  //
  // final nsIPrefService service = MozillaUtils.getService(XPCOM.NS_PREFSERVICE_CONTRACTID, nsIPrefService.class);
  // if (service == null) {
  // MozillaUtils.logger.debug("Cannot set user agent for browser, default will apply");
  // return;
  // }
  // final nsIPrefBranch branch = service.getBranch("general.useragent.");
  // // branch.setComplexValue("", "nsISupportsString", "");
  // // either copy that in xulrunner/xxx/greprefs/allElements.js
  // // pref( "general.useragent.override",
  // // "Mozilla/5.0 (X11; U; Linux x86_64; en-GB; rv:1.9.2.17) Gecko/20110422 Ubuntu/10.10 (maverick) Firefox/3.6.17"
  // // );
  // // or set via preferences
  // MozillaUtils.logger.debug("Setting Firefox user agent to {}", MozillaUtils.MOZILLA_UA_LINUX);
  // branch.setCharPref("override", MozillaUtils.MOZILLA_UA_LINUX);
  // // System.out.println(service);
  // }
  //
  // /**
  // * Enables or disable Firefox autoscan of plugins
  // * @param enabled
  // */
  // public static void setAutoPluginScanEnabled(final boolean enabled) {
  //
  // final nsIPrefService service = MozillaUtils.getService(XPCOM.NS_PREFSERVICE_CONTRACTID, nsIPrefService.class);
  // if (service == null) {
  // MozillaUtils.logger.debug("Cannot set the autoscan of plugin to {}, default will apply", enabled);
  // return;
  // }
  // // Locate plugins by the directories specified in the Windows registry
  // // for PLIDs
  // // Which is currently HKLM\Software\MozillaPlugins\xxxPLIDxxx\Path
  // final nsIPrefBranch branch = service.getBranch("plugin.scan.plid.");
  // MozillaUtils.logger.debug("Setting enable plugin autoscan mode to {}", enabled);
  // branch.setBoolPref("allElements", enabled ? 1 : 0);
  // // System.out.println(service);
  // }
  //
  // /**
  // * Returns field name which contains XPCOM ID for the given interface- {@code type}.
  // * <P>
  // * Examples:
  // *
  // * <pre>
  // * getInterfaceIdFieldName(nsIDOMNode.class)="NS_IDOMNODE_IID"
  // * getInterfaceIdFieldName(jsdIScript.class)="JSDISCRIPT_IID"
  // * </pre>
  // * @param type interface extending {@link nsISupports}
  // */
  // private static <T extends nsISupports> String getInterfaceIdFieldName(final Class<T> type) {
  //
  // final String typeName = type.getSimpleName();
  // String interfaceIdFieldName;
  // if (typeName.startsWith("ns")) {
  // // e.g. "nsIDOMNode" becomes "NS_IDOMNODE"
  //      interfaceIdFieldName = "NS_" + typeName.substring(2).toUpperCase(); //$NON-NLS-1$
  // } else {
  // // e.g. "jsdIScript" becomes "JSDISCRIPT"
  // interfaceIdFieldName = typeName.toUpperCase();
  // }
  //    interfaceIdFieldName = interfaceIdFieldName + "_IID"; //$NON-NLS-1$
  // return interfaceIdFieldName;
  // }

  /**
   * For debug usage, gets some stats on the current page
   * 
   * @param b
   * @return
   */
  public static String getStats(final WebBrowser b) {

    final StringBuilder s = new StringBuilder();
    s.append("//* -- TITLE: " + XPathUtil.doQuery("string(/descendant::title)", "STRING", b));
    s.append("\n");
    s.append("//* -- Nodes total: " + XPathUtil.doQuery("count(/descendant::node())", "NUM", b));
    s.append("\n");
    // s.append("Links total: " + XPathUtil.doQuery("count(/descendant::a)", "NUM", b));
    // s.append("\n");
    // s.append("Nodes total having href: " + XPathUtil.doQuery("count(/descendant::*[@href])", "NUM", b));
    // s.append("\n");
    // s.append("Nodes total having attributes: " + XPathUtil.doQuery("count(/descendant::*[@*])", "NUM", b));
    // s.append("\n");
    // final String doQuery = XPathUtil.doQuery("count(/descendant::*[@my_id_123])", "NUM", b);
    // s.append("Nodes total having attributes: " + doQuery);
    // s.append("\n");
    // s.append("Ancestors of first link 'next': "
    // + XPathUtil.doQuery("count(/descendant::a[string(.)='Next'][1]/ancestor::*)", "NUM", b));
    // s.append("\n");
    // s.append("following of first link 'next': "
    // + XPathUtil.doQuery("count(/descendant::a[string(.)='Next'][1]/following::*)", "NUM", b));
    // s.append("\n");
    // s.append("preceeding of first link 'next': "
    // + XPathUtil.doQuery("count(/descendant::a[string(.)='Next'][1]/preceding::*)", "NUM", b));
    // s.append("\n");
    // s.append("descendant of first link 'next': "
    // + XPathUtil.doQuery("count(/descendant::a[string(.)='Next'][1]/descendant::*)", "NUM", b));
    // s.append("\n");
    return s.toString();
  }
}