package uk.ac.ox.cs.diadem.webapi;

import org.slf4j.Logger;

import uk.ac.ox.cs.diadem.env.DiademRuntimeException;

public class WebAPIRuntimeException extends DiademRuntimeException {
  /**
   *
   */
  private static final long serialVersionUID = 6379064535303915818L;

  public WebAPIRuntimeException(final String message, final Logger logger) {
    super(message, logger);
  }

  public WebAPIRuntimeException(final String message, final Throwable cause, final Logger logger) {
    super(message, cause, logger);
  }

}