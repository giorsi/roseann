/*
 * Copyright (c)2011, DIADEM Team
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the DIADEM team nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DIADEM Team BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package uk.ac.ox.cs.diadem.webapi.dom.utils;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.util.misc.EscapingUtils;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.DOMDocument;
import uk.ac.ox.cs.diadem.webapi.dom.DOMDocument.CRITERIA;
import uk.ac.ox.cs.diadem.webapi.dom.DOMElement;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNamedNodeMap;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNodeList;

@Deprecated
public class DOMElementFinder {

  static final Logger logger = LoggerFactory.getLogger(DOMElementFinder.class);
  private final Configuration config = ConfigurationFacility.getConfiguration();
  private long counter;

  public DOMElementFinder() {

  }

  /**
   * Finds an element given an element ID. Returns <code>null</code> if no such node.
   */
  public DOMElement findElementByID(final WebBrowser browser, final String id) {

    logger.debug("* Searching for element {} ", id); // -------------------------
    final String elPrefix = config.getString("facts.dom.html-element.idprefix");
    final int prefixLen = elPrefix.length();
    if (!id.startsWith(elPrefix)) {
      logger.error("No node with ID {} found.", id);
      return null;
    }
    final String start = id.substring(prefixLen, id.indexOf('_', prefixLen));
    final String tag = id.substring(prefixLen + start.length() + 1);
    final DOMDocument document = browser.getContentDOMWindow().getDocument();
    // NOT USED, PREVENTS GENERATING TONS OF NODES
    // final long numElements = Helpers.numDescendants(document);
    // logger.info("document loaded with {} nodes ", count);
    final DOMNode n = findNodeByStart(document.getDocumentElement(), Long.parseLong(start));
    if (n == null) {
      logger.error("No node with ID {} found.", id);
      return null;
    }
    logger.debug("* Found element {}, looking for {}", n.getLocalName(), tag); // -------------------------
    if ((n instanceof DOMElement) && EscapingUtils.escapeForDLVConstants(n.getLocalName()).equals(tag))
      return (DOMElement) n;
    logger.error("No node with ID {} and tag {} found.", id, tag);
    return null;
  }

  // /**
  // * Finds an element given a element literal. Returns <code>null</code> if no such node.
  // */
  // public DOMElement findElementByLiteral(final WebBrowser browser, final Literal l) {
  //
  // return findElementByID(browser, l.getAttributeAt(0).toString());
  // }

  /**
   * Find node in the subtree at the given node with the given attribute and attribute value
   */
  public DOMNode findNodeByAttribute(final DOMNode ctxt, final String attrName, final String attrValue) {

    logger.debug("* Searching for {}={}", attrName, attrValue); // -------------------------
    logger.debug("* Searching in {}={}", ctxt.getNodeName(), ctxt.getNodeValue()); // -------------------------
    if (ctxt.getAttributes() != null) {
      final DOMNamedNodeMap<?> attrs = ctxt.getAttributes();
      if (attrs.getNamedItem(attrName) != null)
        if (attrs.getNamedItem(attrName).getNodeValue().equals(attrValue))
          return ctxt;
    }
    for (final DOMNode n : ctxt.getChildNodes()) {
      if (n.getNodeType() != DOMNode.Type.ELEMENT) {
        continue;
      }
      final DOMNode found = findNodeByAttribute(n, attrName, attrValue);
      if (found != null)
        return found;
    }
    return null;
  }

  /**
   * Find node in the given node with the given start ID
   */
  public DOMNode findNodeByStart(final DOMNode node, final long start) {

    counter = 0;
    return findNodeByStart_Helper(node, start);
  }

  /**
   * Helper
   */
  private DOMNode findNodeByStart_Helper(final DOMNode node, final long target) {

    // logger.debug("   Looking at node {} with id {} ",
    // node.getLocalName(), node.getAttributes() != null &&
    // node.getAttributes().getNamedItem("id") != null ?
    // node.getAttributes().getNamedItem("id").getNodeValue(): "");
    if (counter > target) {
      logger.error("Counter {} above target {}, stopping", counter, target);
      return null;
    }
    switch (node.getNodeType()) {
    case ELEMENT:
      counter++;
      if (counter == target)
        return node;
      final DOMNodeList childNodes = node.getChildNodes();
      for (final DOMNode child : childNodes) {
        final DOMNode result = findNodeByStart_Helper(child, target);
        if (result != null)
          return result;
      }
      counter++;
      break;
    case COMMENT:
      break;
    case TEXT:
      final boolean skip = EscapingUtils.isGarbageText(node.getNodeValue());
      if (!skip) {
        counter++;
        if (counter == target)
          return node;
        counter++;
      }
      break;
    case PROCESSING_INSTRUCTION:
      break;
    default:
      logger.error("Unexpected node type {} at node {}", node.getNodeType(), node);
      break;
    }
    return null;
  }

  public DOMNode findTextByID(final WebBrowser browser, final String id) {

    logger.debug("* Searching for text node {} ", id); // -------------------------
    final String elPrefix = config.getString("facts.dom.html-text.idprefix");
    final int prefixLen = elPrefix.length();
    if (!id.startsWith(elPrefix)) {
      logger.error("No node with ID {} found.", id);
      return null;
    }
    final String start = id.substring(prefixLen);
    final DOMNode n = findNodeByStart(browser.getContentDOMWindow().getDocument().getDocumentElement(),
        Long.parseLong(start));
    if (n == null) {
      logger.error("No node with ID {} found.", id);
      return null;
    }
    logger.debug("* Found text {}", n.getNodeValue());
    return n;
  }

  public DOMNode findNodeByID(final WebBrowser browser, final String id) {

    final String elPrefix = config.getString("facts.dom.html-element.idprefix");
    final String textPrefix = config.getString("facts.dom.html-text.idprefix");
    if (id.startsWith(elPrefix))
      return findElementByID(browser, id);
    if (id.startsWith(textPrefix))
      return findTextByID(browser, id);
    return null;
  }

  public DOMNode findDescendant(final DOMNode node, final String tag, final String... keywords) {

    if ((node == null) || (node.getLocalName() == null))
      return null;
    DOMElementFinder.logger.debug("Find descendant for Node {} with keywords {}", node, keywords);
    if (node.getLocalName().equals(tag)) {
      for (final String k : keywords)
        if (node.getTextContent().contains(k))
          return node;
    }
    for (final DOMNode c : node.getChildNodes()) {
      final DOMNode desc = findDescendant(c, tag, keywords);
      if (desc != null)
        return desc;
    }
    return null;
  }

  public DOMElement findElementByOriginalStart(final WebBrowser browser, final String id) {

    DOMElementFinder.logger.debug("* Searching for element using attribute injection {} ", id); // -------------------------
    final String elPrefix = config.getString("facts.dom.html-element.idprefix");
    final int prefixLen = elPrefix.length();
    if (!id.startsWith(elPrefix)) {
      DOMElementFinder.logger.error("No node with ID {} found.", id);
      return null;
    }
    final String start = id.substring(prefixLen, id.indexOf('_', prefixLen));
    final DOMElement n = browser.getContentDOMWindow().getDocument()
        .selectElementBy(CRITERIA.xpath, "/descendant::a[@my_id_123='" + start + "']");
    if (n == null) {
      DOMElementFinder.logger.error("No node with ID {} found using attribute injection.", id);
      return null;
    }
    DOMElementFinder.logger.debug("* Found node with text {}", n.getTextContent());
    return n;
  }

  /**
   * Method to find a list of DOMNode given a set of id
   * 
   * @param wb
   *          WebBRowser
   * @param ids
   *          set of node's id to be found
   * @return Map where key is the node id, value is the DOMNode object
   */
  public Map<String, DOMNode> findMapNodeById(final WebBrowser wb, final Set<String> ids) {
    logger.debug("* Searching for list of elements...");

    final String elPrefix = config.getString("facts.dom.html-element.idprefix");
    final String textPrefix = config.getString("facts.dom.html-text.idprefix");
    final int elPrefixLen = elPrefix.length();
    final int textPrefixLen = textPrefix.length();

    final Map<Long, String> target2id = new TreeMap<Long, String>();
    ;

    String start = null;

    for (final String id : ids) {
      if (!id.startsWith(elPrefix) && !id.startsWith(textPrefix)) {
        logger.error("Node with id {} has uknown id", id);
        return null;
      }
      if (id.startsWith(elPrefix)) {
        start = id.substring(elPrefixLen, id.indexOf('_', elPrefixLen));
      }
      if (id.startsWith(textPrefix)) {
        start = id.substring(textPrefixLen);
      }
      target2id.put(Long.parseLong(start), id);
    }

    final DOMNode node = wb.getContentDOMWindow().getDocument().getDocumentElement();
    counter = 0;

    final Map<String, DOMNode> id2node = new TreeMap<String, DOMNode>();

    findNodeMapByStart_Helper(node, id2node, target2id);

    return id2node;
  }

  /**
   * Method to find all Browser's DOMNode
   * 
   * @param wb
   *          WebBRowser
   * 
   * @return Map where key is the node start-node inside the DOM, value is the DOMNode object
   */
  public Map<String, DOMNode> findAllNodes(final WebBrowser wb) {

    logger.debug("* Searching for all DOM elements...");
    final DOMNode node = wb.getContentDOMWindow().getDocument().getDocumentElement();
    counter = 0;

    final Map<String, DOMNode> id2node = new TreeMap<String, DOMNode>();

    findNodeMapByStart_Helper(node, id2node, null);

    return id2node;
  }

  /**
   * helper to fill a map where key is the node's id, value is the DOMNode object
   */
  private void findNodeMapByStart_Helper(final DOMNode node, final Map<String, DOMNode> id2node,
      final Map<Long, String> target2id) {

    /*
     * explore the entire tree and for every node check if the start-node matches with target nodes if no target nodes
     * are specified, simply add them all
     */
    switch (node.getNodeType()) {
    case ELEMENT:
      counter++;

      if ((target2id != null) && target2id.containsKey(counter)) {
        final String id = target2id.get(counter);
        id2node.put(id, node);
        logger.debug("Found element {} with id {}", node.getNodeValue(), id);
      }
      if (target2id == null) {
        id2node.put(counter + "", node);
        logger.debug("Found element {} ", node.getNodeValue());
      }
      final DOMNodeList childNodes = node.getChildNodes();
      for (final DOMNode child : childNodes) {
        findNodeMapByStart_Helper(child, id2node, target2id);
      }
      counter++;
      break;
    case COMMENT:
      break;
    case TEXT:
      final boolean skip = EscapingUtils.isGarbageText(node.getNodeValue());
      if (!skip) {
        counter++;
        if ((target2id != null) && target2id.containsKey(counter)) {
          id2node.put(target2id.get(counter), node);
          logger.debug("* Found text {}", node.getNodeValue());
        }
        if (target2id == null) {
          id2node.put(counter + "", node);
          logger.debug("* Found text {}", node.getNodeValue());
        }
        counter++;
      }
      break;
    case PROCESSING_INSTRUCTION:
      break;
    default:
      logger.error("Unexpected node type {} at node {}", node.getNodeType(), node);
      break;
    }
  }
}
