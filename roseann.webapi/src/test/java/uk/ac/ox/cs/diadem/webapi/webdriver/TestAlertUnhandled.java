/**
 * Header
 */
package uk.ac.ox.cs.diadem.webapi.webdriver;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;

/**
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public class TestAlertUnhandled extends StandardTestcase {

  static WebBrowser browser;

  @BeforeClass
  public static void oneTimesetUp() throws Exception {
    browser = BrowserFactory.newBuilder(Engine.WEBDRIVER_FF).doNotLoadContent(WebBrowser.ContentType.IMAGE)
        .pluginsEnabled(false).newWebBrowser();
  }

  @AfterClass
  public static void shutdown() {
    browser.shutdown();
  }

  // @AfterClass
  // public static void shutdown() {
  // browser.shutdown();
  // }

  @Test
  @Ignore
  public void test() throws InterruptedException, IOException {

    browser.navigate("http://web.archive.org/web/20090827015310/http://www.bhphotovideo.com:80/");
    Assert.assertTrue("", true);

  }

}
