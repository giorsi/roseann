package uk.ac.ox.cs.diadem.webapi.dom;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;

public class DomElementPropertiesTest extends StandardTestcase {

  private WebBrowser browser;

  @Before
  public void setUp() throws Exception {

    browser = BrowserFactory.newWebBrowser(Engine.WEBDRIVER_FF);
    final String url = DomElementPropertiesTest.class.getResource("simple.html").toURI().toString();
    browser.navigate(url);
  }

  @Test
  public void testGetInner() throws Exception {

    final DOMWindow contentDOMWindow = browser.getContentDOMWindow();
    final DOMDocument document = contentDOMWindow.getDocument();
    final DOMElement elementById = document.getElementById("homepageprimarycontent");
    final String innerHTML = elementById.getInnerHTML();
    Assert.assertEquals("giovanni", innerHTML);
    browser.shutdown();
  }

}
