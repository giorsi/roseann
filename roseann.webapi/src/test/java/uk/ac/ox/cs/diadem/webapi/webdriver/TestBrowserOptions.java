/**
 * Header
 */
package uk.ac.ox.cs.diadem.webapi.webdriver;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;

/**
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public class TestBrowserOptions extends StandardTestcase {

  static WebBrowser browser;
  private static final BlockingQueue<Object> SLEEPER = new ArrayBlockingQueue<Object>(1);

  @BeforeClass
  public static void oneTimesetUp() throws Exception {

  }

  @AfterClass
  public static void shutdown() {
    if (browser != null) {
      browser.shutdown();
    }
  }

  // @Test
  // public void testOptions() throws Exception {
  // browser = BrowserFactory.newBuilder(Engine.WEBDRIVER_FF).useXvfb(false).pluginsEnabled(false).newWebBrowser();
  // browser.manageOptions().configureXPathLocatorHeuristics(true, false);
  // browser.navigate("http://www.chuckecheese.com/experience/locations");
  // }

  // @AfterClass
  // public static void shutdown() {
  // browser.shutdown();
  // }

  @Test
  @Ignore
  public void test() throws InterruptedException, IOException {
    browser = BrowserFactory.newBuilder(Engine.WEBDRIVER_FF).newWebBrowser();
    browser.navigate("about:config");
    forceSleep();
  }

  @Test
  @Ignore
  public void testLocation() {
    browser = BrowserFactory.newBuilder(Engine.WEBDRIVER_FF).useXvfb(false)// .useLocation("40.711380", "-74.009893")
        .pluginsEnabled(false).newWebBrowser();
    browser.navigate("http://ctrlq.org/maps/where/");
    forceSleep();
  }

  private void forceSleep() {
    try {
      SLEEPER.poll(10, TimeUnit.MINUTES);
    } catch (final Exception e) {
      // toNothing
    }
  }

}
