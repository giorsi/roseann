package uk.ac.ox.cs.diadem.webapi.webdriver;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;

public class TestAutoDownload extends StandardTestcase {

  static WebBrowser browser;
  static FirefoxDriver driver;

  @AfterClass
  public static void shutdown() {
    browser.shutdown();
  }

  @BeforeClass
  public static void init() {
    browser = BrowserFactory.newBuilder(Engine.WEBDRIVER_FF).pluginsEnabled(true).newWebBrowser();
    driver = (FirefoxDriver) browser.getWindowFrame();

  }

  @Before
  public void loadPage() {
    browser.navigate("http://support.apple.com/manuals/");
  }

  @Test
  public void testOnPDF() {

    final WebElement object = driver
        .findElementByXPath("descendant::a[substring(@href, string-length(@href) - string-length('.pdf') +1) = '.pdf'][1]");
    object.click();

  }
}