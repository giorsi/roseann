package uk.ac.ox.cs.diadem.webapi.event;

import java.net.URISyntaxException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.DOMDocument;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;

public class ActionByJavascript {

  private static String URL;
  static {
    try {
      URL = ActionByJavascript.class.getResource("basicForm.html").toURI().toString();
    } catch (final URISyntaxException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
  private static WebBrowser browser;

  @BeforeClass
  public static void oneTimesetUp() throws Exception {

    ActionByJavascript.browser = BrowserFactory.newWebBrowser(Engine.WEBDRIVER_FF);
    browser.manageOptions().setFallBackToJSExecutionOnNotInteractableElements(true);
    ActionByJavascript.browser.navigate(ActionByJavascript.URL);
  }

  @Before
  public void resetPage() throws Exception {

    ActionByJavascript.browser.navigate(ActionByJavascript.URL);
    // DOMDocument firstDocument =
    // browser.getContentDOMWindow().getDocument();
    // firstDocument.getElementById("Etype").setTextContent(" ");
  }

  @AfterClass
  public static void tearDown() throws Exception {

    // ActionByJavascript.browser.shutdown();
  }

  @Test
  public void testMouseMove() throws Exception {

    final DOMDocument newDoc = ActionByJavascript.browser.getContentDOMWindow().getDocument();

    newDoc.getElementById("text").type("some value");
    newDoc.getElementById("select").htmlUtil().asHTMLSelect().selectOptionIndex(2);
    newDoc.getElementById("radio2").click();
    newDoc.getElementById("button").click();
    System.out.println(newDoc.getElementById("out").getTextContent());

  }

  @Test
  public void testMouseMoveInvisible() throws Exception {

    final DOMDocument newDoc = ActionByJavascript.browser.getContentDOMWindow().getDocument();
    newDoc.getElementById("text").type("some value");
    newDoc.getElementById("select").htmlUtil().asHTMLSelect().selectOptionIndex(2);
    newDoc.getElementById("radio2").click();
    newDoc.getElementById("button").click();
    System.out.println(newDoc.getElementById("out").getTextContent());

  }

}