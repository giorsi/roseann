package uk.ac.ox.cs.diadem.webapi.dom;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;
import uk.ac.ox.cs.diadem.util.misc.EscapingUtils;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;
import uk.ac.ox.cs.diadem.webapi.utils.XPathUtil;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;

public class RSSTest extends StandardTestcase {

  private WebBrowser browser;
  private static final String CREATOR_TERM_NAME = "creator";
  private static final String CREATOR_TERM_VALUE = "rssEntriesProcessor";
  private static final String AUTHOR_TERM_NAME = "author";
  private static final String DATE_TERM_NAME = "date";
  private static final String TITLE_TERM_NAME = "title";
  private static final String PRIORITY_TERM_NAME = "priority";
  private static final String RANK_TERM_NAME = "rank";
  private static final String URL_TERM_NAME = "url";
  private static final String TYPE_TERM_NAME = "type";
  private static final String RSS_ENTRY_TERM_VALUE = "rssEntry";
  private static final String RSS_PAGE_TERM_VALUE = "rssPage";

  @Before
  public void setUp() throws Exception {

    browser = BrowserFactory.newBuilder(Engine.WEBDRIVER_FF).enable(WebBrowser.FeatureType.ADBLOCK).newWebBrowser();
  }

  @After
  public void after() throws Exception {

    browser.shutdown();
  }

  @Test
  public void testNoRSS() throws Exception {
    final String page = RSSTest.class.getResource("simple.html").toURI().toString();
    browser.navigate(page);
    Thread.sleep(10000000);
    doRss();
  }

  @Test
  public void testRSSLinkTag() throws Exception {
    final String page = RSSTest.class.getResource("rss_link_tag.html").toURI().toString();
    browser.navigate(page);
    doRss();
  }

  @Test
  public void testRSSHomePage() throws Exception {
    final String page = RSSTest.class.getResource("rss_home_page_interesting.html").toURI().toString();
    browser.navigate(page);
    doRss();
  }

  @Test
  public void testRSSFeedPage() throws Exception {
    // final String page = RSSTest.class.getResource("rss_feed_page.html").toURI().toString();
    browser.navigate("http://feeds.bbci.co.uk/news/world/africa/rss.xml");
    doRss();
  }

  void doRss() throws Exception {

    boolean success = false;
    final String currentUrl = browser.getLocationURL();
    final String pageId = "p1";

    if (currentUrl.endsWith("xml")) {
      final Set<SyndEntry> entries = parseFeed(Lists.newArrayList(currentUrl));
      if (!entries.isEmpty()) {
        serializeEntries(entries, null, pageId);
        success = true;
      }
      return;
    }

    final Collection<String> nodes = getLinkNodesIfAny();// standard rss support

    if (!nodes.isEmpty()) {

      final Set<SyndEntry> entries = parseFeed(nodes);
      if (!entries.isEmpty()) {
        serializeEntries(entries, null, pageId);
        success = true;
      }
    } else {
      // look for candidate rss links
      final Collection<String> candidateHref = getCandidateRssHRef();

      if (!candidateHref.isEmpty()) {
        success = true;
        final Set<SyndEntry> entries = parseFeed(candidateHref);

        if (entries.isEmpty()) {
          serializeLandingPages(candidateHref, null, pageId);
        } else {
          serializeEntries(entries, null, pageId);
        }
      }

    }
    LOGGER.info("SUCCESS {}", success);
  }

  private void serializeEntries(final Set<SyndEntry> entries, final Object out, final String pageId) {

    final LinkIdGenerator linkIdGenerator = new LinkIdGenerator();

    for (final SyndEntry syndEntry : entries) {

      String uri = null;
      // rssEntries1__link(P, LinkId, url, Url)
      if (syndEntry.getLink() != null) {
        uri = syndEntry.getLink();
      } else if (syndEntry.getUri() != null) {
        uri = syndEntry.getUri();
      } else {
        LOGGER.warn("Skipping rss/atom entry because no link is available. <{}>", syndEntry);
        continue;
      }
      if (!isWebPage(uri)) {
        continue;
      }

      final String linkId = linkIdGenerator.getNextLinkId(pageId);

      serializeCommonLink(out, linkIdGenerator.getCurrentRank(), uri, linkId, RSS_ENTRY_TERM_VALUE);

      // specific for entries

      // rssEntries1__link(P, LinkId, title, "title")
      if (syndEntry.getTitle() != null) {
        log(out, linkId, TITLE_TERM_NAME, EscapingUtils.quoteAndEscapeString(syndEntry.getTitle()));
      }
      // rssEntries1__link(P, LinkId, date, Url)
      if (syndEntry.getPublishedDate() != null) {
        log(out, linkId, DATE_TERM_NAME, EscapingUtils.quoteAndEscapeString(syndEntry.getPublishedDate().toString()));
      }
      // rssEntries1__link(P, LinkId, author, Url)
      if (syndEntry.getAuthor() != null) {
        log(out, linkId, AUTHOR_TERM_NAME, EscapingUtils.quoteAndEscapeString(syndEntry.getAuthor()));
      }

    }

  }

  private static class LinkIdGenerator {
    /**
     *
     */
    private static final String TEMPLATE = "{0}_l{1}";
    int counter = -1;

    String getNextLinkId(final String prefix) {
      counter++;
      return MessageFormat.format(TEMPLATE, prefix, Integer.toString(counter));
    }

    /**
     * @return
     */
    int getCurrentRank() {
      if (counter == -1) // just to avoid to throw
        return 0;

      return counter;
    }
  }

  /**
   * @param candidateHref
   * @param out
   * @param pageId
   * @throws ProcessingException
   */
  private void serializeLandingPages(final Collection<String> candidateHref, final Object out, final String pageId) {
    final LinkIdGenerator linkIdGenerator = new LinkIdGenerator();
    for (final String href : candidateHref) {

      final String linkId = linkIdGenerator.getNextLinkId(pageId);

      if (!isWebPage(href)) {
        continue;
      }

      serializeCommonLink(out, linkIdGenerator.getCurrentRank(), href, linkId, RSS_PAGE_TERM_VALUE);

    }

  }

  private void serializeCommonLink(final Object out, final int rank, final String href, final String linkId,
      final String rssLinkType) {

    // rssEntries1__link(P, LinkId, type, rssPage)
    log(out, linkId, TYPE_TERM_NAME, rssLinkType);

    // rssEntries1__link(P, LinkId, url, rssEntry)
    log(out, linkId, URL_TERM_NAME, EscapingUtils.quoteUnquotedString(href));

    // rssEntries1__link(P, LinkId, rank, integer)
    log(out, linkId, RANK_TERM_NAME, String.valueOf(rank));
    // rssEntries1__link(P, LinkId, priority, 1)
    log(out, linkId, PRIORITY_TERM_NAME, String.valueOf(1));
    // rssEntries1__link(P, LinkId, creator, name)
    log(out, linkId, CREATOR_TERM_NAME, CREATOR_TERM_VALUE);
  }

  /**
   * @param out
   * @param linkId
   * @param creatorTermName
   * @param creatorTermValue
   */
  private void log(final Object out, final Object a, final Object b, final Object c) {
    LOGGER.info(Joiner.on(",").join("rssEntries1__link(p1", a, b, c, ")."));

  }

  /**
   * @param uri
   * @return
   */
  private boolean isWebPage(final String uri) {
    final ArrayList<String> blacklist = Lists.newArrayList("xml");
    for (final String ext : blacklist) {
      if (uri.endsWith(ext))
        return false;
    }
    return true;
  }

  /**
   * @param entries
   * @param href
   */
  private void addEntries(final Set<SyndEntry> entries, final String href) {
    XmlReader reader = null;
    try {
      reader = new XmlReader(getUrl(href));
      final SyndFeedInput syndFeedInput = new SyndFeedInput();
      syndFeedInput.setAllowDoctypes(true);
      final SyndFeed feeder = syndFeedInput.build(reader);

      LOGGER.debug("Feed URL: " + feeder.getLink());

      for (final SyndEntry syndEntry : feeder.getEntries()) {
        entries.add(syndEntry);

      }
    } catch (final Error e) {
      LOGGER.error("Error reading rss feed <{}>. <{}>", href, e);
    } catch (final Throwable e) {
      LOGGER.error("Exception reading rss feed <{}>. <{}>", href, e);
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (final IOException e) {
          LOGGER.error("Exception closing the XMLReader for rss feed <{}>. <{}>", href, e);
        }
      }
    }

  }

  private URL getUrl(final String href) throws MalformedURLException {
    // prevent a bug: http://stackoverflow.com/questions/5268280/using-feed-in-rome
    if (href.startsWith("feed://"))
      return new URL("http://" + href.substring(7));
    return new URL(href);
  }

  private Collection<String> getCandidateRssHRef() {
    final List<DOMNode> nodes = XPathUtil
        .getNodes(
            "/descendant::*[attribute::href[contains(.,'rss') or contains(.,'atom') or contains(.,'xml')]][local-name()!='link']",
            getBrowser());

    return getHrefProperty(nodes);
  }

  private Collection<String> getHrefProperty(final List<DOMNode> nodes) {
    final Set<String> href = Sets.newHashSetWithExpectedSize(nodes.size());

    for (final DOMNode domNode : nodes) {
      href.add(domNode.getDOMProperty("href"));
    }
    return href;
  }

  private Collection<String> getLinkNodesIfAny() {
    final List<DOMNode> nodes = XPathUtil.getNodes(
        "//link[@type='application/rss+xml' or @type='application/rss+atom' ]", getBrowser());
    return getHrefProperty(nodes);
  }

  /**
   * @return
   */
  private WebBrowser getBrowser() {
    return browser;
  }

  /**
   * @param nodes
   * @return
   */
  private Set<SyndEntry> parseFeed(final Collection<String> nodes) {

    final Set<SyndEntry> entries = Sets.newLinkedHashSet();
    for (final String href : nodes) {

      addEntries(entries, href);
    }
    return entries;
  }

  @Test
  public void testExtractLinkTag() throws MalformedURLException, URISyntaxException {

    final String page = RSSTest.class.getResource("simplerss.html").toURI().toString();
    browser.navigate(page);

    final List<DOMNode> nodes = XPathUtil.getNodes(
        "//link[@type='application/rss+xml' or @type='application/rss+atom' ]", browser);

    final Set<SyndEntry> entries = Sets.newLinkedHashSet();
    for (final DOMNode domNode : nodes) {
      final String href = domNode.getDOMProperty("href");
      final URL url = getUrl(href);
      addEntries(entries, url);
    }

    logEntries(entries);
  }

  @Test
  public void testExtractHref() throws MalformedURLException, URISyntaxException {

    final String page = RSSTest.class.getResource("simplerss2.html").toURI().toString();
    browser.navigate(page);

    final List<DOMNode> nodes = XPathUtil.getNodes(
        "/descendant::*[attribute::href[contains(.,'rss') or contains(.,'atom')]][local-name()!='link']", browser);

    final Set<SyndEntry> entries = Sets.newLinkedHashSet();
    for (final DOMNode domNode : nodes) {
      final String href = domNode.getDOMProperty("href");
      final URL url = getUrl(href);
      addEntries(entries, url);
    }

    logEntries(entries);
  }

  /**
   * @param entries
   */
  private void logEntries(final Set<SyndEntry> entries) {
    for (final SyndEntry syndEntry : entries) {
      LOGGER.debug("LINK: " + syndEntry.getLink());
      final String uri = syndEntry.getUri();

      LOGGER.debug("URI: " + uri);
      LOGGER.debug(syndEntry.getTitle());
      LOGGER.debug(syndEntry.getPublishedDate().toString());
      LOGGER.debug(syndEntry.getContributors().toString());
    }

  }

  /**
   * @param entries
   * @param url
   */
  private void addEntries(final Set<SyndEntry> entries, final URL url) {
    XmlReader reader = null;
    try {

      reader = new XmlReader(url);
      final SyndFeed feeder = new SyndFeedInput().build(reader);

      LOGGER.debug("Feed URL: " + feeder.getLink());

      for (final SyndEntry syndEntry : feeder.getEntries()) {
        entries.add(syndEntry);

      }
    } catch (final Error e) {
      LOGGER.error("Error reading rss feed <{}>. <{}>", url, e);
    } catch (final Throwable e) {
      LOGGER.error("Exception reading rss feed <{}>. <{}>", url, e);
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (final IOException e) {
          LOGGER.error("Exception closing the XMLReader for rss feed <{}>. <{}>", url, e);
        }
      }
    }

  }

  @Test
  public void testGetInner() throws Exception {

    // rssEntries1__link(P, LinkId, url, Url)
    // rssEntries1__link(P, LinkId, creator, Url)
    // rssEntries1__link(P, LinkId, priority, Url)
    // rssEntries1__link(P, LinkId, rank, Url)
    // rssEntries1__link(P, LinkId, title, Url)
    // rssEntries1__link(P, LinkId, author, Url)
    // rssEntries1__link(P, LinkId, date, Url)

    // final URL url = new URL("http://www.bloomberg.com/tvradio/podcast/cat_markets.xml");

    // final URL url = new URL("http://feeds.feedburner.com/javatipsfeed");
    // final URL url = new URL("http://rss.cnn.com/rss/cnn_topstories.rss");
    // final URL url = new URL("http://www.nytimes.com/services/xml/rss/nyt/HomePage.xml");

  }

}
