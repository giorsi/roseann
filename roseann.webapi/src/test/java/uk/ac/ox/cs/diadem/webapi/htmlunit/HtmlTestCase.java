package uk.ac.ox.cs.diadem.webapi.htmlunit;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;
import uk.ac.ox.cs.diadem.env.testsupport.StringDatabase.Mode;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNodeList;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;
import uk.ac.ox.cs.diadem.webapi.dom.xpath.DOMXPathResult;

public class HtmlTestCase extends StandardTestcase {

  static WebBrowser browser;

  @AfterClass
  public static void shutdown() {
    browser.shutdown();
  }

  @BeforeClass
  public static void init() {
    browser = BrowserFactory.newWebBrowser(Engine.HTMLUNIT);
  }

  @Test
  public void testURL() {
    database.setMethodKeyPrefix();
    database.setMode(Mode.TEST);
    browser.navigate("http://www.google.co.uk");
    Assert.assertTrue("URL", database.check("url", browser.getLocationURL()));
  }

  @Test
  public void testXPathNum() {
    database.setMethodKeyPrefix();
    database.setMode(Mode.TEST);
    browser.navigate("http://www.google.co.uk");
    final DOMXPathResult res = browser.getOXPathEvaluator().evaluate("count(//div)",
        browser.getContentDOMWindow().getDocument(), null, Short.MIN_VALUE, null);

    System.out.println(res.getNumberValue());
  }

  @Test
  public void testXPathBool() {
    database.setMethodKeyPrefix();
    database.setMode(Mode.TEST);
    browser.navigate("http://www.google.co.uk");
    final DOMXPathResult res = browser.getOXPathEvaluator().evaluate("boolean(count(//div)>0)",
        browser.getContentDOMWindow().getDocument(), null, Short.MIN_VALUE, null);

    System.out.println(res.getBooleanValue());
  }

  @Test
  public void testXPathString() {
    database.setMethodKeyPrefix();
    database.setMode(Mode.TEST);
    browser.navigate("http://www.google.co.uk");
    final DOMXPathResult res = browser.getOXPathEvaluator().evaluate("string(//title)",
        browser.getContentDOMWindow().getDocument(), null, Short.MIN_VALUE, null);

    System.out.println(res.getStringValue());
  }

  @Test
  public void testXPathNodes() {
    database.setMethodKeyPrefix();
    database.setMode(Mode.TEST);
    browser.navigate("http://www.google.co.uk");
    final DOMXPathResult res = browser.getOXPathEvaluator().evaluate("//a",
        browser.getContentDOMWindow().getDocument(), null, Short.MIN_VALUE, null);
    for (int i = 0; i < res.getSnapshotLength(); i++) {
      System.out.println(res.snapshotItem(i));
    }

  }

  @Test
  @Ignore
  public void testTags() {
    database.setMethodKeyPrefix();
    database.setMode(Mode.TEST);
    browser.navigate("http://www.google.co.uk");
    final DOMNodeList list = browser.getContentDOMWindow().getDocument().getElementsByTagName("div");
    final StringBuilder b = new StringBuilder();
    for (final DOMNode domNode : list) {
      b.append(domNode.toString());
    }

    Assert.assertTrue("DIVS", database.check("DIVS", b.toString()));
  }

}