/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.api.datamodel.document;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.XMLOutputter;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.AnnotationAttribute;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.OmissionConflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict.ConflictType;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.LogicalConflict;
/**
 * Super class to define a general annotated document model, which contains
 * a piece of text to annotate, a set of annotations and a set of conflicts(if exist).
 * 
 * The class provide utilities methods to retrieve the annotated text, the annotations found and the conflicts
 * 
 * @author Stefano Ortona (stefano dot ortona at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class AnnotatedDocumentModel {

	//the text annotated
	private String annotatedText;

	//the set of annotations found
	private Set<Annotation> annotations;

	//set of annotations with concepts unknown in the ontology
	private Set<Annotation> unknownAnnotations;

	//the set of conflicts
	private Set<Conflict> conflicts;

	//the set of annotator name which have been involved to annotate the text
	private Set<String> completedAnnotators;

	//the set of aggregators name which were able to reconcile the annotations
	private Set<String> completedAggregator;

	/**
	 * Public constructor
	 * @param text
	 * @param annotations
	 * @param conflicts
	 */
	public AnnotatedDocumentModel(String text,Set<Annotation> annotations,Set<Conflict> conflicts,
			Set<Annotation> unknownAnnotation,Set<String> completedAnnotators,Set<String> completedAggregators){
		this.annotatedText=text;
		this.annotations=annotations;
		this.conflicts=conflicts;
		this.unknownAnnotations=unknownAnnotation;
		this.completedAnnotators=completedAnnotators;
		this.completedAggregator=completedAggregators;
	}

	public AnnotatedDocumentModel() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Get all the annotations found on the input document
	 * @return	all the annotations found on the input document
	 */
	public Set<Annotation> getKnownAnnotations(){
		return annotations;
	}

	public Set<Annotation> getAllAnnotations(){
		Set<Annotation> allAnnotations=new HashSet<Annotation>();
		if(annotations!=null)
			allAnnotations.addAll(annotations);
		if(unknownAnnotations!=null)
			allAnnotations.addAll(unknownAnnotations);
		return allAnnotations;
	}

	/**
	 * Get all the annotations on the input document found by a specific annotator
	 * @param annotatorName	the name of the specific annotator
	 * @return	all the annotations on the input document found by a specific annotator
	 */
	public Set<Annotation> getAnnotationByAnnotator(final String annotatorName){
		final Set<Annotation> annotatorAnnotation = new HashSet<Annotation>();
		for(Annotation annotation:annotations){
			if(annotation.getOriginAnnotator().equalsIgnoreCase(annotatorName))
				annotatorAnnotation.add(annotation);
		}
		return annotatorAnnotation;
	}

	/**
	 * Get all the annotations on the input document having a specific concept
	 * @param concept	the name of the specific concept
	 * @return	all the annotations on the input document having a specific concept
	 */
	public Set<Annotation> getAnnotationByConcept(final String concept){
		final Set<Annotation> conceptAnnotation = new HashSet<Annotation>();
		for(Annotation annotation:this.getAllAnnotations()){
			if(annotation.getConcept().equalsIgnoreCase(concept))
				conceptAnnotation.add(annotation);
		}
		return conceptAnnotation;
	}

	/**
	 * Get all the annotations having start equal or greater than the input start and 
	 * end equal or less than the input end
	 * @param start	the input offset start
	 * @param end	the input offset end
	 * @return	all the annotations haveing start equal or greater than the input start and 
	 * end equal or less than the input end
	 */
	public Set<Annotation> getAnnotationByIncludingStartEnd(final long start, final long end){
		final Set<Annotation> spanAnnotation = new HashSet<Annotation>();
		for(Annotation annotation:this.getAllAnnotations()){
			if(annotation.getStart()>=start&&annotation.getEnd()<=end)
				spanAnnotation.add(annotation);
		}
		return spanAnnotation;
	}

	/**
	 * Get all the annotations having start equal to the input start and 
	 * end equal to the input end
	 * @param start	the input offset start
	 * @param end	the input offset end
	 * @return	all the annotations haveing start equal to the input start and 
	 * end equal tothe input end
	 */
	public Set<Annotation> getAnnotationByStartEnd(final long start, final long end){
		final Set<Annotation> spanAnnotation = new HashSet<Annotation>();
		for(Annotation annotation:this.getAllAnnotations()){
			if(annotation.getStart()==start&&annotation.getEnd()==end)
				spanAnnotation.add(annotation);
		}
		return spanAnnotation;
	}


	/**
	 * Get the text which has been annotated
	 * @return	the text which has been annotated
	 */
	public String getAnnotatedText(){
		return annotatedText;
	}

	/**
	 * Get all the conflicts, both logical and omission
	 * @return	a set of conflicts
	 */
	public Set<Conflict> getAllConflicts(){
		return this.conflicts;
	}
	/**
	 * Set all the conflicts, both logical and omission
	 * @param	a set of conflicts
	 */
	public void setAllConflicts(Set<Conflict> conflicts){
		this.conflicts = conflicts;
	}

	/**
	 * Get the annotator names which have been able to annotate the document. If for there was a problem 
	 * for a specific annotator (such as downtime service) and no answer from the annotator was found,
	 * then the annotator name won't be in the result set
	 * @return	the annotator names which have been able to annotate the document
	 */
	public Set<String> getCompletedAnnotators(){
		return completedAnnotators;
	}

	/**
	 * Set the annotator names which have been able to annotate the document. 
	 */
	public void setCompletedAnnotators(Set<String> completeIndividuals){
		this.completedAnnotators = completeIndividuals;
	}

	/**
	 * Get the set of omission conflicts
	 * @return set of omission conflicts
	 */
	public Set<OmissionConflict> getOmissionConflicts(){
		final Set<OmissionConflict> omissionConflicts=new HashSet<OmissionConflict>();
		for(Conflict conflict:conflicts){
			if(conflict.getType().equals(ConflictType.OMISSION)){
				try{
					omissionConflicts.add((OmissionConflict)conflict);
				}
				catch(ClassCastException c){
					continue;
				}
			}
		}
		return omissionConflicts;
	}

	/**
	 * Get the set of logical conflicts
	 * @return set of logical conflicts
	 */
	public Set<LogicalConflict> getLogicalConflicts(){
		final Set<LogicalConflict> logicalConflicts=new HashSet<LogicalConflict>();
		for(Conflict conflict:conflicts){
			if(conflict.getType().equals(ConflictType.LOGICAL)){
				try{
					logicalConflicts.add((LogicalConflict)conflict);
				}
				catch(ClassCastException c){
					continue;
				}
			}
		}
		return logicalConflicts;
	}

	/**
	 * Get all the conflicts having start equal to the the input start and 
	 * end equal to the input end
	 * @param start	the input offset start
	 * @param end	the input offset end
	 * @return	all the conflicts having start equal to the input start and 
	 * end equal to the input end
	 */
	public Set<Conflict> getConflictByStartEnd(long start,long end){
		final Set<Conflict> spanConflict = new HashSet<Conflict>();
		for(Conflict conflict:conflicts){
			if(conflict.getStart()==start&&conflict.getEnd()==end)
				spanConflict.add(conflict);
		}
		return spanConflict;
	}

	/**
	 * Save the document with xml notation in the given file path
	 * @param pathFile	the file to save the document
	 * @throws FileNotFoundException,IOException	if problem arises when writing the file in the output stream
	 * 												or if the file has not been found 
	 */
	public void save(final File pathFile) throws FileNotFoundException,IOException{
		final FileOutputStream outputStream = new FileOutputStream(pathFile);
		this.writeDocument(this, outputStream);
		outputStream.flush();
		outputStream.close();
	}

	/**
	 * Save the document with xml notation in the given outputstream
	 * @param outputstreamFile	the output stream to write the document
	 * @throws IOException	if problem arises when writing the file in the output stream
	 */
	public void save(final OutputStream outputstreamFile) throws IOException{
		this.writeDocument(this, outputstreamFile);
	}

	/**
	 * Save the document with Gate xml notation in the given file path
	 * @param pathFile	the file to save the document
	 * @throws FileNotFoundException,IOException	if problem arises when writing the file in the output stream
	 * 												or if the file has not been found 
	 */
	public void saveGateFormat(final File pathFile) throws FileNotFoundException,IOException{
		final FileOutputStream outputStream = new FileOutputStream(pathFile);
		this.writeGateDocument(this, outputStream);
		outputStream.flush();
		outputStream.close();
	}

	/**
	 * Save the document with Gate xml notation in the given outputstream
	 * @param outputstreamFile	the output stream to write the document
	 * @throws IOException	if problem arises when writing the file in the output stream
	 */
	public void saveGateFormat(final OutputStream outputstreamFile) throws IOException{
		this.writeGateDocument(this, outputstreamFile);
	}

	/**
	 * Given an AnnotatedDocumentModel, save the annotations using Gate xml format 
	 * (the other information like conflicts will not be saved)
	 * Each annotation and conflict is represented as xml node
	 * 
	 * @param document	the annotated document model containing annotations and conflicts
	 * @param outputstreamFile		output stream where to write the file
	 * @throws IOException	if problem arises when writing the file in the output stream
	 */
	private void writeGateDocument(final AnnotatedDocumentModel document,
			final OutputStream outputstreamFile) throws IOException{

		final String inputText = document.getAnnotatedText();
		final Set<Annotation> annotations=document.getAllAnnotations();
		final Set<Long> descendantOffset=new TreeSet<Long>();

		Element root=new Element("GateDocument");
		root.addContent(new Element("GateDocumentFeatures"));

		Element textWithNodes=new Element("TextWithNodes");

		//Calculate all the start-end for each annotation
		for(Annotation annotation: annotations){
			descendantOffset.add(annotation.getStart());
			descendantOffset.add(annotation.getEnd());
		}

		long actualPosition=0;
		long nextPosition=0;

		//create a new node for each annotation, for each start-end
		for(Long position:descendantOffset){
			nextPosition=position;
			textWithNodes.addContent(inputText.substring((int)actualPosition,(int)nextPosition));
			final Element node=new Element("Node");
			node.setAttribute("id",nextPosition+"");
			textWithNodes.addContent(node);
			actualPosition=nextPosition;
		}
		textWithNodes.addContent(inputText.substring((int)actualPosition,inputText.length()));

		root.addContent(textWithNodes);
		//save the annotation
		//create the annotation set element with a child for each annotation
		final Element annotationSet=new Element("AnnotationSet");

		int counter = 1;
		for(Annotation annotation: annotations){
			final Element annotationElement=new Element("Annotation");

			//set the attribute
			annotationElement.setAttribute("Id", counter+"");
			annotationElement.setAttribute("Type",annotation.getConcept());
			annotationElement.setAttribute("StartNode", annotation.getStart()+"");
			annotationElement.setAttribute("EndNode", annotation.getEnd()+"");


			//save the origin annotator as a feature
			Element featureOriginAnnotaotor = new Element("Feature");
			Element featureOriginAnnotaotorName = new Element("Name");
			featureOriginAnnotaotorName.addContent("OriginAnnotator");
			Element featureOriginAnnotatorValue = new Element("Value");
			featureOriginAnnotatorValue.addContent(annotation.getOriginAnnotator());
			featureOriginAnnotaotor.addContent(featureOriginAnnotaotorName);
			featureOriginAnnotaotor.addContent(featureOriginAnnotatorValue);
			annotationElement.addContent(featureOriginAnnotaotor);

			//save the confidence as a feature
			Element featureConfidence = new Element("Feature");
			Element featureConfidenceName = new Element("Name");
			featureConfidenceName.addContent("Confidence");
			Element featureConfidenceValue = new Element("Value");
			featureConfidenceValue.addContent(annotation.getConfidence()+"");
			featureConfidence.addContent(featureConfidenceName);
			featureConfidence.addContent(featureConfidenceValue);
			annotationElement.addContent(featureConfidence);

			//save the type as a feature
			Element featureType = new Element("Feature");
			Element featureTypeName = new Element("Name");
			featureTypeName.addContent("Type");
			Element featureTypeValue = new Element("Value");
			featureTypeValue.addContent(annotation.getType()+"");
			featureType.addContent(featureTypeName);
			featureType.addContent(featureTypeValue);
			annotationElement.addContent(featureType);

			//save the annotation source as a feature
			final String annotationSource=annotation.getAnnotationSource();
			if(annotationSource!=null){
				//save the type as a feature
				Element featureSource = new Element("Feature");
				Element featureSourceName = new Element("Name");
				featureSourceName.addContent("Source");
				Element featureSourceValue = new Element("Value");
				featureSourceValue.addContent(annotationSource);
				featureSource.addContent(featureSourceName);
				featureSource.addContent(featureSourceValue);
				annotationElement.addContent(featureSource);
			}

			//save each attribute as a feature
			for(AnnotationAttribute attribute:annotation.getAttributes()){
				annotationElement.setAttribute(attribute.getName(),attribute.getValue());
				
				Element featureAttribute = new Element("Feature");
				Element featureAttributeName = new Element("Name");
				featureAttributeName.addContent(attribute.getName());
				Element featureAttributeValue = new Element("Value");
				featureAttributeValue.addContent(attribute.getValue());
				featureAttribute.addContent(featureAttributeName);
				featureAttribute.addContent(featureAttributeValue);
				annotationElement.addContent(featureAttribute);
			}
			annotationSet.addContent(annotationElement);

			counter++;
		}

		root.addContent(annotationSet);

		final Document finalDoc=new Document(root);

		XMLOutputter xmlOutput = new XMLOutputter();

		xmlOutput.output(finalDoc, outputstreamFile);

	}

	/**
	 * Given an AnnotatedDocumentModel, save the annotations and the conflict using xml format.
	 * Each annotation and conflict is represented as xml node, where all the annotation (conflict)
	 * properties are attributes of the node
	 * 
	 * @param document	the annotated document model containing annotations and conflicts
	 * @param outputstreamFile		output stream where to write the file
	 * @throws IOException	if problem arises when writing the file in the output stream
	 */
	private void writeDocument(final AnnotatedDocumentModel document,
			final OutputStream outputstreamFile) throws IOException{

		final String inputText = document.getAnnotatedText();
		final Set<Annotation> annotations=document.getAllAnnotations();
		final Set<Long> descendantOffset=new TreeSet<Long>();

		Element root=new Element("GateDocument");
		root.addContent(new Element("GateDocumentFeatures"));

		Element textWithNodes=new Element("TextWithNodes");

		//Calculate all the start-end for each annotation
		for(Annotation annotation: annotations){
			descendantOffset.add(annotation.getStart());
			descendantOffset.add(annotation.getEnd());
		}

		long actualPosition=0;
		long nextPosition=0;

		//create a new node for each annotation, for each start-end
		for(Long position:descendantOffset){
			nextPosition=position;
			textWithNodes.addContent(inputText.substring((int)actualPosition,(int)nextPosition));
			final Element node=new Element("Node");
			node.setAttribute("id",nextPosition+"");
			textWithNodes.addContent(node);
			actualPosition=nextPosition;
		}
		textWithNodes.addContent(inputText.substring((int)actualPosition,inputText.length()));

		root.addContent(textWithNodes);
		//save the annotation
		//create the annotation set element with a child for each annotation
		final Element annotationSet=new Element("AnnotationSet");

		for(Annotation annotation: annotations){
			final Element annotationElement=new Element("Annotation");

			//set the attribute
			annotationElement.setAttribute("Id", annotation.getId());
			annotationElement.setAttribute("Concept", annotation.getConcept());
			annotationElement.setAttribute("StartNode", annotation.getStart()+"");
			annotationElement.setAttribute("EndNode", annotation.getEnd()+"");
			annotationElement.setAttribute("Annotator", annotation.getOriginAnnotator());
			annotationElement.setAttribute("Confidence", annotation.getConfidence()+"");
			annotationElement.setAttribute("Type", annotation.getType()+"");
			final String annotationSource=annotation.getAnnotationSource();
			if(annotationSource!=null)
				annotationElement.setAttribute("Source", annotation.getAnnotationSource()+"");

			//add the attribute values
			for(AnnotationAttribute attribute:annotation.getAttributes()){
				annotationElement.setAttribute(attribute.getName(),attribute.getValue());
			}
			annotationSet.addContent(annotationElement);		

		}

		root.addContent(annotationSet);

		//save the conflict
		//create the annotation set element with a child for each annotation
		final Element conflictSet=new Element("ConflictSet");
		final Set<Conflict> conflicts = document.getAllConflicts();
		for(Conflict conflict: conflicts){
			final Element conflictElement=new Element("Conflict");

			//set the conflicts attribute
			conflictElement.setAttribute("Id", conflict.getId());
			conflictElement.setAttribute("StartNode", conflict.getStart()+"");
			conflictElement.setAttribute("EndNode", conflict.getEnd()+"");
			conflictElement.setAttribute("Type", conflict.getType()+"");

			//add the involved annotation
			final Element involvedAnnotations = new Element("InvolvedAnnotation");
			for(Annotation annotation:conflict.getInvolvedAnnotations()){
				final Element involvedAnnotation = new Element("Annotation");
				involvedAnnotation.setAttribute("Id",annotation.getId());
				involvedAnnotations.addContent(involvedAnnotation);
			}
			conflictElement.addContent(involvedAnnotations);

			//if it's an omission conflict, add the concept, agreed annotator and omitted annotators
			if(conflict.getType().equals(ConflictType.OMISSION)){
				OmissionConflict omission = (OmissionConflict) conflict;
				conflictElement.setAttribute("Concept",omission.getConcept());

				//add the agreed annotators
				final Element agreedAnnotators = new Element("AgreedAnnotators");
				for(String annotator:omission.getAgreedAnnotators()){
					final Element agreedAnnotator = new Element("Annotator");
					agreedAnnotator.setAttribute("Name",annotator);
					agreedAnnotators.addContent(agreedAnnotator);
				}
				conflictElement.addContent(agreedAnnotators);

				//add the omitted annotators
				final Element omittedAnnotators = new Element("OmittedAnnotators");
				for(String annotator:omission.getOmittedAnnotators()){
					final Element omittedAnnotator = new Element("Annotator");
					omittedAnnotator.setAttribute("Name",annotator);
					omittedAnnotators.addContent(omittedAnnotator);
				}
				conflictElement.addContent(omittedAnnotators);

			}

			conflictSet.addContent(conflictElement);

		}

		root.addContent(conflictSet);

		final Document finalDoc=new Document(root);

		XMLOutputter xmlOutput = new XMLOutputter();

		xmlOutput.output(finalDoc, outputstreamFile);

	}

	/**
	 * Method to set the unknown annotations
	 * @param unknownAnnotations
	 */
	public void setUnknownAnnotation(Set<Annotation> unknownAnnotations){
		this.unknownAnnotations=unknownAnnotations;
	}

	/**
	 * Set the text that has been annotated
	 * @param text	The text to be set
	 */
	public void setAnnotatedText(String text){
		this.annotatedText=text;
	}

	/**
	 * Public method to get the unknwon annotations
	 * @return	The unknown annotations
	 */
	public Set<Annotation> getUnknownAnnotation(){
		return this.unknownAnnotations;
	}


	/**
	 * Get the set name of aggregators that reconcile the document
	 * @return	Set of aggregators name
	 */
	public Set<String> getCompletedAggregators(){
		return this.completedAggregator;
	}

	/**
	 * Set the set of aggregators name which reconciled the document
	 * @param completedAggregator
	 */
	public void setCompletedAggregators(Set<String> completedAggregator){
		this.completedAggregator=completedAggregator;
	}

	/**
	 * get the number of omission conflicts having start=start and end=end
	 * @param start
	 * @param end
	 * @return	The set of omission conflicts with the input span
	 */
	public int getOmissionConflictByStartEnd(int start,int end){
		Set<Conflict> conflicts = this.getConflictByStartEnd(start, end);

		int numConf=0;
		for(Conflict conflict:conflicts){
			if(conflict.getType().equals(ConflictType.OMISSION))
				numConf++;
		}
		return numConf;
	}

	/**
	 * get the number of logical conflicts having start=start and end=end
	 * @param start
	 * @param end
	 * @return	The set of logical conflicts with the input span
	 */
	public int getLogicalConflictByStartEnd(int start,int end){
		Set<Conflict> conflicts = this.getConflictByStartEnd(start, end);

		int numConf=0;
		for(Conflict conflict:conflicts){
			if(conflict.getType().equals(ConflictType.LOGICAL))
				numConf++;
		}
		return numConf;
	}

	/**
	 * Get the set of conflicts having start, end and the type as the input parameters
	 * @param start
	 * @param end
	 * @param confType
	 * @return	The set of conflicts having start, end and type equal to the input parameters
	 */
	public Set<Conflict> getConflictByStartEndType(int start,int end, ConflictType confType){
		final Set<Conflict> conflicts = this.getConflictByStartEnd(start, end);
		Set<Conflict> res = new HashSet<Conflict>();
		for(Conflict conflict:conflicts){
			if(conflict.getType().equals(confType))
				res.add(conflict);
		}
		return res;
	}

	public void setKnownAnnotations(Set<Annotation> knownAnnotations) {
		this.annotations = knownAnnotations;

	}


}
