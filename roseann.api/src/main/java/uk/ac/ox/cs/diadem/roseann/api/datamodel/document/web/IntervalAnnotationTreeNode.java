/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * @author Giorgio Orsi (giorgio dot orsi at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 * 
 */
class IntervalAnnotationTreeNode {

	private final SortedMap<IntervalAnnotation, List<IntervalAnnotation>> intervals;
	private final int midpoint;
	private IntervalAnnotationTreeNode leftNode;
	private IntervalAnnotationTreeNode rightNode;

	public IntervalAnnotationTreeNode() {
		intervals = new TreeMap<IntervalAnnotation, List<IntervalAnnotation>>();
		midpoint = 0;
		leftNode = null;
		rightNode = null;
	}

	/**
	 * Constructs an {@link IntervalAnnotationTreeNode} from a set of
	 * {@link IntervalAnnotation}s
	 * 
	 * @param intervalList
	 *            the list of intervals
	 */
	public IntervalAnnotationTreeNode(final List<IntervalAnnotation> intervalList) {

		intervals = new TreeMap<IntervalAnnotation, List<IntervalAnnotation>>();

		final SortedSet<Integer> endpoints = new TreeSet<Integer>();

		for (final IntervalAnnotation interval : intervalList) {
			endpoints.add(interval.getRange().getMinimumInteger());
			endpoints.add(interval.getRange().getMaximumInteger());
		}

		final int median = computeMidpoint(endpoints);
		midpoint = median;

		final List<IntervalAnnotation> left = new ArrayList<IntervalAnnotation>();
		final List<IntervalAnnotation> right = new ArrayList<IntervalAnnotation>();

		for (final IntervalAnnotation interval : intervalList) {
			if (interval.getRange().getMaximumInteger() < median) {
				left.add(interval);
			} else if (interval.getRange().getMinimumInteger() > median) {
				right.add(interval);
			} else {
				List<IntervalAnnotation> posting = intervals.get(interval);
				if (posting == null) {
					posting = new ArrayList<IntervalAnnotation>();
					intervals.put(interval, posting);
				}
				posting.add(interval);
			}
		}

		if (left.size() > 0) {
			leftNode = new IntervalAnnotationTreeNode(left);
		}
		if (right.size() > 0) {
			rightNode = new IntervalAnnotationTreeNode(right);
		}
	}

	/**
	 * Perform a point-query on the node
	 * 
	 * @param point
	 *            the point to query for
	 * @return all intervals containing the integer point given as input
	 */
	public List<IntervalAnnotation> get(final int point) {
		final List<IntervalAnnotation> result = new ArrayList<IntervalAnnotation>();

		for (final Entry<IntervalAnnotation, List<IntervalAnnotation>> entry : intervals.entrySet()) {
			if (entry.getKey().contains(point)) {
				for (final IntervalAnnotation interval : entry.getValue()) {
					result.add(interval);
				}
			} else if (entry.getKey().getRange().getMinimumInteger() > point) {
				break;
			}
		}

		if (point < midpoint && leftNode != null) {
			result.addAll(leftNode.get(point));
		} else if (point > midpoint && rightNode != null) {
			result.addAll(rightNode.get(point));
		}
		return result;
	}

	/**
	 * Perform an interval intersection query on the node
	 * 
	 * @param target
	 *            the interval to intersect
	 * @return all intervals containing time
	 */
	public List<IntervalAnnotation> query(final IntervalAnnotation target) {
		final List<IntervalAnnotation> result = new ArrayList<IntervalAnnotation>();

		for (final Entry<IntervalAnnotation, List<IntervalAnnotation>> entry : intervals.entrySet()) {
			if (entry.getKey().overlaps(target)) {
				for (final IntervalAnnotation interval : entry.getValue()) {
					result.add(interval);
				}
			} else if (entry.getKey().getRange().getMaximumInteger() > target.getRange().getMaximumInteger()) {
				break;
			}
		}

		if (target.getRange().getMinimumInteger() < midpoint && leftNode != null) {
			result.addAll(leftNode.query(target));
		}
		if (target.getRange().getMaximumInteger() > midpoint && rightNode != null) {
			result.addAll(rightNode.query(target));
		}
		return result;
	}

	/**
	 * Get the midpoint of the interval of this node
	 * 
	 * @return the midpoint of this interval
	 */
	public int getMidpoint() {
		return midpoint;
	}

	/**
	 * Get the left child of this interval node
	 * 
	 * @return the left child
	 */
	public IntervalAnnotationTreeNode getLeftChild() {
		return leftNode;
	}

	/**
	 * Set the left child of this interval node
	 * 
	 * @param left
	 *            the left child
	 */
	public void setLeftChild(final IntervalAnnotationTreeNode left) {
		leftNode = left;
	}

	/**
	 * Get the right child of this interval node
	 * 
	 * @return the right child
	 */
	public IntervalAnnotationTreeNode getRightChild() {
		return rightNode;
	}

	/**
	 * Set the right child of this interval node
	 * 
	 * @param right
	 *            the right child
	 */
	public void setRightChild(final IntervalAnnotationTreeNode right) {
		rightNode = right;
	}

	/**
	 * @param set
	 *            the set to search into
	 * @return the median of the set, not interpolated
	 */
	private Integer computeMidpoint(final SortedSet<Integer> set) {
		int i = 0;
		final int middle = set.size() / 2;
		for (final Integer point : set) {
			if (i == middle) {
				return point;
			}
			i++;
		}
		return null;
	}
}