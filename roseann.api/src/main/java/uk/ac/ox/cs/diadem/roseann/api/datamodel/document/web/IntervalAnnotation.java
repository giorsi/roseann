/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web;

import java.util.Set;

import org.apache.commons.lang.math.IntRange;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;

/**
 * @author Giorgio Orsi (giorgio dot orsi at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 * 
 */
class IntervalAnnotation implements Comparable<IntervalAnnotation> {

	// An integer interval
	private final IntRange interval;

	// Data associated to the interval
	private final Set<Annotation> data;

	/**
	 * Constructs an interval atom as a pair <IntRange,Set<Atom>>
	 * 
	 * @param interval
	 *            the interval
	 * @param data
	 *            the set of atoms associated to the data
	 */
	public IntervalAnnotation(final IntRange interval, final Set<Annotation> data) {
		this.interval = interval;
		this.data = data;
	}

	/**
	 * Getter for the interval
	 * 
	 * @return the interval
	 */
	public IntRange getRange() {
		return interval;
	}

	/**
	 * Getter for the data value
	 * 
	 * @return the data value
	 */
	public Set<Annotation> getData() {
		return data;
	}

	/**
	 * Checks whether the interval contains an integer point
	 * 
	 * @param point
	 *            the integer point
	 * @return true if this interval contains the integer point
	 */
	public boolean contains(final int point) {
		return this.getRange().containsInteger(point);
	}

	/**
	 * @param other
	 * @return return true if this interval intersects other
	 */
	public boolean overlaps(final IntervalAnnotation iAtom) {
		return this.getRange().overlapsRange(iAtom.getRange());
	}

	/**
	 * Return -1 if this interval's min value is less than the one of atom, 1 if
	 * greater In the event of a tie, -1 if this interval's end time is less
	 * than the other, 1 if greater, 0 if same
	 * 
	 * @param other
	 * @return 1 or -1
	 */
	@Override
	public int compareTo(final IntervalAnnotation atom) {
		if (this.getRange().getMinimumInteger() < atom.getRange().getMinimumInteger()) {
			return -1;
		} else if (this.getRange().getMinimumInteger() > atom.getRange().getMinimumInteger()) {
			return 1;
		} else if (this.getRange().getMaximumInteger() < atom.getRange().getMaximumInteger()) {
			return -1;
		} else if (this.getRange().getMaximumInteger() > atom.getRange().getMaximumInteger()) {
			return 1;
		} else {
			return 0;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o instanceof IntervalAnnotation) {
			return this.getRange().equals(((IntervalAnnotation) o).getRange())
			        && this.getData().equals(((IntervalAnnotation) o).getData());
		}
		return false;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer(50);
		sb.append(this.getRange());
		sb.append("->");
		sb.append(this.getData());

		return sb.toString();
	}
}