/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.api.datamodel.document;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.pdfbox.pdmodel.PDPage;

import uk.ac.ox.cs.diadem.pdfanalyser.DocumentProcessor;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;
import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;
import uk.ac.ox.cs.diadem.pdfanalyser.util.SegmentedContents;

/**
 * Class that represent a PDF file object.
 * A PDF file is made of a set of pages, a list of block groups, a list of blocks, a list of lines and a 
 * list of glyphs (single characters). The getter methods return those objects ordered as they appear 
 * on the pdf file.
 * Each block group, block, line or glyph is associated with four coordinates that identify the relative
 * object in the vectorial space of the pdf file
 * 
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk), Department of Computer Science, Oxford University
 *
 */
public class PdfDoc {

	/**
	 * The page_url
	 */
	private File pdfFile;

	/**
	 * The segmentation of the pdf file
	 */
	private Map<PDPage, SegmentedContents> page2segmentation;

	/**
	 * The set of pdf pages
	 */
	private Set<PDPage> pages;

	/**
	 * The list of all block_groups
	 */
	private List<BlockGroup> blockGroups;

	/**
	 * The list of all the blocks
	 */
	private List<Block> blocks;

	/**
	 * The list of all the lines
	 */
	private List<Line> lines;

	/**
	 * List of all the glyphs
	 */
	private List<Glyph> glyphs;

	/**
	 * The visible textual content of the page
	 */
	private String textualContent;

	public PdfDoc(File pdfFile) throws IOException{
		this.pdfFile=pdfFile;

		//initialize the segmentation
		DocumentProcessor processor = new DocumentProcessor();
		this.page2segmentation=processor.getOriginalDocumentSegmentation(this.pdfFile);
		this.pages=this.page2segmentation.keySet();
		this.blockGroups=new LinkedList<BlockGroup>();
		this.blocks=new LinkedList<Block>();
		this.lines=new LinkedList<Line>();
		this.glyphs=new LinkedList<Glyph>();
		StringBuilder text = new StringBuilder();

		int numPag=0;
		int glyphPos = 0;
		for(PDPage page:this.pages){
			final SegmentedContents content = this.page2segmentation.get(page);
			List<BlockGroup> groups = content.getGroups();
			for(BlockGroup group:groups){
				group.setPage(numPag);
			}
			this.blockGroups.addAll(groups);

			List<Block> blocks = content.getBlocks();
			for(Block block:blocks)
				block.setPage(numPag);
			this.blocks.addAll(blocks);

			List<Line> lines = content.getLines();
			for(Line line:lines){
				line.setPage(numPag);

				text.append(line.toString());

				List<Glyph> glyphs=line.getGlyphs();
				this.glyphs.addAll(glyphs);

				Glyph precedingGlyph = glyphs.get(0);
				precedingGlyph.setStart(glyphPos);
				precedingGlyph.setEnd(glyphPos+precedingGlyph.getCharacter().length());
				precedingGlyph.setPage(numPag);

				// some glyph contains multiple characters
				glyphPos += precedingGlyph.getCharacter().length();

				for (int j = 1; j < glyphs.size(); j++) {
					final Glyph g = glyphs.get(j);
					if (g.getLeft() - precedingGlyph.getRight() > Line.SPACING_TOLERANCE) {
						glyphPos++;
					}

					g.setStart(glyphPos);
					g.setEnd(glyphPos + g.getCharacter().length());
					g.setPage(numPag);

					precedingGlyph = g;
					glyphPos += precedingGlyph.getCharacter().length();
				}

				// add white space between different lines
				glyphPos++;
				text.append(" ");

			}
			this.lines.addAll(lines);

			numPag++;

			// add two line breaks between pages (not for the last page)
			if(numPag!=this.pages.size()){
				text.append("\n\n");
				glyphPos+=2;
			}

		}

		this.textualContent=text.toString();
	}

	/**
	 * Public method to get as a string the textual visible content of the pdf file
	 * @return	The String representing the textual content of the pdf file
	 */
	public String getTextualContent(){
		return this.textualContent;
	}

	public List<Line> getLines(){
		return this.lines;
	}

	public File getPdfFile() {
		return pdfFile;
	}

	public Set<PDPage> getPages() {
		return pages;
	}

	public List<BlockGroup> getBlockGroups() {
		return blockGroups;
	}

	public List<Block> getBlocks() {
		return blocks;
	}

	public List<Glyph> getGlyphs() {
		return glyphs;
	}

	public Map<PDPage,SegmentedContents> getPageSegmentedContent(){
		return this.page2segmentation;
	}

	/**
	 * Get the glyph starting at a given offset.
	 * The offset is the global offset of the pdf textual content
	 * @param startOffset	The offset where the glyph start
	 * @return	Null if there is no glyph at given offset, otherwise the glyph at the starting offset
	 */
	public Glyph getGlyphByStartOffset(long startOffset){
		for(Glyph glyph:this.glyphs){
			if(glyph.getStart()==startOffset)
				return glyph;
		}
		return null;
	}

	/**
	 * Get the glyph ending at a given offset.
	 * The offset is the global offset of the pdf textual content
	 * @param endOffset	The offset where the glyph ends
	 * @return	Null if there is no glyph at given offset, otherwise the glyph at the starting offset
	 */
	public Glyph getGlyphByEndOffset(long endOffset){
		for(Glyph glyph:this.glyphs){
			if(glyph.getEnd()==endOffset)
				return glyph;
		}
		return null;
	}

	public List<Line> getLinesByPage(int pageNum){
		List<Line> lines = new LinkedList<Line>();
		for(Line line:this.lines){
			if(line.getPage()==pageNum)
				lines.add(line);
		}
		return lines;
	}

	public List<Block> getBlockByPage(int pageNum){
		List<Block> blocks = new LinkedList<Block>();
		for(Block block:this.blocks){
			if(block.getPage()==pageNum)
				blocks.add(block);
		}
		return blocks;
	}

	public List<BlockGroup> getBlockGroupByPage(int pageNum){
		List<BlockGroup> groups = new LinkedList<BlockGroup>();
		for(BlockGroup group:this.blockGroups){
			if(group.getPage()==pageNum)
				groups.add(group);
		}
		return groups;
	}

}

