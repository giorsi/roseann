/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web;

import org.apache.commons.lang.math.IntRange;

/**
 * This class contains helpers for manipulating integer closed intervals
 * 
 * @author Giorgio Orsi (giorgio dot orsi at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 * 
 */
public final class Intervals {

    /**
     * Checks wether the {@link IntRange} "a" overlaps with the {@link IntRange}
     * "b".
     * 
     * @param a
     *            the reference interval
     * @param b
     *            the interval to be compared with a
     * @return true if a overlaps with b
     */
    public static boolean overlaps(final IntRange a, final IntRange b) {
        return a.overlapsRange(b);
    }

    /**
     * Returns the length of the {@link IntRange} interval
     * 
     * @param interval
     *            the interval
     * @return an integer value for the length of the interval
     */
    public static int length(final IntRange interval) {
        return interval.getMaximumInteger() - interval.getMinimumInteger() + 1;
    }

    /**
     * Computes how much the {@link IntRange} "a" covers the interval
     * {@link IntRange} "b" as an integer value in [0..100] where 0 means no
     * overlap and 100 means full overlap.
     * 
     * @param a
     *            the first interval
     * @param b
     *            the reference interval
     * @return an integer value representing the fraction of overlap of a w.r.t.
     *         b
     */
    public static int covers(final IntRange a, final IntRange b) {

        final IntRange projection = project(a, b);

        if (projection != null) {
            return Intervals.length(projection) * 100 / Intervals.length(b);
        } else {
            return 0;
        }
    }

    /**
     * Computes the projection of the {@link IntRange} "a" on the
     * {@link IntRange} "b"
     * 
     * @param a
     *            the projected interval
     * @param b
     *            the reference interval
     * @return an {@link IntRange} representing the projection of a on b
     */
    public static IntRange project(final IntRange a, final IntRange b) {
        if (!a.overlapsRange(b)) {
            return null;
        } else if (b.containsRange(a)) {
            return a;
        } else if (a.containsRange(b)) {
            return b;
        } else if (b.getMaximumInteger() >= a.getMinimumInteger() && a.getMinimumInteger() > b.getMinimumInteger()) {
            return new IntRange(a.getMinimumInteger(), b.getMaximumInteger());
        } else {
            return new IntRange(b.getMinimumInteger(), a.getMaximumInteger());
        }
    }

    /**
     * Checks whether the {@link IntRange} "a" overlaps with {@link IntRange}
     * "b" and spans over its boundaries.
     * 
     * @param a
     *            the reference interval
     * @param b
     *            the interval to be compared with a
     * @return true if b spans over a.
     */
    public static int split(final IntRange a, final IntRange b) {
        if (a.overlapsRange(b) && !b.containsRange(a)) {
            return 1;
        } else {
            return 0;
        }
    }
}