/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.api.datamodel.document;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict;

public class AnnotatedPDFModel extends AnnotatedDocumentModel {

	/**
	 * Logger
	 */
	static final Logger logger = LoggerFactory.getLogger(AnnotatedPDFModel.class);

	/**
	 * The object representing the pdf file
	 */
	private PdfDoc pdfDoc;

	private Map<Annotation,Pair<Glyph,Glyph>> annotation2glyphscoordinate;

	/**
	 * Constructor, @see {@link AnnotatedDocumentModel}
	 * @param annotations
	 * @param conflicts
	 * @param unknownAnnotation
	 * @param completedAnnotators
	 * @param pdfDoc	The object representing the pdf file
	 */
	public AnnotatedPDFModel(Set<Annotation> annotations,Set<Conflict> conflicts,
			Set<Annotation> unknownAnnotation,Set<String> completedAnnotators,PdfDoc pdfDoc,Set<String> completedAggregators){
		super(pdfDoc.getTextualContent(),annotations,conflicts,unknownAnnotation,completedAnnotators,completedAggregators);
		this.pdfDoc=pdfDoc;
		//builf the map that associates for each annotation a pair of glyphs 
		//representing the start-end character of the annotation in the pdf document
		this.annotation2glyphscoordinate=new HashMap<Annotation,Pair<Glyph,Glyph>>();

		for(Annotation annotation:this.getAllAnnotations()){
			Glyph startGlyph = this.pdfDoc.getGlyphByStartOffset(annotation.getStart());
			if(startGlyph==null){
				logger.warn("Unable to find the glyph representing the first" +
						" character of the annotation {} with start={}",annotation,annotation.getStart());
				continue;
			}

			Glyph endGlyph = this.pdfDoc.getGlyphByEndOffset(annotation.getEnd());
			if(endGlyph==null){
				logger.warn("Unable to find the glyph representing the last" +
						" character of the annotation {} with end={}",annotation,annotation.getEnd());
				continue;
			}
			this.annotation2glyphscoordinate.put(annotation, Pair.of(startGlyph, endGlyph));
		}

	}

	public PdfDoc getPdfDoc(){
		return this.pdfDoc;
	}
	
	/**
	 * Method to get the pair of glyphs representing the first and the last character of the annotation
	 * in the pdf document
	 * @param annotation The annotation
	 * @return	A pair of glyphs where the left component is the start character and the right component is the last 
	 * character. If not able to locate one of the two glyphs of the annotation null is returned
	 */
	public Pair<Glyph,Glyph> getBoundingGlyphs(Annotation annotation){
		return this.annotation2glyphscoordinate.get(annotation);
	}
	
	/**
	 * Method that given 4 bounding box parameters, return those annotations which have exactly the same
	 * bounding box parameters.
	 * A bounding box for an annotation is given as follow: left, top, bottom are taken from the first character
	 * (glyph), right is taken from the last character (glyph)
	 * @param left
	 * @param top
	 * @param right
	 * @param bottom
	 * @return The Set of annotations with the input bounding box, empty set if there are no such annotations
	 */
	public Set<Annotation> getAnnotationByBoundingBox(double left, double top, double right, double bottom){
		
		Set<Annotation> annotations = new HashSet<Annotation>();
		for(Annotation annotation:this.annotation2glyphscoordinate.keySet()){
			Pair<Glyph,Glyph> boundingGlyphs = this.annotation2glyphscoordinate.get(annotation);
			
			if(boundingGlyphs.getLeft().getLeft()==left&&boundingGlyphs.getLeft().getTop()==top
					&&boundingGlyphs.getRight().getRight()==right&&
					boundingGlyphs.getLeft().getBottom()==bottom)
				annotations.add(annotation);
		}
		
		return annotations;
	}
	
	/**
	 * Given 4 coordinates that represent a square in the pdf file,
	 * returns all those annotations that are contained in such square
	 * 
	 * @param left
	 * @param top
	 * @param right
	 * @param bottom
	 * @return The set of annotations that are inside the input defined square, an empty set if there are not such annotations
	 */
	public Set<Annotation> getCotainedAnnotation(double left, double top, double right, double bottom){
		Set<Annotation> contained = new HashSet<Annotation>();
		for(Annotation annotation:this.annotation2glyphscoordinate.keySet()){
			Pair<Glyph,Glyph> boundingGlyphs = this.annotation2glyphscoordinate.get(annotation);
			
			if(boundingGlyphs.getLeft().getLeft()>=left&&boundingGlyphs.getLeft().getTop()>=top
					&&boundingGlyphs.getRight().getRight()<=right&&
					boundingGlyphs.getLeft().getBottom()<=bottom)
				contained.add(annotation);
		}
		return contained;
	}
}
