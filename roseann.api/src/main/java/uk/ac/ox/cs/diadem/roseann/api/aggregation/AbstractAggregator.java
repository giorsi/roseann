/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.api.aggregation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.tree.ConfigurationNode;
import org.apache.commons.configuration.tree.xpath.XPathExpressionEngine;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;


/**
 * The AbstractAggregator class provide a general class for the aggregation algorithm.
 * In order to use a new Aggregation algorithm in RoseAnn, the class must extends the AbstractAggregator class
 * and implements the relative method 
 * 
 * The aggregator can also specifies some name-value parameters in a configuration file stored in conf/AggregatorConfiguration.xml
 * The method readConfigurationFile reads the configuration file and initialize a Map<String,Set<String>> containing the name-value parameters,
 * the map then can be accessed through the method getParameters
 * 
 * @author Stefano Ortona (stefano dot ortona at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */

public abstract class AbstractAggregator {

	/**
	 * the configuration file
	 */
	private final static String CONFIGURATION_FILE="conf/AggregatorConfiguration.xml";

	/**
	 * Aggregator name
	 */
	private String aggregatorName;

	private Map<String,Set<String>> parameter2values;

	/**
	 * Abstract method to implement the reconciliation algorithm.
	 * The input annotation must have as concepts the global concept in the Ontology
	 * @param originalText
	 * 					The original text annotated
	 * @param originalAnnotation
	 * 					The annotations provided by the basic annotators where each concept is already mapped
	 * 					into the global concept
	 * @param involvedAnnotators
	 * 					The set of annotator's names involved in the annotation process
	 * @return
	 */
	public abstract Set<Annotation> reconcile(final String originalText, 
			final Set<Annotation> originalAnnotation, final Set<String> involvedAnnotators) throws AggregationException;

	/**
	 * Get the aggregator name
	 * @return	the aggregator name
	 */
	public String getAggregatorName(){
		return this.aggregatorName;
	}

	/**
	 * Set the aggregator name
	 * @param name	the aggregatora name to be set
	 */
	public void setAggregatorName(final String name){
		this.aggregatorName=name;
	}
	
	/**
	 * Method to read the configuration file and initialize the parameters map
	 * @throws ConfigurationException	If there were errors while reading the conf file
	 */
	public void readConfigurationFile() throws ConfigurationException{

		parameter2values = new HashMap<String,Set<String>>();
		
		XMLConfiguration conf=null;
		conf = new XMLConfiguration(CONFIGURATION_FILE);


		conf.setExpressionEngine(new XPathExpressionEngine());

		//read the parameters
		HierarchicalConfiguration pr=conf.configurationAt("nlp/aggregators/aggregator" +
				"[@name='"+getAggregatorName()+"']/parameters");
		@SuppressWarnings("unchecked")
		final List<ConfigurationNode> parameters=pr.getRootNode().getChildren();
		for(ConfigurationNode node:parameters){
			final String name=node.getName();
			String value=node.getValue().toString();

			if(value!=null&&value.length()>0){
				Set<String> previousValues = parameter2values.get(name);
				if(previousValues==null)
					previousValues=new HashSet<String>();
				previousValues.add(value);
				parameter2values.put(name, previousValues);
			}
		}
	}
	
	/**
	 * Method to get the parameters name values map, where the key is the name of the parameter and the value is the the set of values
	 * for the parameter
	 * @return	The prameters map
	 */
	public Map<String,Set<String>> getParametersMap(){
		return this.parameter2values;
	}


}
