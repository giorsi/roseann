/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web;

/**
 * Class that represents an annotated html span.
 * An annotated span is represent by the id of the start node, the offset within the start node,
 * the id of the end node, the offset within the end node and the length of the character of the
 * annotated span
 * 
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk), Department of Computer Science, University of Oxford
 *
 */
public class AnnotatedHtmlSpan {

	private String startNodeId;

	private String endNodeId;

	private long offsetStart;

	private long offsetEnd;

	private int annotationLength;

	public String getStartNodeId() {
		return startNodeId;
	}

	public void setStartNodeId(String startNodeId) {
		this.startNodeId = startNodeId;
	}

	public String getEndNodeId() {
		return endNodeId;
	}

	public void setEndNodeId(String endNodeId) {
		this.endNodeId = endNodeId;
	}

	public long getOffsetStart() {
		return offsetStart;
	}

	public void setOffsetStart(long offsetStart) {
		this.offsetStart = offsetStart;
	}

	public long getOffsetEnd() {
		return offsetEnd;
	}

	public void setOffsetEnd(long offsetEnd) {
		this.offsetEnd = offsetEnd;
	}

	public int getAnnotationLength() {
		return annotationLength;
	}

	public void setAnnotationLength(int annotationLength) {
		this.annotationLength = annotationLength;
	}
	
	@Override
	public int hashCode(){
		return (int)(this.startNodeId.hashCode()+this.endNodeId.hashCode()+this.offsetStart+this.offsetEnd+annotationLength);
	}
	
	@Override
	public boolean equals(Object o){
		AnnotatedHtmlSpan span = (AnnotatedHtmlSpan) o;
		return (this.startNodeId.equals(span.getStartNodeId())&&
				this.endNodeId.equals(span.getEndNodeId()) &&
				this.annotationLength==span.getAnnotationLength()&&
				this.offsetStart==span.getOffsetStart()&&
				this.offsetEnd==span.getOffsetEnd());
	}
	
	@Override
	public String toString(){
		return this.startNodeId+"_"+this.offsetStart+"_"+this.endNodeId+"_"+this.offsetEnd+"_"+this.annotationLength;
	}
}
