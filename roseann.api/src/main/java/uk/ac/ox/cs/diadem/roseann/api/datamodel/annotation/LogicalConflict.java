/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.lang3.tuple.Pair;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;

/**
 * A logical conflict is a conflict involving only two annotations having disjoint concepts
 * 
 * @author Stefano Ortona (stefano dot ortona at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */

public class LogicalConflict extends Conflict{
	
	/**
	 * Pair of conflicting annotations
	 */
	final Pair<Annotation,Annotation> conflictingAnnotations;
	
	/**
	 * Public constructor
	 * @param start
	 * @param end
	 * @param involvedAnnotation	the set of the 2 conflicting annotations. If the set contains more than 2
	 * 								annotations only the first two will be considered
	 */
	public LogicalConflict(String id,final long start,final long end,Set<Annotation> involvedAnnotation){
		super(id,start,end,involvedAnnotation);
		final Set<Annotation> twoInvolvedAnnotations = new HashSet<Annotation>();
		final Iterator<Annotation> iterator = involvedAnnotation.iterator();
		final Annotation firstAnnotation = iterator.next();
		twoInvolvedAnnotations.add(firstAnnotation);
		final Annotation secondAnnotation = iterator.next();
		twoInvolvedAnnotations.add(secondAnnotation);
		//create the pair object
		conflictingAnnotations=Pair.of(firstAnnotation, secondAnnotation);
		this.setType(ConflictType.LOGICAL);
	}
	
	/**
	 * Get the pair of conflicting annotations
	 * @return	the pair of conflicting annotations
	 */
	public Pair<Annotation,Annotation> getConflictingAnnotation(){
		return this.conflictingAnnotations;
	}

}
