/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.math.IntRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import uk.ac.ox.cs.diadem.model.Atom;
import uk.ac.ox.cs.diadem.model.Model;
import uk.ac.ox.cs.diadem.model.Term;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.AnnotatedDocumentModel;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.DOMElement;
import uk.ac.ox.cs.diadem.webapi.utils.XPathUtil;
public class AnnotatedHTMLModel extends AnnotatedDocumentModel{

	private final static Logger LOGGER = LoggerFactory.getLogger(AnnotatedHTMLModel.class);

	/**
	 * The object representing the html web page
	 */
	private HtmlDoc htmlDoc;

	private Map<AnnotatedHtmlSpan,Set<Annotation>> span2annotations;

	private Map<Annotation,AnnotatedHtmlSpan> annotation2span;

	private Map<Annotation,AnnotationDomRange> annotation2range;


	/**
	 * Constructor, @see {@link AnnotatedDocumentModel}
	 * @param annotations
	 * @param conflicts
	 * @param unknownAnnotation
	 * @param completedAnnotators
	 * @param htmlDoc	The object representing the html web page
	 * @throws IOException If there are some IO problems extracting the textual visible content from the web page
	 */
	public AnnotatedHTMLModel(Set<Annotation> annotations,Set<Conflict> conflicts,
			Set<Annotation> unknownAnnotation,Set<String> completedAnnotators,HtmlDoc htmlDoc,
			Set<String> completedAggregators) throws IOException{
		super(htmlDoc.getTextualContent(),annotations,conflicts,unknownAnnotation,completedAnnotators,completedAggregators);
		this.htmlDoc=htmlDoc;
		this.retrieveAnnotationHtmlSpan(getAllAnnotations());
		this.retrieveAnnotationDomRange(getAllAnnotations());
	}

	public HtmlDoc getHtmlDoc(){
		return this.htmlDoc;
	}


	public Set<AnnotatedHtmlSpan> getAnnotatedSpan(){
		Set<AnnotatedHtmlSpan> setCopy=null;
		if(span2annotations!=null){
			setCopy=new HashSet<AnnotatedHtmlSpan>();
			setCopy.addAll(span2annotations.keySet());
		}
		return setCopy;
	}

	/**
	 * Return all the annotation that insists on a given span
	 * @param span
	 * @return
	 */
	public Set<Annotation> getAnnotationByAnnotatedSpan(final AnnotatedHtmlSpan span){
		Set<Annotation> setCopy=null;
		if(span2annotations!=null){
			setCopy=new HashSet<Annotation>();
			setCopy.addAll(span2annotations.get(span));
		}
		return setCopy;
	}

	public AnnotatedHtmlSpan getSpanByAnnotation(Annotation annotation){
		if(span2annotations==null)
			return null;
		return annotation2span.get(annotation);
	}

	@SuppressWarnings("unchecked")
	private void retrieveAnnotationHtmlSpan(final Set<Annotation> annotations) {

		this.span2annotations = new HashMap<AnnotatedHtmlSpan,Set<Annotation>>();
		this.annotation2span = new HashMap<Annotation,AnnotatedHtmlSpan>();

		Model browserModel = this.htmlDoc.getBrowserModel();
		if(browserModel==null){
			LOGGER.warn("The Browser Model in the annotated html model is null!");
			return;
		}

		LOGGER.debug("Store annotations into an interval tree.");
		final IntervalAnnotationTree intervalTree = new IntervalAnnotationTree();

		// Retrieve the annotation atoms from the annotation model
		for (final Annotation annotation : annotations) {
			final Set<Annotation> annotationSet = new HashSet<Annotation>();
			annotationSet.add(annotation);
			/*
			 * Associate to this (set of) atom(s) a closed interval, i.e., [start,end) is equivalent to [start,end-1]
			 */
			intervalTree.addInterval(new IntervalAnnotation(new IntRange(annotation.getStart(),
					annotation.getEnd()), annotationSet));
		}

		final Set<Atom> clobAtoms = browserModel.getAtomsByPredicate("dom1__clob0__text");
		if (clobAtoms.size() == 1) {

			final Atom clobAtom = clobAtoms.iterator().next();

			final Term clobId = clobAtom.getSubTerm(1);

			// For each content element check whether there are annotations
			// overlapping the content
			for (final Atom content : browserModel.getAtomsByPrefixMask("dom1__clob0__span", null, null, clobId)) {



				final Term nodeId = content.getSubTerm(1);

				final int start = Integer.parseInt(content.getSubTerm(3).toString());
				// the interval in the DOM facts is open [start, end). In order to
				// compare it with the Interval tree, we must close it [start,
				// end-1].
				final int end = Integer.parseInt(content.getSubTerm(4).toString());

				//consider only the text nodes
				if(browserModel.getAtomsByPrefixMask("dom1__text0__node", null,nodeId).size()==0)
					continue;

				// skip empty elements
				if (end >= start) {
					// The (closed) range of the content
					final IntRange contentRange = new IntRange(start, end);

					// If the annotation range overlaps the content range produce an
					// annotation for the corresponding DOM node
					final Set<Annotation> overlappingAnnotations = intervalTree.get(contentRange);

					// For each overlapping annotation
					for (final Annotation annotation : overlappingAnnotations) {

						long annotationStart = annotation.getStart();
						long annotationEnd = annotation.getEnd();

						if(start<=annotationStart&&end>annotationStart){

							// Compute the range of the annotation
							final IntRange annotationRange = new IntRange(annotationStart,
									annotationEnd);

							// Compute the projection of the annotation range over the content range
							final IntRange projection = Intervals.project(annotationRange, contentRange);
							assert projection != null;

							//final int coverage = Intervals.covers(annotationRange, contentRange);
							final int elementFrom = projection.getMinimumInteger() - contentRange.getMinimumInteger();
							//final int elementTo = projection.getMaximumInteger() - contentRange.getMinimumInteger() + 1;

							//save the new found span in the map
							AnnotatedHtmlSpan span = this.annotation2span.get(annotation);
							if(span == null){
								span = new AnnotatedHtmlSpan();
								span.setOffsetStart(elementFrom);
								span.setStartNodeId(nodeId.toString());
								this.annotation2span.put(annotation, span);
							}
							else{
								//check that an initial node has not been found so far
								if(span.getStartNodeId()!=null)
									LOGGER.warn("Found multiple starting text nodes for the " +
											"annotaiton with start-end {},{}",annotation.getStart(),annotation.getEnd());
								span.setOffsetStart(elementFrom);
								span.setStartNodeId(nodeId.toString());
								span.setAnnotationLength((int)(annotation.getEnd()-annotation.getStart()));
								Set<Annotation> spanAnnotations = this.span2annotations.get(span);
								if(spanAnnotations==null)
									spanAnnotations=new HashSet<Annotation>();
								spanAnnotations.add(annotation);
								this.span2annotations.put(span, spanAnnotations);
								this.annotation2span.put(annotation, span);
							}
						}

						if(end>=annotationEnd&&start<annotationEnd){

							// Compute the range of the annotation
							final IntRange annotationRange = new IntRange(annotationStart,
									annotationEnd);

							// Compute the projection of the annotation range over the content range
							final IntRange projection = Intervals.project(annotationRange, contentRange);
							assert projection != null;

							//final int coverage = Intervals.covers(annotationRange, contentRange);
							//final int elementFrom = projection.getMinimumInteger() - contentRange.getMinimumInteger();
							final int elementTo = projection.getMaximumInteger() - contentRange.getMinimumInteger();

							AnnotatedHtmlSpan span = this.annotation2span.get(annotation);
							if(span == null){
								span = new AnnotatedHtmlSpan();
								span.setOffsetEnd((elementTo));
								span.setEndNodeId(nodeId.toString());
								this.annotation2span.put(annotation, span);
							}
							else{
								//check that an initial node has not been found so far
								if(span.getEndNodeId()!=null)
									LOGGER.warn("Found multiple ending text nodes for the " +
											"annotaiton with start-end {},{}",annotation.getStart(),annotation.getEnd());
								span.setOffsetEnd(elementTo);
								span.setEndNodeId(nodeId.toString());
								span.setAnnotationLength((int)(annotation.getEnd()-annotation.getStart()));
								Set<Annotation> spanAnnotations = this.span2annotations.get(span);
								if(spanAnnotations==null)
									spanAnnotations=new HashSet<Annotation>();
								spanAnnotations.add(annotation);
								this.span2annotations.put(span, spanAnnotations);
								this.annotation2span.put(annotation, span);
							}
						}


					}
				}
			}
		}

	}

	/**
	 * For each annotation, build an AnnotationDomRange
	 * @param annotations
	 */
	@SuppressWarnings("unchecked")
	private void retrieveAnnotationDomRange(Set<Annotation> annotations){
		this.annotation2range=new HashMap<Annotation,AnnotationDomRange>();

		Model browserModel = this.htmlDoc.getBrowserModel();
		if(browserModel==null)
			return;
		for(Annotation annotation:annotations){

			try{
				AnnotatedHtmlSpan span = this.annotation2span.get(annotation);

				int startOffset = (int) span.getOffsetStart();
				int endOffset = (int) span.getOffsetEnd();
				span.getOffsetEnd();
				String startNodeId = span.getStartNodeId();
				String endNnodeId = span.getEndNodeId();

				if(browserModel.getAtomsByPrefixMask("dom1__node0__id", 
						null,browserModel.getConstantTerm(startNodeId)).size()!=1){
					LOGGER.warn("Found multiple facts dom1__node0__id for the node with id {}",startNodeId);
					continue;
				}

				if(browserModel.getAtomsByPrefixMask("dom1__node0__id", 
						null,browserModel.getConstantTerm(endNnodeId)).size()!=1){
					LOGGER.warn("Found multiple facts dom1__node0__id for the node with id {}",endNnodeId);
					continue;
				}

				String xpathEnd = browserModel.getAtomsByPrefixMask("dom1__node0__id", 
						null,browserModel.getConstantTerm(endNnodeId)).iterator().next().
						getSubTerm(2).toString().replaceAll("\"", "");

				String xpathStart = browserModel.getAtomsByPrefixMask("dom1__node0__id", 
						null,browserModel.getConstantTerm(startNodeId)).iterator().next().
						getSubTerm(2).toString().replaceAll("\"", "");

				AnnotationDomRange range = new AnnotationDomRange();
				range.setXpathStart(xpathStart);
				range.setXpathEnd(xpathEnd);
				range.setStartOffset(startOffset);
				range.setEndOffset(endOffset);

				this.annotation2range.put(annotation, range);
			}
			catch(Exception e){
				LOGGER.warn("Error while retrieveing the AnnotationDomRange for the annotation {}",annotation,e);
			}
		}

	}

	/**
	 * Method to get the AnnotationDomRange of a given input annotation
	 * @param annotation	The annotation to get the AnnotationDomRange which refers to
	 * @return	The AnnotationDomRange associated with the input annotation, null if the annotation
	 * is unknwon in the document or there has been a problem to retrieve the Dom Range for the annotation
	 */
	public AnnotationDomRange getAnnotationDomRange(Annotation annotation){
		return this.annotation2range.get(annotation);
	}

	/**
	 * Get all the annotations that are contained into a give DOM node (only element nodes, no text node allowed).
	 * An annotation is contained into a DOM node iff its start and end text nodes are both children 
	 * of the input DOM node
	 * This method will work iff the browser window of the annotated page is kept alive, otherwise
	 * an exception will be thrown 
	 * WARNING: the method has not been optimized, on very big web pages it might take up to a minute to compute 
	 * all the annotations
	 * @param node	the DOMElement to find the annotations contained into the node
	 * @return	The set of Annotation contained into the dom node
	 */
	public Set<Annotation> getAnnotationNodeContained(DOMElement node){
		WebBrowser browser = htmlDoc.getWebBrowser();
		if(browser==null){
			LOGGER.warn("You are trying to perfrom some actions that require the web browser to be running, " +
					"but it has been shut down");
			return null;
		}

		Set<Annotation> containedAnnotation = new HashSet<Annotation>();

		for(Annotation annotation:getAllAnnotations()){

			AnnotationDomRange range = getAnnotationDomRange(annotation);
			String startXpath = range.getXpathStart();
			String endXpath = range.getXpathEnd();

			//check if the current annotation is contained in the node
			boolean isContained=false;
			if(startXpath.equals(endXpath)){
				String []allExpressions = {startXpath};
				isContained = node.js().isAncestorOf(allExpressions);
			}
			else{
				String []allExpressions = {startXpath,endXpath};
				isContained = node.js().isAncestorOf(allExpressions);
			}
			
			if(isContained)
				containedAnnotation.add(annotation);
		}
		return containedAnnotation;
	}

	/**
	 * Get all the annotations that are contained into a node identifed by an input xpath 
	 * (only xpath that identifies element nodes, no text node allowed).
	 * An annotation is contained into a DOM node iff its start and end text nodes are both children 
	 * of the input DOM node
	 * This method will work iff the browser window of the annotated page is kept alive, otherwise
	 * an exception will be thrown
	 * WARNING: the method has not been optimized, on very big web pages it might take up to a minute to compute 
	 * all the annotations 
	 * @param node	the DOMNode to find the annotations contained into the node
	 * @return	The set of Annotation contained into the dom node, null if the xpath identifies a text node
	 */
	public Set<Annotation> getAnnotationXpathContained(String xpath){
		WebBrowser browser = htmlDoc.getWebBrowser();
		if(browser==null){
			LOGGER.warn("You are trying to perfrom some actions that require the web browser to be running, " +
					"but it has been shut down");
			return null;
		}

		DOMElement xpathNode = null;
		try{
			xpathNode = (DOMElement) XPathUtil.getFirstNode(xpath, browser);
		}
		catch(Exception e){
			LOGGER.warn("The input xpath {} does not identify an element node in the DOM tree of the web page",xpath);
			return null;
		}
		
		//if the xpath didn't find any dom element, returns an empty set
		if(xpathNode==null) return new HashSet<Annotation>();
		
		return getAnnotationNodeContained(xpathNode);
	}

	/**
	 * Method to close the browser window if it has been left open. If the window is not open, nothing will happen
	 */
	public void shutDownBrowser(){
		this.htmlDoc.shutDownBrowser();
	}

}
