/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation;

import java.util.Set;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;

/**
 * An omission conflict is a conflict when there is an annotation with concept c and another annotator 
 * (or more than one) which has knowledge of c (or a superclass)
 * did not annotate the same span with c (or a superclass)
 * The omission conflict referes only about a concept c and a set of annotations (agreed annotations) which 
 * have annotated the same span with c (or a superclass) and a set of annotators (omitted annotators) 
 * which have knowledge about c (or a superclass) but did not annotate the involved span with c (or a superclass)
 * 
 * @author Stefano Ortona (stefano dot ortona at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */

public class OmissionConflict extends Conflict{
	
	/**
	 * The agreed_annotators
	 */
	private Set<String> agreedAnnotators;
	
	/**
	 * The annotators which didn't annotate with the same concept but have knowledge about the concept
	 */
	private Set<String> omittedAnnotators;
	
	/**
	 * The concept involved in omission conflict
	 */
	private String concept;
	
	/**
	 * Public constructor
	 * @param start
	 * @param end
	 * @param involvedAnnotation
	 * @param agreedAnnotators
	 * @param omittedAnnotators
	 * @param concept
	 */
	public OmissionConflict(String id,long start,long end,Set<Annotation> involvedAnnotation
			,Set<String> agreedAnnotators,Set<String> omittedAnnotators,
			final String concept){
		super(id,start,end,involvedAnnotation);
		this.agreedAnnotators=agreedAnnotators;
		this.omittedAnnotators=omittedAnnotators;
		this.concept=concept;
		this.setType(ConflictType.OMISSION);
	}
	
	/**
	 * Return the annotators which annotate with the concept c (or a superclass) the involved span conflict
	 * @return	the agreed annotators
	 */
	public Set<String> getAgreedAnnotators(){
		return this.agreedAnnotators;
	}
	
	/**
	 * Return the annotators which didn't annotate with the concept c (or a superclass) 
	 * the involved span conflict but have knowledge pf c (or a superclass)
	 * @return	the omitted annotators
	 */
	public Set<String> getOmittedAnnotators(){
		return this.omittedAnnotators;
	}
	
	/**
	 * Get the concept involved in the omission conflict
	 * @return	the concept involved in the omission conflict
	 */
	public String getConcept(){
		return this.concept;
	}

}
