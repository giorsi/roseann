/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.domFactsGenerator.DOMFactGenerator;
import uk.ac.ox.cs.diadem.model.Model;
import uk.ac.ox.cs.diadem.model.ModelFactory;
import uk.ac.ox.cs.diadem.roseann.api.RoseAnnException;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.WebBrowser.FeatureType;
import uk.ac.ox.cs.diadem.webapi.dom.DOMCSS2Properties.CssProperty;
import uk.ac.ox.cs.diadem.webapi.dom.DOMDocument;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;

public class HtmlDoc {

	private WebBrowser	browser;

	/**
	 * @return the pageUri
	 */
	public URI getPageUri() {
		return pageUri;
	}

	/**
	 * Logger
	 */
	static final Logger	LOGGER	= LoggerFactory.getLogger(HtmlDoc.class);

	/**
	 * The page_uri
	 */
	private URI	        pageUri;

	/**
	 * The visible textual content of the page
	 */
	private String	    textualContent;

	/**
	 * The internal web browser model
	 */
	private Model	    webBrowserModel;

	public HtmlDoc(URI pageUri, boolean keepBrowserAlive) throws IOException {

		WebBrowser wb = null;
		try {
			this.pageUri = pageUri;

			LOGGER.info("Extracting the textual content from the web page '{}'", pageUri.toString());

			wb = BrowserFactory.newBuilder(Engine.WEBDRIVER_FF).disable(FeatureType.PLUGINS).newWebBrowser();
			wb.enableFeatures(FeatureType.FIREBUG_HIDDEN);
			wb.setWindowSize(0, 0);

			wb.navigate(this.pageUri.toString(), true);

			Model browserModel = ModelFactory.createIsolatedModel();

			// save the browser model in a temporary file
			final File tmpFile = new File("tmp");

			final File browserFactsFile = new File("tmp");

			final DOMFactGenerator generator = DOMFactGenerator.newDOMFactGenerator(wb);
			final Writer browserModelWriter = new FileWriter(browserFactsFile);

			generator.factsFor().allElementsExcept(browserModelWriter, "script", "noscript", "style").with()
			        .text(browserModelWriter).withElementClob(browserModelWriter).and().ids(browserModelWriter).and()
			        .attributes().onlyFor(browserModelWriter, "selected").and().css()
			        .onlyProperties(browserModelWriter, CssProperty.display, CssProperty.visibility);

			generator.generateFacts();

			// close.
			browserModelWriter.close();

			// read back the model
			browserModel.read(browserFactsFile);

			this.webBrowserModel = browserModel;

			final String textToAnnotate = browserModel.getAtomsByPredicate("dom1__clob0__text").iterator().next()
			        .getSubTerm(2).getValue().toString();

			// check the text to annotate is not null
			if (textToAnnotate == null) {
				throw new RoseAnnException("There is no visible text to be extracted from the web page", LOGGER);
			}

			// check the wanted page has been extracted and not a not found page
			if (textToAnnotate.contains("Problem loading page") && textToAnnotate.contains("File not found")
			        && textToAnnotate.contains("Firefox can't find the file")) {
				throw new RoseAnnException("The specified html document cannot be found", LOGGER);
			}

			this.browser = wb;

			LOGGER.info("Textual content extracted from the web page '{}' succesfully: '{}'", pageUri.toString(),
			        textToAnnotate);

			// delete the temporary file
			if (tmpFile.exists())
				tmpFile.delete();

			this.textualContent = textToAnnotate;
		}
		// if an exception is thrown, make sure the browser is shut down
		catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (wb != null && !keepBrowserAlive) {
				wb.shutdown();
				this.browser = null;
			}
		}
	}

	/**
	 * Public method to get as a string the textual visible content of the web
	 * page
	 * 
	 * @return The String representing the textual content of the web page
	 * @throws IOException
	 *             If there are some IO errors during the extraction of the
	 *             textual visible content from the web page
	 */
	public String getTextualContent() {

		return this.textualContent;
	}

	public Model getBrowserModel() {
		return this.webBrowserModel;
	}

	/**
	 * Method to get a DOMDocument object, compatible with W3C standard. This
	 * method will return a valid object iff the browser window of the current
	 * page is kept alive, otherwise return null
	 * 
	 * @return The DOMDocument representing the current web page, null if the
	 *         browser window has been shut down
	 */
	public DOMDocument getDOMDocument() {
		if (this.browser == null) {
			LOGGER.warn("You are trying to access the DOM Document of the page when the browser window has"
			        + " been shut down, it needs to be running");
			return null;
		}
		return this.browser.getContentDOMWindow().getDocument();
	}

	public WebBrowser getWebBrowser() {
		return this.browser;
	}

	/**
	 * Shut down the browser and set it to null
	 */
	public void shutDownBrowser() {
		if (this.browser != null) {
			this.browser.shutdown();
			this.browser = null;
		}
	}

}
