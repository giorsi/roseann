/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.math.IntRange;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;


/**
 * @author Giorgio Orsi (giorgio dot orsi at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 * 
 */
class IntervalAnnotationTree {

	private IntervalAnnotationTreeNode head;
	private final List<IntervalAnnotation> intervalList;
	private boolean inSync;
	private int size;

	/**
	 * Instantiate a new interval tree with no intervals
	 */
	public IntervalAnnotationTree() {
		head = new IntervalAnnotationTreeNode();
		intervalList = new ArrayList<IntervalAnnotation>();
		inSync = true;
		size = 0;
	}

	/**
	 * Perform a stabbing query, returning the associated data Will rebuild the
	 * tree if out of sync
	 * 
	 * @param time
	 *            the time to stab
	 * @return the data associated with all intervals that contain time
	 */
	public Set<Annotation> get(final int point) {
		final List<IntervalAnnotation> intervals = getIntervals(point);
		final Set<Annotation> result = new HashSet<Annotation>();
		for (final IntervalAnnotation interval : intervals) {
			result.addAll(interval.getData());
		}
		return result;
	}

	/**
	 * Perform a stabbing query, returning the interval objects Will rebuild the
	 * tree if out of sync
	 * 
	 * @param time
	 *            the time to stab
	 * @return all intervals that contain time
	 */
	public List<IntervalAnnotation> getIntervals(final int point) {
		build();
		return head.get(point);
	}

	/**
	 * Perform an interval query, returning the associated data Will rebuild the
	 * tree if out of sync
	 * 
	 * @param start
	 *            the start of the interval to check
	 * @param end
	 *            the end of the interval to check
	 * @return the data associated with all intervals that intersect target
	 */
	public Set<Annotation> get(final IntRange target) {
		final List<IntervalAnnotation> intervals = getIntervals(target);
		final Set<Annotation> result = new HashSet<Annotation>();
		for (final IntervalAnnotation interval : intervals) {
			result.addAll(interval.getData());
		}
		return result;
	}

	/**
	 * Perform an interval query, returning the interval objects Will rebuild
	 * the tree if out of sync
	 * 
	 * @param start
	 *            the start of the interval to check
	 * @param end
	 *            the end of the interval to check
	 * @return all intervals that intersect target
	 */
	public List<IntervalAnnotation> getIntervals(final IntRange interval) {
		build();
		return head.query(new IntervalAnnotation(interval, null));
	}

	/**
	 * Add an interval object to the interval tree's list Will not rebuild the
	 * tree until the next query or call to build
	 * 
	 * @param interval
	 *            the interval object to add
	 */
	public void addInterval(final IntervalAnnotation interval) {
		if (!interval.getData().isEmpty()) {
			intervalList.add(interval);
			inSync = false;
		}
	}

	/**
	 * Determine whether this interval tree is currently a reflection of all
	 * intervals in the interval list
	 * 
	 * @return true if no changes have been made since the last build
	 */
	public boolean inSync() {
		return inSync;
	}

	/**
	 * Build the interval tree to reflect the list of intervals, Will not run if
	 * this is currently in sync
	 */
	public void build() {
		if (!inSync) {
			head = new IntervalAnnotationTreeNode(intervalList);
			inSync = true;
			size = intervalList.size();
		}
	}

	/**
	 * @return the number of entries in the currently built interval tree
	 */
	public int currentSize() {
		return size;
	}

	/**
	 * @return the number of entries in the interval list, equal to .size() if
	 *         inSync()
	 */
	public int listSize() {
		return intervalList.size();
	}

}
