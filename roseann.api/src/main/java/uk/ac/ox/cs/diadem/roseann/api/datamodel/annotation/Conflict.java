/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation;

import java.util.Set;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;

/**
 * Class that implements the concept of conflict
 * A conflict is a span where there are two (or more) annotations having disjoint types (logical conflict)
 * or when there is an annotation with concept c and another annotator which has knowledge of c (or a superclass)
 * did not annotate the same span with c (or a superclass) (omission conflict)
 * 
 * @author Stefano Ortona (stefano dot ortona at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class Conflict {
	
	public enum ConflictType  {OMISSION,LOGICAL};
	
	/**
	 * The conflict id
	 */
	private String id;
	
	/**
	 * Public constructor
	 * @param start
	 * @param end
	 * @param involvedAnnotation
	 */
	public Conflict(String id,final long start,final long end,Set<Annotation> involvedAnnotation){
		this.id=id;
		this.start=start;
		this.end=end;
		this.invovledAnnotation=involvedAnnotation;
	}
	
	/**
	 * the conflict type
	 */
	private ConflictType type;
	
	/**
	 * start offset
	 */
	private long start;
	
	/**
	 * end offset
	 */
	private long end;
	
	/**
	 * the annotations involved in the conflict
	 */
	private Set<Annotation> invovledAnnotation;
	
	/**
	 * Get the conflict type
	 * @return
	 */
	public ConflictType getType(){
		return type;
	}
	
	/**
	 * get the start offset
	 * @return	start offset
	 */
	public long getStart(){
		return this.start;
	}
	
	/**
	 * get the end offset
	 * @return	the end offset
	 */
	public long getEnd(){
		return this.end;
	}
	
	/**
	 * Get the conflict id
	 * @return	the conflict id
	 */
	public String getId(){
		return this.id;
	}
	
	/**
	 * get the involved annotations
	 * @return	the involved annotations
	 */
	public Set<Annotation> getInvolvedAnnotations(){
		return this.invovledAnnotation;
	}
	
	/**
	 * Set the conflict type
	 * @param type
	 */
	public void setType(ConflictType type){
		this.type=type;
	}
	
	@Override
	public String toString(){
		String object=id+"_"+start+"_"+end+"_"+type;
		for(Annotation annotation:invovledAnnotation){
			object+="_"+annotation.getId();
		}
		
		return object;
	}

}
