/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.illinoisner;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.annotation.TextAnnotationBuilder;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.View;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.ner.NERAnnotator;
import edu.illinois.cs.cogcomp.ner.NerAnnotatorManager;
import edu.illinois.cs.cogcomp.nlp.tokenizer.IllinoisTokenizer;
import edu.illinois.cs.cogcomp.nlp.utility.TokenizerTextAnnotationBuilder;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;

public class IllinoisNERAnnotatorTest {

  static final Logger LOGGER = LoggerFactory.getLogger(IllinoisNERAnnotatorTest.class);

  private Annotator illinois;

  private String text;

  @Before
  public void bringUp() {
    illinois = IllinoisNERAnnotator.getInstance();
    text = "Barack Obama is the new president of the United States.";
  }

  /**
   * Test to check that a text can be submitted to IllinoisNER annotator
   *
   * @throws InvalidSyntaxException
   */
  @Test
  public void testSubmitToAnnotator() {

    // check that the text can be submitted to the annotator and no exceptions
    // are thrown
    final String response = illinois.submitToAnnotator(text);

    Assert.assertNotNull(response);
    Assert.assertTrue(response.length() > 0);

    // check the response is in a desired format
    Assert.assertTrue(response.equals("[PER Barack Obama ] [LOC United States ] "));
  }

  /**
   * Test to verify the entities retrieved from IllinoisNER annotator
   */
  @Test
  public void testAnnotateEntities() {
    final Set<Annotation> annotations = illinois.annotateEntity(text);
    Assert.assertNotNull(annotations);
    Assert.assertTrue(annotations.size() > 0);

    // check the returned annotations contain the desidered annotations
    final Set<Annotation> expected_annotations = new HashSet<Annotation>();
    final Annotation a1 = new EntityAnnotation();
    a1.setId("illinoisNER_0");
    a1.setConcept("PER");
    a1.setAnnotationClass(AnnotationClass.INSTANCE);
    a1.setStart(0);
    a1.setEnd(12);
    a1.setOriginAnnotator("illinoisNER");
    expected_annotations.add(a1);

    final Annotation a2 = new EntityAnnotation();
    a2.setId("illinoisNER_1");
    a2.setConcept("LOC");
    a2.setAnnotationClass(AnnotationClass.INSTANCE);
    a2.setStart(41);
    a2.setEnd(54);
    a2.setOriginAnnotator("illinoisNER");
    expected_annotations.add(a2);

    Assert.assertEquals(annotations, expected_annotations);

  }

  /**
   * Test to verify that no entitie ares retrieved from IllinoisNER annotator
   * with an senseless text
   */
  @Test
  public void testAnnotateNoEntities() {
    text = "cfrgfvdfgbfdgdgdfgdfgdgdg";

    final Set<Annotation> annotations = illinois.annotateEntity(text);
    Assert.assertNotNull(annotations);
    Assert.assertTrue(annotations.size() == 0);

  }

  @Test
  public void testIllinoisNERDirect() {

    final String testText = "NOHO Ltd. partners with Telford International Company Inc";
    final TextAnnotationBuilder tab = new TokenizerTextAnnotationBuilder(new IllinoisTokenizer());
    final NERAnnotator nerAnnotator = NerAnnotatorManager.buildNerAnnotator(new ResourceManager(new Properties()),
        ViewNames.NER_CONLL);

    final TextAnnotation ta = tab.createTextAnnotation(testText);
    nerAnnotator.addView(ta);
    final View view = ta.getView(nerAnnotator.getViewName());

    for (final Constituent c : view.getConstituents()) {
      System.out.print(StringUtils.join("[", c.getStartCharOffset(), ",", c.getEndCharOffset(), "] :"));
      System.out.println(c.getLabel());
    }

  }
}
