/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.illinoisner;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.annotation.TextAnnotationBuilder;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.View;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.ner.NERAnnotator;
import edu.illinois.cs.cogcomp.ner.NerAnnotatorManager;
import edu.illinois.cs.cogcomp.nlp.tokenizer.IllinoisTokenizer;
import edu.illinois.cs.cogcomp.nlp.utility.TokenizerTextAnnotationBuilder;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;

/**
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk) Oxford University,
 *         Department of Computer Science The wrapper submit the text to
 *         illinoisNER annotator, take the response string in local format and
 *         then parse it to harvest entity annotations into a set of annotation
 *         objects
 */

public class IllinoisNERAnnotator extends AnnotatorAdapter {
  static final Logger LOGGER = LoggerFactory.getLogger(IllinoisNERAnnotator.class);

  /**
   * Tagger classifier for IllinoisNET
   */
  private static NERAnnotator illinoisTagger;
  private static TextAnnotationBuilder tab;

  /**
   * id counter to increment the annotation id
   */
  private long id_counter;

  /**
   * Singleton instance
   */
  private static IllinoisNERAnnotator INSTANCE;

  /**
   * Private constructor to build the singleton instance
   */
  private IllinoisNERAnnotator() {
    ConfigurationFacility.getConfiguration();
    setAnnotatorName("illinoisNER");
    id_counter = 0;

    // initialize the parameters configuration
    final AnnotatorConfiguration conf = new AnnotatorConfiguration();
    conf.initializeConfiguration(getAnnotatorName());
    setConfig(conf);

    tab = new TokenizerTextAnnotationBuilder(new IllinoisTokenizer());
    illinoisTagger = NerAnnotatorManager.buildNerAnnotator(new ResourceManager(new Properties()), ViewNames.NER_CONLL);
  }

  /**
   * method to get the singleton instance
   *
   * @return singleton instance
   */
  public static synchronized Annotator getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new IllinoisNERAnnotator();
    }
    return INSTANCE;
  }

  /**
   * Convert the IllinoisNER response into a set of annotation objects
   *
   * @param sourceText
   * @param response
   * @return the set of retrieved entity annotations
   */
  private Set<Annotation> retrieveEntityAnnotations(final View view) {
    LOGGER.info("Submit text to IllinoisNER.");

    final Set<Annotation> found_annotations = new HashSet<Annotation>();
    int annotationCount = 0;

    for (final Constituent c : view.getConstituents()) {
      // create the annotation objects
      final Annotation annotation = new EntityAnnotation();
      annotation.setId(getAnnotatorName() + "_" + (id_counter + annotationCount));
      annotation.setConcept(c.getLabel());
      annotation.setAnnotationClass(AnnotationClass.INSTANCE);
      annotation.setStart(c.getStartCharOffset());
      annotation.setEnd(c.getEndCharOffset());
      annotation.setOriginAnnotator(getAnnotatorName());

      annotationCount++;
      found_annotations.add(annotation);
    }
    LOGGER.info("Response processed and annotations saved.");
    return found_annotations;

  }

  @Override
  public Set<Annotation> annotateEntity(String text) {

    // remove from the original text square brackets since they are of special
    // purpose in Illinois
    text = text.replaceAll("\\[", " ");
    text = text.replaceAll("\\]", " ");

    // save the annotations and increment the id counter
    final View view = getEntityAnnotations(text);

    final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(view);
    final int num_annotations = entityAnnotations.size();
    id_counter += num_annotations;
    return entityAnnotations;
  }

  private View getEntityAnnotations(final String text) {
    final TextAnnotation ta = tab.createTextAnnotation(text);
    illinoisTagger.addView(ta);
    return ta.getView(illinoisTagger.getViewName());
  }

  /**
   * IllinoisNER is not a web service, therefore the invocation method with the
   * timeout doesn't have any effect
   */
  @Override
  public Set<Annotation> annotateEntity(final String text, final int timeout) {
    return this.annotateEntity(text);
  }

  @Override
  public String submitToAnnotator(final String text) {
    return getEntityAnnotations(text).toString();
  }

  @Override
  public String submitToAnnotator(final String freeText, final int timeout) {
    // IllinoisNER is a standalone annotator. Timeout does not apply.
    return submitToAnnotator(freeText);
  }

}
