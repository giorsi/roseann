package uk.ac.ox.cs.diadem.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

final class Context {
  private final Set<ModelImplementation> modelImplementations = new HashSet<ModelImplementation>();
  private final Map<Atom, Context> subContexts = new HashMap<Atom, Context>();

  public Context(final ModelImplementation owner) {
    assert owner != null;
    modelImplementations.add(owner);
  }

  public int size() {
    return modelImplementations.size();
  }

  public void add(final ModelImplementation newMember) {
    assert newMember != null;
    modelImplementations.add(newMember);
  }

  public void remove(final ModelImplementation oldMember) {
    assert oldMember != null;
    assert modelImplementations.contains(oldMember) : "Violated Invariance";
    modelImplementations.remove(oldMember);
    if (modelImplementations.size() > 1)
      return;
    assert modelImplementations.size() == 1 : "Violated Invariance.";
    for (final ModelImplementation impl : modelImplementations)
      impl.decontextualize();
  }

  public boolean contains(final Model other) {
    return modelImplementations.contains(other);
  }

  public ModelImplementation getRepresentative() {
    assert !modelImplementations.isEmpty();
    return modelImplementations.iterator().next();
  }

  public Context getSubcontext(Atom atom, ModelImplementation subImplementation) {
    assert atom != null;
    assert subImplementation != null;
    Context result = subContexts.get(atom);
    if (result != null)
      return result;
    subContexts.put(atom, result = new Context(subImplementation));
    return result;
  }
}