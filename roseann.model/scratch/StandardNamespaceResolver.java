package uk.ac.ox.cs.diadem.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;

class StandardNamespaceResolver implements NamespaceResolver {
  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(StandardNamespaceResolver.class);
  @SuppressWarnings("unused")
  private static final Configuration config = ConfigurationFacility.getConfiguration();
}
