// $codepro.audit.disable declareDefaultConstructors
/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.ParserException;
import uk.ac.ox.cs.diadem.util.collect.EmptyIterableIterator;
import uk.ac.ox.cs.diadem.util.collect.IterableIterator;
import uk.ac.ox.cs.diadem.util.collect.IteratorOfIterablesIterator;
import uk.ac.ox.cs.diadem.util.collect.KeyMask;
import uk.ac.ox.cs.diadem.util.collect.Trie;
import uk.ac.ox.cs.diadem.util.collect.TrieImplementation;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 *         TODO ModelImplementation could remember if its constructed from a file only, without any modifications. If
 *         so, we can use the file directly in reasoning tasks.
 * 
 */
final class HierarchicalModelImplementation extends ModelImplementation implements HierarchicalModel {
  static final private Logger logger = LoggerFactory.getLogger(HierarchicalModelImplementation.class);

  // if context is set null, the Model has been released
  private Context context;

  public HierarchicalModelImplementation() {
  }

  private HierarchicalModelImplementation(final HierarchicalModelImplementation other, final boolean preserveContext,
      final boolean preserveAtoms) {
    super(other, preserveContext, preserveAtoms);
  }

  private HierarchicalModelImplementation(final HierarchicalModelImplementation other, final boolean preserveContext,
      final String id, final KeyMask<Term>... masks) {
    super(other, preserveContext, id, masks);
  }

  @Override
  public HierarchicalModelImplementation create(final boolean preserveContext, final String id,
      final KeyMask<Term>... masks) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    return new HierarchicalModelImplementation(this, preserveContext, id, masks);
  }

  @Override
  public HierarchicalModelImplementation create(final boolean preserveContext, final boolean preserveAtoms) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    return new HierarchicalModelImplementation(this, preserveContext, preserveAtoms);
  }

  @Override
  public boolean hasSameContext(final Model other) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    return context.contains(other);
  }

  @Override
  public boolean isIsolated() {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    return contextAtoms == null;
  }

  @Override
  public void isolate() {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    if (contextAtoms == null)
      return;
    context.remove(this);
    context = new Context(this);
    contextAtoms = null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#finalize()
   * 
   * In principle, once this is not in use anymore, you should call release, as it isolates this, reducing the number of
   * Models attached to the original context attached to this.
   */
  @Override
  public void finalize() { // $codepro.audit.disable
                           // com.instantiations.assist.eclipse.analysis.audit.rule.effectivejava.avoidFinalizers.avoidFinalizers
    if (context != null)
      release();
  }

  @Override
  public void clear() {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    atoms = new HashMap<String, Trie<Term, Atom>>();
  }

  @Override
  public void clearPredicate(final String id) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    final Trie<Term, Atom> atomsForId = atoms.get(id);
    if (atomsForId != null)
      atomsForId.clear();
  }

  @Override
  public String getStatistics() {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    final StringWriter writer = new StringWriter();
    writer.append("constant terms = " + getNumConstantTerms());
    writer.append(", string literals = " + getNumStringLiterals());
    writer.append(", integer literals = " + getNumIntegerLiterals());
    writer.append(", list terms = " + getNumListTerms());
    writer.append(", compound terms = " + getNumCompoundTerms());
    writer.append(", atoms = " + size());
    writer.append(", contextAtom = " + getNumContextAtoms());

    return writer.toString();
  }

  @Override
  public int getNumConstantTerms() {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    return constantTerms.size();
  }

  @Override
  public int getNumStringLiterals() {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    return stringLiterals.size();
  }

  @Override
  public int getNumIntegerLiterals() {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    return integerLiterals.size();
  }

  @Override
  public int getNumListTerms() {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    return listTerms.size();
  }

  @Override
  public int getNumCompoundTerms(final String id) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    final Trie<Term, CompoundTerm> termsForId = compoundTerms.get(id);
    if (termsForId == null)
      return 0;
    else
      return termsForId.size();
  }

  @Override
  public int getNumCompoundTerms() {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    int result = 0;
    for (final Trie<Term, CompoundTerm> termsForId : compoundTerms.values())
      result += termsForId.size();
    return result;
  }

  @Override
  public int sizeOfPredicate(final String id) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    final Trie<Term, Atom> atomsForId = atoms.get(id);
    if (atomsForId == null)
      return 0;
    else
      return atomsForId.size();
  }

  @Override
  public int size() {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    int result = 0;
    for (final Trie<Term, Atom> atomsForId : atoms.values())
      result += atomsForId.size();
    return result;
  }

  @Override
  public int getNumContextAtoms() {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    if (contextAtoms == null)
      return size();
    int result = 0;
    for (final Trie<Term, Atom> atomsForId : contextAtoms.values())
      result += atomsForId.size();
    return result;
  }

  @Override
  public StringLiteral getStringLiteral(final String id) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert id != null : "Invalid Argument.";
    StringLiteral result = stringLiterals.get(id);
    if (result == null)
      stringLiterals.put(id, result = new StringLiteralImplementation(id));
    return result;
  }

  @Override
  public IntegerLiteral getIntegerLiteral(final long integer) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    IntegerLiteral result = integerLiterals.get(integer);
    if (result == null)
      integerLiterals.put(integer, result = new IntegerLiteralImplementation(integer));
    return result;
  }

  @Override
  public ConstantTerm getConstantTerm(final String id) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    ConstantTerm result = constantTerms.get(id);
    if (result == null)
      constantTerms.put(id, result = new ConstantTermImplementation(id));
    return result;
  }

  @Override
  public CompoundTerm getCompoundTerm(final String id, final Term... terms) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    assert terms != null : "Invalid Argument: with Id '" + id + "'";
    assert terms.length > 0 : "Invalid Argument: with Id '" + id + "'";
    assert containsNoNullTerm(terms) : "Invalid Argument: with Id '" + id + "'";
    Trie<Term, CompoundTerm> termsForId = compoundTerms.get(id);
    if (termsForId == null)
      compoundTerms.put(id, termsForId = getTrie());
    // Potential for optimization: the next two lines traverse the trie each
    // once
    CompoundTerm result = termsForId.get(terms);
    if (result == null)
      termsForId.putOverwrite(result = new CompoundTermImplementation(id, terms), terms);
    return result;
  }

  @Override
  public ListTerm getListTerm(final Term... terms) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert terms != null : "Invalid Argument.";
    assert containsNoNullTerm(terms) : "Invalid Argument.";
    // Potential for optimization: the next two lines traverse the trie each
    // once
    ListTerm result = listTerms.get(terms);
    if (result == null)
      listTerms.putOverwrite(result = new ListTermImplementation(terms), terms);
    return result;
  }

  @Override
  public Atom add(final String id, final Term... terms) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    assert terms != null : "Invalid Argument: with Id '" + id + "'";
    assert containsNoNullTerm(terms) : "Invalid Argument: with Id '" + id + "'";

    if (contextAtoms != null) {
      Trie<Term, Atom> contextAtomsForId = contextAtoms.get(id);
      Trie<Term, Atom> atomsForId;
      if (contextAtomsForId == null) {
        contextAtoms.put(id, contextAtomsForId = getTrie());
        assert atoms.get(id) == null : "Violated Invariance";
        atoms.put(id, atomsForId = getTrie());
      } else if ((atomsForId = atoms.get(id)) == null)
        atoms.put(id, atomsForId = getTrie());
      final Atom result = contextAtomsForId.putTentatively(new AtomImplementation(id, terms), terms);
      atomsForId.putTentatively(result, terms);
      return result;
    } else {
      Trie<Term, Atom> atomsForId = atoms.get(id);
      if (atomsForId == null)
        atoms.put(id, atomsForId = getTrie());
      return atomsForId.putTentatively(new AtomImplementation(id, terms), terms);
    }
  }

  @Override
  public Atom add(final Atom atom) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert atom != null : "Invalid Argument.";
    return add(atom.getId(), atom.getSubTerms());
  }

  @Override
  public void release() {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    isolate();
    context = null;
  }

  private static <T, V> Trie<T, V> getTrie() {
    return new TrieImplementation<T, V>(true);
  }

  @Override
  public Atom get(final String id, final Term... terms) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    assert terms != null : "Invalid Argument: with Id '" + id + "'";
    assert containsNoNullTerm(terms) : "Invalid Argument: with Id '" + id + "'";
    final Trie<Term, Atom> atomsForId = atoms.get(id);
    if (atomsForId == null)
      return null;
    return atomsForId.get(terms);
  }

  @Override
  public void remove(final String id, final Term... terms) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    assert terms != null : "Invalid Argument: with Id '" + id + "'";
    assert containsNoNullTerm(terms) : "Invalid Argument: with Id '" + id + "'";
    final Trie<Term, Atom> atomsForId = atoms.get(id);
    if (atomsForId == null)
      return;
    atomsForId.remove(terms);
  }

  @Override
  public String toString() {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    final StringWriter writer = new StringWriter();
    for (final Atom atom : this) {
      writer.append(atom.toString());
      writer.append('.');
      writer.append('\n');
    }
    return writer.toString();
  }

  @Override
  public IterableIterator<Atom> iterator() {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    return new IteratorOfIterablesIterator<Atom, Trie<Term, Atom>>(atoms.values().iterator());
  }

  @Override
  public IterableIterator<Atom> iteratorForPredicate(final String id) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    final Trie<Term, Atom> atomsForId = atoms.get(id);
    if (atomsForId == null)
      return new EmptyIterableIterator<Atom>();
    return atomsForId.iterator();
  }

  @Override
  public IterableIterator<Atom> iteratorForMask(final String predicateName, final KeyMask<Term>... termMasks) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    // this time null terms are allowed!
    final Trie<Term, Atom> atomsForId = atoms.get(predicateName);
    if (atomsForId == null)
      return new EmptyIterableIterator<Atom>();
    return atomsForId.iteratorForMasks(termMasks);
  }

  @Override
  public IterableIterator<Atom> iteratorForPrefixMask(final String predicateName,
      final KeyMask<Term>... termPrefixMasks) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termPrefixMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    // this time null terms are allowed!
    final Trie<Term, Atom> atomsForId = atoms.get(predicateName);
    if (atomsForId == null)
      return new EmptyIterableIterator<Atom>();
    return atomsForId.iteratorForPrefixMasks(termPrefixMasks);
  }

  @Override
  public void remove(final Atom atom) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    remove(atom.getId(), atom.getSubTerms());
  }

  @Override
  public boolean contains(final String id, final Term... terms) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    assert terms != null : "Invalid Argument: with Id '" + id + "'";
    assert containsNoNullTerm(terms) : "Invalid Argument: with Id '" + id + "'";
    final Trie<Term, Atom> atomsForId = atoms.get(id);
    if (atomsForId == null)
      return false;
    return atomsForId.get(terms) != null;
  }

  @Override
  public boolean contains(final Atom atom) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert atom != null : "Invalid Argument.";
    final Trie<Term, Atom> atomsForId = atoms.get(atom.getId());
    if (atomsForId == null)
      return false;
    final Atom foundAtom = atomsForId.get(atom.getSubTerms());
    assert (foundAtom == null) || (foundAtom == atom) : "If foundAtom == atom, then you"
        + "are comparing for an atom which is not in the context of the current Model.";
    return foundAtom != null;
  }

  @Override
  public boolean containsPredicate(final String id) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    final Trie<Term, Atom> atomsForId = atoms.get(id);
    if (atomsForId == null)
      return false;
    return !atoms.get(id).isEmpty();
  }

  @Override
  public boolean containsMask(final String predicateName, final KeyMask<Term>... termMasks) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    // this time null terms are allowed!
    return iteratorForMask(predicateName, termMasks).hasNext(); // TODO
                                                                // improve.
  }

  @Override
  public boolean containsPrefixMask(final String predicateName, final KeyMask<Term>... termPrefixMasks) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termPrefixMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    // this time null terms are allowed!
    return iteratorForPrefixMask(predicateName, termPrefixMasks).hasNext(); // TODO
                                                                            // improve.
  }

  @Override
  public Set<Atom> getAtomsByMask(final String predicateName, final KeyMask<Term>... termMasks) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    final Set<Atom> results = new HashSet<Atom>();
    for (final Atom a : iteratorForMask(predicateName, termMasks))
      results.add(a);
    return results;
  }

  @Override
  public Set<Atom> getAtomsByPrefixMask(final String predicateName, final KeyMask<Term>... termPrefixMasks) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termPrefixMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    final Set<Atom> results = new HashSet<Atom>();
    for (final Atom a : iteratorForPrefixMask(predicateName, termPrefixMasks))
      results.add(a);
    return results;
  }

  @Override
  public Set<Term> getTermsByMask(final int termPosition, final String predicateName, final KeyMask<Term>... termMasks) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    assert 0 <= termPosition;
    assert termPosition < termMasks.length;
    final Set<Term> results = new HashSet<Term>();
    for (final Atom a : iteratorForMask(predicateName, termMasks))
      results.add(a.getSubTerms()[termPosition]);
    return results;
  }

  @Override
  public Set<Term> getTermsByPrefixMask(final int termPosition, final String predicateName,
      final KeyMask<Term>... termMasks) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    assert 0 <= termPosition;
    assert termPosition < termMasks.length;
    final Set<Term> results = new HashSet<Term>();
    for (final Atom a : iteratorForPrefixMask(predicateName, termMasks))
      results.add(a.getSubTerms()[termPosition]);
    return results;
  }

  @Override
  public Set<Atom> getAtomsByPredicate(final String id) {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    final Set<Atom> results = new HashSet<Atom>();
    for (final Atom a : iteratorForPredicate(id))
      results.add(a);
    return results;
  }

  @Override
  public Atom addByParsing(final String atomAsString) throws ParserException {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    parser.init(atomAsString);
    try {
      return parser.parseAtom();
    } catch (final IOException e) {
      e.printStackTrace();
      assert false : "Violated Invariance.";
      return null;
    }
  }

  @Override
  public Term parseTerm(final String termAsString) throws ParserException {
    assert this.context != null : "Violated Protocol: ModelImplementation already released.";
    parser.init(termAsString);
    try {
      return parser.parseTerm();
    } catch (final IOException e) {
      e.printStackTrace();
      assert false : "Violated Invariance.";
      return null;
    }
  }

  @Override
  protected boolean equatableTo(final Object other) {
    return other instanceof HierarchicalModelImplementation;
  }

  protected boolean equalsCore(final HierarchicalModelImplementation other) {
    if (!hasSameContext(other))
      return false;
    if (size() != other.size())
      return false;
    for (final Atom atom : other.iterator())
      if (!contains(atom))
        return false;
    return true;
  }

  @Override
  public boolean equals(final Object other) {
    if (this == other)
      return true;
    if (!(other instanceof HierarchicalModelImplementation))
      return false;
    final HierarchicalModelImplementation o = (HierarchicalModelImplementation) other;
    if (!o.equatableTo(this))
      return false;
    return equalsCore(o);
  }

  private static boolean isValidId(final String id) {
    assert id != null : "isValidId with null";

    if (id.isEmpty())
      return false;

    final char first = id.charAt(0);
    if (((first < 'a') || ('z' < first)) && ((first < 'A') || ('Z' < first)))
      return false;

    final int len = id.length();
    for (int i = 1; i < len; ++i) {
      final char cur = id.charAt(i);
      if (((cur < 'a') || ('z' < cur)) && ((cur < 'A') || ('Z' < cur)) && ((cur < '0') || ('9' < cur)) && (cur != '_'))
        return false;
    }
    return true;
  }

  /**
   * @param subTerms
   * @return
   */
  private static boolean containsNoNullTerm(final Term[] terms) {
    for (final Term term : terms)
      if (term == null)
        return false;
    return true;
  }

  @Override
  public void write(final Writer writer) {
    assert writer != null;
    try {
      for (final Atom atom : this) {
        atom.write(writer);
        writer.append(".\n");
      }
    } catch (final IOException e) {
      throw new ModelIOException("Failed writing Model to writer", e, logger);
    }
  }

  @Override
  public void write(final Writer writer, final String prefix) {
    assert writer != null;
    try {
      for (final Atom atom : this) {
        writer.append(prefix);
        atom.write(writer);
        writer.append(".\n");
      }
    } catch (final IOException e) {
      throw new ModelIOException("Failed writing Model to writer", e, logger);
    }
  }

  @Override
  public void write(final File file) {
    assert file != null;
    logger.debug("Saving Model to '{}'.", file);
    Writer writer = null;
    try {
      writer = new BufferedWriter(new FileWriter(file), 4096 * 4096);
      write(writer);
      writer.close();
    } catch (final IOException e) {
      throw new ModelIOException("Failed writing Model to file '" + file + "'", e, logger);
    } finally {
      logger.debug("DONE saving Model to '{}'.", file);
    }
  }

  @Override
  public void add(final Collection<Atom> atoms) {
    for (final Atom atom : atoms)
      add(atom);
  }

  @Override
  public void read(final String string) {
    read(new StringReader(string));

  }

  @Override
  public void read(final File file) {
    try {
      parser.init(new FileReader(file));
      parser.parseAtoms();
    } catch (final FileNotFoundException e) {
      throw new ModelIOException("File '" + file + "' could not be opened to read model", e, logger);
    } catch (final IOException e) {
      throw new ModelIOException("File '" + file + "' cound not be read", e, logger);
    } catch (final ParserException e) {
      throw new ModelIOException("File '" + file + "' could not be parsed as model", e, logger);
    }
  }

  @Override
  public void read(final Reader reader) {
    parser.init(reader);
    try {
      parser.parseAtoms();
    } catch (final IOException e) {
      throw new ModelIOException("Could not read model", e, logger);
    } catch (final ParserException e) {
      throw new ModelIOException("Could not parse a model", e, logger);
    }
  }

  @Override
  public HierarchicalModel getRoot() {
    // TODO Auto-generated method stub
    return null;
  }
}