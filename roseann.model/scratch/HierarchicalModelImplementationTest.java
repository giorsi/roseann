/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
public class HierarchicalModelImplementationTest extends HierarchicalModelTest {

  @Override
  public Model getModel() {
    return new HierarchicalModelImplementation();
  }

}
