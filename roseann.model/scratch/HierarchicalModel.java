// $codepro.audit.disable overloadedMethods, booleanMethodNamingConvention
/**
 * 
 */
package uk.ac.ox.cs.diadem.model; // $codepro.audit.disable packagePrefixNamingConvention

import uk.ac.ox.cs.diadem.util.collect.KeyMask;

/**
 * <p>
 * SUMMARY: HierarchicalModel is the interface for manipulating a hierarchy of logical models kept in memory.
 * </p>
 * 
 * 
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 * @version $Revision: 1.0 $
 */
public interface HierarchicalModel extends Model {
  /**
   * @return the root of the model hierarchy
   */
  HierarchicalModel getRoot();

  /**
   * moves this into its own, isolated context.
   */
  @Override
  void isolate();

  /**
   * Creates a new Model, optionally sharing the context with this, and containing all Atoms which match the predicate
   * and masks.
   * 
   * @param preserveContext
   *          if true, the newly created Model shares the context with this.
   * @param predicate
   *          the name of the predicate to copy Atoms from
   * @param masks
   *          the masks for the terms to select the Atoms to copy
   * @return a newly created Model, according to the above specification.
   */
  @Override
  HierarchicalModel create(boolean preserveContext, String predicate, KeyMask<Term>... masks);

  /**
   * Creates a new Model, optionally sharing the context with this, and optionally containing the same Atoms as this, or
   * being empty otherwise.
   * 
   * @param preserveContext
   *          if true, the newly created Model shares the context with this.
   * @param preserveAtoms
   *          if true, the newly created Model contains the same atoms as this and is empty otherwise.
   * @return a newly created Model, according to the above specification.
   */
  @Override
  HierarchicalModel create(boolean preserveContext, boolean preserveAtoms);
}