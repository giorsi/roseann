package uk.ac.ox.cs.diadem.model;

interface ModelConfiguration {
  static final String HIERARCHY_SEPARATOR = "__";
}
