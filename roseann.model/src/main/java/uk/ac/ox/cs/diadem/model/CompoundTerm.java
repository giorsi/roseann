/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * @version $Revision: 1.0 $
 * 
 */
public interface CompoundTerm extends Term {
  /**
   * @return the functor name
   */
  String getId();

  /**
   * @return the Terms of this
   */
  Term[] getSubTerms();

  /**
   * @param index
   *          with 0<=index<getSubTerms().length
   * @return the index-th Term of this
   */
  Term getSubTerm(int index);
}
