// $codepro.audit.disable declareDefaultConstructors
/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

import java.io.IOException;
import java.io.Writer;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
final class ConstantTermImplementation implements ConstantTerm {
  final private String id;

  ConstantTermImplementation(final String id) {
    this.id = id;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public String toString() {
    return id;
  }

  @Override
  public void write(final Writer writer) throws IOException {
    writer.append(id);
  }

  @Override
  public boolean matchKeyByIdentity(final Term term) {
    return term == this;
  }

  @Override
  public Term singleyKeyMatch() {
    return this;
  }

  @Override
  public boolean matchKeyByEquality(final Term term) {
    assert false : "Invalid Protocol: Terms should only identity-compared.";
    return false;
  }

  @Override
  public <T> T accept(final ModelVisitor<T> modelVisitor) {
    assert modelVisitor != null;
    return modelVisitor.visit(this);
  }

  @Override
  public String getValue() {
    return id;
  }
}
