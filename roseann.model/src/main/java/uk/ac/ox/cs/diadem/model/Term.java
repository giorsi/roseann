/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

import java.io.IOException;
import java.io.Writer;

import uk.ac.ox.cs.diadem.util.collect.KeyMask;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
public interface Term extends KeyMask<Term> {
  public void write(Writer writer) throws IOException;

  public <T> T accept(ModelVisitor<T> modelVisitor);

  /**
   * Returns the value of this term as a string removing quotes in case of a StringLiteral or converting to String in
   * case of a IntegerLiteral.
   */
  public String getValue();
}
