/**
 *
 */
package uk.ac.ox.cs.diadem.model;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * @version $Revision: 1.0 $
 */
public interface IntegerLiteral extends Term {
  /**
   * @return the integer value of this
   */
  int getInteger();

  final public static int MAX_INT = 99999999;

}
