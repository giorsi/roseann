/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * @version $Revision: 1.0 $
 * 
 */
public interface ListTerm extends Term {
  /**
   * @return the Terms in this
   */
  Term[] getSubTerms();

  /**
   * @param index
   *          with 0<=index<getSubTerms().length
   * @return the index-th Term of this
   */
  Term getSubTerm(int index);
}
