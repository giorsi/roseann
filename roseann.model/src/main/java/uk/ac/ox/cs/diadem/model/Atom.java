package uk.ac.ox.cs.diadem.model;

import java.io.IOException;
import java.io.Writer;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * @version $Revision: 1.0 $
 */
public interface Atom {
  /**
   * @return the predicate name of this
   */
  String getId();

  /**
   * @return the Terms of this
   */
  Term[] getSubTerms();

  /**
   * @param index
   *          with 0<=index<getSubTerms().length
   * @return the index-th Term of this
   */
  Term getSubTerm(int index);

  /**
   * Writes the Atom in standard fact notation into the writer, but without tailing dot.
   * 
   * @param writer
   *          to write into
   * @throws IOException
   */
  void write(Writer writer) throws IOException;

  <T> T accept(ModelVisitor<T> modelVisitor);
}
