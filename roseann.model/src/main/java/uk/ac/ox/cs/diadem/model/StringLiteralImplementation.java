// $codepro.audit.disable declareDefaultConstructors
/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

import java.io.IOException;
import java.io.Writer;

import uk.ac.ox.cs.diadem.util.misc.EscapingUtils;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
final class StringLiteralImplementation implements StringLiteral {
  final private String id;

  StringLiteralImplementation(final String id) {
    this.id = unescape(id);
  }

  @Override
  public String getString() {
    return id;
  }

  @Override
  public String toString() {
    return new String("\"") + escape(id) + "\"";
  }

  @Override
  public void write(final Writer writer) throws IOException {
    writer.append('\"');
    writer.append(escape(id));
    writer.append('\"');
  }

  @Override
  public boolean matchKeyByIdentity(final Term term) {
    return term == this;
  }

  @Override
  public Term singleyKeyMatch() {
    return this;
  }

  @Override
  public boolean matchKeyByEquality(final Term term) {
    assert false : "Invalid Protocol: Terms should only identity-compared.";
    return false;
  }

  @Override
  public <T> T accept(final ModelVisitor<T> modelVisitor) {
    assert modelVisitor != null;
    return modelVisitor.visit(this);
  }

  @Override
  public String getValue() {
    return id;
  }

  private static final String escape(final String id) {
    return EscapingUtils.escapeStringContentForDLV(id);
  }

  private static final String unescape(final String id) {
    return EscapingUtils.unescapeDLVStringContent(id);
  }
}
