/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

import java.io.IOException;
import java.io.Writer;

//$codepro.audit.disable declareDefaultConstructors
/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
final public class ModelDiffAnalyzer {
  private final Writer writer;

  public ModelDiffAnalyzer(Writer writer) {
    assert writer != null : "Invalid Argument.";
    this.writer = writer;
  }

  public enum RESULT {
    SUBSET, SUPERSET, EQUAL, INCOMPARABLE;
  };

  public RESULT compare(Model left, Model right) throws IOException {
    assert left.hasSameContext(right) : "Invalid Arguments: left and right must share the same context";
    RESULT result = RESULT.EQUAL;
    for (Atom atom : left.iterator())
      if (!right.contains(atom)) {
        writer.append('-');
        writer.append(atom.toString());
        writer.append('\n');
        if (result == RESULT.EQUAL)
          result = RESULT.SUPERSET;
        else if (result == RESULT.SUBSET)
          result = RESULT.INCOMPARABLE;
      }
    for (Atom atom : right.iterator())
      if (!left.contains(atom)) {
        writer.append('+');
        writer.append(atom.toString());
        writer.append('\n');
        if (result == RESULT.EQUAL)
          result = RESULT.SUBSET;
        else if (result == RESULT.SUPERSET)
          result = RESULT.INCOMPARABLE;
      }
    return result;
  }
}
