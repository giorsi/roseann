// $codepro.audit.disable declareDefaultConstructors
/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

import org.slf4j.Logger;

import uk.ac.ox.cs.diadem.env.DiademRuntimeException;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
public class ModelIOException extends DiademRuntimeException {
  private static final long serialVersionUID = 1L;

  ModelIOException(String message, Logger logger) {
    super(message, logger);
    logger.error(message);
  }

  ModelIOException(String message, Throwable cause, Logger logger) {
    super(message, cause, logger);
    logger.error(message);
  }
}
