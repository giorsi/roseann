// $codepro.audit.disable declareDefaultConstructors
/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
final class AtomImplementation implements Atom {
  final private String id;
  final private Term[] subTerms;

  AtomImplementation(String id, Term[] subTerms) {
    this.id = id;
    this.subTerms = subTerms;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public Term[] getSubTerms() {
    return subTerms;
  }

  @Override
  public String toString() {
    if (subTerms.length == 0)
      return id;
    final StringWriter writer = new StringWriter();
    try {
      this.write(writer);
    } catch (IOException e) {
      assert false : "Violated Invariance.";
      e.printStackTrace();
    }
    return writer.toString();
  }

  @Override
  public void write(Writer writer) throws IOException {
    writer.append(id);
    if (subTerms.length == 0)
      return;
    writer.append('(');
    subTerms[0].write(writer);
    for (int i = 1; i < subTerms.length; ++i) {
      writer.append(',');
      subTerms[i].write(writer);
    }
    writer.append(')');
  }

  @Override
  public Term getSubTerm(final int index) {
    assert (0 <= index) && (index < subTerms.length);
    return subTerms[index];
  }

  @Override
  public <T> T accept(ModelVisitor<T> modelVisitor) {
    return modelVisitor.visit(this);
  }
}