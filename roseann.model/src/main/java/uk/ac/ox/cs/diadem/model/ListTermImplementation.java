/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

//$codepro.audit.disable declareDefaultConstructors
/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
final class ListTermImplementation implements ListTerm {
  private final Term[] subTerms;

  ListTermImplementation(final Term[] subTerms) {
    this.subTerms = subTerms;
  }

  @Override
  public Term[] getSubTerms() {
    return subTerms;
  }

  @Override
  public Term getSubTerm(final int index) {
    assert 0 <= index && index < subTerms.length;
    return subTerms[index];
  }

  @Override
  public String toString() {
    final StringWriter writer = new StringWriter();
    try {
      write(writer);
    } catch (final IOException e) {
      assert false : "Violated Invariance";
      e.printStackTrace();
    }
    return writer.toString();
  }

  @Override
  public void write(final Writer writer) throws IOException {
    writer.append('[');
    if (subTerms.length != 0) {
      subTerms[0].write(writer);
      for (int i = 1; i < subTerms.length; ++i) {
        writer.append(',');
        subTerms[i].write(writer);
      }
    }
    writer.append(']');
  }

  @Override
  public boolean matchKeyByIdentity(final Term term) {
    return term == this;
  }

  @Override
  public Term singleyKeyMatch() {
    return this;
  }

  @Override
  public boolean matchKeyByEquality(final Term term) {
    assert false : "Invalid Protocol: Terms should only identity-compared.";
    return false;
  }

  @Override
  public <T> T accept(final ModelVisitor<T> modelVisitor) {
    assert modelVisitor != null;
    return modelVisitor.visit(this);
  }

  @Override
  public String getValue() {
    return toString();
  }
}
