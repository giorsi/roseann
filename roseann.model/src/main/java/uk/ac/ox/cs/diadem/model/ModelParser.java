// $codepro.audit.disable declareDefaultConstructors
/**
 *
 */
package uk.ac.ox.cs.diadem.model;

import static uk.ac.ox.cs.diadem.model.ModelConfiguration.HIERARCHY_SEPARATOR;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.ParserException;
import uk.ac.ox.cs.diadem.util.misc.StreamTokenizer;
import uk.ac.ox.cs.diadem.util.misc.StreamTokenizer.State;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 */
final class ModelParser {
  private static final Logger logger = LoggerFactory.getLogger(ModelParser.class);
  private Model model;
  private boolean hierarchical;

  private final StreamTokenizer tokenizer = new StreamTokenizer();

  public void init(final String string, final Model model) {
    assert string != null;
    assert model != null;
    this.model = model;
    hierarchical = false;
    tokenizer.init(new StringReader(string));
  }

  public void init(final File file, final Model model) throws FileNotFoundException {
    assert file != null;
    assert model != null;
    this.model = model;
    hierarchical = false;
    tokenizer.init(new FileReader(file));
  }

  public void init(final Reader reader, final Model model) {
    assert reader != null;
    assert model != null;
    this.model = model;
    hierarchical = false;
    tokenizer.init(reader);
  }

  public void init(final Reader reader, final Model model, final boolean hierarchical) {
    assert reader != null;
    assert model != null;
    this.model = model;
    this.hierarchical = hierarchical;
    tokenizer.init(reader);
  }

  public Term parseTerm() throws ParserException, IOException { // $codepro.audit.disable
    // blockDepth
    assert tokenizer != null : "Invalid Protocol";
    switch (tokenizer.nextToken()) {
    case LONG:
      return model.getIntegerLiteral((int) tokenizer.getLong());
    case WORD:
      final String id = tokenizer.getWord();
      if ((tokenizer.nextToken() == State.CHAR) && (tokenizer.getChar() == '(')) {
        final List<Term> terms = new ArrayList<Term>();
        while (true) {
          final Term term = parseTerm();
          if (term != null) {
            terms.add(term);
            if ((tokenizer.nextToken() == State.CHAR) && (tokenizer.getChar() == ',')) {
              continue;
            } else {
              tokenizer.pushBack();
            }
          }
          if ((tokenizer.nextToken() == State.CHAR) && (tokenizer.getChar() == ')')) {
            if (terms.isEmpty()) {
              throw new ParserException("Terms of arity zero must be written without brackets.", logger);
            }
            return model.getCompoundTerm(id, terms.toArray(new Term[0]));
          } else {
            throw new ParserException("Expected <Term>, ')', or ','. Got '" + tokenizer.getState() + "'", logger);
          }
        }
      } else {
        tokenizer.pushBack();
        return model.getConstantTerm(id);
      }
    case STRING:
      return model.getStringLiteral(tokenizer.getString());
    case CHAR:
      if (tokenizer.getChar() == '[') {
        final List<Term> terms = new ArrayList<Term>();
        while (true) {
          final Term term = parseTerm();
          if (term != null) {
            terms.add(term);
            if ((tokenizer.nextToken() == State.CHAR) && (tokenizer.getChar() == ',')) {
              continue;
            } else {
              tokenizer.pushBack();
            }
          }
          if ((tokenizer.nextToken() == State.CHAR) && (tokenizer.getChar() == ']')) {
            return model.getListTerm(terms.toArray(new Term[0]));
          } else {
            throw new ParserException("Expected <Term>, ']', or ','. Got " + tokenizer.getContentAsString(), logger);
          }
        }
      }
      tokenizer.pushBack();
      return null;
    default:
      tokenizer.pushBack();
      return null;
    }
  }

  public Atom parseAtom() throws ParserException, IOException { // $codepro.audit.disable
    // blockDepth
    if (tokenizer.nextToken() != State.WORD) {
      final String token = tokenizer.getTokenAsString();
      tokenizer.pushBack();
      throw new ParserException("Expected Id for an atom, but got '" + token + "'.", logger);
    }
    final String id = tokenizer.getWord();
    final List<Term> terms = new ArrayList<Term>();

    if ((tokenizer.nextToken() == State.CHAR) && (tokenizer.getChar() == '(')) {
      terms.clear();
      while (true) {
        final Term term = parseTerm();
        if (term != null) {
          terms.add(term);
          if ((tokenizer.nextToken() == State.CHAR) && (tokenizer.getChar() == ',')) {
            continue;
          } else {
            tokenizer.pushBack();
          }
        }
        if ((tokenizer.nextToken() == State.CHAR) && (tokenizer.getChar() == ')')) {
          if (terms.isEmpty()) {
            throw new ParserException("Terms of arity zero must be written without brackets.", logger);
          }

          final Term[] termArray = terms.toArray(new Term[terms.size()]);
          if (!hierarchical) {
            return model.add(id, termArray);
          }

          final String[] predicateNames = id.split(HIERARCHY_SEPARATOR);
          // final int[] arities =
          // modelSchema.getArities(predicateNames);

          final int[] arities = new int[predicateNames.length];
          final int predicates1 = predicateNames.length - 1;
          int prefixArity = 0;
          for (int i = 0; i < predicates1; ++i) {
            final String predicateName = predicateNames[i];
            int lastNonNumberChar = predicateName.length() - 1;
            for (; ('0' <= predicateName.charAt(lastNonNumberChar)) && (predicateName.charAt(lastNonNumberChar) <= '9'); --lastNonNumberChar) {
              ;
            }
            assert lastNonNumberChar >= 0;
            assert lastNonNumberChar < (predicateName.length() - 1) : "Hierarchical predicate names must encode their arity. ["
            + predicateName + "] does not.";
            arities[i] = Integer.parseInt(predicateName.substring(lastNonNumberChar + 1, predicateName.length()));
            prefixArity += arities[i];
            predicateNames[i] = predicateName.substring(0, lastNonNumberChar + 1);
          }

          assert prefixArity <= termArray.length;
          arities[predicates1] = termArray.length - prefixArity;

          Model curModel = model;
          final int len1 = arities.length - 1;
          int start = 0;
          for (int i = 0; i < len1; ++i) {
            final int end = start + arities[i];
            final Atom atom = curModel.add(predicateNames[i], Arrays.copyOfRange(termArray, start, end));
            start = end;
            curModel = curModel.getSubmodel(atom);
          }
          final int end = start + arities[len1];
          return curModel.add(predicateNames[len1], Arrays.copyOfRange(termArray, start, end));

        } else if (tokenizer.getState() == State.WORD) {
          throw new ParserException("Expected <Term>, ')', or ','. Got " + tokenizer.getContentAsString(), logger);
        } else {
          throw new ParserException("Expected <Term>, ')', or ','. Got " + tokenizer.getContentAsString(), logger);
        }
      }
    }
    // For facts within a 0 arity namespace (or no namespace at all).
    tokenizer.pushBack();
    if (!hierarchical) {
      return model.add(id);
    }

    final String[] predicateNames = id.split(HIERARCHY_SEPARATOR);
    // final int[] arities =
    // modelSchema.getArities(predicateNames);

    final int[] arities = new int[predicateNames.length];
    final int predicates1 = predicateNames.length - 1;
    for (int i = 0; i < predicates1; ++i) {
      final String predicateName = predicateNames[i];
      int lastNonNumberChar = predicateName.length() - 1;
      for (; ('0' <= predicateName.charAt(lastNonNumberChar)) && (predicateName.charAt(lastNonNumberChar) <= '9'); --lastNonNumberChar) {
        ;
      }
      assert lastNonNumberChar >= 0;
      assert lastNonNumberChar < (predicateName.length() - 1) : "Hierarchical predicate names must encode their arity. ["
      + predicateName + "] does not.";
      arities[i] = Integer.parseInt(predicateName.substring(lastNonNumberChar + 1, predicateName.length()));
      predicateNames[i] = predicateName.substring(0, lastNonNumberChar + 1);
    }
    for (final int a : arities) {
      if (a != 0) {
        throw new ParserException("Non zero namespace arity with 0 arity predicate: " + tokenizer.getContentAsString(),
            logger);
      }
    }

    Model curModel = model;
    final int len1 = arities.length - 1;
    for (int i = 0; i < len1; ++i) {
      final Atom atom = curModel.add(predicateNames[i]);
      curModel = curModel.getSubmodel(atom);
    }
    return curModel.add(predicateNames[len1]);
  }

  public Atom parseAtomWithDot() throws ParserException, IOException {
    final Atom atom = parseAtom();
    if ((tokenizer.nextToken() == State.CHAR) && (tokenizer.getChar() == '.')) {
      return atom;
    }
    throw new ParserException("Expected '.' after atom '" + atom.toString() + "'", logger);
  }

  public void parseAtoms() throws ParserException, IOException {
    while (tokenizer.nextToken() != State.EOF) {
      tokenizer.pushBack();
      parseAtomWithDot();
    }
  }

  public boolean isEmpty() throws ParserException, IOException {
    final boolean result = tokenizer.nextToken() != State.EOF;
    tokenizer.pushBack();
    return result;
  }
}
