package uk.ac.ox.cs.diadem.model;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModelSchema {
  final Map<String, Integer> arityMap = new HashMap<String, Integer>();
  final Map<String, ModelSchema> subSchemaMap = new HashMap<String, ModelSchema>();

  final static private Logger logger = LoggerFactory.getLogger(ModelSchema.class);

  /**
   * Get the SubmodelSchema for predicateName.
   * 
   * @param predicateName
   * @return the SubmodelSchema for predicateName, or null if its does not exist.
   */
  public ModelSchema getSubmodelSchema(String predicateName) {
    assert predicateName != null;
    return subSchemaMap.get(predicateName);
  }

  /**
   * Get the arity of a predicate (we allow only a single arity per predicate).
   * 
   * @param predicateName
   * @return the arity of predicateName, or -1 if the no pedicate with predicateName exists.
   */
  public int getArity(String predicateName) {
    assert predicateName != null;
    final Integer arity = arityMap.get(predicateName);
    if (arity == null)
      return -1;
    return arity;
  }

  public void addPredicate(String predicateName, int arity) throws ModelSchemaException {
    assert predicateName != null;
    assert arity >= 0;

    final int oldArity = getArity(predicateName);
    if (oldArity != -1) {
      if (oldArity == arity)
        logger.debug("Predicate '{}' already registed with arity {}.", predicateName, arity);
      else
        throw new ModelSchemaException("Attempt to overwrite arity " + oldArity + " for predicate '" + predicateName
            + "' with arity " + arity + ".", logger);
    } else {
      arityMap.put(predicateName, arity);
      logger.debug("Predicate '{}' added with arity {}.", predicateName, arity);
    }
  }

  public ModelSchema addSubmodelSchema(String predicateName) {
    assert predicateName != null;
    ModelSchema result = subSchemaMap.get(predicateName);
    if (result != null)
      return result;
    subSchemaMap.put(predicateName, result = new ModelSchema());
    return result;
  }

  private void write(Writer writer, String prefix) throws IOException {
    final SortedSet<String> predicateNames = new TreeSet<String>(arityMap.keySet());
    for (final String predicateName : predicateNames) {
      writer.write(prefix);
      writer.write("Predicate '");
      writer.write(predicateName);
      writer.write("/");
      writer.write(arityMap.get(predicateName).toString());
      writer.write("\n");
      final ModelSchema submodelSchema = getSubmodelSchema(predicateName);
      if (submodelSchema != null)
        submodelSchema.write(writer, prefix + "  ");
    }
  }

  public void write(Writer writer) throws IOException {
    write(writer, "");
  }

  @Override
  public String toString() {
    final StringWriter writer = new StringWriter();
    try {
      write(writer);
    } catch (IOException e) {
      assert false : "No RAM or what?";
      e.printStackTrace();
    }
    return writer.toString();
  }

  public int[] getArities(String[] predicateNames) throws ModelSchemaException {
    final int[] result = new int[predicateNames.length];
    ModelSchema modelSchema = this;
    final int len1 = predicateNames.length - 1;
    for (int i = 0; i < len1; ++i) {
      result[i] = modelSchema.getArity(predicateNames[i]);
      modelSchema = modelSchema.getSubmodelSchema(predicateNames[i]);
      if (modelSchema == null)
        throw new ModelSchemaException("No submodel for '" + predicateNames[i] + "' in '" + predicateNames + "'.",
            logger);
    }
    result[len1] = modelSchema.getArity(predicateNames[len1]);
    if (result[len1] == -1)
      throw new ModelSchemaException("No predicate '" + predicateNames[len1] + "' in '" + predicateNames
          + "' known in schema.", logger);
    return result;
  }
}
