// $codepro.audit.disable overloadedMethods, booleanMethodNamingConvention
/**
 *
 */
package uk.ac.ox.cs.diadem.model; // $codepro.audit.disable packagePrefixNamingConvention

import java.io.File;
import java.io.Reader;
import java.io.Writer;
import java.util.Iterator;
import java.util.Set;

import uk.ac.ox.cs.diadem.env.ParserException;
import uk.ac.ox.cs.diadem.util.collect.IterableIterator;
import uk.ac.ox.cs.diadem.util.collect.KeyMask;

/**
 * <p>
 * SUMMARY: Model is the interface for manipulating logical models kept in memory. Conceptually a Model maintains a set
 * of {@link Atom Atoms} and allows to add, remove, and access them. Access methods provide containment checks and
 * (selective) iterations. In support of this core functionality, a Model allows to build {@link Term Terms} to
 * construct Atoms. Terms may be built from constants, integers, strings, lists, compound terms.
 * </p>
 * <p>
 * QUERYING: We specify subset of the Atoms contained in the Model typically with a predicate name and a number of
 * {@link KeyMask KeyMasks}. A KeyMask is either
 * <ul>
 * <li>a Term -- requiring precisely this Term,</li>
 * <li>null -- meaning "don't care", or</li>
 * <li>a {@link uk.ac.ox.cs.diadem.util.NegatedKeyMask NegatedKeyMask} -- matching all Terms unequal the one specified
 * in the NegatedKeyMask, or</li>
 * <li>any other future implementation of KeyMask.</li>
 * </ul>
 * For example, {@link #iteratorForMask(String, KeyMask...)} ("childOf",null,adam) returns an iterator over all
 * childOf-Atoms having adam as father. Mask matching methods come in two flavours: In the first flavour, as here, the
 * ARITY must match, i.e., the above iterator does not contain e.g. childOf(seth,adam,thirdBorn). Methods of the second
 * flavour match only the first Terms of an Atoms, i.e., the ignore the ARITY and the tailing Terms. For exmaple,
 * {@link #iteratorForMask(String, KeyMask...)}("childOf",cain) does not match childOf(cain,adam) but
 * {@link #iteratorForPrefixMask(String, KeyMask...)} ("childOf",cain) does.
 * </p>
 * <p>
 * EQUALITY IMPLIES IDENITY: For efficiency reasons, Terms and Atoms are maintained in pools such that equality implies
 * identity, i.e., atom1.equals(atom2) is equivalent to atom1==atom2. In consequence, each Term and Atom must be
 * registered in a central pool to check on future construction of Terms and Atoms whether already fitting instances
 * exist. Thus, Model provides methods such as {@link #getIntegerLiteral(long)} to construct suitable Terms and Atoms in
 * a suitable way.
 * </p>
 * <p>
 * CONTEXT: Atoms and Terms are shared between different models, i.e., if you create a Term or Atom with one Model, this
 * Term or Atom is also usable for another Model -- IF these Models share the same CONTEXT (checkable with
 * {@link #hasSameContext(Model)}). Otherwise, if they do not share the same context, the behaviour is undefined, i.e.,
 * a Term or Atom from another context may work or not -- in particular, it might behave incorrectly WITHOUT REPORTING
 * AN ERROR.
 * </p>
 * <p>
 * CONTEXT MANAGEMENT: If you create a Model with {@link ModelFactory#createModel()}, then this Model shares its context
 * with a GLOBAL CONTEXT -- hence all Models created this way share their Atoms and Terms. However, there are a couple
 * of ways to obtain Models with different contexts:
 * <ul>
 * <li>We can move a Model into a newly created context with {@link #isolate()}. Then this Model does not share its
 * Atoms and Terms with other Models, making it faster on some operations. You can check for isolation with
 * {@link #isIsolated()}.</li>
 * <li>We can create a Model which is created already in isolation with {@link ModelFactory#createIsolatedModel()}.</li>
 * <li>We can create a Model with {@link #create(boolean preserveContext, boolean preserveAtoms)} -- if the first flag
 * is false, the new Model is isolated as well, otherwise it shares the context with Model we create the new one from.</li>
 * </ul>
 * Thus, in most cases you should never pull a Model out of the global context. It is sensible for two reasons: First,
 * when you create a lot of Atoms and Terms in Model, and you never need to compare the outcome with Terms or Atoms of
 * other Models, and second, when you parallelize your stuff and you want to have a context for each thread: CONCURRENT
 * ACCESS to Model sharing the same context is unsupported.
 * </p>
 * <p>
 * GENERAL REMARKS:
 * <ul>
 * <li>null pointers are only allowed where explicitly documented. Otherwise, null pointer yield -- with assertions
 * switched on -- RuntimeExceptions.</li>
 * <li>A model is not usable, once it has been released via {@link #release}. After releasing, practically all methods
 * deliver undefined behaviour (with assertion switched on, they throw).</li>
 * </ul>
 * </p>
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * @version $Revision: 1.0 $ TODO Must atom with a submodel be part of the model? Do we delete the submodel with the
 *          atom?
 */
public interface Model extends Iterable<Atom> {
  /**
   * @return true iff this does not share its context with any other model.
   */
  boolean isIsolated();

  /**
   * moves this into its own, isolated context.
   */
  void isolate();

  /**
   * @param other
   * @return true iff this and other share the same context.
   */
  boolean hasSameContext(Model other);

  /**
   * Creates a new Model, optionally sharing the context with this, and containing all Atoms which match the predicate
   * and masks.
   * @param preserveContext if true, the newly created Model shares the context with this.
   * @param predicate the name of the predicate to copy Atoms from
   * @param masks the masks for the terms to select the Atoms to copy
   * @return a newly created Model, according to the above specification.
   */
  Model create(boolean preserveContext, String predicate, KeyMask<Term>... masks);

  /**
   * Creates a new Model, optionally sharing the context with this, and optionally containing the same Atoms as this, or
   * being empty otherwise.
   * @param preserveContext if true, the newly created Model shares the context with this.
   * @param preserveAtoms if true, the newly created Model contains the same atoms as this and is empty otherwise.
   * @return a newly created Model, according to the above specification.
   */
  Model create(boolean preserveContext, boolean preserveAtoms);

  /**
   * Returns the IntegerLiteral for a given integer number in the context of this. Calling this method (in this or
   * another Model in the same context) a second time with the same integer value yields the SAME Term object.
   * @param integer
   * @return the Term object for a given integer number in the context of this.
   */
  IntegerLiteral getIntegerLiteral(int integer);

  /**
   * Returns the StringLiteral for a given string in the context of this. Calling this method (in this or another Model
   * in the same context) a second time with the same string yields the SAME Term object.
   * @param string
   * @return the Term object for a given string in the context of this.
   */
  StringLiteral getStringLiteral(String string);

  /**
   * Returns the ConstantTerm for a given (logical) constant in the context of this. Calling this method (in this or
   * another Model in the same context) a second time with the same constant yields the SAME Term object.
   * @param constantName
   * @return the Term object for a given constant in the context of this.
   */
  ConstantTerm getConstantTerm(String constantName);

  /**
   * Returns the CompoundTerm for a given function and a list of terms in the context of this. Calling this method (in
   * this or another Model in the same context) a second time with the same functions and terms yields the SAME Term
   * object.
   * @param functionName the function symbol, e.g., "childOf"
   * @param terms the terms to form the compound term, e.g., this.createConstantTerm
   *          ("cain"),this.createConstantTerm("adam"). Note that terms.length>0 must hold -- otherwise use
   *          {@link #getConstantTerm(String)}.
   * @return the Term object for given function and terms in the context of this.
   */
  CompoundTerm getCompoundTerm(String functionName, Term... terms);

  /**
   * Returns the ListTerm for a given list of terms in the context of this. Calling this method (in this or another
   * Model in the same context) a second time with the same terms yields the SAME Term object.
   * @param terms
   * @return the Term object for given terms in the context of this.
   */
  ListTerm getListTerm(Term... terms);

  /**
   * Returns the Term obtained from parsing termString in the context of this. Calling this method (in this or another
   * Model in the same context) a second time with the same terms yields the SAME Term object.
   * @param termString the string containing a term
   * @return the Term resulting from parsing or null if no such term could be parsed.
   * @throws ParserException if termString is not properly formated TODO to be renamed into getTermByParsing
   */
  Term parseTerm(String termString) throws ParserException;

  /**
   * Returns the Term obtained from copying the given term, where term can originate from an arbitrary context.
   * @param term the term to be obtained
   * @return the input term (if term is in the context as this), or the term constructed in the context of this.
   */
  Term getTermFromOtherContext(Term term);

  /**
   * Returns the Atom matching predicate and terms -- if contained in this, and null otherwise.
   * @param predicate the predicate of the Atom
   * @param terms the terms of the Atom
   * @return the matching Atom if contained in this, and null otherwise
   */
  Atom get(String predicate, Term... terms);

  /**
   * Adds the Atom specified by predicate and terms to this. If the Atom is already contained in this, nothing happens.
   * If the Atom already exists in the context of this (but is not contained in this), this Atom is reused.
   * @param predicateName the name of the predicate for the Atom to add
   * @param terms the terms of the Atom
   * @return the Atom added to or already contained in this.
   */
  Atom add(String predicateName, Term... terms);

  /**
   * Adds atom to this (if it is already contained, nothing happens). CAREFUL: atom must be in the context of this,
   * otherwise the behaviour is undefined.
   * @param atom the atom to be added, which must be part of the context of this.
   * @return returns atom for convenience.
   */
  Atom add(Atom atom);

  /**
   * Adds all atoms to this (already contained ones are ignores). CAREFUL: all atoms must be in the context of this.
   * @param atoms
   */
  void add(Iterable<Atom> atoms);

  /**
   * Parses atomString and add the resulting Atom to this, also returning this Atom. If this Atom is already contained,
   * nothing happens.
   * @param atomString the string to be parsed as Atom (no tailing dot required). If the string continues after the
   *          Atom, then this tail is SILENTLY IGNORED.
   * @return the added Atom.
   * @throws ParserException if atomString does not start with a proper Atom.
   */
  Atom addByParsing(String atomString) throws ParserException;

  /**
   * TODO fix docu and method Parses atomString and add the resulting Atom to this, also returning this Atom. If this
   * Atom is already contained, nothing happens.
   * @param atomString the string to be parsed as Atom (no tailing dot required). If the string continues after the
   *          Atom, then this tail is SILENTLY IGNORED.
   * @return the added Atom.
   * @throws ParserException if atomString does not start with a proper Atom.
   */
  Atom addByParsingHierarchically(String atomString) throws ParserException;

  /**
   * Adds atom to this, accepting atoms from arbitrary contexts.
   * @param atom the atom to be added
   * @return returns the added atom (if the added added atom originates from the same context as this, it is the
   *         unchanged atom, otherwise it is the newly created atom).
   */
  Atom addFromOtherContext(Atom atom);

  /**
   * Adds atoms from to this, accepting atoms from arbitrary contexts.
   * @param atoms the atoms to be added
   */
  void addFromOtherContext(Iterable<Atom> atoms);

  /**
   * Adds atoms from a hierarchical model hierarchically into this
   */
  void addFromOtherContextHierarchically(Model model);

  /**
   * Removes the Atom identified with predicate and terms. If this Atom is not part of this, nothing happens.
   * @param predicateName the name of the predicate to be removed
   * @param terms the terms of the predicate to be removed
   */
  void remove(String predicateName, Term... terms);

  /**
   * Removes atom from this. If atom is not part of this, nothing happens. If atom is not part of the context of this,
   * the behaviour is UNDEFINED.
   * @param atom the Atom to be removed -- it must be part of the context of this.
   */
  void remove(Atom atom);

  /**
   * @param id the name of the predicate of the atoms to be removed
   * @param terms the term mask of the atoms to be removed
   */
  public void removeByMask(final String id, final KeyMask<Term>... terms);

  /**
   * @param id the name of the predicate of the atoms to be removed
   * @param terms the term mask of the atoms to be removed
   */
  public void removeByPrefixMask(final String id, final KeyMask<Term>... terms);

  /**
   * Returns true iff this contains the Atom with name predicate and Terms terms.
   * @param predicateName the predicate name of the Atom to check for
   * @param terms the terms of the Atom to check for
   * @return true iff the Atom defined by predicate and terms is contained in this.
   */
  boolean contains(String predicateName, Term... terms);

  /**
   * Returns true iff this contains atom. CAREFUL: If atom is not in the context of this, the behaviour is undefined.
   * @param atom the atom to check whether it is contained in this -- it must be in the context of this.
   * @return true if atom is contained in this.
   */
  boolean contains(Atom atom);

  /**
   * Returns true iff this contains at least one atom of with predicate name predicate. NOTE: the arity is ignored in
   * this check.
   * @param predicateName the predicate name to check for
   * @return true iff this contains at least one Atom with predicate name predicate.
   */
  boolean containsPredicate(String predicateName);

  /**
   * Returns true iff this contains at least one Atom having predicate name predicate and terms matching the termMasks.
   * @param predicateName the predicate name for the Atoms to check for
   * @param termMasks the KeyMask to be matched by the Atoms -- {@see Model}.
   * @return true iff this contains at least one predicate matching predicate and termMasks.
   */
  boolean containsMask(String predicateName, KeyMask<Term>... termMasks);

  /**
   * Returns true iff this contains at least one Atom having predicate name predicate and terms matching the
   * termPrefixMasks. The arity is IGNORED, i.e., only the first termPrefixMasks.length terms are checked.
   * @param predicateName the predicate name for the Atoms to check for
   * @param termPrefixMasks the KeyMask to be matched by the starting Terms of Atoms -- {@see Model}.
   * @return true iff this contains at least one predicate matching predicate and termMasks.
   */
  boolean containsPrefixMask(String predicateName, KeyMask<Term>... termPrefixMasks);

  /**
   * Removes all Atoms from this -- {@link isEmpty()} is true afterwards.
   */
  void clear();

  /**
   * Removes all Atoms with predicate name predicateName, ignoring arity.
   * @param predicateName the name of the predicate of the Atoms to remove
   */
  void clearPredicate(String predicateName);

  /**
   * Returns the number of Atoms in this.
   * @return the number of Atoms in this.
   */
  int size();

  /**
   * Returns the number of Atoms in this and its submodels.
   * @return the number of Atoms in this and its submodels.
   */
  int sizeHierarchically();

  /**
   * Returns the number of Atoms with predicate name predicateName, ignoring airty.
   * @param predicateName the name of the predicate of the Atoms to be counted.
   * @return returns the number of Atoms with predicate name predicateName.
   */
  int sizeOfPredicate(String predicateName);

  /**
   * Returns an {@link uk.ac.ox.cs.util.collect.IterableIterator} over all Atoms contained in this. An IterableIterator
   * is an Iterator which is also an Iterable. As Iterable, if asked for an iterator, it returns itself again. Hence the
   * iterator can be used in enhanced for loops as well.
   * @return an IterableIterator over all Atoms contained in this.
   * @see java.lang.Iterable#iterator()
   */
  @Override
  IterableIterator<Atom> iterator();

  /**
   * Returns an {@link uk.ac.ox.cs.util.collect.IterableIterator} over all Atoms with predicate name predicateName
   * contained in this. An IterableIterator is an Iterator which is also an Iterable. As Iterable, if asked for an
   * iterator, it returns itself again. Hence the iterator can be used in enhanced for loops as well.
   * @param predicateName the name of the predicate of the Atoms to iterate over
   * @return an IterableIterator over all Atoms with predicate name predicateName
   */
  IterableIterator<Atom> iteratorForPredicate(String predicateName);

  /**
   * Returns an {@link uk.ac.ox.cs.util.collect.IterableIterator} over all Atoms with predicate name predicateName and
   * matching termMasks contained in this. An IterableIterator is an Iterator which is also an Iterable. As Iterable, if
   * asked for an iterator, it returns itself again. Hence the iterator can be used in enhanced for loops as well.
   * @param predicateName the name of the predicate of the Atoms to iterate over
   * @param termMasks the KeyMasks to be matched by the Atoms to iterate over
   * @return an IterableIterator over all Atoms with predicate name predicateName and matching termMasks
   */
  IterableIterator<Atom> iteratorForMask(String predicateName, KeyMask<Term>... termMasks);

  /**
   * Returns the set of Atoms with predicate name predicateName and matching termMasks contained in this.
   * @param predicateName the name of the predicate of the Atoms to return
   * @param termMasks the termMasks to be matched by the Atoms to return
   * @return the set of Atoms matching predicateName and termMasks
   */
  Set<Atom> getAtomsByMask(String predicateName, KeyMask<Term>... termMasks);

  /**
   * Returns the unique Atoms with predicate name predicateName and matching termMasks contained in this.
   * @param predicateName the name of the predicate of the Atoms to return
   * @param termMasks the termMasks to be matched by the Atoms to return
   * @throws ModelAccessException if no or more than one matching predicate is found.
   * @return the set of Atoms matching predicateName and termMasks
   */
  Atom getUniqueAtomByMask(String predicateName, KeyMask<Term>... termMasks) throws ModelAccessException;

  /**
   * Returns an {@link uk.ac.ox.cs.util.collect.IterableIterator} over all Atoms with predicate name predicateName and
   * matching termPrefixMasks contained in this. The arity is IGNORED, i.e., only the termPrefixMasks.length first terms
   * are matched, the remaining noes are arbitrary. An IterableIterator is an Iterator which is also an Iterable. As
   * Iterable, if asked for an iterator, it returns itself again. Hence the iterator can be used in enhanced for loops
   * as well.
   * @param predicateName the name of the predicate of the Atoms to iterate over
   * @param termPrefixMasks the termPrefixMasks to be matched by the Atoms to iterate over
   * @return an IterableIterator over all Atoms with predicate name predicateName and matching termMasks
   */
  IterableIterator<Atom> iteratorForPrefixMask(String predicateName, KeyMask<Term>... termPrefixMasks);

  /**
   * Returns the set of Atoms with predicate name predicateName and matching termPrefixMasks contained in this. The
   * arity is IGNORED, i.e., only the termPrefixMasks.length first terms are matched, the remaining onces are arbitrary.
   * @param predicateName the name of the predicate of the Atoms to return
   * @param termPrefixMasks the termPrefixMasks to be matched by the Atoms to return
   * @return the set of Atoms matching predicateName and termMasks
   */
  Set<Atom> getAtomsByPrefixMask(String predicateName, KeyMask<Term>... termPrefixMasks);

  /**
   * Returns the unique Atoms with predicate name predicateName and matching termPrefixMasks contained in this. The
   * arity is IGNORED, i.e., only the termPrefixMasks.length first terms are matched, the remaining ones are arbitrary.
   * @param predicateName the name of the predicate of the Atoms to return
   * @param termPrefixMasks the termPrefixMasks to be matched by the Atoms to return
   * @throws ModelAccessException if no or more than one Atom is selected.
   * @return the set of Atoms matching predicateName and termMasks
   */
  Atom getUniqueAtomByPrefixMask(String predicateName, KeyMask<Term>... termPrefixMasks) throws ModelAccessException;

  /**
   * Returns the set of Terms which occur at termPosition in an Atom of this which has predicate name predicateName and
   * matches termMasks. E.g., if an Atom childOf(cain, adam) is selected, then termPostion set to 0 selects cain.
   * @param termPosition the position of the searched for Term in an Atom, with 0<= termPosition < termMasks.length.
   * @param predicateName the name of the predicate of the Atoms to be selected.
   * @param termMasks the termMasks to be matched by the Atoms to be selected.
   * @return the set of Terms occurring at termPosition within the selected Atoms.
   */
  Set<Term> getTermsByMask(int termPosition, String predicateName, KeyMask<Term>... termMasks);

  /**
   * Returns the set of Terms which occur at termPosition in an Atom of this which has predicate name predicateName and
   * matches termPrefixMasks. E.g., if an Atom childOf(cain, adam) is selected, then termPostion set to 0 selects cain.
   * @param termPosition the position of the searched for Term in an Atom, with 0<= termPosition < termMasks.length.
   * @param predicateName the name of the predicate of the Atoms to be selected.
   * @param termPrefixMasks the termPrefixMasks to be matched by the Atoms to be selected (only the first
   *          termPrefixMasks.length Terms are matched).
   * @return the set of Terms occurring at termPosition within the selected Atoms.
   */
  Set<Term> getTermsByPrefixMask(int termPosition, String predicateName, KeyMask<Term>... termPrefixMasks);

  /**
   * Returns the Term which occurs termPosition in the UNIQUE Atom with predicate name predicateName, matching
   * termMasks. CAUTION: If more than one Atom are matched, having the same term at termPosition, this method will still
   * throw. E.g., if an Atom childOf(cain, adam) is selected, then termPostion set to 0 selects cain.
   * @param termPosition the position of the searched for Term in an Atom, with 0<= termPosition < termMasks.length.
   * @param predicateName the name of the predicate of the Atoms to be selected.
   * @param termMasks the termMasks to be matched by the Atoms to be selected.
   * @throws ModelAccessException if no or more than one Term is selected by termMasks
   * @return the set of Terms occurring at termPosition within the selected Atoms.
   */
  Term getUniqueTermByMask(int termPosition, String predicateName, KeyMask<Term>... termMasks)
      throws ModelAccessException;

  /**
   * Returns the Term which occurs termPosition in the UNIQUE Atom with predicate name predicateName, matching
   * termPrefixMasks. CAUTION: If more than one Atom are matched, having the same term at termPosition, this method will
   * still throw. E.g., if an Atom childOf(cain, adam) is selected, then termPostion set to 0 selects cain.
   * @param termPosition the position of the searched for Term in an Atom, with 0<= termPosition < termMasks.length.
   * @param predicateName the name of the predicate of the Atoms to be selected.
   * @param termPrefixMasks the termPrefixMasks to be matched by the Atoms to be selected (only the first
   *          termPrefixMasks.length Terms are matched).
   * @throws ModelAccessException if no or more than one Term is selected by termPrefixMasks
   * @throws ModelAccessException if termPosition is larger than the found atom's arity.
   * @return the set of Terms occurring at termPosition within the selected Atoms.
   */
  Term getUniqueTermByPrefixMask(int termPosition, String predicateName, KeyMask<Term>... termPrefixMasks)
      throws ModelAccessException;

  /**
   * Returns the set of Atoms which have predicate name predicateName, ignoring the arity.
   * @param predicateName the name of the predicate of the Atoms to select.
   * @return the set of Atoms having the predicate name predicateName of arbitrary arity.
   */
  Set<Atom> getAtomsByPredicate(String predicateName);

  /**
   * Returns a string with some statistics on the contents of this, such as the number of Atoms in this. The precise
   * contents are implementation specific.
   * @return some statistics on this.
   */
  String getStatistics();

  /**
   * Equality: two models are equal, iff (i) they live in the same context, and (ii) contain the same set of atoms.
   * @param other the other model to compare with
   * @return true if the models are equal TODO buggy -- does not check for hierarchical equality, only checks for local
   *         equality
   */
  @Override
  boolean equals(Object other); // $codepro.audit.disable checkTypeInEquals

  /**
   * Releases the contents of this -- note that this is unusable afterwards.
   */
  void release();

  /**
   * Writes the contents of this as a sequence of '.'-terminated Atoms into writer.
   * @param writer to write the Atoms contained in this into.
   * @throws ModelIOException if the writer fails (disk full, cannot create file etc.)
   */
  void write(Writer writer);

  /**
   * Writes the contents of this as a sequence of '.'-terminated Atoms into writer.
   * @param writer to write the Atoms contained in this into.
   * @param prefix a prefix to add to the atoms.
   * @throws ModelIOException if the writer fails (disk full, cannot create file etc.)
   */
  void write(Writer writer, String prefix);

  /**
   * Writes the contents of this as a sequence of '.'-terminated Atoms into file
   * @param file the File to write to
   * @throws ModelIOException if writing fails (cannot create File etc.)
   */
  void write(File file);

  /**
   * Reads a sequence of '.'-terminated Atoms from reader and adds them to this.
   * @param reader the reader to read from
   * @throws ModelIOException if reading fails or a parse error occurs.
   */
  void read(Reader... reader);

  /**
   * Reads a sequence of '.'-terminated Atoms from file and adds them to this.
   * @param file the file to read from
   * @throws ModelIOException if reading fails or a parse error occurs.
   */
  void read(File... file);

  /**
   * Reads a sequence of '.'-terminated Atoms from string and adds them to this.
   * @param string the string to read from
   * @throws ModelIOException if reading fails or a parse error occurs.
   */
  void read(String... string);

  /**
   * Returns the Sub-Model associated with atom. If no such model exists, it is created.
   * @param atom TODO probably, we want to change this in requiring that atom is part of the model. This raises the
   *          question whether we should delete the submodel upon deleting the atom in the model.
   * @return the Sub-Model associated with atom.
   */
  Model getSubmodel(Atom atom);

  /**
   * Returns the Sub-Model associated with an fact with the given atomAsString. If no such model exists, it is created.
   */
  Model getSubmodelByPredicateName(String atomAsString);

  /**
   * Returns the Sub-Model associated with atom. If no such model exists, it is created. Also, if atom is element of
   * this, the atom is added.
   * @param atom
   * @return the Sub-Model associated with atom.
   * @throws ParserException
   */
  Model getSubmodel(String atom) throws ParserException;

  /**
   * Returns an iterator over all atoms which have an associated submodel.
   * @return an iterator over all atoms which have an associated submodel.
   */
  Iterator<Atom> getSubmodelAtoms();

  /**
   * Returns the parent model or null if this model is root.
   * @return the parent model or null if this model is root.
   */
  Model getParentModel();

  /**
   * Returns the root model of this model (in case itself).
   * @return the root model of this model.
   */
  Model getRootModel();

  /**
   * Returns a string with all directly and indirectly contained atoms. Equivalent to calling writeHierarchically with a
   * StringWriter.
   * @return a string with all directly and indirectly contained atoms.
   */
  String toStringHierarchically();

  /**
   * Writes into writer all directly and indirectly contained atoms.
   * @param writer
   */
  void writeHierarchically(Writer writer);

  /**
   * Writes into file all directly and indirectly contained atoms. Equivalent to calling writeHierarchically with a
   * FileWriter.
   * @param file
   */
  void writeHierarchically(File file);

  /**
   * Reads from reader a set of atoms hierarchically, i.e., the facts are broken up to obtain the atoms determining the
   * submodel to place the corresponding fact. TODO more explanation on how the facts are broken up.
   * @param reader
   */
  void readHierarchically(Reader... reader);

  /**
   * Reads from file a set of atoms hierarchically. Equivalent to calling readHierarchically with a FileReader.
   * @param file
   */
  void readHierarchically(File... file);

  /**
   * Returns a ModelSchema for this, describing the submodel structure and the predicates contained therein. Careful:
   * The ModelSchema only allows a single arity for each predicate, however, on creating the ModelSchema, we DO NOT
   * CHECK for that. TODO add a method for checking the consistency of a Model to ModelSchema.
   * @return a ModelSchema for this.
   */
  ModelSchema getModelSchema();

  Model getSubmodel(String predicateName, String... params);

  Model getSubmodel(String predicateName, Term... params);

  /**
   * Adds an atom with the given predicate name and parameters to the model. The parameters are quoted if necessary and
   * any numeric parameters are considered as IntegerLiterals.
   * @see Model#add(Atom)
   */
  Atom addAtomByString(String predicateName, String... params);

  /**
   * Returns the (first) value of terms at positions i in predicates with the given name.
   */
  String[] getValue(String predicateName, int... i);

  /**
   * Returns a set of all predicate names in this model.
   */
  Set<String> getPredicateNameSet();

  /**
   * Returns the set of submodel atoms.
   */
  Set<Atom> getSubmodelAtomsSet();

  /**
   * Returns the existing submodel with the given id and parameters or {@code null} if not such submodel exists.
   */
  Model getExistingSubmodel(String id, String[] params);

}