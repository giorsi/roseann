/**
 *
 */
package uk.ac.ox.cs.diadem.model; // $codepro.audit.disable packagePrefixNamingConvention

import uk.ac.ox.cs.diadem.env.ParserException;

/**
 * <p>
 * SUMMARY: The ModelFactory creates new {@link Model Models}, {@link Atom Atoms}, and {@link Term Terms}.
 * </p>
 * <p>
 * GLOBAL CONTEXT: The Atoms and Terms created by ModelFactory reside in the same context as those Models which are
 * created via {@link #createModel()}. We call this context the GLOBAL CONTEXT.For a discussion on the context of a
 * Model, {@see uk.ac.ox.cs.diadem.model.Model}.
 * </p>
 * <p>
 * ISOLATED CONTEXT: Alternatively, a Model can also be created via {@link #createIsolatedModel()}, resulting in a Model
 * which is placed in a fresh and isolated context. If such an isolated Model is later used to create other Models (via
 * {@link Model#create(boolean, boolean)} or
 * {@link Model#create(boolean, String, uk.ac.ox.cs.diadem.util.collect.KeyMask...)}), then these Models share their
 * context with the formerly isolated Model (if the preserveContext flag is set in the corresponding calls).
 * </p>
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * @version $Revision: 1.0 $
 */
public class ModelFactory { // $codepro.audit.disable
  // com.instantiations.assist.eclipse.analysis.audit.rule.effectivejava.alwaysOverridetoString.alwaysOverrideToString
  //
  /**
   * This implementation carries the context used for non-isolated creation of Terms, Atoms, and Models.
   */
  final static private ModelImplementation globalContextModelImplementation = new ModelImplementation();

  /**
   * Creates a Model in the global context.
   * @return a newly created Model, sharing its context with all other Models created through this method, and with all
   *         subsequently derived Models, which were created through a chain of {@link Model#create(boolean, boolean)}
   *         or {@link Model#create(boolean, String, uk.ac.ox.cs.diadem.util.collect.KeyMask...)} calls.
   */
  public static Model createModel() {
    return globalContextModelImplementation.create(true, false);
  }

  /**
   * Creates a Model in a newly created and hence isolated context.
   * @return a newly created Model with an isolated context, i.e., the Model's Atoms and Terms are incompatible with any
   *         other Model. Therefore, the Terms and Atoms known to (and shared by) the new Model do not need to be
   *         managed centrally, allowing such a Model to be updated slightly more efficient. However, if you ever
   *         compare Terms or Atoms from two Models which do not share their context -- or call a method doing so, then
   *         you end up with undefined behaviour.
   */
  public static Model createIsolatedModel() {
    return new ModelImplementation();
  }

  /**
   * Creates an IntegerLiteral in the global context.
   * @param integer
   * @return the IntegerLiteral in the global context corresponding to integer
   * @throws ModelSchemaException
   */
  public static IntegerLiteral createIntegerLiteral(final int integer) {
    return globalContextModelImplementation.getIntegerLiteral(integer);
  }

  /**
   * Creates a StringLiteral in the global context.
   * @param string
   * @return the StringLiteral in the global context corresponding to string
   */
  public static StringLiteral createStringLiteral(final String string) {
    return globalContextModelImplementation.getStringLiteral(string);
  }

  /**
   * Creates a ConstantTerm in the global context.
   * @param constantName
   * @return the ConstantTerm in the global context corresponding to constantName
   */
  public static ConstantTerm createConstantTerm(final String constantName) {
    return globalContextModelImplementation.getConstantTerm(constantName);
  }

  /**
   * Creates a CompoundTerm in the global context.
   * @param functionName
   * @param terms each included Term must reside in the global context, and terms.length>0 must hold (otherwise use
   *          {@link #createConstantTerm(String)}.
   * @return the CompoundTerm in the global context corresponding to functionName and terms.
   */
  public static CompoundTerm createCompoundTerm(final String functionName, final Term... terms) {
    return globalContextModelImplementation.getCompoundTerm(functionName, terms);
  }

  /**
   * Creates a ListTerm in the global context.
   * @param terms each included Term must reside in the global context
   * @return the ListTerm in the global context corresponding to the list in terms.
   */
  public static ListTerm createListTerm(final Term... terms) {
    return globalContextModelImplementation.getListTerm(terms);
  }

  /**
   * Creates a Term in the global context by parsing termString.
   * @param termString the beginning of termString must describe a Term, the tail is ignored.
   * @return the Term in the global context which is described at the beginning of termString.
   * @throws ParserException
   */
  public static Term createTermByParsing(final String termString) throws ParserException {
    return globalContextModelImplementation.parseTerm(termString);
  }

  /**
   * Creates an Atom in the global context from a predicateName and terms.
   * @param predicateName
   * @param terms each included Term must reside in the global context.
   * @return the Atom in the global context, constructed from predicateName and terms.
   */
  public static Atom createAtom(final String predicateName, final Term... terms) {
    return globalContextModelImplementation.add(predicateName, terms);
  }

  /**
   * Creates an Atom in the global context by parsing atomString.
   * @param atomString the beginning of atomString must describe an Atom, the tail is ignored -- no terminating dot
   *          required.
   * @return the Atom in the global context which is described at the beginning of atomString.
   * @throws ParserException
   */
  public static Atom createAtomByParsing(final String atomString) throws ParserException {
    return globalContextModelImplementation.addByParsing(atomString);
  }

}
