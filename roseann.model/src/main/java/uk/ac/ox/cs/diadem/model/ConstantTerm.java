/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * @version $Revision: 1.0 $
 * 
 */
public interface ConstantTerm extends Term {
  /**
   * @return the constand name
   */
  String getId();
}
