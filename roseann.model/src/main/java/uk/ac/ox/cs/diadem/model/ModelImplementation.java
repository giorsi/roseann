// $codepro.audit.disable declareDefaultConstructors
/**
 *
 */
package uk.ac.ox.cs.diadem.model;

import static uk.ac.ox.cs.diadem.model.ModelConfiguration.HIERARCHY_SEPARATOR;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.ParserException;
import uk.ac.ox.cs.diadem.util.collect.EmptyIterableIterator;
import uk.ac.ox.cs.diadem.util.collect.IterableIterator;
import uk.ac.ox.cs.diadem.util.collect.IteratorOfIterablesIterator;
import uk.ac.ox.cs.diadem.util.collect.KeyMask;
import uk.ac.ox.cs.diadem.util.collect.Trie;
import uk.ac.ox.cs.diadem.util.collect.TrieImplementation;

import com.google.common.collect.ImmutableSet;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk> TODO ModelImplementation could remember if its
 *         constructed from a file only, without any modifications. If so, we can use the file directly in reasoning
 *         tasks.
 */
final class ModelImplementation implements Model {
  static final private Logger LOGGER = LoggerFactory.getLogger(ModelImplementation.class);

  private static final class Context {
    private final Set<ModelImplementation> modelImplementations = new HashSet<ModelImplementation>();
    private final Map<Atom, Context> subContexts = new HashMap<Atom, Context>();

    public Context(final ModelImplementation founder) {
      assert founder != null;
      modelImplementations.add(founder);
    }

    public int size() {
      return modelImplementations.size();
    }

    public void add(final ModelImplementation newMember) {
      assert newMember != null;
      modelImplementations.add(newMember);
    }

    public void remove(final ModelImplementation oldMember) {
      assert oldMember != null;
      assert modelImplementations.contains(oldMember) : "Violated Invariance";
      modelImplementations.remove(oldMember);
      if (modelImplementations.size() > 1) {
        return;
      }
      assert modelImplementations.size() == 1 : "Violated Invariance.";
      for (final ModelImplementation impl : modelImplementations) {
        impl.decontextualize();
      }
    }

    public boolean contains(final Model other) {
      return modelImplementations.contains(other);
    }

    public ModelImplementation getRepresentative() {
      assert !modelImplementations.isEmpty();
      return modelImplementations.iterator().next();
    }

    public Context getSubcontext(final Atom atom, final ModelImplementation subImplementation) {
      assert atom != null;
      assert subImplementation != null;
      Context result = subContexts.get(atom);
      if (result != null) {
        return result;
      }
      subContexts.put(atom, result = new Context(subImplementation));
      return result;
    }
  }

  // if context is set null, the Model has been released
  private Context context;

  private final Map<Atom, ModelImplementation> submodels = new HashMap<Atom, ModelImplementation>();

  private Map<String, ConstantTerm> constantTerms;
  private Map<String, StringLiteral> stringLiterals;
  private Map<Integer, IntegerLiteral> integerLiterals;
  private Map<String, Trie<Term, CompoundTerm>> compoundTerms;
  private Trie<Term, ListTerm> listTerms;
  final private Map<String, Trie<Term, Atom>> atoms = new HashMap<String, Trie<Term, Atom>>();
  private Map<String, Trie<Term, Atom>> contextAtoms;

  private final ModelParser parser;
  private final CloneModelVisistor cloneModelVisistor;

  private final ModelImplementation parentModel;

  public ModelImplementation() {
    context = new Context(this);

    parentModel = null;
    parser = new ModelParser();
    cloneModelVisistor = new CloneModelVisistor();

    initEmptyContext();
  }

  private ModelImplementation(final ModelImplementation other, final boolean preserveContext,
      final boolean preserveAtoms) {
    assert other != null;

    parentModel = null;
    parser = new ModelParser();
    cloneModelVisistor = new CloneModelVisistor();

    if (preserveAtoms) {
      for (final Entry<String, Trie<Term, Atom>> childEntry : other.atoms.entrySet()) {
        atoms.put(childEntry.getKey(), childEntry.getValue().clone());
      }
    }

    if (preserveContext) {
      context = other.context;
      context.add(this);
      initSharedContext(other);
    } else {
      context = new Context(this);
      if (preserveAtoms) {
        initCopiedContext(other);
      } else {
        initEmptyContext();
      }
    }

    if (preserveAtoms) {
      if (preserveContext) {
        for (final Entry<Atom, ModelImplementation> submodelEntry : other.submodels.entrySet()) {
          submodels.put(submodelEntry.getKey(), new ModelImplementation(submodelEntry.getValue(), preserveContext,
              preserveAtoms));
        }
      } else {
        for (final Entry<Atom, ModelImplementation> submodelEntry : other.submodels.entrySet()) {
          submodels.put(addFromOtherContext(submodelEntry.getKey()), new ModelImplementation(submodelEntry.getValue(),
              preserveContext, preserveAtoms));
        }
      }
    }
  }

  private ModelImplementation(final ModelImplementation other, final boolean preserveContext, final String id,
      final KeyMask<Term>... masks) {
    assert other != null;

    parentModel = null;
    parser = new ModelParser();
    cloneModelVisistor = new CloneModelVisistor();

    if (preserveContext) {
      context = other.context;
      context.add(this);
      initSharedContext(other);
    } else {
      context = new Context(this);
      initCopiedContext(other);
    }

    // the instance is readily build, the class invariance holds,
    // hence we can add atoms now.
    final Iterator<Atom> iter = other.iteratorForMask(id, masks);
    while (iter.hasNext()) {
      add(id, iter.next().getSubTerms());
    }

    // TODO wired -- we might want to add the models associated with matched
    // atoms
    if (preserveContext) {
      for (final Entry<Atom, ModelImplementation> submodelEntry : other.submodels.entrySet()) {
        submodels.put(submodelEntry.getKey(), new ModelImplementation(submodelEntry.getValue(), preserveContext, id,
            masks));
      }
    } else {
      for (final Entry<Atom, ModelImplementation> submodelEntry : other.submodels.entrySet()) {
        submodels.put(addFromOtherContext(submodelEntry.getKey()), new ModelImplementation(submodelEntry.getValue(),
            preserveContext, id, masks));
      }
    }
  }

  private ModelImplementation(final Atom atom, final Context parentContext, final ModelImplementation parentModel) {
    context = parentContext.getSubcontext(atom, this);

    this.parentModel = parentModel;
    parser = parentModel.parser;
    cloneModelVisistor = parentModel.cloneModelVisistor;

    if (context.size() == 1) {
      initEmptyContextFromParent(this);
    } else {
      initSharedContext(context.getRepresentative());
    }
  }

  private void initEmptyContextFromParent(final ModelImplementation parent) {
    // TODO check carefully: this is about sharing terms across different
    // model hierarchy levels.
    // By using the root of other, we use the same terms across levels.
    // Before that, we used root=other.
    // Note the ModelParser implicitly uses the same terms across different
    // hierarchy levels.
    final ModelImplementation root = parent.getRootModel();

    contextAtoms = null;

    constantTerms = root.constantTerms;
    stringLiterals = root.stringLiterals;
    integerLiterals = root.integerLiterals;
    compoundTerms = root.compoundTerms;
    listTerms = root.listTerms;
  }

  private void initEmptyContext() {
    contextAtoms = null;

    constantTerms = new HashMap<String, ConstantTerm>();
    stringLiterals = new HashMap<String, StringLiteral>();
    integerLiterals = new HashMap<Integer, IntegerLiteral>();
    compoundTerms = new HashMap<String, Trie<Term, CompoundTerm>>();
    listTerms = getTrie();
  }

  private void initSharedContext(final ModelImplementation other) {
    // TODO check carefully: this is about sharing terms across different
    // model hierarchy levels.
    // By using the root of other, we use the same terms across levels.
    // Before that, we used root=other.
    // Note the ModelParser implicitly uses the same terms across different
    // hierarchy levels.
    final ModelImplementation root = other.getRootModel();

    constantTerms = root.constantTerms;
    stringLiterals = root.stringLiterals;
    integerLiterals = root.integerLiterals;
    compoundTerms = root.compoundTerms;
    listTerms = root.listTerms;

    if (other.contextAtoms == null) {
      // other did not share its context so far
      // the context contains now other and this
      assert other.context.size() == 2 : "Violated Invariance.";
      // create context atoms as a copy of the current atoms of other
      other.contextAtoms = new HashMap<String, Trie<Term, Atom>>();
      for (final Entry<String, Trie<Term, Atom>> childEntry : other.atoms.entrySet()) {
        other.contextAtoms.put(childEntry.getKey(), childEntry.getValue().clone());
      }
    }
    contextAtoms = other.contextAtoms;
  }

  private void initCopiedContext(final ModelImplementation other) {
    // TODO check carefully: this is about sharing terms across different
    // model hierarchy levels.
    // By using the root of other, we use the same terms across levels.
    // Before that, we used root=other.
    // Note the ModelParser implicitly uses the same terms across different
    // hierarchy levels.
    final ModelImplementation root = other.getRootModel();

    contextAtoms = null;

    constantTerms = new HashMap<String, ConstantTerm>(root.constantTerms);
    stringLiterals = new HashMap<String, StringLiteral>(root.stringLiterals);
    integerLiterals = new HashMap<Integer, IntegerLiteral>(root.integerLiterals);
    compoundTerms = new HashMap<String, Trie<Term, CompoundTerm>>();
    for (final Entry<String, Trie<Term, CompoundTerm>> childEntry : root.compoundTerms.entrySet()) {
      compoundTerms.put(childEntry.getKey(), childEntry.getValue().clone());
    }
    listTerms = root.listTerms.clone();
  }

  @Override
  public ModelImplementation create(final boolean preserveContext, final String id, final KeyMask<Term>... masks) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    return new ModelImplementation(this, preserveContext, id, masks);
  }

  @Override
  public ModelImplementation create(final boolean preserveContext, final boolean preserveAtoms) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    return new ModelImplementation(this, preserveContext, preserveAtoms);
  }

  @Override
  public boolean hasSameContext(final Model other) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    return context.contains(other);
  }

  @Override
  public boolean isIsolated() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    return contextAtoms == null;
  }

  @Override
  public void isolate() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    if (contextAtoms == null) {
      return;
    }
    context.remove(this);
    context = new Context(this);
    contextAtoms = null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#finalize()
   * 
   * In principle, once this is not in use anymore, you should call release, as it isolates this, reducing the number of
   * Models attached to the original context attached to this.
   */
  @Override
  public void finalize() { // $codepro.audit.disable
    // com.instantiations.assist.eclipse.analysis.audit.rule.effectivejava.avoidFinalizers.avoidFinalizers
    if (context != null) {
      release();
    }
  }

  @Override
  public void clear() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    atoms.clear();
  }

  @Override
  public void clearPredicate(final String id) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    final Trie<Term, Atom> atomsForId = atoms.get(id);
    if (atomsForId != null) {
      atomsForId.clear();
    }
  }

  @Override
  public String getStatistics() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    final StringWriter writer = new StringWriter();
    writer.append("constant terms = " + getNumConstantTerms());
    writer.append(", string literals = " + getNumStringLiterals());
    writer.append(", integer literals = " + getNumIntegerLiterals());
    writer.append(", list terms = " + getNumListTerms());
    writer.append(", compound terms = " + getNumCompoundTerms());
    writer.append(", atoms = " + size());
    writer.append(", contextAtom = " + getNumContextAtoms());

    return writer.toString();
  }

  public int getNumConstantTerms() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    return constantTerms.size();
  }

  public int getNumStringLiterals() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    return stringLiterals.size();
  }

  public int getNumIntegerLiterals() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    return integerLiterals.size();
  }

  public int getNumListTerms() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    return listTerms.size();
  }

  public int getNumCompoundTerms(final String id) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    final Trie<Term, CompoundTerm> termsForId = compoundTerms.get(id);
    if (termsForId == null) {
      return 0;
    } else {
      return termsForId.size();
    }
  }

  public int getNumCompoundTerms() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    int result = 0;
    for (final Trie<Term, CompoundTerm> termsForId : compoundTerms.values()) {
      result += termsForId.size();
    }
    return result;
  }

  @Override
  public int sizeOfPredicate(final String id) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    final Trie<Term, Atom> atomsForId = atoms.get(id);
    if (atomsForId == null) {
      return 0;
    } else {
      return atomsForId.size();
    }
  }

  @Override
  public int size() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    int result = 0;
    for (final Trie<Term, Atom> atomsForId : atoms.values()) {
      result += atomsForId.size();
    }
    return result;
  }

  @Override
  public int sizeHierarchically() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    int size = size();
    final Iterator<Atom> submodelAtomIter = getSubmodelAtoms();
    while (submodelAtomIter.hasNext()) {
      size += getSubmodel(submodelAtomIter.next()).sizeHierarchically();
    }
    return size;
  }

  public int getNumContextAtoms() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    if (contextAtoms == null) {
      return size();
    }
    int result = 0;
    for (final Trie<Term, Atom> atomsForId : contextAtoms.values()) {
      result += atomsForId.size();
    }
    return result;
  }

  @Override
  public StringLiteral getStringLiteral(final String id) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert id != null : "Invalid Argument.";
    StringLiteral result = stringLiterals.get(id);
    if (result == null) {
      stringLiterals.put(id, result = new StringLiteralImplementation(id));
    }
    return result;
  }

  @Override
  public IntegerLiteral getIntegerLiteral(final int integer) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert integer >= 0 : "Can not add negative integers to a model.";
    IntegerLiteral result = integerLiterals.get(integer);
    if (result == null) {
      integerLiterals.put(integer, result = new IntegerLiteralImplementation(integer));
    }
    return result;
  }

  @Override
  public ConstantTerm getConstantTerm(final String id) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidConstantId(id) : "Invalid Argument: Id '" + id + "'";
    ConstantTerm result = constantTerms.get(id);
    if (result == null) {
      constantTerms.put(id, result = new ConstantTermImplementation(id));
    }
    return result;
  }

  @Override
  public CompoundTerm getCompoundTerm(final String id, final Term... terms) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidConstantId(id) : "Invalid Argument: Id '" + id + "'";
    assert terms != null : "Invalid Argument: with Id '" + id + "'";
    assert terms.length > 0 : "Invalid Argument: with Id '" + id + "'";
    assert containsNoNullTerm(terms) : "Invalid Argument: with Id '" + id + "'";
    Trie<Term, CompoundTerm> termsForId = compoundTerms.get(id);
    if (termsForId == null) {
      compoundTerms.put(id, termsForId = getTrie());
    }
    // Potential for optimization: the next two lines traverse the trie each
    // once
    CompoundTerm result = termsForId.get(terms);
    if (result == null) {
      termsForId.putOverwrite(result = new CompoundTermImplementation(id, terms), terms);
    }
    return result;
  }

  @Override
  public ListTerm getListTerm(final Term... terms) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert terms != null : "Invalid Argument.";
    assert containsNoNullTerm(terms) : "Invalid Argument.";
    // Potential for optimization: the next two lines traverse the trie each
    // once
    ListTerm result = listTerms.get(terms);
    if (result == null) {
      listTerms.putOverwrite(result = new ListTermImplementation(terms), terms);
    }
    return result;
  }

  @Override
  public Atom add(final String id, final Term... terms) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    assert terms != null : "Invalid Argument: with Id '" + id + "'";
    assert containsNoNullTerm(terms) : "Invalid Argument: with Id '" + id + "'";

    if (contextAtoms != null) {
      Trie<Term, Atom> contextAtomsForId = contextAtoms.get(id);
      Trie<Term, Atom> atomsForId;
      if (contextAtomsForId == null) {
        contextAtoms.put(id, contextAtomsForId = getTrie());
        assert atoms.get(id) == null : "Violated Invariance : Context for id is empty, but concrete atoms are not.";
        atoms.put(id, atomsForId = getTrie());
      } else if ((atomsForId = atoms.get(id)) == null) {
        atoms.put(id, atomsForId = getTrie());
      }
      final Atom result = contextAtomsForId.putTentatively(new AtomImplementation(id, terms), terms);
      atomsForId.putTentatively(result, terms);
      return result;
    } else {
      Trie<Term, Atom> atomsForId = atoms.get(id);
      if (atomsForId == null) {
        atoms.put(id, atomsForId = getTrie());
      }
      return atomsForId.putTentatively(new AtomImplementation(id, terms), terms);
    }
  }

  @Override
  public Atom add(final Atom atom) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert atom != null : "Invalid Argument.";
    return add(atom.getId(), atom.getSubTerms());
  }

  @Override
  public void release() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    isolate();
    context = null;
  }

  private static <T, V> Trie<T, V> getTrie() {
    return new TrieImplementation<T, V>(true);
  }

  @Override
  public Atom get(final String id, final Term... terms) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    assert terms != null : "Invalid Argument: with Id '" + id + "'";
    assert containsNoNullTerm(terms) : "Invalid Argument: with Id '" + id + "'";
    final Trie<Term, Atom> atomsForId = atoms.get(id);
    if (atomsForId == null) {
      return null;
    }
    return atomsForId.get(terms);
  }

  @Override
  public void remove(final String id, final Term... terms) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    assert terms != null : "Invalid Argument: with Id '" + id + "'";
    assert containsNoNullTerm(terms) : "Invalid Argument (null term) for predicate with Id '" + id + "'";
    final Trie<Term, Atom> atomsForId = atoms.get(id);
    if (atomsForId == null) {
      return;
    }
    atomsForId.remove(terms);
  }

  @Override
  public void removeByMask(final String predicateName, final KeyMask<Term>... termMasks) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termMasks != null : "Invalid Argument: with Id '" + predicateName + "'";

    for (final Atom a : getAtomsByMask(predicateName, termMasks)) {
      this.remove(a);
    }
  }

  @Override
  public void removeByPrefixMask(final String predicateName, final KeyMask<Term>... termMasks) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termMasks != null : "Invalid Argument: with Id '" + predicateName + "'";

    for (final Atom a : getAtomsByPrefixMask(predicateName, termMasks)) {
      this.remove(a);
    }
  }

  @Override
  public String toString() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    final StringWriter writer = new StringWriter();
    write(writer);
    return writer.toString();
  }

  @Override
  public String toStringHierarchically() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    final StringWriter writer = new StringWriter();
    writeHierarchically(writer);
    return writer.toString();
  }

  @Override
  public IterableIterator<Atom> iterator() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    return new IteratorOfIterablesIterator<Atom, Trie<Term, Atom>>(atoms.values().iterator());
  }

  private void cleanAtoms() {
    final Iterator<Entry<String, Trie<Term, Atom>>> iter = atoms.entrySet().iterator();
    while (iter.hasNext()) {
      final Entry<String, Trie<Term, Atom>> entry = iter.next();
      if (entry.getValue().isEmpty()) {
        iter.remove();
      }
    }
  }

  @Override
  public Set<String> getPredicateNameSet() {
    cleanAtoms();
    return new HashSet<String>(atoms.keySet());
  }

  @Override
  public IterableIterator<Atom> iteratorForPredicate(final String id) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    final Trie<Term, Atom> atomsForId = atoms.get(id);
    if (atomsForId == null) {
      return new EmptyIterableIterator<Atom>();
    }
    return atomsForId.iterator();
  }

  @Override
  public IterableIterator<Atom> iteratorForMask(final String predicateName, final KeyMask<Term>... termMasks) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    // this time null terms are allowed!
    final Trie<Term, Atom> atomsForId = atoms.get(predicateName);
    if (atomsForId == null) {
      return new EmptyIterableIterator<Atom>();
    }
    return atomsForId.iteratorForMasks(termMasks);
  }

  @Override
  public IterableIterator<Atom> iteratorForPrefixMask(final String predicateName,
      final KeyMask<Term>... termPrefixMasks) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termPrefixMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    // this time null terms are allowed!
    final Trie<Term, Atom> atomsForId = atoms.get(predicateName);
    if (atomsForId == null) {
      return new EmptyIterableIterator<Atom>();
    }
    return atomsForId.iteratorForPrefixMasks(termPrefixMasks);
  }

  @Override
  public void remove(final Atom atom) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    remove(atom.getId(), atom.getSubTerms());
  }

  @Override
  public boolean contains(final String id, final Term... terms) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    assert terms != null : "Invalid Argument: with Id '" + id + "'";
    assert containsNoNullTerm(terms) : "Invalid Argument: with Id '" + id + "'";
    final Trie<Term, Atom> atomsForId = atoms.get(id);
    if (atomsForId == null) {
      return false;
    }
    return atomsForId.get(terms) != null;
  }

  @Override
  public boolean contains(final Atom atom) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert atom != null : "Invalid Argument.";
    final Trie<Term, Atom> atomsForId = atoms.get(atom.getId());
    if (atomsForId == null) {
      return false;
    }
    final Atom foundAtom = atomsForId.get(atom.getSubTerms());
    assert (foundAtom == null) || (foundAtom == atom) : "If foundAtom == atom, then you"
        + "are comparing for an atom which is not in the context of the current Model.";
    return foundAtom != null;
  }

  @Override
  public boolean containsPredicate(final String id) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    final Trie<Term, Atom> atomsForId = atoms.get(id);
    if (atomsForId == null) {
      return false;
    }
    return !atoms.get(id).isEmpty();
  }

  @Override
  public boolean containsMask(final String predicateName, final KeyMask<Term>... termMasks) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    // this time null terms are allowed!
    return iteratorForMask(predicateName, termMasks).hasNext(); // TODO
    // improve.
  }

  @Override
  public boolean containsPrefixMask(final String predicateName, final KeyMask<Term>... termPrefixMasks) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termPrefixMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    // this time null terms are allowed!
    return iteratorForPrefixMask(predicateName, termPrefixMasks).hasNext(); // TODO
    // improve.
  }

  @Override
  public Set<Atom> getAtomsByMask(final String predicateName, final KeyMask<Term>... termMasks) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    final Set<Atom> results = new HashSet<Atom>();
    for (final Atom a : iteratorForMask(predicateName, termMasks)) {
      results.add(a);
    }
    return results;
  }

  @Override
  public Set<Atom> getAtomsByPrefixMask(final String predicateName, final KeyMask<Term>... termPrefixMasks) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termPrefixMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    final Set<Atom> results = new HashSet<Atom>();
    for (final Atom a : iteratorForPrefixMask(predicateName, termPrefixMasks)) {
      results.add(a);
    }
    return results;
  }

  @Override
  public Atom getUniqueAtomByMask(final String predicateName, final KeyMask<Term>... termMasks)
      throws ModelAccessException {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    final Iterator<Atom> iter = iteratorForMask(predicateName, termMasks);
    if (!iter.hasNext()) {
      throw new ModelAccessException("No Atom in model for predicate '" + predicateName + "' and keymask '" + termMasks
          + "'", LOGGER);
    }
    final Atom result = iter.next();
    if (iter.hasNext()) {
      throw new ModelAccessException("More than Atom in model for predicate '" + predicateName + "' and keymask '"
          + termMasks + "'", LOGGER);
    }
    return result;
  }

  @Override
  public Atom getUniqueAtomByPrefixMask(final String predicateName, final KeyMask<Term>... termPrefixMasks)
      throws ModelAccessException {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termPrefixMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    final Iterator<Atom> iter = iteratorForPrefixMask(predicateName, termPrefixMasks);
    if (!iter.hasNext()) {
      throw new ModelAccessException("No Atom in model for predicate '" + predicateName + "' and prefix keymask '"
          + termPrefixMasks + "'", LOGGER);
    }
    final Atom result = iter.next();
    if (iter.hasNext()) {
      throw new ModelAccessException("More than Atom in model for predicate '" + predicateName
          + "' and prefix keymask '" + termPrefixMasks + "'", LOGGER);
    }
    return result;
  }

  @Override
  public Set<Term> getTermsByMask(final int termPosition, final String predicateName, final KeyMask<Term>... termMasks) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termMasks != null : "Invalid Argument: " + termMasks + "with Id '" + predicateName + "'";
    assert 0 <= termPosition;
    assert termPosition < termMasks.length;
    final Set<Term> results = new HashSet<Term>();
    for (final Atom a : iteratorForMask(predicateName, termMasks)) {
      results.add(a.getSubTerms()[termPosition]);
    }
    return results;
  }

  @Override
  public Set<Term> getTermsByPrefixMask(final int termPosition, final String predicateName,
      final KeyMask<Term>... termMasks) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    assert 0 <= termPosition;
    // assert termPosition < termMasks.length;
    final Set<Term> results = new HashSet<Term>();
    for (final Atom a : iteratorForPrefixMask(predicateName, termMasks)) {
      results.add(a.getSubTerms()[termPosition]);
    }
    return results;
  }

  @Override
  public Set<Atom> getAtomsByPredicate(final String id) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(id) : "Invalid Argument: Id '" + id + "'";
    final Set<Atom> results = new HashSet<Atom>();
    for (final Atom a : iteratorForPredicate(id)) {
      results.add(a);
    }
    return results;
  }

  @Override
  public Atom addByParsing(final String atomAsString) throws ParserException {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    parser.init(atomAsString, this);
    try {
      return parser.parseAtom();
    } catch (final IOException e) {
      e.printStackTrace();
      assert false : "Violated Invariance.";
      return null;
    }
  }

  @Override
  public Atom addByParsingHierarchically(final String atomAsString) throws ParserException {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    parser.init(new StringReader(atomAsString), this, true);
    try {
      return parser.parseAtom();
    } catch (final IOException e) {
      e.printStackTrace();
      assert false : "Violated Invariance.";
      return null;
    }
  }

  @Override
  public Term parseTerm(final String termAsString) throws ParserException {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    parser.init(termAsString, this);
    Term result;
    try {
      result = parser.parseTerm();
      // TODO has not been enabled for a long time. Should be enabled.
      // if (!parser.isEmpty())
      // throw new ParserException("The string '" + termAsString + "' contains more than one term.", LOGGER);
    } catch (final IOException e) {
      e.printStackTrace();
      assert false : "Violated Invariance.";
      result = null;
    }
    if (result == null) {
      throw new ParserException("Could not parse '" + termAsString + "' as term.", LOGGER);
    }
    return result;
  }

  protected boolean equatableTo(final Object other) {
    return other instanceof ModelImplementation;
  }

  protected boolean equalsCore(final ModelImplementation other) {
    if (!hasSameContext(other)) {
      return false;
    }
    if (size() != other.size()) {
      return false;
    }
    for (final Atom atom : other.iterator()) {
      if (!contains(atom)) {
        LOGGER.debug("Atom differs: {}", atom.toString());
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean equals(final Object other) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    if (this == other) {
      return true;
    }
    if (!(other instanceof ModelImplementation)) {
      return false;
    }
    final ModelImplementation o = (ModelImplementation) other;
    if (!o.equatableTo(this)) {
      return false;
    }
    return equalsCore(o);
  }

  private static boolean isValidId(final String id) {
    assert id != null : "isValidId with null";

    if (id.isEmpty()) {
      return false;
    }

    final char first = id.charAt(0);
    if (((first < 'a') || ('z' < first)) && ((first < 'A') || ('Z' < first))) {
      return false;
    }

    final int len = id.length();
    for (int i = 1; i < len; ++i) {
      final char cur = id.charAt(i);
      if (((cur < 'a') || ('z' < cur)) && ((cur < 'A') || ('Z' < cur)) && ((cur < '0') || ('9' < cur)) && (cur != '_')) {
        return false;
      }
    }
    return true;
  }

  private static boolean isValidConstantId(final String id) {
    assert id != null : "isValidId with null";

    if (id.isEmpty()) {
      return false;
    }

    final char first = id.charAt(0);
    if (((first < 'a') || ('z' < first)) && ((first < 'A') || ('Z' < first))) {
      return false;
    }
    if ((first < 'a') || ('z' < first)) {
      return false;
      // LOGGER.error("Invalid constant id '{}'.", id);
    }

    final int len = id.length();
    for (int i = 1; i < len; ++i) {
      final char cur = id.charAt(i);
      if (((cur < 'a') || ('z' < cur)) && ((cur < 'A') || ('Z' < cur)) && ((cur < '0') || ('9' < cur)) && (cur != '_')) {
        return false;
      }
    }
    return true;
  }

  /**
   * @param subTerms
   * @return
   */
  private static boolean containsNoNullTerm(final Term[] terms) {
    assert terms != null;
    for (final Term term : terms) {
      if (term == null) {
        return false;
      }
    }
    return true;
  }

  @Override
  public void writeHierarchically(final Writer writer) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert writer != null;
    write(writer);
    for (final Entry<Atom, ModelImplementation> submodelEntry : submodels.entrySet()) {
      final Atom keyAtom = submodelEntry.getKey();
      final String outputPrefix = keyAtom.getId() + Integer.toString(keyAtom.getSubTerms().length)
          + HIERARCHY_SEPARATOR;
      submodelEntry.getValue().writeInternal(writer, outputPrefix, submodelEntry.getKey().getSubTerms());
    }
  }

  private void writeInternal(final Writer writer, final String outputPrefix, final Term[] prefixTerms) {
    assert writer != null;
    assert outputPrefix != null;
    assert prefixTerms != null;
    try {
      final String termPrefix;
      if (prefixTerms.length != 0) {
        final StringWriter prefixWriter = new StringWriter();
        prefixWriter.write('(');
        prefixTerms[0].write(prefixWriter);
        for (int i = 1; i < prefixTerms.length; ++i) {
          prefixWriter.write(',');
          prefixTerms[i].write(prefixWriter);
        }
        termPrefix = prefixWriter.toString();
      } else {
        termPrefix = null;
      }

      if (termPrefix != null) {
        for (final Atom atom : this) {
          writer.append(outputPrefix);
          writer.append(atom.getId());
          final Term[] subTerms = atom.getSubTerms();
          writer.append(termPrefix);
          for (final Term subTerm : subTerms) {
            writer.write(',');
            subTerm.write(writer);
          }
          writer.append(')');
          writer.append(".\n");
        }
      } else {
        for (final Atom atom : this) {
          writer.append(outputPrefix);
          atom.write(writer);
          writer.append(".\n");
        }
      }
    } catch (final IOException e) {
      throw new ModelIOException("Failed writing Model to writer", e, LOGGER);
    }

    for (final Entry<Atom, ModelImplementation> submodelEntry : submodels.entrySet()) {
      final Atom keyAtom = submodelEntry.getKey();
      final String expandedOutputPrefix = outputPrefix + keyAtom.getId()
          + Integer.toString(keyAtom.getSubTerms().length) + HIERARCHY_SEPARATOR;
      final Term[] additionalPrefixTerms = submodelEntry.getKey().getSubTerms();
      final Term[] newPrefixTerms = new Term[prefixTerms.length + additionalPrefixTerms.length];
      for (int i = 0; i < prefixTerms.length; ++i) {
        newPrefixTerms[i] = prefixTerms[i];
      }
      for (int i = 0; i < additionalPrefixTerms.length; ++i) {
        newPrefixTerms[prefixTerms.length + i] = additionalPrefixTerms[i];
      }
      submodelEntry.getValue().writeInternal(writer, expandedOutputPrefix, newPrefixTerms);
    }
  }

  @Override
  public void write(final Writer writer) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert writer != null;
    try {
      for (final Atom atom : this) {
        atom.write(writer);
        writer.append(".\n");
      }
    } catch (final IOException e) {
      throw new ModelIOException("Failed writing Model to writer", e, LOGGER);
    }
  }

  @Override
  public void write(final Writer writer, final String prefix) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert writer != null;
    try {
      for (final Atom atom : this) {
        writer.append(prefix);
        atom.write(writer);
        writer.append(".\n");
      }
    } catch (final IOException e) {
      throw new ModelIOException("Failed writing Model to writer", e, LOGGER);
    }
  }

  @Override
  public void write(final File file) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert file != null;
    LOGGER.debug("Saving Model to '{}'.", file);
    Writer writer = null;
    try {
      writer = new BufferedWriter(new FileWriter(file), 4096 * 4096);
      write(writer);
      writer.close();
    } catch (final IOException e) {
      throw new ModelIOException("Failed writing Model to file '" + file + "'", e, LOGGER);
    } finally {
      LOGGER.debug("DONE saving Model to '{}'.", file);
    }
  }

  @Override
  public void writeHierarchically(final File file) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert file != null;
    LOGGER.debug("Saving Model hierarchically to '{}'.", file);
    Writer writer = null;
    try {
      writer = new BufferedWriter(new FileWriter(file), 4096 * 4096);
      writeHierarchically(writer);
      writer.close();
    } catch (final IOException e) {
      throw new ModelIOException("Failed writing Model to file '" + file + "'", e, LOGGER);
    } finally {
      LOGGER.debug("DONE saving Model hierarchically to '{}'.", file);
    }
  }

  @Override
  public void add(final Iterable<Atom> atoms) {
    for (final Atom atom : atoms) {
      add(atom);
    }
  }

  @Override
  public void read(final String... strings) {
    for (final String s : strings) {
      read(new StringReader(s));
    }
  }

  @Override
  public void readHierarchically(final Reader... readers) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert (readers != null) && (readers.length > 0);
    for (final Reader reader : readers) {
      parser.init(reader, this, true);
      try {
        parser.parseAtoms();
      } catch (final IOException e) {
        throw new ModelIOException("Could not read model", e, LOGGER);
      } catch (final ParserException e) {
        throw new ModelIOException("Could not parse a model", e, LOGGER);
      }
    }
  }

  @Override
  public void readHierarchically(final File... files) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert (files != null) && (files.length > 0);
    for (final File file : files) {
      try {
        final Reader reader = new BufferedReader(new FileReader(file));
        readHierarchically(reader);
        reader.close(); // TODO
      } catch (final FileNotFoundException e) {
        throw new ModelIOException("File '" + file + "' could not be opened to read model", e, LOGGER);
      } catch (final IOException e) {
        throw new ModelIOException("File '" + file + "' cound not be read", e, LOGGER);
      }
    }
  }

  @Override
  public void read(final File... files) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert (files != null) && (files.length > 0);
    for (final File f : files) {
      try {
        parser.init(new BufferedReader(new FileReader(f)), this);
        parser.parseAtoms();
      } catch (final FileNotFoundException e) {
        throw new ModelIOException("File '" + f + "' could not be opened to read model", e, LOGGER);
      } catch (final IOException e) {
        throw new ModelIOException("File '" + f + "' cound not be read", e, LOGGER);
      } catch (final ParserException e) {
        throw new ModelIOException("File '" + f + "' could not be parsed as model", e, LOGGER);
      }
    }
  }

  @Override
  public void read(final Reader... readers) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert (readers != null) && (readers.length > 0);
    for (final Reader r : readers) {
      parser.init(r, this);
      try {
        parser.parseAtoms();
      } catch (final IOException e) {
        throw new ModelIOException("Could not read model", e, LOGGER);
      } catch (final ParserException e) {
        throw new ModelIOException("Could not parse a model", e, LOGGER);
      }
    }
  }

  /**
   * This method is invoked by Context, once only this remains in this context.
   */
  void decontextualize() {
    contextAtoms = null;
  }

  @Override
  public Model getSubmodel(final Atom atom) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert atom != null;
    ModelImplementation result = submodels.get(atom);
    if (result != null) {
      return result;
    }
    submodels.put(atom, result = new ModelImplementation(atom, context, this));
    return result;
  }

  @Override
  public Model getSubmodel(final String atomString) throws ParserException {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert atomString != null;
    final Atom atom = addByParsingHierarchically(atomString);
    assert contains(atom) : "getSubmodel only works for a single step. This is a bug to be fixed.";

    ModelImplementation result = submodels.get(atom);
    if (result != null) {
      return result;
    }
    submodels.put(atom, result = new ModelImplementation(atom, context, this));
    return result;
  }

  @Override
  public Iterator<Atom> getSubmodelAtoms() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    return submodels.keySet().iterator();
  }

  @Override
  public Set<Atom> getSubmodelAtomsSet() {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    return ImmutableSet.copyOf(submodels.keySet());
  }

  @Override
  public Atom addFromOtherContext(final Atom atom) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert atom != null;
    final String predicate = atom.getId();
    final Term[] terms = atom.getSubTerms();
    final Term[] newTerms = new Term[terms.length];
    for (int i = 0; i < terms.length; ++i) {
      newTerms[i] = getTermFromOtherContext(terms[i]);
    }
    return add(predicate, newTerms);
  }

  @Override
  public void addFromOtherContext(final Iterable<Atom> atoms) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert atoms != null;
    for (final Atom atom : atoms) {
      addFromOtherContext(atom);
    }
  }

  @Override
  public void addFromOtherContextHierarchically(final Model model) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert model != null;

    // local add
    addFromOtherContext(model);

    // recursion
    final Iterator<Atom> iter = model.getSubmodelAtoms();
    while (iter.hasNext()) {
      final Atom originalAtom = iter.next();
      final Atom localAtom = addFromOtherContext(originalAtom);
      final Model originalSubmodel = model.getSubmodel(originalAtom);
      final Model localSubmodel = getSubmodel(localAtom);
      localSubmodel.addFromOtherContextHierarchically(originalSubmodel);
    }
  }

  @Override
  public Term getTermFromOtherContext(final Term term) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert term != null;
    cloneModelVisistor.init(this);
    return term.accept(cloneModelVisistor);
  }

  @Override
  public ModelSchema getModelSchema() {
    final ModelSchema result = new ModelSchema();
    fillModelSchema(result);
    return result;
  }

  private void fillModelSchema(final ModelSchema modelSchema) {
    for (final Entry<String, Trie<Term, Atom>> entry : atoms.entrySet()) {
      final Iterator<Atom> iter = entry.getValue().iterator();
      if (!iter.hasNext()) {
        continue;
      }
      final Atom exampleAtom = iter.next();
      try {
        modelSchema.addPredicate(exampleAtom.getId(), exampleAtom.getSubTerms().length);
      } catch (final ModelSchemaException e) {
        e.printStackTrace();
        assert false : "We add each predicate only once -- cannot run into a ModelSchema Exception.";
      }
    }

    for (final Entry<Atom, ModelImplementation> entry : submodels.entrySet()) {
      final ModelSchema submodelSchema = modelSchema.addSubmodelSchema(entry.getKey().getId());
      entry.getValue().fillModelSchema(submodelSchema);
    }
  }

  @Override
  public Model getParentModel() {
    return parentModel;
  }

  @Override
  public ModelImplementation getRootModel() {
    ModelImplementation root = this;
    while (root.parentModel != null) {
      root = root.parentModel;
    }
    return root;
  }

  @Override
  public Model getSubmodel(final String predicateName, final String... params) {
    final Model sub = this.getSubmodel(addAtomByString(predicateName, params));
    return sub;
  }

  @Override
  public Model getSubmodel(final String predicateName, final Term... params) {
    final Model sub = this.getSubmodel(add(predicateName, params));
    return sub;
  }

  @Override
  public Atom addAtomByString(final String predicateName, final String... params) {
    final Term[] terms = new Term[params.length];
    for (int i = 0; i < params.length; i++) {
      try {
        String value = params[i];
        if (value.isEmpty() || !(value.startsWith("\"") || isValidConstantId(value) || value.matches("\\d+"))) {
          value = "\"" + value + "\"";
        }
        terms[i] = parseTerm(value);
      } catch (final ParserException e) {
        LOGGER.error("Problem parsing term {} in predicate {}", params[i], predicateName);
        return null;
      }
    }
    return this.add(predicateName, terms);
  }

  @Override
  public String[] getValue(final String predicateName, final int... pos) {
    final String[] strs = new String[pos.length];
    for (int i = 0; i < pos.length; i++) {
      @SuppressWarnings("unchecked")
      final Set<Term> s = getTermsByPrefixMask(pos[i], predicateName);
      if (s.size() < 1) {
        strs[i] = null;
      }
      strs[i] = s.iterator().next().getValue();
    }
    return strs;
  }

  @Override
  public Model getSubmodelByPredicateName(final String atomAsString) {
    final Model sub = this.getSubmodel(addAtomByString(atomAsString));
    return sub;
  }

  @Override
  public Term getUniqueTermByMask(final int termPosition, final String predicateName, final KeyMask<Term>... termMasks)
      throws ModelAccessException {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termMasks != null : "Invalid Argument: " + termMasks + "with Id '" + predicateName + "'";
    assert 0 <= termPosition;
    assert termPosition < termMasks.length;
    return getUniqueAtomByMask(predicateName, termMasks).getSubTerm(termPosition);
  }

  @Override
  public Term getUniqueTermByPrefixMask(final int termPosition, final String predicateName,
      final KeyMask<Term>... termPrefixMasks) throws ModelAccessException {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert isValidId(predicateName) : "Invalid Argument: Id '" + predicateName + "'";
    assert termPrefixMasks != null : "Invalid Argument: with Id '" + predicateName + "'";
    final Atom atom = getUniqueAtomByPrefixMask(predicateName, termPrefixMasks);
    final Term[] terms = atom.getSubTerms();
    if (terms.length <= termPosition) {
      throw new ModelAccessException("Atom selected '" + atom + "' has no term at position " + termPosition + ".",
          LOGGER);
    }
    return terms[termPosition];
  }

  @Override
  public Model getExistingSubmodel(final String id, final String[] params) {
    assert context != null : "Violated Protocol: ModelImplementation already released.";
    assert id != null;
    final Atom atom = addAtomByString(id, params);
    assert contains(atom) : "getSubmodel only works for a single step. This is a bug to be fixed.";

    final ModelImplementation result = submodels.get(atom);
    return result;
  }
}