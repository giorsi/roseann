// $codepro.audit.disable declareDefaultConstructors
/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

import org.slf4j.Logger;

import uk.ac.ox.cs.diadem.env.DiademException;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
public class ModelSchemaException extends DiademException {
  private static final long serialVersionUID = 1L;

  ModelSchemaException(String message, Logger logger) {
    super(message, logger);
    logger.error(message);
  }

  ModelSchemaException(String message, Throwable cause, Logger logger) {
    super(message, cause, logger);
    logger.error(message);
  }
}
