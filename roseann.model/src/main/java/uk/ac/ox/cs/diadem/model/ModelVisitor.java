package uk.ac.ox.cs.diadem.model;

public interface ModelVisitor<T> {
  T visit(CompoundTerm compoundTerm);

  T visit(ConstantTerm constantTerm);

  T visit(IntegerLiteral integerLiteral);

  T visit(ListTerm listTerm);

  T visit(StringLiteral stringLiteral);

  T visit(AtomImplementation atomImplementation);
}
