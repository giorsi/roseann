package uk.ac.ox.cs.diadem.model;

final class CloneModelVisistor implements ModelVisitor<Term> {

  private Model model;

  void init(Model model) {
    this.model = model;
  }

  @Override
  public Term visit(CompoundTerm compoundTerm) {
    final String predicateName = compoundTerm.getId();
    final Term[] subTerms = compoundTerm.getSubTerms();
    final Term[] newSubTerms = new Term[subTerms.length];
    for (int i = 0; i < subTerms.length; ++i)
      newSubTerms[i] = subTerms[i].accept(this);
    return model.getCompoundTerm(predicateName, newSubTerms);
  }

  @Override
  public Term visit(ConstantTerm constantTerm) {
    return model.getConstantTerm(constantTerm.getId());
  }

  @Override
  public Term visit(IntegerLiteral integerLiteral) {
    return model.getIntegerLiteral(integerLiteral.getInteger());
  }

  @Override
  public Term visit(ListTerm listTerm) {
    final Term[] subTerms = listTerm.getSubTerms();
    final Term[] newSubTerms = new Term[subTerms.length];
    for (int i = 0; i < subTerms.length; ++i)
      newSubTerms[i] = subTerms[i].accept(this);
    return model.getListTerm(newSubTerms);
  }

  @Override
  public Term visit(StringLiteral stringLiteral) {
    return model.getStringLiteral(stringLiteral.getString());
  }

  @Override
  public Term visit(AtomImplementation atomImplementation) {
    assert false;
    return null;
  }

}
