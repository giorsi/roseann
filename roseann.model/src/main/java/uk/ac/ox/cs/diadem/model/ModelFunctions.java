package uk.ac.ox.cs.diadem.model;

import java.util.HashSet;
import java.util.Set;

public class ModelFunctions {
  public static Set<Term> project(Set<Atom> atoms, int position) {
    final Set<Term> result = new HashSet<Term>();
    for (final Atom atom : atoms)
      result.add(atom.getSubTerm(position));
    return result;
  }
}
