/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 *
 */
public interface StringLiteral extends Term {
	public String getString();
}
