/**
 *
 */
package uk.ac.ox.cs.diadem.model;

import java.io.IOException;
import java.io.Writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//$codepro.audit.disable declareDefaultConstructors
/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 */
final class IntegerLiteralImplementation implements IntegerLiteral {

  static final Logger logger = LoggerFactory.getLogger(IntegerLiteralImplementation.class);

  final private int integer;

  IntegerLiteralImplementation(final int integer) {
    assert integer >= 0;
    if ((integer < 0) || (integer > MAX_INT)) {
      throw new ModelIOException("Integer " + integer + " not between 0 and " + MAX_INT + "!", logger);
    }
    this.integer = integer;
  }

  @Override
  public int getInteger() {
    return integer;
  }

  @Override
  public String toString() {
    return Long.toString(integer);
  }

  @Override
  public void write(final Writer writer) throws IOException {
    // Long value=integer;
    // if (value==0) { writer.append('0'); return; }
    // if(value<0) { writer.append('-'); value=-value; }
    //
    // char[] chars=new char[64];
    // int pos=0;
    // while(value!=0) {
    // chars[pos]=(char)(value%10+'0');
    // ++pos;
    // value=value/10;
    // }
    // for(--pos;pos>=0;writer.append(chars[pos]),--pos);
    writer.append(Long.toString(integer));
  }

  @Override
  public boolean matchKeyByIdentity(final Term term) {
    return term == this;
  }

  @Override
  public Term singleyKeyMatch() {
    return this;
  }

  @Override
  public boolean matchKeyByEquality(final Term term) {
    assert false : "Invalid Protocol: Terms should only identity-compared.";
    return false;
  }

  @Override
  public <T> T accept(final ModelVisitor<T> modelVisitor) {
    assert modelVisitor != null;
    return modelVisitor.visit(this);
  }

  @Override
  public String getValue() {
    return Long.toString(integer);
  }
}
