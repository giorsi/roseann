/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringWriter;

import org.junit.Test;

import uk.ac.ox.cs.diadem.model.Atom;
import uk.ac.ox.cs.diadem.model.Model;
import uk.ac.ox.cs.diadem.model.ModelDiffAnalyzer;
import uk.ac.ox.cs.diadem.model.ModelDiffAnalyzer.RESULT;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;


/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 *
 */
public class ModelDiffAnalyzerTest extends StandardTestcase {
	
	static private Model getSmallModel() {
		final Model model=new ModelImplementation();
		
		int size=0;
		for(int i=0;i<3;++i) {
			assertEquals(model.sizeOfPredicate("a"),i);
			model.add("a", model.getIntegerLiteral(i));
			++size;
			assertEquals(model.size(),size);
			assertEquals(model.sizeOfPredicate("a"),i+1);
			for(int j=0;j<3;++j) {
				model.add("b", model.getIntegerLiteral(i),
						model.getIntegerLiteral(j));
				++size;
				assertEquals(model.size(),size);
				assertEquals(model.sizeOfPredicate("b"),i*3+j+1);
			}
		}		
		return model;
	}
	
	@Test
	public void testCompareModel() throws IOException {
		StringWriter writer=new StringWriter();
		Model model=getSmallModel();
		Model model1=model.create(true, true);
		ModelDiffAnalyzer modelDiffAnalyzer=new ModelDiffAnalyzer(writer);
		assertEquals(RESULT.EQUAL,modelDiffAnalyzer.compare(model,model1));
		assertEquals(RESULT.EQUAL,modelDiffAnalyzer.compare(model1,model));

		Atom atom=model1.add("huhu", model1.getStringLiteral("hihi"));
		assertEquals(RESULT.SUBSET,modelDiffAnalyzer.compare(model,model1));
		assertEquals(RESULT.SUPERSET,modelDiffAnalyzer.compare(model1,model));
		assertTrue(database.check("testCompareModel/1", writer.toString()));
		
		model1.remove(atom);
		assertEquals(RESULT.EQUAL,modelDiffAnalyzer.compare(model,model1));
		assertEquals(RESULT.EQUAL,modelDiffAnalyzer.compare(model1,model));
		
		model.add(atom);
		model1.add("hoho", model1.getStringLiteral("hihi"));
		writer=new StringWriter(); modelDiffAnalyzer=new ModelDiffAnalyzer(writer);
		assertEquals(RESULT.INCOMPARABLE,modelDiffAnalyzer.compare(model,model1));
		assertTrue(database.check("testCompareModel/2", writer.toString()));
		writer=new StringWriter(); modelDiffAnalyzer=new ModelDiffAnalyzer(writer);
		assertEquals(RESULT.INCOMPARABLE,modelDiffAnalyzer.compare(model1,model));
		assertTrue(database.check("testCompareModel/3", writer.toString()));
	}
}
