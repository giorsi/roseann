package uk.ac.ox.cs.diadem.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import uk.ac.ox.cs.diadem.env.ParserException;
import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;

public class CloneModelVisistorTest extends StandardTestcase {

  private Model getModel() {
    return new ModelImplementation();
  }

  @Test
  public void testScratch() throws ParserException {
    final Model model = getModel();
    final Term[] terms = model.addByParsing("atom(1,\"a\",[],[1,2,3],[f(5),s(s(5,s(8)))])").getSubTerms();
    final Model model2 = getModel();
    final CloneModelVisistor visitor = new CloneModelVisistor();
    visitor.init(model2);
    final Term[] terms2 = new Term[terms.length];
    for (int i = 0; i < terms.length; ++i)
      terms2[i] = terms[i].accept(visitor);
    for (int i = 0; i < terms.length; ++i)
      assertEquals(terms2[i].toString(), terms[i].toString());
  }

}
