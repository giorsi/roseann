package uk.ac.ox.cs.diadem.model;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;

public class ModelSchemaTest extends StandardTestcase {

  @Test
  public void testCreation() throws ModelSchemaException {
    final ModelSchema ms = new ModelSchema();
    assertEquals(ms.toString(), "");

    assertEquals(ms.getArity("painter"), -1);
    assertEquals(ms.getSubmodelSchema("painter"), null);
    assertEquals(ms.toString(), "");

    ms.addPredicate("painter", 1);
    assertEquals(ms.getArity("painter"), 1);
    assertEquals(ms.getSubmodelSchema("painter"), null);
    assertTrue(database.check("1", ms.toString()));

    final ModelSchema subms = ms.addSubmodelSchema("painter");
    assertEquals(ms.getSubmodelSchema("painter"), subms);
    assertTrue(database.check("1", ms.toString()));

    subms.addPredicate("favoriteColor", 1);
    subms.addPredicate("painting", 2);
    assertTrue(database.check("2", ms.toString()));

    assertArrayEquals(ms.getArities(new String[] { "painter", "favoriteColor" }), new int[] { 1, 1 });
    assertArrayEquals(ms.getArities(new String[] { "painter", "painting" }), new int[] { 1, 2 });
  }

}
