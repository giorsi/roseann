/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

import uk.ac.ox.cs.diadem.env.ParserException;
import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 * 
 */
public class ModelParserTest extends StandardTestcase {

  @Test
  public void testParseTerm() throws ParserException, IOException {
    final Model model = ModelFactory.createModel();
    ModelParser parser = new ModelParser();

    parser.init("[xyz,12]",model);
    assertEquals(parser.parseTerm().toString(), "[xyz,12]");

    parser.init("[xyz, 12]",model);
    assertEquals(parser.parseTerm().toString(), "[xyz,12]");

    parser.init("  [   xyz, 12]",model);
    assertEquals(parser.parseTerm().toString(), "[xyz,12]");

    parser.init(" [xyz, 12]",model);
    assertEquals(parser.parseTerm().toString(), "[xyz,12]");

    parser.init(" []",model);
    assertEquals(parser.parseTerm().toString(), "[]");

    parser.init(" [xyz,[],[aa], 12]",model);
    assertEquals(parser.parseTerm().toString(), "[xyz,[],[aa],12]");

    parser.init(" [xyz,[],[aa], a(12)]",model);
    assertEquals(parser.parseTerm().toString(), "[xyz,[],[aa],a(12)]");

    parser.init(" \"xxx\"",model);
    assertEquals(parser.parseTerm().toString(), "\"xxx\"");
  }

  @Test(expected = ParserException.class)
  public void testParseTermEmptyFunctionTerm() throws ParserException, IOException {
    final Model model = ModelFactory.createModel();
    ModelParser parser = new ModelParser();

    parser.init(" [xyz,[],[aa], a()]",model);
    parser.parseTerm();
  }

  @Test
  public void testParseAtom() throws ParserException, IOException, ModelSchemaException {
    final Model model = ModelFactory.createModel();
    ModelParser parser = new ModelParser();

    parser.init("a([xyz,12])",model);
    assertEquals(parser.parseAtom().toString(), "a([xyz,12])");

    parser.init("a(xyz,[],[aa], a(12))",model);
    assertEquals(parser.parseAtom().toString(), "a(xyz,[],[aa],a(12))");

    parser.init(" a ( \"xxx\"  )",model);
    assertEquals(parser.parseAtom().toString(), "a(\"xxx\")");
  }

  @Test
  public void testParseAtomWithDot() throws ParserException, IOException, ModelSchemaException {
    final Model model = ModelFactory.createModel();
    ModelParser parser = new ModelParser();

    parser.init("a([xyz,12]).",model);
    assertEquals(parser.parseAtomWithDot().toString(), "a([xyz,12])");

    parser.init("a(xyz,[],[aa], a(12)).",model);
    assertEquals(parser.parseAtomWithDot().toString(), "a(xyz,[],[aa],a(12))");

    parser.init(" a ( \"xxx\"  ).",model);
    assertEquals(parser.parseAtomWithDot().toString(), "a(\"xxx\")");
  }

  @Test
  public void testParseAtomWithNewlines() throws ParserException, IOException, ModelSchemaException {
    final Model model = ModelFactory.createModel();
    ModelParser parser = new ModelParser();

    parser.init("a([xyz,\n12]).",model);
    assertEquals(parser.parseAtomWithDot().toString(), "a([xyz,12])");

    parser.init(" a ( \"xxx\nbla\"  ).",model);
    assertEquals(parser.parseAtomWithDot().toString(), "a(\"xxx\nbla\")");
  }

}
