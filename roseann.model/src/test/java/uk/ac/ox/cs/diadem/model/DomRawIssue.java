/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.model;

import java.io.InputStreamReader;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;

/**
 * @author timfu
 */
public class DomRawIssue {
  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(DomRawIssue.class);
  @SuppressWarnings("unused")
  private static final Configuration config = ConfigurationFacility.getConfiguration();

  @Test
  public final void test() {
    final Model m = ModelFactory.createIsolatedModel();

    m.readHierarchically(new InputStreamReader(getClass().getResourceAsStream("p0_s0.dom.raw")));
  }
}
