/**
 *
 */
package uk.ac.ox.cs.diadem.model;

import org.junit.Test;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 */
public class ModelImplementationTest extends ModelTest {

  @Override
  public Model getModel() {
    return new ModelImplementation();
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getStringLiteral(java.lang.String)}.
   */
  @Test
  public void testGetStringLiteralWithCounting() {
    final ModelImplementation modelImpl = new ModelImplementation();
    assert modelImpl.getNumStringLiterals() == 0;
    final StringLiteral sLHallo = modelImpl.getStringLiteral("hallo");
    assert sLHallo != null;
    assert sLHallo.getString().contentEquals("hallo");
    assert modelImpl.getNumStringLiterals() == 1;

    final StringLiteral sLHola = modelImpl.getStringLiteral("hola");
    assert sLHola != null;
    assert sLHola.getString().contentEquals("hola");
    assert sLHallo != sLHola;
    assert modelImpl.getNumStringLiterals() == 2;

    final StringLiteral sLHola2 = modelImpl.getStringLiteral("hola");
    assert sLHola2 != null;
    assert sLHola2.getString().contentEquals("hola");
    assert sLHola == sLHola2;
    assert modelImpl.getNumStringLiterals() == 2;

    final StringLiteral sLhallo2 = modelImpl.getStringLiteral("hallo");
    assert sLhallo2 != null;
    assert sLhallo2.getString().contentEquals("hallo");
    assert sLHallo == sLhallo2;
    assert modelImpl.getNumStringLiterals() == 2;
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getIntegerLiteral(int)}.
   * @throws ModelSchemaException
   */
  @Test
  public void testGetIntegerLiteralWithCounting() throws ModelSchemaException {
    final ModelImplementation modelImpl = new ModelImplementation();
    assert modelImpl.getNumIntegerLiterals() == 0;
    final IntegerLiteral iLOne = modelImpl.getIntegerLiteral(1);
    assert iLOne != null;
    assert iLOne.getInteger() == 1;
    assert modelImpl.getNumIntegerLiterals() == 1;

    final IntegerLiteral iLTwo = modelImpl.getIntegerLiteral(2);
    assert iLTwo != null;
    assert iLTwo.getInteger() == 2;
    assert iLOne != iLTwo;
    assert modelImpl.getNumIntegerLiterals() == 2;

    final IntegerLiteral iLTwo2 = modelImpl.getIntegerLiteral(2);
    assert iLTwo2 != null;
    assert iLTwo2.getInteger() == 2;
    assert iLTwo == iLTwo2;
    assert modelImpl.getNumIntegerLiterals() == 2;

    final IntegerLiteral iLOne2 = modelImpl.getIntegerLiteral(1);
    assert iLOne2 != null;
    assert iLOne2.getInteger() == 1;
    assert iLOne == iLOne2;
    assert modelImpl.getNumIntegerLiterals() == 2;

    for (int i = 0; i < 100; ++i) {
      final IntegerLiteral iL = modelImpl.getIntegerLiteral(i);
      assert iL.getInteger() == i;
      for (int j = 0; j < i; ++j) {
        // let's not debate efficency here
        assert iL != modelImpl.getIntegerLiteral(j);
      }
    }
    assert modelImpl.getNumIntegerLiterals() == 100;
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getConstantTerm(java.lang.String)}.
   */
  @Test
  public void testGetConstantTermWithCounting() {
    final ModelImplementation modelImpl = new ModelImplementation();
    assert modelImpl.getNumConstantTerms() == 0;
    final ConstantTerm sLHallo = modelImpl.getConstantTerm("hallo");
    assert sLHallo != null;
    assert sLHallo.getId().contentEquals("hallo");
    assert modelImpl.getNumConstantTerms() == 1;

    final ConstantTerm sLHola = modelImpl.getConstantTerm("hola");
    assert sLHola != null;
    assert sLHola.getId().contentEquals("hola");
    assert sLHallo != sLHola;
    assert modelImpl.getNumConstantTerms() == 2;

    final ConstantTerm sLHola2 = modelImpl.getConstantTerm("hola");
    assert sLHola2 != null;
    assert sLHola2.getId().contentEquals("hola");
    assert sLHola == sLHola2;
    assert modelImpl.getNumConstantTerms() == 2;

    final ConstantTerm sLhallo2 = modelImpl.getConstantTerm("hallo");
    assert sLhallo2 != null;
    assert sLhallo2.getId().contentEquals("hallo");
    assert sLHallo == sLhallo2;
    assert modelImpl.getNumConstantTerms() == 2;
  }

  /**
   * Test method for
   * {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getCompoundTerm(java.lang.String, uk.ac.ox.cs.diadem.model.Term[])}
   * .
   */
  @Test
  public void testGetCompoundTermWithCounting() {
    final ModelImplementation modelImpl = new ModelImplementation();
    final ConstantTerm sLHello = modelImpl.getConstantTerm("hello");
    final ConstantTerm sLHallo = modelImpl.getConstantTerm("hallo");
    final ConstantTerm sLHola = modelImpl.getConstantTerm("hola");

    assert modelImpl.getNumCompoundTerms() == 0;
    final CompoundTerm cT1 = modelImpl.getCompoundTerm("germanSpanish", sLHallo, sLHola);
    assert cT1.getId().contentEquals("germanSpanish");
    assert cT1.getSubTerms().length == 2;
    assert cT1.getSubTerms()[0] == sLHallo;
    assert cT1.getSubTerms()[1] == sLHola;
    assert modelImpl.getNumCompoundTerms() == 1;
    assert modelImpl.getNumCompoundTerms("germanSpanish") == 1;
    assert modelImpl.getNumCompoundTerms("germanSpanishXXX") == 0;

    final CompoundTerm cT1prime = modelImpl.getCompoundTerm("germanSpanish", sLHallo, sLHola);
    assert cT1prime.getId().contentEquals("germanSpanish");
    assert cT1prime.getSubTerms().length == 2;
    assert cT1prime.getSubTerms()[0] == sLHallo;
    assert cT1prime.getSubTerms()[1] == sLHola;
    assert modelImpl.getNumCompoundTerms() == 1;
    assert modelImpl.getNumCompoundTerms("germanSpanish") == 1;
    assert modelImpl.getNumCompoundTerms("germanSpanishXXX") == 0;

    assert cT1 == cT1prime;

    final CompoundTerm cT2 = modelImpl.getCompoundTerm("englishGerman", sLHello, sLHallo);
    assert cT2.getId().contentEquals("englishGerman");
    assert cT2.getSubTerms().length == 2;
    assert cT2.getSubTerms()[0] == sLHello;
    assert cT2.getSubTerms()[1] == sLHallo;
    assert modelImpl.getNumCompoundTerms() == 2;
    assert modelImpl.getNumCompoundTerms("germanSpanish") == 1;
    assert modelImpl.getNumCompoundTerms("englishGerman") == 1;
    assert modelImpl.getNumCompoundTerms("germanSpanishXXX") == 0;

    final Term t1 = modelImpl.getIntegerLiteral(1);
    final Term tOne = modelImpl.getStringLiteral("one");
    final Term tEins = modelImpl.getStringLiteral("eins");

    final CompoundTerm cT3 = modelImpl.getCompoundTerm("englishGerman", t1, t1);
    assert cT3.getId().contentEquals("englishGerman");
    assert cT3.getSubTerms().length == 2;
    assert cT3.getSubTerms()[0] == t1;
    assert cT3.getSubTerms()[1] == t1;
    assert modelImpl.getNumCompoundTerms() == 3;
    assert modelImpl.getNumCompoundTerms("germanSpanish") == 1;
    assert modelImpl.getNumCompoundTerms("englishGerman") == 2;
    assert modelImpl.getNumCompoundTerms("germanSpanishXXX") == 0;

    final CompoundTerm cT4 = modelImpl.getCompoundTerm("englishGerman", tOne, tEins);
    assert cT4.getId().contentEquals("englishGerman");
    assert cT4.getSubTerms().length == 2;
    assert cT4.getSubTerms()[0] == tOne;
    assert cT4.getSubTerms()[1] == tEins;
    assert modelImpl.getNumCompoundTerms() == 4;
    assert modelImpl.getNumCompoundTerms("germanSpanish") == 1;
    assert modelImpl.getNumCompoundTerms("englishGerman") == 3;
    assert modelImpl.getNumCompoundTerms("germanSpanishXXX") == 0;

    assert cT1 == modelImpl.getCompoundTerm("germanSpanish", sLHallo, sLHola);
    assert cT2 == modelImpl.getCompoundTerm("englishGerman", sLHello, sLHallo);
    assert cT3 == modelImpl.getCompoundTerm("englishGerman", t1, t1);
    assert cT4 == modelImpl.getCompoundTerm("englishGerman", tOne, tEins);

    final CompoundTerm cT5 = modelImpl.getCompoundTerm("germanSpanish", t1, t1);
    assert cT5.getId().contentEquals("germanSpanish");
    assert cT5.getSubTerms().length == 2;
    assert cT5.getSubTerms()[0] == t1;
    assert cT5.getSubTerms()[1] == t1;
    assert modelImpl.getNumCompoundTerms() == 5;
    assert modelImpl.getNumCompoundTerms("germanSpanish") == 2;
    assert modelImpl.getNumCompoundTerms("englishGerman") == 3;
    assert modelImpl.getNumCompoundTerms("germanSpanishXXX") == 0;
  }

  /**
   * Test method for
   * {@link uk.ac.ox.cs.diadem.model.ModgetPredicelImplementation#getListTerm(uk.ac.ox.cs.diadem.model.Term[])}.
   */
  @Test
  public void testGetListTermWithCounting() {
    final ModelImplementation modelImpl = new ModelImplementation();
    final ConstantTerm sLHello = modelImpl.getConstantTerm("hello");
    final ConstantTerm sLHallo = modelImpl.getConstantTerm("hallo");
    final ConstantTerm sLHola = modelImpl.getConstantTerm("hola");

    assert modelImpl.getNumListTerms() == 0;
    final ListTerm cT1 = modelImpl.getListTerm(sLHallo, sLHola);
    assert cT1.getSubTerms().length == 2;
    assert cT1.getSubTerms()[0] == sLHallo;
    assert cT1.getSubTerms()[1] == sLHola;
    assert modelImpl.getNumListTerms() == 1;

    final ListTerm cT1prime = modelImpl.getListTerm(sLHallo, sLHola);
    assert cT1prime.getSubTerms().length == 2;
    assert cT1prime.getSubTerms()[0] == sLHallo;
    assert cT1prime.getSubTerms()[1] == sLHola;
    assert modelImpl.getNumListTerms() == 1;

    assert cT1 == cT1prime;

    final ListTerm cT2 = modelImpl.getListTerm(sLHello, sLHallo);
    assert cT2.getSubTerms().length == 2;
    assert cT2.getSubTerms()[0] == sLHello;
    assert cT2.getSubTerms()[1] == sLHallo;
    assert modelImpl.getNumListTerms() == 2;

    final Term t1 = modelImpl.getIntegerLiteral(1);
    final Term tOne = modelImpl.getStringLiteral("one");
    final Term tEins = modelImpl.getStringLiteral("eins");

    final ListTerm cT3 = modelImpl.getListTerm(t1, t1);
    assert cT3.getSubTerms().length == 2;
    assert cT3.getSubTerms()[0] == t1;
    assert cT3.getSubTerms()[1] == t1;
    assert modelImpl.getNumListTerms() == 3;

    final ListTerm cT4 = modelImpl.getListTerm(tOne, tEins);
    assert cT4.getSubTerms().length == 2;
    assert cT4.getSubTerms()[0] == tOne;
    assert cT4.getSubTerms()[1] == tEins;
    assert modelImpl.getNumListTerms() == 4;

    assert cT1 == modelImpl.getListTerm(sLHallo, sLHola);
    assert cT2 == modelImpl.getListTerm(sLHello, sLHallo);
    assert cT3 == modelImpl.getListTerm(t1, t1);
    assert cT4 == modelImpl.getListTerm(tOne, tEins);

    final ListTerm cT5 = modelImpl.getListTerm(t1, t1);
    assert cT5.getSubTerms().length == 2;
    assert cT5.getSubTerms()[0] == t1;
    assert cT5.getSubTerms()[1] == t1;
    assert modelImpl.getNumListTerms() == 4;
    assert cT5 == cT3;
  }
}
