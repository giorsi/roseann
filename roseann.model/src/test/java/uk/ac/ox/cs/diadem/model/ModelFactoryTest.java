package uk.ac.ox.cs.diadem.model;

import static org.junit.Assert.*;

import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;

public class ModelFactoryTest extends StandardTestcase {

	@Test
	public void testCreation() {
		Model m1=ModelFactory.createModel();
		Model m2=ModelFactory.createModel();
		assertNotSame(m1,m2);
		assertTrue(m1.hasSameContext(m2));
		assertTrue(m2.hasSameContext(m1));
		
		Model m3=ModelFactory.createIsolatedModel();
		assertNotSame(m1,m3);
		assertFalse(m1.hasSameContext(m3));
		assertFalse(m2.hasSameContext(m3));
		assertFalse(m3.hasSameContext(m1));
		assertFalse(m3.hasSameContext(m2));
	}

}
