/**
 * 
 */
package uk.ac.ox.cs.diadem.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Iterator;
import java.util.Set;

import org.junit.Ignore;
import org.junit.Test;

import uk.ac.ox.cs.diadem.env.ParserException;
import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;

/**
 * @author Christian Schallhart <christian.schallhart@comlab.ox.ac.uk>
 */
public abstract class ModelTest extends StandardTestcase {

  abstract protected Model getModel();

  private Model getSmallModel() {
    final Model model = getModel();

    int size = 0;
    for (int i = 0; i < 3; ++i) {
      assertEquals(model.sizeOfPredicate("a"), i);
      model.add("a", model.getIntegerLiteral(i));
      ++size;
      assertEquals(model.size(), size);
      assertEquals(model.sizeHierarchically(), size);
      assertEquals(model.sizeOfPredicate("a"), i + 1);
      for (int j = 0; j < 3; ++j) {
        model.add("b", model.getIntegerLiteral(i), model.getIntegerLiteral(j));
        ++size;
        assertEquals(model.size(), size);
        assertEquals(model.sizeOfPredicate("b"), (i * 3) + j + 1);
      }
    }

    for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 3; ++j) {
        model.add("a", model.getCompoundTerm("b", model.getIntegerLiteral(i), model.getIntegerLiteral(j)));
        ++size;
        assertEquals(model.size(), size);
        assertEquals(model.sizeHierarchically(), size);
      }
    }

    return model;
  }

  private Model getLargeModel() { // $codepro.audit.disable blockDepth
    final Model model = getModel();

    int size = 0;
    for (int i = 0; i < 10; ++i) {
      assertEquals(model.sizeOfPredicate("a"), i);
      model.add("a", model.getIntegerLiteral(i));
      ++size;
      assertEquals(model.size(), size);
      assertEquals(model.sizeHierarchically(), size);
      assertEquals(model.sizeOfPredicate("a"), i + 1);
      for (int j = 0; j < 10; ++j) {
        model.add("b", model.getIntegerLiteral(i), model.getIntegerLiteral(j));
        ++size;
        assertEquals(model.size(), size);
        assertEquals(model.sizeHierarchically(), size);
        assertEquals(model.sizeOfPredicate("b"), (i * 10) + j + 1);
        for (int k = 0; k < 10; ++k) {
          model.add("c", model.getIntegerLiteral(i), model.getIntegerLiteral(j), model.getIntegerLiteral(k));
          ++size;
          assertEquals(model.size(), size);
          assertEquals(model.sizeHierarchically(), size);
          assertEquals(model.sizeOfPredicate("c"), (i * 100) + (j * 10) + k + 1);
          for (int l = 0; l < 10; ++l) {
            model.add("d", model.getIntegerLiteral(i), model.getIntegerLiteral(j), model.getIntegerLiteral(k),
                model.getIntegerLiteral(l));
            ++size;
            assertEquals(model.size(), size);
            assertEquals(model.sizeHierarchically(), size);
            assertEquals(model.sizeOfPredicate("d"), (i * 1000) + (j * 100) + (k * 10) + l + 1);
          }
        }
      }
    }
    return model;
  }

  private Model getHierarhicalModel(final int base) {
    final Model model = getModel();

    for (int i = 0; i < base; ++i) {
      assertEquals(model.sizeOfPredicate("a"), i);
      assertEquals(model.size(), i);
      final Atom atomA = model.add("a", model.getIntegerLiteral(i));
      assertEquals(model.sizeOfPredicate("a"), i + 1);
      assertEquals(model.size(), i + 1);
      assertEquals(model.sizeHierarchically(), 1 + (i * (1 + (base * (1 + (base * (1 + base)))))));
      final Model submodelA = model.getSubmodel(atomA);
      for (int j = 0; j < base; ++j) {
        final Atom atomB = submodelA.add("b", submodelA.getIntegerLiteral(i), submodelA.getIntegerLiteral(j));
        assertEquals(submodelA.sizeOfPredicate("b"), j + 1);
        assertEquals(submodelA.size(), j + 1);
        assertEquals(submodelA.sizeHierarchically(), 1 + (j * (1 + (base * (1 + base)))));
        assertEquals(model.sizeHierarchically(), 1 + (i * (1 + (base * (1 + (base * (1 + base)))))) + 1
            + (j * (1 + (base * (1 + base)))));
        final Model submodelB = submodelA.getSubmodel(atomB);
        for (int k = 0; k < base; ++k) {
          final Atom atomC = submodelB.add("c", submodelB.getIntegerLiteral(i), submodelB.getIntegerLiteral(j),
              submodelB.getIntegerLiteral(k));
          assertEquals(submodelB.sizeOfPredicate("c"), k + 1);
          assertEquals(submodelB.size(), k + 1);
          assertEquals(submodelB.sizeHierarchically(), 1 + (k * (1 + base)));
          assertEquals(model.sizeHierarchically(), 1 + (i * (1 + (base * (1 + (base * (1 + base)))))) + 1
              + (j * (1 + (base * (1 + base)))) + 1 + (k * (1 + base)));
          final Model submodelC = submodelB.getSubmodel(atomC);
          for (int l = 0; l < base; ++l) {
            submodelC.add("d", submodelC.getIntegerLiteral(i), submodelC.getIntegerLiteral(j),
                submodelC.getIntegerLiteral(k), submodelC.getIntegerLiteral(l));
            assertEquals(submodelC.sizeOfPredicate("d"), l + 1);
            assertEquals(submodelC.size(), l + 1);
            assertEquals(submodelC.sizeHierarchically(), 1 + l);
            assertEquals(model.sizeHierarchically(), 1 + (i * (1 + (base * (1 + (base * (1 + base)))))) + 1
                + (j * (1 + (base * (1 + base)))) + 1 + (k * (1 + base)) + 1 + l);
          }
        }
      }
    }
    return model;
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testRemoveByMask() {
    final Model model = getSmallModel();

    model.removeByMask("a", model.getIntegerLiteral(1));
    model.removeByMask("a", model.getIntegerLiteral(0));
    assertTrue(database.checkSorted("remove-1", model.toString()));
    model.removeByPrefixMask("a");
    assertTrue(database.checkSorted("remove-2", model.toString()));
    assertTrue(database.checkSorted("preds", model.getPredicateNameSet().toString()));

  }

  @SuppressWarnings("unchecked")
  @Test
  public void testEscapingInStringLiterals() throws ParserException, ModelAccessException {
    final Model model = getModel();
    model.addByParsing("huhu(\"ein toller \u0003 string.\")");

    assertTrue(database.check("escaping-1", model.toString()));
    assertTrue(database.check("escaping-2", model.getUniqueTermByPrefixMask(0, "huhu").getValue()));
  }

  @Test
  // $codepro.audit.disable blockDepth
  public void testCreateWithContextAndAtoms() {
    final Model model1 = getLargeModel();
    final Model model2 = model1.create(true, true);

    for (int i = 0; i < 10; ++i) {
      assertTrue(model1.get("a", model1.getIntegerLiteral(i)) == model2.get("a", model1.getIntegerLiteral(i)));
      for (int j = 0; j < 10; ++j) {
        assertTrue(model1.get("a", model1.getIntegerLiteral(i), model1.getIntegerLiteral(j)) == model2.get("a",
            model1.getIntegerLiteral(i), model1.getIntegerLiteral(j)));
        for (int k = 0; k < 10; ++k) {
          assertTrue(model1.get("a", model1.getIntegerLiteral(i), model1.getIntegerLiteral(j),
              model1.getIntegerLiteral(k)) == model2.get("a", model1.getIntegerLiteral(i), model1.getIntegerLiteral(j),
              model1.getIntegerLiteral(k)));
          for (int l = 0; l < 10; ++l) {
            assertTrue(model1.get("a", model1.getIntegerLiteral(i), model1.getIntegerLiteral(j),
                model1.getIntegerLiteral(k), model1.getIntegerLiteral(l)) == model2.get("a",
                model1.getIntegerLiteral(j), model1.getIntegerLiteral(j), model1.getIntegerLiteral(k),
                model1.getIntegerLiteral(l)));
          }
        }
      }
    }
  }

  @Test
  public void testCreateWithContextAndAtoms2() {
    final Model model1 = getLargeModel();
    final Model model2 = model1.create(true, true); // preserve context and atoms

    // on both sides the same
    assertTrue(model1.getIntegerLiteral(1) == model2.getIntegerLiteral(1));
    assertTrue(model1.getStringLiteral("a") == model2.getStringLiteral("a"));
    assertTrue(model1.getConstantTerm("a") == model2.getConstantTerm("a"));
    assertTrue(model1.getCompoundTerm("a", model1.getIntegerLiteral(1)) == model2.getCompoundTerm("a",
        model1.getIntegerLiteral(1)));
    assertTrue(model1.getListTerm(model1.getIntegerLiteral(1)) == model2.getListTerm(model1.getIntegerLiteral(1)));
    assertTrue(model1.add("a", model1.getIntegerLiteral(1)) == model2.add("a", model1.getIntegerLiteral(1)));
    // also, if newly created
    assertTrue(model1.getIntegerLiteral(1000) == model2.getIntegerLiteral(1000));
    assertTrue(model1.add("aa", model1.getIntegerLiteral(1)) == model2.add("aa", model1.getIntegerLiteral(1)));

    model2.clear(); // atoms in model2 are flushed, context stays

    // still, everything can be compared by ===
    assertTrue(model1.getIntegerLiteral(1) == model2.getIntegerLiteral(1));
    assertTrue(model1.getStringLiteral("a") == model2.getStringLiteral("a"));
    assertTrue(model1.getConstantTerm("a") == model2.getConstantTerm("a"));
    assertTrue(model1.getCompoundTerm("a", model1.getIntegerLiteral(1)) == model2.getCompoundTerm("a",
        model1.getIntegerLiteral(1)));
    assertTrue(model1.getListTerm(model1.getIntegerLiteral(1)) == model2.getListTerm(model1.getIntegerLiteral(1)));
    assertTrue(model1.add("a", model1.getIntegerLiteral(1)) == model2.add("a", model1.getIntegerLiteral(1)));
    // also if newly created
    assertTrue(model1.getIntegerLiteral(100) == model2.getIntegerLiteral(100));
    assertTrue(model1.add("aaa", model1.getIntegerLiteral(1)) == model2.add("aaa", model1.getIntegerLiteral(1)));
  }

  // TODO this test breaks with maven. don't know why.
  @Ignore
  @Test
  public void testCreateWithoutContextAndAtoms() {
    final Model model1 = getLargeModel();
    final Model model2 = model1.create(false, true); // drop context but preserve atoms
    assertEquals(model1.toString(), model2.toString());
    assertTrue(database.checkSorted("createWithoutContextAndAtoms", model2.toString()));

    // preexisting atoms and literals are shared
    // (the interface does not guarantee that)
    assertTrue(model1.getIntegerLiteral(1) == model2.getIntegerLiteral(1));
    // new ones are not
    // (the interface does not guarantee that)
    assertTrue(model1.getStringLiteral("a") != model2.getStringLiteral("a"));
    assertTrue(model1.getConstantTerm("a") != model2.getConstantTerm("a"));
    assertTrue(model1.getCompoundTerm("a", model1.getIntegerLiteral(1)) != model2.getCompoundTerm("a",
        model1.getIntegerLiteral(1)));
    assertTrue(model1.getListTerm(model1.getIntegerLiteral(1)) != model2.getListTerm(model1.getIntegerLiteral(1)));
    assertTrue(model1.add("a", model1.getIntegerLiteral(1)) == model2.add("a", model1.getIntegerLiteral(1)));
    assertTrue(model1.getIntegerLiteral(1000) != model2.getIntegerLiteral(1000));
    // newly created atoms differ
    assertFalse(model1.add("aa", model1.getIntegerLiteral(1)) == model2.add("aa", model1.getIntegerLiteral(1)));

    model2.clear(); // atoms in model2 are flushed, context stays

    // still, every preexisting term can be compared by ===
    assertTrue(model1.getIntegerLiteral(1) == model2.getIntegerLiteral(1));
    assertTrue(model1.getStringLiteral("a") != model2.getStringLiteral("a"));
    assertTrue(model1.getConstantTerm("a") != model2.getConstantTerm("a"));
    assertTrue(model1.getCompoundTerm("a", model1.getIntegerLiteral(1)) != model2.getCompoundTerm("a",
        model1.getIntegerLiteral(1)));
    assertTrue(model1.getListTerm(model1.getIntegerLiteral(1)) != model2.getListTerm(model1.getIntegerLiteral(1)));
    assertTrue(model1.getIntegerLiteral(100) != model2.getIntegerLiteral(100));
    // ALL atoms differ
    assertFalse(model1.add("a", model1.getIntegerLiteral(1)) == model2.add("a", model1.getIntegerLiteral(1)));
    assertFalse(model1.add("aaa", model1.getIntegerLiteral(1)) == model2.add("aaa", model1.getIntegerLiteral(1)));
  }

  @Test
  public void testCreateWithoutContextAndWithoutAtoms() {
    final Model model1 = getLargeModel();
    final Model model2 = model1.create(false, false); // drop context and atoms

    // everything is different now
    assertTrue(model1.getIntegerLiteral(1) != model2.getIntegerLiteral(1));
    assertTrue(model1.getStringLiteral("a") != model2.getStringLiteral("a"));
    assertTrue(model1.getConstantTerm("a") != model2.getConstantTerm("a"));
    assertTrue(model1.getCompoundTerm("a", model1.getIntegerLiteral(1)) != model2.getCompoundTerm("a",
        model1.getIntegerLiteral(1)));
    assertTrue(model1.getListTerm(model1.getIntegerLiteral(1)) != model2.getListTerm(model1.getIntegerLiteral(1)));
    assertTrue(model1.getIntegerLiteral(1000) != model2.getIntegerLiteral(1000));

    // preexisting atom differ
    assertFalse(model1.add("a", model1.getIntegerLiteral(1)) == model2.add("a", model1.getIntegerLiteral(1)));

    // newly created atoms differ
    assertFalse(model1.add("aa", model1.getIntegerLiteral(1)) == model2.add("aa", model1.getIntegerLiteral(1)));
  }

  @Test
  public void testCreateWithContextAndFilter() throws ParserException {
    final Model model1 = getLargeModel();
    @SuppressWarnings("unchecked")
    final Model model2 = model1.create(true, "b", null, model1.getIntegerLiteral(2));
    assertTrue(database.checkSorted("createwithcontextandfilter1", model2.toString()));

    for (final Atom atom : model2) {
      assertTrue(model1.contains(atom));
    }

    final Atom a1 = model2.addByParsing("a(2)");
    model2.add(a1);
    assertTrue(model1.contains(a1));
  }

  @Test(expected = AssertionError.class)
  public void testCreateWithoutContextAndFilter() throws ParserException {
    final Model model1 = getLargeModel();
    @SuppressWarnings("unchecked")
    final Model model2 = model1.create(false, "b", null, model1.getIntegerLiteral(2));
    assertTrue(database.checkSorted("createwithoutcontextandfilter1", model2.toString()));

    // CAREFUL: THIS RELIES ON WHITE BOX KNOWLEDGE

    final Atom a1 = model2.addByParsing("a(2)");
    assertTrue(model2.contains(a1));
    // the next assert holds, since we copy the terms.
    // this is an implementation internal decision, the
    // behaviour wrt to the assert below is undefined in the
    // interface
    assertSame(model1.getIntegerLiteral(2), a1.getSubTerm(0));

    // implementation defined: model2 could use the same atoms
    // as model1 -- but right now it does not
    final Atom a2 = model1.addByParsing("a(2)");
    assertNotSame(a1, a2);

    // this call is implementation specific as well:
    // here we can catch the case that somebody checks
    // for containment with an atom, that does not belong
    // to the context of the model1
    assertTrue(model1.contains(a1));
  }

  @Test
  public void testMultipleModelsOnTheSameContext() {
    final Model model1 = getLargeModel();
    assertTrue(model1.isIsolated());
    final Model model2 = model1.create(true, false);
    assertFalse(model1.isIsolated());
    assertFalse(model2.isIsolated());
    assertTrue(model1.hasSameContext(model2));
    assertTrue(model2.hasSameContext(model1));

    assertTrue(model1.add("a") == model2.add("a"));

    final Model model3 = model2.create(true, true);
    assertFalse(model1.isIsolated());
    assertFalse(model2.isIsolated());
    assertFalse(model3.isIsolated());
    assertEquals(model3.size(), 1);
    assertTrue(model1.hasSameContext(model2));
    assertTrue(model2.hasSameContext(model1));
    assertTrue(model1.hasSameContext(model3));
    assertTrue(model3.hasSameContext(model1));
    assertTrue(model2.hasSameContext(model3));
    assertTrue(model3.hasSameContext(model2));

    model3.isolate();
    assertTrue(model1.hasSameContext(model2));
    assertTrue(model2.hasSameContext(model1));
    assertFalse(model1.hasSameContext(model3));
    assertFalse(model3.hasSameContext(model1));
    assertFalse(model2.hasSameContext(model3));
    assertFalse(model3.hasSameContext(model2));
    assertTrue(model3.isIsolated());
    assertTrue(model1.add("x") != model3.add("x"));

    model2.release();
    assertTrue(model1.isIsolated());
    assertTrue(model3.isIsolated());
    assertFalse(model1.hasSameContext(model3));
    assertFalse(model3.hasSameContext(model1));
  }

  @Test(expected = AssertionError.class)
  public void testInvalidIds() {
    final Model model = getModel();
    try {
      model.add("A", model.getIntegerLiteral(1));
      model.add("A_", model.getIntegerLiteral(1));
    } catch (final Exception e) {
      assertTrue(false);
    }
    model.add("_A_", model.getIntegerLiteral(1));
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getStringLiteral(java.lang.String)}.
   */
  @Test
  public void testGetStringLiteral() {
    final Model model = getModel();
    final StringLiteral sLHallo = model.getStringLiteral("hallo");
    assert sLHallo != null;
    assert sLHallo.getString().contentEquals("hallo");
    assert sLHallo.toString().contentEquals("\"hallo\"");

    final StringLiteral sLHola = model.getStringLiteral("hola");
    assert sLHola != null;
    assert sLHola.getString().contentEquals("hola");
    assert sLHola.toString().contentEquals("\"hola\"");
    assert sLHallo != sLHola;

    final StringLiteral sLHola2 = model.getStringLiteral("hola");
    assert sLHola2 != null;
    assert sLHola2.getString().contentEquals("hola");
    assert sLHola2.toString().contentEquals("\"hola\"");
    assert sLHola == sLHola2;

    final StringLiteral sLhallo2 = model.getStringLiteral("hallo");
    assert sLhallo2 != null;
    assert sLhallo2.getString().contentEquals("hallo");
    assert sLHallo.toString().contentEquals("\"hallo\"");
    assert sLHallo == sLhallo2;
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getStringLiteral(java.lang.String)}.
   */
  @Test(expected = AssertionError.class)
  public void testGetStringLiteralNull() {
    new ModelImplementation().getStringLiteral(null);
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getIntegerLiteral(int)}.
   */
  @Test
  public void testGetIntegerLiteral() {
    final Model model = getModel();
    final IntegerLiteral iLOne = model.getIntegerLiteral(1);
    assert iLOne != null;
    assert iLOne.getInteger() == 1;
    assert iLOne.toString().contentEquals("1");

    final IntegerLiteral iLTwo = model.getIntegerLiteral(2);
    assert iLTwo != null;
    assert iLTwo.getInteger() == 2;
    assert iLTwo.toString().contentEquals("2");
    assert iLOne != iLTwo;

    final IntegerLiteral iLTwo2 = model.getIntegerLiteral(2);
    assert iLTwo2 != null;
    assert iLTwo2.getInteger() == 2;
    assert iLTwo2.toString().contentEquals("2");
    assert iLTwo == iLTwo2;

    final IntegerLiteral iLOne2 = model.getIntegerLiteral(1);
    assert iLOne2 != null;
    assert iLOne2.getInteger() == 1;
    assert iLOne2.toString().contentEquals("1");
    assert iLOne == iLOne2;

    for (int i = 0; i < 100; ++i) {
      final IntegerLiteral iL = model.getIntegerLiteral(i);
      assert iL.getInteger() == i;
      for (int j = 0; j < i; ++j) {
        assert iL != model.getIntegerLiteral(j);
      }
    }
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getConstantTerm(java.lang.String)}.
   */
  @Test
  public void testGetConstantTerm() {
    final Model model = getModel();
    final ConstantTerm sLHallo = model.getConstantTerm("hallo");
    assert sLHallo != null;
    assert sLHallo.getId().contentEquals("hallo");
    assert sLHallo.toString().contentEquals("hallo");

    final ConstantTerm sLHola = model.getConstantTerm("hola");
    assert sLHola != null;
    assert sLHola.getId().contentEquals("hola");
    assert sLHola.toString().contentEquals("hola");
    assert sLHallo != sLHola;

    final ConstantTerm sLHola2 = model.getConstantTerm("hola");
    assert sLHola2 != null;
    assert sLHola2.getId().contentEquals("hola");
    assert sLHola2.toString().contentEquals("hola");
    assert sLHola == sLHola2;

    final ConstantTerm sLhallo2 = model.getConstantTerm("hallo");
    assert sLhallo2 != null;
    assert sLhallo2.getId().contentEquals("hallo");
    assert sLhallo2.toString().contentEquals("hallo");
    assert sLHallo == sLhallo2;
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getConstantTerm(java.lang.String)}.
   */
  @Test(expected = AssertionError.class)
  public void testGetConstantTermNull() {
    new ModelImplementation().getConstantTerm(null);
  }

  /**
   * Test method for
   * {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getCompoundTerm(java.lang.String, uk.ac.ox.cs.diadem.model.Term[])}
   * .
   */
  @Test
  public void testGetCompoundTerm() {
    final Model model = getModel();
    final ConstantTerm sLHello = model.getConstantTerm("hello");
    final ConstantTerm sLHallo = model.getConstantTerm("hallo");
    final ConstantTerm sLHola = model.getConstantTerm("hola");

    final CompoundTerm cT1 = model.getCompoundTerm("germanSpanish", sLHallo, sLHola);
    assert cT1.getId().contentEquals("germanSpanish");
    assert cT1.getSubTerms().length == 2;
    assert cT1.getSubTerms()[0] == sLHallo;
    assert cT1.getSubTerms()[1] == sLHola;
    assert cT1.getSubTerm(0) == sLHallo;
    assert cT1.getSubTerm(1) == sLHola;
    assertEquals(cT1.toString(), "germanSpanish(hallo,hola)");

    final CompoundTerm cT1prime = model.getCompoundTerm("germanSpanish", sLHallo, sLHola);
    assert cT1prime.getId().contentEquals("germanSpanish");
    assert cT1prime.getSubTerms().length == 2;
    assert cT1prime.getSubTerms()[0] == sLHallo;
    assert cT1prime.getSubTerms()[1] == sLHola;
    assert cT1prime.getSubTerm(0) == sLHallo;
    assert cT1prime.getSubTerm(1) == sLHola;
    assert cT1prime.toString().contentEquals("germanSpanish(hallo,hola)");

    assert cT1 == cT1prime;

    final CompoundTerm cT2 = model.getCompoundTerm("englishGerman", sLHello, sLHallo);
    assert cT2.getId().contentEquals("englishGerman");
    assert cT2.getSubTerms().length == 2;
    assert cT2.getSubTerms()[0] == sLHello;
    assert cT2.getSubTerms()[1] == sLHallo;
    assert cT2.getSubTerm(0) == sLHello;
    assert cT2.getSubTerm(1) == sLHallo;
    assert cT2.toString().contentEquals("englishGerman(hello,hallo)");

    final Term t1 = model.getIntegerLiteral(1);
    final Term tOne = model.getStringLiteral("one");
    final Term tEins = model.getStringLiteral("eins");

    final CompoundTerm cT3 = model.getCompoundTerm("englishGerman", t1, t1);
    assert cT3.getId().contentEquals("englishGerman");
    assert cT3.getSubTerms().length == 2;
    assert cT3.getSubTerms()[0] == t1;
    assert cT3.getSubTerms()[1] == t1;
    assert cT3.getSubTerm(0) == t1;
    assert cT3.getSubTerm(1) == t1;
    assert cT3.toString().contentEquals("englishGerman(1,1)");

    final CompoundTerm cT4 = model.getCompoundTerm("englishGerman", tOne, tEins);
    assert cT4.getId().contentEquals("englishGerman");
    assert cT4.getSubTerms().length == 2;
    assert cT4.getSubTerms()[0] == tOne;
    assert cT4.getSubTerms()[1] == tEins;
    assert cT4.getSubTerm(0) == tOne;
    assert cT4.getSubTerm(1) == tEins;
    assertEquals(cT4.toString(), "englishGerman(\"one\",\"eins\")");

    assert cT1 == model.getCompoundTerm("germanSpanish", sLHallo, sLHola);
    assert cT2 == model.getCompoundTerm("englishGerman", sLHello, sLHallo);
    assert cT3 == model.getCompoundTerm("englishGerman", t1, t1);
    assert cT4 == model.getCompoundTerm("englishGerman", tOne, tEins);

    final CompoundTerm cT5 = model.getCompoundTerm("germanSpanish", t1, t1);
    assert cT5.getId().contentEquals("germanSpanish");
    assert cT5.getSubTerms().length == 2;
    assert cT5.getSubTerms()[0] == t1;
    assert cT5.getSubTerms()[1] == t1;
    assert cT5.getSubTerm(0) == t1;
    assert cT5.getSubTerm(1) == t1;
    assert cT5.toString().contentEquals("germanSpanish(1,1)");
  }

  /**
   * Test method for
   * {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getCompoundTerm(java.lang.String, uk.ac.ox.cs.diadem.model.Term[])}
   * .
   */
  @Test(expected = AssertionError.class)
  public void testGetCompoundTermNullId() {
    final Model model = getModel();
    final ConstantTerm sLHello = model.getConstantTerm("hello");
    final ConstantTerm sLHallo = model.getConstantTerm("hallo");

    model.getCompoundTerm(null, sLHello, sLHallo);
  }

  @Test(expected = AssertionError.class)
  public void testCompoundTermsGetSubtermWrongIndex() {
    final Model model = getModel();
    final ConstantTerm sLHello = model.getConstantTerm("hello");
    final ConstantTerm sLHallo = model.getConstantTerm("hallo");

    final CompoundTerm cT1 = model.getCompoundTerm("bla", sLHello, sLHallo);
    cT1.getSubTerm(2);
  }

  /**
   * Test method for
   * {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getCompoundTerm(java.lang.String, uk.ac.ox.cs.diadem.model.Term[])}
   * .
   */
  @Test(expected = AssertionError.class)
  public void testGetCompoundTermNullSubTerm() {
    final Model model = getModel();
    final ConstantTerm sLHallo = model.getConstantTerm("hallo");

    model.getCompoundTerm("huhu", null, sLHallo);
  }

  /**
   * Test method for
   * {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getCompoundTerm(java.lang.String, uk.ac.ox.cs.diadem.model.Term[])}
   * .
   */
  @Test(expected = AssertionError.class)
  public void testGetCompoundTermNoSubTerms() {
    new ModelImplementation().getCompoundTerm("huhu");
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getListTerm(uk.ac.ox.cs.diadem.model.Term[])}.
   */
  @Test
  public void testGetListTerm() {
    final Model model = getModel();
    final ConstantTerm sLHello = model.getConstantTerm("hello");
    final ConstantTerm sLHallo = model.getConstantTerm("hallo");
    final ConstantTerm sLHola = model.getConstantTerm("hola");

    final ListTerm cT1 = model.getListTerm(sLHallo, sLHola);
    assert cT1.getSubTerms().length == 2;
    assert cT1.getSubTerms()[0] == sLHallo;
    assert cT1.getSubTerms()[1] == sLHola;
    assertEquals(cT1.toString(), "[hallo,hola]");

    final ListTerm cT1prime = model.getListTerm(sLHallo, sLHola);
    assert cT1prime.getSubTerms().length == 2;
    assert cT1prime.getSubTerms()[0] == sLHallo;
    assert cT1prime.getSubTerms()[1] == sLHola;
    assertEquals(cT1prime.toString(), "[hallo,hola]");

    assert cT1 == cT1prime;

    final ListTerm cT2 = model.getListTerm(sLHello, sLHallo, sLHola);
    assert cT2.getSubTerms().length == 3;
    assert cT2.getSubTerms()[0] == sLHello;
    assert cT2.getSubTerms()[1] == sLHallo;
    assert cT2.getSubTerms()[2] == sLHola;
    assertEquals(cT2.toString(), "[hello,hallo,hola]");

    final Term t1 = model.getIntegerLiteral(1);
    final Term tOne = model.getStringLiteral("one");
    final Term tEins = model.getStringLiteral("eins");

    final ListTerm cT3 = model.getListTerm(t1, t1);
    assert cT3.getSubTerms().length == 2;
    assert cT3.getSubTerms()[0] == t1;
    assert cT3.getSubTerms()[1] == t1;
    assertEquals(cT3.toString(), "[1,1]");

    final ListTerm cT4 = model.getListTerm(tOne, tEins);
    assert cT4.getSubTerms().length == 2;
    assert cT4.getSubTerms()[0] == tOne;
    assert cT4.getSubTerms()[1] == tEins;
    assertEquals(cT4.toString(), "[\"one\",\"eins\"]");

    assert cT1 == model.getListTerm(sLHallo, sLHola);
    assert cT2 == model.getListTerm(sLHello, sLHallo, sLHola);
    assert cT3 == model.getListTerm(t1, t1);
    assert cT4 == model.getListTerm(tOne, tEins);

    final ListTerm cT5 = model.getListTerm(t1, t1);
    assert cT5.getSubTerms().length == 2;
    assert cT5.getSubTerms()[0] == t1;
    assert cT5.getSubTerms()[1] == t1;
    assert cT5 == cT3;

    final ListTerm cT6 = model.getListTerm();
    assert cT6.getSubTerms().length == 0;
    assertEquals(cT6.toString(), "[]");
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getListTerm(uk.ac.ox.cs.diadem.model.Term[])}.
   */
  @Test(expected = AssertionError.class)
  public void testGetListTermNullSubTerm() {
    final Model model = getModel();
    final ConstantTerm sLHallo = model.getConstantTerm("hallo");

    model.getListTerm(null, sLHallo);
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getListTerm(uk.ac.ox.cs.diadem.model.Term[])}.
   */
  @Test(expected = AssertionError.class)
  @SuppressWarnings({ "all" })
  // want to test explicitly with null
  public void testGetListTermNullList1() {
    final Model model = getModel();
    model.getListTerm(null);
  }

  /**
   * Test method for {@link uk.ac.ox.cs.diadem.model.ModelImplementation#getListTerm(uk.ac.ox.cs.diadem.model.Term[])}.
   */
  @Test(expected = AssertionError.class)
  public void testGetListTermNullList2() {
    final Model model = getModel();
    model.getListTerm((Term[]) null);
  }

  /**
   * Test method for
   * {@link uk.ac.ox.cs.diadem.model.ModelImplementation#add(java.lang.String, uk.ac.ox.cs.diadem.model.Term[])}.
   * 
   * @throws IOException
   */
  @Test
  public void testGetAtom() throws IOException {
    final Model model = getModel();
    final ConstantTerm sLHello = model.getConstantTerm("hello");
    final ConstantTerm sLHallo = model.getConstantTerm("hallo");
    final ConstantTerm sLHola = model.getConstantTerm("hola");

    assert model.size() == 0;

    final Atom cT0 = model.add("true");
    assert cT0 == model.get("true");
    assert cT0.getId().contentEquals("true");
    assert cT0.getSubTerms().length == 0;
    assertEquals(cT0.toString(), "true");
    assert model.size() == 1;
    assert model.sizeOfPredicate("true") == 1;
    StringWriter sw = new StringWriter();
    cT0.write(sw);
    assertTrue(database.check("cT0", sw.toString()));

    final Atom cT1 = model.add("germanSpanish", sLHallo, sLHola);
    assert cT1 == model.get("germanSpanish", sLHallo, sLHola);
    assert cT1.getId().contentEquals("germanSpanish");
    assert cT1.getSubTerms().length == 2;
    assert cT1.getSubTerms()[0] == sLHallo;
    assert cT1.getSubTerms()[1] == sLHola;
    assertEquals(cT1.toString(), "germanSpanish(hallo,hola)");
    assert model.size() == 2;
    assert model.sizeOfPredicate("germanSpanish") == 1;
    assert model.sizeOfPredicate("germanSpanishXXX") == 0;
    sw = new StringWriter();
    cT1.write(sw);
    assertTrue(database.check("cT1", sw.toString()));

    final Atom cT1prime = model.add("germanSpanish", sLHallo, sLHola);
    assert cT1prime == model.get("germanSpanish", sLHallo, sLHola);
    assert cT1prime.getId().contentEquals("germanSpanish");
    assert cT1prime.getSubTerms().length == 2;
    assert cT1prime.getSubTerms()[0] == sLHallo;
    assert cT1prime.getSubTerms()[1] == sLHola;
    assertEquals(cT1prime.toString(), "germanSpanish(hallo,hola)");
    assert model.size() == 2;
    assert model.sizeOfPredicate("germanSpanish") == 1;
    assert model.sizeOfPredicate("germanSpanishXXX") == 0;
    sw = new StringWriter();
    cT1prime.write(sw);
    assertTrue(database.check("cT1", sw.toString()));

    assert cT1 == cT1prime;

    final Atom cT2 = model.add("englishGerman", sLHello, sLHallo);
    assert cT2 == model.get("englishGerman", sLHello, sLHallo);
    assert cT2.getId().contentEquals("englishGerman");
    assert cT2.getSubTerms().length == 2;
    assert cT2.getSubTerms()[0] == sLHello;
    assert cT2.getSubTerms()[1] == sLHallo;
    assert model.size() == 3;
    assertEquals(cT2.toString(), "englishGerman(hello,hallo)");
    assert model.sizeOfPredicate("germanSpanish") == 1;
    assert model.sizeOfPredicate("englishGerman") == 1;
    assert model.sizeOfPredicate("germanSpanishXXX") == 0;
    sw = new StringWriter();
    cT2.write(sw);
    assertTrue(database.check("cT2", sw.toString()));

    final Term t1 = model.getIntegerLiteral(1);
    final Term tOne = model.getStringLiteral("one");
    final Term tEins = model.getStringLiteral("eins");

    final Atom cT3 = model.add("englishGerman", t1, t1);
    assert cT3 == model.get("englishGerman", t1, t1);
    assert cT3.getId().contentEquals("englishGerman");
    assert cT3.getSubTerms().length == 2;
    assert cT3.getSubTerms()[0] == t1;
    assert cT3.getSubTerms()[1] == t1;
    assertEquals(cT3.toString(), "englishGerman(1,1)");
    assert model.size() == 4;
    assert model.sizeOfPredicate("germanSpanish") == 1;
    assert model.sizeOfPredicate("englishGerman") == 2;
    assert model.sizeOfPredicate("germanSpanishXXX") == 0;
    sw = new StringWriter();
    cT3.write(sw);
    assertTrue(database.check("cT3", sw.toString()));

    final Atom cT4 = model.add("englishGerman", tOne, tEins);
    assert cT4 == model.get("englishGerman", tOne, tEins);
    assert cT4.getId().contentEquals("englishGerman");
    assert cT4.getSubTerms().length == 2;
    assert cT4.getSubTerms()[0] == tOne;
    assert cT4.getSubTerms()[1] == tEins;
    assertEquals(cT4.toString(), "englishGerman(\"one\",\"eins\")");
    assert model.size() == 5;
    assert model.sizeOfPredicate("germanSpanish") == 1;
    assert model.sizeOfPredicate("englishGerman") == 3;
    assert model.sizeOfPredicate("germanSpanishXXX") == 0;
    sw = new StringWriter();
    cT4.write(sw);
    assertTrue(database.check("cT4", sw.toString()));

    assert cT1 == model.add("germanSpanish", sLHallo, sLHola);
    assert cT2 == model.add("englishGerman", sLHello, sLHallo);
    assert cT3 == model.add("englishGerman", t1, t1);
    assert cT4 == model.add("englishGerman", tOne, tEins);

    assert null == model.get("nope", sLHallo, sLHola);
    assert null == model.get("germanSpanish", sLHallo, sLHallo);
    assert null == model.get("unity");
    final Atom cTNoArg = model.add("unity");
    assert cTNoArg == model.get("unity");
    assertEquals(cTNoArg.toString(), "unity");

    final Atom cT5 = model.add("germanSpanish", t1, t1);
    assert cT5 == model.get("germanSpanish", t1, t1);
    assert cT5.getId().contentEquals("germanSpanish");
    assert cT5.getSubTerms().length == 2;
    assert cT5.getSubTerms()[0] == t1;
    assert cT5.getSubTerms()[1] == t1;
    assertEquals(cT5.toString(), "germanSpanish(1,1)");
    assert model.size() == 7;
    assert model.sizeOfPredicate("germanSpanish") == 2;
    assert model.sizeOfPredicate("englishGerman") == 3;
    assert model.sizeOfPredicate("germanSpanishXXX") == 0;
    sw = new StringWriter();
    cT5.write(sw);
    assertTrue(database.check("cT5", sw.toString()));

  }

  /**
   * Test method for
   * {@link uk.ac.ox.cs.diadem.model.ModelImplementation#add(java.lang.String, uk.ac.ox.cs.diadem.model.Term[])}.
   */
  @Test(expected = AssertionError.class)
  public void testGetAtomNullId() {
    final Model model = getModel();
    final ConstantTerm sLHello = model.getConstantTerm("hello");
    final ConstantTerm sLHallo = model.getConstantTerm("hallo");

    model.add(null, sLHello, sLHallo);
  }

  /**
   * Test method for
   * {@link uk.ac.ox.cs.diadem.model.ModelImplementation#add(java.lang.String, uk.ac.ox.cs.diadem.model.Term[])}.
   */
  @Test(expected = AssertionError.class)
  public void testGetAtomsNullSubTerm() {
    final Model model = getModel();
    final ConstantTerm sLHallo = model.getConstantTerm("hallo");

    model.add("huhu", null, sLHallo);
  }

  /**
   * Test method for
   * {@link uk.ac.ox.cs.diadem.model.ModelImplementation#add(java.lang.String, uk.ac.ox.cs.diadem.model.Term[])}.
   */
  @Test
  public void testBuildLargeModel() {
    getLargeModel();
  }

  /**
   * Test method for
   * {@link uk.ac.ox.cs.diadem.model.ModelImplementation#add(java.lang.String, uk.ac.ox.cs.diadem.model.Term[])}.
   */
  @Test
  public void testRemoveAtom() {
    final Model model = getLargeModel();
    final Term x1 = model.getIntegerLiteral(1);
    final Term x2 = model.getIntegerLiteral(2);
    final Term x3 = model.getIntegerLiteral(3);
    final Term x4 = model.getIntegerLiteral(4);

    int size = model.size();
    final int sizea = model.sizeOfPredicate("a");
    final int sizeb = model.sizeOfPredicate("b");
    int sizec = model.sizeOfPredicate("c");
    final int sized = model.sizeOfPredicate("d");

    assertEquals(size, 11110);
    assertEquals(sizea, 10);
    assertEquals(sizeb, 100);
    assertEquals(sizec, 1000);
    assertEquals(sized, 10000);
    assertTrue(model.get("c", x1, x2, x3) != null);
    model.remove("c", x1, x2, x3);
    assertTrue(model.get("c", x1, x2, x3) == null);

    --size;
    --sizec;
    assertEquals(size, model.size());
    assertEquals(sizea, model.sizeOfPredicate("a"));
    assertEquals(sizeb, model.sizeOfPredicate("b"));
    assertEquals(sizec, model.sizeOfPredicate("c"));
    assertEquals(sized, model.sizeOfPredicate("d"));

    model.remove("c", x1, x2, x3);
    assertEquals(size, model.size());
    assertEquals(sizea, model.sizeOfPredicate("a"));
    assertEquals(sizeb, model.sizeOfPredicate("b"));
    assertEquals(sizec, model.sizeOfPredicate("c"));
    assertEquals(sized, model.sizeOfPredicate("d"));

    model.remove("c", x1, x2, x3, x4);
    assertEquals(size, model.size());
    assertEquals(sizea, model.sizeOfPredicate("a"));
    assertEquals(sizeb, model.sizeOfPredicate("b"));
    assertEquals(sizec, model.sizeOfPredicate("c"));
    assertEquals(sized, model.sizeOfPredicate("d"));
  }

  /**
   * Test method for
   * {@link uk.ac.ox.cs.diadem.model.ModelImplementation#add(java.lang.String, uk.ac.ox.cs.diadem.model.Term[])}.
   */
  @Test
  public void testRemoveAtomFromAtom() {
    final Model model = getLargeModel();
    final Term x1 = model.getIntegerLiteral(1);
    final Term x2 = model.getIntegerLiteral(2);
    final Term x3 = model.getIntegerLiteral(3);

    int size = model.size();
    final int sizea = model.sizeOfPredicate("a");
    final int sizeb = model.sizeOfPredicate("b");
    int sizec = model.sizeOfPredicate("c");
    final int sized = model.sizeOfPredicate("d");

    assertEquals(size, 11110);
    assertEquals(sizea, 10);
    assertEquals(sizeb, 100);
    assertEquals(sizec, 1000);
    assertEquals(sized, 10000);
    final Atom atom = model.get("c", x1, x2, x3);
    assertTrue(atom != null);
    model.remove(atom);
    assertTrue(model.get("c", x1, x2, x3) == null);

    --size;
    --sizec;
    assertEquals(size, model.size());
    assertEquals(sizea, model.sizeOfPredicate("a"));
    assertEquals(sizeb, model.sizeOfPredicate("b"));
    assertEquals(sizec, model.sizeOfPredicate("c"));
    assertEquals(sized, model.sizeOfPredicate("d"));

    // create and delete
    model.remove(model.add("c", x1, x2, x3));
    assertEquals(size, model.size());
    assertEquals(sizea, model.sizeOfPredicate("a"));
    assertEquals(sizeb, model.sizeOfPredicate("b"));
    assertEquals(sizec, model.sizeOfPredicate("c"));
    assertEquals(sized, model.sizeOfPredicate("d"));
  }

  @Test
  public void testToString() throws ParserException {
    final Model smallModel = getSmallModel();
    final Model largeModel = getLargeModel();

    assertTrue(database.checkSorted("smallModel", smallModel.toString()));
    smallModel.remove(smallModel.addByParsing("a(0)"));
    assertTrue(database.checkSorted("smallModel-1", smallModel.toString()));
    smallModel.remove(smallModel.addByParsing("b(1,1)"));
    assertTrue(database.checkSorted("smallModel-2", smallModel.toString()));
    smallModel.add(smallModel.addByParsing("b(1,1,1,1)"));
    assertTrue(database.checkSorted("smallModel-2+1", smallModel.toString()));

    assertTrue(database.checkSorted("largeModel", largeModel.toString()));

  }

  @Test
  public void testParseTerm() throws ParserException {
    final Model smallModel = getSmallModel();

    final Term t1 = smallModel.parseTerm("1");
    assertTrue(t1 != null);
    final Term t2 = smallModel.parseTerm("\"1\"");
    assertTrue(t1 != t2);
    final Term t3 = smallModel.parseTerm("1");
    assertTrue(t1 == t3);
  }

  @Test
  public void testIterator() throws IOException {
    final Model model = getSmallModel();
    final StringWriter sw = new StringWriter();
    for (final Atom atom : model) {
      atom.write(sw);
      sw.append(".\n");
    }
    assertTrue(database.checkSorted("iterator", sw.toString()));
  }

  @Test
  public void testIteratorForPredicate() throws IOException {
    final Model model = getSmallModel();
    final StringWriter sw = new StringWriter();
    final Iterator<Atom> iter = model.iteratorForPredicate("b");
    while (iter.hasNext()) {
      iter.next().write(sw);
      sw.append(".\n");
    }
    assertTrue(database.checkSorted("iteratorforP", sw.toString()));
  }

  @Test
  public void testIteratorForMask() throws IOException {
    final Model model = getSmallModel();
    final StringWriter sw = new StringWriter();
    @SuppressWarnings("unchecked")
    final Iterator<Atom> iter = model.iteratorForMask("b", null, model.getIntegerLiteral(1));
    while (iter.hasNext()) {
      iter.next().write(sw);
      sw.append(".\n");
    }
    assertTrue(database.checkSorted("iteratorForM", sw.toString()));
  }

  @Test
  public void testIteratorForPrefixMask() throws IOException {
    final Model model = getSmallModel();
    final StringWriter sw = new StringWriter();
    @SuppressWarnings("unchecked")
    final Iterator<Atom> iter = model.iteratorForPrefixMask("b", model.getIntegerLiteral(1));
    while (iter.hasNext()) {
      iter.next().write(sw);
      sw.append(".\n");
    }
    assertTrue(database.checkSorted("iteratorForPM", sw.toString()));
  }

  @Test
  public void testAtomsForMask() throws IOException {
    final Model model = getSmallModel();
    final StringWriter sw = new StringWriter();
    @SuppressWarnings("unchecked")
    final Set<Atom> atoms = model.getAtomsByMask("b", null, model.getIntegerLiteral(1));
    for (final Atom atom : atoms) {
      atom.write(sw);
      sw.append(".\n");
    }
    assertTrue(database.checkSorted("AtomsForM", sw.toString()));
  }

  @Test
  public void testTermsForMask() throws IOException {
    final Model model = getSmallModel();
    final StringWriter sw = new StringWriter();
    @SuppressWarnings("unchecked")
    final Set<Term> terms = model.getTermsByMask(0, "b", null, model.getIntegerLiteral(1));
    for (final Term term : terms) {
      term.write(sw);
      sw.append('\n');
    }
    assertTrue(database.checkSorted("TermsForMask", sw.toString()));
  }

  @Test
  public void testContainsPredicate() {
    final Model model = getSmallModel();
    assertTrue(model.containsPredicate("a"));
    assertTrue(model.containsPredicate("b"));
    assertFalse(model.containsPredicate("aa"));
  }

  @Test
  public void testContainsAtom() {
    final Model model = getSmallModel();
    final Term t0 = model.getIntegerLiteral(0);
    final Term t1 = model.getIntegerLiteral(1);
    final Term t2 = model.getIntegerLiteral(2);
    final Term t3 = model.getIntegerLiteral(3);

    assertTrue(model.contains("b", t0, t0));
    assertTrue(model.contains("b", t2, t1));
    assertFalse(model.contains("b", t1, t3));
    assertFalse(model.contains("b", t1, t1, t3));
    assertFalse(model.contains("a", t1, t1));
    assertFalse(model.contains("b", t1));
    assertFalse(model.contains("a", t0, t0));

    final Atom b00 = model.add("b", t0, t0);
    final Atom b01 = model.add("b", t0, t1);

    assertTrue(model.contains(b00));
    assertTrue(model.contains(b01));
    model.remove(b00);
    assertFalse(model.contains(b00));
    assertTrue(model.contains(b01));
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testContainsAtomForMask() {
    final Model model = getSmallModel();
    final Term t0 = model.getIntegerLiteral(0);
    final Term t1 = model.getIntegerLiteral(1);
    final Term t2 = model.getIntegerLiteral(2);
    final Term t3 = model.getIntegerLiteral(3);

    assertTrue(model.containsMask("b", t0, t0));
    assertTrue(model.containsMask("b", t2, t1));
    assertFalse(model.containsMask("b", t1, t3));
    assertFalse(model.containsMask("b", t1, t1, t3));
    assertFalse(model.containsMask("a", t1, t1));
    assertFalse(model.containsMask("b", t1));
    assertFalse(model.containsMask("a", t0, t0));

    assertTrue(model.containsMask("b", t0, null));
    assertTrue(model.containsMask("b", null, null));
    assertTrue(model.containsMask("b", null, t0));

    assertTrue(model.containsMask("b", t2, null));
    assertTrue(model.containsMask("b", null, t2));

    assertFalse(model.containsMask("b", null, t3));
    assertFalse(model.containsMask("b", t3, null));
    assertFalse(model.containsMask("b", t2, null, null));
    assertFalse(model.containsMask("b", null, null, null));

    assertFalse(model.containsMask("a", null, null));
    assertFalse(model.containsMask("b", new Term[] { null }));
    assertFalse(model.containsMask("a", t0, null));

  }

  @Test
  public void testEquals() {
    final Model model = getSmallModel();
    assertTrue(model.equals(model.create(true, true)));
    assertFalse(model.equals(model.create(true, false)));
    assertFalse(model.equals(model.create(false, true)));
    assertFalse(model.equals(model.create(false, false)));

    final Model model1 = model.create(true, true);
    assertTrue(model.equals(model1));
    Atom atom = model1.add("huhu", model1.getConstantTerm("oha"));
    assertFalse(model.equals(model1));
    model1.remove(atom);
    assertTrue(model.equals(model1));
    atom = model.iterator().next();
    model1.remove(atom);
    assertFalse(model.equals(model1));
    model1.add(atom);
    assertTrue(model.equals(model1));

    assertFalse(model.equals(new Integer(1)));
    assertFalse(model.equals(null));
    assertEquals(model, model);
  }

  @Test
  public void testGetStatistics() {
    assertTrue(database.check("stats-E", getModel().getStatistics()));
    assertTrue(database.check("stats-S", getSmallModel().getStatistics()));
    assertTrue(database.check("stats-L", getLargeModel().getStatistics()));
  }

  @Test
  public void testAddCollection() {
    final Model model = getLargeModel();
    final Model model2 = model.create(true, false);
    assertFalse(model.equals(model2));
    model2.add(model.getAtomsByPredicate("d"));
    assertFalse(model.equals(model2));
    assertTrue(database.checkSorted("lage-d", model2.toString()));
    model2.add(model.getAtomsByPredicate("a"));
    assertFalse(model.equals(model2));
    model2.add(model.getAtomsByPredicate("b"));
    assertFalse(model.equals(model2));
    model2.add(model.getAtomsByPredicate("c"));
    assertTrue(database.checkSorted("lage-all", model2.toString()));
    assertTrue(database.checkSorted("lage-all", model.toString()));
    assertEquals(model, model2);
  }

  @Test
  public void testLoadSaveWithReaderWriterString() throws ModelIOException {
    final Model model = getSmallModel();

    StringWriter writer = new StringWriter();
    model.write(writer);
    final String dump = writer.toString();
    assertTrue(database.checkSorted("loadSave1", dump));

    final Model model1 = model.create(true, false);
    assertEquals(model1.size(), 0);
    writer = new StringWriter();
    model1.write(writer);
    assertTrue(database.checkSorted("loadSave-empty", writer.toString()));

    model1.read(new StringReader(dump));
    writer = new StringWriter();
    model1.write(writer);
    assertTrue(database.checkSorted("loadSave1", writer.toString()));

    final Model model2 = model.create(true, false);
    assertEquals(model2.size(), 0);
    model2.read(dump);
    writer = new StringWriter();
    model1.write(writer);
    assertTrue(database.checkSorted("loadSave1", writer.toString()));

  }

  @Test(expected = ModelIOException.class)
  public void testLoadNonExisting() throws ModelIOException {
    final Model model = getModel();

    model.read(new File("data/test/does-not-exist"));
  }

  @Test(expected = ModelIOException.class)
  public void testSaveNonExisting() throws ModelIOException {
    final Model model = getSmallModel();
    model.read(new File("data/test/dir-does-not-exist/bla.facts"));
  }

  private String toString(final Iterator<Atom> atoms) {
    final StringWriter writer = new StringWriter();
    while (atoms.hasNext()) {
      try {
        atoms.next().write(writer);
      } catch (final IOException e) {
        assert false : "Not RAM or what?!";
        e.printStackTrace();
      }
      writer.write('\n');
    }
    return writer.toString();
  }

  @Test
  public void testHierarchicalModelCreation() {
    final Model model = getModel();
    final Atom componentAtom = model.add("component", model.getIntegerLiteral(1));
    final Model submodel = model.getSubmodel(componentAtom);
    assert model.getIntegerLiteral(2) == submodel.getIntegerLiteral(2);

    submodel.add("huhu", submodel.getIntegerLiteral(2));
    final Atom subcomponentAtom = submodel.add("huhuDeep", submodel.getIntegerLiteral(2),
        submodel.getStringLiteral("hatschi"));
    submodel.add("blabla");
    final Model subsubmodel = submodel.getSubmodel(subcomponentAtom);

    for (int i = 0; i < 10; ++i) {
      subsubmodel.add("counter", subsubmodel.getIntegerLiteral(i));
    }
    subsubmodel.add("plok");
    assertTrue(database.checkSorted("1", model.toString()));
    assertTrue(database.checkSorted("2", submodel.toString()));
    assertTrue(database.checkSorted("3", subsubmodel.toString()));
    assertTrue(database.checkSorted("1-sm", toString(model.getSubmodelAtoms())));
    assertTrue(database.checkSorted("2-sm", toString(submodel.getSubmodelAtoms())));
    assertTrue(database.checkSorted("3-sm", toString(subsubmodel.getSubmodelAtoms())));
    assertSame(model, submodel.getParentModel());
    assertNull(model.getParentModel());
    assertSame(submodel, subsubmodel.getParentModel());

    {
      final Model model2 = model.create(true, true);
      assertTrue(database.checkSorted("1", model2.toString()));
      assertTrue(model2.hasSameContext(model));
      assertTrue(database.checkSorted("1-sm", toString(model2.getSubmodelAtoms())));
      final Model submodel2 = model2.getSubmodel(componentAtom);
      assertTrue(database.checkSorted("2", submodel2.toString()));
      assertTrue(submodel2.hasSameContext(submodel));
      assertTrue(database.checkSorted("2-sm", toString(submodel2.getSubmodelAtoms())));
      final Model subsubmodel2 = submodel2.getSubmodel(subcomponentAtom);
      assertTrue(database.checkSorted("3", subsubmodel2.toString()));
      assertTrue(subsubmodel2.hasSameContext(subsubmodel));
      assertTrue(database.checkSorted("1-sm", toString(model2.getSubmodelAtoms())));
      assertTrue(database.checkSorted("2-sm", toString(submodel2.getSubmodelAtoms())));
      assertTrue(database.checkSorted("3-sm", toString(subsubmodel2.getSubmodelAtoms())));
    }

    {
      final Model model3 = model.create(false, true);
      assertTrue(database.checkSorted("1", model3.toString()));
      assertFalse(model3.hasSameContext(model));
      final Atom componentAtom3 = model3.get("component", model3.getIntegerLiteral(1));
      assertTrue(database.checkSorted("1-sm", toString(model3.getSubmodelAtoms())));
      final Model submodel3 = model3.getSubmodel(componentAtom3);
      assertTrue(database.checkSorted("2-sm", toString(submodel3.getSubmodelAtoms())));
      final Atom subcomponentAtom3 = submodel3.get("huhuDeep", submodel3.getIntegerLiteral(2),
          submodel3.getStringLiteral("hatschi"));
      assertTrue(database.checkSorted("2", submodel3.toString()));
      assertFalse(submodel3.hasSameContext(submodel));
      final Model subsubmodel3 = submodel3.getSubmodel(subcomponentAtom3);
      assertTrue(database.checkSorted("3", subsubmodel3.toString()));
      assertFalse(subsubmodel3.hasSameContext(subsubmodel));
      assertTrue(database.checkSorted("1-sm", toString(model3.getSubmodelAtoms())));
      assertTrue(database.checkSorted("2-sm", toString(submodel3.getSubmodelAtoms())));
      assertTrue(database.checkSorted("3-sm", toString(subsubmodel3.getSubmodelAtoms())));
    }

    {
      final Model model4 = model.create(true, false);
      assertTrue(database.checkSorted("empty", model4.toString()));
      assertTrue(model4.hasSameContext(model));
      assertTrue(database.checkSorted("empty-sm", toString(model4.getSubmodelAtoms())));
      final Model submodel4 = model4.getSubmodel(componentAtom);
      assertTrue(database.checkSorted("empty", submodel4.toString()));
      assertTrue(submodel4.hasSameContext(submodel));
      assertTrue(database.checkSorted("empty-sm", toString(submodel4.getSubmodelAtoms())));
      final Model subsubmodel4 = submodel4.getSubmodel(subcomponentAtom);
      assertTrue(database.checkSorted("empty", submodel4.toString()));
      assertTrue(subsubmodel4.hasSameContext(subsubmodel));
      assertTrue(database.checkSorted("1-sm", toString(model4.getSubmodelAtoms())));
      assertTrue(database.checkSorted("2-sm", toString(submodel4.getSubmodelAtoms())));
      assertTrue(database.checkSorted("empty-sm", toString(subsubmodel4.getSubmodelAtoms())));
    }

    assertTrue(database.checkSorted("4", model.toStringHierarchically()));
  }

  @Test
  public void testHierarchicalModelCreation2() {
    for (int i = 1; i < 5; ++i) {
      final Model model = getHierarhicalModel(i);
      assertTrue(database.checkSorted("Hierarchical-" + Integer.toString(i), model.toStringHierarchically()));
    }
  }

  @Test
  public void testAddByParsingHierarchically() throws ParserException {
    final Model model = getModel();

    model.addByParsingHierarchically("a1__b0__c(1,2,4)");
    assertTrue(database.checkSorted("1,0", model.toStringHierarchically()));

    model.addByParsingHierarchically("a0__b0__c(1,2,4)");
    assertTrue(database.checkSorted("0,0", model.toStringHierarchically()));

    model.addByParsingHierarchically("a1__b1__c(1,2,4)");
    assertTrue(database.checkSorted("1,1", model.toStringHierarchically()));

    model.addByParsingHierarchically("a0__b2__c(1,2,4)");
    assertTrue(database.checkSorted("0,2", model.toStringHierarchically()));
  }

  private static void writeHierarchicalModelInSegments(final Writer writer, final Model model) throws IOException {
    model.write(writer);
    final Iterator<Atom> submodelAtoms = model.getSubmodelAtoms();
    while (submodelAtoms.hasNext()) {
      final Atom atom = submodelAtoms.next();
      final Model submodel = model.getSubmodel(atom);
      model.write(writer);
      writer.write("BEGIN for ");
      atom.write(writer);
      writer.write("\n");
      writeHierarchicalModelInSegments(writer, submodel);
      writer.write("END for ");
      atom.write(writer);
      writer.write("\n");
    }
  }

  private static String toStringHierarchicalModelInSegments(final Model model) {
    final StringWriter writer = new StringWriter();
    try {
      writeHierarchicalModelInSegments(writer, model);
    } catch (final IOException e) {
      e.printStackTrace();
      assert false : "No RAM ?!";
    }
    return writer.toString();
  }

  @Test
  public void testHierarchialRead() throws ModelSchemaException {
    for (int i = 1; i < 5; ++i) {
      final Model model = getHierarhicalModel(i);
      final String modelString = model.toStringHierarchically();
      assertTrue(database.checkSorted("Hierarchical-" + Integer.toString(i), modelString));
      final ModelSchema modelSchema = model.getModelSchema();
      assertTrue(database.check("Hierarchical-Schema", modelSchema.toString()));
      final Model model2 = getModel();
      model2.readHierarchically(new StringReader(modelString));
      assertTrue(database.checkSorted("Hierarchical-" + Integer.toString(i), model2.toStringHierarchically()));
      // to sort here is kind of weak, but otherwise, we would have to sort during writing;
      // it should suffice together with toStringHierarchically to ensure that the hierarchy is properly read.
      assertTrue(database.checkSorted("Hierarchical-seg-" + Integer.toString(i),
          toStringHierarchicalModelInSegments(model2)));
    }
  }

  @Test
  public void testAddAtomFromOtherContext() throws ParserException {
    final Model model = getModel();
    final Model model2 = getSmallModel();
    final Model relatedModel = model.create(true, false);

    final Atom atom = model.addByParsing("a(0).");

    Atom newAtom = model.addFromOtherContext(atom);
    assertTrue(newAtom == atom);

    newAtom = relatedModel.addFromOtherContext(atom);
    assertTrue(newAtom == atom);

    newAtom = model2.addFromOtherContext(atom);
    assertTrue(newAtom != atom);
    assertTrue(database.check("fromother-1", newAtom.toString()));
  }

  @Test
  public void testGetSubmodel() throws ParserException {
    final Model model = getModel();
    Model submodel = model.getSubmodel("a(1))").getSubmodel("b(2,3)").getSubmodel("c(4)");
    assertTrue(database.checkSorted("getsubmodel-all", model.toStringHierarchically()));

    assertTrue(database.checkSorted("getsubmodel-submodel", submodel.toStringHierarchically()));

    submodel = submodel.getParentModel();
    assertTrue(database.checkSorted("getsubmodel-submodel1", submodel.toStringHierarchically()));

    submodel = submodel.getParentModel();
    assertTrue(database.checkSorted("getsubmodel-submodel2", submodel.toStringHierarchically()));

    submodel = submodel.getParentModel();
    assertTrue(database.checkSorted("getsubmodel-all", submodel.toStringHierarchically()));

    assertNull(submodel.getParentModel());
  }

  @Test
  public void testGetFromOtherContext() throws ParserException {
    final Model model = getModel();
    final Model model2 = getSmallModel();
    final Model relatedModel = model.create(true, false);

    final Term term = model.parseTerm("a(0)");

    Term newterm = model.getTermFromOtherContext(term);
    assertTrue(newterm == term);

    newterm = relatedModel.getTermFromOtherContext(term);
    assertTrue(newterm == term);

    newterm = model2.getTermFromOtherContext(term);
    assertTrue(newterm != term);
    assertTrue(database.check("fromother-1", newterm.toString()));
  }

  @Test
  public void testModelSchemaCreation() {
    final Model model = getHierarhicalModel(10);
    final ModelSchema modelSchema = model.getModelSchema();
    assertTrue(database.check("schema-2", modelSchema.toString()));
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testGetUniqueAtom() throws ModelAccessException {
    final Model model = getLargeModel();
    assertTrue(database.check("unique1", model.getUniqueAtomByMask("a", model.getIntegerLiteral(0)).toString()));
    assertTrue(database.check("unique2",
        model.getUniqueAtomByMask("b", model.getIntegerLiteral(0), model.getIntegerLiteral(1)).toString()));
  }

  @SuppressWarnings("unchecked")
  @Test(expected = ModelAccessException.class)
  public void testGetUniqueAtomNonUnique() throws ModelAccessException {
    final Model model = getLargeModel();
    model.getUniqueAtomByMask("b", model.getIntegerLiteral(0), null);
  }

  @SuppressWarnings("unchecked")
  @Test(expected = ModelAccessException.class)
  public void testGetUniqueAtomPrefixNonUnique() throws ModelAccessException {
    final Model model = getLargeModel();
    model.getUniqueAtomByPrefixMask("b", model.getIntegerLiteral(0));
  }

  @SuppressWarnings("unchecked")
  @Test
  public void testGetUniqueTerm() throws ModelAccessException {
    final Model model = getLargeModel();
    assertTrue(database.check("uniqueterm1", model.getUniqueTermByMask(0, "a", model.getIntegerLiteral(0)).toString()));
    assertTrue(database.check("uniqueterm2",
        model.getUniqueTermByMask(1, "b", model.getIntegerLiteral(0), model.getIntegerLiteral(1)).toString()));
  }

  @SuppressWarnings("unchecked")
  @Test(expected = ModelAccessException.class)
  public void testGetUniqueTermNonUnique() throws ModelAccessException {
    final Model model = getLargeModel();
    model.getUniqueTermByMask(0, "b", model.getIntegerLiteral(0), null);
  }

  @SuppressWarnings("unchecked")
  @Test(expected = ModelAccessException.class)
  public void testGetUniqueTermPrefixNonUnique() throws ModelAccessException {
    final Model model = getLargeModel();
    assertTrue(database.check("uniqueterm2", model.getUniqueTermByPrefixMask(0, "b", model.getIntegerLiteral(0))
        .toString()));
    model.getUniqueTermByPrefixMask(2, "b", model.getIntegerLiteral(0));
  }

  @Test
  public void testLoadPerformance() throws ModelIOException {
    final Model model = getModel();
    model.read(new File("data/test/domFactsLarge.facts"));
  }

  @Test
  public void testLoadSavePerformance() throws ModelIOException {
    final Model model = getModel();
    model.read(new File("data/test/domFactsLarge.facts"));
    model.write(new File("data/test/domFactsLargeTestOutput.facts"));
  }

  @Test
  public void testFactWithZeroArityNamespace() throws ParserException {
    database.setMethodKeyPrefix();

    final String firstS = "dom0__b.";
    final Model first = getModel();

    first.readHierarchically(new StringReader(firstS));
    assertTrue(database.check("total", first.toString()));
    assertTrue(database.check("submodel", first.getSubmodel("dom").toString()));
  }

  @Ignore
  @Test(expected = AssertionError.class)
  public void testUpperCaseConstantName() throws ParserException {
    database.setMethodKeyPrefix();

    final Model model = getModel();
    model.addByParsing("dideldum(AA)");
  }
  
  @Test
  public void testAddFromOtherContextHierarchially() throws ParserException {
    database.setMethodKeyPrefix();
    
    final Model model = getHierarhicalModel(5);
    final Model newModel = getModel();
    
    newModel.addFromOtherContextHierarchically(model);
    assertTrue(database.checkSorted("total/hierarchical", model.toStringHierarchically()));
    assertTrue(database.checkSorted("total/hierarchical", newModel.toStringHierarchically()));
  }
  
  
}
