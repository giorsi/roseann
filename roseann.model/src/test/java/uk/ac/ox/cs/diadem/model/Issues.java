/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.model;

import org.junit.AfterClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.ParserException;
import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;

/**
 * @author timfu
 */
public class Issues {
  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(Issues.class);
  @SuppressWarnings("unused")
  private static final Configuration config = ConfigurationFacility.getConfiguration();

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Test(expected = ParserException.class)
  public final void test() throws ParserException {
    final Model m = ModelFactory.createIsolatedModel();
    final Term t = m.parseTerm("dom.clob");
    logger.info(t.toString());
  }
}
