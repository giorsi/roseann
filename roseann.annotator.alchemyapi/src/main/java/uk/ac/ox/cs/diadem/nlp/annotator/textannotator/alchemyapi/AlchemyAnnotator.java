/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.alchemyapi;

import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonNodeDoesNotMatchJsonNodeSelectorException;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.AnnotationAttribute;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.util.Offset;

/**
 *
 *
 * @author Luying Chen
 *
 *         The adapter submit the text to AlchempAPI, take the response as
 *         String representing JSon Object, builds a JSon Object and select the
 *         annotations to be returned. We can retrieve entity annotations as
 *         well as relation annotations
 *
 *         TO DO: Relation annotation Sentiment annotation ...
 */

public class AlchemyAnnotator extends AnnotatorAdapter {
  static final Logger logger = LoggerFactory.getLogger(AlchemyAnnotator.class);
  /**
   * Low layer api of alchemy service
   */
  private AlchemyAPI alchemyApi;
  /**
   * counter to increment the annotation id
   */
  private long id_counter;
  /**
   * singleton instance
   */
  private static AlchemyAnnotator INSTANCE;

  /**
   * private constructor to build the singleton instance
   */
  private AlchemyAnnotator() {
    ConfigurationFacility.getConfiguration();
    setAnnotatorName("alchemyAPI");

    // start the counter from 0
    id_counter = 0;

    // initialize the parametrs
    final AnnotatorConfiguration configuration = new AnnotatorConfiguration();
    configuration.initializeConfiguration(getAnnotatorName());
    setConfig(configuration);

  }

  /*
   * (non-Javadoc)
   *
   * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.
   * AnnotatorAdapter
   * #setConfig(uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.
   * AnnotatorConfiguration)
   */
  @Override
  public void setConfig(final AnnotatorConfiguration config) {
    // TODO Auto-generated method stub
    super.setConfig(config);
    // Instantiate the alchemyAPI with the new configuration

    alchemyApi = AlchemyAPI.GetInstanceFromConfig(config);

  }

  /**
   * method to get the singleton instance
   *
   * @return singleton instance
   */
  public static synchronized Annotator getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new AlchemyAnnotator();
    }
    return INSTANCE;
  }

  /**
   * An utility method to get the NE extraction response from alchemy service
   *
   * @param text
   *          The text to be annotated
   * @return String The response string
   */
  private String getEntityResponse(final String text, final int timeout) {
    String res = null;

    try {

      res = alchemyApi.invokeNEService(text, config, timeout);
      logger.debug("AlchemyAPI Process done and response received.");

      return res;
    } catch (final IllegalArgumentException ce) {
      logger.error("Error retrieving the key for AlchemyAPI");
      throw new AnnotatorException("Error retrieving the key for AlchemyAPI", ce, logger);
    } // check if the timeout has expired
    catch (final SocketTimeoutException e) {
      logger.warn("{} was not able to receive the web response within timeout {} milliseconds", getAnnotatorName(),
          timeout);
      return null;
    }

    catch (final Exception e) {
      logger.error("Error submitting text to AlchemyAPI");
      throw new AnnotatorException(e.getMessage(), e, logger);
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.
   * AnnotatorAdapter #annotateEntity(java.lang.String)
   */
  @Override
  public Set<Annotation> annotateEntity(final String text) {
    return annotate(text, 0);
  }

  @Override
  public Set<Annotation> annotateEntity(final String text, final int timeout) {
    return annotate(text, timeout);
  }

  private Set<Annotation> annotate(String text, final int timeout) {
    if ((text == null) || (text.length() < 1)) {
      logger.error("Trying to submit an empty text to Alchemy");
      throw new AnnotatorException("Trying to submit an empty text to alchemy", logger);
    }
    // eliminate from the original text special characters
    text = text.replaceAll("\u0003", " ");
    final String res = getEntityResponse(text, timeout);
    // check if the timeout has expired
    if (res == null)
      return new HashSet<Annotation>();
    final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(res, text, id_counter);
    final int num_annotations = entityAnnotations.size();
    id_counter += num_annotations;
    return entityAnnotations;
  }

  public String submitToAnnotator(final String freeText) {
    // TODO Auto-generated method stub
    return null;
  }

  /**
   * An utility method to harvest entity annotations from annotator response
   *
   * @param text
   *          Text to be annotated
   * @param res
   *          Response to be interpreted
   * @param progressive
   * @return Set<Annotation> Set of entity annotations
   */
  private Set<Annotation> retrieveEntityAnnotations(final String res, final String text, final long progressive) {
    JsonRootNode doc;
    logger.debug("Processing the AlchemyAPI response.");
    try {
      // create a JSon node parsing the text response
      doc = new JdomParser().parse(res);
    } catch (final InvalidSyntaxException e) {
      logger.error("The String response from AlchemyAPI can't be parse as JSonNode");
      throw new AnnotatorException("The String response from AlchemyAPI " + "can't be parse as JSonNode", e, logger);
    }

    // select only the entity annotations
    final List<JsonNode> selected_annotations = selectAnnotations(doc, "entities");

    // the count the annotation
    int count = 0;
    final Set<Annotation> entityAnnotation = new HashSet<Annotation>();
    if (selected_annotations != null) {
      for (final JsonNode annotation : selected_annotations) {
        // increment the annotation id and save the annotations found
        final Set<Annotation> found_annotations = buildAnnotationFromJson(annotation, text, count + progressive);
        if (found_annotations != null) {
          count += found_annotations.size();
          entityAnnotation.addAll(found_annotations);
        }

      }
      logger.debug("Response processed and annotations saved.");
    } else {
      logger.debug("AlchemyAPI service did not find any entity annotations.");
    }

    return entityAnnotation;
  }

  private Set<Annotation> buildAnnotationFromJson(final JsonNode annotation, final String doc,
      final long annotation_id) {

    final Set<Annotation> annotations_found = new HashSet<Annotation>();
    // get start-end for every instance of the given annotation
    final Map<Integer, Integer> instances = Offset.getInstancePosition(
        Integer.parseInt(annotation.getStringValue("count")), doc, annotation.getStringValue("text"));

    // the annotator type
    final String annotator_term = annotation.getStringValue("type");
    // transform the concept dlv compatible
    // annotator_term = AnnotationUtil.modifyConcept(annotator_term);

    // if the system is not able to identify instances over the text just
    // skip the annotation
    if (instances == null) {
      final String text = annotation.getStringValue("text");
      logger.debug("The system is not able to find the instances of {} annotated with type {}", text, annotator_term);
      return annotations_found;

    }

    // to count the annotation
    // int progressive = 0;

    long start;
    long end;
    final String annotator_name = annotatorName;

    // confidence given by the annotator
    final Double confidence_double = Double.parseDouble(annotation.getStringValue("relevance"));

    // get the annotation attribute
    final Map<String, String> attribute2value = getAnnotationAttribute(annotation);

    // get subtypes for the annotation, if there are
    // final List<String> subtypes = this.getSubtypes(annotation);

    // iterate over all instances and create a fact for every instance
    for (final int key : instances.keySet()) {

      // the source of the annotator knowledge is uknown in AlchemyAPI.
      // It's base on
      // sophisticated statistical algorithms and natural language
      // processing technology
      final String source = "null";

      // increment the id
      // progressive++;

      // retrieve all the information for the annotation instance

      // id of annotation: clobId_annotatorId_annotationId_instanceId
      final String id = getAnnotatorName() + "_" + (annotation_id + annotations_found.size());
      start = key;
      end = instances.get(key);

      // create the new entity annotation
      final Annotation oneAnnotation = new EntityAnnotation(id, annotator_term, start, end, annotator_name, source,
          confidence_double, AnnotationClass.INSTANCE);

      // create a new fact for every attribute found for the annotation
      if (attribute2value != null) {
        // for every attribute create a object
        for (final String attribute_name : attribute2value.keySet()) {
          final String value = attribute2value.get(attribute_name);

          final AnnotationAttribute attribute = new AnnotationAttribute(attribute_name, value);
          oneAnnotation.addAttribute(attribute);

        }
      }
      annotations_found.add(oneAnnotation);

    }

    // return the annotations number saved
    return annotations_found;
  }

  /**
   * Select in the annotated document only the annottation of a certain type
   *
   * @param root
   *          the JSon node representing the annotated document
   * @param type
   *          type of the annotations to be selected
   * @return a list of selected annotations represented as JSon node
   */
  private List<JsonNode> selectAnnotations(final JsonNode root, final String type) {
    List<JsonNode> selected_annotations = new LinkedList<JsonNode>();
    logger.debug("Select entities nodes from json response.");
    try {
      selected_annotations = root.getArrayNode(type);
    } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
      // if there are no annotation of a certain type return null
      return null;

    }
    return selected_annotations.size() > 0 ? selected_annotations : null;
  }

  /**
   * Method to find the annotation attribute Currently in AlchemyAPI we extract
   * the sentiment of the annotation and if has been disambiguated the website
   *
   * @param annotation
   *          JsonNode representing the annotation
   * @return map where key is the name of the attribute, value all values
   *         associated with that specific attribute (same attribute can have
   *         multiple value, e.g. subtype attribute)
   */
  private Map<String, String> getAnnotationAttribute(final JsonNode annotation) {
    final Map<String, String> attribute2value = new HashMap<String, String>();

    // try to retrieve sentiment information for a given annotation
    try {
      final String sentiment_type = annotation.getNode("sentiment").getStringValue("type");
      attribute2value.put("sentiment_type", sentiment_type);

      // if sentiment is not neutral, retrive the score
      if (!sentiment_type.equals("neutral")) {
        final String score = annotation.getNode("sentiment").getStringValue("score");
        attribute2value.put("sentiment_score", score);
      }
    } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {

      // if not able to find sentiment information, just continue
      logger.info("System is not able to retrive the attribute sentiment" + " for the annotation of in AlchemyAPI", je);
    }

    // if the entity has been disambiguated, retrieve website
    try {
      final String web_site = annotation.getNode("disambiguated").getStringValue("website");
      attribute2value.put("website", web_site);

    } catch (final Exception e) {
      // if not able to find website just continue
    }
    // if the entity has been disambiguated, retrieve name
    try {
      final String name = annotation.getNode("disambiguated").getStringValue("name");
      attribute2value.put("name", name);

    } catch (final Exception e) {
      // if not able to find name just continue
    }

    return attribute2value.size() > 0 ? attribute2value : null;
  }

  public String submitToAnnotator(final String freeText, final int timeout) {
    // TODO Auto-generated method stub
    return null;
  }

}
