/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.alchemyapi;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

class AlchemyAPI_NamedEntityParams extends AlchemyAPI_Params {
	public static final String CLEANED_OR_RAW = "cleaned_or_raw";
	public static final String CLEANED = "cleaned";
	public static final String RAW = "raw";
	public static final String CQUERY = "cquery";
	public static final String XPATH = "xpath";

	private Boolean disambiguate;
	private Boolean linkedData;
	private Boolean coreference;
	private Boolean quotations;
	private String sourceText;
	private Boolean showSourceText;
	private String cQuery;
	private String xPath;
	private Integer maxRetrieve;
	private String baseUrl;
	private Boolean sentiment;

	public boolean isDisambiguate() {
		return disambiguate;
	}

	public void setDisambiguate(boolean disambiguate) {
		this.disambiguate = disambiguate;
	}

	public boolean isLinkedData() {
		return linkedData;
	}

	public void setLinkedData(boolean linkedData) {
		this.linkedData = linkedData;
	}

	public boolean isCoreference() {
		return coreference;
	}

	public void setCoreference(boolean coreference) {
		this.coreference = coreference;
	}

	public boolean isQuotations() {
		return quotations;
	}

	public void setQuotations(boolean quotations) {
		this.quotations = quotations;
	}

	public String getSourceText() {
		return sourceText;
	}

	public void setSourceText(String sourceText) {
		if (!sourceText.equals(AlchemyAPI_NamedEntityParams.CLEANED)
		        && !sourceText.equals(AlchemyAPI_NamedEntityParams.CLEANED_OR_RAW)
		        && !sourceText.equals(AlchemyAPI_NamedEntityParams.RAW)
		        && !sourceText.equals(AlchemyAPI_NamedEntityParams.CQUERY)
		        && !sourceText.equals(AlchemyAPI_NamedEntityParams.XPATH)) {
			throw new RuntimeException("Invalid setting " + sourceText + " for parameter sourceText");
		}
		this.sourceText = sourceText;
	}

	public boolean isShowSourceText() {
		return showSourceText;
	}

	public void setShowSourceText(boolean showSourceText) {
		this.showSourceText = showSourceText;
	}

	public String getCQuery() {
		return cQuery;
	}

	public void setCQuery(String cQuery) {
		this.cQuery = cQuery;
	}

	public String getXPath() {
		return xPath;
	}

	public void setXPath(String xPath) {
		this.xPath = xPath;
	}

	public int getMaxRetrieve() {
		return maxRetrieve;
	}

	public void setMaxRetrieve(int maxRetrieve) {
		this.maxRetrieve = maxRetrieve;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	@Override
	public String getParameterString() {
		String retString = super.getParameterString();
		try {
			if (disambiguate != null)
				retString += "&disambiguate=" + (disambiguate ? "1" : "0");
			if (linkedData != null)
				retString += "&linkedData=" + (linkedData ? "1" : "0");
			if (coreference != null)
				retString += "&coreference=" + (coreference ? "1" : "0");
			if (quotations != null)
				retString += "&quotations=" + (quotations ? "1" : "0");
			if (sourceText != null)
				retString += "&sourceText=" + sourceText;
			if (showSourceText != null)
				retString += "&showSourceText=" + (showSourceText ? "1" : "0");
			if (sentiment != null)
				retString += "&sentiment=" + (sentiment ? "1" : "0");
			if (cQuery != null)
				retString += "&cquery=" + URLEncoder.encode(cQuery, "UTF-8");
			if (xPath != null)
				retString += "&xpath=" + URLEncoder.encode(xPath, "UTF-8");
			if (maxRetrieve != null)
				retString += "&maxRetrieve=" + maxRetrieve.toString();
			if (baseUrl != null)
				retString += "&baseUrl=" + URLEncoder.encode(baseUrl, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			retString = "";
		}
		return retString;
	}

	/**
	 * @param sentiment
	 *            the sentiment to set
	 */
	public void setSentiment(Boolean sentiment) {
		this.sentiment = sentiment;
	}

	/**
	 * @return the sentiment
	 */
	public Boolean getSentiment() {
		return sentiment;
	}

}
