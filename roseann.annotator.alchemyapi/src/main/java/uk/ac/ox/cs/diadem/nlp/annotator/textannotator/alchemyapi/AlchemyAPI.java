/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.alchemyapi;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonNodeDoesNotMatchJsonNodeSelectorException;
import argo.saj.InvalidSyntaxException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;

/**
 * Use this class to send a request to AlchemyAPI web service and get a response
 * as a string.
 *
 * @author Luying Chen
 */
class AlchemyAPI {

  private String _apiKey;
  private String _requestUri = "http://access.alchemyapi.com/calls/";

  private AlchemyAPI() {
  }

  /*
   * static public AlchemyAPI GetInstanceFromConfigurationFile(final String
   * annotator_name) throws ConfigurationException { AlchemyAPI api = new
   * AlchemyAPI(); final XMLConfiguration conf = new
   * XMLConfiguration(Constant.TEXT_ANNOTATOR_CONFIGURATION);
   * conf.setExpressionEngine(new XPathExpressionEngine()); final String
   * api_key=conf.getString("annotator[@name='"+annotator_name+"']/keys/key[1]")
   * ; if(api_key == null){ throw new IllegalArgumentException(
   * "API key must be specified in the configuration file following the given path."
   * ); } api.SetAPIKey(api_key); return api; }
   */
  /**
   * Get alchemyapi instance from configuration object.
   * 
   * @param config
   *          Input configuration object of this alchemyapi instance
   * @return AlchemyAPI The alchemy instance with the specific configuration
   */
  static public AlchemyAPI GetInstanceFromConfig(final AnnotatorConfiguration config) {
    final AlchemyAPI api = new AlchemyAPI();
    final String key = config.getApiKey();
    if (key == null)
      throw new IllegalArgumentException("The api key has not been specified");

    api.SetAPIKey(key);
    final URL url = config.getURlendpoint();
    if (url != null) {
      api.SetAPIHost(url.toString());
    }

    return api;
  }

  void SetAPIKey(final String apiKey) {
    _apiKey = apiKey;

    if ((null == _apiKey) || (_apiKey.length() < 5))
      throw new IllegalArgumentException("Too short API key.");
  }

  void SetAPIHost(final String apiHost) {
    if ((null == apiHost) || (apiHost.length() < 2))
      throw new IllegalArgumentException("Too short API host.");

    _requestUri = apiHost;
  }

  /**
   * @return the _apiKey
   */
  String get_apiKey() {
    return _apiKey;
  }

  /**
   * @return the _requestUri
   */
  String get_requestUri() {
    return _requestUri;
  }

  /*
   * public String URLGetRankedNamedEntities(String url) throws IOException,
   * SAXException, ParserConfigurationException, XPathExpressionException {
   * return URLGetRankedNamedEntities(url, new AlchemyAPI_NamedEntityParams());
   * }
   * 
   * public String URLGetRankedNamedEntities(String url,
   * AlchemyAPI_NamedEntityParams params) throws IOException, SAXException,
   * ParserConfigurationException, XPathExpressionException { CheckURL(url);
   * 
   * params.setUrl(url);
   * 
   * return GET("URLGetRankedNamedEntities", "url", params); }
   * 
   * public String HTMLGetRankedNamedEntities(String html, String url) throws
   * IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { return HTMLGetRankedNamedEntities(html, url, new
   * AlchemyAPI_NamedEntityParams()); }
   * 
   * public String HTMLGetRankedNamedEntities(String html, String url,
   * AlchemyAPI_NamedEntityParams params) throws IOException, SAXException,
   * ParserConfigurationException, XPathExpressionException { CheckHTML(html,
   * url);
   * 
   * params.setUrl(url); params.setHtml(html);
   * 
   * return POST("HTMLGetRankedNamedEntities", "html", params); }
   */
  public String TextGetRankedNamedEntities(final String text, final int timeout)
      throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
    return TextGetRankedNamedEntities(text, new AlchemyAPI_NamedEntityParams(), timeout);
  }

  public String TextGetRankedNamedEntities(final String text, final AlchemyAPI_NamedEntityParams params,
      final int timeout) throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
    CheckText(text);

    params.setText(text);

    return POST("TextGetRankedNamedEntities", "text", params, timeout);
  }

  /*
   * public String TextGetTextSentiment(String text,
   * AlchemyAPI_NamedEntityParams params) throws IOException, SAXException,
   * ParserConfigurationException, XPathExpressionException { CheckText(text);
   * 
   * params.setText(text);
   * 
   * return POST("TextGetTextSentiment", "text", params); }
   * 
   * public String URLGetRankedConcepts(String url) throws IOException,
   * SAXException, ParserConfigurationException, XPathExpressionException {
   * return URLGetRankedConcepts(url, new AlchemyAPI_ConceptParams()); }
   * 
   * public String URLGetRankedConcepts(String url, AlchemyAPI_ConceptParams
   * params) throws IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckURL(url);
   * 
   * params.setUrl(url);
   * 
   * return GET("URLGetRankedConcepts", "url", params); }
   * 
   * public String HTMLGetRankedConcepts(String html, String url) throws
   * IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { return HTMLGetRankedConcepts(html, url, new
   * AlchemyAPI_ConceptParams()); }
   * 
   * public String HTMLGetRankedConcepts(String html, String url,
   * AlchemyAPI_ConceptParams params) throws IOException, SAXException,
   * ParserConfigurationException, XPathExpressionException { CheckHTML(html,
   * url);
   * 
   * params.setUrl(url); params.setHtml(html);
   * 
   * return POST("HTMLGetRankedConcepts", "html", params); }
   * 
   * public String TextGetRankedConcepts(String text) throws IOException,
   * SAXException, ParserConfigurationException, XPathExpressionException {
   * return TextGetRankedConcepts(text, new AlchemyAPI_ConceptParams()); }
   * 
   * public String TextGetRankedConcepts(String text, AlchemyAPI_ConceptParams
   * params) throws IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckText(text);
   * 
   * params.setText(text);
   * 
   * return POST("TextGetRankedConcepts", "text", params); }
   * 
   * public String URLGetRankedKeywords(String url) throws IOException,
   * SAXException, ParserConfigurationException, XPathExpressionException {
   * return URLGetRankedKeywords(url, new AlchemyAPI_KeywordParams()); }
   * 
   * public String URLGetRankedKeywords(String url, AlchemyAPI_KeywordParams
   * params) throws IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckURL(url);
   * 
   * params.setUrl(url);
   * 
   * return GET("URLGetRankedKeywords", "url", params); }
   * 
   * public String HTMLGetRankedKeywords(String html, String url) throws
   * IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { return HTMLGetRankedKeywords(html, url, new
   * AlchemyAPI_KeywordParams()); }
   * 
   * public String HTMLGetRankedKeywords(String html, String url,
   * AlchemyAPI_KeywordParams params) throws IOException, SAXException,
   * ParserConfigurationException, XPathExpressionException { CheckHTML(html,
   * url);
   * 
   * params.setUrl(url); params.setHtml(html);
   * 
   * return POST("HTMLGetRankedKeywords", "html", params); }
   * 
   * public String TextGetRankedKeywords(String text) throws IOException,
   * SAXException, ParserConfigurationException, XPathExpressionException {
   * return TextGetRankedKeywords(text, new AlchemyAPI_KeywordParams()); }
   * 
   * public String TextGetRankedKeywords(String text, AlchemyAPI_KeywordParams
   * params) throws IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckText(text);
   * 
   * params.setText(text);
   * 
   * return POST("TextGetRankedKeywords", "text", params); }
   * 
   * public String URLGetLanguage(String url) throws IOException, SAXException,
   * ParserConfigurationException, XPathExpressionException { return
   * URLGetLanguage(url, new AlchemyAPI_LanguageParams()); }
   * 
   * public String URLGetLanguage(String url, AlchemyAPI_LanguageParams params)
   * throws IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckURL(url);
   * 
   * params.setUrl(url);
   * 
   * return GET("URLGetLanguage", "url", params); }
   * 
   * public String HTMLGetLanguage(String html, String url) throws IOException,
   * SAXException, ParserConfigurationException, XPathExpressionException {
   * return HTMLGetLanguage(html, url, new AlchemyAPI_LanguageParams()); }
   * 
   * public String HTMLGetLanguage(String html, String url,
   * AlchemyAPI_LanguageParams params) throws IOException, SAXException,
   * ParserConfigurationException, XPathExpressionException { CheckHTML(html,
   * url);
   * 
   * params.setUrl(url); params.setHtml(html);
   * 
   * return POST("HTMLGetLanguage", "html", params); }
   * 
   * public String TextGetLanguage(String text) throws IOException,
   * SAXException, ParserConfigurationException, XPathExpressionException {
   * return TextGetLanguage(text, new AlchemyAPI_LanguageParams()); }
   * 
   * public String TextGetLanguage(String text, AlchemyAPI_LanguageParams
   * params) throws IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckText(text);
   * 
   * params.setText(text);
   * 
   * return POST("TextGetLanguage", "text", params); }
   * 
   * public String URLGetCategory(String url) throws IOException, SAXException,
   * ParserConfigurationException, XPathExpressionException { return
   * URLGetCategory(url, new AlchemyAPI_CategoryParams()); }
   * 
   * public String URLGetCategory(String url, AlchemyAPI_CategoryParams params)
   * throws IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckURL(url);
   * 
   * params.setUrl(url);
   * 
   * return GET("URLGetCategory", "url", params); }
   * 
   * public String HTMLGetCategory(String html, String url) throws IOException,
   * SAXException, ParserConfigurationException, XPathExpressionException {
   * return HTMLGetCategory(html, url, new AlchemyAPI_CategoryParams()); }
   * 
   * public String HTMLGetCategory(String html, String url,
   * AlchemyAPI_CategoryParams params) throws IOException, SAXException,
   * ParserConfigurationException, XPathExpressionException { CheckHTML(html,
   * url);
   * 
   * params.setUrl(url); params.setHtml(html);
   * 
   * return POST("HTMLGetCategory", "html", params); }
   * 
   * public String TextGetCategory(String text) throws IOException,
   * SAXException, ParserConfigurationException, XPathExpressionException {
   * return TextGetCategory(text, new AlchemyAPI_TextParams()); }
   * 
   * public String TextGetCategory(String text, AlchemyAPI_TextParams params)
   * throws IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckText(text);
   * 
   * params.setText(text);
   * 
   * return POST("TextGetCategory", "text", params); }
   * 
   * public String URLGetText(String url) throws IOException, SAXException,
   * ParserConfigurationException, XPathExpressionException { return
   * URLGetText(url, new AlchemyAPI_TextParams()); }
   * 
   * public String URLGetText(String url, AlchemyAPI_TextParams params) throws
   * IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckURL(url);
   * 
   * params.setUrl(url);
   * 
   * return GET("URLGetText", "url", params); }
   * 
   * public String HTMLGetText(String html, String url) throws IOException,
   * SAXException, ParserConfigurationException, XPathExpressionException {
   * return HTMLGetText(html, url, new AlchemyAPI_TextParams()); }
   * 
   * public String HTMLGetText(String html, String url, AlchemyAPI_TextParams
   * params) throws IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckHTML(html, url);
   * 
   * params.setUrl(url); params.setHtml(html);
   * 
   * return POST("HTMLGetText", "html", params); }
   * 
   * public String URLGetRawText(String url) throws IOException, SAXException,
   * ParserConfigurationException, XPathExpressionException { return
   * URLGetRawText(url, new AlchemyAPI_Params()); }
   * 
   * public String URLGetRawText(String url, AlchemyAPI_Params params) throws
   * IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckURL(url);
   * 
   * params.setUrl(url);
   * 
   * return GET("URLGetRawText", "url", params); }
   * 
   * public String HTMLGetRawText(String html, String url) throws IOException,
   * SAXException, ParserConfigurationException, XPathExpressionException {
   * return HTMLGetRawText(html, url, new AlchemyAPI_Params()); }
   * 
   * public String HTMLGetRawText(String html, String url, AlchemyAPI_Params
   * params) throws IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckHTML(html, url);
   * 
   * params.setUrl(url); params.setHtml(html);
   * 
   * return POST("HTMLGetRawText", "html", params); }
   * 
   * public String URLGetTitle(String url) throws IOException, SAXException,
   * ParserConfigurationException, XPathExpressionException { return
   * URLGetTitle(url, new AlchemyAPI_Params()); }
   * 
   * public String URLGetTitle(String url, AlchemyAPI_Params params) throws
   * IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckURL(url);
   * 
   * params.setUrl(url);
   * 
   * return GET("URLGetTitle", "url", params); }
   * 
   * public String HTMLGetTitle(String html, String url) throws IOException,
   * SAXException, ParserConfigurationException, XPathExpressionException {
   * return HTMLGetTitle(html, url, new AlchemyAPI_Params()); }
   * 
   * public String HTMLGetTitle(String html, String url, AlchemyAPI_Params
   * params) throws IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckHTML(html, url);
   * 
   * params.setUrl(url); params.setHtml(html);
   * 
   * return POST("HTMLGetTitle", "html", params); }
   * 
   * public String URLGetFeedLinks(String url) throws IOException, SAXException,
   * ParserConfigurationException, XPathExpressionException { return
   * URLGetFeedLinks(url, new AlchemyAPI_Params()); }
   * 
   * public String URLGetFeedLinks(String url, AlchemyAPI_Params params) throws
   * IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckURL(url);
   * 
   * params.setUrl(url);
   * 
   * return GET("URLGetFeedLinks", "url", params); }
   * 
   * public String HTMLGetFeedLinks(String html, String url) throws IOException,
   * SAXException, ParserConfigurationException, XPathExpressionException {
   * return HTMLGetFeedLinks(html, url, new AlchemyAPI_Params()); }
   * 
   * public String HTMLGetFeedLinks(String html, String url, AlchemyAPI_Params
   * params) throws IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckHTML(html, url);
   * 
   * params.setUrl(url); params.setHtml(html);
   * 
   * return POST("HTMLGetFeedLinks", "html", params); }
   * 
   * public String URLGetMicroformats(String url) throws IOException,
   * SAXException, ParserConfigurationException, XPathExpressionException {
   * return URLGetMicroformats(url, new AlchemyAPI_Params()); }
   * 
   * public String URLGetMicroformats(String url, AlchemyAPI_Params params)
   * throws IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckURL(url);
   * 
   * params.setUrl(url);
   * 
   * return GET("URLGetMicroformatData", "url", params); }
   * 
   * public String HTMLGetMicroformats(String html, String url) throws
   * IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { return HTMLGetMicroformats(html, url, new
   * AlchemyAPI_Params()); }
   * 
   * public String HTMLGetMicroformats(String html, String url,
   * AlchemyAPI_Params params) throws IOException, SAXException,
   * ParserConfigurationException, XPathExpressionException { CheckHTML(html,
   * url);
   * 
   * params.setUrl(url); params.setHtml(html);
   * 
   * return POST("HTMLGetMicroformatData", "html", params); }
   * 
   * public String URLGetConstraintQuery(String url, String query) throws
   * IOException, XPathExpressionException, SAXException,
   * ParserConfigurationException { return URLGetConstraintQuery(url, query, new
   * AlchemyAPI_ConstraintQueryParams()); }
   * 
   * public String URLGetConstraintQuery(String url, String query,
   * AlchemyAPI_ConstraintQueryParams params) throws IOException,
   * XPathExpressionException, SAXException, ParserConfigurationException {
   * CheckURL(url); if (null == query || query.length() < 2) throw new
   * IllegalArgumentException("Invalid constraint query specified");
   * 
   * params.setUrl(url); params.setCQuery(query);
   * 
   * return POST("URLGetConstraintQuery", "url", params); }
   * 
   * public String HTMLGetConstraintQuery(String html, String url, String query)
   * throws IOException, XPathExpressionException, SAXException,
   * ParserConfigurationException { return HTMLGetConstraintQuery(html, url,
   * query, new AlchemyAPI_ConstraintQueryParams()); }
   * 
   * public String HTMLGetConstraintQuery(String html, String url, String query,
   * AlchemyAPI_ConstraintQueryParams params) throws IOException,
   * XPathExpressionException, SAXException, ParserConfigurationException {
   * CheckHTML(html, url); if (null == query || query.length() < 2) throw new
   * IllegalArgumentException("Invalid constraint query specified");
   * 
   * params.setUrl(url); params.setHtml(html); params.setCQuery(query);
   * 
   * return POST("HTMLGetConstraintQuery", "html", params); }
   * 
   * private void CheckHTML(String html, String url) { if (null == html ||
   * html.length() < 5) throw new IllegalArgumentException(
   * "Enter a HTML document to analyze.");
   * 
   * if (null == url || url.length() < 10) throw new IllegalArgumentException(
   * "Enter an URL to analyze."); }
   */
  private void CheckText(final String text) {
    if ((null == text) || (text.length() < 5))
      throw new IllegalArgumentException("Enter some text to analyze.");
  }

  /*
   * private void CheckURL(String url) { if (null == url || url.length() < 10)
   * throw new IllegalArgumentException("Enter an URL to analyze."); }
   * 
   * public String TextGetRelation(String text) throws IOException,
   * SAXException, ParserConfigurationException, XPathExpressionException {
   * return TextGetRelation(text, new AlchemyAPI_RelationParams()); }
   * 
   * public String TextGetRelation(String text, AlchemyAPI_RelationParams
   * params) throws IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { CheckText(text);
   * 
   * params.setText(text);
   * 
   * return POST("TextGetRelations", "text", params); }
   */
  /*
   * private String GET(String callName, String callPrefix, AlchemyAPI_Params
   * params) throws IOException, SAXException, ParserConfigurationException,
   * XPathExpressionException { StringBuilder uri = new StringBuilder();
   * uri.append(_requestUri).append(callPrefix).append('/').append(callName).
   * append('?').append("apikey=") .append(this._apiKey);
   * uri.append(params.getParameterString());
   * 
   * URL url = new URL(uri.toString()); HttpURLConnection handle =
   * (HttpURLConnection) url.openConnection(); handle.setDoOutput(true);
   * 
   * return doRequest(handle, params.getOutputMode()); }
   */
  private String POST(final String callName, final String callPrefix, final AlchemyAPI_Params params, final int timeout)
      throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
    final URL url = new URL(_requestUri + callPrefix + "/" + callName);

    final HttpURLConnection handle = (HttpURLConnection) url.openConnection();
    handle.setDoOutput(true);
    handle.setConnectTimeout(timeout);

    final StringBuilder data = new StringBuilder();

    data.append("apikey=").append(_apiKey);
    data.append(params.getParameterString());

    handle.addRequestProperty("Content-Length", Integer.toString(data.length()));

    final DataOutputStream ostream = new DataOutputStream(handle.getOutputStream());
    ostream.write(data.toString().getBytes());
    ostream.close();

    return doRequest(handle, params.getOutputMode());
  }

  private String doRequest(final HttpURLConnection handle, final String outputMode)
      throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
    final DataInputStream istream = new DataInputStream(handle.getInputStream());
    Document doc = null;

    final StringBuilder sb = new StringBuilder();
    final BufferedReader br = new BufferedReader(new InputStreamReader(istream));

    String line;
    while ((line = br.readLine()) != null) {
      sb.append(line);
    }

    istream.close();

    br.close();
    handle.disconnect();

    final XPathFactory factory = XPathFactory.newInstance();

    if (AlchemyAPI_Params.OUTPUT_XML.equals(outputMode)) {

      doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
          .parse(new ByteArrayInputStream(sb.toString().getBytes()));
      final String statusStr = getNodeValue(factory, doc, "/results/status/text()");
      if ((null == statusStr) || !statusStr.equals("OK")) {
        final String statusInfoStr = getNodeValue(factory, doc, "/results/statusInfo/text()");
        if ((null != statusInfoStr) && (statusInfoStr.length() > 0))
          throw new IOException("Error making API call: " + statusInfoStr + '.');

        throw new IOException("Error making API call: " + statusStr + '.');
      }
    }
    if (AlchemyAPI_Params.OUTPUT_RDF.equals(outputMode)) {

      doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
          .parse(new ByteArrayInputStream(sb.toString().getBytes()));
      final String statusStr = getNodeValue(factory, doc, "//RDF/Description/ResultStatus/text()");
      if ((null == statusStr) || !statusStr.equals("OK")) {
        final String statusInfoStr = getNodeValue(factory, doc, "//RDF/Description/ResultStatus/text()");
        if ((null != statusInfoStr) && (statusInfoStr.length() > 0))
          throw new IOException("Error making API call: " + statusInfoStr + '.');

        throw new IOException("Error making API call: " + statusStr + '.');
      }
    }
    if (AlchemyAPI_Params.OUTPUT_JSON.equals(outputMode)) {
      final String statusStr = getJsonNodeValue(sb.toString(), "status");
      if ((null == statusStr) || !statusStr.equals("OK")) {
        final String statusInfoStr = getJsonNodeValue(sb.toString(), "statusInfo");
        if ((null != statusInfoStr) && (statusInfoStr.length() > 0))
          throw new IOException("Error making API call: " + statusInfoStr + '.');
      }
    }

    return sb.toString();
  }

  private String getNodeValue(final XPathFactory factory, final Document doc, final String xpathStr)
      throws XPathExpressionException {
    final XPath xpath = factory.newXPath();
    final XPathExpression expr = xpath.compile(xpathStr);
    final Object result = expr.evaluate(doc, XPathConstants.NODESET);
    final NodeList results = (NodeList) result;

    if ((results.getLength() > 0) && (null != results.item(0)))
      return results.item(0).getNodeValue();

    return null;
  }

  private String getJsonNodeValue(final String response, final String path) throws IOException {
    String status = null;
    try {
      final JsonNode json_response = new JdomParser().parse(response);
      status = json_response.getStringValue(path);
    } catch (final InvalidSyntaxException e) {
      throw new IOException("Error making API call: " + "can't parse the response as Json object. " + e);
    } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
      // if can't find the value specified in path, simply return null
      return null;
    }
    return status;

  }

  /**
   * Utility method to invoke NE service and return the response string.
   * 
   * @param text
   *          The source text to be annotated
   * @param config
   *          The input configration object
   * @return String The response string of the service
   * @throws XPathExpressionException
   * @throws IOException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  public String invokeNEService(final String text, final AnnotatorConfiguration config, final int timeout)
      throws XPathExpressionException, IOException, SAXException, ParserConfigurationException {
    final AlchemyAPI_NamedEntityParams entityParams = getParamsFromConfigurationFile(config);
    if ((null == text) || (text.length() < 1))
      throw new IllegalArgumentException("Too short text to analyze.");

    return TextGetRankedNamedEntities(text, entityParams, timeout);

  }

  /**
   * Utility method to assign the configuration items from annotator
   * configuration object
   * 
   * @param config
   *          The annotator configuration object
   * @return AlchemyAPI_NamedEntityParams AlchemyAPI parameters
   */
  private AlchemyAPI_NamedEntityParams getParamsFromConfigurationFile(final AnnotatorConfiguration config) {
    final AlchemyAPI_NamedEntityParams entityParams = new AlchemyAPI_NamedEntityParams();
    final Map<String, String> params = config.getSpecificParameters();
    if (params == null)
      return entityParams;
    String val = params.get("disambiguate");
    if (val != null) {
      entityParams.setDisambiguate(val.trim().equals("1") ? true : false);
    }
    val = params.get("linkedData");
    if (val != null) {
      entityParams.setLinkedData(val.trim().equals("1") ? true : false);
    }
    val = params.get("coreference");
    if (val != null) {
      entityParams.setCoreference(val.trim().equals("1") ? true : false);
    }
    val = params.get("sentiment");
    if (val != null) {
      entityParams.setSentiment(val.trim().equals("1") ? true : false);
    }
    val = params.get("showSourceText");
    if (val != null) {
      entityParams.setShowSourceText(val.trim().equals("1") ? true : false);
    }
    val = params.get("outputMode");
    if (val != null) {
      entityParams.setOutputMode(val.trim().toLowerCase());
    }
    ;
    return entityParams;
  }

}
