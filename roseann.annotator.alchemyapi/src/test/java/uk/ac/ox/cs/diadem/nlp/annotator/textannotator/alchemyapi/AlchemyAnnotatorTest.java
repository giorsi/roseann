/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.alchemyapi;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;

public class AlchemyAnnotatorTest {

  private Annotator alchemy;

  private String text;

  @Before
  public void bringUp() {
    alchemy = AlchemyAnnotator.getInstance();
    text = "Barack Obama is the new president of the United States.";
  }

  /**
   * Test to check that a text can be submitted to Alchemy annotator service
   * (Not Implemented at the moment)
   * 
   * @throws InvalidSyntaxException
   */
  /*
   * @Test public void testSubmitToAnnotator() throws InvalidSyntaxException{
   * 
   * //check the KEY has been specified in the configuration
   * Assert.assertNotNull(alchemy.getConfig().getApiKey());
   * 
   * //check that the text can be submitted to the annotator and no exceptions
   * are thrown String response = alchemy.submitToAnnotator(text);
   * 
   * Assert.assertNotNull(response); Assert.assertTrue(response.length()>0);
   * 
   * //check the response can be parse as json node and contains an element
   * entities and an element relations JsonRootNode doc = new
   * JdomParser().parse(response);
   * 
   * List<JsonNode> entities = doc.getArrayNode("entities");
   * Assert.assertNotNull(entities); Assert.assertTrue(entities.size()>0);
   * 
   * List<JsonNode> relations = doc.getArrayNode("relations");
   * Assert.assertNotNull(relations); Assert.assertTrue(relations.size()>0); }
   */

  /**
   * Test to verify the entities retrieved from Alchemy web service
   */
  @Test
  public void testAnnotateEntities() {
    final Set<Annotation> annotations = alchemy.annotateEntity(text);
    Assert.assertNotNull(annotations);
    System.out.println(annotations);
    Assert.assertTrue(annotations.size() > 0);
  }

  /**
   * Test to verify that no Alchemy ares retrieved from Alchemy web service with
   * an senseless text
   */
  @Test(expected = AnnotatorException.class)
  public void testAnnotateNoEntities() {
    text = "cfrgfvdfgbfdgdgdfgdfgdgdg";

    final Set<Annotation> annotations = alchemy.annotateEntity(text);
    Assert.assertNotNull(annotations);
    Assert.assertTrue(annotations.size() == 0);
  }

}
