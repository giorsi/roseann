/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.model;

import java.util.ArrayList;
import java.util.List;

public class Column {
    // This class is only used to group lines into potential columns that
    // minimize the vertical spacing (not used for gathering lines into blocks)

    /**
     * @param line The line that will initialise the column
     */
    public Column(final Line line) {
        lines = new ArrayList<Line>();
        lines.add(line);
        top = line.getTop();
        bottom = line.getBottom();
        left = line.getLeft();
        right = line.getRight();
    }
    
    private double top, bottom, left, right;
    List<Line> lines = new ArrayList<Line>();

    /**
     * @param line The line to be added to the current column
     */
    public void addLine(final Line line) {
        lines.add(line);
        if (line.getTop() < top) {
            top = line.getTop();
        }

        if (line.getLeft() < left) {
            left = line.getLeft();
        }

        if (line.getRight() > right) {
            right = line.getRight();
        }

        if (line.getBottom() > bottom) {
            bottom = line.getBottom();
        }
    }

    /**
     * @return
     */
    public double getTop() {
        return top;
    }

    /**
     * @return
     */
    public double getBottom() {
        return bottom;
    }

    /**
     * @return
     */
    public double getLeft() {
        return left;
    }

    /**
     * @return
     */
    public double getRight() {
        return right;
    }

    /**
     * @return
     */
    public List<Line> getLines() {
        return lines;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String returnValue = "";

        for (final Line l : lines) {
            returnValue += l.toString();
        }

        return returnValue;
    }
}
