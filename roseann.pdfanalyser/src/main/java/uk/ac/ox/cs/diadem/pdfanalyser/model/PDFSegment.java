package uk.ac.ox.cs.diadem.pdfanalyser.model;

public abstract class PDFSegment {
	
	/**
	 * The page number in a pdf document
	 */
	private int page;

	protected double top, bottom, left, right;
	protected double fontSize;

	/**
	 * @return The width of the block
	 */
	public double getWidth() {
		return right - left;
	}

	/**
	 * @return The top coordinate of the block
	 */
	public double getTop() {
		return top;
	}

	/**
	 * @param top
	 */
	public void setTop(final double top) {
		this.top = top;
	}

	/**
	 * @return
	 */
	public double getBottom() {
		return bottom;
	}

	/**
	 * @param bottom
	 */
	public void setBottom(final double bottom) {
		this.bottom = bottom;
	}

	/**
	 * @return The left coordinate of the block
	 */
	public double getLeft() {
		return left;
	}

	/**
	 * @param left
	 */
	public void setLeft(final double left) {
		this.left = left;
	}

	/**
	 * @return The right coordinate of the block
	 */
	public double getRight() {
		return right;
	}

	/**
	 * @param right
	 */
	public void setRight(final double right) {
		this.right = right;
	}
	
	/**
	 * @return The font size of the block
	 */
	public double getFontSize() {
		return fontSize;
	}
	
	public void setFontSize(double font_size){
		this.fontSize=font_size;
	}
	
	public void setPage(int page){
		this.page=page;
	}
	
	public int getPage(){
		return this.page;
	}

}
