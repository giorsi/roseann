package uk.ac.ox.cs.diadem.pdfanalyser.util.comparator;

import java.util.Comparator;
import java.util.List;

import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.model.HieBlockGroup;

public class HieBlockGroupCoordinateComparator implements Comparator<HieBlockGroup> {
		List<HieBlockGroup> groups;

		public HieBlockGroupCoordinateComparator(List<HieBlockGroup> blockGroups) {
			groups = blockGroups;
		}

		@Override
		public int compare(HieBlockGroup group1, HieBlockGroup group2) {
			// whether group1 and group2 has horizontal overlap
			boolean horiOverlap = Math.max(group1.getLeft(), group2.getLeft()) < Math
					.min(group1.getRight(), group2.getRight());
			boolean verOVerlap = Math.max(group1.getTop(), group2.getTop()) < Math.min(group1.getBottom(), group2.getBottom());
			// this threshold sets a tolerance to group block groups into the same
			// column
			// double threshold = averageBlockGroupWidth;

			if (horiOverlap || (!verOVerlap && otherGroupInBetween(group1, group2))) {
				// the two fragments are in the same column or they overlap
				if (group1.getTop() < group2.getTop()) {
					return -1;
				} else if (group1.getTop() > group2.getTop()) {
					return 1;
				} else {
					return 0;
				}
			} else if (group1.getLeft() < group2.getLeft()) {
				return -1;
			} else if (group1.getLeft() > group2.getLeft()) {
				return 1;
			} else {
				return 0;
			}
		}

		
		private boolean otherGroupInBetween(HieBlockGroup group1, HieBlockGroup group2){
			double top = Math.min(group1.getBottom(), group2.getBottom());
			double bottom = Math.max(group1.getTop(), group2.getTop());
			for(HieBlockGroup group: groups){
				// if group is in the middle
				if(group.getTop() > top && group.getBottom() < bottom){
					// if group overlaps with both group1 and group2
					boolean horiOverlap1 = Math.max(group1.getLeft(), group.getLeft()) < Math
							.min(group1.getRight(), group.getRight());
					boolean horiOverlap2 = Math.max(group2.getLeft(), group.getLeft()) < Math
							.min(group2.getRight(), group.getRight());
					if(horiOverlap1 && horiOverlap2){
						return true;
					}
				}
			}
			return false;
		}
}
