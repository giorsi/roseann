/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.pdfbox.util.TextPosition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.model.Model;
import uk.ac.ox.cs.diadem.pdfanalyser.evaluation.Evaluation;
import uk.ac.ox.cs.diadem.pdfanalyser.evaluation.Files;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;
import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Column;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Segment;
import uk.ac.ox.cs.diadem.pdfanalyser.output.Output;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.AnnotationSegmentor;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.evaluation.AnnoSegEvaluation;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.evaluation.EvaluationFiles;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.evaluation.TrainingFiles;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.model.HieBlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.model.SemanticStructureBlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.output.HierarchicalOutput;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.output.OutputModel;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.semanticstructure.BlockGroupHierarchyAnalyser;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.semanticstructure.SemanticStructureAnalyser;
import uk.ac.ox.cs.diadem.pdfanalyser.util.Bullet;
import uk.ac.ox.cs.diadem.pdfanalyser.util.FontStyle;
import uk.ac.ox.cs.diadem.pdfanalyser.util.ProcessConfig;
import uk.ac.ox.cs.diadem.pdfanalyser.util.Punctuation;
import uk.ac.ox.cs.diadem.pdfanalyser.util.RegionProcessing;
import uk.ac.ox.cs.diadem.pdfanalyser.util.SegmentedContents;
import uk.ac.ox.cs.diadem.pdfanalyser.util.comparator.BlockCoordinateComparator;
import uk.ac.ox.cs.diadem.pdfanalyser.util.comparator.BlockGroupCoordinateComparator;
import uk.ac.ox.cs.diadem.pdfanalyser.util.comparator.GlyphPositionComparator;
import uk.ac.ox.cs.diadem.pdfanalyser.util.comparator.LeftGlyphCoordinateComparator;
import uk.ac.ox.cs.diadem.pdfanalyser.util.comparator.LineCoordinateComparator;


public class DocumentProcessor extends PDFTextStripper implements PDFSegmenter {

	// PDFTextStripper
	/**
	 * @throws IOException
	 */
	public DocumentProcessor() throws IOException {
		super();

		//initialize parameter
		try{
			LINE_SPLIT_PARAM=Integer.parseInt(ConfigurationFacility
					.getConfiguration().getString("pdfanalyser.line.lineSplitParam"));
		}catch(Exception e){
			LINE_SPLIT_PARAM=2;
		}

		try{
			VERTICAL_OUTLIER_PARAM = Integer
					.parseInt(ConfigurationFacility.getConfiguration().getString(
							"pdfanalyser.vertical.outlierParam"));
		}catch(Exception e){
			VERTICAL_OUTLIER_PARAM=2;
		}

		try{
			HORIZONTAL_OUTLIER_PARAM = Integer
					.parseInt(ConfigurationFacility.getConfiguration().getString(
							"pdfanalyser.horizontal.outlierParam"));}
		catch(Exception e){
			HORIZONTAL_OUTLIER_PARAM=2;
		}

		VERTICAL_GROUP_PARAM = LINE_SPLIT_PARAM;

	}

	private static final Logger LOGGER = LoggerFactory
			.getLogger(DocumentProcessor.class);
	private int LINE_SPLIT_PARAM;
	private int VERTICAL_GROUP_PARAM;
	private int VERTICAL_OUTLIER_PARAM; 

	private int HORIZONTAL_OUTLIER_PARAM;

	private PDDocument document = null;
	private List<PDPage> allPages = null;
	private PDPage currentPage = null;
	private String docName = "";
	private List<Glyph> glyphsList;
	private List<Line> lines;
	private List<Block> blockFragments;
	private List<BlockGroup> groups;
	private Map<PDPage, SegmentedContents> documentSegmentation = new LinkedHashMap<PDPage, SegmentedContents>();
	public static String outputDirectory = "";
	public static String truthDirectory = "";
	public static String annoTruthDirectory = "";
	public static String externalResultDirectory = "";
	public static String internalResultDirectory = "";
	private static File outputModelFile;

	/**
	 * Processes the given pages of the document, if the pages list is null, it
	 * processes the whole document
	 * 
	 * @param doc
	 *            The PDF Document loaded as a PDDocument
	 * @param pagesList
	 *            The list of pages to be processed (if null, we process the
	 *            whole document)
	 * @param outputStream
	 *            Null if
	 * @param segments
	 *            The array of segments to be represented in the output
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public void processDocument(final PDDocument doc,
			final List<Integer> pagesList, final Segment... segments)
					throws IOException {
		documentSegmentation = new LinkedHashMap<PDPage, SegmentedContents>();
		FontStyle.initFontStylesMap();
		allPages = document.getDocumentCatalog().getAllPages();

		if (pagesList == null) {
			// We process all the pages of the document
			final Iterator<PDPage> pageIter = allPages.iterator();
			int i = 0;
			while (pageIter.hasNext()) {
				final PDPage currentPage = pageIter.next();
				processPage(currentPage, i);
				i++;
			}
		} else {
			// some specific pages were provided
			for (int i = 0; i < pagesList.size(); i++) {
				final int pageNumber = pagesList.get(i);
				final PDPage pageToProcess = allPages.get(pageNumber);
				processPage(pageToProcess, pageNumber);
			}
		}
	}

	/**
	 * Segments the page given as argument
	 * 
	 * @param page
	 *            The current processed page
	 * @param pageNumber
	 *            The number of the current page
	 * @throws IOException
	 */
	protected void processPage(final PDPage page, final int pageNumber)
			throws IOException {
		lines = new ArrayList<Line>();
		glyphsList = new ArrayList<Glyph>();
		blockFragments = new ArrayList<Block>();
		groups = new ArrayList<BlockGroup>();

		currentPage = page;
		processStream(page, page.findResources(), page.getContents()
				.getStream());

		extractLines();
		// cleanUpLines();
		detectHorizontalSplit();
		groupBlocks();

		final SegmentedContents pageSegmentation = new SegmentedContents(
				glyphsList, blockFragments, groups);
		documentSegmentation.put(currentPage, pageSegmentation);
	}

	/**
	 * Groups the different extracted blocks into groups of blocks. This method
	 * loops over all the blocks, trying to group them to other ones, based on
	 * some statistical analysis.
	 * 
	 */
	private void groupBlocks() {

		LOGGER.trace("Grouping blocks.");
		boolean changing = true;
		while (changing) {
			// while we continue grouping blocks into groups
			changing = false;

			for (final Block blockToGroup : blockFragments) {
				boolean groupFound = false;

				// if(blockToGroup.toString().startsWith("currently available")){
				// System.out.println(blockToGroup.toString());
				// }

				for (int i = 0; i < blockFragments.size() && !groupFound; i++) {
					final Block currentBlock = blockFragments.get(i);

					// if(currentBlock.toString().startsWith("HEC publications are")){
					// System.out.println(currentBlock.toString());
					// }

					if (currentBlock != blockToGroup
							&& currentBlock.getGroup() != null) {
						if (blockToGroup.hasSameStyle(currentBlock)) {
							// if both blocks have the same style, we continue
							// to search if they can be grouped together
							final double maxTop = Math.max(
									currentBlock.getTop(),
									blockToGroup.getTop());
							final double minBottom = Math.min(
									currentBlock.getBottom(),
									blockToGroup.getBottom());
							final double currentVerticalSpace = maxTop
									- minBottom;
							double averageVerticalSpace = 0.;

							if (currentBlock.getGroup() != null) {
								averageVerticalSpace = currentBlock.getGroup()
										.getAverageVerticalSpace();
							}

							if (currentBlock.getGroup() == null
									|| Double.isNaN(averageVerticalSpace)) {
								averageVerticalSpace = getAverageVerticalSpace(getMaxVerticalSpace());
							}

							// group two blocks together when
							// if the vertical space is lower than the average
							if (currentVerticalSpace < averageVerticalSpace
									|| Math.abs(Math.abs(currentVerticalSpace)
											- averageVerticalSpace) < 0.1) {
								// if the vertical space is lower than the
								// average(and still below a certain threshold),
								// obviously we try to group those
								// blocks otherwise, we check if the difference
								// between both is almost 0
								final double maxLeft = Math.max(
										currentBlock.getLeft(),
										blockToGroup.getLeft());
								final double minRight = Math.min(
										currentBlock.getRight(),
										blockToGroup.getRight());

								if (minRight > maxLeft) {
									// if the two lines overlap vertically
									final BlockGroup currentGroup = currentBlock
											.getGroup();

									if (blockToGroup.getGroup() != currentGroup) {
										if (blockToGroup.getGroup() != null) {
											final BlockGroup groupToDelete = blockToGroup
													.getGroup();
											currentGroup.addBlocks(blockToGroup
													.getGroup().getBlocks());
											groups.remove(groupToDelete);
										} else {
											currentGroup.addBlock(blockToGroup);
										}
										groupFound = true;
										changing = true;
									}
								}
							}
						}
					}
				}
				if (!groupFound && blockToGroup.getGroup() == null) {
					changing = true;
					final BlockGroup group = new BlockGroup(blockToGroup);
					groups.add(group);
					blockToGroup.setGroup(group);
				}
			}
		}

		// enforce reading order of block groups
		// sort block groups horizontally and then vertically
		// which means block groups with smaller Left coordinate should be read
		// first
		// block groups which same left coordinate, the larger the top
		// coordinate,
		// the later it should be read
		// double averageBlockGroupWidth = getAverageBlockGroupWidth();
		BlockGroupCoordinateComparator blockGroupComparator = new BlockGroupCoordinateComparator(
				groups);
		Collections.sort(groups, blockGroupComparator);

	}

	/**
	 * Detects if an extracted line should be splitted into two line fragments
	 * (in the case of a multi-column document)
	 */
	private void detectHorizontalSplit() {

		LOGGER.trace("Splitting lines");
		final double maxVerticalSpace = getMaxVerticalSpace();
		final double averageVerticalSpace = getAverageVerticalSpace(maxVerticalSpace);
		final double verticalSpaceVariance = getVerticalSpaceVariance(
				averageVerticalSpace, maxVerticalSpace);

		for (int lineIndex = 0; lineIndex < lines.size(); lineIndex++) {
			Line line = lines.get(lineIndex);
			double averageLineHorizontalSpacing = line
					.getAverageHorizontalSpacing(false,
							HORIZONTAL_OUTLIER_PARAM);
			double averageLinePunctationSpacing = line
					.getAverageHorizontalSpacing(true, HORIZONTAL_OUTLIER_PARAM);
			double lineHorizontalVariance = line.getHorizontalVariance(
					averageLineHorizontalSpacing, HORIZONTAL_OUTLIER_PARAM);

			if (line.toString().startsWith("Winform you")) {
				System.out.println(line.toString());
			}

			// we loop over all the glyphs contained in the current line
			Glyph precedingGlyph = line.getGlyphs().get(0);
			Line lineFragment = new Line(precedingGlyph);

			for (int i = 1; i < line.getGlyphs().size(); i++) {

				while (i < line.getGlyphs().size()
						&& line.getGlyphs().get(i).getCharacter().equals(" ")) {
					i++;
				}

				if (i < line.getGlyphs().size()) {
					Glyph currentGlyph = line.getGlyphs().get(i);

					if (!currentGlyph.getCharacter().equals(" ")) {
						// The last character of the line is a not a space
						final double currentSpaceWidth = currentGlyph.getLeft()
								- precedingGlyph.getRight();

						// considering split
						// if:
						// (1) the currentSpaceWidth is greater than
						// average and the difference is obvious enough
						// (2) the precedingGlyph is not a bullet
						if (!Bullet.isBullet(lineFragment.toString(),
								lineIndex, lines, currentPage)
								&& currentSpaceWidth
								- averageLineHorizontalSpacing > LINE_SPLIT_PARAM
								* getSpacingTolerance()) {
							// if the current horizontal spacing is
							// greater than the average spacing
							if (Punctuation.isPunctuation(precedingGlyph)) {
								if (!Character.isUpperCase(currentGlyph
										.getCharacter().charAt(0))) {
									// if the preceding glyph is a point (. or ?
									// or !) and the current one is not upper
									// case, that means that they do not belong
									// to the same line fragment
									findSuitableBlock(lineFragment,
											verticalSpaceVariance,
											averageVerticalSpace,
											lineHorizontalVariance,
											averageLineHorizontalSpacing);
									lineFragment = new Line(currentGlyph);
									// split if:
									// (1) currentSpaceWidth -
									// averageLinePunctationSpacing >
									// threshold.(when there are more than
									// one punctuations in the line)
									// (2) enoughPunctuation==false &&
									// currentSpaceWidth is a lot bigger than
									// averageLineHorizontalSpacing(when there
									// is
									// only one punctuation)
								} else if (((currentSpaceWidth - averageLinePunctationSpacing) > LINE_SPLIT_PARAM
										* getSpacingTolerance())
										&& Math.pow(currentSpaceWidth
												- averageLineHorizontalSpacing,
												2) > lineHorizontalVariance) {
									// the current glyph is upper case, we check
									// if the current space is wider than the
									// avg after a punctuation
									findSuitableBlock(lineFragment,
											verticalSpaceVariance,
											averageVerticalSpace,
											lineHorizontalVariance,
											averageLineHorizontalSpacing);
									lineFragment = new Line(currentGlyph);
								} else if (!line.enoughPunctuationEvidence()
										&& currentSpaceWidth > line
										.getHorizontalMaxSpaceTolerance(averageLineHorizontalSpacing)) {
									// if the current glyph is upper case and
									// the spacing is not greater
									// than avg. But if there is only one
									// punctuation and
									// the currentSpace is a lot bigger than the
									// average. we
									// should also split.
									// multi column: e.g. for modern day living.
									// School
									findSuitableBlock(lineFragment,
											verticalSpaceVariance,
											averageVerticalSpace,
											lineHorizontalVariance,
											averageLineHorizontalSpacing);
									lineFragment = new Line(currentGlyph);

								} else {
									// if the current glyph is upper case and
									// the spacing is not greater than the avg
									// after a punctuation mark, we add the
									// glyph to the current line.
									lineFragment.addGlyph(currentGlyph);
								}
							} else {
								// There is a horizontal space that is wider
								// than the average one, this is a multi column
								// line so we split it into two new lines

								findSuitableBlock(lineFragment,
										verticalSpaceVariance,
										averageVerticalSpace,
										lineHorizontalVariance,
										averageLineHorizontalSpacing);
								lineFragment = new Line(currentGlyph);
							}
						} else {
							// the preceding glyph is a bullet or
							// The spacing is not big enough to be significant,
							// we do not split the current line at this point.
							lineFragment.addGlyph(currentGlyph);

						}
					}
					precedingGlyph = currentGlyph;
				}
			}

			// We will find a block in which the current line can be added.
			findSuitableBlock(lineFragment, verticalSpaceVariance,
					averageVerticalSpace, lineHorizontalVariance,
					averageLineHorizontalSpacing);
		}

		// Sort blocks according to reading orders
		final BlockCoordinateComparator topComparator = new BlockCoordinateComparator();
		Collections.sort(blockFragments, topComparator);
	}

	/**
	 * Given a line, this method finds a block in which we can add this line. If
	 * no block is found, a new one is created.
	 * 
	 * @param line
	 *            The line to add to a block
	 * @param verticalSpaceVariance
	 * @param averageVerticalSpace
	 * @param horizontalSpaceVariance
	 * @param averageHorizontalSpace
	 */
	private void findSuitableBlock(final Line line,
			final double verticalSpaceVariance,
			final double averageVerticalSpace,
			final double horizontalSpaceVariance,
			final double averageHorizontalSpace) {

		boolean blockFound = false;

		//		if (line.toString().startsWith("were met and")) {
		//			System.out.println(line.toString());
		//		}

		for (int j = 0; j < blockFragments.size() && !blockFound; j++) {

			final Block currentBlock = blockFragments.get(j);

			//			if (currentBlock.toString().startsWith("Instrumentation (MRI)")) {
			//				System.out.println(currentBlock.toString());
			//			}

			// the addition is possible only if the current line and block have
			// the same style
			boolean additionPossible = currentBlock.hasSameStyle(line);

			if (additionPossible) {
				double verticalSpace = 0.;
				if (line.getTop() > currentBlock.getBottom()) {
					verticalSpace = line.getTop() - currentBlock.getBottom();
				} else if (line.getBottom() < currentBlock.getTop()) {
					verticalSpace = currentBlock.getTop() - line.getBottom();
				}
				final double verticalSquaredDifference = Math.pow(verticalSpace
						- averageVerticalSpace, 2);

				double horizontalSpace = 0.;
				if (line.getLeft() > currentBlock.getRight()) {
					horizontalSpace = line.getLeft() - currentBlock.getRight();
				} else if (line.getRight() < currentBlock.getLeft()) {
					horizontalSpace = currentBlock.getLeft() - line.getRight();
				}
				final double horizontalSquaredDifference = Math.pow(
						horizontalSpace - averageHorizontalSpace, 2);

				// if the current line is directly below the current block and
				// if they belong to the same column
				// They are groupped together when if verticalSpace is
				// smaller than averageVerticalSpace
				if ((verticalSpace < averageVerticalSpace || verticalSpace
						- averageVerticalSpace < 0.1)
						|| verticalSpaceVariance - verticalSquaredDifference > VERTICAL_GROUP_PARAM
						* getSpacingTolerance()) {
					if (horizontalSpace <= averageHorizontalSpace
							|| horizontalSquaredDifference <= horizontalSpaceVariance) {
						// if there is an area next to the current line
						if (line.getRight() < currentBlock.getRight()) {
							additionPossible = RegionProcessing
									.isRegionEmpty(
											currentPage,
											line.getRight(),
											currentBlock.getBottom(),
											currentBlock.getRight()
											- line.getRight(),
											line.getBottom()
											- currentBlock.getBottom());
						}
						// if there is an area above the current line and right
						// from the currentBlock
						else if (currentBlock.getRight() < line.getRight()) {
							additionPossible = RegionProcessing.isRegionEmpty(
									currentPage, currentBlock.getRight(),
									currentBlock.getTop(), line.getRight()
									- currentBlock.getRight(),
									line.getTop() - currentBlock.getTop());
						}

						// if the preceding result returned true and if there is
						// an area left from the current line
						if (additionPossible
								&& line.getLeft() > currentBlock.getLeft()) {
							additionPossible = RegionProcessing
									.isRegionEmpty(
											currentPage,
											currentBlock.getLeft(),
											currentBlock.getBottom(),
											line.getLeft()
											- currentBlock.getLeft(),
											line.getBottom()
											- currentBlock.getBottom());
						}
						// if there is an area above the current line and left
						// from the current block
						else if (additionPossible
								&& currentBlock.getLeft() > line.getLeft()) {
							additionPossible = RegionProcessing.isRegionEmpty(
									currentPage, line.getLeft(),
									currentBlock.getTop(),
									currentBlock.getLeft() - line.getLeft(),
									line.getTop() - currentBlock.getTop());
						}
					} else {
						additionPossible = false;
					}
				} else {
					additionPossible = false;
				}
			}

			if (additionPossible) {

				// we check if there is text between the current block and the
				// current line (in case it was not detected before: too narrow
				// region for example)
				final double x = Math.min(currentBlock.getLeft(),
						line.getLeft());
				final double y = Math.min(currentBlock.getBottom(),
						line.getBottom());

				final double width = Math.max(currentBlock.getRight(),
						line.getRight())
						- x;
				final double height = Math.max(currentBlock.getTop(),
						line.getTop())
						- y;

				additionPossible = RegionProcessing.isRegionEmpty(currentPage,
						x, y, width, height);

			}

			if (additionPossible) {
				currentBlock.addLine(line);
				blockFound = true;
			}
		}
		if (!blockFound) {
			final Block b = new Block(line);
			blockFragments.add(b);
		}
	}


	/**
	 * Calculates and returns the vertical space variance of the current page
	 * 
	 * @param mean
	 *            The vertical space mean that was calculated before
	 * @return The vertical variance
	 */
	private double getVerticalSpaceVariance(final double mean,
			double spaceOutlier) {
		double squaredDifferencesSum = 0.;
		double variance = 0.;
		int totalBlanks = 0;

		if (lines.size() > 1) {

			Line precedingLine = lines.get(1);

			for (int i = 2; i < lines.size() - 1; i++) {
				final Line currentLine = lines.get(i);
				final double currentSpaceHeight = currentLine.getTop()
						- precedingLine.getBottom();
				if (currentSpaceHeight > 0
						&& currentSpaceHeight < spaceOutlier
						/ VERTICAL_OUTLIER_PARAM) {
					if (totalBlanks == 0
							|| (totalBlanks != 0 && (squaredDifferencesSum + Math
									.pow(currentSpaceHeight - mean, 2))
									/ (totalBlanks + 1) < 10
									* squaredDifferencesSum / totalBlanks)) {
						squaredDifferencesSum += Math.pow(currentSpaceHeight
								- mean, 2);
						totalBlanks++;
					}

				}
				precedingLine = currentLine;

			}

			variance = squaredDifferencesSum / totalBlanks;
		}

		return variance;
	}

	/**
	 * Based on the list of extracted glyphs, this method gather them into
	 * lines, based on overlapping analysis
	 */
	private void extractLines() {

		LOGGER.trace("Extracting lines.");
		for (final Glyph currentGlyph : glyphsList) {
			int equationFlag = 0;
			boolean lineFound = false;

			lineFound = extractLineFromGlyph(equationFlag, lineFound,
					currentGlyph, false);

			equationFlag = 0;
			lineFound = false;
		}

		// handle superscript
		/*
		 * for (int i = 0; i < lines.size(); i++) { Line line = lines.get(i); if
		 * (line.getBottom() - line.getTop() < 4 && line.getGlyphs().size() < 3)
		 * { int equationFlag = 0; boolean lineExist = true; lines.remove(line);
		 * for (Glyph glyph : line.getGlyphs()) { boolean lineFound = false; if
		 * (extractLineFromGlyph(equationFlag, lineFound, glyph, true) == false)
		 * { lineExist = false; break; } } if (!lineExist) { lines.add(i, line);
		 * } else { i--; } } }
		 */

		// sort the lines according to reading order
		// there is a chance that the column lines have already formed formed
		// initially
		final LineCoordinateComparator topComparator = new LineCoordinateComparator(
				lines);
		Collections.sort(lines, topComparator);
		// sort glyphs in each line according to their horizontal sequence
		LeftGlyphCoordinateComparator leftCoordinateComparator = new LeftGlyphCoordinateComparator();
		for (Line line : lines) {
			Collections.sort(line.getGlyphs(), leftCoordinateComparator);
		}
	}

	private boolean extractLineFromGlyph(int equationFlag, boolean lineFound,
			Glyph currentGlyph, boolean script) {
		for (int i = 0; i < lines.size() && !lineFound; i++) {
			final Line currentLine = lines.get(i);

			if (currentGlyph.getTop() <= currentLine.getTop()
					&& currentLine.getTop() <= currentGlyph.getBottom()
					&& currentGlyph.getBottom() <= currentLine.getBottom()) {
				equationFlag = 1;
			} else if (currentLine.getTop() <= currentGlyph.getTop()
					&& currentGlyph.getTop() <= currentLine.getBottom()
					&& currentLine.getBottom() <= currentGlyph.getBottom()) {
				equationFlag = 2;
			} else if (currentLine.getTop() <= currentGlyph.getTop()
					&& currentGlyph.getTop() <= currentGlyph.getBottom()
					&& currentGlyph.getBottom() <= currentLine.getBottom()) {
				equationFlag = 3;
			} else if (currentGlyph.getTop() <= currentLine.getTop()
					&& currentLine.getTop() <= currentLine.getBottom()
					&& currentLine.getBottom() <= currentGlyph.getBottom()) {
				equationFlag = 4;
			}

			if (equationFlag != 0) {
				// that means that we found a line overlapping with the
				// current glyph

				final double line_threshold = 0.5;
				// delta helps to calculate the horizontal overlap ratio
				final double delta = Math.min(currentLine.getBottom()
						- currentLine.getTop(), currentGlyph.getBottom()
						- currentGlyph.getTop());

				double horizontal_overlap_ratio = 0.;

				switch (equationFlag) {
				case 1:
					horizontal_overlap_ratio = (currentGlyph.getBottom() - currentLine
							.getTop()) / delta;
					break;
				case 2:
					horizontal_overlap_ratio = (currentLine.getBottom() - currentGlyph
							.getTop()) / delta;
					break;
				case 3:
					horizontal_overlap_ratio = (currentGlyph.getBottom() - currentGlyph
							.getTop()) / delta;
					break;
				case 4:
					horizontal_overlap_ratio = (currentLine.getBottom() - currentLine
							.getTop()) / delta;
					break;
				default:
					LOGGER.warn("Invalid value for the equation flag.");
					break;
				}

				if (horizontal_overlap_ratio >= line_threshold) {
					/*
					 * || (script && horizontal_overlap_ratio > 0 &&
					 * currentGlyph .getFontSize() <= currentLine .getFontSize()
					 * / 2)) {
					 */
					// the current glyph is added to the currentLine and we
					// update left, right, top and bottom coordinates if
					// needed
					currentLine.getGlyphs().add(currentGlyph);
					lineFound = true;

					if (currentGlyph.getTop() <= currentLine.getTop()) {
						currentLine.setTop(currentGlyph.getTop());
					}
					if (currentGlyph.getBottom() >= currentLine.getBottom()) {
						currentLine.setBottom(currentGlyph.getBottom());
					}
					if (currentGlyph.getRight() >= currentLine.getRight()) {
						currentLine.setRight(currentGlyph.getRight());
					}
					if (currentGlyph.getLeft() <= currentLine.getLeft()) {
						currentLine.setLeft(currentGlyph.getLeft());
					}
				}
			}
		}

		if (!lineFound && !script) {
			final Line newLine = new Line(currentGlyph);
			lines.add(newLine);
		}
		return lineFound;
	}

	private double getMaxVerticalSpace() {
		// find the vertical space of the first and last line on the page
		if (lines.size() == 0) {
			return 0;
		}

		double minTop, maxTop;
		minTop = Double.MAX_VALUE;
		maxTop = Double.MIN_VALUE;

		for (final Line currentLine : lines) {
			if (currentLine.getTop() < minTop) {
				minTop = currentLine.getTop();
			}
			if (currentLine.getTop() > maxTop) {
				maxTop = currentLine.getTop();
			}
		}

		return maxTop - minTop;
	}

	/**
	 * Calculates the average vertical space height
	 * 
	 * @return The average vertical space height
	 */
	private double getAverageVerticalSpace(double spaceDiffFirstLastLine) {
		double averageSpacing = 0.;

		double spacing = 0.;
		int totalBlanks = 0;

		// First, we sort the lines list in order to have the lines in a reading
		// order
		final LineCoordinateComparator topComparator = new LineCoordinateComparator(
				lines);
		Collections.sort(lines, topComparator);

		if (!lines.isEmpty()) {
			final List<Column> columns = new ArrayList<Column>();

			// don't consider the first line and the last line
			for (int i = 1; i < lines.size() - 1; i++) {
				Line currentLine = lines.get(i);
				double minSpacing = Double.MAX_VALUE;
				Column minColumn = null;

				for (final Column column : columns) {
					final Line lastLine = column.getLines().get(
							column.getLines().size() - 1);
					final double currentSpacing = currentLine.getTop()
							- lastLine.getBottom();
					final double maxLeft = Math.max(currentLine.getLeft(),
							lastLine.getLeft());
					final double minRight = Math.min(currentLine.getRight(),
							lastLine.getRight());

					if (currentSpacing > 0 && currentSpacing < minSpacing
							&& minRight > maxLeft) {
						// minRight > maxLeft means that the last line of the
						// current column and the current line are not
						// explicitly in two different columns (vertical
						// overlap)
						minSpacing = currentSpacing;
						minColumn = column;
					}
				}

				if (minColumn == null) {
					columns.add(new Column(currentLine));
				} else {
					minColumn.addLine(currentLine);
				}
			}

			for (final Column c : columns) {
				Line precedingLine = c.getLines().get(0);

				for (int i = 1; i < c.getLines().size(); i++) {
					final Line currentLine = c.getLines().get(i);
					double currentSpacing = currentLine.getTop()
							- precedingLine.getBottom();

					// we need to remove outliers
					if (currentSpacing < spaceDiffFirstLastLine
							/ VERTICAL_OUTLIER_PARAM) {
						if (totalBlanks == 0
								|| (totalBlanks != 0 && (spacing + currentSpacing)
								/ (totalBlanks + 1) < 10 * spacing
								/ totalBlanks)) {
							spacing += currentSpacing;
							totalBlanks++;
						}
					}
					precedingLine = currentLine;
				}
			}

			averageSpacing = spacing / totalBlanks;
		}

		return averageSpacing;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.pdfbox.util.PageDrawer#processTextPosition(org.apache
	 * .pdfbox.util.TextPosition)
	 */
	@Override
	protected void processTextPosition(final TextPosition text) {
		final float x0 = text.getXDirAdj();
		final float x1 = x0 + text.getWidthDirAdj();
		final float y0 = text.getYDirAdj();
		final float y1 = y0 + text.getHeightDir();

		final GlyphPositionComparator comparator = new GlyphPositionComparator();
		try {
			Color color = this.getGraphicsState().getNonStrokingColor()
					.getJavaColor();

			glyphsList.add(new Glyph(x0, x1, y0, y1, text.getCharacter(), text
					.getFontSizeInPt(), text.getFont(), color.getRGB()));

			Collections.sort(glyphsList, comparator);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get the size of the page that is currently being drawn.
	 * 
	 * @see org.apache.pdfbox.util.PageDrawer#getPageSize
	 * @return The size of the page that is being drawn.
	 */
	/*
	 * @Override public Dimension getPageSize() { double height =
	 * currentPage.getMediaBox().getHeight(); double width =
	 * currentPage.getMediaBox().getWidth(); return new Dimension((int)width,
	 * (int)height); }
	 */

	/**
	 * Draw the AWT image. Called by Invoke. Moved into PageDrawer so that
	 * Invoke doesn't have to reach in here for Graphics as that breaks
	 * extensibility.
	 * 
	 * @see org.apache.pdfbox.util.PageDrawer#drawImage
	 * @param awtImage
	 *            The image to draw.
	 * @param at
	 *            The transformation to use when drawing.
	 * 
	 */

	/*
	 * @Override public void drawImage(Image awtImage, AffineTransform at) {
	 * 
	 * }
	 * 
	 * private double getSpacingTolerance(){ return 0.5; }
	 */

	@Override
	public Model segment(final InputStream inputStream,
			final Segment... segments) {
		try {
			System.out.println("Segmenting " + inputStream.toString());
			FontStyle.initFontStylesMap();
			document = getPDDocumentFromInput(inputStream);

			processDocument(document, null, segments);

			document.close();
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void segment(final InputStream inputStream,
			final OutputStream outputStream, final Segment... segments) {
		FontStyle.initFontStylesMap();
		try {
			document = getPDDocumentFromInput(inputStream);
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Model segment(final File inputFile, final Segment... segments) {
		FontStyle.initFontStylesMap();
		try {
			document = getPDDocumentFromInput(inputFile);
		} catch (final IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void segment(final File inputFile, final File outputFile,
			final Segment... segments) {
		try {
			LOGGER.trace("Segmenting " + inputFile.toString());

			FontStyle.initFontStylesMap();
			document = getPDDocumentFromInput(inputFile);
			docName = inputFile.getName();
			docName = docName.substring(0, docName.lastIndexOf("."));
			List<Integer> pagesList = null;

			if (EVALUATION || ANNO_SEGMENT_EVALUATION) {
				pagesList = appendEvaluationPages(inputFile);
			}

			processDocument(document, pagesList, segments);
			document.close();

			// if(EVALUATION){ Evaluation.evaluate(); }

			// clone the old documentSegmentation before we start refine them
			// because we want to show the difference at later stage
			Map<PDPage, SegmentedContents> oldSegmentation = copyDocumentSegmentation();

			// refine segmentation through annotation
			//TODO: uncomment this. We donly comment it to avoid errors in annotator.
			AnnotationSegmentor as = new AnnotationSegmentor(
					documentSegmentation);
			documentSegmentation = as.refineSegmentation();

			// evaluate segmentation result which has been optimized through
			// annotation:
			// (1)output the segmentation result to an xml file
			// (2)compare the output xml file against the ground truth
			if (ANNO_SEGMENT_EVALUATION || EVALUATION) {
				if (HIERARCHICAL_OUTPUT) {
					Map<PDPage, List<BlockGroup>> annoTitles = as.getAnnoTitles();
					BlockGroupHierarchyAnalyser hierarchyAnalyser = new BlockGroupHierarchyAnalyser();
					for (int pageNum : pagesList) {
						PDPage page = allPages.get(pageNum);
						hierarchyAnalyser.constructPageHierarchy(page,
								annoTitles.get(page),
								documentSegmentation.get(page).getGroups(),
								documentSegmentation.get(page).getGlyphs());
					}

					Map<PDPage, List<HieBlockGroup>> hierarchicalSegmentation = hierarchyAnalyser
							.getSegmentation();
					writeSegmentation(hierarchicalSegmentation, outputFile);
				} else {
					writeSegmentation(outputFile);
				}

			} else {
				//TODO
				//analyse the semantic structure of the pdf file
				SemanticStructureAnalyser sa = new SemanticStructureAnalyser(
						documentSegmentation, as.getTitles());
				Map<PDPage, List<SemanticStructureBlockGroup>> semanticStructures = sa
						.analyseSemanticStructure();
				// build output model for annotator.visualization
				OutputModel om = new OutputModel(docName, documentSegmentation,
						oldSegmentation, semanticStructures, 
						allPages, segments);
				Model analyserModel = om.buildModel();
				analyserModel.write(outputModelFile);
			}

		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Copy the old segmentation before we do any refinement through annotation
	 * We do this because we need to do a comparision later We only need to deep
	 * copy the block and blockgroup information because they are the only
	 * things we will compare
	 */
	private Map<PDPage, SegmentedContents> copyDocumentSegmentation() {
		Map<PDPage, SegmentedContents> oldSegmentation = new LinkedHashMap<PDPage, SegmentedContents>();
		for (PDPage curPage : documentSegmentation.keySet()) {
			List<BlockGroup> groups = new ArrayList<BlockGroup>();
			List<Block> blocks = new ArrayList<Block>();
			for (BlockGroup curGroup : documentSegmentation.get(curPage)
					.getGroups()) {
				BlockGroup group = null;
				for (Block curBlock : curGroup.getBlocks()) {
					Block block = new Block(curBlock.getLines());
					if (group == null) {
						group = new BlockGroup(block);
					} else {
						group.addBlock(block);
					}
					blocks.add(block);
				}
				groups.add(group);
			}
			SegmentedContents contents = new SegmentedContents(
					documentSegmentation.get(curPage).getGlyphs(), blocks,
					groups);
			oldSegmentation.put(curPage, contents);
		}
		return oldSegmentation;
	}

	private void writeSegmentation(
			Map<PDPage, List<HieBlockGroup>> hierarchicalSegmentation,
			File outputFile) throws IOException {

		// output the optimized documentSegmentation to xml file
		HierarchicalOutput output = new HierarchicalOutput(docName,
				outputDirectory, new FileOutputStream(outputFile));

		Iterator<PDPage> it = hierarchicalSegmentation.keySet().iterator();
		while (it.hasNext()) {
			PDPage curPage = it.next();
			List<HieBlockGroup> contents = hierarchicalSegmentation
					.get(curPage);
			output.addPage(allPages.indexOf(curPage), contents);
		}
		output.endProcess();
	}

	private void writeSegmentation(File outputFile) throws IOException {
		Segment[] segmentTypes = { Segment.BLOCK_GROUP };
		Output output = new Output(docName, outputDirectory,
				new FileOutputStream(outputFile), segmentTypes);

		Iterator<PDPage> it = documentSegmentation.keySet().iterator();
		while (it.hasNext()) {
			PDPage curPage = it.next();
			SegmentedContents contents = documentSegmentation.get(curPage);
			output.addPage(allPages.indexOf(curPage), contents);
		}
		output.endProcess();
	}

	private List<Integer> appendEvaluationPages(File inputFile) {
		List<Integer> pagesList = new ArrayList<Integer>();

		// EVALUATION ONLY (we provide some specific pages
		// to segment pagesList = new
		// ArrayList<Integer>();
		if (EVALUATION) {

			if (Files.easyFiles.containsKey(inputFile.getName())) {
				pagesList.addAll(Files.easyFiles.get(inputFile.getName()));
			}

			if (Files.normalFiles.containsKey(inputFile.getName())) {
				pagesList.addAll(Files.normalFiles.get(inputFile.getName()));
			}

			if (Files.nastyFiles.containsKey(inputFile.getName())) {
				pagesList.addAll(Files.nastyFiles.get(inputFile.getName()));
			}
		}

		// if we want to evaluate the segmentation result which has been
		// improved through annotation. We need to output the segmentation
		// as
		if (ANNO_SEGMENT_EVALUATION) {
			if (TrainingFiles.hamptonsFiles.containsKey(inputFile.getName())) {
				pagesList.addAll(TrainingFiles.hamptonsFiles.get(inputFile
						.getName()));
			}

			if (TrainingFiles.savillsFiles.containsKey(inputFile.getName())) {
				pagesList.addAll(TrainingFiles.savillsFiles.get(inputFile
						.getName()));
			}

			if (EvaluationFiles.singleColumn.containsKey(inputFile.getName())) {
				pagesList.addAll(EvaluationFiles.singleColumn.get(inputFile
						.getName()));
			}

			if (EvaluationFiles.multiColumn.containsKey(inputFile.getName())) {
				pagesList.addAll(EvaluationFiles.multiColumn.get(inputFile
						.getName()));
			}

			if (EvaluationFiles.irregularStyle.containsKey(inputFile.getName())) {
				pagesList.addAll(EvaluationFiles.irregularStyle.get(inputFile
						.getName()));
			}
		}
		return pagesList;
	}

	/**
	 * @param inputFile
	 * @return The document loaded as a PDDocument
	 * @throws IOException
	 */
	private PDDocument getPDDocumentFromInput(final File inputFile)
			throws IOException {
		return PDDocument.load(inputFile);
	}

	/**
	 * @param inputStream
	 * @return The document loaded as a PDDocument
	 * @throws IOException
	 */
	private PDDocument getPDDocumentFromInput(final InputStream inputStream)
			throws IOException {
		return PDDocument.load(inputStream);
	}

	/**
	 * Launches the system, trying to read the configuration file provided as
	 * argument argument
	 * 
	 * @param args
	 *            The resource path to the XML configuration file
	 * @throws IOException
	 */
	public static void main(final String args[]) throws IOException {
		final DocumentProcessor app = new DocumentProcessor();
		final Segment[] segments;
		Files.initFilesList();
		Files.initEvaluationFiles();
		TrainingFiles.initFilesList();
		TrainingFiles.initEvaluationFiles();
		EvaluationFiles.initFilesList();
		EvaluationFiles.initEvaluationList();


		URL configURL = DocumentProcessor.class
				.getResource("Configuration.xml");
		File configFile = new File(configURL.getFile());
		ProcessConfig config = new ProcessConfig(configFile);
		File inputFile = config.getInputFile();
		File outputFile = config.getOutputFile();
		outputModelFile = config.getOutputModelFile();
		segments = config.getSegments();
		DocumentProcessor.outputDirectory = config.getOutputPath();

		EVALUATION = config.getEvaluation();
		if (EVALUATION) {
			DocumentProcessor.truthDirectory = config.getTruthPath();
			Evaluation.evaluate();
		}

		ANNO_SEGMENT_EVALUATION = config.getAnnoEvaluation();
		HIERARCHICAL_OUTPUT = config.getHierarchicalAnno();
		// Comment this if you only want to generate the segmentation XML
		// output
		if (ANNO_SEGMENT_EVALUATION) {
			DocumentProcessor.annoTruthDirectory = config
					.getAnnoGroudTruthPath();
			DocumentProcessor.internalResultDirectory = config
					.getInternalResultPath();
			DocumentProcessor.externalResultDirectory = config
					.getExternalResultPath();

			AnnoSegEvaluation.evaluateInternalResult(HIERARCHICAL_OUTPUT);
			AnnoSegEvaluation.evaluateExternalResult(HIERARCHICAL_OUTPUT);

			System.exit(0);
		}

		app.segment(inputFile, outputFile, segments);


	}

	private static boolean EVALUATION;
	private static boolean ANNO_SEGMENT_EVALUATION;
	private static boolean HIERARCHICAL_OUTPUT;

	public Map<PDPage, SegmentedContents> getDocumentSegmentation() {
		return this.documentSegmentation;
	}

	/**
	 * Public method that given an input file, compute the original segmentation of the document
	 * and returns it as a map
	 * @param pdf_file	The file to be analyzed
	 * @return	A map where the key is a PDPage representing a PDF page and value is SegmentedContents,
	 * representing the segmented content of the page
	 * @throws IOException	If there are IO problems opening/closing the pdf file
	 */
	public Map<PDPage, SegmentedContents> getOriginalDocumentSegmentation(final File pdf_file) throws IOException{
		LOGGER.trace("Segmenting " + pdf_file.getName());

		final Segment[] segments = {Segment.GLYPH,Segment.BLOCK,Segment.LINE,Segment.BLOCK_GROUP};

		FontStyle.initFontStylesMap();
		document = getPDDocumentFromInput(pdf_file);
		docName = pdf_file.getName();
		docName = docName.substring(0, docName.lastIndexOf("."));
		List<Integer> pagesList = null;

		processDocument(document, pagesList, segments);
		document.close();

		return this.documentSegmentation;

	}

}
