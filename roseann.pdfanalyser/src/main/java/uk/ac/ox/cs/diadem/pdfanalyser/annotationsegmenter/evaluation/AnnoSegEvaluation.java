package uk.ac.ox.cs.diadem.pdfanalyser.annotationsegmenter.evaluation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import uk.ac.ox.cs.diadem.pdfanalyser.DocumentProcessor;
import uk.ac.ox.cs.diadem.pdfanalyser.evaluation.EvalResults;
import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;

public class AnnoSegEvaluation {
	private static String inputPath = "";
	private static Map<String, List<PDPage>> pages = null;
	private static String annoOutputPath = "";
	private static String annoTruthPath = "";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DocumentProcessor.class);

	/**
	 * Evaluates the system on the test files
	 * 
	 * @return An {@link EvalResults} object containing the purity and
	 *         completeness of the extraction
	 */
	public static EvalResults evaluate() {
		annoOutputPath = DocumentProcessor.outputDirectory;
		annoTruthPath = DocumentProcessor.annoTruthDirectory;

		pages = new HashMap<String, List<PDPage>>();
		initPagesMap(AnnoSegFiles.hamptonsFiles);
		initPagesMap(AnnoSegFiles.savillsFiles);

		final EvalResults hamptonsResults = evaluate(AnnoSegFiles.hamptonsFiles);
		final EvalResults savillsResults = evaluate(AnnoSegFiles.savillsFiles);

		final float globalCompleteness = (hamptonsResults.getCompleteness() + savillsResults
				.getCompleteness()) / (float) 2;
		final float globalPurity = (hamptonsResults.getPurity() + savillsResults
				.getPurity()) / (float) 2;
		final float globalPrecision = (hamptonsResults.getPrecision() + savillsResults
				.getPrecision()) / (float) 2;
		final float globalRecall = (hamptonsResults.getRecall() + savillsResults
				.getRecall()) / (float) 2;
		final EvalResults globalResults = new EvalResults(globalCompleteness,
				globalPurity, globalPrecision, globalRecall);

		return globalResults;
	}

	@SuppressWarnings("unchecked")
	private static void initPagesMap(Map<String, ArrayList<Integer>> filesMap) {
		for (String fileName : filesMap.keySet()) {
			String pdfFile = getInputPath() + fileName;
			PDDocument pdfDoc;
			try {
				pdfDoc = PDDocument.load(new File(pdfFile));
				List<PDPage> allPages = pdfDoc.getDocumentCatalog()
						.getAllPages();
				pages.put(fileName, allPages);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param filesMap
	 *            Maps the files we want to use to the pages that will be tested
	 * @return An {@link EvalResults} object containing the purity and
	 *         completeness of the map given as input
	 */
	private static EvalResults evaluate(Map<String, ArrayList<Integer>> filesMap) {
		final EvalResults results;
		int totalExtractedElements = 0;
		int totalExistingElements = 0;
		int totalPurity = 0;
		int totalCompleteness = 0;
		int totalCorrectRetrievedElements = 0;
		List<BlockGroup> pageExtractedGroups = new ArrayList<BlockGroup>();
		List<BlockGroup> pageExistingGroups = new ArrayList<BlockGroup>();
		final DocumentBuilderFactory dbFactoryOutput = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder dBuilderOutput;
		final DocumentBuilderFactory dbFactoryTruth = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder dBuilderTruth;

		for (String fileName : filesMap.keySet()) {
			List<PDPage> allPages = pages.get(fileName);
			int documentCompleteness = 0;
			int documentPurity = 0;
			int documentExtractedElements = 0;
			int documentExistingElements = 0;
			int documentCorrectRetrievedElements = 0;
			String outputFileName = annoOutputPath
					+ fileName.substring(0, fileName.lastIndexOf("."))
					+ "_output.xml";
			String truthFileName = annoTruthPath + fileName + "_truth.xml";
			File outputFile = new File(outputFileName);
			File truthFile = new File(truthFileName);

			try {
				dBuilderOutput = dbFactoryOutput.newDocumentBuilder();
				Document outputTree = dBuilderOutput.parse(outputFile);
				outputTree.getDocumentElement().normalize();

				dBuilderTruth = dbFactoryTruth.newDocumentBuilder();
				Document truthTree = dBuilderTruth.parse(truthFile);
				truthTree.getDocumentElement().normalize();

				NodeList groups = outputTree.getDocumentElement()
						.getElementsByTagName("group");

				// The first step is to add all the extracted and real block
				// groups to the corresponding lists
				if (groups.getLength() < 1) {
					// if there are no <block> elements in the output we cannot
					// perform the evaluation
					LOGGER.error("The evaluation must be run at the BLOCK GROUP level. "
							+ "There is no <group> element in the output "
							+ outputFileName);
				} else {
					NodeList pagesList = outputTree.getDocumentElement()
							.getElementsByTagName("page");
					for (int i = 0; i < pagesList.getLength(); i++) {
						Element currentPage = (Element) pagesList.item(i);
						String pageNumber = currentPage.getAttribute("number");

						if (filesMap.get(fileName).contains(
								Integer.parseInt(pageNumber))) {
							int pageCompleteness = 0;
							int pagePurity = 0;
							int pageExtractedElements = 0;
							int pageExistingElements = 0;
							int pageCorrectRetrievedElements = 0;
							pageExistingGroups = new ArrayList<BlockGroup>();
							pageExtractedGroups = new ArrayList<BlockGroup>();
							groups = currentPage.getElementsByTagName("group");

							for (int j = 0; j < groups.getLength(); j++) {
								// we add all the blocks of the output to the
								// extracted blocks list
								Node currentGroup = groups.item(j);
								float top = Float.parseFloat(currentGroup
										.getAttributes().getNamedItem("top")
										.getTextContent());
								float bottom = Float.parseFloat(currentGroup
										.getAttributes().getNamedItem("bottom")
										.getTextContent());
								float left = Float.parseFloat(currentGroup
										.getAttributes().getNamedItem("left")
										.getTextContent());
								float right = Float.parseFloat(currentGroup
										.getAttributes().getNamedItem("right")
										.getTextContent());
								pageExtractedGroups.add(new BlockGroup(top,
										bottom, left, right));
							}

							// we add all the real blocks to the existing blocks
							// list
							NodeList truthPages = truthTree
									.getElementsByTagName("page");
							boolean pageFound = false;

							for (int j = 0; j < truthPages.getLength()
									&& !pageFound; j++) {
								if (pageNumber.equals(truthPages.item(j)
										.getAttributes().getNamedItem("number")
										.getTextContent())) {
									currentPage = (Element) truthPages.item(j);
									pageFound = true;
								}
							}

							if (!pageFound) {
								// This page was not specified in the ground
								// truth
								// file
								LOGGER.error("Page number "
										+ pageNumber
										+ " does not exist in the groung truth file.");
								System.exit(1);
							}

							float pageHeight = allPages
									.get(Integer.parseInt(pageNumber))
									.getMediaBox().getHeight();

							groups = currentPage.getElementsByTagName("block");

							for (int j = 0; j < groups.getLength(); j++) {
								// we add all the blocks of the output to the
								// extracted blocks list
								Node currentGroup = groups.item(j);
								// adjust top and bottom by 7 and 5 because the
								// tools we used to generate ground truth has
								// inaccurate shift
								float top = Float.parseFloat(currentGroup
												.getAttributes()
												.getNamedItem("top")
												.getTextContent());
								float bottom = Float.parseFloat(currentGroup
												.getAttributes()
												.getNamedItem("bottom")
												.getTextContent());
								float left = Float.parseFloat(currentGroup
										.getAttributes().getNamedItem("left")
										.getTextContent());
								float right = Float.parseFloat(currentGroup
										.getAttributes().getNamedItem("right")
										.getTextContent());
								pageExistingGroups.add(new BlockGroup(top,
										bottom, left, right));
							}

							pageExistingElements = pageExistingGroups.size();
							pageExtractedElements = pageExtractedGroups.size();

							for (int j = 0; j < pageExtractedGroups.size(); j++) {
								BlockGroup extracted = pageExtractedGroups
										.get(j);
								for (int k = 0; k < pageExistingGroups.size(); k++) {
									BlockGroup existing = pageExistingGroups
											.get(k);
									if (extracted.contains(existing)) {
										pageCompleteness++;
									}

									if (existing.contains(extracted)) {
										pagePurity++;
									}

									if (extracted.isIdentical(existing)) {
										pageCorrectRetrievedElements++;
									}
								}
							}

							documentCorrectRetrievedElements += pageCorrectRetrievedElements;
							documentExtractedElements += pageExtractedElements;
							documentExistingElements += pageExistingElements;
							documentCompleteness += pageCompleteness;
							documentPurity += pagePurity;

						}
					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			totalCorrectRetrievedElements += documentCorrectRetrievedElements;
			totalCompleteness += documentCompleteness;
			totalPurity += documentPurity;
			totalExtractedElements += documentExtractedElements;
			totalExistingElements += documentExistingElements;
		}

		final float completeness = totalCompleteness
				/ (float) totalExistingElements;
		final float purity = totalPurity / (float) totalExtractedElements;

		final float precision = totalCorrectRetrievedElements
				/ (float) totalExtractedElements;
		final float recall = totalCorrectRetrievedElements
				/ (float) totalExistingElements;
		results = new EvalResults(completeness, purity, precision, recall);
		return results;
	}

	public static String getInputPath() {
		return inputPath;
	}

	public static void setInputPath(String inputPath) {
		AnnoSegEvaluation.inputPath = inputPath;
	}

}
