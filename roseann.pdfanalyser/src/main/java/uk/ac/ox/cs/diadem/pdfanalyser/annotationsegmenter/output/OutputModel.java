package uk.ac.ox.cs.diadem.pdfanalyser.annotationsegmenter.output;

import java.io.OutputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDPage;

import uk.ac.ox.cs.diadem.model.Model;
import uk.ac.ox.cs.diadem.model.ModelFactory;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;
import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Segment;
import uk.ac.ox.cs.diadem.pdfanalyser.util.SegmentedContents;

public class OutputModel {
	private final String docName;
	private final Map<PDPage, SegmentedContents> documentSegmentation;
	private final Map<PDPage, SegmentedContents> originalSegmentation;
	private final List<PDPage> allPages;
	private final Segment[] segments;

	/**
	 * @param document
	 *            The document that will be visualised
	 * @param segmentedContents
	 *            The map of the segmented contents of the processed pages
	 * @param outputStream
	 *            A reference to the output stream
	 * @param segments
	 *            The array of segments that will be visualised (block, group,
	 *            line, glyph)
	 */
	public OutputModel(String docName,
			Map<PDPage, SegmentedContents> documentSegmentation,
			Map<PDPage, SegmentedContents> originalSegmentation,
			List<PDPage> allPages, Segment... segments) {
		this.docName = docName;
		this.documentSegmentation = documentSegmentation;
		this.originalSegmentation = originalSegmentation;
		this.allPages = allPages;
		this.segments = segments;
	}

	public Model buildModel() {
		Model m = ModelFactory.createIsolatedModel();
		int pageNum = 0;

		// write segmentations into model
		for (PDPage curPage : allPages) {
			SegmentedContents contents = documentSegmentation.get(curPage);
			SegmentedContents originalContents = originalSegmentation.get(curPage);

			if (contents == null) {
				pageNum++;
				continue;
			}

			// write block group
			if (Arrays.asList(segments).contains(Segment.BLOCK_GROUP)) {
				appendOriginalBlockGroups(pageNum, originalContents.getGroups(), m);
				appendBlockGroups(pageNum, contents.getGroups(), m);
			}
			// write block
			if (Arrays.asList(segments).contains(Segment.BLOCK)) {
				appendOriginalBlock(pageNum, originalContents.getBlocks(), m);
				appendBlock(pageNum, contents.getBlocks(), m);
			}
			// write line
			if (Arrays.asList(segments).contains(Segment.LINE)) {
				appendLine(pageNum, contents.getLines(), m);
			}
			// write glyph
			if (Arrays.asList(segments).contains(Segment.GLYPH)) {
				appendGlyph(pageNum, contents.getGroups(), m);
			}
			// write clob
			appendClob(pageNum, contents.getGroups(), m);

			pageNum++;
		}

		return m;
	}

	private void appendClob(int pageNum, List<BlockGroup> groups, Model m) {
		StringBuilder text = new StringBuilder();
		for (int i = 0; i < groups.size(); i++) {
			String groupText = groups.get(i).toString();
			text.append(groupText);
		}
		m.add("Clob", m.getConstantTerm("clob" + pageNum),
				m.getConstantTerm(docName),
				m.getConstantTerm("page" + pageNum),
				m.getStringLiteral(text.toString()));
	}

	private void appendGlyph(int pageNum, List<BlockGroup> groups, Model m) {
		int glyphPos = 0;
		for (int i = 0; i < groups.size(); i++) {
			BlockGroup group = groups.get(i);
			List<Line> lines = group.getLines();

			for (Line line : lines) {
				List<Glyph> glyphs = line.getGlyphs();
				Glyph precedingGlyph = glyphs.get(0);
				m.add("Glyph",
						m.getConstantTerm("blockGroup" + i + "glyph" + 0),
						m.getConstantTerm(docName),
						m.getConstantTerm("page" + pageNum),
						m.getStringLiteral("" + precedingGlyph.getLeft()),
						m.getStringLiteral("" + precedingGlyph.getTop()),
						m.getStringLiteral("" + precedingGlyph.getRight()),
						m.getStringLiteral("" + precedingGlyph.getBottom()),
						m.getIntegerLiteral(glyphPos),
						m.getIntegerLiteral(glyphPos + 1));
				// some glyph contains multiple characters
				glyphPos += precedingGlyph.getCharacter().length();

				for (int j = 1; j < glyphs.size(); j++) {
					final Glyph g = glyphs.get(j);
					if (g.getLeft() - precedingGlyph.getRight() > Line.SPACING_TOLERANCE) {
						glyphPos++;
					}

					m.add("Glyph",
							m.getConstantTerm("blockGroup" + i + "glyph" + j),
							m.getConstantTerm(docName),
							m.getConstantTerm("page" + pageNum),
							m.getStringLiteral("" + g.getLeft()),
							m.getStringLiteral("" + g.getTop()),
							m.getStringLiteral("" + g.getRight()),
							m.getStringLiteral("" + g.getBottom()),
							m.getIntegerLiteral(glyphPos),
							m.getIntegerLiteral(glyphPos + 1));

					precedingGlyph = g;
					glyphPos += precedingGlyph.getCharacter().length();
				}

				// add white space between different lines
				glyphPos++;
			}
		}
	}

	private void appendLine(int pageNum, List<Line> lines, Model m) {
		for (int i = 0; i < lines.size(); i++) {
			Line line = lines.get(i);
			double verticalShift = line.getBottom() - line.getTop();
			m.add("Line", m.getConstantTerm("line" + i),
					m.getConstantTerm(docName),
					m.getConstantTerm("page" + pageNum),
					m.getStringLiteral("" + line.getLeft()),
					m.getStringLiteral("" + line.getTop()),
					m.getStringLiteral("" + line.getRight()),
					m.getStringLiteral("" + line.getBottom()),
					m.getStringLiteral("" + verticalShift));
		}
	}

	private void appendBlock(int pageNum, List<Block> blocks, Model m) {
		for (int i = 0; i < blocks.size(); i++) {
			Block block = blocks.get(i);
			Line firstLine = block.getLines().get(0);
			double verticalShift = firstLine.getBottom() - firstLine.getTop();
			m.add("Block", m.getConstantTerm("block" + i),
					m.getConstantTerm(docName),
					m.getConstantTerm("page" + pageNum),
					m.getStringLiteral("" + block.getLeft()),
					m.getStringLiteral("" + block.getTop()),
					m.getStringLiteral("" + block.getRight()),
					m.getStringLiteral("" + block.getBottom()),
					m.getStringLiteral("" + verticalShift));
		}
	}

	private void appendBlockGroups(int pageNum, List<BlockGroup> groups, Model m) {
		for (int i = 0; i < groups.size(); i++) {
			BlockGroup group = groups.get(i);
			System.out.println(group.toString());
			Line firstLine = group.getBlocks().get(0).getLines().get(0);
			double verticalShift = firstLine.getBottom() - firstLine.getTop();
			m.add("BlockGroup", m.getConstantTerm("blockGroup" + i),
					m.getConstantTerm(docName),
					m.getConstantTerm("page" + pageNum),
					m.getStringLiteral("" + group.getLeft()),
					m.getStringLiteral("" + group.getTop()),
					m.getStringLiteral("" + group.getRight()),
					m.getStringLiteral("" + group.getBottom()),
					m.getStringLiteral("" + verticalShift));
		}
	}
	
	private void appendOriginalBlock(int pageNum, List<Block> blocks, Model m) {
		for (int i = 0; i < blocks.size(); i++) {
			Block block = blocks.get(i);
			Line firstLine = block.getLines().get(0);
			double verticalShift = firstLine.getBottom() - firstLine.getTop();
			m.add("OriginalBlock", m.getConstantTerm("oriBlock" + i),
					m.getConstantTerm(docName),
					m.getConstantTerm("page" + pageNum),
					m.getStringLiteral("" + block.getLeft()),
					m.getStringLiteral("" + block.getTop()),
					m.getStringLiteral("" + block.getRight()),
					m.getStringLiteral("" + block.getBottom()),
					m.getStringLiteral("" + verticalShift));
		}
	}
	
	private void appendOriginalBlockGroups(int pageNum, List<BlockGroup> groups, Model m) {
		for (int i = 0; i < groups.size(); i++) {
			BlockGroup group = groups.get(i);
			Line firstLine = group.getBlocks().get(0).getLines().get(0);
			double verticalShift = firstLine.getBottom() - firstLine.getTop();
			m.add("OriginalBlockGroup", m.getConstantTerm("oriBlockGroup" + i),
					m.getConstantTerm(docName),
					m.getConstantTerm("page" + pageNum),
					m.getStringLiteral("" + group.getLeft()),
					m.getStringLiteral("" + group.getTop()),
					m.getStringLiteral("" + group.getRight()),
					m.getStringLiteral("" + group.getBottom()),
					m.getStringLiteral("" + verticalShift));
		}
	}

}
