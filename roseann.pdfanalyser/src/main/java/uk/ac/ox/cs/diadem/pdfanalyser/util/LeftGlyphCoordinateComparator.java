/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.util;

import java.util.Comparator;

import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;

public class LeftGlyphCoordinateComparator implements Comparator<Glyph> {

    @Override
    public int compare(final Glyph g1, final Glyph g2) {

        if (g1.getLeft() < g2.getLeft()) {
            return -1;
        } else if (g1.getLeft() > g2.getLeft()) {
            return 1;
        } else {
            return 0;
        }
    }

}
