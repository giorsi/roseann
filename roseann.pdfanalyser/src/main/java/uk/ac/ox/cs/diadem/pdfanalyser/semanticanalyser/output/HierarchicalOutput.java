package uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.output;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;
import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Segment;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.model.HieBlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.util.SegmentedContents;

public class HierarchicalOutput {

	private DocumentBuilderFactory dbfact = null;
	private DocumentBuilder docBuilder = null;
	private Document doc = null;
	private Element rootElement = null;
	private String outputName;
	private String outputPath;
	private OutputStream outputStream;

	public HierarchicalOutput(String fileName, String outputPath,
			OutputStream outputStream) {
		try {
			this.outputPath = outputPath;
			this.outputStream = outputStream;

			dbfact = DocumentBuilderFactory.newInstance();
			docBuilder = dbfact.newDocumentBuilder();
			doc = docBuilder.newDocument();
			if (fileName != null) {
				outputName = fileName + "_output";
			} else {
				// we generate a random name if there was no name provided
				// for example if the input is an inputStream and not a file
				Random randomName = new Random();
				outputName = new BigInteger(10, randomName).toString(32)
						+ "_output";
			}
			rootElement = doc.createElement("document");
			rootElement.setAttribute("name", fileName);
			doc.appendChild(rootElement);

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	public void addPage(int pageNumber, List<HieBlockGroup> groups) {
		Element pageElement = doc.createElement("page");
		pageElement.setAttribute("number", Integer.toString(pageNumber));

		int i = 0;
		for (HieBlockGroup group : groups) {
			addGroup(group, pageElement, i);
			i++;
		}

		rootElement.appendChild(pageElement);
	}
	
	private void addGroup(HieBlockGroup groupToAdd, Element parentElement, int id) {
		DecimalFormat df = new DecimalFormat("0.##");
		String top = df.format(groupToAdd.getTop());
		String bottom = df.format(groupToAdd.getBottom());
		String left = df.format(groupToAdd.getLeft());
		String right = df.format(groupToAdd.getRight());

		Element groupElement = doc.createElement("group");
		groupElement.setAttribute("id", Integer.toString(id));
		groupElement.setAttribute("top", top);
		groupElement.setAttribute("bottom", bottom);
		groupElement.setAttribute("left", left);
		groupElement.setAttribute("right", right);

		Element groupContents = doc.createElement("contents");
		groupContents.setTextContent(groupToAdd.toString());
		groupElement.appendChild(groupContents);

		if (groupToAdd.getSubGroups().size() > 0) {
			int i = 0;
			for (HieBlockGroup subGroup: groupToAdd.getSubGroups()) {
				addGroup(subGroup, groupElement, i);
				i++;
			}
		} 
		parentElement.appendChild(groupElement);
	}
	
	/**
	 * Ends the output process by transforming the XML tree into a String and by
	 * saving the output file
	 */
	public void endProcess() {
		try {
			TransformerFactory transformerFact = TransformerFactory
					.newInstance();
			Transformer transformer;
			transformer = transformerFact.newTransformer();

			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
					"yes");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(
					"{http://xml.apache.org/xslt}indent-amount", "4");

			// create string from xml tree
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, result);
			String xmlString = sw.toString();

			if (outputStream == null) {
				// if the output given was a file
				File outputFile = new File(outputPath + outputName + ".xml");
				BufferedWriter writer = new BufferedWriter(new FileWriter(
						outputFile));
				writer.write(xmlString);
				writer.close();
			} else {
				// if we provided an outputstream and not a file
				OutputStreamWriter osw = new OutputStreamWriter(outputStream);
				osw.write(xmlString);
				osw.close();
			}

		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
