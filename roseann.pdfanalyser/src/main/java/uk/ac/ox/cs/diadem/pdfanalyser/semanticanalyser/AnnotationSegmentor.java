package uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser;

/*
 * The annotator part has been commented out (//-annotator-//) 
 * because at the moment the project annotator cannot be included in the pom
 */
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.model.Atom;
import uk.ac.ox.cs.diadem.model.Model;
import uk.ac.ox.cs.diadem.model.ModelFactory;
//-annotator-//import uk.ac.ox.cs.diadem.nlp.annotator.AnnotatorException;
//-annotator-//import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.TextAnnotator;
//-annotator-//import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.gate.realestate.RealEstateAnnotator;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;
import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.util.Saliency;
import uk.ac.ox.cs.diadem.pdfanalyser.util.SegmentedContents;
import uk.ac.ox.cs.diadem.util.collect.IterableIterator;

import com.google.common.collect.Sets;

public class AnnotationSegmentor {

	static final Logger LOGGER = LoggerFactory
			.getLogger(AnnotationSegmentor.class);

	private final int TITLE_TEXT_VSPACE = ConfigurationFacility
			.getConfiguration().getInt(
					"pdfanalyser.annotationsegmenter.titleTextSpace");
	private final int ADJACENT_GROUP_VSPACE = ConfigurationFacility
			.getConfiguration().getInt(
					"pdfanalyser.annotationsegmenter.adjacentGroupsSpace");
	private final double SALIENCY_THRESHOLD = ConfigurationFacility
			.getConfiguration().getDouble(
					"pdfanalyser.annotationsegmenter.saliencyThreshold");
	private final String CAT_SECTION_TITLE = ConfigurationFacility
			.getConfiguration()
			.getString(
					"pdfanalyser.annotationsegmenter.category.definition.sec_title");
	private final String SECTION_TITLE = "SEC_TITLE";
	private final String UNKNOWN = ConfigurationFacility
			.getConfiguration()
			.getString(
					"pdfanalyser.annotationsegmenter.category.definition.unknown");
	private final Configuration[] oversplit_cat = ConfigurationFacility
			.getConfiguration().getSubConfigurationArray(
					"pdfanalyser.annotationsegmenter.category.rules.oversplit");
	private final Configuration[] undersplit_cat = ConfigurationFacility
			.getConfiguration()
			.getSubConfigurationArray(
					"pdfanalyser.annotationsegmenter.category.rules.undersplit");

	private Map<String, String> semanticMappingBag;
	private Map<String, List<String[]>> undersplit_cat_rules;
	private Map<String, List<String[]>> oversplit_cat_rules;
	private Map<PDPage, SegmentedContents> documentSegmentation;
	private Map<PDPage, SegmentedContents> newSegmentation;
	private List<Block> newBlocks;
	private List<BlockGroup> newGroups;
	private List<BlockGroup> obviousSecTitles;
	//-annotator-//final TextAnnotator annotator;
	Map<PDPage, Double> averageFontSize;
	float curPageHeight;
	private PDPage currentRefinePage;
	private List<Block> titles;
	private Map<PDPage, List<BlockGroup>> annoTitles;

	public AnnotationSegmentor(
			final Map<PDPage, SegmentedContents> documentSegmentation) {

		this.documentSegmentation = documentSegmentation;

		newSegmentation = new HashMap<PDPage, SegmentedContents>();

		averageFontSize = new HashMap<PDPage, Double>();

		titles = new ArrayList<Block>();

		this.annoTitles = new HashMap<PDPage, List<BlockGroup>>();

		//-annotator-//annotator = RealEstateAnnotator.getInstance();
		ConfigurationFacility.getConfiguration();

		calculateAverageFontSize();

		initialize();

		configCategoryIdentifyRules();

	}

	public AnnotationSegmentor() {
		//-annotator-//annotator = RealEstateAnnotator.getInstance();
		ConfigurationFacility.getConfiguration();

	}

	private void initialize() {
		final URL outputURL = AnnoSegJAXB.class
				.getResource("semanticTypeMap.xml");

		JAXBContext context;
		try {
			context = JAXBContext.newInstance(SemanticTypeMap.class);
			final Unmarshaller um = context.createUnmarshaller();
			final SemanticTypeMap stm = (SemanticTypeMap) um
					.unmarshal(new FileReader(new File(outputURL.getFile())));
			semanticMappingBag = stm.getSemanticMappingBag();

		} catch (final Exception e) {
			e.printStackTrace();
		}

	}

	private void configCategoryIdentifyRules() {
		undersplit_cat_rules = new HashMap<String, List<String[]>>();
		oversplit_cat_rules = new HashMap<String, List<String[]>>();

		IterableIterator<String> cats = undersplit_cat[0].getKeys();
		while (cats.hasNext()) {
			final String cat = cats.next();
			final List<String[]> types = extractSemanticTypeList(undersplit_cat[0]
					.getString(cat));
			undersplit_cat_rules.put(cat, types);
		}
		cats = oversplit_cat[0].getKeys();
		while (cats.hasNext()) {
			final String cat = cats.next();
			final List<String[]> types = extractSemanticTypeList(oversplit_cat[0]
					.getString(cat));
			oversplit_cat_rules.put(cat, types);
		}
	}

	public Map<PDPage, SegmentedContents> refineSegmentation() {

		// sort lines in blockgroup and blocks
		sortSegmentation();

		// refine segmentation
		final Iterator<PDPage> it = documentSegmentation.keySet().iterator();
		while (it.hasNext()) {
			currentRefinePage = it.next();
			curPageHeight = currentRefinePage.getMediaBox().getHeight();
			final SegmentedContents contents = documentSegmentation
					.get(currentRefinePage);
			newBlocks = new ArrayList<Block>();
			newGroups = new ArrayList<BlockGroup>();

			removeEmptyComponents(contents);
			refineUnderSplit(contents.getGroups());
			refineOverSplit(newGroups);
			removeOverlapGroups(newGroups);

			final SegmentedContents newContents = new SegmentedContents(
					contents.getGlyphs(), newBlocks, newGroups);
			newSegmentation.put(currentRefinePage, newContents);
		}

		return newSegmentation;
	}

	/**
	 * over split problems mainly exist in "BLOCKGROUP", they are: label:text
	 * (e.g. Description : description_text)
	 * 
	 * @param groups
	 */
	private void refineOverSplit(final List<BlockGroup> groups) {

		if (groups.size() == 0)
			return;

		obviousSecTitles = new ArrayList<BlockGroup>();
		// (1) group blocks with the same semantic meaning(e.g. two blocks both
		// describe the address of the house; two blocks both describe the
		// contact of the agent)
		// (2) group section_title and contexts (Description: ...). At current
		// stage we only group those obvious ones which are directly confirmed
		// through annotation
		BlockGroup precedingGroup = groups.get(0);
		BlockGroup currentGroup = null;
		for (int i = 1; i < groups.size(); i++) {
			currentGroup = groups.get(i);

			final Set<String> precedingType = identifyBlockGroup(precedingGroup);
			final Set<String> currentType = identifyBlockGroup(currentGroup);
			String precedingCat = getOverSplitCategory(precedingType);
			String currentCat = getOverSplitCategory(currentType);

			boolean additionPossible = false;
			if (isObviousSecTitle(precedingGroup, currentGroup, precedingCat)) {
				additionPossible = true;
				// record title
				titles.addAll(precedingGroup.getBlocks());
				recordTitle(precedingGroup);

			} else {
				precedingType.remove(SECTION_TITLE);
				currentType.remove(SECTION_TITLE);
				precedingCat = getOverSplitCategory(precedingType);
				currentCat = getOverSplitCategory(currentType);
				if (hasSameSemanticContents(precedingGroup, currentGroup,
						precedingCat, currentCat)) {
					additionPossible = true;
				}
			}

			if (additionPossible) {
				// compact the two block groups.
				precedingGroup.addBlocks(currentGroup.getBlocks());
				newGroups.remove(currentGroup);
				i--;
			} else {
				precedingGroup = currentGroup;
			}
		}

		// at current stage we have learned enough information about obvious
		// titles
		// we need to infer those non-obvious section titles from those obvious
		// ones
		precedingGroup = groups.get(0);
		currentGroup = null;
		for (int i = 1; i < groups.size(); i++) {
			currentGroup = groups.get(i);
			if (isInferredSecTitle(precedingGroup, currentGroup)) {
				// compact the two block groups.
				precedingGroup.addBlocks(currentGroup.getBlocks());
				newGroups.remove(currentGroup);
				titles.addAll(precedingGroup.getBlocks());
				recordTitle(precedingGroup);
				i--;
			} else {
				precedingGroup = currentGroup;
			}
		}
	}

	private boolean isInferredSecTitle(final BlockGroup precedingGroup,
			final BlockGroup currentGroup) {
		final List<BlockGroup> groupToEvaluate = new ArrayList<BlockGroup>();
		final List<BlockGroup> otherGroups = new ArrayList<BlockGroup>();
		groupToEvaluate.add(precedingGroup);
		otherGroups.add(currentGroup);
		final boolean result = (Saliency.groupFontSaliency(groupToEvaluate,
				obviousSecTitles) == 0)
				&& (Saliency.groupFontSaliency(groupToEvaluate, otherGroups) >= SALIENCY_THRESHOLD)
				&& sameColumn(precedingGroup, currentGroup)
				&& ((currentGroup.getTop() - precedingGroup.getBottom()) < TITLE_TEXT_VSPACE);
		return result;
	}

	private boolean isObviousSecTitle(final BlockGroup precedingGroup,
			final BlockGroup currentGroup, final String precedingCat) {
		// TODO (1) make 10 configurable (2) saliency (3)use annotation to get
		// some title that are very
		// certain then we use their style to match those uncertain ones
		final List<BlockGroup> groupToEvaluate = new ArrayList<BlockGroup>();
		final List<BlockGroup> otherGroups = new ArrayList<BlockGroup>();
		groupToEvaluate.add(precedingGroup);
		otherGroups.add(currentGroup);
		final boolean result = precedingCat.equals(CAT_SECTION_TITLE)
				&& (precedingGroup.getNonEmptyLines().size() == 1)
				&& sameColumn(precedingGroup, currentGroup)
				&& (Saliency.groupFontSaliency(groupToEvaluate, otherGroups) >= SALIENCY_THRESHOLD)
				&& ((currentGroup.getTop() - precedingGroup.getBottom()) < TITLE_TEXT_VSPACE);
		if (result) {
			obviousSecTitles.add(new BlockGroup(precedingGroup.getBlocks().get(
					0)));
		}
		return result;
	}

	/**
	 * under split problems mainly exist in "BLOCK", they are: (1) address and
	 * price (2) address and contacts under split problems may also exist in
	 * "BlockGroup". If two blocks are talking about different things we shall
	 * split them into two different blockgroups
	 * 
	 * @param groups
	 */
	private void refineUnderSplit(final List<BlockGroup> groups) {
		for (final BlockGroup group : groups) {
			BlockGroup newGroup = null;
			final List<Block> blocks = group.getBlocks();
			for (final Block block : blocks) {
				final List<Line> lines = block.getLines();
				// if there are big block texts(more than 5 lines). We don't try
				// to split them
				if ((lines.size() > 5)
						|| (!isHead(lines.get(0)) && !isFoot(block))) {
					newBlocks.add(block);
					if (newGroup == null) {
						newGroup = new BlockGroup(block);
					} else {
						newGroup.addBlock(block);
					}
					continue;
				}

				Block newBlock = new Block(lines.get(0));
				// annotate each line
				final Line precedingLine = lines.get(0);
				Set<String> precedingType = identifyLine(precedingLine);
				for (int i = 1; i < lines.size(); i++) {
					final Line currentLine = lines.get(i);
					final Set<String> currentType = identifyLine(currentLine);

					final String precedingCat = getUndersplitCategory(precedingType);
					final String currentCat = getUndersplitCategory(currentType);
					if (precedingCat.equals(UNKNOWN)
							|| currentCat.equals(UNKNOWN)
							|| precedingCat.equals(currentCat)) {
						newBlock.addLine(currentLine);
						precedingType = currentType;
						continue;
					}
					// if preceding line and current line has difference
					// semantic contents
					newBlocks.add(newBlock);
					if (newGroup == null) {
						newGroup = new BlockGroup(newBlock);
					} else {
						newGroup.addBlock(newBlock);
					}

					newGroups.add(newGroup);

					newGroup = null;
					newBlock = new Block(currentLine);
					precedingType = currentType;
				}
				// add the last newBlock in new blocks

				newBlocks.add(newBlock);
				if (newGroup == null) {
					newGroup = new BlockGroup(newBlock);
				} else {
					newGroup.addBlock(newBlock);
				}

			}
			newGroups.add(newGroup);
		}
	}

	/**
	 * remove those block groups only contains white space or non-letter marks
	 * 
	 * @param groups
	 */
	public static void removeEmptyComponents(final SegmentedContents contents) {
		final List<BlockGroup> groups = contents.getGroups();
		final List<Block> blocks = contents.getBlocks();
		final List<Line> lines = contents.getLines();
		// remove empty blockgroups
		for (int i = 0; i < groups.size(); i++) {
			final String text = groups.get(i).toString().trim();
			if (text.equals("") || !containsNonEmpChar(text)) {
				blocks.removeAll(groups.get(i).getBlocks());
				lines.removeAll(groups.get(i).getLines());
				groups.remove(i);
				i--;
			}
		}
	}

	/**
	 * remove those block groups that are contained by other groups This might
	 * happen because we group blocks through annotation before But for L
	 * irregular layout, we shall still present the block groups separately we
	 * determine whether it's L shape through checking their contents
	 */
	private void removeOverlapGroups(final List<BlockGroup> groups) {
		for (int i = 0; i < groups.size(); i++) {
			final BlockGroup currentGroup = groups.get(i);
			for (int j = 0; j < groups.size(); j++) {
				BlockGroup group = groups.get(j);
				if (currentGroup != group && group.contains(currentGroup)
						&& group.toString().contains(currentGroup.toString())) {
					group.addBlocks(currentGroup.getBlocks());
					group.sortLines();
					groups.remove(currentGroup);
					i--;
				}
			}
		}
	}

	/**
	 * Here we only focus on those cases that are most likely over split based
	 * on some statistical analysis.
	 * 
	 * @param types
	 * @return
	 */
	private String getOverSplitCategory(final Set<String> types) {
		final IterableIterator<String> cats = oversplit_cat[0].getKeys();
		while (cats.hasNext()) {
			final String cat = cats.next();
			final List<String[]> targetTypes = oversplit_cat_rules.get(cat);
			for (final String[] combo : targetTypes) {
				if (types.containsAll(Arrays.asList(combo)))
					return cat;
			}
		}
		return UNKNOWN;
	}

	private boolean isHead(final Line line) {
		return line.getFontSize() >= averageFontSize.get(currentRefinePage);
	}

	private boolean isFoot(final Block block) {
		return block.getTop() > (curPageHeight * 0.8);
	}

	/**
	 * here we are not trying to map lines to as many category as possible we
	 * only focus on those that are easily mis-segmented by layout based on some
	 * statistical analysis. e.g Lower Radley Abingdon OX14 £6,000 per month -
	 * Available Now address and price always wrongly put in same block
	 * according to layout so we can divide them according to annotation e.g
	 * Luxury residence built in the late 1990's, with an outstanding Thames
	 * waterfront position, private gym, tennis court, boathouse and slipway.
	 * this is initially correctly groupped. But if we check the first line, it
	 * will be categorized as Address. While the second line will be categorized
	 * as home_equipment. They will be wrongly separated
	 * 
	 * @param types
	 * @return
	 */
	private String getUndersplitCategory(final Set<String> types) {
		final IterableIterator<String> cats = undersplit_cat[0].getKeys();
		while (cats.hasNext()) {
			final String cat = cats.next();
			final List<String[]> targetTypes = undersplit_cat_rules.get(cat);
			for (final String[] combo : targetTypes) {
				if (types.containsAll(Arrays.asList(combo)))
					return cat;
			}
		}
		return UNKNOWN;
	}

	private boolean hasSameSemanticContents(final BlockGroup precedingGroup,
			final BlockGroup currentGroup, final String precedingCat,
			final String currentCat) {
		final List<Block> precedingBlocks = precedingGroup.getBlocks();
		final List<Line> currentLines = currentGroup.getLines();
		final Block precedingLastBlock = precedingBlocks.get(precedingBlocks
				.size() - 1);
		final Line currentFirstLine = currentLines.get(0);

		if (precedingLastBlock.hasSameStyle(currentFirstLine)
				&& sameColumn(precedingGroup, currentGroup)
				&& ((currentGroup.getTop() - precedingGroup.getBottom()) < ADJACENT_GROUP_VSPACE)
				&& !precedingCat.equals(UNKNOWN) && !currentCat.equals(UNKNOWN)
				&& isHead(precedingGroup.getLines().get(0))
				&& isHead(currentGroup.getLines().get(0))
				&& precedingCat.equals(currentCat))
			return true;

		return false;
	}

	private List<String[]> extractSemanticTypeList(final String str) {
		final List<String[]> types = new ArrayList<String[]>();
		final String[] orCombo = str.split(";");
		for (final String element : orCombo) {
			String curStr = element.trim();
			String[] curStrArr = null;
			if (curStr.contains("(") && curStr.contains(")")) {
				curStr = curStr.replace("(", "");
				curStr = curStr.replace(")", "");
				curStrArr = curStr.split(",");
			} else {
				curStrArr = new String[1];
				curStrArr[0] = curStr;
			}
			types.add(curStrArr);
		}
		return types;
	}

	private Set<String> identifyBlockGroup(final BlockGroup group) {
		return identifyText(group.toString());
	}

	private Set<String> identifyLine(final Line line) {
		return identifyText(line.toString());
	}

	public Set<String> identifyText(final String text) {
		final Set<String> types = new HashSet<String>();
		Model model = ModelFactory.createIsolatedModel();
		//-annotator-//model = annotator.annotate(text);
		final Set<Atom> entities = model
				.getAtomsByPredicate("EntityAnnotation");
		for (final Atom entity : entities) {
			final String concept = entity.getSubTerm(1).getValue();
			final String type = getSemanticType(concept);
			types.add(type);
		}

		return types;
	}

	private String getSemanticType(final String key) {
		final String type = semanticMappingBag.get(key);
		if (type == null)
			return UNKNOWN;
		return type;
	}

	private void sortSegmentation() {
		final Iterator<PDPage> it = documentSegmentation.keySet().iterator();
		while (it.hasNext()) {
			final PDPage curPage = it.next();
			final SegmentedContents contents = documentSegmentation
					.get(curPage);

			final List<Block> blocks = contents.getBlocks();
			for (final Block block : blocks) {
				block.sortLines();
			}

			final List<BlockGroup> groups = contents.getGroups();
			for (final BlockGroup group : groups) {
				group.sortLines();
			}
		}
	}

	public Map<PDPage, SegmentedContents> getDocumentSegmentation() {
		return documentSegmentation;
	}

	public boolean isPostCode(final String text) {
		Model model = ModelFactory.createIsolatedModel();
		//-annotator-//model = annotator.annotate(text);
		final Set<Atom> entities = model
				.getAtomsByPredicate("EntityAnnotation");
		for (final Atom entity : entities) {
			final String concept = entity.getSubTerm(1).getValue();
			if (concept.equals("postcode"))
				return true;
		}
		return false;
	}

	public boolean isBulletMark(String text) {
		Model model = ModelFactory.createIsolatedModel();
		//-annotator-//model = annotator.annotate(text);
		Set<Atom> entities = model.getAtomsByPredicate("EntityAnnotation");
		for (Atom entity : entities) {
			String concept = entity.getSubTerm(1).getValue();
			if (concept.equals("bullet")) {
				return true;
			}
		}
		return false;
	}

	private void calculateAverageFontSize() {
		final Iterator<PDPage> it = documentSegmentation.keySet().iterator();
		while (it.hasNext()) {
			int lineNum = 0;
			double sum = 0;
			final PDPage curPage = it.next();
			final SegmentedContents contents = documentSegmentation
					.get(curPage);
			final List<Line> lines = contents.getLines();
			for (final Line line : lines) {
				sum += line.getFontSize();
			}
			lineNum += lines.size();
			if (lineNum > 0) {
				averageFontSize.put(curPage, sum / lineNum);
			}
		}
	}

	private boolean sameColumn(final BlockGroup group1, final BlockGroup group2) {
		final double overlap = Math.min(group1.getRight(), group2.getRight())
				- Math.max(group1.getLeft(), group2.getLeft());

		if (overlap >= 0) {
			if ((((group1.getLeft() - group2.getLeft()) < 0.1) && ((group1
					.getRight() - group2.getRight()) > -0.1))
					|| (((group1.getLeft() - group2.getLeft()) > -0.1) && ((group1
							.getRight() - group2.getRight()) < 0.1)))
				return true;
			else {
				final double overlapRatio = overlap
						/ (Math.max(group1.getRight(), group2.getRight()) - Math
								.min(group1.getLeft(), group2.getLeft()));
				if (overlapRatio > 0.5)
					return true;
			}
		}
		return false;
	}

	public static boolean containsNonEmpChar(final String text) {
		boolean contains = false;
		for (int i = 0; i < text.length(); i++) {
			if (Character.isLetter(text.charAt(i))
					|| Character.isDigit(text.charAt(i))) {
				contains = true;
				break;
			}
		}
		return contains;
	}

	public List<Block> getTitles() {
		return titles;
	}

	private void recordTitle(BlockGroup group) {
		List<BlockGroup> curTitles = annoTitles.get(currentRefinePage);
		if (curTitles == null) {
			curTitles = new ArrayList<BlockGroup>();
		}
		// assert: only one block and only one line for a section title
		BlockGroup groupCopy = new BlockGroup(group.getBlocks().get(0));
		curTitles.add(groupCopy);
		annoTitles.put(currentRefinePage, curTitles);
	}

	public Map<PDPage, List<BlockGroup>> getAnnoTitles() {
		return annoTitles;
	}
}
