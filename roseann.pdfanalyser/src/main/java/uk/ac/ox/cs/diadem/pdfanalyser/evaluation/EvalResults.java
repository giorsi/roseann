/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.evaluation;

public class EvalResults {

    /**
     * Creates an instance of an {@link EvalResults} object
     * 
     * @param completeness
     * @param purity
     * @param precision
     * @param recall
     */
    public EvalResults(final float completeness, final float purity, final float precision, final float recall) {
        this.completeness = completeness;
        this.purity = purity;
        this.precision = precision;
        this.recall = recall;
    }

    private final float completeness;
    private final float purity;
    private final float precision;
    private final float recall;

    public float getCompleteness() {
        return completeness;
    }

    public float getPurity() {
        return purity;
    }

    public float getPrecision() {
        return precision;
    }

    public float getRecall() {
        return recall;
    }
}
