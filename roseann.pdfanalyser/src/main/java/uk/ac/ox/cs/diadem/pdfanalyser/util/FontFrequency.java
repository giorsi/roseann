/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.util;

public class FontFrequency {

    private final String font;
    private double frequency;

    public FontFrequency(final String font, final double frequency) {
        this.font = font;
        this.frequency = frequency;
    }

    public String getFontName() {
        return font;
    }

    public double getFrequency() {
        return frequency;
    }

    public void incrementFrequency() {
        frequency++;
    }
}
