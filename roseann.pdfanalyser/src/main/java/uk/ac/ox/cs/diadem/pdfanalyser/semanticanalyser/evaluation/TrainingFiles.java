package uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.evaluation;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TrainingFiles {
	public final static List<String> filesList = new ArrayList<String>();
	public final static Map<String, ArrayList<Integer>> hamptonsFiles = new LinkedHashMap<String, ArrayList<Integer>>();
	public final static Map<String, ArrayList<Integer>> savillsFiles = new LinkedHashMap<String, ArrayList<Integer>>();

	/**
	 * Adds the evaluation files to the filesList
	 */
	public static void initFilesList() {
		filesList.add("hamptons_rent_001.pdf");
		filesList.add("hamptons_rent_002.pdf");
		filesList.add("hamptons_rent_003.pdf");
		filesList.add("hamptons_rent_004.pdf");
		filesList.add("hamptons_sale_001.pdf");
		filesList.add("hamptons_sale_002.pdf");
		filesList.add("hamptons_sale_003.pdf");
		filesList.add("hamptons_sale_004.pdf");
		filesList.add("hamptons_sale_005.pdf");
		filesList.add("savills_rent_001.pdf");
		filesList.add("savills_rent_002.pdf");
		filesList.add("savills_rent_003.pdf");
		filesList.add("savills_rent_004.pdf");
		filesList.add("savills_rent_005.pdf");
		filesList.add("savills_sale_001.pdf");
		filesList.add("savills_sale_002.pdf");
		filesList.add("savills_sale_003.pdf");
		filesList.add("savills_sale_004.pdf");
	}

	/**
	 * Initialises the evaluation lists
	 */
	public static void initEvaluationFiles() {
		initHamptonsFiles();
		initSavillsFiles();
	}

	/**
	 * Adds the file (as a key) and the pages (as the value) to the hamptons
	 * file map
	 */
	private static void initHamptonsFiles() {
		ArrayList<Integer> pagesList = new ArrayList<Integer>();
		pagesList.add(1);
		pagesList.add(2);
		hamptonsFiles.put("hamptons_rent_001.pdf", pagesList);

		pagesList = new ArrayList<Integer>();
		pagesList.add(1);
		pagesList.add(2);
		pagesList.add(3);
		hamptonsFiles.put("hamptons_rent_002.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(1);
		pagesList.add(2);
		pagesList.add(3);
		hamptonsFiles.put("hamptons_rent_003.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		pagesList.add(2);
		hamptonsFiles.put("hamptons_rent_004.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(1);
		pagesList.add(2);
		hamptonsFiles.put("hamptons_sale_001.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		pagesList.add(2);
		pagesList.add(4);
		pagesList.add(6);
		pagesList.add(7);
		pagesList.add(9);
		pagesList.add(12);
		pagesList.add(15);
		pagesList.add(18);
		hamptonsFiles.put("hamptons_sale_002.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(1);
		pagesList.add(3);
		hamptonsFiles.put("hamptons_sale_003.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(3);
		pagesList.add(5);
		pagesList.add(7);
		hamptonsFiles.put("hamptons_sale_004.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(1);
		pagesList.add(2);
		pagesList.add(3);
		hamptonsFiles.put("hamptons_sale_005.pdf", pagesList);
	}

	/**
	 * Adds the file (as a key) and the pages (as the value) to the sallvis file
	 * map
	 */
	private static void initSavillsFiles() {
		ArrayList<Integer> pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		savillsFiles.put("savills_rent_001.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		pagesList.add(2);
		pagesList.add(3);
		savillsFiles.put("savills_rent_002.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		pagesList.add(3);
		savillsFiles.put("savills_rent_003.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		pagesList.add(2);
		savillsFiles.put("savills_rent_004.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		savillsFiles.put("savills_rent_005.pdf", pagesList);
		
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(2);
		pagesList.add(3);
		pagesList.add(7);
		pagesList.add(8);
		pagesList.add(10);
		pagesList.add(14);
		savillsFiles.put("savills_sale_001.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(1);
		pagesList.add(2);
		savillsFiles.put("savills_sale_002.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		pagesList.add(2);
		pagesList.add(3);
		savillsFiles.put("savills_sale_003.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		pagesList.add(2);
		pagesList.add(3);
		pagesList.add(4);
		pagesList.add(7);
		savillsFiles.put("savills_sale_004.pdf", pagesList);
	}

}
