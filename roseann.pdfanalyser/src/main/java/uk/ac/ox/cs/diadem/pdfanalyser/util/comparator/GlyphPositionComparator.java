/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.util.comparator;

import java.util.Comparator;

import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;

public class GlyphPositionComparator implements Comparator<Glyph> {

    /* (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(Glyph pos1, Glyph pos2) {
    	if (pos1.getTop() < pos2.getTop()) {
            return -1;
        } else if (pos1.getTop() > pos2.getTop()) {
            return 1;
        }else {
            return 0;
        }
    }
}
