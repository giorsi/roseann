package uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.evaluation;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class EvaluationFiles {
	public final static List<String> filesList = new ArrayList<String>();
	public final static Map<String, ArrayList<Integer>> singleColumn = new LinkedHashMap<String, ArrayList<Integer>>();
	public final static Map<String, ArrayList<Integer>> multiColumn = new LinkedHashMap<String, ArrayList<Integer>>();
	public final static Map<String, ArrayList<Integer>> irregularStyle = new LinkedHashMap<String, ArrayList<Integer>>();

	/**
	 * Adds the evaluation files to the filesList
	 */
	public static void initFilesList() {
		filesList.add("external_acm_001.pdf");
		filesList.add("external_acm_002.pdf");
		filesList.add("external_acm_003.pdf");
		filesList.add("external_acm_005.pdf");
		filesList.add("external_d-us-013.pdf");
		filesList.add("external_d-us-014.pdf");
		filesList.add("external_d-us-015.pdf");
		filesList.add("external_us-03.pdf");
		filesList.add("external_us-07.pdf");
		filesList.add("external_cesr_081014.pdf");
		filesList.add("external_cesr_10697.pdf");
		filesList.add("external_ieee_001.pdf");
		filesList.add("external_ieee_002.pdf");
		filesList.add("external_ieee_004.pdf");
		filesList.add("external_ieee_007.pdf");
		filesList.add("external_realestate_001.pdf");
		filesList.add("external_realestate_002.pdf");
		filesList.add("external_realestate_003.pdf");
		filesList.add("external_realestate_004.pdf");
		filesList.add("external_paper_001.pdf");
	}

	/**
	 * Initialises the evaluation lists
	 */
	public static void initEvaluationList() {
		initSingleColumnList();
		initMultiColumnList();
		initIrregularStyleList();
	}

	private static void initSingleColumnList() {
		ArrayList<Integer> pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		singleColumn.put("external_acm_001.pdf", pagesList);

		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		singleColumn.put("external_acm_003.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(2);
		pagesList.add(3);
		multiColumn.put("external_d-us-013.pdf", pagesList);

		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		singleColumn.put("external_d-us-015.pdf", pagesList);

		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		pagesList.add(2);
		singleColumn.put("external_cesr_081014.pdf", pagesList);

		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		pagesList.add(2);
		singleColumn.put("external_cesr_10697.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(2);
		singleColumn.put("external_realestate_001.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(1);
		singleColumn.put("external_realestate_002.pdf", pagesList);

		pagesList = new ArrayList<Integer>();
		pagesList.add(1);
		singleColumn.put("external_realestate_003.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(16);
		singleColumn.put("external_d-us-014.pdf", pagesList);
	
	}

	private static void initMultiColumnList() {
		
		ArrayList<Integer> pagesList = new ArrayList<Integer>();
		pagesList.add(1);
		multiColumn.put("external_acm_005.pdf", pagesList);

		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		multiColumn.put("external_ieee_001.pdf", pagesList);

		pagesList = new ArrayList<Integer>();
		pagesList.add(1);
		multiColumn.put("external_ieee_002.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		multiColumn.put("external_ieee_004.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(1);
		multiColumn.put("external_realestate_001.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(2);
		pagesList.add(3);
		multiColumn.put("external_realestate_002.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(21);
		multiColumn.put("external_realestate_004.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(2);
		multiColumn.put("external_us-03.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
	    pagesList.add(0);
	    pagesList.add(3);
	    multiColumn.put("external_us-07.pdf", pagesList);
	    
	    pagesList = new ArrayList<Integer>();
	    pagesList.add(1);
	    pagesList.add(6);
	    multiColumn.put("external_paper_001.pdf", pagesList);
	}

	private static void initIrregularStyleList() {
		ArrayList<Integer> pagesList = new ArrayList<Integer>();
		pagesList.add(1);
		pagesList.add(4);
		irregularStyle.put("external_d-us-013.pdf", pagesList);

		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		irregularStyle.put("external_ieee_002.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		pagesList.add(1);
		irregularStyle.put("external_ieee_007.pdf", pagesList);

		pagesList = new ArrayList<Integer>();
		pagesList.add(0);
		irregularStyle.put("external_acm_002.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
		pagesList.add(1);
		pagesList.add(9);
		irregularStyle.put("external_us-03.pdf", pagesList);
		
		pagesList = new ArrayList<Integer>();
	    pagesList.add(5);
	    irregularStyle.put("external_us-07.pdf", pagesList);
	    
	    pagesList = new ArrayList<Integer>();
	    pagesList.add(14);
	    pagesList.add(17);
	    pagesList.add(21);
	    irregularStyle.put("external_d-us-014.pdf", pagesList);
	}

}
