package uk.ac.ox.cs.diadem.pdfanalyser.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import uk.ac.ox.cs.diadem.pdfanalyser.DocumentProcessor;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Segment;

public class ProcessConfig {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(DocumentProcessor.class);

	private String resourcesPath;
	private String outputPath;
	private String outputModelPath;
	private String outputFile;
	private String outputModelFile;
	private ArrayList<Segment> segments = new ArrayList<Segment>();
	private String fileName;
	private String visualizationPath;
	private boolean evaluation;
	private String groundTruthPath;
	private boolean annoEvaluation;
	private boolean hierarchicalAnno;
	private String annoGroudTruthPath;
	private String externalResultPath;
	private String internalResultPath;

	public ProcessConfig(File config) {
		try {
			final DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder;

			dBuilder = dbFactory.newDocumentBuilder();

			Document configTree = dBuilder.parse(config);
			configTree.getDocumentElement().normalize();

			NodeList resourcesNL = configTree.getDocumentElement()
					.getElementsByTagName("resources_folder");
			NodeList outputNL = configTree.getDocumentElement()
					.getElementsByTagName("output_folder");
			NodeList outputModelNL = configTree.getDocumentElement()
					.getElementsByTagName("model_folder");
			NodeList segmentsNL = configTree.getDocumentElement()
					.getElementsByTagName("segments");
			NodeList evaluationNL = configTree.getDocumentElement()
					.getElementsByTagName("evaluation");
			NodeList fileNameNL = configTree.getDocumentElement()
					.getElementsByTagName("fileName");
			NodeList annoEvaluationNL = configTree.getDocumentElement()
					.getElementsByTagName("annotationSegmentEvaluation");

			if (fileNameNL.getLength() != 1) {
				LOGGER.error("No file name specified in the config file");
				System.exit(1);
			}

			if (resourcesNL.getLength() != 1) {
				LOGGER.error("No resources files path specified in the config file");
				System.exit(1);
			}

			if (outputNL.getLength() != 1) {
				LOGGER.error("No output files path specified in the config file");
				System.exit(1);
			}

			if (outputModelNL.getLength() != 1) {
				LOGGER.error("No output model files path specified in the config file");
				System.exit(1);
			}

			if (segmentsNL.getLength() != 1) {
				LOGGER.error("No segments specified in the config file");
				System.exit(1);
			}

			NodeList segmentsListNL = configTree.getDocumentElement()
					.getElementsByTagName("segment");

			if (evaluationNL.getLength() != 1) {
				System.exit(1);
				LOGGER.error("Evaluation mode not specified in the config file");
			} else {
				Node evalNode = evaluationNL.item(0);
				String evalMode = evalNode.getAttributes().getNamedItem("mode")
						.getTextContent();

				if (evalMode.equals("true")) {
					evaluation = true;
					NodeList groundTruthNL = configTree.getDocumentElement()
							.getElementsByTagName("ground_truth_path");
					if (groundTruthNL.getLength() != 1) {
						LOGGER.error("There is no ground thruth files path specified in the config file");
						System.exit(1);
					}
					groundTruthPath = groundTruthNL.item(0).getAttributes()
							.getNamedItem("path").getTextContent();
				} else {
					evaluation = false;
				}
			}

			if (annoEvaluationNL.getLength() != 1) {
				System.exit(1);
				LOGGER.error("AnnoEvaluation mode not specified in the config file");
			} else {
				Node annoEvalNode = annoEvaluationNL.item(0);
				String annoEvalMode = annoEvalNode.getAttributes()
						.getNamedItem("mode").getTextContent();

				if (annoEvalMode.equals("true")) {
					annoEvaluation = true;

					hierarchicalAnno = annoEvalNode.getAttributes()
							.getNamedItem("hierarchical").getTextContent()
							.equals("true");
					NodeList annoGroundTruthNL = configTree
							.getDocumentElement().getElementsByTagName(
									"anno_ground_truth_path");
					if (annoGroundTruthNL.getLength() != 1) {
						LOGGER.error("There is no annotation segment ground thruth files path specified in the config file");
						System.exit(1);
					}
					annoGroudTruthPath = annoGroundTruthNL.item(0)
							.getAttributes().getNamedItem("path")
							.getTextContent();

					NodeList externalResultNL = configTree.getDocumentElement()
							.getElementsByTagName("external_result_path");
					if (externalResultNL.getLength() != 1) {
						LOGGER.error("There is no external result path specified in the config file");
						System.exit(1);
					}
					externalResultPath = externalResultNL.item(0)
							.getAttributes().getNamedItem("path")
							.getTextContent();

					NodeList internalResultNL = configTree.getDocumentElement()
							.getElementsByTagName("internal_result_path");
					if (internalResultNL.getLength() != 1) {
						LOGGER.error("There is no internal result path specified in the config file");
						System.exit(1);
					}
					internalResultPath = internalResultNL.item(0)
							.getAttributes().getNamedItem("path")
							.getTextContent();
				} else {
					annoEvaluation = false;
				}
			}

			resourcesPath = resourcesNL.item(0).getAttributes()
					.getNamedItem("path").getTextContent();
			fileName = fileNameNL.item(0).getAttributes().getNamedItem("name")
					.getTextContent();
			outputPath = outputNL.item(0).getAttributes().getNamedItem("path")
					.getTextContent();
			outputFile = outputPath
					+ fileName.substring(0, fileName.lastIndexOf("."))
					+ "_output.xml";
			outputModelPath = outputModelNL.item(0).getAttributes()
					.getNamedItem("path").getTextContent();
			outputModelFile = outputModelPath
					+ fileName.substring(0, fileName.lastIndexOf("."))
					+ "_model";

			for (int i = 0; i < segmentsListNL.getLength(); i++) {
				if (segmentsListNL.item(i).getAttributes().getNamedItem("type")
						.getTextContent().equals("GLYPH")) {
					segments.add(Segment.GLYPH);
				} else if (segmentsListNL.item(i).getAttributes()
						.getNamedItem("type").getTextContent().equals("LINE")) {
					segments.add(Segment.LINE);
				} else if (segmentsListNL.item(i).getAttributes()
						.getNamedItem("type").getTextContent().equals("BLOCK")) {
					segments.add(Segment.BLOCK);
				} else if (segmentsListNL.item(i).getAttributes()
						.getNamedItem("type").getTextContent()
						.equals("BLOCK_GROUP")) {
					segments.add(Segment.BLOCK_GROUP);
				}
			}

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean getEvaluation() {
		return evaluation;
	}

	public File getVisualizationFile() {
		return new File(visualizationPath + fileName + "_visu.pdf");
	}

	public Segment[] getSegments() {
		Segment[] list = new Segment[segments.size()];
		return segments.toArray(list);
	}

	public String getResourcesPath() {
		return resourcesPath;
	}

	public File getOutputFile() {
		return new File(outputFile);
	}

	public File getGroundTruthFile() {
		return new File(groundTruthPath
				+ fileName.substring(0, fileName.lastIndexOf("."))
				+ "_ground.xml");
	}

	public File getInputFile() {
		return new File(resourcesPath + fileName);
	}

	public String getTruthPath() {
		return groundTruthPath;
	}

	public boolean getAnnoEvaluation() {
		return annoEvaluation;
	}

	public void setAnnoEvaluation(boolean annoEvaluation) {
		this.annoEvaluation = annoEvaluation;
	}

	public String getAnnoGroudTruthPath() {
		return annoGroudTruthPath;
	}

	public void setAnnoGroudTruthPath(String annoGroudTruthPath) {
		this.annoGroudTruthPath = annoGroudTruthPath;
	}

	public File getOutputModelFile() {
		return new File(outputModelFile);
	}

	public String getOutputPath() {
		return outputPath;
	}

	public String getOutputModelPath() {
		return outputModelPath;
	}

	public String getExternalResultPath() {
		return externalResultPath;
	}
	
	public String getInternalResultPath(){
		return internalResultPath;
	}

	public void setExternalResultPath(String externalResultPath) {
		this.externalResultPath = externalResultPath;
	}

	public boolean getHierarchicalAnno() {
		return this.hierarchicalAnno;
	}

}
