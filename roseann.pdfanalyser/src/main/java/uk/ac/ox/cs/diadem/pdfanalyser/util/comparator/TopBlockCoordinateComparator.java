/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.util.comparator;

import java.util.Comparator;

import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;

public class TopBlockCoordinateComparator implements Comparator<Block> {

    /* (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(Block line1, Block line2) {
        if (line1.getTop() < line2.getTop()) {
            return -1;
        } else if (line1.getTop() > line2.getTop()) {
            return 1;
        } else {
            return 0;
        }
    }

}