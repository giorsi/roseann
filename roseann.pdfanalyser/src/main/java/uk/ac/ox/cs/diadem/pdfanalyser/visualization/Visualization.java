/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.visualization;

import java.awt.Color;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;

import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;
import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Segment;
import uk.ac.ox.cs.diadem.pdfanalyser.util.SegmentedContents;

public class Visualization {

    /**
     * @param document
     *            The document that will be visualised
     * @param segmentedContents
     *            The map of the segmented contents of the processed pages
     * @param outputStream
     *            A reference to the output stream
     * @param segments
     *            The array of segments that will be visualised (block, group,
     *            line, glyph)
     */
    public Visualization(PDDocument document, Map<PDPage, SegmentedContents> segmentedContents,
            OutputStream outputStream, Segment... segments) {
        this.document = document;
        this.segmentedContents = segmentedContents;
        this.pages = segmentedContents.keySet();
        this.segments = segments;
        this.outputStream = outputStream;
        initColoursList();
    }

    private PDPageContentStream contentStream;
    private final PDDocument document;
    private final Map<PDPage, SegmentedContents> segmentedContents;
    private final Set<PDPage> pages;
    private final Segment[] segments;
    private final OutputStream outputStream;
    private List<Color> colorsList;

    /**
     * Adds some colours the a list
     */
    private void initColoursList() {
        colorsList = new ArrayList<Color>();
        colorsList.add(Color.ORANGE);
        colorsList.add(Color.RED);
        colorsList.add(Color.BLUE);
        colorsList.add(Color.GREEN);
        colorsList.add(Color.YELLOW);
        colorsList.add(Color.CYAN);
        colorsList.add(Color.GRAY);
        colorsList.add(Color.MAGENTA);
        colorsList.add(Color.DARK_GRAY);
        colorsList.add(Color.PINK);
    }

    /**
     * Calls the methods needed, depending on the segments that have to be
     * visualised and saves the output
     * 
     * @throws IOException
     */
    public void visualize() throws IOException {

        Iterator<PDPage> iterator = this.pages.iterator();
        while (iterator.hasNext()) {
            PDPage currentPage = iterator.next();
            final float pageHeight = currentPage.getMediaBox().getHeight();
            SegmentedContents contents = segmentedContents.get(currentPage);
            contentStream = new PDPageContentStream(document, currentPage, true, false);

            if (Arrays.asList(segments).contains(Segment.BLOCK_GROUP)) {
                visualizeGroups(contents.getGroups(), pageHeight);
            }
            if (Arrays.asList(segments).contains(Segment.BLOCK)) {
                visualizeBlocks(contents.getGroups(), pageHeight);
            }
            if (Arrays.asList(segments).contains(Segment.LINE)) {
                visualizeLines(contents.getLines(), pageHeight);
            }
            if (Arrays.asList(segments).contains(Segment.GLYPH)) {
                visualizeGlyphs(contents.getGlyphs(), pageHeight);
            }
            contentStream.close();
        }

        try {
            document.save(outputStream);
        } catch (COSVisitorException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Draws the bounding box of all the glyphs given as argument
     * 
     * @param glyphs
     *            The list of glyphs that will be visualised
     * @param pageHeight
     *            The height of the current page
     * @throws IOException
     */
    private void visualizeGlyphs(final List<Glyph> glyphs, final float pageHeight) throws IOException {
        for (final Glyph glyph : glyphs) {
            // each block will be highlighted in another colour
            contentStream.setStrokingColor(Color.RED);

            // we need a vertical shift in order to align the lines we are
            // drawing with the text lines
            final float verticalShift = (float) (glyph.getBottom() - glyph.getTop());
            final float xLeft = (float) glyph.getLeft();
            final float xRight = (float) glyph.getRight();
            final float yBottom = (float) (pageHeight - glyph.getBottom() + verticalShift);
            final float yTop = (float) (pageHeight - glyph.getTop() + verticalShift);

            // bottom line
            contentStream.drawLine(xLeft, yBottom, xRight, yBottom);
            contentStream.drawLine(xLeft, yBottom, xLeft, yTop);
            contentStream.drawLine(xLeft, yTop, xRight, yTop);
            contentStream.drawLine(xRight, yTop, xRight, yBottom);
        }
    }

    /**
     * Draws the bounding box of each line given as argument
     * 
     * @param lines
     *            The list of lines to be visualised
     * @param pageHeight
     *            The height of the current page
     * @throws IOException
     */
    private void visualizeLines(final List<Line> lines, final float pageHeight) throws IOException {
        for (final Line line : lines) {
            // each block will be highlighted in another colour
            contentStream.setStrokingColor(Color.GRAY);

            // we need a vertical shift in order to align the lines we are
            // drawing with the text lines
            final float verticalShift = (float) (line.getBottom() - line.getTop());
            final float xLeft = (float) line.getLeft();
            final float xRight = (float) line.getRight();
            final float yBottom = (float) (pageHeight - line.getBottom() + verticalShift);
            final float yTop = (float) (pageHeight - line.getTop() + verticalShift);

            // bottom line
            contentStream.drawLine(xLeft, yBottom, xRight, yBottom);
            contentStream.drawLine(xLeft, yBottom, xLeft, yTop);
            contentStream.drawLine(xLeft, yTop, xRight, yTop);
            contentStream.drawLine(xRight, yTop, xRight, yBottom);
        }
    }

    /**
     * Draws the bounding box of each block of the current page
     * 
     * @param groups
     *            The list of groups containing the blocks that will be
     *            visualised
     * @param pageHeight
     *            The height of the current page
     * @throws IOException
     */
    private void visualizeBlocks(final List<BlockGroup> groups, final float pageHeight) throws IOException {

        int i = 0;
        for (final BlockGroup group : groups) {
            List<Block> blocks = group.getBlocks();
            // the blocks belonging to the same group are highlighted in the
            // same color
            contentStream.setStrokingColor(colorsList.get(i % colorsList.size()));
            for (final Block block : blocks) {
                // vertical shift to adjust rectangles to the blocks (there
                // is a
                // difference in the coordinates due to PDFBox trying to
                // guess
                // the right text position on the pages
                final Line firstLine = block.getLines().get(0);
                final float verticalShift = (float) (firstLine.getBottom() - firstLine.getTop());

                // +1 and -1 are just for visual style (in order to separate
                // the
                // lines from the text
                final float xLeft = (float) block.getLeft() - 1;
                final float xRight = (float) block.getRight() + 1;
                final float yBottom = (float) (pageHeight - block.getBottom() + verticalShift - 1);
                final float yTop = (float) (pageHeight - block.getTop() + verticalShift + 1);

                // bottom line
                contentStream.drawLine(xLeft, yBottom, xRight, yBottom);
                contentStream.drawLine(xLeft, yBottom, xLeft, yTop);
                contentStream.drawLine(xLeft, yTop, xRight, yTop);
                contentStream.drawLine(xRight, yTop, xRight, yBottom);
            }
            i++;
        }
    }

    /**
     * Draws the bounding box of each group of the current page
     * 
     * @param groups
     *            The list of groups that will be visualised
     * @param pageHeight
     *            The height of the current page
     * @throws IOException
     */
    private void visualizeGroups(final List<BlockGroup> groups, final float pageHeight) throws IOException {

        for (BlockGroup group : groups) {
            List<Block> blocks = group.getBlocks();
            // We draw the lines representing the group but have to take care:
            // blocks may be on the same line (separated due to text
            // justification
            // for example
            Block firstBlock = blocks.get(0);
            float currentLeft = (float) firstBlock.getLeft();
            float currentRight = (float) firstBlock.getRight();
            float currentTop = (float) firstBlock.getTop();
            float currentBottom = (float) firstBlock.getBottom();
            float previousLineLeft;
            float previousLineRight;
            float xLeft, xRight, yTop, yBottom;
            int i = 1;
            final Line firstLine = firstBlock.getLines().get(0);
            final float verticalShift = (float) (firstLine.getBottom() - firstLine.getTop());

            while (i < blocks.size() && blocks.get(i).onSameLine(firstBlock)) {
                Block currentBlock = blocks.get(i);
                currentLeft = Math.min(currentLeft, (float) currentBlock.getLeft());
                currentRight = Math.max(currentRight, (float) currentBlock.getRight());
                currentTop = Math.min(currentTop, (float) currentBlock.getTop());
                currentBottom = Math.max(currentBottom, (float) currentBlock.getBottom());
                i++;
            }
            previousLineLeft = currentLeft;
            previousLineRight = currentRight;

            xLeft = currentLeft - 3;
            xRight = currentRight + 3;
            yTop = pageHeight - currentTop + verticalShift + 3;
            yBottom = pageHeight - currentBottom + verticalShift - 3;
            contentStream.setStrokingColor(Color.DARK_GRAY);
            // We have the first "line" of blocks, we draw the top line of it
            // and the left and right line too
            contentStream.drawLine(xLeft, yTop, xRight, yTop);
            contentStream.drawLine(xLeft, yTop, xLeft, yBottom);
            contentStream.drawLine(xRight, yTop, xRight, yBottom);

            if (i < blocks.size()) {
                // if there are blocks remaining

                // this is the first block that is not on the first line of the
                // page
                Block previousBlock = blocks.get(i);
                currentLeft = (float) previousBlock.getLeft();
                currentRight = (float) previousBlock.getRight();
                currentBottom = (float) previousBlock.getBottom();
                i++;
                while (i < blocks.size()) {
                    // now, we search other blocks that are on the same line
                    Block currentBlock = blocks.get(i);
                    while (i < blocks.size() && currentBlock.onSameLine(previousBlock)) {
                        currentBlock = blocks.get(i);
                        currentLeft = Math.min(currentLeft, (float) currentBlock.getLeft());
                        currentRight = Math.max(currentRight, (float) currentBlock.getRight());
                        currentBottom = Math.max(currentBottom, (float) currentBlock.getBottom());
                        previousBlock = currentBlock;
                        i++;
                    }

                    xLeft = currentLeft - 3;
                    xRight = currentRight + 3;
                    yTop = yBottom;
                    yBottom = pageHeight - currentBottom + verticalShift;
                    // we just found another line of blocks, we draw the
                    // horizontal
                    // lines between these blocks and the preceding
                    contentStream.drawLine(previousLineLeft - 3, yTop, xLeft, yTop);
                    contentStream.drawLine(previousLineRight + 3, yTop, xRight, yTop);
                    // and the left and right side of the current line of blocks
                    contentStream.drawLine(xLeft, yTop, xLeft, yBottom);
                    contentStream.drawLine(xRight, yTop, xRight, yBottom);

                    previousLineLeft = currentLeft;
                    previousLineRight = currentRight;
                    currentLeft = (float) currentBlock.getLeft();
                    currentRight = (float) currentBlock.getRight();
                    currentBottom = (float) currentBlock.getBottom();

                    previousBlock = currentBlock;
                    i++;
                }
            }

            xLeft = currentLeft - 3;
            xRight = currentRight + 3;
            yTop = yBottom;
            yBottom = pageHeight - currentBottom + verticalShift - 3;
            // we draw the final lines
            contentStream.drawLine(previousLineLeft - 3, yTop, xLeft, yTop);
            contentStream.drawLine(previousLineRight + 3, yTop, xRight, yTop);
            contentStream.drawLine(xLeft, yTop, xLeft, yBottom);
            contentStream.drawLine(xRight, yTop, xRight, yBottom);
            contentStream.drawLine(xLeft, yBottom, xRight, yBottom);
        }
    }
}
