package uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.model;

import java.util.ArrayList;
import java.util.List;

import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;
import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;

public class HieBlockGroup extends BlockGroup{
	private List<HieBlockGroup> subGroups;
	List<Line> lines;
	private double top, bottom, left, right;
	private HieBlockGroup parentGroup;

	
	public HieBlockGroup(BlockGroup group){
		subGroups = new ArrayList<HieBlockGroup>();
		this.setTop(group.getTop());
		this.setBottom(group.getBottom());
		this.setLeft(group.getLeft());
		this.setRight(group.getRight());
		lines = group.getLines();
	}
	
	public HieBlockGroup(){
		subGroups = new ArrayList<HieBlockGroup>();
		lines = new ArrayList<Line>();
	}
	
	public void addLine(Line line){
		if(lines.size() == 0){
			this.setLeft(line.getLeft());
			this.setRight(line.getRight());
			this.setTop(line.getTop());
			this.setBottom(line.getBottom());
		}
		else{
			if (line.getTop() < this.top) {
				this.top = line.getTop();
			}
			if (line.getBottom() > this.bottom) {
				this.bottom = line.getBottom();
			}
			if (line.getLeft() < this.left) {
				this.left = line.getLeft();
			}
			if (line.getRight() > this.right) {
				this.right = line.getRight();
			}
		}
		lines.add(line);
	}
	
	public void addSubGroup(HieBlockGroup group){
		if(subGroups.size() == 0){
			this.setTop(group.getTop());
			this.setBottom(group.getBottom());
			this.setLeft(group.getLeft());
			this.setRight(group.getRight());
		}
		else{
			if (group.getTop() < this.top) {
				this.top = group.getTop();
			}
			if (group.getBottom() > this.bottom) {
				this.bottom = group.getBottom();
			}
			if (group.getLeft() < this.left) {
				this.left = group.getLeft();
			}
			if (group.getRight() > this.right) {
				this.right = group.getRight();
			}
		}
		lines.addAll(group.getLines());
		subGroups.add(group);
		group.setParentGroup(this);
	}

	public List<HieBlockGroup> getSubGroups() {
		return subGroups;
	}

	public void setSubGroups(List<HieBlockGroup> subGroups) {
		this.subGroups = subGroups;
	}

	public double getTop() {
		return top;
	}

	public void setTop(double top) {
		this.top = top;
	}

	public double getBottom() {
		return bottom;
	}

	public void setBottom(double bottom) {
		this.bottom = bottom;
	}

	public double getLeft() {
		return left;
	}

	public void setLeft(double left) {
		this.left = left;
	}

	public double getRight() {
		return right;
	}

	public void setRight(double right) {
		this.right = right;
	}

	public List<Line> getLines(){
		return lines;
	}
	
	public String toString(){
		String returnValue = "";
		
		for (Line line : lines) {
			returnValue += line.toString() + " ";
		}

		return returnValue;
	}
	
	public void clearSubGroups(){
		subGroups.clear();
	}
	
	public boolean contains(BlockGroup blockGroup) {

		boolean containsTop = top <= blockGroup.getTop()
				|| Math.abs(top - blockGroup.getTop()) <= 3;
		boolean containsBottom = bottom >= blockGroup.getBottom()
				|| Math.abs(bottom - blockGroup.getBottom()) <= 3;
		boolean containsLeft = left <= blockGroup.getLeft()
				|| Math.abs(left - blockGroup.getLeft()) <= 3;
		boolean containsRight = right >= blockGroup.getRight()
				|| Math.abs(right - blockGroup.getRight()) <= 3;

		return containsTop && containsBottom && containsLeft && containsRight;
	}

	public HieBlockGroup getParentGroup() {
		return parentGroup;
	}

	public void setParentGroup(HieBlockGroup parentGroup) {
		this.parentGroup = parentGroup;
	}
	
}
