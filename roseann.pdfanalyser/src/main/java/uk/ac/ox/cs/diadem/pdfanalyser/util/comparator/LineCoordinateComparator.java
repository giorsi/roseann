/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.util.comparator;

import java.util.Comparator;
import java.util.List;

import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;

public class LineCoordinateComparator implements Comparator<Line> {

	List<Line> lines = null;
	
	public LineCoordinateComparator(List<Line> currentLines){
		lines = currentLines;
	}
	
    /* (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(Line line1, Line line2) {

        /*double topDifference = line1.getTop() - line2.getTop();
        double threshold = 0.1;

        if (Math.abs(topDifference) < threshold) {
            // the two fragments are on the same line
            if (line1.getLeft() < line2.getLeft()) {
                return -1;
            } else if (line1.getLeft() > line2.getLeft()) {
                return 1;
            } else {
                return 0;
            }
        } else if (line1.getTop() < line2.getTop()) {
            return -1;
        } else if (line1.getTop() > line2.getTop()) {
            return 1;
        } else {
            return 0;
        }*/
		// whether line1 and line2 has horizontal overlap
		boolean overlap = Math.max(line1.getLeft(), line2.getLeft()) < Math
				.min(line1.getRight(), line2.getRight());

		if (overlap || (!overlap && otherLineInBetween(line1, line2))) {
			// the two fragments are considered in the same column
			if (line1.getTop() < line2.getTop()) {
				return -1;
			} else if (line1.getTop() > line2.getTop()) {
				return 1;
			} else {
				return 0;
			}
		} else if (line1.getLeft() < line2.getLeft()) {
			return -1;
		} else if (line1.getLeft() > line2.getLeft()) {
			return 1;
		} else {
			return 0;
		}
    }

    private boolean otherLineInBetween(Line line1, Line line2){
		double top = Math.min(line1.getBottom(), line2.getBottom());
		double bottom = Math.max(line1.getTop(), line2.getTop());
		for(Line line: lines){
			// if group is in the middle
			if(line.getTop() > top && line.getBottom() < bottom){
				// if group overlaps with both group1 and group2
				boolean horiOverlap1 = Math.max(line1.getLeft(), line.getLeft()) < Math
						.min(line1.getRight(), line.getRight());
				boolean horiOverlap2 = Math.max(line2.getLeft(), line.getLeft()) < Math
						.min(line2.getRight(), line.getRight());
				if(horiOverlap1 && horiOverlap2){
					return true;
				}
			}
		}
		return false;
	}
}
