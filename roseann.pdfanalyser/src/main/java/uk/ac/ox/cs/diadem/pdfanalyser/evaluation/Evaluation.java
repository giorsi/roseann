/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.evaluation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import uk.ac.ox.cs.diadem.pdfanalyser.DocumentProcessor;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;

public class Evaluation {

    private static String outputPath = "";
    private static String truthPath = "";
    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentProcessor.class);

    /**
     * Evaluates the system on the test files
     * 
     * @return An {@link EvalResults} object containing the purity and
     *         completeness of the extraction
     */
    public static EvalResults evaluate() {
        
        outputPath = DocumentProcessor.outputDirectory;
        truthPath = DocumentProcessor.truthDirectory;
        final EvalResults easyResults = evaluate(Files.easyFiles);
        final EvalResults normalResults = evaluate(Files.normalFiles);
        final EvalResults nastyResults = evaluate(Files.nastyFiles);

        final float globalCompleteness = (easyResults.getCompleteness() + normalResults.getCompleteness() + nastyResults
                .getCompleteness()) / (float) 3;
        final float globalPurity = (easyResults.getPurity() + normalResults.getPurity() + nastyResults.getPurity())
                / (float) 3;
        final float globalPrecision = (easyResults.getPrecision() + normalResults.getPrecision() + nastyResults
                .getPrecision()) / (float) 3;
        final float globalRecall = (easyResults.getRecall() + normalResults.getRecall() + nastyResults.getRecall())
                / (float) 3;
        final EvalResults globalResults = new EvalResults(globalCompleteness, globalPurity, globalPrecision,
                globalRecall);

        return globalResults;
    }

    /**
     * @param filesMap
     *            Maps the files we want to use to the pages that will be tested
     * @return An {@link EvalResults} object containing the purity and
     *         completeness of the map given as input
     */
    private static EvalResults evaluate(Map<String, ArrayList<Integer>> filesMap) {
        final EvalResults results;
        int totalExtractedElements = 0;
        int totalExistingElements = 0;
        int totalPurity = 0;
        int totalCompleteness = 0;
        int totalCorrectRetrievedElements = 0;
        List<Block> pageExtractedBlocks = new ArrayList<Block>();
        List<Block> pageExistingBlocks = new ArrayList<Block>();
        final DocumentBuilderFactory dbFactoryOutput = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilderOutput;
        final DocumentBuilderFactory dbFactoryTruth = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilderTruth;

        for (String fileName : filesMap.keySet()) {
            int documentCompleteness = 0;
            int documentPurity = 0;
            int documentExtractedElements = 0;
            int documentExistingElements = 0;
            int documentCorrectRetrievedElements = 0;
            String outputFileName = outputPath + fileName + "_output.xml";
            String truthFileName = truthPath + fileName + "_truth.xml";
            File outputFile = new File(outputFileName);
            File truthFile = new File(truthFileName);

            try {
                dBuilderOutput = dbFactoryOutput.newDocumentBuilder();
                Document outputTree = dBuilderOutput.parse(outputFile);
                outputTree.getDocumentElement().normalize();

                dBuilderTruth = dbFactoryTruth.newDocumentBuilder();
                Document truthTree = dBuilderTruth.parse(truthFile);
                truthTree.getDocumentElement().normalize();

                NodeList blocks = outputTree.getDocumentElement().getElementsByTagName("block");

                // The first step is to add all the extracted and real blocks to
                // the corresponding lists

                if (blocks.getLength() < 1) {
                    // if there are no <block> elements in the output we cannot
                    // perform the evaluation
                    LOGGER.error("The evaluation must be run at the BLOCK level. "
                            + "There is no <block> element in the output " + outputFileName);
                } else {
                    NodeList pagesList = outputTree.getDocumentElement().getElementsByTagName("page");

                    for (int i = 0; i < pagesList.getLength(); i++) {
                        Element currentPage = (Element) pagesList.item(i);
                        String pageNumber = currentPage.getAttribute("number");

                        if (filesMap.get(fileName).contains(Integer.parseInt(pageNumber))) {

                            int pageCompleteness = 0;
                            int pagePurity = 0;
                            int pageExtractedElements = 0;
                            int pageExistingElements = 0;
                            int pageCorrectRetrievedElements = 0;
                            pageExistingBlocks = new ArrayList<Block>();
                            pageExtractedBlocks = new ArrayList<Block>();

                            blocks = currentPage.getElementsByTagName("block");

                            for (int j = 0; j < blocks.getLength(); j++) {
                                // we add all the blocks of the output to the
                                // extracted blocks list
                                Node currentBlock = blocks.item(j);
                                float top = Float.parseFloat(currentBlock.getAttributes().getNamedItem("top")
                                        .getTextContent());
                                float bottom = Float.parseFloat(currentBlock.getAttributes().getNamedItem("bottom")
                                        .getTextContent());
                                float left = Float.parseFloat(currentBlock.getAttributes().getNamedItem("left")
                                        .getTextContent());
                                float right = Float.parseFloat(currentBlock.getAttributes().getNamedItem("right")
                                        .getTextContent());
                                pageExtractedBlocks.add(new Block(top, bottom, left, right));

                            }

                            // we add all the real blocks to the existing blocks
                            // list
                            NodeList truthPages = truthTree.getElementsByTagName("page");
                            boolean pageFound = false;

                            for (int j = 0; j < truthPages.getLength() && !pageFound; j++) {
                                if (pageNumber.equals(truthPages.item(j).getAttributes().getNamedItem("number")
                                        .getTextContent())) {
                                    currentPage = (Element) truthPages.item(j);
                                    pageFound = true;
                                }
                            }

                            if (!pageFound) {
                                // This page was not specified in the ground
                                // truth
                                // file
                                LOGGER.error("Page number " + pageNumber + " does not exist in the groung truth file.");
                                System.exit(1);
                            }

                            blocks = currentPage.getElementsByTagName("block");

                            for (int j = 0; j < blocks.getLength(); j++) {
                                // we add all the blocks of the output to the
                                // extracted blocks list
                                Node currentBlock = blocks.item(j);
                                float top = Float.parseFloat(currentBlock.getAttributes().getNamedItem("top")
                                        .getTextContent());
                                float bottom = Float.parseFloat(currentBlock.getAttributes().getNamedItem("bottom")
                                        .getTextContent());
                                float left = Float.parseFloat(currentBlock.getAttributes().getNamedItem("left")
                                        .getTextContent());
                                float right = Float.parseFloat(currentBlock.getAttributes().getNamedItem("right")
                                        .getTextContent());
                                pageExistingBlocks.add(new Block(top, bottom, left, right));
                            }

                            pageExistingElements = pageExistingBlocks.size();
                            pageExtractedElements = pageExtractedBlocks.size();

                            for (int j = 0; j < pageExtractedBlocks.size(); j++) {
                                Block extracted = pageExtractedBlocks.get(j);
                                for (int k = 0; k < pageExistingBlocks.size(); k++) {
                                    Block existing = pageExistingBlocks.get(k);
                                    if (extracted.contains(existing)) {
                                        pageCompleteness++;
                                    }

                                    if (existing.contains(extracted)) {
                                        pagePurity++;
                                    }

                                    if (extracted.isIdentical(existing)) {
                                        pageCorrectRetrievedElements++;
                                    }
                                }
                            }

                            documentCorrectRetrievedElements += pageCorrectRetrievedElements;
                            documentExtractedElements += pageExtractedElements;
                            documentExistingElements += pageExistingElements;
                            documentCompleteness += pageCompleteness;
                            documentPurity += pagePurity;
                        }
                    }
                }
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            totalCorrectRetrievedElements += documentCorrectRetrievedElements;
            totalCompleteness += documentCompleteness;
            totalPurity += documentPurity;
            totalExtractedElements += documentExtractedElements;
            totalExistingElements += documentExistingElements;
        }

        final float completeness = totalCompleteness / (float) totalExistingElements;
        final float purity = totalPurity / (float) totalExtractedElements;

        final float precision = totalCorrectRetrievedElements / (float) totalExtractedElements;
        final float recall = totalCorrectRetrievedElements / (float) totalExistingElements;
        results = new EvalResults(completeness, purity, precision, recall);
        return results;
    }
}