package uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.model;

import java.util.ArrayList;
import java.util.List;

public class SemanticStructureBlockGroup implements SemanticStructure {
	private List<SemanticStructureBlock> semanticBlocks;
	
	public SemanticStructureBlockGroup(){
		semanticBlocks = new ArrayList<SemanticStructureBlock>();
	}
	
	public void addSemanticBlock(SemanticStructureBlock semanticBlock){
		this.semanticBlocks.add(semanticBlock);
	}
	
	public List<SemanticStructureBlock> getSemanticBlocks(){
		return this.semanticBlocks;
	}
	
	public String toString(){
		String text = "";
		for(SemanticStructureBlock block: semanticBlocks){
			text += block.toString() + " ";
		}
		return text;
	}
}
