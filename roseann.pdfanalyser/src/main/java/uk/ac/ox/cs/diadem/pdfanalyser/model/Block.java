/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uk.ac.ox.cs.diadem.pdfanalyser.util.Bullet;
import uk.ac.ox.cs.diadem.pdfanalyser.util.FontStyle;
import uk.ac.ox.cs.diadem.pdfanalyser.util.Punctuation;
import uk.ac.ox.cs.diadem.pdfanalyser.util.comparator.LineCoordinateComparator;

public class Block extends PDFSegment{

	/**
	 * @param line
	 *            The line that initialises a new block
	 */
	public Block(final Line line) {
		lines = new ArrayList<Line>();
		lines.add(line);
		top = line.getTop();
		bottom = line.getBottom();
		left = line.getLeft();
		right = line.getRight();
		fontSize = line.getFontSize();
	}
	
	/**
	 * @param lines
	 * 			the lines that are contained in a new block
	 */
	public Block(final List<Line> lines){
		this(lines.get(0));
		for(int i=1; i<lines.size(); i++){
			addLine(lines.get(i));
		}
	}

	/**
	 * This constructor is only used to create block entities for the evaluation
	 * process
	 * 
	 * @param top
	 *            The top coordinate of the block
	 * @param bottom
	 *            The bottom coordinate of the block
	 * @param left
	 *            The left coordinate of the block
	 * @param right
	 *            The right coordinate of the block
	 * 
	 */
	public Block(final float top, final float bottom, final float left,
			final float right) {
		this.top = top;
		this.bottom = bottom;
		this.left = left;
		this.right = right;
		fontSize = 0;
	}

	private BlockGroup group;
	private List<Line> lines;

	/**
	 * Adds a line to the current block
	 * 
	 * @param line
	 *            The line to be added
	 */
	public void addLine(final Line line) {
		lines.add(line);

		// we update the coordinates of the bounding box of the block
		if (line.getTop() < top) {
			top = line.getTop();
		}
		if (line.getLeft() < left) {
			left = line.getLeft();
		}
		if (line.getRight() > right) {
			right = line.getRight();
		}
		if (line.getBottom() > bottom) {
			bottom = line.getBottom();
		}
	}

	/**
	 * Calculates the overlap ratio between the given line and tue current block
	 * 
	 * @param line
	 * @return The overlapping ratio
	 */
	public double getVertivalOverlapRatio(final Line line) {
		double ratio = 0.;
		int flag = 0;
		if (line.getLeft() <= left && left <= line.getRight()
				&& line.getRight() <= right) {
			flag = 1;
		} else if (left <= line.getLeft() && line.getLeft() <= right
				&& right <= line.getRight()) {
			flag = 2;
		} else if (left <= line.getLeft() && line.getLeft() <= line.getRight()
				&& line.getRight() <= right) {
			flag = 3;
		} else if (line.getLeft() <= left && left <= right
				&& right <= line.getRight()) {
			flag = 4;
		}

		final double delta = Math.min(right - left,
				line.getRight() - line.getLeft());

		switch (flag) {
		case 1:
			ratio = (line.getRight() - left) / delta;
			break;
		case 2:
			ratio = (right - line.getLeft()) / delta;
			break;
		case 3:
			ratio = (line.getRight() - line.getLeft()) / delta;
			break;
		case 4:
			ratio = (right - left) / delta;
			break;
		default:
			ratio = 0.;
		}

		return ratio;
	}

	/**
	 * @return The lines contained in the block
	 */
	public List<Line> getLines() {
		return lines;
	}

	/**
	 * @return The group containing the block
	 */
	public BlockGroup getGroup() {
		return group;
	}

	/**
	 * @param group
	 */
	public void setGroup(final BlockGroup group) {
		this.group = group;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String retValue = "";

		if (lines != null) {
			sortLines();

			for (final Line l : lines) {
				retValue += l.toString() + " ";
			}
		} else {
			retValue = "top: " + top + "; bottom: " + bottom + "; left: "
					+ left + "; right: " + right;
		}

		return retValue;
	}

	/**
	 * sort lines according to reading order
	 */
	public void sortLines() {
		if (lines != null) {
			final LineCoordinateComparator comparator = new LineCoordinateComparator(lines);
			Collections.sort(lines, comparator);
		}
	}

	/**
	 * @return The average vertical space within the block
	 */
	public double getAverageVerticalSpace() {
		return getTotalVerticalSpacing() / getTotalVerticalBlanks();
	}

	/**
	 * @return The number of inter-lines spaces
	 */
	public int getTotalVerticalBlanks() {
		return lines.size() - 1;
	}

	/**
	 * @return
	 */
	public double getTotalVerticalSpacing() {
		double totalSpace = 0.;
		if (!lines.isEmpty()) {
			Line precedingLine = lines.get(0);

			for (int i = 1; i < lines.size(); i++) {
				final Line currentLine = lines.get(i);

				final double currentSpace = currentLine.getTop()
						- precedingLine.getBottom();
				totalSpace += currentSpace;
				precedingLine = currentLine;
			}
		}

		return totalSpace;
	}

	/**
	 * Checks if the given line and the current block have the same style or
	 * not.
	 * 
	 * @param line
	 *            The line to be compared to the block
	 * @return True if the line and block have the same style, false otherwise
	 */
	public boolean hasSameStyle(final Line line) {

		boolean sameStyle = true;
		//final boolean size = Math.abs(1 - line.getFontSize() / fontSize) < 0.1;
		final boolean size = Math.abs(line.getFontSize()-fontSize) < 1;

		final Line lastLine = lines.get(lines.size() - 1);

		// The firstfont should be the first glyphs that is not a punctuation
		String firstFont = "";
		for (int i = 0; i < line.getGlyphs().size(); i++) {
			if (!Punctuation.isPunctuation(line.getGlyphs().get(i)) && !Bullet.isBullet(line.getGlyphs().get(i))) {
				firstFont = line.getGlyphs().get(i).getFontName();
				break;
			}
		}
		if (firstFont.equals("")) {
			return false;
		}
		// TODO: if the firstFont or some glyphs in last line is not in the
		// frontStyleMap,
		// we simply check whether is is in the last line
		boolean lastLineFontRecorgnized = true;
		for (int i = 0; i < lastLine.getGlyphs().size()
				&& lastLineFontRecorgnized; i++) {
			lastLineFontRecorgnized = FontStyle.fontStylesMap.get(lastLine
					.getGlyphs().get(i).getFontName()) != null;
		}
		boolean firstFrontRecorgnized = FontStyle.fontStylesMap.get(firstFont) != null;
		if ((!lastLineFontRecorgnized) || (!firstFrontRecorgnized)) {
			sameStyle = lastLine.getFontFamily().contains(firstFont);
			return size && sameStyle;
		}
		if (FontStyle.fontStylesMap.get(firstFont).isBold()) {
			// if the first glyph is bold, the same bold font must be found in
			// the last line of the current block and also both the first glyph
			// and
			// last glyph should be bold(there is likely that only one word in
			// the
			// last line is made to be bold to be emphasized), otherwise the
			// styles are not
			// equivalent
			if (!lastLine.getFontFamily().contains(firstFont)) {
				sameStyle = false;
			} else {
				List<Glyph> lastLineGlyphs = lastLine.getGlyphs();
				if (lastLineGlyphs.size() > 0) {
					boolean firstGlyphBold = FontStyle.fontStylesMap.get(
							lastLineGlyphs.get(0).getFontName()).isBold();
					boolean lastGlyphBold = FontStyle.fontStylesMap.get(
							lastLineGlyphs.get(lastLineGlyphs.size() - 1)
									.getFontName()).isBold();
					if ((!firstGlyphBold) || (!lastGlyphBold)) {
						sameStyle = false;
					}
				}
			}

		} else {
			// that means that the glyph is either regular or italic => the same
			// base font name must be retrieved in the last line of the current
			// block

			// If last line of the block is completely bold(except punctuation),
			// we consider it
			// as a title
			boolean lastLineIsBold = true;

			for (int i = 0; i < lastLine.getGlyphs().size() && lastLineIsBold; i++) {
				Glyph curGlyph = lastLine.getGlyphs().get(i);
				if (Character.isLetter(curGlyph.getCharacter().charAt(0))) {
					lastLineIsBold = FontStyle.fontStylesMap.get(
							curGlyph.getFontName()).isBold();
				}
			}

			// If the current line is completely bold, so it is also part of the
			// title
			boolean lineIsBold = true;

			for (int i = 0; i < line.getGlyphs().size() && lineIsBold; i++) {
				Glyph curGlyph = line.getGlyphs().get(i);
				if (Character.isLetter(curGlyph.getCharacter().charAt(0)) && FontStyle.fontStylesMap.get(
						curGlyph.getFontName())!=null) {
					lineIsBold = FontStyle.fontStylesMap.get(
							curGlyph.getFontName()).isBold();
				}
			}

			// also if the last line is completely in capital(except
			// punctuation), we also consider it as
			// a title
			boolean lastLineIsCapital = true;

			for (int i = 0; i < lastLine.getGlyphs().size()
					&& lastLineIsCapital; i++) {
				char curChar = lastLine.getGlyphs().get(i).getCharacter()
						.charAt(0);
				if (Character.isLetter(curChar)) {
					lastLineIsCapital = Character.isUpperCase(curChar);
				}
			}

			// also if the line is completely in capital, we also consider it as
			// part of a title
			boolean lineIsCapital = true;

			for (int i = 0; i < line.getGlyphs().size() && lineIsCapital; i++) {
				char curChar = line.getGlyphs().get(i).getCharacter().charAt(0);
				if (Character.isLetter(curChar)) {
					lineIsCapital = Character.isUpperCase(curChar);
				}
			}

			final String baseFontName = FontStyle.fontStylesMap.get(firstFont)
					.getBaseFontName();
			int i = 0;
			boolean fontFound = false;

			while (i < lastLine.getGlyphs().size()
					&& (!fontFound || lastLineIsBold || lastLineIsCapital)) {
				final Glyph currentGlyph = lastLine.getGlyphs().get(i);
				final String currentBaseFontName = FontStyle.fontStylesMap.get(
						currentGlyph.getFontName()).getBaseFontName();

				if (baseFontName.equals(currentBaseFontName)) {
					fontFound = true;
				}
				i++;
			}

			// (1) if currentLine and the lastLine don't even have the same
			// fontName,
			// their style must be different
			// (2) If they have the same font name, the previous block could be
			// a title.
			// They can only be grouped together when the current block is also
			// part of the title
			if (!fontFound) {
				sameStyle = false;
			} else {
				if ((!lastLineIsBold) && (!lastLineIsCapital)) {
					// last line is neither bold nor capital, the current line shouldn't be bold
					// but it can be capital
					sameStyle = !lineIsBold;
				} else if (lastLineIsBold && lastLineIsCapital) {
					// when both of the two lines are both bold and capital.
					// They have same style
					sameStyle = lineIsBold && lineIsCapital;
				} else if (lastLineIsBold) {
					// when last line is bold but not capital, the current line
					// must be bold as well
					sameStyle = lineIsBold;
				} 
			}
		}

		return size && sameStyle;
	}

	/**
	 * Checks if the given and the current blocks have the same style or not.
	 * 
	 * @param block
	 *            The block to be compared to the current one
	 * @return True if the current and the given block have the same style,
	 *         false otherwise
	 */
	public boolean hasSameStyle(final Block block) {
		// If the difference of fontSize is smaller than 10%
		final boolean size = Math.abs(1 - block.getFontSize() / fontSize) < 0.1;

		// if block is below the current block, we should compare current block
		// with block's first line
		if (block.getTop() > this.getBottom()) {
			return size && hasSameStyle(block.getLines().get(0));
		}
		// if block is above the current block, we should compare the block with
		// the first line of
		// the current block
		else {
			return size && block.hasSameStyle(getLines().get(0));
		}
	}

	/**
	 * Checks if the current block and the given one are on the same line
	 * 
	 * @param otherBlock
	 * @return True if both block belong to the same line, false otherwise
	 */
	public boolean onSameLine(Block otherBlock) {

		boolean onSameLine = false;

		if (this.getTop() >= otherBlock.getTop()
				&& this.getTop() < otherBlock.getBottom()
				|| this.getBottom() > otherBlock.getTop()
				&& this.getBottom() <= otherBlock.getBottom()) {
			onSameLine = true;
		}

		return onSameLine;
	}

	public boolean contains(Block block) {

		boolean containsTop = top <= block.top
				|| Math.abs(top - block.top) <= 1;
		boolean containsBottom = bottom >= block.bottom
				|| Math.abs(bottom - block.bottom) <= 1;
		boolean containsLeft = left <= block.left
				|| Math.abs(left - block.left) <= 1;
		boolean containsRight = right >= block.right
				|| Math.abs(right - block.right) <= 1;

		return containsTop && containsBottom && containsLeft && containsRight;
	}

	public boolean isIdentical(Block block) {
		boolean identicalTop = Math.abs(top - block.top) <= 1;
		boolean identicalBottom = Math.abs(bottom - block.bottom) <= 1;
		boolean identicalLeft = Math.abs(left - block.left) <= 1;
		boolean identicalRight = Math.abs(right - block.right) <= 1;

		return identicalTop && identicalBottom && identicalLeft
				&& identicalRight;
	}
	
	public double getVerticalSpace(){
		Line firstLine = this.getLines().get(0);
		return (firstLine.getBottom() - firstLine.getTop());
	}
}
