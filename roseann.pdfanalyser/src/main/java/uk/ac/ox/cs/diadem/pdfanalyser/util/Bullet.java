/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 */
package uk.ac.ox.cs.diadem.pdfanalyser.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.documentinterchange.taggedpdf.PDListAttributeObject;

import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.AnnotationSegmentor;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.util.Saliency;

public class Bullet {
	
	public static boolean sameType(String fragment1, String fragment2){
		// both are strong types
		if(isStrongBullet(fragment1) && isStrongBullet(fragment2)){
			return fragment1.equals(fragment2);
		}
		// both are weak types
		if(!isStrongBullet(fragment1) && !isStrongBullet(fragment2)){
			boolean bothDotType = fragment1.trim().endsWith(".") && fragment2.trim().endsWith(".");
			boolean bothBranketType = fragment1.trim().endsWith(")") && fragment2.trim().endsWith(")");
			return bothDotType || bothBranketType;
		}
		//one strong one weak types
		return false;
	}
	
	public static boolean isBullet(Glyph glyph) {
		boolean isBullet = false;
		String character = glyph.getCharacter();

		if (character.equals("■") || character.equals("•") || character.equals("●")
				|| character.equals("#")) {
			isBullet = true;
		}

		return isBullet;
	}

	public static boolean isStrongBullet(String fragment) {
		boolean isBullet = false;

		if (fragment.equals("■") || fragment.equals("•") || fragment.equals("●")
				|| fragment.equals("#")) {
			isBullet = true;
		}

		return isBullet;
	}

	public static boolean isBullet(String fragment) {
		boolean isBullet = false;

		fragment = fragment.trim();

		if (fragment.equals("■") || fragment.equals("•") || fragment.equals("●")
				|| fragment.equals("#")) {
			isBullet = true;
		}
		
		if(fragment.length() > 1){
			char lastChar = fragment.charAt(fragment.length() - 1);
			if(lastChar == ')' || lastChar == '.'){
				String remaining = fragment.substring(0, fragment.length()-1);
				// there is usually number with '.' and letters/numbers with ')'
				if(remaining.length() == 1&&Character.isLetter(remaining.charAt(0)) && lastChar == ')'){
					isBullet = true;
				}
				else if(isNumber(remaining)){
					isBullet = true;
				}
			}
		}
		
//		AnnotationSegmentor as = new AnnotationSegmentor();
//		if(as.isBulletMark(fragment)){
//			isBullet = true;
//		}
		
		return isBullet;
	}
	
	private static boolean isNumber(String str) {
	    try {
	        Integer.parseInt(str);
	        return true;
	    } catch (NumberFormatException nfe) {}
	    return false;
	}

	public static boolean isBullet(String lineFragment, int index,
			List<Line> lines, PDPage currentPage) {
		boolean isBullet = true;

		if (!isBullet(lineFragment)) {
			return false;
		}
		
		if(isStrongBullet(lineFragment)){
			return true;
		}

		Line currentLine = lines.get(index);
		Line previousLine = index > 0 ? lines.get(index - 1) : null;
		Line nextLine = index < lines.size() - 1 ? lines.get(index + 1) : null;
		
		List<Line> linesToEvaluate = new ArrayList<Line>();
		List<Line> otherLines = new ArrayList<Line>();
		linesToEvaluate.add(currentLine);
		if (previousLine != null && !previousLine.toString().trim().equals("")
				&& (previousLine.getLeft() - currentLine.getLeft()) > 0.1
				&& previousLine.getTop() < currentLine.getTop()) {
			boolean regionEmpty = RegionProcessing.isRegionEmpty(currentPage,
					currentLine.getLeft(), previousLine.getTop(),
					previousLine.getLeft() - currentLine.getLeft(),
					currentLine.getTop() - previousLine.getTop());
			if (regionEmpty == false) {
				isBullet = false;
			}
		}
		if (previousLine != null && !previousLine.toString().trim().equals("")
				&& previousLine.toString().length() > lineFragment.length()
				&& isBullet(previousLine.toString().substring(0, lineFragment.length()))
				&& getBulletLength(previousLine.toString()) == lineFragment.length()
				&& Math.abs(previousLine.getLeft() - currentLine.getLeft()) <= 0.1
				&& previousLine.getTop() < currentLine.getTop()) {
			otherLines.add(previousLine);
			// another bullet item. The saliency of it should be the same with
			// the current one
			if (Saliency.lineFontSaliency(linesToEvaluate, otherLines) != 0) {
				isBullet = false;
			}
		}
		if (nextLine != null && !nextLine.toString().trim().equals("")
				&& (nextLine.getLeft() - currentLine.getLeft()) > 0.1
				&& nextLine.getTop() > currentLine.getTop()) {
			boolean regionEmpty = RegionProcessing.isRegionEmpty(currentPage,
					currentLine.getLeft(), nextLine.getTop(),
					nextLine.getLeft() - currentLine.getLeft(),
					nextLine.getTop() - currentLine.getTop());
			if (regionEmpty == false) {
				isBullet = false;
			}
		}
		if (nextLine != null && !nextLine.toString().trim().equals("")
				&& nextLine.toString().length() > lineFragment.length()
				&& isBullet(nextLine.toString().substring(0, lineFragment.length()))
				&& getBulletLength(nextLine.toString()) == lineFragment.length()
				&& Math.abs(nextLine.getLeft() - currentLine.getLeft()) <= 0.1
				&& nextLine.getTop() > currentLine.getTop()) {
			otherLines.add(nextLine);
			if (Saliency.lineFontSaliency(linesToEvaluate, otherLines) != 0) {
				isBullet = false;
			}
		}
		return isBullet;
	}
	
	public static int getStrongBulletLength(String text){
		int bulletLength = 0;
		text = text.trim();
		
		if (text.length() > 0 && Bullet.isStrongBullet(text.substring(0, 1))) {
			bulletLength = 1;
		} else if (text.length() > 1 && Bullet.isStrongBullet(text.substring(0, 2))) {
			bulletLength = 2;
		} 
		return bulletLength;
	}
	
	public static int getBulletLength(String text) {
		int bulletLength = 0;
		text = text.trim();
		
		if (text.length() > 0 && Bullet.isBullet(text.substring(0, 1))) {
			bulletLength = 1;
		} else if (text.length() > 1 && Bullet.isBullet(text.substring(0, 2))) {
			bulletLength = 2;
		} else if (text.length() > 2 && Bullet.isBullet(text.substring(0, 3))) {
			bulletLength = 3;
		} else if (text.length() > 3 && Bullet.isBullet(text.substring(0, 4))) {
			bulletLength = 4;
		}
		return bulletLength;
	}
	

}
