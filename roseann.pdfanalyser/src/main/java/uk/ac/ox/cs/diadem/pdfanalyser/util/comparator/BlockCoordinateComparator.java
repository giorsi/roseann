package uk.ac.ox.cs.diadem.pdfanalyser.util.comparator;

import java.util.Comparator;

import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;

public class BlockCoordinateComparator implements Comparator<Block>{
	 @Override
	    public int compare(Block block1, Block block2) {
			// whether line1 and line2 has horizontal overlap
			boolean overlap = Math.max(block1.getLeft(), block2.getLeft()) < Math
					.min(block1.getRight(), block2.getRight());

			if (overlap) {
				// the two fragments are considered in the same column
				if (block1.getTop() < block2.getTop()) {
					return -1;
				} else if (block1.getTop() > block2.getTop()) {
					return 1;
				} else {
					return 0;
				}
			} else if (block1.getLeft() < block2.getLeft()) {
				return -1;
			} else if (block1.getLeft() > block2.getLeft()) {
				return 1;
			} else {
				return 0;
			}
	    }
}
