/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.util;

import java.util.Comparator;

public class FontFrequencyComparator implements Comparator<FontFrequency> {

    @Override
    public int compare(final FontFrequency font1, final FontFrequency font2) {

        if (font1.getFrequency() > font2.getFrequency()) {
            return -1;
        } else if (font1.getFrequency() < font2.getFrequency()) {
            return 1;
        } else {
            return 0;
        }
    }
}
