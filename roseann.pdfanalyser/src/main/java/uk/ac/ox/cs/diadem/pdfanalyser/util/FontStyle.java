/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.util;

import java.util.LinkedHashMap;
import java.util.Map;

public class FontStyle {

    private final String baseFontName;
    private final boolean isBold;
    private final boolean isItalic;

    public static Map<String, FontStyle> fontStylesMap = new LinkedHashMap<String, FontStyle>();

    /**
     * Initialises the map of fonts (giving the features of each font: base font
     * name, bold and italic)
     */
    public static void initFontStylesMap() {
        fontStylesMap.put("CMTI9", new FontStyle("cm", false, true));
        fontStylesMap.put("NimbusRomNo9L-Medi", new FontStyle("NimbusRomanNo9L", true, false));
        fontStylesMap.put("NimbusRomanNo9L", new FontStyle("NimbusRomanNo9L", false, false));
        fontStylesMap.put("NimbusSansL", new FontStyle("NimbusSansL", false, false));
        fontStylesMap.put("CMR9", new FontStyle("cm", false, false));
        fontStylesMap.put("CMTT9", new FontStyle("cm", false, false));
        fontStylesMap.put("CMBX9", new FontStyle("cm", true, false));
        fontStylesMap.put("CMR6", new FontStyle("cm", false, false));
        fontStylesMap.put("CMR10", new FontStyle("cm", false, false));
        fontStylesMap.put("CMBX12", new FontStyle("cm", true, false));
        fontStylesMap.put("TimesNewRoman", new FontStyle("TimesNewRoman", false, false));
        fontStylesMap.put("TimesNewRomanPSMT", new FontStyle("TimesNewRomanPSMT", false, false));
        fontStylesMap.put("TimesNewRomanPS-ItalicMT", new FontStyle("TimesNewRomanPSMT", false, true));
        fontStylesMap.put("TimesNewRomanPS-BoldMT", new FontStyle("TimeNewsRomanPS", true, false));
        fontStylesMap.put("TimesLTStd-Roman", new FontStyle("TimesLTStd", false, false));
        fontStylesMap.put("TimesLTStd-Italic", new FontStyle("TimesLTStd", false, true));
        fontStylesMap.put("TimesLTStd-Bold", new FontStyle("TimesLTStd", true, false));
        fontStylesMap.put("TimesLTStd-BoldItalic", new FontStyle("TimesLTStd", true, true));
        fontStylesMap.put("Times New Roman,Bold", new FontStyle("Times New Roman", true, false));
        fontStylesMap.put("Times New Roman", new FontStyle("Times New Roman", false, false));
        fontStylesMap.put("Times New Roman,Italic", new FontStyle("Times New Roman", false, true));
        fontStylesMap.put("Times-BoldItalic", new FontStyle("Times", true, true));
        fontStylesMap.put("SymbolMT", new FontStyle("SymbolMT", false, false));
        fontStylesMap.put("FranklinGothic-Demi", new FontStyle("FranklinGothic-Demi", false, false));
        fontStylesMap.put("FranklinGothic-DemiItalic", new FontStyle("FranklinGothic-Demi", false, true));
        fontStylesMap.put("FranklinGothic-BookItalic", new FontStyle("FranklinGothic-Book", false, true));
        fontStylesMap.put("FranklinGothic-Book", new FontStyle("FranklinGothic-Book", false, false));
        fontStylesMap.put("FranklinGothic-Medium", new FontStyle("FranklinGothic-Medium", false, false));
        fontStylesMap.put("LiberationSerif", new FontStyle("LiberationSerif", false, false));
        fontStylesMap.put("LiberationSerif-Italic", new FontStyle("LiberationSerif", false, true));
        fontStylesMap.put("LiberationSerif-Bold", new FontStyle("LiberationSerif", true, false));
        fontStylesMap.put("LiberationSerif-BoldItalic", new FontStyle("LiberationSerif", true, true));
        fontStylesMap.put("LiberationSans-Italic", new FontStyle("LiberationSans", false, true));
        fontStylesMap.put("LiberationSans-Bold", new FontStyle("LiberationSans", true, false));
        fontStylesMap.put("LiberationSans", new FontStyle("LiberationSans", false, false));
        fontStylesMap.put("DejaVuSans", new FontStyle("DejaVuSans", false, false));
        fontStylesMap.put("SymbolMT", new FontStyle("SymbolMT", false, false));
        fontStylesMap.put("ArialMT", new FontStyle("ArialMT", false, false));
        fontStylesMap.put("Arial-BoldMT", new FontStyle("ArialMT", true, false));
        fontStylesMap.put("Arial-ItalicMT", new FontStyle("ArialMT", false, true));
        fontStylesMap.put("Arial-BoldItalicMT", new FontStyle("ArialMT", true, true));
        fontStylesMap.put("Arial", new FontStyle("Arial", false, false));
        fontStylesMap.put("CairoFont-5-0", new FontStyle("CairoFont", false, false));
        fontStylesMap.put("CairoFont-14-0", new FontStyle("CairoFont", false, false));
        fontStylesMap.put("CairoFont-16-0", new FontStyle("CairoFont", false, false));
        fontStylesMap.put("CairoFont-2-0", new FontStyle("CairoFont", true, false));
        fontStylesMap.put("CairoFont-12-0", new FontStyle("CairoFont", true, false));
        fontStylesMap.put("CairoFont-17-0", new FontStyle("CairoFont", true, false));
        fontStylesMap.put("CairoFont-0-0", new FontStyle("CairoFont", true, true));
        fontStylesMap.put("CairoFont-1-0", new FontStyle("CairoFont", false, false));
        fontStylesMap.put("CairoFont-3-0", new FontStyle("CairoFont", false, true));
        fontStylesMap.put("CairoFont-4-0", new FontStyle("CairoFont", false, false));
        fontStylesMap.put("CairoFont-6-0", new FontStyle("CairoFont", false, false));
        fontStylesMap.put("CairoFont-11-0", new FontStyle("CairoFont", false, true));
        fontStylesMap.put("CairoFont-7-0", new FontStyle("CairoFont", true, false));
        fontStylesMap.put("CairoFont-28-0", new FontStyle("CairoFont", false, false));
        fontStylesMap.put("CairoFont-27-0", new FontStyle("CairoFont", true, false));
        //TODO: how to tell whether it's bold and italic
        fontStylesMap.put("CairoFont-15-0", new FontStyle("CairoFront", false, false));
        fontStylesMap.put("Batang", new FontStyle("Batang", false, false));
        fontStylesMap.put("CenturyGothic-Bold", new FontStyle("CenturyGothic", true, false));
        fontStylesMap.put("CenturyGothic", new FontStyle("CenturyGothic", false, false));
        fontStylesMap.put("Garamond", new FontStyle("Garamond", false, false));
        fontStylesMap.put("Palatino-Roman", new FontStyle("Palatino-Roman", false, false));
        fontStylesMap.put("Palatino-Bold", new FontStyle("Palatino-Roman", true, false));
        fontStylesMap.put("Palatino-Italic", new FontStyle("Palatino-Roman", false, true));
        fontStylesMap.put("DejaVuSans-Bold", new FontStyle("DejaVuSans", true, false));
        fontStylesMap.put("DejaVuSans-ExtraLight", new FontStyle("DejaVuSans", false, true));
        fontStylesMap.put("Dingbats", new FontStyle("Dingbats", false, false));
        fontStylesMap.put("SIL-Song-Reg-Jian", new FontStyle("SIL-Song-Reg-Jian", true, false));
        fontStylesMap.put("ArrusBT-Roman", new FontStyle("Arrus-Roman", true, false));
        fontStylesMap.put("Wingdings-Regular", new FontStyle("Wingdings-Regular", false, false));
        fontStylesMap.put("Calibri-BoldItalic", new FontStyle("Calibri", true, true));
        fontStylesMap.put("Calibri-Italic", new FontStyle("Calibri", false, true));
        fontStylesMap.put("Calibri", new FontStyle("Calibri", false, false));
        fontStylesMap.put("Calibri-Bold", new FontStyle("Calibri", true, false));
        fontStylesMap.put("CharterOSITCTT", new FontStyle("CharterOSITCTT", false, false));
        fontStylesMap.put("HelveticaNeueBold", new FontStyle("HelveticaNeue", true, false));
        fontStylesMap.put("Helvetica", new FontStyle("Helvetica", false, false));
        fontStylesMap.put("Helvetica-Bold", new FontStyle("Helvetica", true, false));
        fontStylesMap.put("Bembo", new FontStyle("Bembo", false, false));
        fontStylesMap.put("Bembo-Bold", new FontStyle("Bembo", true, false));
        fontStylesMap.put("Times-Roman", new FontStyle("Times-Roman", false, false));
        fontStylesMap.put("ACaslon-Regular", new FontStyle("ACaslon", false, false));
        fontStylesMap.put("ACaslon-Italic", new FontStyle("ACaslon", false, true));
    }

    /**
     * @param name The base name of the font
     * @param bold Whether the font is bold
     * @param italic Whether the font is italic
     */
    public FontStyle(final String name, final boolean bold, final boolean italic) {
        baseFontName = name;
        isBold = bold;
        isItalic = italic;
    }

    /**
     * @return
     */
    public String getBaseFontName() {
        return baseFontName;
    }

    /**
     * @return
     */
    public boolean isBold() {
        return isBold;
    }

    /**
     * @return
     */
    public boolean isItalic() {
        return isItalic;
    }
}
