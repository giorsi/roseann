package uk.ac.ox.cs.diadem.pdfanalyser.evaluation;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Files {
    public final static List<String> filesList = new ArrayList<String>();
    public final static Map<String, ArrayList<Integer>> easyFiles = new LinkedHashMap<String, ArrayList<Integer>>();
    public final static Map<String, ArrayList<Integer>> normalFiles = new LinkedHashMap<String, ArrayList<Integer>>();
    public final static Map<String, ArrayList<Integer>> nastyFiles = new LinkedHashMap<String, ArrayList<Integer>>();

    /**
     * Initialises the evaluation lists
     */
    public static void initEvaluationFiles() {
        initEasyFiles();
        initNormalFiles();
        initNastyFiles();
    }

    /**
     * Adds the evaluation files to the filesList
     */
    public static void initFilesList() {
        filesList.add("d-us-001.pdf");
        filesList.add("d-us-002.pdf");
        filesList.add("d-us-003.pdf");
        filesList.add("d-us-006.pdf");
        filesList.add("d-us-008.pdf");
        filesList.add("d-us-010.pdf");
        filesList.add("d-us-011.pdf");
        filesList.add("d-us-013.pdf");
        filesList.add("d-us-14.pdf");
        filesList.add("d-us-015.pdf");
        filesList.add("d-us-016.pdf");
        filesList.add("us-03.pdf");
        filesList.add("us-07.pdf");
        filesList.add("us-08.pdf");
        filesList.add("us-32.pdf");
        filesList.add("us-33.pdf");
        filesList.add("us-41.pdf");
    }

    /**
     * Adds the file (as a key) and the pages (as the value) to the easyFiles map
     */
    private static void initEasyFiles() {
        ArrayList<Integer> pagesList = new ArrayList<Integer>();
        pagesList.add(16);
        pagesList.add(17);
        pagesList.add(22);
        easyFiles.put("d-us-002.pdf", pagesList);

        pagesList = new ArrayList<Integer>();
        pagesList.add(20);
        pagesList.add(21);
        easyFiles.put("d-us-015.pdf", pagesList);

        pagesList = new ArrayList<Integer>();
        pagesList.add(27);
        pagesList.add(41);
        easyFiles.put("d-us-14.pdf", pagesList);

        pagesList = new ArrayList<Integer>();
        pagesList.add(4);
        pagesList.add(46);
        easyFiles.put("d-us-013.pdf", pagesList);

        pagesList = new ArrayList<Integer>();
        pagesList.add(14);
        easyFiles.put("d-us-003.pdf", pagesList);

        pagesList = new ArrayList<Integer>();
        pagesList.add(6);
        easyFiles.put("d-us-006.pdf", pagesList);

        pagesList = new ArrayList<Integer>();
        pagesList.add(9);
        pagesList.add(21);
        easyFiles.put("d-us-008.pdf", pagesList);

        pagesList = new ArrayList<Integer>();
        pagesList.add(1);
        easyFiles.put("d-us-011.pdf", pagesList);

        pagesList = new ArrayList<Integer>();
        pagesList.add(155);
        easyFiles.put("us-08.pdf", pagesList);
    }
    
    /**
     * Adds the file (as a key) and the pages (as the value) to the normalFiles map
     */
    private static void initNormalFiles()
    {
        ArrayList<Integer> pagesList = new ArrayList<Integer>();
        pagesList.add(4);
        pagesList.add(9);
        pagesList.add(14);
        normalFiles.put("d-us-001.pdf", pagesList);
        
        pagesList = new ArrayList<Integer>();
        pagesList.add(15);
        pagesList.add(23);
        pagesList.add(52);
        normalFiles.put("d-us-013.pdf", pagesList);
        
        pagesList = new ArrayList<Integer>();
        pagesList.add(12);
        pagesList.add(24);
        normalFiles.put("d-us-14.pdf", pagesList);
        
        pagesList = new ArrayList<Integer>();
        pagesList.add(4);
        normalFiles.put("d-us-015.pdf", pagesList);
        
        pagesList = new ArrayList<Integer>();
        pagesList.add(6);
        pagesList.add(7);
        normalFiles.put("d-us-016.pdf", pagesList);
        
        pagesList = new ArrayList<Integer>();
        pagesList.add(47);
        normalFiles.put("d-us-003.pdf", pagesList);
        
        pagesList = new ArrayList<Integer>();
        pagesList.add(14);
        pagesList.add(17);
        normalFiles.put("d-us-006.pdf", pagesList);
        
        pagesList = new ArrayList<Integer>();
        pagesList.add(15);
        normalFiles.put("us-08.pdf", pagesList);
        
        pagesList = new ArrayList<Integer>();
        pagesList.add(3);
        normalFiles.put("us-32.pdf", pagesList);
        
        pagesList = new ArrayList<Integer>();
        pagesList.add(6);
        pagesList.add(12);
        pagesList.add(29);
        pagesList.add(47);
        normalFiles.put("us-41.pdf", pagesList);
    }
    
    /**
     * Adds the file (as a key) and the pages (as the value) to the nastyFiles map
     */
    private static void initNastyFiles()
    {
        ArrayList<Integer> pagesList = new ArrayList<Integer>();
        pagesList.add(13);
        pagesList.add(19);
        pagesList.add(102);
        nastyFiles.put("d-us-013.pdf", pagesList);
        
        pagesList = new ArrayList<Integer>();
        pagesList.add(14);
        pagesList.add(17);
        nastyFiles.put("d-us-14.pdf", pagesList);
        
        pagesList = new ArrayList<Integer>();
        pagesList.add(0);
        pagesList.add(4);
        pagesList.add(13);
        nastyFiles.put("d-us-010.pdf", pagesList);
        
        pagesList = new ArrayList<Integer>();
        pagesList.add(1);
        pagesList.add(2);
        pagesList.add(7);
        nastyFiles.put("us-03.pdf", pagesList);
        
        pagesList = new ArrayList<Integer>();
        pagesList.add(0);
        pagesList.add(3);
        pagesList.add(5);
        nastyFiles.put("us-07.pdf", pagesList);
        
        pagesList = new ArrayList<Integer>();
        pagesList.add(9);
        nastyFiles.put("us-33.pdf", pagesList);
    }
}
