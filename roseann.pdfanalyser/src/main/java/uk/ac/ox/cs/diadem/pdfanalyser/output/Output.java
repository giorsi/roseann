/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.output;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;
import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Segment;
import uk.ac.ox.cs.diadem.pdfanalyser.util.SegmentedContents;

public class Output {

	private DocumentBuilderFactory dbfact = null;
	private DocumentBuilder docBuilder = null;
	private Document doc = null;
	private Element rootElement = null;
	private String outputName;
	private Segment[] segmentationType;
	private String outputPath;
	private OutputStream outputStream;

	/**
	 * Initialises the output process by creating the XML tree
	 * 
	 * @param fileName
	 *            The base name of the output file
	 * @param outputPath
	 *            The path where the output will be saved
	 * @param segmentationType
	 *            The array representing the segments that will be written to
	 *            the output file
	 */
	public Output(String fileName, String outputPath,
			OutputStream outputStream, Segment... segmentationType) {
		try {
			this.segmentationType = segmentationType;
			this.outputPath = outputPath;
			this.outputStream = outputStream;

			dbfact = DocumentBuilderFactory.newInstance();
			docBuilder = dbfact.newDocumentBuilder();
			doc = docBuilder.newDocument();
			if (fileName != null) {
				outputName = fileName + "_output";
			} else {
				// we generate a random name if there was no name provided
				// for example if the input is an inputStream and not a file
				Random randomName = new Random();
				outputName = new BigInteger(10, randomName).toString(32)
						+ "_output";
			}
			rootElement = doc.createElement("document");
			rootElement.setAttribute("name", fileName);
			doc.appendChild(rootElement);

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creates a group element and adds it to a parent element
	 * 
	 * @param groupToAdd
	 *            The group that will be added to the output file
	 * @param parentElement
	 *            The element to which the group markup will be appended
	 * @param id
	 *            The identifier of the group
	 */
	private void addGroup(BlockGroup groupToAdd, Element parentElement, int id) {
		DecimalFormat df = new DecimalFormat("0.##");
		String top = df.format(groupToAdd.getTop());
		String bottom = df.format(groupToAdd.getBottom());
		String left = df.format(groupToAdd.getLeft());
		String right = df.format(groupToAdd.getRight());

		Element groupElement = doc.createElement("group");
		groupElement.setAttribute("id", Integer.toString(id));
		groupElement.setAttribute("top", top);
		groupElement.setAttribute("bottom", bottom);
		groupElement.setAttribute("left", left);
		groupElement.setAttribute("right", right);

		Element groupContents = doc.createElement("contents");
		groupContents.setTextContent(groupToAdd.toString());
		groupElement.appendChild(groupContents);

		if (Arrays.asList(segmentationType).contains(Segment.BLOCK)) {
			int i = 0;
			for (Block block : groupToAdd.getBlocks()) {
				addBlock(block, groupElement, i);
				i++;
			}
		} else if (Arrays.asList(segmentationType).contains(Segment.LINE)) {
			int i = 0;
			for (Block block : groupToAdd.getBlocks()) {
				for (Line line : block.getLines()) {
					addLine(line, groupElement, i);
					i++;
				}
			}
		} else if (Arrays.asList(segmentationType).contains(Segment.GLYPH)) {
			for (Block block : groupToAdd.getBlocks()) {
				for (Line line : block.getLines()) {
					for (Glyph glyph : line.getGlyphs()) {
						addGlyph(glyph, groupElement);
					}
				}
			}
		}

		parentElement.appendChild(groupElement);
	}

	/**
	 * Creates a block element and adds it to a parent element
	 * 
	 * @param block
	 *            The block to be added to the output file
	 * @param parentElement
	 *            The element to which this block will be appended
	 * @param id
	 *            The identifier of the block
	 */
	private void addBlock(Block block, Element parentElement, int id) {

		DecimalFormat df = new DecimalFormat("0.##");
		String top = df.format(block.getTop());
		String bottom = df.format(block.getBottom());
		String left = df.format(block.getLeft());
		String right = df.format(block.getRight());
		String contents = block.toString();

		Element blockElement = doc.createElement("block");
		blockElement.setAttribute("id", Integer.toString(id));
		blockElement.setAttribute("top", top);
		blockElement.setAttribute("bottom", bottom);
		blockElement.setAttribute("left", left);
		blockElement.setAttribute("right", right);

		Element blockContents = doc.createElement("contents");
		blockContents.setTextContent(contents);
		blockElement.appendChild(blockContents);

		if (Arrays.asList(segmentationType).contains(Segment.LINE)) {
			int i = 0;
			for (Line line : block.getLines()) {
				addLine(line, blockElement, i);
				i++;
			}
		} else if (Arrays.asList(segmentationType).contains(Segment.GLYPH)) {
			for (Line line : block.getLines()) {
				for (Glyph glyph : line.getGlyphs()) {
					addGlyph(glyph, blockElement);
				}
			}
		}

		parentElement.appendChild(blockElement);
	}

	/**
	 * Creates a glyph element and adds it to a parent element
	 * 
	 * @param glyph
	 *            The glyph that will be added to the output file
	 * @param parentElement
	 *            The element to which the glyph will be appended
	 */
	private void addGlyph(Glyph glyph, Element parentElement) {

		DecimalFormat df = new DecimalFormat("0.##");
		String top = df.format(glyph.getTop());
		String bottom = df.format(glyph.getBottom());
		String left = df.format(glyph.getLeft());
		String right = df.format(glyph.getRight());
		String text = glyph.toString();

		Element glyphElement = doc.createElement("glyph");
		glyphElement.setAttribute("top", top);
		glyphElement.setAttribute("bottom", bottom);
		glyphElement.setAttribute("left", left);
		glyphElement.setAttribute("right", right);

		Element glyphContents = doc.createElement("contents");
		glyphContents.setTextContent(text);
		glyphElement.appendChild(glyphContents);

		parentElement.appendChild(glyphElement);
	}

	/**
	 * Creates a line element and adds it to a parent element
	 * 
	 * @param line
	 *            The line to be added to the output file
	 * @param parentElement
	 *            The element to which the line will be appended
	 * @param id
	 *            The identifier of the line
	 */
	private void addLine(Line line, Element parentElement, int id) {

		DecimalFormat df = new DecimalFormat("0.##");
		String top = df.format(line.getTop());
		String bottom = df.format(line.getBottom());
		String left = df.format(line.getLeft());
		String right = df.format(line.getRight());
		String text = line.toString();

		Element lineElement = doc.createElement("line");
		lineElement.setAttribute("id", Integer.toString(id));
		lineElement.setAttribute("top", top);
		lineElement.setAttribute("bottom", bottom);
		lineElement.setAttribute("left", left);
		lineElement.setAttribute("right", right);

		Element lineContents = doc.createElement("contents");
		lineContents.setTextContent(text);
		lineElement.appendChild(lineContents);

		if (Arrays.asList(segmentationType).contains(Segment.GLYPH)) {
			for (Glyph glyph : line.getGlyphs()) {
				addGlyph(glyph, lineElement);
			}
		}

		parentElement.appendChild(lineElement);
	}

	/**
	 * Create and adds the page element to the root element
	 * 
	 * @param pageNumber
	 *            The number of the page that is added to the output
	 * @param segmentation
	 *            The lists of groups, blocks, lines, glyphs related to the
	 *            current page
	 */
	public void addPage(int pageNumber, SegmentedContents segmentation) {
		Element pageElement = doc.createElement("page");
		pageElement.setAttribute("number", Integer.toString(pageNumber));

		if (Arrays.asList(segmentationType).contains(Segment.BLOCK_GROUP)) {
			List<BlockGroup> groups = segmentation.getGroups();
			int i = 0;
			for (BlockGroup group : groups) {
				addGroup(group, pageElement, i);
				i++;
			}
		} else if (Arrays.asList(segmentationType).contains(Segment.BLOCK)) {
			int i = 0;
			for (Block block : segmentation.getBlocks()) {
				addBlock(block, pageElement, i);
				i++;
			}
		} else if (Arrays.asList(segmentationType).contains(Segment.LINE)) {
			int i = 0;
			for (Line line : segmentation.getLines()) {
				addLine(line, pageElement, i);
				i++;
			}
		} else if (Arrays.asList(segmentationType).contains(Segment.GLYPH)) {
			for (Glyph glyph : segmentation.getGlyphs()) {
				addGlyph(glyph, pageElement);
			}
		}

		rootElement.appendChild(pageElement);
	}

	/**
	 * Ends the output process by transforming the XML tree into a String and by
	 * saving the output file
	 */
	public void endProcess() {
		try {
			TransformerFactory transformerFact = TransformerFactory
					.newInstance();
			Transformer transformer;
			transformer = transformerFact.newTransformer();

			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
					"yes");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(
					"{http://xml.apache.org/xslt}indent-amount", "4");

			// create string from xml tree
			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, result);
			String xmlString = sw.toString();

			if (outputStream == null) {
				// if the output given was a file
				File outputFile = new File(outputPath + outputName + ".xml");
				BufferedWriter writer = new BufferedWriter(new FileWriter(
						outputFile));
				writer.write(xmlString);
				writer.close();
			} else {
				// if we provided an outputstream and not a file
				OutputStreamWriter osw = new OutputStreamWriter(outputStream);
				osw.write(xmlString);
				osw.close();
			}

		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
