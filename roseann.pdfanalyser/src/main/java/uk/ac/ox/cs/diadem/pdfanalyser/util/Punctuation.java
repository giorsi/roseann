/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.util;

import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;

public class Punctuation {

	/**
	 * @param glyph
	 *            The glyph to be analysed
	 * @return True if the given glyph is a punctuation mark, false otherwise
	 */
	public static boolean isPunctuation(final Glyph glyph) {
		boolean isPunctuation = false;
		final String character = glyph.getCharacter();

		if (character.equals(".") || character.equals("?")
				|| character.equals("!") || character.equals(",")
				|| character.equals(":")) {
			isPunctuation = true;
		}

		return isPunctuation;
	}

	/**
	 * @param glyph
	 *            The glyph to be analysed
	 * @return True if the given glyph is a punctuation point, false otherwise
	 */
	public static boolean isPoint(final Glyph glyph) {
		boolean isPoint = false;
		final String character = glyph.getCharacter();

		if (character.equals(".") || character.equals("?")
				|| character.equals("!")) {
			isPoint = true;
		}

		return isPoint;
	}
}
