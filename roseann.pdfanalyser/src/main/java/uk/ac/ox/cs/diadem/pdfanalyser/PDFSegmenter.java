/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDPage;

import uk.ac.ox.cs.diadem.model.Model;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Segment;
import uk.ac.ox.cs.diadem.pdfanalyser.util.SegmentedContents;

/**
 * @author Giorgio Orsi (giorgio dot orsi at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public interface PDFSegmenter {

    /**
     * Segments a PDF document into blocks.
     * 
     * @param inputStream
     * @param blocks
     * @return
     */
    public Model segment(InputStream inputStream, Segment... blocks);

    /**
     * Segments a PDF document into blocks.
     * 
     * @param inputStream
     * @param outputStream
     * @param blocks
     */
    public void segment(InputStream inputStream, OutputStream outputStream, Segment... blocks);

    /**
     * Segments a PDF document into blocks.
     * 
     * @param inputFile
     * @param blocks
     * @return
     */
    public Model segment(File inputFile, Segment... blocks);

    /**
     * Segments a PDF document into blocks.
     * 
     * @param inputFile
     * @param outputFile
     * @param blocks
     */
    public void segment(File inputFile, File outputFile, Segment... blocks);

}
