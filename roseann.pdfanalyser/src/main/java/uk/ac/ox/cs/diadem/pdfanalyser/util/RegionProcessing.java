/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.util;

import java.awt.geom.Rectangle2D;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.PDFTextStripperByArea;

public class RegionProcessing {

    /**
     * @param page
     *            The page to be analysed
     * @param x
     *            The left coordinate of the region to analyse
     * @param y
     *            The top coordinate of the region to analyse
     * @param width
     *            The width of the region to analyse
     * @param height
     *            The height of the region to analyse
     * @return True if the given region is empty, false otherwise
     */
    public static boolean isRegionEmpty(final PDPage page, final double x, final double y, final double width,
            final double height) {
        boolean returnValue = true;
        try {
            final Rectangle2D region = new Rectangle2D.Double(x, y, width, height);
            final String regionName = "region";
            PDFTextStripperByArea stripper;

            stripper = new PDFTextStripperByArea();
            stripper.addRegion(regionName, region);
            stripper.extractRegions(page);

            final String contents = stripper.getTextForRegion(regionName);

            if (contents.matches("\\s+")) {
                returnValue = true;
            } else {
                returnValue = false;
            }
        } catch (final Exception e) {
            // TODO Auto-generated catch block
        	e.printStackTrace();
            returnValue = false;
        }

        return returnValue;
    }
}
