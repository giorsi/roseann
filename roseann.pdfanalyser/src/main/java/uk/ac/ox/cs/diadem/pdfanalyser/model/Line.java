/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.model;

import java.util.ArrayList;
import java.util.List;

import uk.ac.ox.cs.diadem.pdfanalyser.util.Punctuation;

public class Line extends PDFSegment{


	/**
	 * @param glyph
	 *            The glyph that will initialise the new line
	 */
	public Line(final Glyph glyph) {
		fontFamily = new ArrayList<String>();
		fontFamily.add(glyph.getFontName());
		colors = new ArrayList<Integer>();
		colors.add(glyph.getColor());
		glyphs = new ArrayList<Glyph>();
		glyphs.add(glyph);

		top = glyph.getTop();
		bottom = glyph.getBottom();
		left = glyph.getLeft();
		right = glyph.getRight();
		fontSize = glyph.getFontSize();
	}

	private List<Glyph> glyphs;
	private final List<String> fontFamily;
	private final List<Integer> colors;

	/**
	 * @param glyph
	 *            The glyph to be added to the current line
	 */
	public void addGlyph(final Glyph glyph) {
		boolean fontFound = false;

		glyphs.add(glyph);
		// we update the different coordinates of the bounding box of the
		// current line
		if (glyph.getTop() < top) {
			// if the top position of the added glyph is higher than the current
			// top position, we extend the line
			top = glyph.getTop();
		}

		if (glyph.getBottom() > bottom) {
			bottom = glyph.getBottom();
		}

		if (glyph.getLeft() < left) {
			left = glyph.getLeft();
		}

		if (glyph.getRight() > right) {
			right = glyph.getRight();
		}

		if (glyph.getFontSize() != fontSize) {
			fontSize = getAverageFontSize();
		}

		for (int i = 0; i < fontFamily.size() && !fontFound; i++) {
			final String font = fontFamily.get(i);
			if (glyph.getFontName().equals(font)) {
				fontFound = true;
			}
		}

		if (!fontFound) {
			fontFamily.add(glyph.getFontName());
		}

		boolean colorFound = false;
		for (int i = 0; i < colors.size() && !colorFound; i++) {
			final int color = colors.get(i).intValue();
			if (glyph.getColor() == color) {
				colorFound = true;
			}
		}

		if (!colorFound) {
			colors.add(glyph.getColor());
		}
	}

	public boolean enoughPunctuationEvidence() {
		int count = 0;
		
		if (!getGlyphs().isEmpty()) {
			for (int i = 1; i < getGlyphs().size()-1; i++) {
				Glyph curGlyph = getGlyphs().get(i);
				if (Punctuation.isPunctuation(curGlyph)) {
					count++;
				}
			}
		}
		
		return count>3;
	}
	
	public double getHorizontalMaxSpaceTolerance(final double averageLineHorizontalSpacing){
		return 2 * averageLineHorizontalSpacing;
	}
	
	/**
	 * get the horizontal space between the first and last glyph in a line
	 */
	private double getMaxGlyphSpace(){
		if(getGlyphs().size() == 0){
			return 0;
		}
		
		double minLeft = Double.MAX_VALUE;
		double maxLeft = Double.MIN_VALUE;
		for(final Glyph glyph : getGlyphs()){
			if(glyph.getLeft() < minLeft){
				minLeft = glyph.getLeft();
			}
			if(glyph.getLeft() > maxLeft){
				maxLeft = glyph.getLeft();
			}
		}
		return maxLeft - minLeft;
	}

	public double getAverageHorizontalSpacing(final boolean afterPunctuation,  final int HORIZONTAL_OUTLIER_PARAM) {
		double spacing = 0.;
		int totalBlanks = 0;
		double average = 0.;
		double outlierBoundary = getMaxGlyphSpace()/HORIZONTAL_OUTLIER_PARAM;

		if (!getGlyphs().isEmpty()) {
			Glyph precedingGlyph = getGlyphs().get(0);

			for (int i = 1; i < getGlyphs().size(); i++) {
				while (i < getGlyphs().size()
						&& getGlyphs().get(i).getCharacter().equals(" ")) {
					i++;
				}
				
				if (i < getGlyphs().size()) {
					Glyph currentGlyph = getGlyphs().get(i);
					double currentSpacing = currentGlyph.getLeft() - precedingGlyph.getRight();
					if (currentSpacing > SPACING_TOLERANCE && currentSpacing < outlierBoundary) {
						if (afterPunctuation) {
							if (Punctuation.isPunctuation(precedingGlyph)) {
								spacing += currentSpacing;
								totalBlanks++;
							}
						} else {
							spacing += currentSpacing;
							totalBlanks++;
						}
					}
					precedingGlyph = currentGlyph;
				}
			}

			if (totalBlanks > 0) {
				average = spacing / (double) totalBlanks;
			} else {
				// there was only one word on the line and therefore no space
				average = 0;
			}
		}

		return average;
	}

	public double getHorizontalVariance(double mean, final int HORIZONTAL_OUTLIER_PARAM) {
		double variance = 0;
		double squaredDifferencesSum = 0;
		int totalBlanks = 0;
		double outlierBoundary = getMaxGlyphSpace()/HORIZONTAL_OUTLIER_PARAM;

		if (!getGlyphs().isEmpty()) {
			Glyph precedingGlyph = getGlyphs().get(0);

			for (int i = 1; i < getGlyphs().size(); i++) {
				while (i < getGlyphs().size()
						&& getGlyphs().get(i).getCharacter().equals(" ")) {
					i++;
				}

				if (i < getGlyphs().size()) {
					Glyph currentGlyph = getGlyphs().get(i);
					double currentSpace = currentGlyph.getLeft() - precedingGlyph.getRight();
					if ( currentSpace > SPACING_TOLERANCE && currentSpace < outlierBoundary) {
						squaredDifferencesSum += Math.pow(currentSpace - mean,
								2);
						totalBlanks++;
					}
					precedingGlyph = currentGlyph;
				}
			}

			if (totalBlanks > 0) {
				variance = squaredDifferencesSum / (double) totalBlanks;
			} else {
				// there was only one word on the line and therefore no space
				variance = 0;
			}
		}
		return variance;
	}

	/**
	 * @return
	 */
	public List<String> getFontFamily() {
		return fontFamily;
	}

	/**
	 * @return
	 */
	public List<Integer> getColors() {
		return colors;
	}

	/**
	 * @return
	 */
	public List<Glyph> getGlyphs() {
		return glyphs;
	}

	/**
	 * @param glyphs
	 */
	public void setGlyphs(final List<Glyph> glyphs) {
		this.glyphs = glyphs;
	}

	/**
	 * @return
	 */
	private double getAverageFontSize() {
		double averageFontSize = 0;
		;
		double totalSize = 0;

		for (final Glyph glyph : glyphs) {
			totalSize += glyph.getFontSize();
		}

		averageFontSize = totalSize / glyphs.size();

		return averageFontSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String retValue = "";

		if (!glyphs.isEmpty()) {

			Glyph precedingGlyph = glyphs.get(0);
			retValue += precedingGlyph.getCharacter();

			for (int i = 1; i < glyphs.size(); i++) {

				final Glyph g = glyphs.get(i);
				if (g.getLeft() - precedingGlyph.getRight() > SPACING_TOLERANCE) {
					retValue += " ";
				}

				retValue += g.getCharacter();
				precedingGlyph = g;
			}
		}
		return retValue;
	}

	// This is the default value that evaluates if two consecutive glyphs
	// are in the same word or not
	public static final double SPACING_TOLERANCE = 0.5;
	
	public double getVerticalSpace(){
		return (this.getBottom() - this.getTop());
	}
}
