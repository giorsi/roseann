/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.model;

import org.apache.pdfbox.pdmodel.font.PDFont;

public class Glyph extends PDFSegment{

	/**
	 * @param x0
	 *            The left coordinate of the glyph
	 * @param x1
	 *            The right coordinate of the glyph
	 * @param y0
	 *            The top coordinate of the glyph
	 * @param y1
	 *            The bottom coordinate of the glyph
	 * @param c
	 *            The character representing the glyph
	 * @param size
	 *            The font size of the glyph
	 * @param font
	 *            The font of the glyph
	 * @param color
	 * 			  The rgb color value of glyph
	 */
	public Glyph(final double x0, final double x1, final double y0, final double y1, final String c, final double size,
			final PDFont font, final int color) {
		left = x0;
		right = x1;
		top = y0;
		bottom = y1;
		// we do this test do avoid parsing exceptions later on (due to invalid
		// unicode characters)
		if (c.charAt(0) >= 32) {
			character = c;
		} else {
			character = "#";
		}
		fontSize = size;
		final String completeFontName = font.getBaseFont();
		this.font = completeFontName.substring(completeFontName.lastIndexOf("+") + 1);
		this.color = color;
		// this.fontWeight = font.getFontDescriptor().getFontWeight();
	}

	private String character;
	private String font;
	
	private int color;
	private long start;
	private long end;
	// private float fontWeight;

	/**
	 * @return
	 */
	public String getCharacter() {
		return character;
	}

	/**
	 * @return
	 */
	public String getFontName() {
		return font;
	}

	/**
	 * @return
	 */
	public int getColor(){
		return color;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return character;
	}

	/**
	 * Set the start-offset of the character inside the pdf page
	 * @param start
	 */
	public void setStart(long start){
		this.start=start;
	}

	/**
	 * Set the end-offset of the character inside the pdf page
	 * @param start
	 */
	public void setEnd(long end){
		this.end=end;
	}

	/**
	 * Get the start-offset of the character inside the pdf page
	 */
	public long getStart(){
		return start;
	}

	/**
	 * Get the end-offset of the character inside the pdf page
	 */
	public long getEnd(){
		return end;
	}


}
