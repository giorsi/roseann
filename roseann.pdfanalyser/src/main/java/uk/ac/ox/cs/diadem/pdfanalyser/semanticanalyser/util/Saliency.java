package uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;
import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.model.HieBlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.util.FontStyle;

public class Saliency {

	public static int lineFontSaliency(List<Line> linesToEvaluate,
			List<Line> lines) {
		Set<Double> fontSizes = new HashSet<Double>();
		Set<String> fontFamilies = new HashSet<String>();
		Set<Boolean> fontCapitals = new HashSet<Boolean>();
		Set<Boolean> fontBolds = new HashSet<Boolean>();
		Set<Boolean> fontItalics = new HashSet<Boolean>();
		extractLineFontInfo(linesToEvaluate, fontSizes, fontFamilies,
				fontCapitals, fontBolds, fontItalics);

		Set<Double> otherFontSizes = new HashSet<Double>();
		Set<String> otherFontFamilies = new HashSet<String>();
		Set<Boolean> otherFontCapitals = new HashSet<Boolean>();
		Set<Boolean> otherFontBolds = new HashSet<Boolean>();
		Set<Boolean> otherFontItalics = new HashSet<Boolean>();
		extractLineFontInfo(lines, otherFontSizes, otherFontFamilies,
				otherFontCapitals, otherFontBolds, otherFontItalics);

		fontSizes.removeAll(otherFontSizes);
		fontFamilies.removeAll(otherFontFamilies);
		fontCapitals.removeAll(otherFontCapitals);
		fontBolds.removeAll(otherFontBolds);
		fontItalics.removeAll(otherFontItalics);

		int diffSizeCnt = fontSizes.size();
		int diffFamiliesCnt = fontFamilies.size();
		int diffCapitalsCnt = fontCapitals.size();
		int diffBoldsCnt = fontBolds.size();
		int diffItalicsCnt = fontItalics.size();

		int saliency = diffSizeCnt + diffFamiliesCnt + diffCapitalsCnt
				+ diffBoldsCnt + diffItalicsCnt;

		return saliency;
	}

	public static int blockFontSaliency(List<BlockGroup> groupsToEvaluate,
			List<Block> blocks) {
		Set<Double> fontSizes = new HashSet<Double>();
		Set<String> fontFamilies = new HashSet<String>();
		Set<Boolean> fontCapitals = new HashSet<Boolean>();
		Set<Boolean> fontBolds = new HashSet<Boolean>();
		Set<Boolean> fontItalics = new HashSet<Boolean>();
		extractBlockGroupFontInfo(groupsToEvaluate, fontSizes, fontFamilies,
				fontCapitals, fontBolds, fontItalics);

		Set<Double> otherFontSizes = new HashSet<Double>();
		Set<String> otherFontFamilies = new HashSet<String>();
		Set<Boolean> otherFontCapitals = new HashSet<Boolean>();
		Set<Boolean> otherFontBolds = new HashSet<Boolean>();
		Set<Boolean> otherFontItalics = new HashSet<Boolean>();
		extractBlockFontInfo(blocks, otherFontSizes, otherFontFamilies,
				otherFontCapitals, otherFontBolds, otherFontItalics);

		fontSizes.removeAll(otherFontSizes);
		fontFamilies.removeAll(otherFontFamilies);
		fontCapitals.removeAll(otherFontCapitals);
		fontBolds.removeAll(otherFontBolds);
		fontItalics.removeAll(otherFontItalics);

		int diffSizeCnt = fontSizes.size();
		int diffFamiliesCnt = fontFamilies.size();
		int diffCapitalsCnt = fontCapitals.size();
		int diffBoldsCnt = fontBolds.size();
		int diffItalicsCnt = fontItalics.size();

		int saliency = diffSizeCnt + diffFamiliesCnt + diffCapitalsCnt
				+ diffBoldsCnt + diffItalicsCnt;

		return saliency;

	}

	public static int groupFontSaliency(List<BlockGroup> groupsToEvaluate,
			List<BlockGroup> otherGroups) {
		Set<Double> fontSizes = new HashSet<Double>();
		Set<String> fontFamilies = new HashSet<String>();
		Set<Boolean> fontCapitals = new HashSet<Boolean>();
		Set<Boolean> fontBolds = new HashSet<Boolean>();
		Set<Boolean> fontItalics = new HashSet<Boolean>();
		extractBlockGroupFontInfo(groupsToEvaluate, fontSizes, fontFamilies,
				fontCapitals, fontBolds, fontItalics);

		Set<Double> otherFontSizes = new HashSet<Double>();
		Set<String> otherFontFamilies = new HashSet<String>();
		Set<Boolean> otherFontCapitals = new HashSet<Boolean>();
		Set<Boolean> otherFontBolds = new HashSet<Boolean>();
		Set<Boolean> otherFontItalics = new HashSet<Boolean>();
		extractBlockGroupFontInfo(otherGroups, otherFontSizes,
				otherFontFamilies, otherFontCapitals, otherFontBolds,
				otherFontItalics);

		fontSizes.removeAll(otherFontSizes);
		fontFamilies.removeAll(otherFontFamilies);
		fontCapitals.removeAll(otherFontCapitals);
		fontBolds.removeAll(otherFontBolds);
		fontItalics.removeAll(otherFontItalics);

		int diffSizeCnt = fontSizes.size();
		int diffFamiliesCnt = fontFamilies.size();
		int diffCapitalsCnt = fontCapitals.size();
		int diffBoldsCnt = fontBolds.size();
		int diffItalicsCnt = fontItalics.size();

		int saliency = diffSizeCnt + diffFamiliesCnt + diffCapitalsCnt
				+ diffBoldsCnt + diffItalicsCnt;

		return saliency;
	}

	public static int HierarchicalGroupFontSaliency(
			List<HieBlockGroup> groupsToEvaluate,
			List<HieBlockGroup> otherGroups) {
		Set<Double> fontSizes = new HashSet<Double>();
		Set<String> fontFamilies = new HashSet<String>();
		Set<Boolean> fontCapitals = new HashSet<Boolean>();
		Set<Boolean> fontBolds = new HashSet<Boolean>();
		Set<Boolean> fontItalics = new HashSet<Boolean>();
		extractHierarchicalBlockGroupFontInfo(groupsToEvaluate, fontSizes,
				fontFamilies, fontCapitals, fontBolds, fontItalics);

		Set<Double> otherFontSizes = new HashSet<Double>();
		Set<String> otherFontFamilies = new HashSet<String>();
		Set<Boolean> otherFontCapitals = new HashSet<Boolean>();
		Set<Boolean> otherFontBolds = new HashSet<Boolean>();
		Set<Boolean> otherFontItalics = new HashSet<Boolean>();
		extractHierarchicalBlockGroupFontInfo(otherGroups, otherFontSizes,
				otherFontFamilies, otherFontCapitals, otherFontBolds,
				otherFontItalics);

		fontSizes.removeAll(otherFontSizes);
		fontFamilies.removeAll(otherFontFamilies);
		fontCapitals.removeAll(otherFontCapitals);
		fontBolds.removeAll(otherFontBolds);
		fontItalics.removeAll(otherFontItalics);

		int diffSizeCnt = fontSizes.size();
		int diffFamiliesCnt = fontFamilies.size();
		int diffBoldsCnt = fontBolds.size();
		int diffItalicsCnt = fontItalics.size();

		int saliency = diffSizeCnt + diffFamiliesCnt + diffBoldsCnt
				+ diffItalicsCnt;

		return saliency;
	}

	private static void extractLineFontInfo(List<Line> lines,
			Set<Double> fontSizes, Set<String> fontFamilies,
			Set<Boolean> fontCapitals, Set<Boolean> fontBolds,
			Set<Boolean> fontItalics) {
		for (Line line : lines) {
			List<Glyph> firstGlyphs = line.getGlyphs().size() > 5 ? line
					.getGlyphs().subList(0, 5) : line.getGlyphs();
			for (Glyph glyph : firstGlyphs) {
				fontSizes.add(glyph.getFontSize());
				fontFamilies.add(glyph.getFontName());
				FontStyle style = FontStyle.fontStylesMap.get(glyph
						.getFontName());
				if (style != null) {
					fontBolds.add(style.isBold());
					fontItalics.add(style.isItalic());
				}

				fontCapitals.add(Character.isUpperCase(glyph.getCharacter()
						.charAt(0)));
			}
		}
	}

	private static void extractBlockFontInfo(List<Block> blocks,
			Set<Double> fontSizes, Set<String> fontFamilies,
			Set<Boolean> fontCapitals, Set<Boolean> fontBolds,
			Set<Boolean> fontItalics) {
		for (Block block : blocks) {
			List<Line> lines = block.getLines();
			for (Line line : lines) {
				List<Glyph> firstGlyphs = line.getGlyphs().size() > 5 ? line
						.getGlyphs().subList(0, 5) : line.getGlyphs();
				for (Glyph glyph : firstGlyphs) {
					fontSizes.add(glyph.getFontSize());
					fontFamilies.add(glyph.getFontName());
					FontStyle style = FontStyle.fontStylesMap.get(glyph
							.getFontName());
					if (style != null) {
						fontBolds.add(style.isBold());
						fontItalics.add(style.isItalic());
					}

					fontCapitals.add(Character.isUpperCase(glyph.getCharacter()
							.charAt(0)));
				}

			}
		}
	}

	private static void extractBlockGroupFontInfo(List<BlockGroup> groups,
			Set<Double> fontSizes, Set<String> fontFamilies,
			Set<Boolean> fontCapitals, Set<Boolean> fontBolds,
			Set<Boolean> fontItalics) {
		for (BlockGroup group : groups) {
			List<Line> lines = group.getLines();
			for (Line line : lines) {
				List<Glyph> firstGlyphs = line.getGlyphs().size() > 5 ? line
						.getGlyphs().subList(0, 5) : line.getGlyphs();
				for (Glyph glyph : firstGlyphs) {
					fontSizes.add(glyph.getFontSize());
					fontFamilies.add(glyph.getFontName());
					FontStyle style = FontStyle.fontStylesMap.get(glyph
							.getFontName());
					if (style != null) {
						fontBolds.add(style.isBold());
						fontItalics.add(style.isItalic());
					}

					fontCapitals.add(Character.isUpperCase(glyph.getCharacter()
							.charAt(0)));
				}

			}
		}
	}

	private static void extractHierarchicalBlockGroupFontInfo(
			List<HieBlockGroup> groups, Set<Double> fontSizes,
			Set<String> fontFamilies, Set<Boolean> fontCapitals,
			Set<Boolean> fontBolds, Set<Boolean> fontItalics) {
		for (BlockGroup group : groups) {
			List<Line> lines = group.getLines();
			for (Line line : lines) {
				List<Glyph> firstGlyphs = line.getGlyphs().size() > 5 ? line
						.getGlyphs().subList(0, 5) : line.getGlyphs();
				for (Glyph glyph : firstGlyphs) {
					fontSizes.add(glyph.getFontSize());
					fontFamilies.add(glyph.getFontName());
					FontStyle style = FontStyle.fontStylesMap.get(glyph
							.getFontName());
					if (style != null) {
						fontBolds.add(style.isBold());
						fontItalics.add(style.isItalic());
					}

					fontCapitals.add(Character.isUpperCase(glyph.getCharacter()
							.charAt(0)));
				}

			}
		}
	}

}
