package uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.semanticstructure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.pdfbox.pdmodel.PDPage;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.model.HieBlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.util.Saliency;
import uk.ac.ox.cs.diadem.pdfanalyser.util.Bullet;
import uk.ac.ox.cs.diadem.pdfanalyser.util.comparator.BlockGroupCoordinateComparator;
import uk.ac.ox.cs.diadem.pdfanalyser.util.comparator.HieBlockGroupCoordinateComparator;

public class BlockGroupHierarchyAnalyser {
	private final double SALIENCY_THRESHOLD = ConfigurationFacility
			.getConfiguration().getDouble(
					"pdfanalyser.annotationsegmenter.saliencyThreshold");

	private Map<PDPage, List<HieBlockGroup>> segmentation;
	private PDPage currentPage;
	private List<Glyph> currentGlyphs;
	private List<BlockGroup> currentGroups;
	private int averageFontSize;

	public BlockGroupHierarchyAnalyser() {
		setSegmentation(new HashMap<PDPage, List<HieBlockGroup>>());
		currentPage = null;
	}

	public void constructPageHierarchy(PDPage page,
			List<BlockGroup> annoTitles, List<BlockGroup> groups,
			List<Glyph> glyphs) {
		currentPage = page;
		currentGlyphs = glyphs;
		currentGroups = groups;
		calculateAverageFontSize();
		List<HieBlockGroup> hierarchicalGroups = convert(groups);
		constructListHierarchy(hierarchicalGroups);
		constructTitleHierarchy(annoTitles, hierarchicalGroups);

		segmentation.put(currentPage, hierarchicalGroups);
	}

	private List<HieBlockGroup> convert(List<BlockGroup> groups) {
		List<HieBlockGroup> hierarhicalBlockGroups = new ArrayList<HieBlockGroup>();
		for (BlockGroup group : groups) {
			HieBlockGroup hg = new HieBlockGroup(group);
			hierarhicalBlockGroups.add(hg);
		}
		return hierarhicalBlockGroups;
	}

	private void constructListHierarchy(List<HieBlockGroup> groups) {

		// break bullet list
		// we only break those strong bullet marks and small weak ones
		// because large weak ones such as a) 23. are alaways used
		// as section titles
		for (int i = 0; i < groups.size(); i++) {
			HieBlockGroup group = groups.get(i);
			int strongBulletLength = Bullet.getStrongBulletLength(group
					.toString());
			int bulletLength = Bullet.getBulletLength(group.toString());
			Line bulletLine = group.getLines().get(0);
			// there is a chance that the block doesn't start with a bullet mark
			// but it contains a bullet list. We also need to split it.
			if (strongBulletLength == 0) {
				for (Line line : group.getLines()) {
					strongBulletLength = Bullet.getStrongBulletLength(line
							.toString());
					if (strongBulletLength != 0) {
						bulletLine = line;
						break;
					}
				}
			}
			if (bulletLength == 0) {
				for (Line line : group.getLines()) {
					bulletLength = Bullet.getBulletLength(line.toString());
					if (bulletLength != 0) {
						bulletLine = line;
						break;
					}
				}
			}

			if (strongBulletLength == 0 && bulletLength == 0) {
				continue;
			}
			if (strongBulletLength == 0 && bulletLength != 0
					&& bulletLine.getFontSize() > averageFontSize) {
				continue;
			}

			bulletLength = strongBulletLength > 0 ? strongBulletLength
					: bulletLength;
			groups.remove(i);
			// break down the current group
			List<Line> lines = group.getLines();
			HieBlockGroup newGroup = new HieBlockGroup();
			double precedingVspace = 0;
			double currentVspace = 0;
			boolean validSpace = false;
			for (int j = 0; j < lines.size(); j++) {
				Line line = lines.get(j);
				currentVspace = j == 0 ? 0 : line.getTop()
						- lines.get(j - 1).getTop();
				validSpace = (currentVspace == 0 || precedingVspace == 0) ? true
						: currentVspace - precedingVspace < 5 * getSpaceTolerance();

				// either it is a new bullet item
				// or there is an abrupt change of vertical space
				if (!validSpace
						|| (line.toString().trim().length() > bulletLength && Bullet
								.isBullet(line.toString().trim()
										.substring(0, bulletLength)))) {
					if (newGroup.getLines().size() != 0) {
						groups.add(i, newGroup);
						i++;
					}
					newGroup = new HieBlockGroup();
				}
				newGroup.addLine(line);
				precedingVspace = currentVspace;
			}
			// insert last newGroup
			groups.add(i, newGroup);
		}

		// combine each sub section of a bullet item into one block group
		for (int i = 0; i < groups.size() - 1; i++) {
			HieBlockGroup group = groups.get(i);
			HieBlockGroup nextGroup = groups.get(i + 1);
			int bulletLength = Bullet.getBulletLength(group.toString());
			if (bulletLength == 0) {
				continue;
			}
			int nextBulletLength = Bullet.getBulletLength(nextGroup.toString());
			if (nextBulletLength != 0) {
				continue;
			}
			String bulletMark = group.toString().trim()
					.substring(0, bulletLength);
			List<Glyph> firstLineGlyphs = null;
			for (Line line : group.getLines()) {
				if (line.toString().trim().startsWith(bulletMark)) {
					firstLineGlyphs = line.getGlyphs();
					break;
				}
			}

			// removeLeadingEmptyGlyphs(firstLineGlyphs);
			double bulletAlign = removeLeadingEmptyGlyphs(firstLineGlyphs).get(
					bulletLength).getLeft();
			if (Math.abs(nextGroup.getLeft() - bulletAlign) < 5
					&& nextGroup.getTop() > group.getBottom()
					&& nextGroup.getLeft() < group.getRight()) {
				if (group.getSubGroups().size() == 0) {
					HieBlockGroup newGroup = new HieBlockGroup();
					newGroup.addSubGroup(group);
					groups.set(i, newGroup);
					group = newGroup;

				}
				group.addSubGroup(nextGroup);
				groups.remove(nextGroup);
				i--;
			}
		}

		// group strong bullets and small weak ones into a larger group
		HieBlockGroup list = null;
		String previousBulletMark = null;
		HieBlockGroup previousGroup = null;
		Stack<HieBlockGroup> stack = new Stack<HieBlockGroup>();
		for (int i = 0; i < groups.size() - 1; i++) {
			HieBlockGroup group = groups.get(i);

			int strongBulletLength = Bullet.getStrongBulletLength(group
					.toString());
			int bulletLength = Bullet.getBulletLength(group.toString());

			// if there is neither bullet marks at all or it is big weak bullet
			// item(section title)
			// we ignore them
			if (strongBulletLength == 0 && bulletLength == 0) {
				popStack(stack);
				previousBulletMark = null;
				previousGroup = null;
				continue;
			}

			bulletLength = strongBulletLength > 0 ? strongBulletLength
					: bulletLength;
			String bulletMark = group.toString().trim()
					.substring(0, bulletLength);

			boolean additionPossible = false;
			if (previousBulletMark == null && previousGroup == null) {
				additionPossible = true;
			} else {
				if (Bullet.sameType(bulletMark, previousBulletMark)
						&& leftAlign(previousGroup, group)) {
					additionPossible = true;
				}
			}

			groups.remove(group);
			if (additionPossible) {
				list = peekStack(stack);
				// both the previous group and current group are bullet items
				// and they belong to the same list
				if (list == null) {
					list = new HieBlockGroup();
					groups.add(i, list);
					stack.push(list);
				} else {
					i--;
				}
				list.addSubGroup(group);
			} else {
				// both the previous group and current group are bullet items
				// but they belong to different lists
				if (group.getLeft() >= previousGroup.getLeft()) {
					// group is subgroup of the previous group. we create a new
					// list
					// e.g. 
					// a) ******
					// 	1) ***
					//  2) ***
					list = new HieBlockGroup();
					previousGroup.addSubGroup(list);
					stack.push(list);
					list.addSubGroup(group);
					i--;
				} else {
					// previous group is subgroup of the current group. we
					// finish the current list by popping it from the stack
					// e.g. 
					//	1) ***
					// 	2) ***
					// b) ******
					popStack(stack);
					list = peekStack(stack);
					if (list == null) {
						list = new HieBlockGroup();
						groups.add(i, list);
						stack.push(list);
					} else {
						i--;
					}
					list.addSubGroup(group);
				}
			}
			previousGroup = group;
			previousBulletMark = bulletMark;
		}

		// remove those block groups that only has one subgroup and the subgroup
		// is itself
		for (HieBlockGroup group : groups) {
			if (group.getSubGroups().size() == 1) {
				String groupText = group.toString();
				String subGroupText = group.getSubGroups().get(0).toString();
				if (groupText.equals(subGroupText)) {
					group.clearSubGroups();
				}
			}
		}
	}

	private void constructTitleHierarchy(List<BlockGroup> annoTitles,
			List<HieBlockGroup> groups) {
		List<HieBlockGroup> titles = null;
		if (annoTitles != null && annoTitles.size() > 0) {
			titles = breakDownTitleParagraphCombo(annoTitles, groups);
			constructHierary(titles, groups);
		}
		titles = titlesWithRepeatedStyle(groups);
		constructHierary(titles, groups);

	}

	private List<HieBlockGroup> breakDownTitleParagraphCombo(
			List<BlockGroup> annoTitles, List<HieBlockGroup> groups) {

		List<HieBlockGroup> titles = new ArrayList<HieBlockGroup>();
		for (BlockGroup title : annoTitles) {
			for (int i = 0; i < groups.size(); i++) {
				HieBlockGroup group = groups.get(i);
				if (group.contains(title)
						&& group.toString().trim()
								.startsWith(title.toString().trim())) {
					HieBlockGroup hieTitle = new HieBlockGroup();
					int j = 0;
					for (; j < group.getLines().size(); j++) {
						Line line = group.getLines().get(j);
						if (group.toString().trim()
								.startsWith(line.toString().trim())) {
							hieTitle.addLine(group.getLines().get(j));
						} else {
							break;
						}
					}

					HieBlockGroup hieParagraph = new HieBlockGroup();
					for (; j < group.getLines().size(); j++) {
						hieParagraph.addLine(group.getLines().get(j));
					}

					group = new HieBlockGroup();
					group.addSubGroup(hieTitle);
					group.addSubGroup(hieParagraph);
					groups.set(i, group);

					titles.add(hieTitle);
				}
			}
		}
		HieBlockGroupCoordinateComparator blockGroupComparator = new HieBlockGroupCoordinateComparator(
				groups);
		Collections.sort(titles, blockGroupComparator);
		return titles;
	}

	private void constructHierary(List<HieBlockGroup> titles,
			List<HieBlockGroup> groups) {
		List<HieBlockGroup> aggregatedGroups = new ArrayList<HieBlockGroup>();
		List<HieBlockGroup> outerGroups = new ArrayList<HieBlockGroup>();
		for (int i = 0; i < titles.size(); i++) {
			HieBlockGroup title = titles.get(i);
			HieBlockGroup outerGroup = title.getParentGroup();
			// if not last title in a column
			double top = title.getBottom();
			double bottom = 0;
			boolean lastTitleOfColumn = false;
			if (i != titles.size() - 1
					&& (leftAlign(title, titles.get(i + 1)) || rightAlign(
							title, titles.get(i + 1)))) {
				bottom = titles.get(i + 1).getTop();

			} else {// current title is the last one in current column
				bottom = currentPage.getMediaBox().getHeight();
				lastTitleOfColumn = true;
			}

			for (int j = 0; j < groups.size(); j++) {
				HieBlockGroup group = groups.get(j);
				if (between(group, top, bottom)) {
					boolean additionPossible = false;
					if (title.getLeft() - group.getLeft() > 2) {
						additionPossible = rightAlign(title, group);
					} else if ((group.getLeft() > title.getLeft() || title
							.getLeft() - group.getLeft() < 2)
							&& (group.getLeft() < title.getRight() || group
									.getLeft() - title.getRight() < 2)) {// left
																			// indent
																			// but
																			// still
																			// overlap
						additionPossible = true;
					} else {// if no overlap, there should be no unexpected
							// area between them
						double x0 = title.getRight();
						double x1 = group.getRight();
						double y0 = title.getTop();
						double y1 = group.getTop();
						additionPossible = !containsAnyGlyph(x0, x1, y0, y1);
					}

					// there should be no groups that are not sub groups of the
					// current title but
					// lying in between of the current group and the title
					if (additionPossible) {
						if (groupInBetween(group, title, outerGroup, groups)) {
							additionPossible = false;
						}
					}

					// the current block should have the same saliency with the
					// previous group
					// sub groups. this is to handle footer or a different
					// section context
					if (additionPossible) {
						if (lastTitleOfColumn && isFooter(group)) {
							if (outerGroup != null) {
								HieBlockGroup previousGroup = outerGroup
										.getSubGroups().get(
												outerGroup.getSubGroups()
														.size() - 1);
								List<HieBlockGroup> groupsToEvaluate = new ArrayList<HieBlockGroup>();
								List<HieBlockGroup> otherGroups = new ArrayList<HieBlockGroup>();
								groupsToEvaluate.add(group);
								otherGroups.add(previousGroup);
								additionPossible = Saliency
										.HierarchicalGroupFontSaliency(
												groupsToEvaluate, otherGroups) == 0;
							}
						}
					}

					if (additionPossible) {
						if (outerGroup == null) {
							outerGroup = new HieBlockGroup();
							outerGroups.add(outerGroup);

							if (!aggregatedGroups.contains(title)) {
								aggregatedGroups.add(title);
							}
							outerGroup.addSubGroup(title);
						}
						outerGroup.addSubGroup(group);
						aggregatedGroups.add(group);
					}
				}
			}
		}

		for (HieBlockGroup aggregatedGroup : aggregatedGroups) {
			groups.remove(aggregatedGroup);
		}
		for (HieBlockGroup outerGroup : outerGroups) {
			groups.add(outerGroup);
		}

		// enforce reading order
		BlockGroupCoordinateComparator blockGroupComparator = new BlockGroupCoordinateComparator(
				currentGroups);
		Collections.sort(groups, blockGroupComparator);

	}

	private boolean containsAnyGlyph(double x0, double x1, double y0, double y1) {
		for (Glyph glyph : currentGlyphs) {
			if (glyph.getCharacter().trim().equals("")) {
				continue;
			}
			boolean leftTop = inside(glyph.getLeft(), glyph.getTop(), x0, x1,
					y0, y1);
			boolean leftBottom = inside(glyph.getLeft(), glyph.getBottom(), x0,
					x1, y0, y1);
			boolean rightTop = inside(glyph.getRight(), glyph.getTop(), x0, x1,
					y0, y1);
			boolean rightBottom = inside(glyph.getRight(), glyph.getBottom(),
					x0, x1, y0, y1);
			boolean overlap = leftTop || leftBottom || rightTop || rightBottom;
			if (overlap) {
				return true;
			}
		}
		return false;
	}

	private boolean inside(double x, double y, double x0, double x1, double y0,
			double y1) {
		return x > x0 && x < x1 && y > y0 && y < y1;
	}

	private boolean between(HieBlockGroup group, double top, double bottom) {
		return (group.getTop() > top || top - group.getTop() < 1)
				&& (group.getBottom() < bottom || group.getBottom() - bottom < 1);
	}

	private boolean leftAlign(HieBlockGroup group1, HieBlockGroup group2) {
		if (Math.abs(group1.getLeft() - group2.getLeft()) < 2) {
			return true;
		}
		return false;
	}

	private boolean rightAlign(HieBlockGroup group1, HieBlockGroup group2) {
		if (Math.abs(group1.getRight() - group2.getRight()) < 2) {
			return true;
		}
		return false;
	}

	private List<HieBlockGroup> titlesWithRepeatedStyle(
			List<HieBlockGroup> groups) {
		List<HieBlockGroup> titles = new ArrayList<HieBlockGroup>();
		List<HieBlockGroup> repeatedStyleTitles = new ArrayList<HieBlockGroup>();
		for (int i = 0; i < groups.size() - 1; i++) {
			HieBlockGroup group = groups.get(i);
			HieBlockGroup next = groups.get(i + 1);
			List<HieBlockGroup> groupsToEvaluate = new ArrayList<HieBlockGroup>();
			List<HieBlockGroup> otherGroups = new ArrayList<HieBlockGroup>();
			groupsToEvaluate.add(group);
			otherGroups.add(next);
			if (nonEmptyLineNum(group.getLines()) == 1
					&& next.getLines().size() >= 1
					&& next.toString().length() > group.toString().length()
					&& Saliency.HierarchicalGroupFontSaliency(groupsToEvaluate,
							otherGroups) >= SALIENCY_THRESHOLD
					&& group.getLines().get(0).getFontSize() >= next.getLines()
							.get(0).getFontSize()) {
				titles.add(group);
			}
		}

		for (HieBlockGroup title : titles) {
			for (HieBlockGroup compareTitle : titles) {
				List<HieBlockGroup> group1 = new ArrayList<HieBlockGroup>();
				List<HieBlockGroup> group2 = new ArrayList<HieBlockGroup>();
				group1.add(title);
				group2.add(compareTitle);
				if (title != compareTitle) {
					if (Saliency.HierarchicalGroupFontSaliency(group1, group2) == 0
							&& Saliency.HierarchicalGroupFontSaliency(group2,
									group1) == 0) {
						if (repeatedStyleTitles.size() == 0
								|| (repeatedStyleTitles.size() > 0
										&& !repeatedStyleTitles.contains(title) && Saliency
										.HierarchicalGroupFontSaliency(group1,
												repeatedStyleTitles) == 0)) {
							repeatedStyleTitles.add(title);
						}

					}
				}
			}
		}

		return repeatedStyleTitles;
	}

	public Map<PDPage, List<HieBlockGroup>> getSegmentation() {
		return segmentation;
	}

	private void setSegmentation(Map<PDPage, List<HieBlockGroup>> segmentation) {
		this.segmentation = segmentation;
	}

	private int nonEmptyLineNum(List<Line> lines) {
		int count = 0;
		for (Line line : lines) {
			if (!line.toString().trim().equals("")) {
				count++;
			}
		}
		return count;
	}

	private void calculateAverageFontSize() {
		int count = 0;
		int fontSum = 0;
		for (BlockGroup group : currentGroups) {
			List<Line> lines = group.getLines();
			for (Line line : lines) {
				fontSum += line.getFontSize();
				count++;
			}
		}
		averageFontSize = fontSum / count;
	}

	private List<Glyph> removeLeadingEmptyGlyphs(List<Glyph> original) {
		List<Glyph> trim = new ArrayList<Glyph>();

		int startIndex = 0;
		for (int i = 0; i < original.size(); i++) {
			if (!original.get(i).getCharacter().trim().equals("")) {
				startIndex = i;
				break;
			}
		}
		for (int i = startIndex; i < original.size(); i++) {
			trim.add(original.get(i));
		}
		return trim;
	}

	private double getSpaceTolerance() {
		return 0.5;
	}

	private boolean isFooter(HieBlockGroup group) {
		return group.getLines().size() == 1
				&& !containsAnyGlyph(group.getLeft(), group.getRight(),
						group.getBottom(), currentPage.getBleedBox()
								.getHeight());
	}

	private boolean groupInBetween(HieBlockGroup group1, HieBlockGroup group2,
			HieBlockGroup outerGroup, List<HieBlockGroup> groups) {
		double top = Math.min(group1.getBottom(), group2.getBottom());
		double bottom = Math.max(group1.getTop(), group2.getTop());
		List<HieBlockGroup> subGroups = outerGroup == null ? new ArrayList<HieBlockGroup>()
				: outerGroup.getSubGroups();
		for (HieBlockGroup group : groups) {
			if (subGroups.contains(group)) {
				continue;
			}
			if (between(group, top, bottom)) {
				if (horiOverlap(group1, group) && horiOverlap(group2, group)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean horiOverlap(HieBlockGroup group1, HieBlockGroup group2) {
		return Math.max(group1.getLeft(), group2.getLeft()) < Math.min(
				group1.getRight(), group2.getRight());
	}
	
	private void popStack(Stack<HieBlockGroup> stack){
		if(!stack.isEmpty()){
			stack.pop();
		}
	}
	
	private HieBlockGroup peekStack(Stack<HieBlockGroup> stack){
		if(!stack.isEmpty()){
			return stack.peek();
		}
		return null;
	}

}
