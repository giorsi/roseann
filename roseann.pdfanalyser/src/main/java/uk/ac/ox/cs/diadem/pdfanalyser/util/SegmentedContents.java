/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.util;

import java.util.ArrayList;
import java.util.List;

import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;
import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;

public class SegmentedContents {

    /**
     * @param gl The list of glyphs related to a page
     * @param b The list of blocks related to a page
     * @param gr The list of groups related to a page
     */
    public SegmentedContents(List<Glyph> gl, List<Block> b, List<BlockGroup> gr) {
        glyphs = new ArrayList<Glyph>();
        blocks = new ArrayList<Block>();
        lines = new ArrayList<Line>();
        groups = new ArrayList<BlockGroup>();
        
        glyphs = gl;
        blocks = b;
        groups = gr;

        // for each block, we extract its lines that we add to the lines list
        for (Block block : blocks) {
            for (Line line : block.getLines()) {
                lines.add(line);
            }
        }
    }
    
    private List<Glyph> glyphs;
    private List<Line> lines;
    private List<Block> blocks;
    private List<BlockGroup> groups;

    /**
     * @return
     */
    public List<Glyph> getGlyphs() {
        return glyphs;
    }

    /**
     * @return
     */
    public List<Line> getLines() {
        return lines;
    }

    /**
     * @return
     */
    public List<Block> getBlocks() {
        return blocks;
    }

    /**
     * @return
     */
    public List<BlockGroup> getGroups() {
        return groups;
    }
}
