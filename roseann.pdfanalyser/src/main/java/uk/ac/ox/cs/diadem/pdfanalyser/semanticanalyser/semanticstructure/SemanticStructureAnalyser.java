package uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.semanticstructure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDPage;

import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;
import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.model.SemanticStructureBlock;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.model.SemanticStructureBlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.util.Saliency;
import uk.ac.ox.cs.diadem.pdfanalyser.util.Bullet;
import uk.ac.ox.cs.diadem.pdfanalyser.util.SegmentedContents;

public class SemanticStructureAnalyser {
	Map<PDPage, SegmentedContents> documentSegmentation;
	List<Block> titles;
	Map<PDPage, List<SemanticStructureBlockGroup>> semanticStructure;

	public SemanticStructureAnalyser(
			Map<PDPage, SegmentedContents> documentSegmentation,
			List<Block> titles) {
		this.documentSegmentation = documentSegmentation;
		this.titles = titles;
		this.semanticStructure = new HashMap<PDPage, List<SemanticStructureBlockGroup>>();
	}

	public Map<PDPage, List<SemanticStructureBlockGroup>> analyseSemanticStructure() {

		for (PDPage page : documentSegmentation.keySet()) {
			List<Block> paragraphs = new ArrayList<Block>();
			List<SemanticStructureBlockGroup> semanticGroups = new ArrayList<SemanticStructureBlockGroup>();

			SegmentedContents contents = documentSegmentation.get(page);
			List<BlockGroup> groups = contents.getGroups();
			SemanticStructureBlockGroup mostRecentTitleGroup = null;
			for (BlockGroup group : groups) {
				List<Block> blocks = group.getBlocks();
				List<Line> lines = group.getLines();

				SemanticStructureBlock content = null;
				// deal with title-paragrah structure
				if (titles.contains(blocks.get(0))) {
					SemanticStructureBlockGroup semanticBlockGroup = new SemanticStructureBlockGroup();
					SemanticStructureBlock titleBlock = new SemanticStructureBlock();
					titleBlock.addLines(blocks.get(0).getLines());

					SemanticStructureBlock contentBlock = new SemanticStructureBlock();
					for (int i = 1; i < blocks.size(); i++) {
						contentBlock.addLines(blocks.get(i).getLines());
						paragraphs.add(blocks.get(i));
					}
					content = contentBlock;

					semanticBlockGroup.addSemanticBlock(titleBlock);
					semanticBlockGroup.addSemanticBlock(contentBlock);
					semanticGroups.add(semanticBlockGroup);
					mostRecentTitleGroup = semanticBlockGroup;
				}
				// deal with title-list structure
				else if (isListHead(lines.get(0), lines)) {
					SemanticStructureBlockGroup semanticBlockGroup = new SemanticStructureBlockGroup();
					SemanticStructureBlock titleBlock = new SemanticStructureBlock();
					titleBlock.addLine(lines.get(0));

					SemanticStructureBlock contentBlock = new SemanticStructureBlock();
					for (int i = 1; i < lines.size(); i++) {
						contentBlock.addLine(lines.get(i));
					}
					content = contentBlock;

					semanticBlockGroup.addSemanticBlock(titleBlock);
					semanticBlockGroup.addSemanticBlock(contentBlock);
					semanticGroups.add(semanticBlockGroup);
				}
				// deal with other structure
				else {
					SemanticStructureBlock semanticBlock = new SemanticStructureBlock();
					semanticBlock.addLines(group.getLines());
					// if the current block is a paragraph
					if (paragraphs.size() > 0 && isParagraph(group, paragraphs)) {
						mostRecentTitleGroup.addSemanticBlock(semanticBlock);
					}
					// if the current block is title, footer and other
					// non-paragraph content
					else {
						SemanticStructureBlockGroup semanticBlockGroup = new SemanticStructureBlockGroup();
						semanticBlockGroup.addSemanticBlock(semanticBlock);
						semanticGroups.add(semanticBlockGroup);
					}
					content = semanticBlock;
				}

				// check current semantic block is a list structure
				String bullet = checkBullet(content);
				if (bullet != null) {
					List<SemanticStructureBlock> subBlocks = buildList(bullet,
							content);
					content = new SemanticStructureBlock();
					content.addSubBlocks(subBlocks);
				}

			}
			semanticStructure.put(page, semanticGroups);
		}
		return semanticStructure;
	}

	private boolean isParagraph(BlockGroup group, List<Block> paragraphs) {
		List<BlockGroup> groupToEvaluate = new ArrayList<BlockGroup>();
		groupToEvaluate.add(group);

		int saliency = Saliency.blockFontSaliency(groupToEvaluate, paragraphs);

		return saliency == 0;
	}

	private String checkBullet(SemanticStructureBlock contentBlock) {
		Line firstLine = contentBlock.getLines().get(0);
		return checkBullet(firstLine);
	}

	private String checkBullet(Line line) {
		Glyph firstNonEmpGlyph = null;
		for (Glyph glyph : line.getGlyphs()) {
			if (!glyph.getCharacter().equals(" ")) {
				firstNonEmpGlyph = glyph;
				break;
			}
		}
		if (firstNonEmpGlyph != null && Bullet.isBullet(firstNonEmpGlyph)) {
			return firstNonEmpGlyph.getCharacter();
		}
		return null;
	}

	private List<SemanticStructureBlock> buildList(String bullet,
			SemanticStructureBlock contentBlock) {
		List<SemanticStructureBlock> blocks = new ArrayList<SemanticStructureBlock>();
		List<Line> lines = contentBlock.getLines();
		SemanticStructureBlock block = null;
		for (Line line : lines) {
			if (line.toString().contains(bullet)) {
				if (block != null) {
					blocks.add(block);
				}
				block = new SemanticStructureBlock();
			}
			block.addLine(line);
		}
		blocks.add(block);
		return blocks;
	}

	private boolean isListHead(Line firstLine, List<Line> lines) {
		// the first line(title) must not contain any bullet
		// the remaining lines must contain at least two list item(two bullets)
		int bulletCount = 0;
		if (checkBullet(firstLine) == null) {
			for (int i = 1; i < lines.size(); i++) {
				Line line = lines.get(i);
				bulletCount += checkBullet(line) != null ? 1 : 0;
			}
			if (bulletCount > 1) {
				return true;
			}
		}
		return false;
	}
}
