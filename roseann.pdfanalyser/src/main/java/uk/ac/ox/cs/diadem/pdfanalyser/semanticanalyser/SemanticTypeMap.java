package uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * To define the semantic type mapping of all annotation concepts
 */
@XmlRootElement(namespace = "diadem.pdfanalyser.annotationsegmenter.xml.jaxb.model")
@XmlAccessorType(XmlAccessType.FIELD)
public class SemanticTypeMap {
	
	@XmlElementWrapper(name = "semanticMappingBag")
	private Map<String, String> semanticMappingBag;
	
	public SemanticTypeMap(){
		
		semanticMappingBag = new HashMap<String, String>();
		semanticMappingBag.put("magazine", "WORK");
		semanticMappingBag.put("book", "WORK");
		semanticMappingBag.put("printmedia", "WORK");
		semanticMappingBag.put("periodicalliterature", "WORK");
		
		semanticMappingBag.put("organisation", "ORGANISATION");
		semanticMappingBag.put("musicgroup", "ORGANISATION");
		semanticMappingBag.put("band", "ORGANISATION");
		semanticMappingBag.put("organizationdivision", "ORGANISATION");
		semanticMappingBag.put("mediaorg", "ORGANISATION");
		
		semanticMappingBag.put("money", "MONEY");
		
		semanticMappingBag.put("calendarunit", "CALENDARUNIT");
		
		semanticMappingBag.put("number", "NUMBER");
		
		semanticMappingBag.put("person", "ORGANISMCLASSIFICATION");
		
		semanticMappingBag.put("poi", "PLACE");
		semanticMappingBag.put("location", "PLACE");
		semanticMappingBag.put("postcode", "PLACE");
		semanticMappingBag.put("localadministration", "PLACE");
		semanticMappingBag.put("country", "PLACE");
		semanticMappingBag.put("district", "PLACE");
		semanticMappingBag.put("stateorcounty", "PLACE");
		semanticMappingBag.put("cityortown", "PLACE");
		semanticMappingBag.put("county", "PLACE");
		semanticMappingBag.put("town", "PLACE");
		semanticMappingBag.put("postcode_prefix", "PLACE");
		semanticMappingBag.put("property_type", "PLACE");
		semanticMappingBag.put("street_address", "PLACE");
		semanticMappingBag.put("street_suffix", "PLACE");
		
		
		semanticMappingBag.put("compass", "COMPASS");
		
		semanticMappingBag.put("email", "CONTACT");
		semanticMappingBag.put("phone_number", "CONTACT");
				
		semanticMappingBag.put("currency", "CURRENCY");
		
		semanticMappingBag.put("technology", "TECHNOLOGY");
		
		semanticMappingBag.put("kitchen_appliance", "HOMEFACILITY");
		semanticMappingBag.put("furniture", "HOMEFACILITY");
		semanticMappingBag.put("bathroom_number", "HOMEFACILITY");
		semanticMappingBag.put("bathroom", "HOMEFACILITY");
		semanticMappingBag.put("bedroom_number", "HOMEFACILITY");
		semanticMappingBag.put("bedroom", "HOMEFACILITY");
		semanticMappingBag.put("purpose_area", "HOMEFACILITY");
		semanticMappingBag.put("reception_room", "HOMEFACILITY");
		
		semanticMappingBag.put("position", "POSITION");
		
		semanticMappingBag.put("section_title", "SEC_TITLE");
	}
	
	public void setSemanticMappingBag(Map<String, String> semanticMappingBag){
		this.semanticMappingBag =  semanticMappingBag;
	}
	
	public Map<String, String> getSemanticMappingBag(){
		return this.semanticMappingBag;
	}
	
}



