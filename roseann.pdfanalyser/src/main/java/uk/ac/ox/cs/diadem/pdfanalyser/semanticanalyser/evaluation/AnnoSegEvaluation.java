package uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.evaluation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import uk.ac.ox.cs.diadem.pdfanalyser.DocumentProcessor;
import uk.ac.ox.cs.diadem.pdfanalyser.evaluation.EvalResults;
import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;

public class AnnoSegEvaluation {
	private static String annoOutputPath = "";
	private static String annoTruthPath = "";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(DocumentProcessor.class);
	
	public static void evaluateInternalResult(boolean hierarchical) {
		if(hierarchical){
			annoOutputPath = DocumentProcessor.internalResultDirectory + "hierarchical/";
			annoTruthPath = DocumentProcessor.annoTruthDirectory + "hierarchical/";
		}
		else{
			annoOutputPath = DocumentProcessor.internalResultDirectory + "flat/";
			annoTruthPath = DocumentProcessor.annoTruthDirectory + "flat/";
		}
		

		final EvalResults singleResults = evaluate(EvaluationFiles.singleColumn, "group");
		final EvalResults multiResults = evaluate(EvaluationFiles.multiColumn, "group");
		final EvalResults irregularResults = evaluate(EvaluationFiles.irregularStyle, "group");

		overall(singleResults, multiResults, irregularResults);
	}
	
	public static void evaluateExternalResult(boolean hierarchical){
		String subfolder = hierarchical? "hierarchical/" : "flat/";
		annoTruthPath = DocumentProcessor.annoTruthDirectory + subfolder;
		evaluateCalibre();
		evaluatePaperCrop();
	}
	
	public static void evaluateCalibre(){
		annoOutputPath = DocumentProcessor.externalResultDirectory + "calibre/";
		
		
		final EvalResults singleResults = evaluate(EvaluationFiles.singleColumn, "block");
		final EvalResults multiResults = evaluate(EvaluationFiles.multiColumn, "block");
		final EvalResults irregularResults = evaluate(EvaluationFiles.irregularStyle, "block");
		
		overall(singleResults, multiResults, irregularResults);
	}
	
	public static void evaluatePaperCrop(){
		annoOutputPath = DocumentProcessor.externalResultDirectory + "papercrop/";
		
		final EvalResults singleResults = evaluate(EvaluationFiles.singleColumn, "block");
		final EvalResults multiResults = evaluate(EvaluationFiles.multiColumn, "block");
		final EvalResults irregularResults = evaluate(EvaluationFiles.irregularStyle, "block");
		
		overall(singleResults, multiResults, irregularResults);
	}

	private static void overall(EvalResults singleResults, EvalResults multiResults, EvalResults irregularResults){
		final float globalCompleteness = (singleResults.getCompleteness() + multiResults
				.getCompleteness() + irregularResults.getCompleteness()) / (float) 3;
		final float globalPurity = (singleResults.getPurity() + multiResults
				.getPurity() + irregularResults.getPurity()) / (float) 3;
		final float globalPrecision = (singleResults.getPrecision() + multiResults
				.getPrecision() + irregularResults.getPrecision()) / (float) 3;
		final float globalRecall = (singleResults.getRecall() + multiResults
				.getRecall() + irregularResults.getRecall()) / (float) 3;
		final EvalResults globalResults = new EvalResults(globalCompleteness,
				globalPurity, globalPrecision, globalRecall);

		printResult(globalResults);
	}

	/**
	 * @param filesMap
	 *            Maps the files we want to use to the pages that will be tested
	 * @return An {@link EvalResults} object containing the purity and
	 *         completeness of the map given as input
	 */
	private static EvalResults evaluate(Map<String, ArrayList<Integer>> filesMap, String granuName) {
		final EvalResults results;
		int totalExtractedElements = 0;
		int totalExistingElements = 0;
		int totalPurity = 0;
		int totalCompleteness = 0;
		int totalCorrectRetrievedElements = 0;
		List<BlockGroup> pageExtractedGroups = new ArrayList<BlockGroup>();
		List<BlockGroup> pageExistingGroups = new ArrayList<BlockGroup>();
		final DocumentBuilderFactory dbFactoryOutput = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder dBuilderOutput;
		final DocumentBuilderFactory dbFactoryTruth = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder dBuilderTruth;

		for (String fileName : filesMap.keySet()) {
			int documentCompleteness = 0;
			int documentPurity = 0;
			int documentExtractedElements = 0;
			int documentExistingElements = 0;
			int documentCorrectRetrievedElements = 0;
			String outputFileName = annoOutputPath
					+ fileName.substring(0, fileName.lastIndexOf("."))
					+  "_output.xml";
			String truthFileName = annoTruthPath + fileName + "_truth.xml";
			File outputFile = new File(outputFileName);
			File truthFile = new File(truthFileName);

			try {
				dBuilderOutput = dbFactoryOutput.newDocumentBuilder();
				Document outputTree = dBuilderOutput.parse(outputFile);
				outputTree.getDocumentElement().normalize();

				dBuilderTruth = dbFactoryTruth.newDocumentBuilder();
				Document truthTree = dBuilderTruth.parse(truthFile);
				truthTree.getDocumentElement().normalize();

				NodeList groups = outputTree.getDocumentElement()
						.getElementsByTagName(granuName);

				// The first step is to add all the extracted and real block
				// groups to the corresponding lists
				if (groups.getLength() < 1) {
					// if there are no <block> elements in the output we cannot
					// perform the evaluation
					LOGGER.error("The evaluation must be run at the BLOCK GROUP level. "
							+ "There is no <group> element in the output "
							+ outputFileName);
				} else {
					NodeList pagesList = outputTree.getDocumentElement()
							.getElementsByTagName("page");
					for (int i = 0; i < pagesList.getLength(); i++) {
						Element currentPage = (Element) pagesList.item(i);
						String pageNumber = currentPage.getAttribute("number");

						if (filesMap.get(fileName).contains(
								Integer.parseInt(pageNumber))) {
							int pageCompleteness = 0;
							int pagePurity = 0;
							int pageExtractedElements = 0;
							int pageExistingElements = 0;
							int pageCorrectRetrievedElements = 0;
							pageExistingGroups = new ArrayList<BlockGroup>();
							pageExtractedGroups = new ArrayList<BlockGroup>();
							groups = currentPage.getElementsByTagName(granuName);

							for (int j = 0; j < groups.getLength(); j++) {
								// we add all the blocks of the output to the
								// extracted blocks list
								Node currentGroup = groups.item(j);
								float top = Float.parseFloat(currentGroup
										.getAttributes().getNamedItem("top")
										.getTextContent());
								float bottom = Float.parseFloat(currentGroup
										.getAttributes().getNamedItem("bottom")
										.getTextContent());
								float left = Float.parseFloat(currentGroup
										.getAttributes().getNamedItem("left")
										.getTextContent());
								float right = Float.parseFloat(currentGroup
										.getAttributes().getNamedItem("right")
										.getTextContent());
								pageExtractedGroups.add(new BlockGroup(top,
										bottom, left, right));
							}

							// we add all the real blocks to the existing blocks
							// list
							NodeList truthPages = truthTree
									.getElementsByTagName("page");
							boolean pageFound = false;

							for (int j = 0; j < truthPages.getLength()
									&& !pageFound; j++) {
								if (pageNumber.equals(truthPages.item(j)
										.getAttributes().getNamedItem("number")
										.getTextContent())) {
									currentPage = (Element) truthPages.item(j);
									pageFound = true;
								}
							}

							if (!pageFound) {
								// This page was not specified in the ground
								// truth
								// file
								LOGGER.error("Page number "
										+ pageNumber
										+ " does not exist in the groung truth file:" + fileName);
								System.exit(1);
							}

							groups = currentPage.getElementsByTagName("block");

							for (int j = 0; j < groups.getLength(); j++) {
								// we add all the blocks of the output to the
								// extracted blocks list
								Node currentGroup = groups.item(j);
								// adjust top and bottom by 7 and 5 because the
								// tools we used to generate ground truth has
								// inaccurate shift
								float top = Float.parseFloat(currentGroup
												.getAttributes()
												.getNamedItem("top")
												.getTextContent());
								float bottom = Float.parseFloat(currentGroup
												.getAttributes()
												.getNamedItem("bottom")
												.getTextContent());
								float left = Float.parseFloat(currentGroup
										.getAttributes().getNamedItem("left")
										.getTextContent());
								float right = Float.parseFloat(currentGroup
										.getAttributes().getNamedItem("right")
										.getTextContent());
								pageExistingGroups.add(new BlockGroup(top,
										bottom, left, right));
							}

							pageExistingElements = pageExistingGroups.size();
							pageExtractedElements = pageExtractedGroups.size();

							for (int j = 0; j < pageExtractedGroups.size(); j++) {
								BlockGroup extracted = pageExtractedGroups
										.get(j);
								for (int k = 0; k < pageExistingGroups.size(); k++) {
									BlockGroup existing = pageExistingGroups
											.get(k);
									if (extracted.contains(existing)) {
										pageCompleteness++;
									}

									if (existing.contains(extracted)) {
										pagePurity++;
									}

									if (extracted.isIdentical(existing)) {
										pageCorrectRetrievedElements++;
									}
								}
							}

							documentCorrectRetrievedElements += pageCorrectRetrievedElements;
							documentExtractedElements += pageExtractedElements;
							documentExistingElements += pageExistingElements;
							documentCompleteness += pageCompleteness;
							documentPurity += pagePurity;

						}
					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			totalCorrectRetrievedElements += documentCorrectRetrievedElements;
			totalCompleteness += documentCompleteness;
			totalPurity += documentPurity;
			totalExtractedElements += documentExtractedElements;
			totalExistingElements += documentExistingElements;
			
		}

		final float completeness = totalCompleteness
				/ (float) totalExistingElements;
		final float purity = totalPurity / (float) totalExtractedElements;

		final float precision = totalCorrectRetrievedElements
				/ (float) totalExtractedElements;
		final float recall = totalCorrectRetrievedElements
				/ (float) totalExistingElements;
		results = new EvalResults(completeness, purity, precision, recall);
		return results;
	}
	
	private static void printResult(EvalResults result) {
		System.out.println("==========EVALUATION RESULT===============");
		System.out.println("Completeness: " + result.getCompleteness());
		System.out.println("Purity: " + result.getPurity());
		System.out.println("Precision: " + result.getPrecision());
		System.out.println("Recall: " + result.getRecall());
	}


}
