/*
 * DIADEM Document Analyser

 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uk.ac.ox.cs.diadem.pdfanalyser.util.comparator.LineCoordinateComparator;
import uk.ac.ox.cs.diadem.pdfanalyser.util.comparator.TopBlockCoordinateComparator;

public class BlockGroup extends PDFSegment{

	/**
	 * Creates a new BlockGroup instance
	 */
	public BlockGroup() {
		blocks = new ArrayList<Block>();
		contents = new ArrayList<Line>();
	}

	/**
	 * This constructor is only used to create block group entities for the
	 * evaluation process
	 * 
	 * @param top
	 *            The top coordinate of the block
	 * @param bottom
	 *            The bottom coordinate of the block
	 * @param left
	 *            The left coordinate of the block
	 * @param right
	 *            The right coordinate of the block
	 * 
	 */
	public BlockGroup(final float top, final float bottom, final float left,
			final float right) {
		this.top = top;
		this.bottom = bottom;
		this.left = left;
		this.right = right;
	}

	List<Block> blocks;
	List<Line> contents;

	/**
	 * @param block
	 *            The block that will initialise a new BlockGroup instance
	 */
	public BlockGroup(final Block block) {
		blocks = new ArrayList<Block>();
		contents = new ArrayList<Line>();
		blocks.add(block);
		for (Line line : block.getLines()) {
			contents.add(line);
		}
		block.setGroup(this);
		this.top = block.getTop();
		this.bottom = block.getBottom();
		this.left = block.getLeft();
		this.right = block.getRight();
	}

	/**
	 * Adds a block to the current group
	 * 
	 * @param block
	 *            The block to be added in the current group
	 */
	public void addBlock(final Block block) {
		blocks.add(block);
		for (Line line : block.getLines()) {
			contents.add(line);
		}
		block.setGroup(this);

		// update top, bottom, left, right
		if (block.getTop() < this.top) {
			this.top = block.getTop();
		}
		if (block.getBottom() > this.bottom) {
			this.bottom = block.getBottom();
		}
		if (block.getLeft() < this.left) {
			this.left = block.getLeft();
		}
		if (block.getRight() > this.right) {
			this.right = block.getRight();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String returnValue = "";

		sortLines();

		for (Line line : contents) {
			returnValue += line.toString() + " ";
		}

		return returnValue;
	}

	/**
	 * sort lines according to reading order
	 */
	public void sortLines() {
		LineCoordinateComparator comparator = new LineCoordinateComparator(contents);
		Collections.sort(contents, comparator);
	}

	/**
	 * @return The list of blocks of the current group
	 */
	public List<Block> getBlocks() {
		return blocks;
	}

	public List<Line> getLines() {
		return contents;
	}

	public List<Line> getNonEmptyLines() {
		List<Line> nonEmpLines = new ArrayList<Line>();
		for (Line line : contents) {
			String lineContext = line.toString();
			if (!lineContext.trim().equals("")) {
				nonEmpLines.add(line);
			}
		}
		return nonEmpLines;
	}

	/**
	 * @return The average vertical space height within the current group
	 */
	public double getAverageVerticalSpace() {

		double averageVerticalSpace = 0.;

		if (!blocks.isEmpty()) {
			double totalSpacing = 0.;
			double totalBlanks = 0.;

			Block precedingBlock = blocks.get(0);
			final TopBlockCoordinateComparator comparator = new TopBlockCoordinateComparator();
			Collections.sort(blocks, comparator);

			totalSpacing = precedingBlock.getTotalVerticalSpacing();
			totalBlanks = precedingBlock.getTotalVerticalBlanks();

			for (int i = 1; i < blocks.size(); i++) {

				final Block currentBlock = blocks.get(i);
				if (currentBlock.getTop() > precedingBlock.getBottom()) {
					totalSpacing += currentBlock.getTop()
							- precedingBlock.getBottom();
					totalBlanks++;
				}

				if (currentBlock.getLines().size() > 1) {
					// otherwise, the averageSpacing is 0...
					totalSpacing += currentBlock.getTotalVerticalSpacing();
					totalBlanks += currentBlock.getTotalVerticalBlanks();
				}

				precedingBlock = currentBlock;
			}

			averageVerticalSpace = totalSpacing / totalBlanks;
		}

		if (averageVerticalSpace < 0.) {
			// This occurs if there is a note mark in the line/block
			averageVerticalSpace = Double.NaN;
		}

		return averageVerticalSpace;
	}

	/**
	 * @param list
	 *            The list of blocks that has to be added to the list of the
	 *            current block
	 */
	public void addBlocks(final List<Block> list) {

		if (!list.isEmpty()) {
			for (final Block b : list) {
				addBlock(b);
			}
		}
	}

	public boolean contains(BlockGroup blockGroup) {

		boolean containsTop = top <= blockGroup.top
				|| Math.abs(top - blockGroup.top) <= 3;
		boolean containsBottom = bottom >= blockGroup.bottom
				|| Math.abs(bottom - blockGroup.bottom) <= 3;
		boolean containsLeft = left <= blockGroup.left
				|| Math.abs(left - blockGroup.left) <= 3;
		boolean containsRight = right >= blockGroup.right
				|| Math.abs(right - blockGroup.right) <= 3;

		return containsTop && containsBottom && containsLeft && containsRight;
	}

	public boolean isIdentical(BlockGroup blockGroup) {
		boolean identicalTop = Math.abs(top - blockGroup.top) <= 3;
		boolean identicalBottom = Math.abs(bottom - blockGroup.bottom) <= 3;
		boolean identicalLeft = Math.abs(left - blockGroup.left) <= 3;
		boolean identicalRight = Math.abs(right - blockGroup.right) <= 3;

		return identicalTop && identicalBottom && identicalLeft
				&& identicalRight;
	}
	
	public double getVerticalSpace(){
		Line firstLine = this.getBlocks().get(0).getLines().get(0);
		return (firstLine.getBottom() - firstLine.getTop());
	}
	
}
