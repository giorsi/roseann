package uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;
import uk.ac.ox.cs.diadem.pdfanalyser.util.comparator.LineCoordinateComparator;

public class SemanticStructureBlock implements SemanticStructure {
	private List<SemanticStructureBlock> subBlocks;
	private List<Line> lines;
	private double top;
	private double bottom;
	private double left;
	private double right;
	
	public SemanticStructureBlock(){
		this.subBlocks = new ArrayList<SemanticStructureBlock>();
		lines = new ArrayList<Line>();
	}
	
	public void addSubBlock(final SemanticStructureBlock subBlock) {
		this.subBlocks.add(subBlock);
		addLines(subBlock.getLines());
	}
	
	public void addSubBlocks(final List<SemanticStructureBlock> blocks){
		for(SemanticStructureBlock subBlock : blocks){
			addSubBlock(subBlock);
		}
	}
	
	public void addLine(Line line){
		if(lines.size() == 0){
			top = line.getTop();
			bottom = line.getBottom();
			left = line.getLeft();
			right = line.getRight();
		}
		else{
			if (line.getTop() < top) {
				top = line.getTop();
			}
			if (line.getLeft() < left) {
				left = line.getLeft();
			}
			if (line.getRight() > right) {
				right = line.getRight();
			}
			if (line.getBottom() > bottom) {
				bottom = line.getBottom();
			}
		}
		this.lines.add(line);
	}
	
	public void addLines(List<Line> lines){
		for(Line line:lines){
			addLine(line);
		}
	}

	public List<SemanticStructureBlock> getSubBlocks(){
		return this.subBlocks;
	}
	
	public List<Line> getLines(){
		return this.lines;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String retValue = "";

		if (lines != null) {
			sortLines();

			for (final Line l : lines) {
				retValue += l.toString() + " ";
			}
		}
		
		return retValue;
	}

	/**
	 * sort lines according to reading order
	 */
	public void sortLines() {
		if (lines != null) {
			final LineCoordinateComparator comparator = new LineCoordinateComparator(lines);
			Collections.sort(lines, comparator);
		}
	}
	
	/**
	 * @return The top coordinate of the block
	 */
	public double getTop() {
		return top;
	}

	/**
	 * @param top
	 */
	public void setTop(final double top) {
		this.top = top;
	}

	/**
	 * @return
	 */
	public double getBottom() {
		return bottom;
	}

	/**
	 * @param bottom
	 */
	public void setBottom(final double bottom) {
		this.bottom = bottom;
	}

	/**
	 * @return The left coordinate of the block
	 */
	public double getLeft() {
		return left;
	}

	/**
	 * @param left
	 */
	public void setLeft(final double left) {
		this.left = left;
	}

	/**
	 * @return The right coordinate of the block
	 */
	public double getRight() {
		return right;
	}

	/**
	 * @param right
	 */
	public void setRight(final double right) {
		this.right = right;
	}

	
}
