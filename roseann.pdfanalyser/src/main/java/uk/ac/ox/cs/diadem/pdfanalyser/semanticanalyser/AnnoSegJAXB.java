package uk.ac.ox.cs.diadem.pdfanalyser.semanticanalyser;

import java.io.File;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class AnnoSegJAXB {

	public static void main(String[] args) {

		try {
			
			/*
			 * Write semantic type map xml mapping file
			 */
			SemanticTypeMap stm = new SemanticTypeMap();
			JAXBContext context = JAXBContext
					.newInstance(SemanticTypeMap.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			// Write to output file
			final URL outputURL = AnnoSegJAXB.class
					.getResource("semanticTypeMap.xml");
			m.marshal(stm, new File(outputURL.getFile()));
			
			/*
			 * 
			 */
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}

}
