/*
 * DIADEM pdfanalyser
 * 
 * Copyright (C) 2012 DIADEM Team, Department of Computer Science,
 * University of Oxford - All rights reserved.
 * 
 * Wolfson Building
 * Parks Road 
 * OX1 3QD - Oxford (UK)
 *
 * This software is proprietary and its source and binary code
 * cannot be copied or re-distributed without prior consent of
 * a DIADEM Team officer.
 * 
 */
package uk.ac.ox.cs.diadem.pdfanalyser;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Segment;
/**
 * PDF Analyser test class. This test class is for unit tesing the
 * DocumentProcessorTest class.
 * 
 * @author Giorgio Orsi (giorgio dot orsi at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class DocumentProcessorTest extends StandardTestcase {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentProcessorTest.class);
    private PDFSegmenter segmenter;

    @Before
    public void bringUp() {
        try {
            LOGGER.error("Loading document processor.");
            segmenter = new DocumentProcessor();
        } catch (final IOException ioe) {
            LOGGER.error("Unable to load Apache PDFTextStripper property file.");
        }
    }

    @Test
    public void testProcessDocument() {
        final String testName = "us-026";

        // segmenter.segment(inputStream, outputStream, blocks);

        assertTrue(true);
    }

    @Test
    public void testOutputContents() {
        try {
            LOGGER.info("Testing the contents of the output file.");
            final String fileName = "d-us-001.pdf";
            final String inputPath = "src/main/resources/files/";
            final String outputPath = "data/test/";
            final File inputFile = new File(inputPath + fileName);
            final File outputFile = new File(outputPath + fileName + "_output.xml");
            final Segment[] segments = { Segment.BLOCK, Segment.BLOCK_GROUP, Segment.GLYPH, Segment.LINE };
            final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder dBuilder;

            segmenter.segment(inputFile, outputFile, segments);

            assertTrue(outputFile.exists() && outputFile.isFile());

            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(outputFile);
            doc.getDocumentElement().normalize();

            Element rootElement = doc.getDocumentElement();
            assertTrue(rootElement.getNodeName().equals("document"));
            assertTrue(rootElement.getAttribute("name").equals(fileName));

            NodeList pages = doc.getElementsByTagName("page");
            assertTrue(pages.getLength() > 0);

            if (Arrays.asList(segments).contains(Segment.BLOCK)) {
                NodeList blocks = doc.getElementsByTagName("block");
                assertTrue(blocks.getLength() > 0);
            }

            if (Arrays.asList(segments).contains(Segment.BLOCK_GROUP)) {
                NodeList groups = doc.getElementsByTagName("group");
                assertTrue(groups.getLength() > 0);
            }

            if (Arrays.asList(segments).contains(Segment.LINE)) {
                NodeList lines = doc.getElementsByTagName("line");
                assertTrue(lines.getLength() > 0);
            }

            if (Arrays.asList(segments).contains(Segment.GLYPH)) {
                NodeList glyphs = doc.getElementsByTagName("glyph");
                assertTrue(glyphs.getLength() > 0);
            }

        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage());
        } catch (ParserConfigurationException e) {
            LOGGER.error(e.getMessage());
        } catch (SAXException e) {
            LOGGER.error(e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

}
