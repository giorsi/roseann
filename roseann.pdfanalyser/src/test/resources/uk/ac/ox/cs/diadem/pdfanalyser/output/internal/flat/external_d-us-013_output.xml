<document name="external_d-us-013">
    <page number="3">
        <group bottom="104.85" id="0" left="139.05" right="370.8" top="98.99">
            <contents>3. Workshop Breakout Session Reports </contents>
        </group>
        <group bottom="143.63" id="1" left="99.53" right="258.24" top="139.18">
            <contents>10 RESEARCH AND POLICY THEMES </contents>
        </group>
        <group bottom="281.22" id="2" left="99.53" right="409.24" top="159.11">
            <contents>The natural scientists, social and behavioral scientists, engineers, policy makers, philosophers, and legal experts participating in the December 2003 workshop divided into 10 panels, each focused on one theme: productivity and equity; future economic scenarios; the quality of life; future social scenarios; converging technologies; national security and space exploration; ethics, governance, risk and uncertainty; public policy, legal and international aspects; interaction with the public; education and human development. Each group produced a summary, reporting the current state of knowledge; anticipated developments; appropriate research and evaluation methodologies; areas of needed research, education and infrastructure development; and action recommendations. Summaries of the conclusions from each of these 10 panels follow. </contents>
        </group>
        <group bottom="354.89" id="3" left="75.99" right="408.74" top="331.69">
            <contents>THEME 1: PRODUCTIVITY AND EQUITY Moderators: Mihail Roco (National Science Foundation) and Marie Thursby (Georgia Tech) </contents>
        </group>
        <group bottom="406.2" id="4" left="75.99" right="433.89" top="369.6">
            <contents>Contributors: James Adams, Mark Andrews, John Belk, Jared Bernstein, William Boulton, Georg G. A. Böhm, James Canton, Ken Chung, Julia Clark, J. Bradford DeLong, Richard Freeman, Robin Hanson, Louis Hornyak, Evelyn Hu, Peter Hébert, Laurence Iannaccone, Bruce Kramer, Joseph Reed, James Rudd, Jeffrey M. Stanton, E. J. Taylor, George Thompson, Raymond K. Tsui, Sarah Turner </contents>
        </group>
        <group bottom="519.61" id="5" left="75.99" right="433.9" top="420.91">
            <contents>Introduction Because nanotechnology enhances many other technologies and has a great variety of potential applications [1, 2, 3, 4, 5, 6], major national efforts should be focused on how nanotechnology can increase productivity in manufacturing, improve energy resources and utilization, reduce environmental impacts of industry and transportation, enhance healthcare, produce better pharmaceuticals, improve agriculture and food production, and expand the capabilities of information technologies (see Figure 3.1 [4]). These areas are of general societal interest and span multiple industrial areas and research disciplines. Education about nanoscience and nanotechnology that underscores the unity of nature and manmade systems at this scale must start early in the educational process. </contents>
        </group>
        <group bottom="603.27" id="6" left="75.99" right="433.91" top="534.32">
            <contents>The panelists made estimations of the rate of introduction of nanotechnology in their own companies and the sector they represent. They reached the conclusion that by 2015, a minimum of 50 percent of new products in manufacturing and medical approaches are expected to be affected by nanotechnology. Nanotechnology is already a part of industrial processes and products—both in several traditional areas of application and in emerging ones. Nanotechnology has already affected industry, basic scientiﬁc research, education, and the global economy. These impacts are expected to continue to accrue over the coming decades. One must proactively address future implications of nanotechnology for their </contents>
        </group>
        <group bottom="644.87" id="7" left="185.87" right="313.42" top="641.01">
            <contents>Nanotechnology: Societal Implications </contents>
        </group>
        <group bottom="645.81" id="8" left="424.86" right="433.85" top="641.57">
            <contents>41 </contents>
        </group>
    </page>
    <page number="2">
        <group bottom="68.85" id="0" left="177.03" right="313.21" top="64.99">
            <contents>2. Introductory and Summary Comments </contents>
        </group>
        <group bottom="111.79" id="1" left="66.18" right="424.11" top="96.76">
            <contents>reaching research. Future generations may well judge our success—and our wisdom—by how well we realize the potential of nanoscience and engineering while avoiding the pitfalls. </contents>
        </group>
        <group bottom="184.67" id="2" left="66.18" right="424.1" top="126.5">
            <contents>In each NSF program or initiative, we embody the need to forge a seamless transition from research results to societal change. We must act quickly as a community to develop the means for measuring and predicting the societal and economic impacts of nanoscale research. Following the pioneering work of Richard Smalley, nanotubes quickly became a recognized commodity. Fine-grained powders manufactured at the atomic level also found a ready market. What are the next outcomes, the next products? The answers are coming swiftly, and we need to be prepared. </contents>
        </group>
        <group bottom="225.2" id="3" left="66.18" right="424.12" top="199.38">
            <contents>As we advance the technology, we must ready the social infrastructure and engage the public in managing change. It’s a weighty challenge, but one that scientists and engineers already engaged in far-reaching research can readily embrace. We welcome your ideas and assistance. </contents>
        </group>
        <group bottom="252.38" id="4" left="66.18" right="232.68" top="248.08">
            <contents>THE FUTURE OF NANOTECHNOLOGY </contents>
        </group>
        <group bottom="271.12" id="5" left="66.18" right="330.52" top="267.03">
            <contents>John H. Marburger, III, Director, Ofﬁce of Science and Technology Policy </contents>
        </group>
        <group bottom="387.3" id="6" left="160.09" right="424.09" top="285.99">
            <contents>Good afternoon. My instructions are to provide a “visionary presentation focusing on the future of the ﬁeld” of nanotechnology from the perspective of the Federal Government. Let me say at the outset that “nanotechnology” is not so much a “ﬁeld” as a word—a neologism—that has been pressed into service to symbolize the status of a very large and important sector of contemporary science. It is possible to ﬁnd narrower uses of the word, some of which I will mention later, but participants in this workshop are surely aware that nanotechnology refers implicitly to a set of capabilities at the atomic scale that grew steadily throughout the last half of the past century into the basis for a true technology revolution in our society. </contents>
        </group>
        <group bottom="586.99" id="7" left="66.18" right="424.12" top="402.01">
            <contents>I speak of a revolution in technology rather than in science, because the underlying science is not itself revolutionary. Not that the scientiﬁc work is complete or unexciting—to the contrary—but we have known for more than a century that all the matter of everyday life is made of atoms. And we have known in principle for nearly 50 years how to calculate, with exquisite precision, many of the properties of matter, given certain input information and enough computing power. But not until recently have we actually had the instruments to make atomic-level measurements, and the computing power to exploit that knowledge. Now we have it, or are getting it, and the implications are enormous. Everything being made of atoms, the capability to measure, manipulate, simulate, and visualize at the atomic scale potentially touches every material aspect of our interaction with the world around us. That is why we speak of a revolution—like the industrial revolution—rather than just another step in technological progress. It is no wonder that developed nations are eager to produce and acquire the technologies that are being spawned by these new atomic-scale capabilities. As far as I can tell, the science plans for every developed nation and the European Union have a strong “nano” focus. The United States has been a world leader in the development of the underlying science infrastructure for the revolution, and the development of nanotechnology is today a national priority. Let me take a few moments to put the National Nanotechnology Initiative (NNI) into perspective. </contents>
        </group>
        <group bottom="615.14" id="8" left="71.76" right="418.37" top="602.84">
            <contents>This talk was presented by Celia Merzbacher of OSTP so that Dr. Marburger could be present at the signing of the Century Nanotechnology Research and Development Act. </contents>
        </group>
        <group bottom="615.14" id="9" left="73.29" right="80.64" top="611.67">
            <contents>21 </contents>
        </group>
        <group bottom="645.81" id="10" left="66.18" right="75.17" top="641.57">
            <contents>12 </contents>
        </group>
        <group bottom="644.87" id="12" left="183.44" right="310.98" top="641.01">
            <contents>Nanotechnology: Societal Implications </contents>
        </group>
    </page>
    <page number="1">
        <group bottom="68.85" id="0" left="225.08" right="265.14" top="64.99">
            <contents>1. Overview </contents>
        </group>
        <group bottom="144.15" id="1" left="66.18" right="424.1" top="96.76">
            <contents>well. They also emphasized the need to assess weaknesses in the existing workforce so that retraining and supplemental training of scientists, engineers, and others will contribute to technological development. Because nanoscience and nanotechnology are being developed in all industrialized nations, no country can depend upon foreign students for its scientiﬁc human capital; thus the United States must produce an ever-increasing number of domestic scientists and engineers. </contents>
        </group>
        <group bottom="270.95" id="2" left="66.18" right="424.08" top="158.86">
            <contents>Nanotechnology presents both the need and the opportunity for transformation of our educational system. Teaching of the sciences is highly fragmented today, whereas nanotechnology bridges physics, chemistry and biology. Some believe that if we ﬁrst integrate science teaching around phenomena at the nanoscale, education could likewise be integrated across the physical sciences, technology, the social sciences and even the humanities [12, 13]. The challenges are immense, but the need for a new model of education is widely recognized. </contents>
        </group>
        <group bottom="600.06" id="3" left="66.18" right="196.12" top="285.66">
            <contents>A second research area emphasized by nearly every breakout group at the workshop (and by many individual contributors to this report) was that of ascertaining risks associated with nanotechnology. All technologies present advantages and disadvantages and must be managed; however, the idea of controlling materials at the nanoscale, which is at the same scale at which living systems operate at their most fundamental level, raises concerns for some people. Research is needed to understand the risks, how to mitigate those risks that are real, and how to harness nanotechnology to the service, but not the disservice, of humanity. Of equal importance to ensuring the development of nanotechnology products that enhance the quality of life, is the need to engage the interested public in discussions about such development. The issues of power and trust are central to public dialogue, and to merely inform the general public about the conclusions of experts is insufﬁcient </contents>
        </group>
        <group bottom="645.81" id="4" left="66.18" right="70.67" top="641.57">
            <contents>8 </contents>
        </group>
        <group bottom="224.3" id="5" left="218.97" right="405.91" top="220.01">
            <contents>ANTICIPATED SOCIETAL DEVELOPMENTS </contents>
        </group>
        <group bottom="301.65" id="6" left="218.97" right="408.55" top="238.96">
            <contents>While true solutions to societal problems require enlightened policy, world peace, and equitable distribution of resources, public and governmental interest in nanotechnology R&amp;D is providing a model for responsible technological development. Among the societal changes that were envisioned at the workshop as contributing toward responsible development of nanotechnology are: </contents>
        </group>
        <group bottom="318.13" id="7" left="218.97" right="382.2" top="313.89">
            <contents>• Substantial contributions to economic growth </contents>
        </group>
        <group bottom="343.65" id="8" left="218.97" right="408.53" top="329.98">
            <contents>• Widespread public involvement in discussions and deliberations about technology development </contents>
        </group>
        <group bottom="408.77" id="9" left="218.97" right="408.55" top="355.88">
            <contents>• Advanced and educationally effective online- information resources devoted to ethics and societal dimensions of nanotechnology, integrated not only into conventional K-12 and college courses but also into continuing education for companies, scientists, and engineers </contents>
        </group>
        <group bottom="464.08" id="10" left="218.97" right="408.55" top="421">
            <contents>• Interactions between teams developing scientiﬁc, engineering, or social projects related to nanotechnology and expert communicators, who communicate across fields, facilitating multidisciplinary collaboration </contents>
        </group>
        <group bottom="519.39" id="11" left="218.97" right="408.56" top="476.32">
            <contents>• Innovations and reforms in many branches of the law, including torts, environmental law, employment and labor law, health and family law, criminal law, constitutional law, international trade law, and antitrust law </contents>
        </group>
        <group bottom="584.51" id="12" left="218.97" right="408.54" top="531.63">
            <contents>• Supported by public understanding and conﬁdence, successful coordination by the National Nanotechnology Initiative of nanoscience research and development to achieve the social, technological, and economic goals driving the development of nanotechnology </contents>
        </group>
        <group bottom="644.87" id="13" left="183.44" right="310.98" top="641.01">
            <contents>Nanotechnology: Societal Implications </contents>
        </group>
    </page>
    <page number="0">
        <group bottom="68.85" id="0" left="225.08" right="265.14" top="64.99">
            <contents>1. Overview </contents>
        </group>
        <group bottom="111.79" id="1" left="66.18" right="424.05" top="96.76">
            <contents>the knowledge they need to make informed decisions about nanotechnology and its products. Here are brief abstracts of the panel reports: </contents>
        </group>
        <group bottom="130.8" id="2" left="66.18" right="170.06" top="126.5">
            <contents>1. Productivity and Equity </contents>
        </group>
        <group bottom="319.65" id="3" left="66.18" right="424.1" top="145.46">
            <contents>Nanotechnology has entered the mainstream of industry and many companies have shown their conﬁdence in nanotechnology’s future by committing substantial resources to its development. In the near term, developments are expected to be gradual changes that will incrementally improve manufacturing costs and product features. Longer-term developments are likely to occur in convergence with other emerging technologies, such as biotechnology and information technology, where nanotechnology will serve as an enabler of a new product or industry category. The effects of nanotechnology are expected to stimulate productivity in manufacturing in most sectors of the economy that deal with the materials world. Better tools and measures for understanding the social and economic implications of nanotechnology are recommended. Researchers must proceed beyond the use of published, aggregate-level data available in econometric studies in order to get inside the research and development (R&amp;D) processes as they occur. The panel recommended a programmatic approach to increase synergy in nanotechnology development by creating partnerships earlier in the R&amp;D processes between industry, academia, national laboratories, and funding agencies. To the extent possible, government and the private sector should anticipate and mitigate negative impacts resulting from new technologies, including worker displacement and unbalanced distribution of beneﬁts and risks in society. </contents>
        </group>
        <group bottom="338.66" id="4" left="66.18" right="183.11" top="334.36">
            <contents>2. Future Economic Scenarios </contents>
        </group>
        <group bottom="527.51" id="5" left="66.18" right="424.12" top="353.31">
            <contents>In addressing future economic effects, researchers need to take a number of different but ultimately complementary viewpoints. The overall macroeconomic viewpoint considers the effects on economic growth, productivity, real wages, and the standard of living. The industrial organization viewpoint focuses on the particular industries that will be most directly affected by nanotechnology and attempts to assess how they will be transformed. For each of these viewpoints, the ﬁrst step is to frame the “counter-factual” question—what is the effect of the nanotechnology compared to what alternative? To maximize the beneﬁts from investment in nanoscale science and engineering, scientiﬁc research should be broadly funded and based primarily on peer-reviewed investigator-initiated proposals. Research should not be driven by a few speciﬁc top-down priorities. Economists can help to maximize development of beneﬁcial applications by contributing research in the following areas as applied to nanotechnology: the transfer of knowledge from academe to industry; the levels of return on nanotechnology investment; the effect of healthier lives on work patterns; the skill biases associated with major nanotechnology applications and their implications for wages and returns to education; the potential impacts and prudence of research exemptions for patents; and, lastly, the most efﬁcient and effective forms of government-industry-academe research cooperation. </contents>
        </group>
        <group bottom="546.51" id="6" left="66.18" right="150.81" top="542.22">
            <contents>3. The Quality of Life </contents>
        </group>
        <group bottom="608.56" id="7" left="66.18" right="424.12" top="561.17">
            <contents>Research on the societal dimensions of nanotechnology should identify the qualities of work, life, and the environment to which citizens give their highest priority, and identify the branches of nanotechnology that are most relevant to them. A means of monitoring for the early signs of negative aspects and risks should be developed, thereby permitting the timely development of contingency plans to handle problems. An issue of great political and ethical signiﬁcance is the possibility that </contents>
        </group>
        <group bottom="645.81" id="8" left="66.18" right="70.67" top="641.57">
            <contents>4 </contents>
        </group>
        <group bottom="644.87" id="9" left="183.44" right="310.98" top="641.01">
            <contents>Nanotechnology: Societal Implications </contents>
        </group>
    </page>
    <page number="4">
        <group bottom="68.85" id="0" left="191.07" right="318.77" top="64.99">
            <contents>3. Workshop Breakout Session Reports </contents>
        </group>
        <group bottom="286.19" id="1" left="75.99" right="259.96" top="257.82">
            <contents>Figure 3.4. Harvard physicist Charlie Marcus engages a live audience on quantum computing at the Museum of Science, Boston (courtesy of Museum of Science). </contents>
        </group>
        <group bottom="440.9" id="2" left="75.99" right="433.91" top="96.76">
            <contents>But young people selecting careers must get their own ﬁrst-hand experience, rather than rely upon aggregate statistics assembled by experts. For example, NSF’s Research Experiences for Undergraduates program involves college students in cutting-edge research, and this could include studies of the societal impacts of nanotechnology. Research and development efforts to shape and apply the best methods for communicating nanoscale research and its potential beneﬁts and related public policy and ethics issues should be encouraged. Public audiences could be engaged on an ongoing basis through high quality television, radio, and multimedia. Science museum partnerships with nanoscale research centers could bring public audiences and school groups in contact with future technologies and careers through guest researcher talks (see Figure 3.4), forums, and exhibits addressing challenges at the frontiers of research. The impact of these programs and the quality of the information disseminated should be systematically evaluated. This kind of information should be directed toward user queries. There are many publics, not just one public, and information needs to be presented to stakeholder groups in a form that these groups can understand. This will encourage the formation of creoles for public engagement. For example, is information desired about a speciﬁc technological area, such as nanoelectronics, nanobio, or nanomaterials; or is information required about speciﬁc application areas, such as computation, medicine, or environmental impact; or is information desired about the economic aspects or public policy? In engaging publics, demographic information about the engaged would be desirable, such as age, education level, and major ﬁelds of expertise. It also would be useful to know why the information is sought. Such informational systems are, of course, very difﬁcult to construct and would need to evolve over the years. </contents>
        </group>
        <group bottom="459.9" id="3" left="75.99" right="284.14" top="455.61">
            <contents>Research, Education, and Infrastructure Development </contents>
        </group>
        <group bottom="594.83" id="4" left="75.99" right="433.88" top="474.56">
            <contents>Because technology is changing at an ever-increasing pace, it is essential that “students” of all ages learn more than just basic skills and knowledge. They must also learn “how to learn” as a way of continuously adapting to a changing world. In elementary and secondary education, for example, this could involve greater participation in the learning process using problem-based challenges (“design a better X”), science fairs, role-playing games and model simulations. For the adult learner, regardless of the level of formal education already obtained, a variety of “continuing education” opportunities need to be made available by educational institutions, industries, and labor organizations. Worker transition programs should be framed as opportunities to get in on the ground ﬂoor of a growing ﬁeld. These programs should include working with different equipment to ensure transfer of tacit as well as explicit knowledge [8]. There also should be a focus on learning the emerging creoles that develop in these new areas. </contents>
        </group>
        <group bottom="644.87" id="5" left="185.87" right="313.42" top="641.01">
            <contents>Nanotechnology: Societal Implications </contents>
        </group>
        <group bottom="645.81" id="6" left="424.86" right="433.85" top="641.57">
            <contents>91 </contents>
        </group>
    </page>
</document>
