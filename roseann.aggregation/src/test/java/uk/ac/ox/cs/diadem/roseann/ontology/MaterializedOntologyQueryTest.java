/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.ontology;

import java.io.File;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;

public class MaterializedOntologyQueryTest {

	@Before
	public void setUp(){
		ConfigurationFacility.getConfiguration();
		//Set the ontology folder to a local resource file
		MaterializedOntologyQuery.setMaterializedOntologyFolderFilePath("materializedOntologySample");
	}

	/**
	 * Test to check that the functionalities to set the location of the materialized ontology folder works fine
	 */
	@Test
	public void testSetOntologyFolder(){
		//test that the current folder is corrent. If it is, each method should not trhow an exception
		Set<String> supertypes = MaterializedOntologyQuery.getAllSupertypes("type");
		//if the type does not exist in the ontology teh supertypes are null
		Assert.assertNull(supertypes);
		//set the folder, rather than the local resource path
		MaterializedOntologyQuery.setOntologyMaterializedFolder(new File("src/test/resources/uk/ac/ox/cs" +
				"/diadem/roseann/ontology/materializedOntologySample"));
		//check that everything goes fine
		supertypes = MaterializedOntologyQuery.getAllSupertypes("type");
		//set an unknown ontology folder path and check that an exception is thrown
		try{
			MaterializedOntologyQuery.setMaterializedOntologyFolderFilePath("unknown");
			Assert.fail("An exception has to be thrown id the onoltogy folder path specified does not exist");
		}
		catch(OntologyException e){
			//everyting fine here
		}
		//check that an exception is thrown when setting a folder that does not exist
		try{
			MaterializedOntologyQuery.setOntologyMaterializedFolder(new File("unknown"));
			Assert.fail("An exception has to be thrown id the onoltogy folder specified does not exist");
		}
		catch(OntologyException e){
			//everyting fine here
		}
	}

	/**
	 * Test to check if the ontology materialized methods work as expected
	 */
	@Test
	public void testGetOntologyContraints(){
		String topConcept = MaterializedOntologyQuery.getTopConcept();
		Assert.assertEquals(topConcept, "top");
		//check the supertypes
		Set<String> returnedTypes = MaterializedOntologyQuery.getAllSupertypes("city");
		Assert.assertTrue(returnedTypes.size()==3);
		Assert.assertTrue(returnedTypes.contains("place"));
		Assert.assertTrue(returnedTypes.contains("city"));
		Assert.assertTrue(returnedTypes.contains("top"));
		//check that null is returned when the supertypes is unknwon
		returnedTypes = MaterializedOntologyQuery.getAllSupertypes("unknown");
		Assert.assertNull(returnedTypes);
		//check disjoint types without inference
		returnedTypes = MaterializedOntologyQuery.getDisjointClasses("populatedPlace", 
				false);
		Assert.assertTrue(returnedTypes.size()==1);
		Assert.assertTrue(returnedTypes.contains("city"));
		//check disjoint types with inference
		returnedTypes = MaterializedOntologyQuery.getDisjointClasses("populatedPlace", 
				true);
		Assert.assertTrue(returnedTypes.size()==2);
		Assert.assertTrue(returnedTypes.contains("city"));
		Assert.assertTrue(returnedTypes.contains("person"));
		//check null is returned when the concept is unknown
		Assert.assertNull(MaterializedOntologyQuery.getDisjointClasses("unknown", 
				true));
		//check the top concept has no disjoint classes
		returnedTypes = MaterializedOntologyQuery.getDisjointClasses("top", true);
		Assert.assertTrue(returnedTypes.size()==0);
		//check that two types are disjoint
		Assert.assertTrue(MaterializedOntologyQuery.areDisjoint("country", "person"));
		Assert.assertFalse(MaterializedOntologyQuery.areDisjoint("country", "place"));
		//check that when of the two types is unknown, null is returned
		Assert.assertNull(MaterializedOntologyQuery.areDisjoint("person", "unknown"));
		//check the annotator super class knowledge
		returnedTypes = MaterializedOntologyQuery.getSuperclassKnowledge("country");
		Assert.assertTrue(returnedTypes.size()==3);
		Assert.assertTrue(returnedTypes.contains("annotator2"));
		Assert.assertTrue(returnedTypes.contains("annotator3"));
		Assert.assertTrue(returnedTypes.contains("annotator4"));
		//check that null is returned when the type is unknown
		Assert.assertNull(MaterializedOntologyQuery.getSuperclassKnowledge("unknown"));

	}

}
