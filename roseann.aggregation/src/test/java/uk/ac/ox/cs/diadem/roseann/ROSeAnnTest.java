/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.Memm;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.preprocessing.NaivePreprocessing;
import uk.ac.ox.cs.diadem.roseann.api.RoseAnnException;
import uk.ac.ox.cs.diadem.roseann.api.aggregation.AbstractAggregator;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict.ConflictType;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.OmissionConflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.AnnotatedDocumentModel;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web.AnnotatedHTMLModel;
import uk.ac.ox.cs.diadem.roseann.gui.ROSeAnnGUI;
import uk.ac.ox.cs.diadem.roseann.gui.control.MainpageBusiness;
import uk.ac.ox.cs.diadem.roseann.ontology.MaterializedOntologyQuery;
import uk.ac.ox.cs.diadem.roseann.ontology.OntologyException;
import uk.ac.ox.cs.diadem.roseann.util.FileAdapter;
import uk.ac.ox.cs.diadem.webapi.dom.DOMElement;

public class ROSeAnnTest {
  Set<Annotation> conflictingAnnotations;
  Annotation a1;
  Annotation a2;
  Annotation a3;
  Annotation a4;
  ROSeAnn roseann;
  Set<String> involvedAnnotators;

  @Before public void setUp() throws ConfigurationException {
    ConfigurationFacility.getConfiguration();
    // initialize ROSeAnn with a specific configuration file
    final XMLConfiguration conf = new XMLConfiguration(new File("src/test/resources"
        + "/uk/ac/ox/cs/diadem/roseann/Configuration.xml"));
    final String textAnnotatorConfFile = "src/test/resources/uk/ac/ox/cs/diadem/roseann/"
        + "TextannotatorConfiguration.xml";
    roseann = new ROSeAnn(conf, textAnnotatorConfFile);
    conflictingAnnotations = new HashSet<Annotation>();
    a1 = new EntityAnnotation("id_1", "city", 0, 10, "annotator1", null, 0.0, AnnotationClass.INSTANCE);
    a2 = new EntityAnnotation("id_2", "place", 0, 10, "annotator3", null, 0.0, AnnotationClass.INSTANCE);
    a3 = new EntityAnnotation("id_3", "country", 0, 10, "annotator2", null, 0.0, AnnotationClass.INSTANCE);
    a4 = new EntityAnnotation("id_4", "person", 0, 10, "annotator4", null, 0.0, AnnotationClass.INSTANCE);
    conflictingAnnotations.add(a1);
    conflictingAnnotations.add(a2);
    conflictingAnnotations.add(a3);
    conflictingAnnotations.add(a4);

    involvedAnnotators = new HashSet<String>();
    involvedAnnotators.add("annotator1");
    involvedAnnotators.add("annotator2");
    involvedAnnotators.add("annotator3");
    involvedAnnotators.add("annotator4");

  }

  /**
   * Test to check that when no configuration file are specified the default one is used
   */
  @Test public void testDefaultConfigurationFile() {
    // initialize ROSeAnn with default configuration file
    final ROSeAnn roseann = new ROSeAnn();
    Assert.assertNotNull(roseann);
    // check that aggregation algorithms are specified
    final Set<AbstractAggregator> aggregators = roseann.getAggregators();
    Assert.assertNotNull(aggregators);
    Assert.assertTrue(aggregators.size() > 0);
    // the timeout is null until roseann is not invoked
    Assert.assertNull(roseann.getTimeout());
    // after the first invocation, the timeout is set
    roseann.setAnnotators(new HashSet<Annotator>());
    roseann.annotateEntityPlainText("Text", false);
    Assert.assertNotNull(roseann.getTimeout());
  }

  /**
   * Test to check that parameters are read from a specific configuration file when used the specialized constructor
   * 
   * @throws ConfigurationException
   */
  @Test public void textCustomizedConfigurationFile() throws ConfigurationException {
    final XMLConfiguration conf = new XMLConfiguration(new File("src/test/resources"
        + "/uk/ac/ox/cs/diadem/roseann/Configuration.xml"));
    final String textAnnotatorConfFile = "src/test/resources/uk/ac/ox/cs/diadem/roseann/"
        + "TextannotatorConfiguration.xml";
    ROSeAnn roseann = new ROSeAnn(conf, textAnnotatorConfFile);
    Assert.assertNotNull(roseann);
    // check that only one aggregator has been initialized and set to the aggregators pool
    final Set<AbstractAggregator> aggregators = roseann.getAggregators();
    Assert.assertNotNull(aggregators);
    Assert.assertTrue(aggregators.size() == 1);
    // check the timeout has been correctly set to one
    Assert.assertNotNull(roseann.getTimeout());
    Assert.assertTrue(roseann.getTimeout() == 1000);
    // check that if the text annotator configuration file does not exist no exceptions are thrown
    roseann = new ROSeAnn(conf, "notExists");
    // check that timeout is set anyway to the default value, which is 0
    Assert.assertTrue(roseann.getTimeout() == 0);
  }

  /**
   * Test to check that when the ontology materialized folder specified in the configuration file deos not exis an
   * exception is thrown
   */
  @SuppressWarnings("unused") @Test(expected = OntologyException.class) public void textWrongConfigurationFile()
      throws ConfigurationException {
    final XMLConfiguration conf = new XMLConfiguration(new File("src/test/resources"
        + "/uk/ac/ox/cs/diadem/roseann/WrongConfiguration.xml"));
    final ROSeAnn roseann = new ROSeAnn(conf, "dontCare");
    Assert.fail("An exception is expected when the configuration roseann specifies a wrong materialized folder file");
  }

  /**
   * Normal test to check whether the expected conflicts are calculated among annotations with same start-end
   */
  @Test public void testOmissionAndLogicalConflict() {

    // set the ontology file
    MaterializedOntologyQuery.setOntologyMaterializedFolder(new File(
        "src/test/resources/uk/ac/ox/cs/diadem/roseann/ontology/materializedOntologySample"));

    final Set<Conflict> conflicts = roseann.calculateConflicts(conflictingAnnotations, involvedAnnotators);
    Assert.assertNotNull(conflicts);
    Assert.assertTrue(conflicts.size() == 5);

    final Set<Set<Annotation>> expectedLogicalConflictAnnotation = new HashSet<Set<Annotation>>();
    final Set<Annotation> conflict1 = new HashSet<Annotation>();
    conflict1.add(a1);
    conflict1.add(a4);
    expectedLogicalConflictAnnotation.add(conflict1);
    final Set<Annotation> conflict2 = new HashSet<Annotation>();
    conflict2.add(a2);
    conflict2.add(a4);
    expectedLogicalConflictAnnotation.add(conflict2);
    final Set<Annotation> conflict3 = new HashSet<Annotation>();
    conflict3.add(a3);
    conflict3.add(a4);
    expectedLogicalConflictAnnotation.add(conflict3);
    final Set<Annotation> conflict4 = new HashSet<Annotation>();
    conflict4.add(a1);
    conflict4.add(a3);
    expectedLogicalConflictAnnotation.add(conflict4);
    // check that we have one omission conflict and 4 logical conflict
    final Set<Set<Annotation>> actualLogicalConflictAnnotation = new HashSet<Set<Annotation>>();
    boolean thereIsOmission = false;
    for (final Conflict conflict : conflicts) {
      if (conflict.getType().equals(ConflictType.OMISSION)) {
        final Set<String> agreedAnnotators = new HashSet<String>();
        agreedAnnotators.add("annotator2");
        agreedAnnotators.add("annotator3");
        final Set<Annotation> involvedAnnotation = new HashSet<Annotation>();
        involvedAnnotation.add(a3);
        involvedAnnotation.add(a2);
        final Set<String> omittedAnnotators = new HashSet<String>();
        omittedAnnotators.add("annotator4");

        Assert.assertEquals(conflict.getId(), "omission_conflict_0");
        Assert.assertTrue(conflict.getStart() == 0);
        Assert.assertTrue(conflict.getEnd() == 10);
        Assert.assertEquals(((OmissionConflict) conflict).getId(), "omission_conflict_0");
        Assert.assertEquals(((OmissionConflict) conflict).getInvolvedAnnotations(), involvedAnnotation);
        Assert.assertEquals(((OmissionConflict) conflict).getAgreedAnnotators(), agreedAnnotators);
        Assert.assertEquals(((OmissionConflict) conflict).getOmittedAnnotators(), omittedAnnotators);
        Assert.assertEquals(((OmissionConflict) conflict).getConcept(), "country");
        thereIsOmission = true;
      } else {
        actualLogicalConflictAnnotation.add(conflict.getInvolvedAnnotations());
      }
    }
    Assert.assertTrue(thereIsOmission);
    Assert.assertEquals(actualLogicalConflictAnnotation, expectedLogicalConflictAnnotation);

  }

  /**
   * Test to check whether no conflicts are retrieved if there are no conflicts among the annotations with same
   * start-end
   */
  @Test public void testNoConflict() {
    // set the ontology file
    MaterializedOntologyQuery.setOntologyMaterializedFolder(new File(
        "src/test/resources/uk/ac/ox/cs/diadem/roseann/ontology/materializedOntologySample"));
    conflictingAnnotations.remove(a4);
    conflictingAnnotations.remove(a3);
    final Set<Conflict> conflicts = roseann.calculateConflicts(conflictingAnnotations, involvedAnnotators);
    Assert.assertNotNull(conflicts);
    Assert.assertTrue(conflicts.size() == 0);
  }

  /**
   * Test to check whether only logical conflicts are retrieved
   */
  @Test public void testOnlyLogicalConflict() {

    // set the ontology file
    MaterializedOntologyQuery.setOntologyMaterializedFolder(new File(
        "src/test/resources/uk/ac/ox/cs/diadem/roseann/ontology/materializedOntologySample"));
    conflictingAnnotations.remove(a1);
    conflictingAnnotations.remove(a3);
    final Set<Conflict> conflicts = roseann.calculateConflicts(conflictingAnnotations, involvedAnnotators);
    Assert.assertNotNull(conflicts);
    Assert.assertTrue(conflicts.size() == 1);

    final Conflict conflict = conflicts.iterator().next();
    Assert.assertEquals(ConflictType.LOGICAL, conflict.getType());
    Assert.assertTrue(conflict.getInvolvedAnnotations().contains(a2));
    Assert.assertTrue(conflict.getInvolvedAnnotations().contains(a4));
  }

  /**
   * Test to check whether only omission conflicts are retrieved
   */
  @Test public void testOnlyOmissionConflict() {

    // set the ontology file
    MaterializedOntologyQuery.setOntologyMaterializedFolder(new File(
        "src/test/resources/uk/ac/ox/cs/diadem/roseann/ontology/materializedOntologySample"));
    conflictingAnnotations.remove(a1);
    conflictingAnnotations.remove(a4);
    final Set<Conflict> conflicts = roseann.calculateConflicts(conflictingAnnotations, involvedAnnotators);
    Assert.assertNotNull(conflicts);
    Assert.assertTrue(conflicts.size() == 1);
    final Conflict conflict = conflicts.iterator().next();
    Assert.assertEquals(ConflictType.OMISSION, conflict.getType());
    Assert.assertTrue(conflict.getInvolvedAnnotations().contains(a2));
    Assert.assertTrue(conflict.getInvolvedAnnotations().contains(a3));
    Assert.assertTrue(((OmissionConflict) conflict).getAgreedAnnotators().size() == 2);
    Assert.assertTrue(((OmissionConflict) conflict).getAgreedAnnotators().contains("annotator2"));
    Assert.assertTrue(((OmissionConflict) conflict).getAgreedAnnotators().contains("annotator3"));
    Assert.assertTrue(((OmissionConflict) conflict).getOmittedAnnotators().size() == 1);
    Assert.assertTrue(((OmissionConflict) conflict).getOmittedAnnotators().contains("annotator4"));
    Assert.assertEquals(((OmissionConflict) conflict).getConcept(), "country");

  }

  /**
   * Test to check whether annotations with different start-end are treated indepndently when computing the conflicts
   */
  @Test public void testDifferentSpanConflict() {
    // set the ontology file
    MaterializedOntologyQuery.setOntologyMaterializedFolder(new File(
        "src/test/resources/uk/ac/ox/cs/diadem/roseann/ontology/materializedOntologySample"));
    conflictingAnnotations.remove(a3);
    a1.setStart(1);
    final Set<Conflict> conflicts = roseann.calculateConflicts(conflictingAnnotations, involvedAnnotators);
    Assert.assertNotNull(conflicts);
    Assert.assertTrue(conflicts.size() == 2);

    boolean thereIsOmission = false;
    boolean thereIsLogical = false;
    for (final Conflict conflict : conflicts) {
      if (conflict.getType().equals(ConflictType.OMISSION)) {
        Assert.assertTrue(conflict.getInvolvedAnnotations().contains(a1));
        Assert.assertTrue(((OmissionConflict) conflict).getAgreedAnnotators().size() == 1);
        Assert.assertTrue(((OmissionConflict) conflict).getAgreedAnnotators().contains("annotator1"));
        Assert.assertTrue(((OmissionConflict) conflict).getOmittedAnnotators().size() == 1);
        Assert.assertTrue(((OmissionConflict) conflict).getOmittedAnnotators().contains("annotator3"));
        Assert.assertEquals(((OmissionConflict) conflict).getConcept(), "city");
        thereIsOmission = true;

      } else {
        Assert.assertEquals(ConflictType.LOGICAL, conflict.getType());
        Assert.assertTrue(conflict.getInvolvedAnnotations().contains(a2));
        Assert.assertTrue(conflict.getInvolvedAnnotations().contains(a4));
        thereIsLogical = true;
      }

    }
    Assert.assertTrue(thereIsOmission && thereIsLogical);
  }

  /**
   * Test to trigger the GUI programatically
   */
  @Test public void testVisualizeText() {
    for (int i = 0; i < 2; i++) {
      final String testingFile = "src/test/resources/uk/ac/ox/cs/diadem/roseann/aggregation/memmagg/predict/2288newsML.txt";
      System.out.println("Processing document " + testingFile);
      final String annotatedFile = "src/test/resources/uk/ac/ox/cs/diadem/roseann/aggregation/memmagg/predict/2288newsML_"
          + "XXX.xml";
      final File testFile = new File(testingFile);

      final String[] mockAnnotators = { "alchemyAPI", "dbpediaSpotlight", "extractiv", "illinoisNer", "lupedia",
          "openCalais", "saplo", "stanfordNer", "wikimeta", "yahooAPI", "zemanta" };
      final Memm memm = new Memm();
      memm.setAnnotators(mockAnnotators);
      final Set<Annotation> annotations = new NaivePreprocessing(memm).harvestAnnotation(annotatedFile, testFile);
      final AnnotatedDocumentModel model = new AnnotatedDocumentModel();
      model.setKnownAnnotations(annotations);
      model.setCompletedAnnotators(new HashSet<String>(Arrays.asList(mockAnnotators)));

      final Set<Conflict> conflicts = MainpageBusiness.getRoseAnnInstance().calculateConflicts(
          model.getKnownAnnotations(), model.getCompletedAnnotators());
      final String text = FileAdapter.getPureFileContents(testingFile);
      model.setAnnotatedText(text);
      model.setAllConflicts(conflicts);
      ROSeAnn.visualizeAnnotatedDocument(model);

    }
    Assert.assertEquals(2, ROSeAnnGUI.myMainPanel.documentTabPane.getTabCount());

  }

  /**
   * Verify that everything goes through smoothly when annotation a plain text
   */
  @Test public void testAnnotatePlainText() {
    final String text = "Barack Obama is the new president of United States.";
    // without disambiguation
    AnnotatedDocumentModel model = roseann.annotateEntityPlainText(text, false);
    Assert.assertNotNull(model);
    Assert.assertTrue(model.getAllAnnotations().size() > 0);
    // only stanfordNER and yahooAPI are configured to be run
    Assert.assertTrue(model.getCompletedAnnotators().size() == 2);
    // there are no aggregators
    Assert.assertTrue(model.getCompletedAggregators().size() == 0);
    Assert.assertEquals(model.getAnnotatedText(), text);
    // check that only one aggregator is abilitated
    model = roseann.annotateEntityPlainText(text, true);
    Assert.assertNotNull(model);
    Assert.assertTrue(model.getCompletedAggregators().size() == 1);
    // check that no annotations are produced if the annotators pool is empty
    roseann.setAnnotators(new HashSet<Annotator>());
    model = roseann.annotateEntityPlainText(text, true);
    Assert.assertNotNull(model);
    Assert.assertTrue(model.getAllAnnotations().size() == 0);
    Assert.assertTrue(model.getKnownAnnotations().size() == 0);
    Assert.assertTrue(model.getUnknownAnnotation().size() == 0);
    Assert.assertTrue(model.getAllConflicts().size() == 0);
    Assert.assertTrue(model.getCompletedAnnotators().size() == 0);
    Assert.assertTrue(model.getCompletedAggregators().size() == 0);
  }

  /**
   * Verify that everything goes through smoothly when annotation a pdf file
   */
  @Test public void testAnnotatePdfFile() {
    final File pdfFile = new File("src/test/resources" + "/uk/ac/ox/cs/diadem/roseann/samplePdf.pdf");
    final AnnotatedDocumentModel model = roseann.annotateEntityPDF(pdfFile, true);
    Assert.assertNotNull(model);
    Assert.assertTrue(model.getAllAnnotations().size() > 0);
    // only stanfordNER and yahooAPI API are configured to be run
    Assert.assertEquals(2, model.getCompletedAnnotators().size());
    Assert.assertTrue(model.getCompletedAggregators().size() == 1);
    final String text = "Barack Obama is the new president of United States. 1 ";
    Assert.assertEquals(model.getAnnotatedText(), text);
    // check an exception is thrown when the pdf file does not exists
    try {
      roseann.annotateEntityPDF(new File("notExists"), true);
      Assert.fail("An exception is expected when the pdf file to be annotated does not exist!");
    } catch (final RoseAnnException e) {
      // everything went through fine and an exception has been thrown
    }
    // check an exception is thrown when the pdf file is not a pdf file
    try {
      roseann.annotateEntityPDF(new File("src/test/resources" + "/uk/ac/ox/cs/diadem/roseann/sampleHtml.html"), true);
      Assert.fail("An exception is expected when the pdf file is not a pdf!");
    } catch (final RoseAnnException e) {
      // everything went through fine and an exception has been thrown
    }
  }

  @Test public void testAnnotateHtmlDocument() throws URISyntaxException, IOException {
    final URI htmlPage = new URI("file:" + System.getProperty("user.dir") + "/src/test/resources"
        + "/uk/ac/ox/cs/diadem/roseann/sampleHtml.html");
    final AnnotatedHTMLModel model = roseann.annotateEntityWebPage(htmlPage, true, false);
    // verify the browser is not still alive
    Assert.assertNull(model.getHtmlDoc().getWebBrowser());
    Assert.assertNotNull(model);
    Assert.assertTrue(model.getAllAnnotations().size() > 0);
    // only stanfordNER and yahooAPI are configured to be run
    Assert.assertTrue(model.getCompletedAnnotators().size() == 2);
    Assert.assertTrue(model.getCompletedAggregators().size() == 1);
    final String text = "    Barack Obama is the new president of United States.  ";
    Assert.assertEquals(model.getAnnotatedText(), text);
    // check an exception is thrown when the html file is not a well formed URI

    // check an exception is thrown when the file does not exist
    try {
      roseann.annotateEntityWebPage(new URI("file:notExists"), true, false);
      Assert.fail("An exception is expected when the html document is not a html document!");
    } catch (final RoseAnnException e) {
      // everything went through fine and an exception has been thrown
    }
  }

  /**
   * Test to check the functionalities of an annotated html web page
   * 
   * @throws URISyntaxException
   * @throws IOException
   */
  @Test public void testHtmlFunctionalities() throws URISyntaxException, IOException {
    final URI htmlPage = new URI("file:" + System.getProperty("user.dir") + "/src/test/resources"
        + "/uk/ac/ox/cs/diadem/roseann/sampleHtml.html");
    final AnnotatedHTMLModel model = roseann.annotateEntityWebPage(htmlPage, true, true);
    // check the browser is still alive
    Assert.assertNotNull(model.getHtmlDoc().getWebBrowser());
    final DOMElement root = model.getHtmlDoc().getDOMDocument().getDocumentElement();
    // check that all annotations are contained into the root element
    final Set<Annotation> allAnnotations = model.getAllAnnotations();
    Assert.assertEquals(allAnnotations, model.getAnnotationNodeContained(root));

    // check the same if a specify an xpath rather than a node
    Assert.assertEquals(allAnnotations, model.getAnnotationXpathContained("//html"));

    // check that only some annotations with start-end are returned with a specific xpath
    final Set<Annotation> someAnnotations = model.getAnnotationXpathContained("//html//p/b");
    // check all the annotations have a start-end inclued in [4,16]
    for (final Annotation annotation : someAnnotations) {

      if (annotation.getStart() < 4 || annotation.getEnd() > 16) {
        Assert.fail("All the annotations included in the xpath //html//p/b are expected to have"
            + " a start-end included in [4,16]");
      }
    }

    // check that if the xpath specifies a text node null is returned
    Assert.assertNull(model.getAnnotationXpathContained("//html//p/b/text()"));
    // check that an empty set is returned when the xpath does not identify any annotations
    Assert.assertTrue(model.getAnnotationXpathContained("//html//p/p").size() == 0);

    // shut down the browser
    model.shutDownBrowser();
    // check the browser is null
    Assert.assertNull(model.getHtmlDoc().getWebBrowser());

  }

}
