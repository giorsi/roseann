/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.wr;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;
import uk.ac.ox.cs.diadem.roseann.ontology.MaterializedOntologyQuery;

public class WeightedRepairTest {

	Set<Annotation> conflictingAnnotations;
	Annotation a1;
	Annotation a2;
	Annotation a3;
	Annotation a4;
	Annotation a5;
	WeightedRepair wr;

	@Before
	public void setUp(){
		ConfigurationFacility.getConfiguration();
		wr=new WeightedRepair();
		conflictingAnnotations=new HashSet<Annotation>();
		a1=new EntityAnnotation("id_1", "city", 0, 10, "annotator1", null, 0.0,AnnotationClass.INSTANCE);
		a2=new EntityAnnotation("id_2", "person", 0, 10, "annotator2", null, 0.,AnnotationClass.INSTANCE);
		a3=new EntityAnnotation("id_3", "artist", 0, 10, "annotator3", null, 0.,AnnotationClass.INSTANCE);
		a4=new EntityAnnotation("id_4", "artist", 0, 10, "annotator4", null, 0.,AnnotationClass.INSTANCE);
		a5=new EntityAnnotation("id_5", "contact", 0, 10, "annotator5", null, 0.,AnnotationClass.INSTANCE);
		conflictingAnnotations.add(a1);
		conflictingAnnotations.add(a2);
		conflictingAnnotations.add(a3);
		conflictingAnnotations.add(a4);
		conflictingAnnotations.add(a5);
		
		//set the ontology file
		MaterializedOntologyQuery.setOntologyMaterializedFolder(new File(
				"src/test/resources/uk/ac/ox/cs/diadem/roseann/aggregation/wr/ontology/materializedOntology"));
	}

	@Test
	public void testNormalReconcile(){
		final Set<Annotation> reconciledAnnotations = wr.reconcile(null, conflictingAnnotations, null);
		Assert.assertTrue(reconciledAnnotations.size()==1);
		Annotation onlyAnnotation=reconciledAnnotations.iterator().next();
		Assert.assertEquals(onlyAnnotation.getConcept(), "artist");
		Assert.assertEquals(onlyAnnotation.getOriginAnnotator(), wr.getAggregatorName());
		Assert.assertEquals(onlyAnnotation.getId(), wr.getAggregatorName()+"_0");
		Assert.assertEquals(onlyAnnotation.getAnnotationSource(), "annotator3");
	}

	/**
	 * Test where there is no enough evidence to choose correct types among conflicting annotations,
	 * return the top concept
	 */
	@Test
	public void testNoResultReconcile(){
		a4.setConcept("organization");
		final Set<Annotation> reconciledAnnotations = wr.reconcile(null, conflictingAnnotations, null);
		Assert.assertTrue(reconciledAnnotations.size()==1);
		Annotation reconciledAnnotation=reconciledAnnotations.iterator().next();
		Assert.assertEquals(reconciledAnnotation.getId(),"wr_0");
		Assert.assertEquals(reconciledAnnotation.getConcept(),"top");
		Assert.assertTrue(reconciledAnnotation.getStart()==0);
		Assert.assertTrue(reconciledAnnotation.getEnd()==10);
		Assert.assertEquals(reconciledAnnotation.getOriginAnnotator(),"wr");
		Assert.assertTrue(reconciledAnnotation.getAnnotationSource().contains("annotator1"));
		Assert.assertTrue(reconciledAnnotation.getAnnotationSource().contains("annotator2"));
		Assert.assertTrue(reconciledAnnotation.getAnnotationSource().contains("annotator3"));
		Assert.assertTrue(reconciledAnnotation.getAnnotationSource().contains("annotator4"));
		Assert.assertTrue(reconciledAnnotation.getAnnotationSource().contains("annotator5"));
		Assert.assertTrue(reconciledAnnotation.getConfidence()==0.);	
	}

	/**
	 * Test where we have 2 annotations city and 2 annotations artist, with artist and city in conflict.
	 * Both of the concept have the same atomic score, therefore the first alphabetically will be chosen 
	 */
	@Test
	public void testZeroWeightConcept(){
		conflictingAnnotations.remove(a5);
		a2.setConcept("city");
		final Set<Annotation> reconciledAnnotations = wr.reconcile(null, conflictingAnnotations, null);
		Assert.assertTrue(reconciledAnnotations.size()==1);
		Annotation onlyAnnotation=reconciledAnnotations.iterator().next();
		Assert.assertEquals(onlyAnnotation.getConcept(), "artist");
		Assert.assertEquals(onlyAnnotation.getOriginAnnotator(), wr.getAggregatorName());
		Assert.assertEquals(onlyAnnotation.getId(), wr.getAggregatorName()+"_0");
		Assert.assertEquals(onlyAnnotation.getAnnotationSource(), "annotator3");

	}

	/**
	 * Test where we have all the leaves annotations with atomic score less than 0,
	 * check that the annotation returned is the least common ancestor
	 */
	@Test
	public void testLeastCommonAncestor(){
		a2.setConcept("city");
		a3.setConcept("country");
		a4.setConcept("country");
		final Set<Annotation> reconciledAnnotations = wr.reconcile(null, conflictingAnnotations, null);
		Assert.assertTrue(reconciledAnnotations.size()==1);
		Annotation onlyAnnotation=reconciledAnnotations.iterator().next();
		Assert.assertEquals(onlyAnnotation.getConcept(), "place");
		Assert.assertEquals(onlyAnnotation.getOriginAnnotator(), wr.getAggregatorName());
		Assert.assertEquals(onlyAnnotation.getId(), wr.getAggregatorName()+"_0");
		Assert.assertTrue(onlyAnnotation.getAnnotationSource().contains("annotator1"));
		Assert.assertTrue(onlyAnnotation.getAnnotationSource().contains("annotator2"));
		Assert.assertTrue(onlyAnnotation.getAnnotationSource().contains("annotator3"));
		Assert.assertTrue(onlyAnnotation.getAnnotationSource().contains("annotator4"));

	}
	
	/**
	 * We expect that span that contains annotations with unknown concept are not reconciled
	 */
	@Test
	public void testUnknownConcept(){
		Annotation a5 = new EntityAnnotation("id_5", "city", 10, 20, "annotator1", null, 0.0,AnnotationClass.INSTANCE);
		Annotation a6 = new EntityAnnotation("id_6", "unknown", 10, 20, "annotator2", null, 0.0,AnnotationClass.INSTANCE);
		conflictingAnnotations.add(a5);
		conflictingAnnotations.add(a6);
		Set<Annotation> reconciledAnnotations = 
				wr.reconcile(null, conflictingAnnotations, null);
		Assert.assertTrue(reconciledAnnotations.size()==1);
		Annotation onlyAnnotation=reconciledAnnotations.iterator().next();
		Assert.assertEquals(onlyAnnotation.getConcept(), "artist");
		Assert.assertEquals(onlyAnnotation.getOriginAnnotator(), wr.getAggregatorName());
		Assert.assertEquals(onlyAnnotation.getId(), wr.getAggregatorName()+"_0");
		Assert.assertEquals(onlyAnnotation.getAnnotationSource(), "annotator3");
	}
	
	/**
	 * Test to check that different spanning annotations are treated separately
	 */
	@Test
	public void testDifferentSpanningAnnotation(){
		Annotation a6=new EntityAnnotation("id_6", "company", 10, 20, "annotator1", null, 0.,AnnotationClass.INSTANCE);
		Annotation a7=new EntityAnnotation("id_7", "commercialOrg", 10, 20, "annotator2", null, 0.,AnnotationClass.INSTANCE);
		Annotation a8=new EntityAnnotation("id_8", "artist", 10, 20, "annotator3", null, 0.,AnnotationClass.INSTANCE);
		conflictingAnnotations.add(a6);
		conflictingAnnotations.add(a7);
		conflictingAnnotations.add(a8);
		final Set<Annotation> reconciledAnnotations = wr.reconcile(null, conflictingAnnotations, null);
		Assert.assertTrue(reconciledAnnotations.size()==2);
		Annotation reconciledAnnotation1=new EntityAnnotation("wr_0", "artist", 0, 10, "wr", "annotator3", 0.,AnnotationClass.INSTANCE);
		Annotation reconciledAnnotation2=new EntityAnnotation("wr_1", "commercialOrg", 10, 20, "wr", "annotator2", 0.,AnnotationClass.INSTANCE);
		Assert.assertTrue(reconciledAnnotations.contains(reconciledAnnotation1));
		Assert.assertTrue(reconciledAnnotations.contains(reconciledAnnotation2));
	}
	
	/**
	 * Test to check that different connected component of the conflicting graph are treated separately
	 */
	@Test
	public void testDifferentConflictingAnnotation(){
		Annotation a6=new EntityAnnotation("id_6", "other_city", 0, 10, "annotator1", null, 0.,AnnotationClass.INSTANCE);
		Annotation a7=new EntityAnnotation("id_7", "other_country", 0, 10, "annotator2", null, 0.,AnnotationClass.INSTANCE);
		Annotation a8=new EntityAnnotation("id_8", "other_town", 0, 10, "annotator3", null, 0.,AnnotationClass.INSTANCE);
		conflictingAnnotations.add(a6);
		conflictingAnnotations.add(a7);
		conflictingAnnotations.add(a8);
		final Set<Annotation> reconciledAnnotations = wr.reconcile(null, conflictingAnnotations, null);
		Assert.assertTrue(reconciledAnnotations.size()==2);
		Annotation reconciledAnnotation1=new EntityAnnotation("wr_0", "artist", 0, 10, "wr", "annotator3", 0.,AnnotationClass.INSTANCE);
		Assert.assertTrue(reconciledAnnotations.contains(reconciledAnnotation1));
		reconciledAnnotations.remove(reconciledAnnotation1);
		Annotation nextReconciledAnnotation=reconciledAnnotations.iterator().next();
		Assert.assertEquals(nextReconciledAnnotation.getId(),"wr_1");
		Assert.assertEquals(nextReconciledAnnotation.getConcept(),"other_place");
		Assert.assertTrue(nextReconciledAnnotation.getStart()==0);
		Assert.assertTrue(nextReconciledAnnotation.getEnd()==10);
		Assert.assertEquals(nextReconciledAnnotation.getOriginAnnotator(),"wr");
		Assert.assertTrue(nextReconciledAnnotation.getAnnotationSource().contains("annotator1"));
		Assert.assertTrue(nextReconciledAnnotation.getAnnotationSource().contains("annotator2"));
		Assert.assertTrue(nextReconciledAnnotation.getAnnotationSource().contains("annotator3"));
		Assert.assertTrue(nextReconciledAnnotation.getConfidence()==0.);	
	}

}
