/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.preprocessing.NaivePreprocessing;
import uk.ac.ox.cs.diadem.roseann.util.FileAdapter;


public class MemmTest {
	
	private Memm memm;
	private Set<Annotation> annotations;
	private Set<String> annotators;
	
	@Before
	public void setUp(){
		String testingFile = "src/test/resources/uk/ac/ox/cs/diadem/roseann/aggregation/memmagg/predict/2288newsML.txt";
		System.out.println("Processing document " + testingFile);
		String annotatedFile = "src/test/resources/uk/ac/ox/cs/diadem/roseann/aggregation/memmagg/predict/2288newsML_" + "XXX.xml";
		File testFile = new File(testingFile);
		memm = new Memm();
		final String[] mockAnnotators  = { "alchemyAPI",  "dbpediaSpotlight","extractiv","illinoisNer","lupedia",
			"openCalais", "saplo", "stanfordNer","wikimeta","yahooAPI","zemanta"};
		memm.setAnnotators(mockAnnotators);
		
			annotations = new NaivePreprocessing(memm).harvestAnnotation(
					annotatedFile, testFile);
			annotators = new HashSet<String>(
					Arrays.asList(mockAnnotators));
		
	}

	/**
	 * Test to check the reconciling task
	 */
	@Test
	public void testReconcile() {
		Assert.assertNotNull(memm.getMyParams().getPoolName());
		String testingFile = "src/test/resources/uk/ac/ox/cs/diadem/roseann/aggregation/memmagg/predict/2288newsML.txt";
		Set<Annotation> res = memm.reconcile(
				FileAdapter.getPureFileContents(testingFile), annotations,
				annotators);
		
		Assert.assertTrue(res.size()>0);
		
	}
	/**
	 * Test to check the training task
	 */
	@Test
	public void testTraining() {
		
		String trainingDataFolder = "src/test/resources/uk/ac/ox/cs/diadem/roseann/aggregation/memmagg/training/dat";
		String trainedModelOutFolder = "src/test/resources/uk/ac/ox/cs/diadem/roseann/aggregation/memmagg/training";
		memm.getMyParams().setIterationNum(10);
		memm.train(trainingDataFolder, trainedModelOutFolder);
		
		File outputFolder = new File(trainedModelOutFolder+"/model");
		int modelNum = (int)outputFolder.list().length;
		
		Assert.assertEquals(1, modelNum);
		
	}
	

}
