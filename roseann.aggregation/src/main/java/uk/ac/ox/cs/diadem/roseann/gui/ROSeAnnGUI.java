/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui;

import java.awt.*;

import javax.swing.*;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.roseann.gui.control.BusinessInterface;
import uk.ac.ox.cs.diadem.roseann.gui.control.MainpageBusiness;
import uk.ac.ox.cs.diadem.roseann.gui.forms.About;
import uk.ac.ox.cs.diadem.roseann.gui.forms.MainPageNew;

/**
 * Entry class of the application
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) - Department of Computer Science - University of
 *         Oxford
 */

public class ROSeAnnGUI {

	public static MainPageNew myMainPanel;

	/**
	 * Create the GUI and show it. For thread safety, this method should be invoked from the event-dispatching thread.
	 */
	public static void launch() {
		
		ConfigurationFacility.getConfiguration();

		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			// UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
			// set the desired time that you want a tooltip to display
			javax.swing.ToolTipManager.sharedInstance().setDismissDelay(100000);
		} catch (final Exception unused) {
			; // Ignore exception because we can't do anything. Will use default.
		}
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				myMainPanel = new MainPageNew();
				myMainPanel.setPreferredSize(new Dimension(1000, 800));
				// myMainPanel.annotatedDocViewTxtpnl.setPreferredSize(new Dimension(400,300));
				final BusinessInterface mainBusiness = new MainpageBusiness(myMainPanel);
				// further decorate
				mainBusiness.initialize();

				final JFrame frame = new JFrame("ROSeAnn: Reconciling Opinions of Semantic Annotators");
				frame.setIconImage(new ImageIcon(About.class.getResource("icons/roseann-logo.png")).getImage());

				frame.getContentPane().add(new JScrollPane(myMainPanel), BorderLayout.CENTER);
				frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
				frame.pack();

				// Center the frame

				final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				final Dimension frameSize = frame.getSize();
				if (frameSize.height > screenSize.height) {
					frameSize.height = screenSize.height;
				}
				if (frameSize.width > screenSize.width) {
					frameSize.width = screenSize.width;
				}
				frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
				frame.setVisible(true);
			}
		});
	}
	
	public static void main(String[] args){
		ROSeAnnGUI.launch();
	}

}
