/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.swingComponent.pdfpane;

import java.awt.Point;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.pdfbox.pdfviewer.PDFPagePanel;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.AnnotatedPDFModel;
import uk.ac.ox.cs.diadem.roseann.gui.forms.*;
import uk.ac.ox.cs.diadem.roseann.gui.swingComponent.pdfpane.PDFSpanUtil.DoubleRectangle;
import uk.ac.ox.cs.diadem.roseann.ontology.MaterializedOntologyQuery;
import uk.ac.ox.cs.diadem.roseann.util.Constant.AnnotatorNameAgg;
import uk.ac.ox.cs.diadem.roseann.util.Constant.AnnotatorNameInd;

@SuppressWarnings("serial")
public class DiademPDFPagePanel extends PDFPagePanel {

	
	private AnnotatedPDFModel annoDocModel;
	// record handles of annotator tags which are alive
	private Map<Integer, Map<DoubleRectangle, Set<String>>> spanAnnotators;
	// record handles of contept tags which are alive
	private Map<Integer, Map<DoubleRectangle, Set<String>>> spanConcepts;
	// current page number
	private int curPageNum;

	public DiademPDFPagePanel() throws IOException {
		super();
	}

	@Override
	public String getToolTipText(MouseEvent e) {

		return showToolTipForSnippet(e.getPoint());

	}

	private String showToolTipForSnippet(Point p) {
		String myHTML = "<html>";
		Map<DoubleRectangle, Set<String>> annotatorSets = null;
		Map<DoubleRectangle, Set<String>> conceptSets = null;
		if (spanAnnotators != null) {
			annotatorSets = spanAnnotators.get(curPageNum);
		}
		if (spanConcepts != null) {
			conceptSets = spanConcepts.get(curPageNum);
		}

		Set<DoubleRectangle> keys = new HashSet<DoubleRectangle>();
		if (annotatorSets != null) {
			keys.addAll(annotatorSets.keySet());
		}
		if (conceptSets != null) {
			keys.addAll(conceptSets.keySet());
		}
		Map<DoubleRectangle, String> tooltipContentIndOp = new HashMap<DoubleRectangle, String>();
		Map<DoubleRectangle, String> tooltipContentAggOp = new HashMap<DoubleRectangle, String>();
		Map<DoubleRectangle, String> tooltipText = new HashMap<DoubleRectangle, String>();
		for (DoubleRectangle oneKey : keys) {
			if (oneKey.containts(p)) {
				constructTooltipForIndividualSnip(oneKey, tooltipContentIndOp,
						tooltipContentAggOp, tooltipText);
			}
		}

		// ignore mouse which is hovered on plain spans without any annotation.
		if (tooltipContentAggOp.size() == 0 && tooltipContentIndOp.size() == 0) {
			return null;
		}
		keys.clear();
		keys.addAll(tooltipContentIndOp.keySet());
		keys.addAll(tooltipContentAggOp.keySet());
		tooltipContentAggOp.keySet();

		for (DoubleRectangle oneSpan : keys) {
			myHTML += "<img src=\""
					+ MainPageNew.class.getResource("icons/gotoobj_tsk.gif")
					+ "\"><span style=\"color:#FF0080\">"
					+ tooltipText.get(oneSpan) + "</span><br>";
			myHTML += tooltipContentIndOp.get(oneSpan) == null ? ""
					: tooltipContentIndOp.get(oneSpan);
			myHTML += tooltipContentAggOp.get(oneSpan) == null ? ""
					: tooltipContentAggOp.get(oneSpan);

		}
		return myHTML + "</html>";
	}

	private void constructTooltipForIndividualSnip(DoubleRectangle oneKey,
			Map<DoubleRectangle, String> tooltipContentIndOp,
			Map<DoubleRectangle, String> tooltipContentAggOp,
			Map<DoubleRectangle, String> tooltipText) {
		
		double x1 = oneKey.getX1();
		double x2 = oneKey.getX2();
		double y1 = oneKey.getY1();
		double y2 = oneKey.getY2();
		
		Set<Annotation> annotations = annoDocModel.getAnnotationByBoundingBox(x1, y1, x2, y2);
		
		String annotator, opinion, text;
		for (Annotation annotation : annotations) {
			annotator = annotation.getOriginAnnotator();
			opinion = annotation.getConcept();
			text = getText(annotation);

			if (AnnotatorNameInd.isValidValue(annotator)) {// it's
				// individual
				// opinion
				String spanAnnotationStr = tooltipContentIndOp.get(oneKey);
				if (spanAnnotationStr == null) {
					spanAnnotationStr = "";
				}
				spanAnnotationStr += "<p class=\"ind\" style=\"BACKGROUND-COLOR: rgb(153,153,255,150)\">"
						+ annotator + ": "
						 + opinion  + "</p>";
						//+ opinion + "</p>";
				tooltipContentIndOp.put(oneKey, spanAnnotationStr);

			} else if (AnnotatorNameAgg.isValidValue(annotator)) {// it's
				// aggregator opinion
				String spanAnnotationStr = tooltipContentAggOp.get(oneKey);
				if (spanAnnotationStr == null) {
					spanAnnotationStr = "";
				}

				String link = null;
				try {
					link = MaterializedOntologyQuery.getDbpediaType(opinion);
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
				if (link != null) {// treat opinion as a hyperlink
					spanAnnotationStr += "<p class=\"agg\" style=\"BACKGROUND-COLOR: rgb(255,153,153,150)\">"
							+ "<a href=\"" + link + "\">" + annotator + ": "
							 + opinion
							+ "</a></p>";

				} else {
					spanAnnotationStr += "<p class=\"agg\" style=\"BACKGROUND-COLOR: rgb(255,153,153,150)\">"
							+ annotator + ": "
							+ opinion
							+ "</p>";
				}
				tooltipContentAggOp.put(oneKey, spanAnnotationStr);

			}
			tooltipText.put(oneKey, text);
		}

	}

	public Map<Integer, Map<DoubleRectangle, Set<String>>> getSpanAnnotators() {
		return spanAnnotators;
	}

	public void setSpanAnnotators(
			Map<Integer, Map<DoubleRectangle, Set<String>>> spanAnnotators) {
		this.spanAnnotators = spanAnnotators;
	}

	public Map<Integer, Map<DoubleRectangle, Set<String>>> getSpanConcepts() {
		return spanConcepts;
	}

	public void setSpanConcepts(
			Map<Integer, Map<DoubleRectangle, Set<String>>> spanConcepts) {
		this.spanConcepts = spanConcepts;
	}

	public void setCurPageNum(int curPageNum) {
		this.curPageNum = curPageNum;
	}

	public void setAnnoDocModel(AnnotatedPDFModel annoDocModel) {
		this.annoDocModel = annoDocModel;
	}

	private String getText(Annotation annotation) {
		long from = annotation.getStart();
		long to = annotation.getEnd();
		
		String text = 
				this.annoDocModel.getAnnotatedText().substring((int)from, (int)to);
		return text;
		
		
	}

}
