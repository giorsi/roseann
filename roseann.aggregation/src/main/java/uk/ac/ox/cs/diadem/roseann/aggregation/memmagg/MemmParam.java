/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg;

/**
 * 
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) - Department
 *         of Computer Science - University of Oxford
 */
public class MemmParam {
	//======== MEMM parameters name ========
	
	public static final String THREAD_COUNT = "threadCount";
	public static final String PRUNING_UPBOUND = "thresholdMax";
	public static final String PRUNING_LOWBOUND = "thresholdMin";
	public static final String MODEL_POOL = "modelPool";
	public static final String ONTOLOGY_INVOLVED = "checkStateConsistency";
	public static final  String INITSTATE_NAME = "initialState";
	public static final  String TERMINAL_CONDITION = "iterationNum";
	public static final  String EVIDENCE_CUTOFF = "cutoff";
	public static final String MULT_OPINION_SPLITTER = "opinionSplitter";
	public static final String FEATURE_COL_SPLITTER = "featureSplitter";
	public static final String SKIP_COLUMN_NUM = "colSkipNum";
	public static final String RETURN_SAFE_CONCEPT = "returnSafeConcept";
	
	//======== MEMM parameters default value ========
	private static final int VAL_THREAD_COUNT = 50;
	private static final double VAL_PRUNE_MAX = 0.6;
	private static final double VAL_PRUNE_MIN = 0.005;
	private static final String VAL_POOLNAME = "./memmData/reuters/models/";
	private static final int VAL_ITERATION_NUM = 100;
	private static final int VAL_CUTOFF = 0;
	private static final String VAL_OPINION_SPLIT = "__";
	private static final String VAL_FEATURE_SPLIT = "	";
	private static final int VAL_SKIPCOL = 4;
	private static final boolean VAL_ONTOLOGY_CHECK  = false;
	private static final boolean VAL_RETURN_SAFE_CONCEPT  = true;
	private static final  String VAL_INITSTATE_NAME = "Begin";

	//======== MEMM optimisation ========
	
	/**
	 * The thread number for parallel processing
	 */
	private int threadCount = VAL_THREAD_COUNT;

	/**
	 * Upper bound for reserving outgoing transition
	 */
	private  double thresholdMax = VAL_PRUNE_MAX;

	/**
	 * Lower bound for pruning outgoing transition
	 */
	private  double thresholdMin = VAL_PRUNE_MIN;
	
	
	
	//======== MEMM input configuration ========
	
	/**
	 * The model pool specifying the root folder which contains
	 * the trained models for all states
	 */
	private String poolName = VAL_POOLNAME;
	private String initialState = VAL_INITSTATE_NAME;
	
	private int iterationNum = VAL_ITERATION_NUM;
	
	private int cutoff = VAL_CUTOFF;
	
	private String opinionSplitter = VAL_OPINION_SPLIT;
	private String featureSplitter = VAL_FEATURE_SPLIT;
	
	
	private boolean checkStateConsistency = VAL_ONTOLOGY_CHECK;
	private boolean returnSafyConcept = VAL_RETURN_SAFE_CONCEPT;
	
	private int colSkipNum = VAL_SKIPCOL;
	
	
	
	/**
	 * @return the initialState
	 */
	public String getInitialState() {
		if(initialState!=null&&initialState.length()>0){
			return initialState;
		}else{
			return VAL_INITSTATE_NAME;
		}
			
		
	}
	/**
	 * @param initialState the initialState to set
	 */
	public void setInitialState(String initialState) {
		if(initialState!=null&&initialState.length()>0)
		this.initialState = initialState;
	}
	/**
	 * @return the iterationNum
	 */
	public int getIterationNum() {
		return iterationNum;
	}
	/**
	 * @param iterationNum the iterationNum to set
	 */
	public void setIterationNum(int iterationNum) {
		if(iterationNum>=0)
		this.iterationNum = iterationNum;
	}
	/**
	 * @return the cutoff
	 */
	public int getCutoff() {
		return cutoff;
	}
	/**
	 * @param cutoff the cutoff to set
	 */
	public void setCutoff(int cutoff) {
		if(cutoff>=0)
		this.cutoff = cutoff;
	}
	/**
	 * @return the opinionSplitter
	 */
	public String getOpinionSplitter() {
		if(opinionSplitter!=null&&opinionSplitter.length()>0)
		return opinionSplitter;
		else{
			return VAL_OPINION_SPLIT;
		}
	}
	/**
	 * @param opinionSplitter the opinionSplitter to set
	 */
	public void setOpinionSplitter(String opinionSplitter) {
		if(opinionSplitter!=null&&opinionSplitter.length()>0)
		this.opinionSplitter = opinionSplitter;
	}
	/**
	 * @return the featureSplitter
	 */
	public String getFeatureSplitter() {
		if(featureSplitter!=null&&featureSplitter.length()>0)
		return featureSplitter;
		else{
			return VAL_FEATURE_SPLIT;
		}
	}
	/**
	 * @param featureSplitter the featureSplitter to set
	 */
	public void setFeatureSplitter(String featureSplitter) {
		if(featureSplitter!=null&&featureSplitter.length()>0)
		this.featureSplitter = featureSplitter;
	}
	/**
	 * @return the checkStateConsistency
	 */
	public boolean isCheckStateConsistency() {
		return checkStateConsistency;
	} 
	/**
	 * @param checkStateConsistency the checkStateConsistency to set
	 */
	public void setCheckStateConsistency(boolean checkStateConsistency) {
		this.checkStateConsistency = checkStateConsistency;
	}
	/**
	 * @return the returnSafyConcept
	 */
	public boolean isReturnSafyConcept() {
		return returnSafyConcept;
	}
	/**
	 * @param returnSafyConcept the returnSafyConcept to set
	 */
	public void setReturnSafyConcept(boolean returnSafyConcept) {
		this.returnSafyConcept = returnSafyConcept;
	}
	/**
	 * @return the colSkipNum
	 */
	public int getColSkipNum() {
		
		return colSkipNum>=0?colSkipNum:VAL_SKIPCOL;
		
	}
	/**
	 * @param colSkipNum the colSkipNum to set
	 */
	public void setColSkipNum(int colSkipNum) {
		if(colSkipNum>=0)
		this.colSkipNum = colSkipNum;
	}
	public String  featureBasedDocRootForTrain = "./memmData/reuters/featureBasedDoc/";
	public String sourceForTrain = "./memmData/reuters/corpus/";
	public String annotatedForTrain = "./memmData/reuters/";
	/**
	 * @return the threadCount
	 */
	public int getThreadCount() {
		
		return threadCount>=1?threadCount:VAL_THREAD_COUNT;
	}
	/**
	 * @param threadCount the threadCount to set
	 */
	public void setThreadCount(int threadCount) {
		if(threadCount>0)
		this.threadCount = threadCount;
	}
	/**
	 * @return the thresholdMax
	 */
	public double getThresholdMax() {
		return thresholdMax;
	}
	/**
	 * @param thresholdMax the thresholdMax to set
	 */
	public void setThresholdMax(double thresholdMax) {
		this.thresholdMax = thresholdMax;
	}
	/**
	 * @return the thresholdMin
	 */
	public double getThresholdMin() {
		return thresholdMin;
	}
	/**
	 * @param thresholdMin the thresholdMin to set
	 */
	public void setThresholdMin(double thresholdMin) {
		this.thresholdMin = thresholdMin;
	}
	/**
	 * @return the poolName
	 */
	public String getPoolName() {
		if(poolName!=null&&poolName.length()>0)
		return poolName;
		else{
			return VAL_POOLNAME;
		}
	}
	/**
	 * @param poolName the poolName to set
	 */
	public void setPoolName(String poolName) {
		if(poolName!=null&&poolName.length()>0)
		this.poolName = poolName;
	}
	

}
