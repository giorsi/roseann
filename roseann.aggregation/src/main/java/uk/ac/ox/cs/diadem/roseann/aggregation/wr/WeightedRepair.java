/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.wr;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.AnnotationAttribute;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;
import uk.ac.ox.cs.diadem.roseann.aggregation.wr.util.SpanningAnnotation;
import uk.ac.ox.cs.diadem.roseann.api.aggregation.AbstractAggregator;
import uk.ac.ox.cs.diadem.roseann.api.aggregation.AggregationException;
import uk.ac.ox.cs.diadem.roseann.ontology.MaterializedOntologyQuery;
import uk.ac.ox.cs.diadem.roseann.util.graph.DAG;
import uk.ac.ox.cs.diadem.roseann.util.graph.Node;


/**
 * Class that implements the weighted repair aggregation strategy.
 * Given a set of annotations, first the spanning set S are computed. For each spanning set s,
 * the conflicting set C are computed. For each conflicting set c the weighted repair computes 
 * the correct annotations. The algorithm iteratively repeats the procedure until no more
 * conflicts are found or after 20 iterations
 * 
 * An AggregatorException is thrown in case one (or more) annotation have a concept unknown in the Ontology.
 * 
 * @author Stefano Ortona (stefano dot ortona at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */

public class WeightedRepair extends AbstractAggregator {

	/**
	 * Logger
	 */
	static final Logger logger = LoggerFactory.getLogger(WeightedRepair.class);

	/**
	 * Trace the number of iterations for weighted repair
	 */
	private int countiteration;

	public WeightedRepair(){
		this.setAggregatorName("wr");
	}

	@Override
	public Set<Annotation> reconcile(String originaltext,
			Set<Annotation> originalAnnotation, Set<String> involvedAnnotators){

		logger.info("Weighted Repair aggregation starting the computation");
		this.countiteration=0;
		Set<Annotation> annotationCopy=new HashSet<Annotation>();
		annotationCopy.addAll(originalAnnotation);
		return reconcileLocally(annotationCopy);

	}

	private Set<Annotation> reconcileLocally(Set<Annotation> originalAnnotation) {

		countiteration++;

		//if more than 20 iterations stop the process
		if(countiteration>20){
			logger.warn("The {} aggregation algorithm was doing more than 20 iterations to complete its job " +
					"and it has been stopped.",this.getAggregatorName());
			//modify the annotator name and id
			originalAnnotation=buildFinalAnnotations(originalAnnotation);
			return originalAnnotation;
		}

		logger.info("Weighted Repair Aggregation: Iteration {}",countiteration);

		//calculate the spanning set
		final Set<Set<Annotation>> setSpanningSet=SpanningAnnotation.calculateSpanningSet(originalAnnotation);

		Set<Annotation> candidateAnnotations=new HashSet<Annotation>();

		//keep trace of the deleted annotations
		final Set<Annotation> deletedAnnotations = new HashSet<Annotation>();

		//for each spanning set, calculate the conflicting annotations and disambiguate the conflicting annotations
		boolean isInfConlfict = false;
		for(Set<Annotation> spanningSet:setSpanningSet){

			final Set<Set<Annotation>> setConflictingSet=SpanningAnnotation.getConflictingSet(spanningSet);

			//disambiguate each conflicting set
			for(Set<Annotation> conflictingSet:setConflictingSet){

				if(conflictingSet.size()>1)
					isInfConlfict=true;

				final Set<Annotation> actualDeletedAnnotations = new HashSet<Annotation>();
				actualDeletedAnnotations.addAll(conflictingSet);


				final Set<Annotation> disambiguatedAnnotation=this.disambiguateAnnotation(conflictingSet);
				actualDeletedAnnotations.removeAll(disambiguatedAnnotation);
				deletedAnnotations.addAll(actualDeletedAnnotations);
				candidateAnnotations.addAll(disambiguatedAnnotation);
			}

		}

		//remove all the annotations that have been deleted at least once
		candidateAnnotations.removeAll(deletedAnnotations);

		if(!isInfConlfict){
			//modify the annotator name and id
			candidateAnnotations = buildFinalAnnotations(candidateAnnotations);
			logger.info("Repair Aggregation algorithm terminates the computation in {} iteration",countiteration);
			return candidateAnnotations;
		}

		return reconcileLocally(candidateAnnotations);
	}

	/**
	 * Utility method to aggregate and disambiguate a set of conflicting annotation, using the weighted repair algorithm
	 * 
	 * @param conflictingAnnotations
	 * 					Set of conflicting annotation
	 * @param involved_annotators
	 * 					The set of annotators which annotated the document
	 * @return
	 */
	private Set<Annotation> disambiguateAnnotation(final Set<Annotation> conflictingAnnotations){

		final Map<String,Annotation> type2annotation=new HashMap<String,Annotation>();

		//for every type save the related annotation.
		//If two annotations have the same type, save the longest, or the one with lower id
		for (Annotation annotation:conflictingAnnotations){
			final String type=annotation.getConcept();
			final Annotation previousAnnotation = type2annotation.get(type);
			if(previousAnnotation==null){
				type2annotation.put(type, annotation);
				continue;
			}
			final long previousSpan = previousAnnotation.getEnd()-previousAnnotation.getStart();
			final long actualSpan = annotation.getEnd()-annotation.getStart();
			if(previousSpan>actualSpan)
				continue;

			if(actualSpan>previousSpan){
				type2annotation.put(type, annotation);
				continue;
			}

			//equal type and equal span, alphabetically check the id
			if(annotation.getId().compareTo(
					previousAnnotation.getId())<0)
				type2annotation.put(type, annotation);
		}

		//get the correct types
		final Set<String> resultTypes=this.getCorrectTypes(conflictingAnnotations);

		/*
		 * build the final annotation
		 * if there's annotation with result type pick that one,otherwise pick the longest annotation with the subtype as type
		 */
		final Set<Annotation> finalAnnotation=new HashSet<Annotation>();
		for(String resultType:resultTypes){
			Annotation annotation=type2annotation.get(resultType);

			//pick the longest annotation with type as subtype of the final results and change the type to the correct one
			if(annotation==null){
				String id="";
				long maxSpan=-1;
				String annotatorSource="";
				for(Annotation subTypeAnnotation:conflictingAnnotations){
					final String type=subTypeAnnotation.getConcept();
					final Set<String> supertypes = MaterializedOntologyQuery.getAllSupertypes(type);
					if(supertypes==null)
						throw new AggregationException("The concept "+type+" " +
								"is unknown in the Ontology method getAllSupertypes", logger);
					if(supertypes.contains(resultType)){
						id+="_"+subTypeAnnotation.getId();
						annotatorSource+="_"+subTypeAnnotation.getOriginAnnotator();
						final long start=subTypeAnnotation.getStart();
						final long end=subTypeAnnotation.getEnd();
						if(end-start>maxSpan){
							maxSpan=end-start;
							annotation=subTypeAnnotation;
						}
					}
				}
				id=id.replaceFirst("_", "");
				annotatorSource=annotatorSource.replaceFirst("_", "");
				annotation=new EntityAnnotation(id, resultType, annotation.getStart(), annotation.getEnd(), annotatorSource, 
						null,annotation.getConfidence(),AnnotationClass.INSTANCE);

			}

			finalAnnotation.add(annotation);

		}

		return finalAnnotation;
	}

	/**
	 * Utility method to modify the id and the origin annotator of a set of annotations and 
	 * return the set of new crated modified annotations
	 * 
	 */
	private Set<Annotation> buildFinalAnnotations(Set<Annotation> annotations){
		final Set<Annotation> modifiedAnnotations=new HashSet<Annotation>();
		//create a new annotation and modify the annotator name and id
		int count=0;
		for(Annotation annotation:annotations){
			Annotation newAnnotation = new EntityAnnotation();
			newAnnotation.setAnnotationClass(annotation.getAnnotationClass());
			newAnnotation.setConcept(annotation.getConcept());
			if(annotation.getConfidence()!=null)
				newAnnotation.setConfidence(annotation.getConfidence());
			newAnnotation.setEnd(annotation.getEnd());
			newAnnotation.setStart(annotation.getStart());

			//set the attributes
			for(AnnotationAttribute attribute:annotation.getAttributes()){
				AnnotationAttribute newAttribute=new AnnotationAttribute(attribute.getName(), attribute.getValue());
				newAnnotation.addAttribute(newAttribute);
			}

			newAnnotation.setId(this.getAggregatorName()+"_"+count);
			//the annotation source became the origin annotator
			newAnnotation.setAnnotationSource(annotation.getOriginAnnotator());
			//the origin annotator became the aggregator method
			newAnnotation.setOriginAnnotator(this.getAggregatorName());
			count++;
			modifiedAnnotations.add(newAnnotation);
		}
		return modifiedAnnotations;
	}

	/**
	 * Utility method that given a set of conflicting annotations,apply the weighted repair algorithm and return a set of reconciled types
	 * @param conflictingAnnotations
	 * 					The input set of conflicting annotations
	 * @param involved_annotators
	 * 					The annotators involved in the annotation process
	 * @return			A set of correct types
	 */
	private Set<String> getCorrectTypes(final Set<Annotation> conflictingAnnotations) throws AggregationException{

		Set<String> correctTypes=new HashSet<String>();
		if(conflictingAnnotations==null||conflictingAnnotations.size()==0)
			return correctTypes;

		//build the DAG
		String concept=conflictingAnnotations.iterator().next().getConcept();
		final DAG<String,Integer> conceptsDag = MaterializedOntologyQuery.getSuperclassesGraph(
				concept);

		if(conceptsDag==null){
			logger.warn("The concept "+concept+" " +
					"is unknown in the Ontology method getSuperclassGraph", logger);
			return new HashSet<String>();
		}

		//store the original annotated types
		final Set<String> originalTypes=new HashSet<String>();
		for(Annotation annotation:conflictingAnnotations){
			concept=annotation.getConcept();
			originalTypes.add(concept);
			final DAG<String,Integer> annotationDag=MaterializedOntologyQuery.getSuperclassesGraph(concept);
			if(annotationDag==null){
				logger.warn("The concept "+concept+" " +
						"is unknown in the Ontology method getSuperclassGraph", logger);
				return new HashSet<String>();
			}
			conceptsDag.mergeGraph(annotationDag);
		}

		//for each score in the graph, calculate the atomic score
		for(Node<String> node:conceptsDag.getAllNodes()){
			int score=0;
			concept=node.getLabel();
			for(Annotation annotation:conflictingAnnotations){
				final String annotationType=annotation.getConcept();
				final Set<String> supertype=MaterializedOntologyQuery.getAllSupertypes(annotationType);
				if(supertype==null){
					logger.warn("The concept "+annotationType+" " +
							"is unknown in the Ontology method getSAllSupertypes", logger);
					return new HashSet<String>();
				}

				//+1 step
				if(supertype.contains(concept))
					score++;
				//-1 step
				Boolean areDisjoint=MaterializedOntologyQuery.areDisjoint(concept, annotationType);
				if(areDisjoint==null){
					logger.warn("The concepts "+concept+" and " +annotationType+" "+
							"are unknown in the Ontology method areDisjoint", logger);
					return new HashSet<String>();
				}
				if(areDisjoint)
					score--;
			}
			conceptsDag.addNode(concept, score);
		}

		boolean noMoreLeaves=false;
		final String topConcept=MaterializedOntologyQuery.getTopConcept();
		while(!noMoreLeaves){
			//compute the leaves
			Collection<Node<String>> leaves=conceptsDag.getSources();

			//there are no more leaves and we've reached top there's nothing else to visit
			if(leaves.size()==1&&leaves.iterator().next().getLabel().equalsIgnoreCase(topConcept))
				noMoreLeaves=true;

			final Set<String> zeroWeightNodes = new TreeSet<String>();

			//iterate over all the leaves
			for(Node<String> leave:leaves){
				concept=leave.getLabel();
				//if the concept has weight>0, return the type
				if(conceptsDag.getWeight(leave)>0)
					correctTypes.add(concept);
				//treat special case for types which were in the original annotation and have weight=0
				if(conceptsDag.getWeight(leave)==0&&originalTypes.contains(concept))
					zeroWeightNodes.add(concept);

				conceptsDag.removeNode(leave);
			}

			if(zeroWeightNodes.size()>0)
				correctTypes.addAll(this.dealZeroWeightNode(zeroWeightNodes, conceptsDag));
		}

		//among the correct concepts, select only the deepest one
		Set<String> allSupertypes=new HashSet<String>();
		for(String correctConcept:correctTypes){
			final Set<String> currentSupertypes=MaterializedOntologyQuery.getAllSupertypes(correctConcept);
			if(currentSupertypes==null){
				logger.warn("The concepts "+
						"is unknown in the Ontology method getAllSupertypes", logger);
				return new HashSet<String>();
			}
			currentSupertypes.remove(correctConcept);
			allSupertypes.addAll(currentSupertypes);
		}
		correctTypes.removeAll(allSupertypes);



		return correctTypes;
	}

	/**
	 * Method to deal with concept that has zero weight
	 * @param zeroWeightNodes
	 * @param conceptsDag
	 * @return
	 */
	private Set<String> dealZeroWeightNode(final Set<String> zeroWeightNodes,final DAG<String,Integer> conceptsDag){
		final Set<String> correctTypes = new HashSet<String>();

		//for each node with weight=0, there could be at most one node which is disjoint with it.
		//if that is the case, just pick the first node and remove the other one

		while(zeroWeightNodes.size()>0){
			final String concept = zeroWeightNodes.iterator().next();
			zeroWeightNodes.remove(concept);
			//choose the first type among the disjoint types
			correctTypes.add(concept);
			String disjointConcept=null;
			for(String otherConcept:zeroWeightNodes){
				Boolean areDisjoint=MaterializedOntologyQuery.areDisjoint(concept, otherConcept);
				if(areDisjoint==null)
					throw new AggregationException("The concept "+concept+" and " +otherConcept+" "+
							"are unknown in the Ontology method areDisjoint", logger);
				if(areDisjoint){
					disjointConcept=otherConcept;
					break;
				}

			}
			if(disjointConcept!=null)
				zeroWeightNodes.remove(disjointConcept);

		}

		return correctTypes;
	}

}
