/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.dataModel;
/**
 * 
 */

import java.util.*;



/**
 * This class defines the data structure of a transition matrix 
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class MarkovSequenceUnit {
	//The corresponding observation(token information) of the input sequence
	protected Event myFeature;
	//The corresponding transition matrix of the given observation.
	private Map<String,TreeMap<String,Double>> transitionMatrix;//<outcomeIndex, outcomeProbability>
	/**
	 * @param transitionMatrix the transitionMatrix to set
	 */
	public void setTransitionMatrix(Map<String,TreeMap<String,Double>> transitionMatrix) {
		this.transitionMatrix = transitionMatrix;
	}
	/**
	 * @return the transitionMatrix
	 */
	public Map<String,TreeMap<String,Double>> getTransitionMatrix() {
		return transitionMatrix;
	}
	/**
	 * @param myFeature
	 * @param transitionMatrix
	 */
	public MarkovSequenceUnit(Event myFeature,
			TreeMap<String, TreeMap<String,Double>> transitionMatrix) {
		super();
		this.myFeature = myFeature;
		this.transitionMatrix = transitionMatrix;
	}
	/**
	 * Return the target set in ascendent order of this transition matrix
	 * @param
	 * @return Set<Integer> the target states set of this transition
	 */
	public Set<String> getTargetStates() {
		TreeSet<String> targetStates = new TreeSet<String>();
		if(transitionMatrix!=null){
			Iterator<TreeMap<String,Double>> valueItr = transitionMatrix.values().iterator();
			while(valueItr.hasNext()){
				TreeMap<String,Double> oneRow = valueItr.next();
				Iterator<String> columnItr = oneRow.keySet().iterator();
				while(columnItr.hasNext()){
					String temp = columnItr.next();
					if(temp==null){
						System.out.println("debug here");
					}
					targetStates.add(temp);
				}
			}
			return targetStates;
		}
		//System.out.println("transitionMatrix unit is empty");
		return null;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		Iterator<Map.Entry<String,TreeMap<String,Double>>> valueItr = transitionMatrix.entrySet().iterator();
		while(valueItr.hasNext()){
			Map.Entry<String,TreeMap<String,Double>> oneRow = valueItr.next();
			sb.append(oneRow.getKey()+" -> ");
			TreeMap<String,Double> rowValue = oneRow.getValue();
			Iterator<Map.Entry<String, Double>> columnsItr = rowValue.entrySet().iterator();
			while(columnsItr.hasNext()){
				Map.Entry<String, Double> oneColumn = columnsItr.next();
				sb.append(oneColumn.getKey()+":"+oneColumn.getValue()+"	");
				
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	/**
	 * A util method to get the matrix value of transition probability from state i to state j
	 * @param 
	 * @return
	 */
	public double getTransitionProbability(String prevStateIdx, String tarStateIdx) {
		if(transitionMatrix!=null){
			if(prevStateIdx!=null&&tarStateIdx!=null){
				TreeMap<String,Double> oneRow = this.transitionMatrix.get(prevStateIdx);
				if(oneRow!=null){
					Double edgeValue = oneRow.get(tarStateIdx);
					if(edgeValue!=null){
						return edgeValue;
					}
				}
			}
		}
		return 0;
	}
	/**
	 * @param
	 * @return
	 */
	public Set<String> getSourceStates() {
		if(transitionMatrix!=null){
			return transitionMatrix.keySet();
		}
		return null;
	}

}
