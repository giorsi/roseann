/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.swing.util;

import java.awt.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.plaf.*;
import javax.swing.text.Document;
import javax.swing.text.html.*;

/**
 * A customised tooltip class which handles user events
 * @author luin
 *
 */
@SuppressWarnings("serial")
public class ClickableTooltip extends JToolTip {

	private JEditorPane theEditorPane;

	public ClickableTooltip() {
		setLayout(new BorderLayout());
		LookAndFeel.installBorder(this, "ToolTip.border");
		LookAndFeel.installColors(this, "ToolTip.background",
				"ToolTip.foreground");
		
		
		
		    
		theEditorPane = new JEditorPane();
		theEditorPane.setContentType("text/html");
		theEditorPane.setEditable(false);
		theEditorPane.setOpaque(false);
		theEditorPane.addHyperlinkListener(new HyperlinkListener() {
			public void hyperlinkUpdate(HyperlinkEvent e) {
				if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
					// do whatever you want with the url
					System.out.println("clicked on link : "
							+ e.getDescription());
					openUrlLink(e.getURL().toString());
				}
			}
		});
		//define the css style for displayment
		setCSSStyle();
		add(theEditorPane);
	}
	 private void setCSSStyle() {
		 HTMLEditorKit cssKit = new HTMLEditorKit();
		 theEditorPane.setEditorKit(cssKit);
		 StyleSheet styleSheet = cssKit.getStyleSheet();
		    styleSheet
		        .addRule("p.ind {margin: 0; border-width: 1; border-style: solid;border-color: rgb(153,193,255)}");
		    styleSheet
	        .addRule("p.agg {margin: 0; border-width: 1; border-style: solid;border-color: rgb(255,193,153)}");
		    styleSheet
	        .addRule("p {margin: 0; border-width: 1; border-style: solid;border-color: rgb(105,105,105)}");
	    
		    
		    Document doc = cssKit.createDefaultDocument();
		    theEditorPane.setDocument(doc);
		
	}
	public void setTipText(String tipText) {
		 theEditorPane.setText(tipText);
		 }

		 public void updateUI() {
			 setUI(new ToolTipUI() {});
		 }
		 public static void openUrlLink(String link) {
				if(link != null){//some entity may not have a uri
					String url = link.replace('\"', ' ').trim();
					
					if(url.startsWith("http:")){
						try {
							java.net.URI uri = new java.net.URI(url);
							java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
							desktop.browse( uri );

						} catch (Exception e) {
							System.err.println(e.getMessage());
						}
					}
					
				}
				
			}


}
