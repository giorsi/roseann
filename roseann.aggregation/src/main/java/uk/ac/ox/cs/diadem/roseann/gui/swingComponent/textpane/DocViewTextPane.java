/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.swingComponent.textpane;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.*;

import javax.swing.*;
import javax.swing.text.BadLocationException;

import org.apache.commons.lang3.tuple.Pair;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.OmissionConflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict.ConflictType;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.LogicalConflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.*;
import uk.ac.ox.cs.diadem.roseann.gui.forms.*;
import uk.ac.ox.cs.diadem.roseann.gui.swing.util.ClickableTooltip;
import uk.ac.ox.cs.diadem.roseann.ontology.MaterializedOntologyQuery;
import uk.ac.ox.cs.diadem.roseann.util.Constant.AnnotatorNameAgg;
import uk.ac.ox.cs.diadem.roseann.util.Constant.AnnotatorNameInd;

/**
 * A customized text pane for the document viewer by overriding the getToolTipText() method.
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class DocViewTextPane extends JTextPane {

	private static final long serialVersionUID = 1L;
	private AnnotatedDocumentModel annoDocModel;
	private int[] blinkingSpan;
	private String blinkSpanID;
	private boolean toolTipLoc = false;
	private Set<String> annotatorsLive, conceptsLive;
	private TextSpanUtil spansUtil;

	@Override
	public String getToolTipText(MouseEvent e) {
		String myTip = null;
		int textLocation = viewToModel(e.getPoint());
		try {
			Rectangle viewLocation = modelToView(textLocation);
			// expand the rectangle a bit
			int error = 10;
			viewLocation = new Rectangle(viewLocation.x - error, viewLocation.y
					- error, viewLocation.width + 2 * error,
					viewLocation.height + 2 * error);

			if (viewLocation.contains(e.getPoint())) {

				/*
				 * do something to customise the tooltip content when user hover
				 * the mouse outside the blinking span
				 */
				if (blinkingSpan == null
						|| (textLocation > blinkingSpan[1] || textLocation < blinkingSpan[0])) {
					return showToolTipForSnippet(textLocation);

				}
				/*
				 * do something to customise the tooltip content when user hover
				 * the mouse over the blinking span
				 */

				// if the data model has been loaded
				if (annoDocModel != null) {
					return showToolTipForConflict();

				} else {
					toolTipLoc = false;
					return myTip;
				}
			}
		} catch (BadLocationException exp) {

			System.err
					.println("BadLocationException: Cannot insert text at offset "
							+ exp.offsetRequested());

		}
		toolTipLoc = false;
		return myTip;
	}

	private String showToolTipForConflict() {
		String aggOpStr = "", indOpStr = "";
		String myHTML = "<html>";
		// HashMap<String, String> annotatorOpinions = new HashMap<String,
		// String>();
		myHTML += "<img src=\""
				+ MainPageNew.class.getResource("icons/gotoobj_tsk.gif")
				+ "\"><span style=\"color:#FF0080\">"
				+ this.getText().substring(blinkingSpan[0], blinkingSpan[1])
				+ "</span><br>";

		// get all opinions first
		final Set<Annotation> atoms = annoDocModel.getAnnotationByStartEnd(blinkingSpan[0], blinkingSpan[1]);
		

		for (Annotation oneAtom : atoms) {

			String annotator = oneAtom.getOriginAnnotator();
			String opinion = oneAtom.getConcept();
			if (AnnotatorNameInd.isValidValue(annotator)) {// it's individual
															// opinion
				// indOpStr +=
				// "<FONT style=\"BACKGROUND-COLOR: rgb(153,153,255)\"><p><i>"
				// + annotator + "</i>: " + opinion + "</p></FONT>";
				indOpStr += "<p style=\"BACKGROUND-COLOR: rgb(153,153,255,150)\">"
						+ annotator + ": " + opinion + "</p>";

			} else if (AnnotatorNameAgg.isValidValue(annotator)) {// it's
																	// aggregator
																	// opinion

				aggOpStr += "<p style=\"BACKGROUND-COLOR: rgb(255,153,153,150)\">"
						+ annotator + ": " + opinion + "</p>";

			}

		}
		myHTML += indOpStr + aggOpStr;
		// get all conflicting information
		if (blinkingSpan != null) {
			// omission conflicts
			final String[] blinkID = this.blinkSpanID.split("_");
			final Set<Conflict> confs= annoDocModel.getConflictByStartEndType(Integer.parseInt(blinkID[0]), Integer.parseInt(blinkID[1]), ConflictType.OMISSION);
					
			if (confs.size() > 0) {
				myHTML += "<hr color=\"#848484\" size=\"2\" width=\"100%\" noshade align=\"right\">";
				myHTML += "<img src=\""
						+ MainPageNew.class.getResource("icons/warn_tsk.gif")
						+ "\"><b>Omission Conflicts:</b><br>";
				for (Conflict oneAtom : confs) {
					// myHTML+=
					// "Only "+oneAtom.getSubTerm(2)+" annotators identified "
					// +oneAtom.getSubTerm(1)+" over "+oneAtom.getSubTerm(3)+" competents.<br>";

					myHTML += "<p style=\"BACKGROUND-COLOR: rgb(255,255,0,150)\">"
							+ ((OmissionConflict)oneAtom).getConcept()
							+ ": "
							+ ((OmissionConflict)oneAtom).getAgreedAnnotators().size()
							+ " (of "
							+ (((OmissionConflict)oneAtom).getAgreedAnnotators().size()+((OmissionConflict)oneAtom).getOmittedAnnotators().size())
							+ " competents)" + "</p>";
				}

			}
			// logical conflicts
			final Set<Conflict> sconfs= annoDocModel.getConflictByStartEndType(Integer.parseInt(blinkID[0]), Integer.parseInt(blinkID[1]), ConflictType.LOGICAL);
			if (atoms.size() > 0) {
				myHTML += "<hr color=\"#0033cc\" size=\"2\" width=\"100%\" noshade align=\"right\">";
				myHTML += "<img src=\""
						+ MainPageNew.class.getResource("icons/hprio_tsk.gif")
						+ "\"><b>Logical Conflicts:</b><br>";
				for (Conflict oneAtom : sconfs) {
					
					Pair<Annotation,Annotation> annoPair = ((LogicalConflict)oneAtom).getConflictingAnnotation();
					myHTML += "<p style=\"BACKGROUND-COLOR: rgb(255,0,0,150)\">"
							+ annoPair.getLeft().getOriginAnnotator()
							+ ": "
							+ annoPair.getLeft().getConcept() + " vs. ";
					
					myHTML += ""
							+ annoPair.getRight().getOriginAnnotator()
							+ ": "
							+ annoPair.getRight().getConcept() + "</p>";

				}
			}

		}
		toolTipLoc = true;
		return myHTML + "</html>";

	}

	private String showToolTipForSnippet(int textLocation) {
		if (spansUtil != null) {
			String myHTML = "<html>";
			final Map<String, HashSet<String>> spanAnnotators = spansUtil
					.getSpanAnnotators();
			final Map<String, HashSet<String>> spanConcepts = spansUtil
					.getSpanConcepts();
			Set<String> keys = new HashSet<String>();
			keys.addAll(spanAnnotators.keySet());
			keys.addAll(spanConcepts.keySet());
			Map<String, String> tooltipContentIndOp = new HashMap<String, String>();
			Map<String, String> tooltipContentAggOp = new HashMap<String, String>();
			for (String oneKey : keys) {
				if (isTooltipEnable(oneKey, textLocation)) {
					constructTooltipForIndividualSnip(oneKey,
							tooltipContentIndOp, tooltipContentAggOp);
				}
			}
			// ignore mouse which is hovered on plain spans without any
			// annotation.
			if (tooltipContentAggOp.size() == 0
					&& tooltipContentIndOp.size() == 0) {
				toolTipLoc = false;
				return null;
			}
			keys.clear();
			keys.addAll(tooltipContentIndOp.keySet());
			keys.addAll(tooltipContentAggOp.keySet());
			tooltipContentAggOp.keySet();

			for (String oneSpan : keys) {
				String[] span = oneSpan.split("_");
				myHTML += "<img src=\""
						+ MainPageNew.class
								.getResource("icons/gotoobj_tsk.gif")
						+ "\"><span style=\"color:#FF0080\">"
						+ this.getText().substring(Integer.parseInt(span[0]),
								Integer.parseInt(span[1])) + "</span><br>";
				myHTML += tooltipContentIndOp.get(oneSpan) == null ? ""
						: tooltipContentIndOp.get(oneSpan);
				myHTML += tooltipContentAggOp.get(oneSpan) == null ? ""
						: tooltipContentAggOp.get(oneSpan);

				// myHTML +=
				// "<hr size=\"2\" width=\"100%\" noshade align=\"center\">";
			}
			toolTipLoc = true;
			return myHTML + "</html>";

		}
		toolTipLoc = false;
		return null;

	}

	@SuppressWarnings({ })
	private void constructTooltipForIndividualSnip(String oneKey,
			Map<String, String> tooltipContentIndOp,
			Map<String, String> tooltipContentAggOp) {
		
		String[] beginEnd = oneKey.split("_");
		final int begin = Integer.parseInt(beginEnd[0]);
		final int end = Integer.parseInt(beginEnd[1]);
		// get all opinions first
		// Set<Atom> atoms = annoDocModel.getAtomsByPredicate("EntityAnnotation");
		Set<Annotation> atoms = annoDocModel.getAnnotationByStartEnd(begin, end);
		String annotator, opinion;
		for (Annotation oneAtom : atoms) {
			annotator = oneAtom.getOriginAnnotator();
			opinion = oneAtom.getConcept();
			if (AnnotatorNameInd.isValidValue(annotator)) {// it's
				// individual
				// opinion

				String spanAnnotationStr = tooltipContentIndOp.get(begin + "_"
						+ end);
				if (spanAnnotationStr == null) {
					spanAnnotationStr = "";
				}
				spanAnnotationStr += "<p class=\"ind\" style=\"BACKGROUND-COLOR: rgb(153,153,255,150)\">"
						+ annotator
						+ ": "
						+ opinion
						+ "</p>";
				tooltipContentIndOp.put(begin + "_" + end, spanAnnotationStr);

			} else if (AnnotatorNameAgg.isValidValue(annotator)) {// it's
				// aggregator
				// opinion
				String spanAnnotationStr = tooltipContentAggOp.get(begin + "_"
						+ end);
				if (spanAnnotationStr == null) {
					spanAnnotationStr = "";
				}

				String link = null;
				try {
					
					link = MaterializedOntologyQuery.getDbpediaType(opinion);
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
				if (link != null) {// treat opinion as a hyperlink
					spanAnnotationStr += "<p class=\"agg\" style=\"BACKGROUND-COLOR: rgb(255,153,153,150)\">"
							+ "<a href=\""
							+ link
							+ "\">"
							+ annotator
							+ ": "
							+ opinion
							+ "</a></p>";

				} else {
					spanAnnotationStr += "<p class=\"agg\" style=\"BACKGROUND-COLOR: rgb(255,153,153,150)\">"
							+ annotator
							+ ": "
							+ opinion
							+ "</p>";
				}
				tooltipContentAggOp.put(begin + "_" + end, spanAnnotationStr);

			}

		}

	}

	private boolean isTooltipEnable(String oneSpanKey, int textLocation) {
		String[] theSpan = oneSpanKey.split("_");
		// check location first
		if (textLocation < Integer.parseInt(theSpan[0])
				|| textLocation > Integer.parseInt(theSpan[1])) {
			return false;
		}
		// check annotators
		if (annotatorsLive != null) {
			HashSet<String> annotators = spansUtil.getSpanAnnotators().get(
					oneSpanKey);

			for (String oneAnnotator : annotators) {
				if (annotatorsLive.contains(oneAnnotator)) {
					return true;
				}
			}
		}

		// check concepts
		if (conceptsLive != null) {
			HashSet<String> concepts = spansUtil.getSpanConcepts().get(
					oneSpanKey);

			for (String oneConcept : concepts) {
				if (conceptsLive.contains(oneConcept)) {
					return true;
				}
			}
		}

		return false;
	}

	

	@Override
	public Point getToolTipLocation(MouseEvent e) {
		// Set tooltip location
		if (toolTipLoc)
			return new Point(e.getPoint().x, e.getPoint().y + 2);
		else
			// for ignorable position
			return super.getToolTipLocation(e);

	}

	@Override
	public JToolTip createToolTip() {
		JToolTip tip = new ClickableTooltip();
		tip.setComponent(this);
		return tip;

	}

	public AnnotatedDocumentModel getAnnoDocModel() {
		return annoDocModel;
	}

	public void setAnnoDocModel(AnnotatedDocumentModel annoDocModel) {
		this.annoDocModel = annoDocModel;
		spansUtil = new TextSpanUtil(annoDocModel);

	}

	public void setBlinkingSpan(int[] blinkingSpan) {
		this.blinkingSpan = blinkingSpan;
	}

	public int[] getBlinkingSpan() {
		return blinkingSpan;
	}

	public void setBlinkSpanID(String blinkSpanID) {
		this.blinkSpanID = blinkSpanID;
	}

	public void removeAnnotatorMember(Set<String> memberToRemove) {
		if (annotatorsLive != null && memberToRemove != null)
			annotatorsLive.removeAll(memberToRemove);
	}

	public void addAnnotatorMember(Set<String> memberToAdd) {
		if (annotatorsLive == null) {
			annotatorsLive = new HashSet<String>();
		}
		if (memberToAdd != null)
			annotatorsLive.addAll(memberToAdd);
	}

	public void removeConceptMember(Set<String> memberToRemove) {
		if (conceptsLive != null && memberToRemove != null)
			conceptsLive.removeAll(memberToRemove);
	}

	public void addConceptMember(Set<String> memberToAdd) {
		if (conceptsLive == null) {
			conceptsLive = new HashSet<String>();
		}
		if (memberToAdd != null)
			conceptsLive.addAll(memberToAdd);
	}

}
