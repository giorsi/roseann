/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.swingComponent.pdfpane;

import java.io.File;
import java.util.List;

import javax.swing.JFrame;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

@SuppressWarnings("serial")
public class PDFViewerPnl extends JFrame {
	
	public PDFViewerPnl(File file) throws Exception {

		pagePanel = new DiademPDFPagePanel();
		
		// use setToolTipText to enable the tooltip
		// but we will override the getToolTipText() method so the toolTip
		// showed will be customized to show annotation information
		pagePanel.setToolTipText("");

		setDocument(PDDocument.load(file));

		currentPageNum = 0;

		this.setLocation(100, 100);

		this.setSize((int) pages.get(currentPageNum).getMediaBox().getWidth(),
				(int) pages.get(currentPageNum).getMediaBox().getHeight());

		setVisible(true);

		pagePanel.setCurPageNum(currentPageNum);
		
		add(pagePanel);

	}

	private DiademPDFPagePanel pagePanel;
	
	private int currentPageNum;

	private List<PDPage> pages;

	private PDDocument document;

	public DiademPDFPagePanel getPagePanel() {
		return pagePanel;
	}

	public PDDocument getDocument() {
		return document;
	}

	@SuppressWarnings("unchecked")
	public void setDocument(PDDocument document) {
		this.document = document;
		this.pages = document.getDocumentCatalog().getAllPages();
		showPage();
	}

	public void showPage(int pageNum) {
		currentPageNum = pageNum;
		showPage();
	}

	public void showPage() {
		setSize((int) pages.get(currentPageNum).getMediaBox().getWidth(),
				(int) pages.get(currentPageNum).getMediaBox().getHeight());
		pagePanel.setPage(pages.get(currentPageNum));
		pagePanel.updateUI();
	}

	public void showNextPage() {
		if (currentPageNum < pages.size() - 1) {
			currentPageNum++;
			pagePanel.setCurPageNum(currentPageNum);
			showPage();
		}
	}

	public void showPreviousPage() {
		if (currentPageNum > 0) {
			currentPageNum--;
			pagePanel.setCurPageNum(currentPageNum);
			showPage();
		}
	}

}
