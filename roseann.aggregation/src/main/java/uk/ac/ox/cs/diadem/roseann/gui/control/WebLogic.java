/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.control;

import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.slf4j.Logger;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.AnnotationAttribute;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.roseann.ROSeAnn;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web.AnnotatedHTMLModel;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web.HtmlDoc;
import uk.ac.ox.cs.diadem.roseann.gui.control.AbstractDocumentViewerBusiness.MODEL_TYPE;
import uk.ac.ox.cs.diadem.roseann.gui.forms.DocumentViewerPnl;
import uk.ac.ox.cs.diadem.roseann.gui.forms.MainPageNew;
import uk.ac.ox.cs.diadem.roseann.gui.swing.util.Console;
import uk.ac.ox.cs.diadem.roseann.gui.web.util.BrowserDecorator;
import uk.ac.ox.cs.diadem.roseann.util.Constant;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.WebBrowser.FeatureType;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;

public class WebLogic {

	public static WebBrowser openWebBrowser(final Logger logger, WebBrowser wb, 
			final boolean enableBrowser) {

		Console.print("Loading the browser...");
		logger.info("Loading the web browser...");

		if (!enableBrowser) {
			Console.println("Sorry, the browser is not enabled. Go in the configuration file and enable it.");
			return null;
		}

		wb = BrowserFactory.newWebBrowser(Engine.WEBDRIVER_FF, true);
		wb.enableFeatures(FeatureType.FIREBUG_HIDDEN);
		wb.setWindowSize(900, 700);

		wb.navigate("http://google.com", true);
		Console.println("...web browser loaded and page displayed.");
		logger.info("...web browser loaded and page displayed.");

		return wb;
	}

	public static void annotateWebPage(final Logger logger, final WebBrowser wb, final MainPageNew entryPage) {

		try {

			// annotate the text
			//ROSeAnn has to be changed with the shared one
			ROSeAnn roseann = MainpageBusiness.getRoseAnnInstance();
			URI uri = new URI(wb.getURL().toString());
			final AnnotatedHTMLModel annDocModel = roseann.annotateEntityWebPage(uri, true,false);


			// create the model with visible annotations
			final BrowserDecorator decorator = new BrowserDecorator(wb,annDocModel);

			final DocumentViewerPnl newPanel = new DocumentViewerPnl();

			entryPage.documentTabPane.addTab(wb.getLocationURL(),
					new ImageIcon(MainPageNew.class.getResource("icons/internal_browser.gif")), newPanel);
			entryPage.documentTabPane.setSelectedIndex(entryPage.documentTabPane.getTabCount() - 1);

			newPanel.setAnnoDocModel(annDocModel);
			final BrowserVisualizationBusiness docViewBus = new BrowserVisualizationBusiness(newPanel,
					decorator, MODEL_TYPE.BrowserModel);

			docViewBus.initialize();
			logger.info("...process done, ready to visualize the annotations.");
		} catch (final Exception exc) {
			exc.printStackTrace();
		}

	}

	public static WebBrowser visualizeStaticWebPage(final Logger logger, WebBrowser wb, final boolean enableBrowser,
			final MainPageNew entryPage, final MouseEvent e) {

		final TreePath tp = entryPage.webpageTree.getPathForLocation(e.getX(), e.getY());

		if (tp == null)
			return null;

		entryPage.webpageTree.setSelectionPath(tp);

		final DefaultMutableTreeNode node = (DefaultMutableTreeNode) tp.getLastPathComponent();

		if (node.isLeaf()) {

			final Object[] path = node.getUserObjectPath();
			String selectedFileName = "";

			for (int i = 1; i < path.length; i++) {
				selectedFileName += "/" + path[i].toString();
			}

			if (!enableBrowser) {
				Console.println("Sorry, the browser is not enabled. Go in the configuration file and enable it.");
				return null;
			}
			Console.println("Opening annotated web page:" + selectedFileName + "...");
			logger.info("Loading the web browser...");
			wb = BrowserFactory.newWebBrowser(Engine.WEBDRIVER_FF, true);
			wb.setWindowSize(900, 700);

			final String webTarget = MainpageBusiness.class.getResource(
					Constant.WEB_PAGES_ROOT + selectedFileName + selectedFileName + ".html").toString();
			URI uri = null;
			try{
				uri = new URI(webTarget);
			}
			catch(URISyntaxException ex){
				logger.error("The uri target '{}' is not a valid URI",webTarget,ex);
				return wb;
			}

			wb.navigate(webTarget, true);
			logger.info("...web browser loaded and page displayed.");

			logger.info("Starting the annotation process...");

			HtmlDoc htmlDoc=null;
			try {
				htmlDoc = new HtmlDoc(uri,false);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				logger.error("Not able to create an html document from the uri '{}'",webTarget,e1);
				return wb;
			}

			Set<Annotation> knownAnnotations = new HashSet<Annotation>();
			Set<Annotation> unknownAnnotations = new HashSet<Annotation>();
			Set<String> completedAnnotators = new HashSet<String>();
			Set<String> completedAggregators = new HashSet<String>();
			try {
				readStaticFile(selectedFileName, knownAnnotations, completedAnnotators, completedAggregators, unknownAnnotations,logger);
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				logger.error("Error while reading the static annotation file for web page '{}'",webTarget,e2);
				return wb;
			}
			Set<Conflict> conflicts = MainpageBusiness.getRoseAnnInstance().calculateConflicts(knownAnnotations, completedAnnotators);

			AnnotatedHTMLModel annotatedModel=null;
			try {
				annotatedModel = new AnnotatedHTMLModel(knownAnnotations, conflicts, 
						unknownAnnotations, completedAnnotators, htmlDoc, completedAggregators);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				logger.error("Not able to create an html annotated document model from the uri '{}'",webTarget,e1);
				return wb;
			}


			final BrowserDecorator decorator = new BrowserDecorator(wb,annotatedModel);


			final DocumentViewerPnl newPanel = new DocumentViewerPnl();

			entryPage.documentTabPane.addTab(selectedFileName,
					new ImageIcon(MainPageNew.class.getResource("icons/internal_browser.gif")), newPanel);

			entryPage.documentTabPane.setSelectedIndex(entryPage.documentTabPane.getTabCount() - 1);

			newPanel.setAnnoDocModel(annotatedModel);
			final BrowserVisualizationBusiness docViewBus = new BrowserVisualizationBusiness(newPanel,
					decorator, MODEL_TYPE.BrowserModel);

			docViewBus.initialize();
			logger.info("...annotation process done.");
		}
		return wb;

	}

	private static void readStaticFile(String fileName, Set<Annotation> knownAnnotations, 
			Set<String> completedAnnotators, Set<String> completedAggregators, 
			Set<Annotation> unknownAnnotations,Logger logger) 
					throws IOException{

		BufferedReader br = null;


		String sCurrentLine;

		String file = Constant.WEB_PAGES_ROOT+fileName+"/"+Constant.WEB_STATIC_MODEL_NAME;
		br = new BufferedReader(new InputStreamReader(PDFLogic.class.getResourceAsStream(
				file)));

		Map<String,Annotation> knownAnnotationMap = new HashMap<String,Annotation>();
		Map<String,Annotation> unknownAnnotationMap = new HashMap<String,Annotation>();

		while ((sCurrentLine = br.readLine()) != null) {

			try{
				String beginLine = sCurrentLine.substring(0,sCurrentLine.indexOf("("));
				switch (beginLine) {
				case "KnownAnnotation":{
					sCurrentLine = sCurrentLine.replace("KnownAnnotation(", "");
					sCurrentLine = sCurrentLine.replace(")", "");
					String []split = sCurrentLine.split(",");
					Annotation knownAnnotation = new EntityAnnotation();
					knownAnnotation.setId(split[0]);
					knownAnnotation.setConcept(split[1]);
					knownAnnotation.setAnnotationClass(AnnotationClass.valueOf(split[2].toUpperCase()));
					knownAnnotation.setOriginAnnotator(split[3]);
					if(!split[4].equals("null"))
						knownAnnotation.setAnnotationSource(split[4]);
					if(!split[5].equals("null"))
						knownAnnotation.setConfidence(Double.parseDouble(split[5]));
					knownAnnotation.setStart(Long.parseLong(split[6]));
					knownAnnotation.setEnd(Long.parseLong(split[7]));
					knownAnnotations.add(knownAnnotation);
					knownAnnotationMap.put(knownAnnotation.getId(), knownAnnotation);
					break;
				}
				case "UnknownAnnotation": { 
					sCurrentLine = sCurrentLine.replace("UnknownAnnotation(", "");
					sCurrentLine = sCurrentLine.replace(")", "");
					String []split = sCurrentLine.split(",");
					Annotation unkownAnnotation = new EntityAnnotation();
					unkownAnnotation.setId(split[0]);
					unkownAnnotation.setConcept(split[1]);
					unkownAnnotation.setAnnotationClass(AnnotationClass.valueOf(split[2]));
					unkownAnnotation.setOriginAnnotator(split[3]);
					if(!split[4].equals("null"))
						unkownAnnotation.setAnnotationSource(split[4]);
					if(!split[5].equals("null"))
						unkownAnnotation.setConfidence(Double.parseDouble(split[5]));
					unkownAnnotation.setStart(Long.parseLong(split[6]));
					unkownAnnotation.setEnd(Long.parseLong(split[7]));
					unknownAnnotations.add(unkownAnnotation);
					unknownAnnotationMap.put(unkownAnnotation.getId(), unkownAnnotation);
					break;
				}

				case "KnownAttribute": {
					sCurrentLine = sCurrentLine.replace("KnownAttribute(", "");
					sCurrentLine = sCurrentLine.replace(")", "");
					String []split = sCurrentLine.split(",");
					AnnotationAttribute atribute = new AnnotationAttribute(split[1], split[2]);
					String annotationId = split[0];
					Annotation annotation = knownAnnotationMap.get(annotationId);
					annotation.addAttribute(atribute);
					break;
				}
				case "UnKnownAttribute": {
					sCurrentLine = sCurrentLine.replace("UnKnownAttribute(", "");
					sCurrentLine = sCurrentLine.replace(")", "");
					String []split = sCurrentLine.split(",");
					AnnotationAttribute atribute = new AnnotationAttribute(split[1], split[2]);
					String annotationId = split[0];
					Annotation annotation = unknownAnnotationMap.get(annotationId);
					annotation.addAttribute(atribute);
					break;
				}
				case "CompletedAggregators": {
					sCurrentLine = sCurrentLine.replace("CompletedAggregators(", "");
					sCurrentLine = sCurrentLine.replace(")", "");
					String []split = sCurrentLine.split(",");
					for(String annotator:split)
						completedAggregators.add(annotator);
					break;
				}
				case "CompletedAnnotators": {
					sCurrentLine = sCurrentLine.replace("CompletedAnnotators(", "");
					sCurrentLine = sCurrentLine.replace(")", "");
					String []split = sCurrentLine.split(",");
					for(String annotator:split)
						completedAnnotators.add(annotator);
					break;
				}
				default : {
					logger.warn("There is un unknwon line '{}' in the static pdf file model",sCurrentLine);
					break;
				}
				}

			} catch(Exception e){
				logger.warn("Error while reading the static pdf model file",e);
			}
		}


		if (br != null)br.close();
	}

	/**
	 * Utility method that given a folder containing the web file, annotates it and save the annotations in a static file model
	 * @throws IOException 
	 * @throws URISyntaxException 
	 */
	private static void createStaticAnnotationModel(File webFolder) throws IOException, URISyntaxException{

		URI uri = new URI("file:"+webFolder.getAbsolutePath()+"/"+webFolder.getName()+".html");
		AnnotatedHTMLModel model = MainpageBusiness.getRoseAnnInstance().annotateEntityWebPage(uri, true,false);

		model.getHtmlDoc().getBrowserModel().write(new File("browser"));

		File toWrite = new File(webFolder.getAbsolutePath()+"/"+Constant.WEB_STATIC_MODEL_NAME);

		FileWriter fw = new FileWriter(toWrite.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);

		//write the known annotation
		for(Annotation annotaiton:model.getKnownAnnotations()){
			bw.write("KnownAnnotation("+annotaiton.getId()+","
					+annotaiton.getConcept()+","
					+annotaiton.getAnnotationClass()+","
					+annotaiton.getOriginAnnotator()+","
					+annotaiton.getAnnotationSource()+","
					+annotaiton.getConfidence()+","
					+annotaiton.getStart()+","
					+annotaiton.getEnd()+")\n");

			//write the attribute
			for(AnnotationAttribute attribute:annotaiton.getAttributes()){
				bw.write("KnownAttribute("+
						annotaiton.getId()+","
						+attribute.getName()+","
						+attribute.getValue()+")\n");
			}

		}

		//write the known annotation
		for(Annotation annotaiton:model.getUnknownAnnotation()){
			bw.write("UnknownAnnotation("+annotaiton.getId()+","
					+annotaiton.getConcept()+","
					+annotaiton.getAnnotationClass()+","
					+annotaiton.getOriginAnnotator()+","
					+annotaiton.getAnnotationSource()+","
					+annotaiton.getConfidence()+","
					+annotaiton.getStart()+","
					+annotaiton.getEnd()+")\n");

			//write the attribute
			for(AnnotationAttribute attribute:annotaiton.getAttributes()){
				bw.write("UnKnownAttribute("+
						annotaiton.getId()+","
						+attribute.getName()+","
						+attribute.getValue()+")\n");
			}

		}

		if(model.getCompletedAggregators().size()>0){
			bw.write("CompletedAggregators(");
			//write the completed aggregators
			int aggregatorSize = model.getCompletedAggregators().size();
			for(String annotator:model.getCompletedAggregators()){
				if(aggregatorSize==1)
					bw.write(annotator+")");
				else
					bw.write(annotator+",");
				aggregatorSize--;
			}
			bw.write("\n");
		}

		if(model.getCompletedAnnotators().size()>0){
			bw.write("CompletedAnnotators(");
			//write the completed aggregators
			int aggregatorSize = model.getCompletedAnnotators().size();
			for(String annotator:model.getCompletedAnnotators()){
				if(aggregatorSize==1)
					bw.write(annotator+")");
				else
					bw.write(annotator+",");
				aggregatorSize--;
			}
			bw.write("\n");
		}

		bw.close();
	}

	public static void main(final String[] args) throws IOException, URISyntaxException {
		File webFolder = new File("/home/stefano/Documents/DIADEM/Developing/workspace/roseann/src/main/resources/uk/ac/ox/cs/diadem/roseann/gui/control/sampleWeb/");
		for(File child:webFolder.listFiles()){
			if(child.isDirectory())
				createStaticAnnotationModel(child);
		}
		
		
	}

}
