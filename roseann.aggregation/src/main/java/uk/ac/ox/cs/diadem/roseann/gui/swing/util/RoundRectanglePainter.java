/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.swing.util;

import java.awt.*;
import javax.swing.text.*;
import javax.swing.text.DefaultHighlighter.DefaultHighlightPainter;
/**
 * A simple highlight painter that renders a rectangle around the
 * text area to be highlighted.
 * @author luyche
 * 
 */

public class RoundRectanglePainter extends DefaultHighlightPainter
{
	public RoundRectanglePainter(Color color)
	{
		super( color );
	}

	/**
	 * Paints a portion of a highlight.
	 *
	 * @param  g the graphics context
	 * @param  offs0 the starting model offset >= 0
	 * @param  offs1 the ending model offset >= offs1
	 * @param  bounds the bounding box of the view, which is not
	 *	       necessarily the region to paint.
	 * @param  c the editor
	 * @param  view View painting for
	 * @return Shape region drawing occured in
	 */
	public Shape paintLayer(Graphics g, int offs0, int offs1, Shape bounds, JTextComponent c, View view)
	{
		Rectangle r = getDrawingArea(offs0, offs1, bounds, view);

		if (r == null) return null;

		//  Do your custom painting

		Color color = getColor();
		g.setColor(color == null ? c.getSelectionColor() : color);

		//  Code is the same as the default highlighter except we use drawRect(...)

//		g.fillRect(r.x, r.y, r.width, r.height);
		//g.drawRect(r.x, r.y, r.width - 1, r.height - 1);
	//	g.drawRect(r.x, r.y, r.width +1, r.height + 1);
		
		//Set the thickness of the border
		float thickness = 2;
		Stroke oldStroke = ((Graphics2D) g).getStroke();
		((Graphics2D) g).setStroke(new BasicStroke(thickness));
		g.drawRoundRect(r.x-2, r.y-2, r.width +4, r.height + 4, 8, 8);
		//reset the thickness
		((Graphics2D) g).setStroke(oldStroke);
		
		// Return the drawing area
		return r;
	}

	
	/**
	 * Utility method to get the region of the text area to be rendered.
	 * @param offs0 the starting position of the snippet in the model
	 * @param offs1 the ending position of the snippet in the model
	 * @param bounds
	 * @param view
	 * @return Rectangle
	 */
	private Rectangle getDrawingArea(int offs0, int offs1, Shape bounds, View view)
	{
		// Contained in view, can just use bounds.

		if (offs0 == view.getStartOffset() && offs1 == view.getEndOffset())
		{
			Rectangle alloc;

			if (bounds instanceof Rectangle)
			{
				alloc = (Rectangle)bounds;
			}
			else
			{
				alloc = bounds.getBounds();
			}

			return alloc;
		}
		else
		{
			// Should only render part of View.
			try
			{
				// --- determine locations ---
				Shape shape = view.modelToView(offs0, Position.Bias.Forward, offs1,Position.Bias.Backward, bounds);
				Rectangle r = (shape instanceof Rectangle) ? (Rectangle)shape : shape.getBounds();

				return r;
			}
			catch (BadLocationException e)
			{
				System.err.println("BadLocationException: Cannot insert text at offset " + e.offsetRequested());
				
			}
		}

		// Can't render

		return null;
	}
}
