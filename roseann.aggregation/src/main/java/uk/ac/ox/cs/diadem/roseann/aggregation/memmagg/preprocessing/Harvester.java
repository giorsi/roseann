/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.preprocessing;

import java.io.File;
import java.util.*;
import java.util.concurrent.Callable;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.roseann.gui.toolkit.AnnotatedDocXMLAdatpter;

/**
 * An utility class to harvest annotations from annotated documents, which
 * invokes AnnotatedDocXMLAdatpter to fulfil the job. Each instance of Harvester
 * represents a harvester for the response from one annotator (specified by
 * annotator_name)
 * 
 * @author Luying Chen
 * 
 */
public class Harvester implements Callable<Set<Annotation>>{

	/**
	 * the annotator of which the response is harvested
	 */
	private String anntotatorName = "gold_standard";
	/**
	 * the response file where the annotations are harvested
	 */
	private String annotatedFile;

	public Harvester(String responseFile, String annotatorName) {
		setAnntotatorName(annotatorName);
		  this.annotatedFile = responseFile.replace("XXX",annotatorName);
	       
	}
	public Harvester(String responseFile) {
		
		  this.annotatedFile = responseFile.replace("XXX",anntotatorName);
	       
	}
	

	/**
	 * @param annotatorName
	 */
	private void setAnntotatorName(String annotatorName) {
		this.anntotatorName = annotatorName;

	}

	/**
	 * A method to harvest annotations from a given response file (in XML format)
	 * @param responseFileName the response file where the annotations are harvested
	 * @return Set<Annotation> the harvested annotations
	 */
	public Set<Annotation> harvestAnnotations() {

		File annotatedFile = new File(this.annotatedFile);
		if (!annotatedFile.exists()) {
			return new HashSet<Annotation>();
		}

		Set<Annotation> annotations = AnnotatedDocXMLAdatpter
				.harvestAnnotationXML(annotatedFile, anntotatorName);
		//System.out.println(this.anntotator_name +" harvests "+ annotations.size()+ " annotations.");

		return annotations;
		
	}

	/**
	 * @return the anntotator_name
	 */
	public String getAnntotatorName() {
		return anntotatorName;
	}

	/* (non-Javadoc)
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Set<Annotation> call() throws Exception {
		Set<Annotation> res = null ;
		
		
		res = harvestAnnotations();
     
		return res;
	}

}
