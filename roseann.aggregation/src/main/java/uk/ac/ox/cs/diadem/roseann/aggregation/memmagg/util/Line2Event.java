/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.util;

import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.dataModel.Event;
import uk.ac.ox.cs.diadem.roseann.util.Constant.AnnotatorNameInd;

/**
 * 
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class Line2Event {
	public static Event line2Event(boolean isTrain, String line, int skipColumn, String splitter){
		if (line.trim().length() > 0) {
			String[] fields = line.split(splitter);
			Event oneRawEvent = new Event();
			if(isTrain)
			oneRawEvent
					.setOutcome(fields[fields.length - 1].trim());
			else{
				oneRawEvent.setOutcome("?");
			}
			oneRawEvent.setToken(fields[0]);
			int[] beginEnd = { Integer.parseInt(fields[1]),
					Integer.parseInt(fields[2]) };
			oneRawEvent.setBeginEnd(beginEnd);
			if (fields[3].equals("true")) {
				oneRawEvent.setBeginOfSentence(true);
			}
			String[] context = new String[fields.length - skipColumn-1];
			for (int i = 0; i < context.length; i++) {
				context[i] = fields[skipColumn + i];
				if(isSingleOpinion(fields[skipColumn + i])){
					int index = fields[skipColumn + i].indexOf("_");
					
					oneRawEvent.getContextPredHash().put(fields[skipColumn + i].substring(0, index), fields[skipColumn + i].substring(index));
					
				}
			}
		
			oneRawEvent.setContextPred(context);;
			return oneRawEvent;
			
		}
		return null;
	}
	private static boolean isSingleOpinion(String opinion) {
		if(!opinion.contains("_")){
			return false;
		}
		for(AnnotatorNameInd you:AnnotatorNameInd.values()){
			if(opinion.contains(you.name().toLowerCase())){
				for(AnnotatorNameInd me:AnnotatorNameInd.values()){
					if(!you.equals(me)&&opinion.contains(me.name().toLowerCase())){
						return false;
					}
				}
			}
			
		}
		return true;
	}

}
