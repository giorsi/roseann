/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.swingComponent.textpane;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.AnnotatedDocumentModel;


public class TextSpanUtil {
	private Map<String, HashSet<String>> spanAnnotators;
	private Map<String, HashSet<String>> spanConcepts;

	public TextSpanUtil(AnnotatedDocumentModel annoDocModel) {
		
		if(annoDocModel!=null){
			spanAnnotators = new HashMap<String, HashSet<String>>();
			spanConcepts = new HashMap<String, HashSet<String>>();
			fillInMaps(annoDocModel.getAllAnnotations());
		}
	}

	private void fillInMaps(final Set<Annotation> model) {
		long begin, end;
		String annotator,concept;
		
		for (Annotation oneAtom : model) {
			begin = oneAtom.getStart();
			end = oneAtom.getEnd();
			annotator = oneAtom.getOriginAnnotator();
			concept = oneAtom.getConcept();
			HashSet<String> set = spanAnnotators.get(begin
							+ "_" + end);
			if(set==null){
				set = new HashSet<String>();
				spanAnnotators.put(begin
							+ "_" + end, set);
			}
			set.add(annotator);
			
			set = spanConcepts.get(begin
					+ "_" + end);
			if(set==null){
				set = new HashSet<String>();
				spanConcepts.put(begin
					+ "_" + end, set);
			}
			set.add(concept);
			
		}
		
	}
	
	public Map<String, HashSet<String>> getSpanAnnotators(){
		
			return spanAnnotators;
		
	}
	public Map<String, HashSet<String>> getSpanConcepts(){
		
		return spanConcepts;
	
}

}
