/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.prediction;


import java.util.*;

import opennlp.model.MaxentModel;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.Memm;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.dataModel.*;



/**
 * A utitlity class to produce markov sequence, given the input event sequence.
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class MarkovSequenceProducer {
	
	
	/**
	 * An utility method to construct a markov sequence for a given event(evidence vector)
	 * @param eventSequence
	 * 						Input event sequence
	 * @param prevStates
	 * 						The init states
	 * @param modelPool
	 * 						The trained model pool configured
	 * @return MarkovSequence
	 * 						The output markov sequence
	 */
	public static MarkovSequence constructMarkovSequence(List<Event> eventSequence,Set<String> prevStates, Memm memm){
		
		MarkovSequence output = null;
		if(prevStates==null||prevStates.size()==0){
			prevStates = new HashSet<String>();
			prevStates.add(memm.getMyParams().getInitialState());
		}
		if(eventSequence!=null){
			int sequenceSize = eventSequence.size();
			MarkovSequenceUnit[] transitionArray = new MarkovSequenceUnit[sequenceSize];
			Set<String> newPrevStates = prevStates;
			for(int i=0;i<sequenceSize;i++){
				transitionArray[i] = constructMarkovSequenceUnit(eventSequence.get(i),newPrevStates,memm);
				if(transitionArray[i]!=null){
					newPrevStates = transitionArray[i].getTargetStates();
				}else{
					return null;
				}
				
			}
			output = new MarkovSequence(sequenceSize,transitionArray, memm.getMyParams().getInitialState());
		}
		return output;
	}
	/**
	 * For a given observation, caculate the matrix of next transition by invoking 
	 * the each maxent-model of previous states.
	 * @param myEvent the current observation
	 * @param prevStates set of the non-zero states of previous obsevation
	 */
	public static MarkovSequenceUnit constructMarkovSequenceUnit(Event myEvent, Set<String> prevStates, Memm memm){
		MarkovSequenceUnit markovSeqUnit = null;
		if(prevStates==null||prevStates.size()==0){
			return null;
		}
		
		if(myEvent!=null){
			TreeMap<String,TreeMap<String,Double>> transitionMatrix = new TreeMap<String,TreeMap<String,Double>>();
			String[] context = myEvent.getContextPred();
			
			Iterator<String> prevStateItr = prevStates.iterator();
			while(prevStateItr.hasNext()){
				String onePrevState = prevStateItr.next();
				//just for testing begin
				if(true){
					
					boolean contain = (isAllOther(myEvent, memm.getAnnotators()));
				//	boolean contain = (isAllOther(myEvent));
					if(contain){
						TreeMap<String,Double> myNextTransition = new TreeMap<String,Double>();
						myNextTransition.put("Other",1.0);
						transitionMatrix.put(onePrevState, myNextTransition );
						continue;
					}
					
				}
				//end testing
				MaxentModel model = memm.getModelPool().getModelMap().get(onePrevState);
				TreeMap<String,Double> myNextTransition = null;
				String nextState = null;
				if(model!=null){
					double[] transition = model.eval(context);//do the prediction
					boolean flag = false;
					for(double oneTransitionProb:transition){
						if(Double.compare(oneTransitionProb, memm.getMyParams().getThresholdMax())>0){
							flag = true;
						}
					}
					myNextTransition = new TreeMap<String,Double>();
					for(int i=0;i<transition.length;i++){
						if(flag){
							if(Double.compare(transition[i], memm.getMyParams().getThresholdMax())>0){
								nextState  = model.getOutcome(i);								
								myNextTransition.put(nextState, Double.valueOf(transition[i]));
								
								break;
							}else{
								continue;
							}
								
						}else{
								if((Double.compare(transition[i], memm.getMyParams().getThresholdMin())>0)){//if no sinking state, then just keep the ones with transition prob>0.1
							//if((Double.compare(transition[i], 0.005)>0)){
							nextState  = model.getOutcome(i);
							
							myNextTransition.put(nextState, Double.valueOf(transition[i]));
							
						}
					}
					
					
				}
				//check there is at least one transition for next step
				if(myNextTransition.size()==0){
					myNextTransition.put("Other",1.0);
				}
					transition = null;
					transitionMatrix.put(onePrevState, myNextTransition );
				}else{
					
					
					nextState = memm.getModelPool().getSingleOutgoingState(onePrevState);
					if(nextState==null){
						nextState = "Other";
					}
					myNextTransition = new TreeMap<String,Double>();
					myNextTransition.put(nextState,1.0);
					transitionMatrix.put(onePrevState, myNextTransition );
				
				}
				
			}
			markovSeqUnit = new MarkovSequenceUnit(myEvent,transitionMatrix );
			
		}
		
		return markovSeqUnit;
	}
	
	
	/**
	 * Utility method to check whether the evidence is with empty opinion
	 * @return
	 */
	private static boolean isAllOther(Event myFeature, String[] annotators){
		
		for(String oneAnnotator:annotators){
			if(myFeature.getContextPredHash().get(oneAnnotator.toString())!=null){
				return false;
			}
		}
		
		return true;
	}
	

}
