/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.forms;

import java.awt.*;
import java.io.IOException;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

/**
 * @author luying chen
 */
public class About extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public About() {
		initComponents();
	}

	private void initComponents() {
		label1 = new JLabel();
		scrollPane1 = new JScrollPane();
		textPane1 = new JTextPane();

		//======== this ========
		setTitle("ROSeAnn v1.0");
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		this.setIconImage(new ImageIcon(getClass().getResource("icons/roseann-logo.png")).getImage());

		//---- label1 ----
		label1.setIcon(new ImageIcon(getClass().getResource("icons/roseann.jpg")));
		label1.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(label1, BorderLayout.NORTH);

		//======== scrollPane1 ========
		{

			//---- textPane1 ----
			textPane1.setContentType("text/html");
			HTMLDocument doc = (HTMLDocument)textPane1.getDocument();
			HTMLEditorKit editorKit = (HTMLEditorKit)textPane1.getEditorKit();
			String text = "<html>\n\t\n\t<body>\n\t\t\n\t\t<p>\n\t\t\t<strong>ROSeAnn v1.0</strong>, is a system for the management of semantic annotations. ROSeAnn provides users with a unified view over the opinion of multiple independent annotators both on text and Web documents. It allows users to understand and reconcile conflicts between annotations via ontology-aware aggregation. ROSeAnn incorporates both supervised aggregation via Maximum-Entropy Markov Models (MEMM), appropriate when representative training data is available, and an unsupervised method based on the notion of weighted-repair (WR). ROSeAnn supports annotation of plain text, (online/offline) Web, and PDF documents.</p>\n\t\t<p>\n\t\t\tFor more details, please visit the project home site:<a href=\"http://diadem.cs.ox.ac.uk/roseann\">http://diadem.cs.ox.ac.uk/roseann</a></p>\n\t\t<hr />\n\t\t<p>\n\t\t\tThis program is licensed to you under the terms of the GNU General Public License Version 2 as published by the Free Software Foundation.</p>\n\t\t<hr /></body>\n</html>";
			
			try {
				editorKit.insertHTML(doc, doc.getLength(), text, 0, 0, null);
			} catch (BadLocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			textPane1.setBackground(Color.white);
			textPane1.setEditable(false);
	
			scrollPane1.setViewportView(textPane1);
		}
		contentPane.add(scrollPane1, BorderLayout.CENTER);
		pack();
		setLocationRelativeTo(getOwner());
		
	}

	
	private JLabel label1;
	private JScrollPane scrollPane1;
	private JTextPane textPane1;
	
}
