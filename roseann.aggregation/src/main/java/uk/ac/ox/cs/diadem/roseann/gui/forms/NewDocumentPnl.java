/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.forms;

import java.awt.*;
import javax.swing.*;



/**
 * 
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class NewDocumentPnl extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3727627514399612457L;
	public NewDocumentPnl() {
		initComponents();
	}

	private void initComponents() {
		//Component initialization 
		label1 = new JLabel();
		panel2 = new JPanel();
		scrollPane7 = new JScrollPane();
		textPane1 = new JTextPane();
		progressBar1 = new JProgressBar();
		toolBar1 = new JToolBar();
		chooseBtn = new JButton();
		processBtn = new JButton();

		//======== this ========
		setLayout(new BorderLayout());

		//---- label1 ----
		label1.setText("Reserved place for other functional buttons");
		label1.setFont(new Font("Arial", Font.PLAIN, 12));
		label1.setForeground(Color.gray);
		add(label1, BorderLayout.NORTH);

		//======== panel2 ========
		{
			panel2.setLayout(new GridBagLayout());
			((GridBagLayout)panel2.getLayout()).columnWidths = new int[] {156, 56, 0};
			((GridBagLayout)panel2.getLayout()).rowHeights = new int[] {207, 24, 0};
			((GridBagLayout)panel2.getLayout()).columnWeights = new double[] {1.0, 1.0, 1.0E-4};
			((GridBagLayout)panel2.getLayout()).rowWeights = new double[] {1.0, 0.0, 1.0E-4};

			//======== scrollPane7 ========
			{

				//---- textPane1 ----
				textPane1.setFont(new Font("Arial", Font.PLAIN, 12));
				textPane1.setText("This is a new text...");
				scrollPane7.setViewportView(textPane1);
			}
			panel2.add(scrollPane7, new GridBagConstraints(0, 0, 2, 1, 0.5, 0.5,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));

			//---- progressBar1 ----
			progressBar1.setStringPainted(true);
			progressBar1.setValue(97);
			progressBar1.setIndeterminate(true);
			progressBar1.setVisible(false);
			panel2.add(progressBar1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
				new Insets(0, 0, 0, 0), 0, 0));

			//======== toolBar1 ========
			{
				toolBar1.setFloatable(false);

				//---- chooseBtn ----
				chooseBtn.setText("Choose");
				chooseBtn.setFont(new Font("Arial", Font.PLAIN, 12));
				chooseBtn.setForeground(new Color(0, 102, 51));
				toolBar1.add(chooseBtn);
				chooseBtn.setVisible(false);

				//---- processBtn ----
				processBtn.setText("Annotate");
				processBtn.setFont(new Font("Arial", Font.PLAIN, 12));
				processBtn.setForeground(new Color(0, 102, 51));
				processBtn.setHorizontalAlignment(SwingConstants.RIGHT);
				toolBar1.add(processBtn);
			}
			panel2.add(toolBar1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.SOUTHEAST, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0));
		}
		add(panel2, BorderLayout.CENTER);
		// End of component initialization 
	}

	// Variables declaration 
	
	private JLabel label1;
	private JPanel panel2;
	private JScrollPane scrollPane7;
	private JTextPane textPane1;
	private JProgressBar progressBar1;
	private JToolBar toolBar1;
	private JButton chooseBtn;
	public JButton processBtn;
	
}
