/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.dataModel;



/**
 * This class defines the data structure of a markov sequence which consists of 
 * an ordered set of transition units.
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class MarkovSequence {
	private int sequenceBufferSize;
	private MarkovSequenceUnit[] mySequence;
	private String initState;
	public MarkovSequence(int size,String initState){
		mySequence = new MarkovSequenceUnit[size];
		this.sequenceBufferSize = size;
		this.initState = initState;
	}
	/**
	 * @param sequenceSize
	 * @param transitionArray
	 */
	public MarkovSequence(int sequenceSize, MarkovSequenceUnit[] transitionArray, String initState) {
		this.sequenceBufferSize = sequenceSize;
		mySequence = transitionArray;
		this.initState = initState;
	}
	/**
	 * @param sequenceBufferSize the sequenceBufferSize to set
	 */
	public void setSequenceBufferSize(int sequenceBufferSize) {
		this.sequenceBufferSize = sequenceBufferSize;
	}
	/**
	 * @return the sequenceBufferSize
	 */
	public int getSequenceBufferSize() {
		return sequenceBufferSize;
	}
	/**
	 * @param mySequence the mySequence to set
	 */
	public void setMySequence(MarkovSequenceUnit[] mySequence) {
		this.mySequence = mySequence;
	}
	/**
	 * @return the mySequence
	 */
	public MarkovSequenceUnit[] getMySequence() {
		return mySequence;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if(mySequence!=null){
			
			int count = 1;
			for(MarkovSequenceUnit oneStep:mySequence){
				sb.append("This is transition "+(count++)+"...");
				sb.append(oneStep);
			}
		}
		return sb.toString();
		
	}
	public void setInitState(String initState) {
		this.initState = initState;
	}
	public String getInitState() {
		return initState==null?"Begin":initState;
	}

}
