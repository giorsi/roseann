/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * There are two major endpoint:
 * 
 * roseann/text ->to annotate a plain text, 
 * all the parameters are otpional except the text parameter that must be specified otherwise a 400 code will be returned
 * 
 * roseann/web ->to annotate a web page, 
 * all the parameters are otpional except the url parameter that must be specified otherwise a 400 code will be returned
 * 
 * Both endpoint offers a GET and POST service, all of them returns the same answer that represents an annotated document,
 * containing the annotations, conflicts, original text, completed annotators and completed aggregators
 */

public class ROSeAnnRestServiceClient {

	private static String textEndpoint = "http://163.1.88.61:9091/roseann/text";
	private static String webEndpoint = "http://163.1.88.61:9091/roseann/web";

	public static void invokeRoseAnnGETRestService() throws Exception{

		//example of text GET service
		String text = "Barack Obama is the new president of United States";
		//timeout in milliseconds, default value 1 minute 
		String timeout="30000";
		//specify whether to reconcile or not, default value is true
		String aggregator="true";
		//specify the annotators. For those which do not require an api key the default value is true,
		//the one which require an api key will not be invoked unless the api key is correctly specified
		String annotators="lupedia," +
				"dbpediaSpotlight," +
				"extractiv,"+
				"yahooAPI,"+
				"illinoisNER,"+
				"stanfordNER,"+
				"zemanta:api_key,"+
				"openCalais:api_key,"+
				"alchemyAPI:api_key,"+
				"saplo_key:api_key,"+
				"saplo_secret:saplo_secret,"+
				"wikimeta:api_key";

		String textUrlRequest=textEndpoint+"?"+"text="+URLEncoder.encode(text,"UTF-8")+"&"+"timeout="+timeout+"&aggregator="+aggregator+
				"&annotators="+annotators;

		//Similarly we can do the same for the web ebdpoint, where instead of a text parameter we have an url parameter

		String url = "http://google.co.uk";;
		String webUrlRequest=webEndpoint+"?"+"url="+URLEncoder.encode(url,"UTF-8")+"&"+"timeout="+timeout+"&aggregator="+aggregator+
				"&annotators="+annotators;

		System.out.println("Text GET request:"+textUrlRequest);
		System.out.println("Web GET request:"+webUrlRequest);
	}

	public static void invokeRoseAnnPOSTRestServiceText() throws Exception{

		//The parameter of the post service are many more, and we can specify all the individual annotators parameters
		//Thes will be specified via a json object, example of a json request can be found in 
		//src/main/resources/uk/ac/ox/cs/diadem/roseann/json_text_request

		//Again all the parameters are optional, except the text/url parameter that is the only one mandatory

		//the json object request contains a collection of "annotators" element, within each element we can specify 
		//all the parameter for an individual annotator. ROSeAnn will invoke only those annotators that have 
		//the annotator element in the json request

		//read the json request for a text request and performs the request
		URL endpoint = new URL(textEndpoint);
		HttpURLConnection handle = (HttpURLConnection) endpoint.openConnection();

		handle.setDoOutput(true);
		handle.setRequestMethod("POST");
		//handle.setConnectTimeout(timeout);
		handle.setRequestProperty("Accept","application/json");
		handle.setRequestProperty("Content-type","application/json");

		StringBuilder data = new StringBuilder();

		String request="";

		//read therequest
		BufferedReader br = new BufferedReader(new InputStreamReader(
				ROSeAnnRestServiceClient.class.getResourceAsStream("json_text_request")));
		String curLine=br.readLine();
		while(curLine!=null){
			request+=curLine+"\n";
			curLine=br.readLine();
		}
		br.close();

		data.append(request);
		handle.addRequestProperty("Content-Length", Integer.toString(data.length()));

		DataOutputStream ostream = new DataOutputStream(handle.getOutputStream());
		ostream.write(data.toString().getBytes());
		ostream.close();

		// get the response.
		BufferedReader rd = new BufferedReader(new InputStreamReader(handle.getInputStream()));
		String line, result = "";

		while ((line = rd.readLine()) != null) {
			result += line;
		}
		handle.disconnect();

		System.out.println(result);

	}

	public static void invokeRoseAnnPOSTRestServiceWeb() throws Exception{

		//read the request for a web endpoint from src/main/resources/uk/ac/ox/cs/diadem/roseann/json_text_request
		//In this case the request is much more compact and much less parameters are specified

		URL endpoint = new URL(webEndpoint);
		HttpURLConnection handle = (HttpURLConnection) endpoint.openConnection();

		handle.setDoOutput(true);
		handle.setRequestMethod("POST");
		//handle.setConnectTimeout(timeout);
		handle.setRequestProperty("Accept","application/json");
		handle.setRequestProperty("Content-type","application/json");

		StringBuilder data = new StringBuilder();

		String request="";

		//read therequest
		BufferedReader br = new BufferedReader(new InputStreamReader(
				ROSeAnnRestServiceClient.class.getResourceAsStream("json_web_request")));
		String curLine=br.readLine();
		while(curLine!=null){
			request+=curLine+"\n";
			curLine=br.readLine();
		}
		br.close();

		data.append(request);
		handle.addRequestProperty("Content-Length", Integer.toString(data.length()));

		DataOutputStream ostream = new DataOutputStream(handle.getOutputStream());
		ostream.write(data.toString().getBytes());
		ostream.close();

		// get the response.
		BufferedReader rd = new BufferedReader(new InputStreamReader(handle.getInputStream()));
		String line = "";
		String result ="";

		while ((line = rd.readLine()) != null) {
			result += line;
		}
		handle.disconnect();

		System.out.println(result);

	}

	public static void main(String[] args) throws Exception{
		invokeRoseAnnGETRestService();
		invokeRoseAnnPOSTRestServiceText();
		invokeRoseAnnPOSTRestServiceWeb();
	}

}
