/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
 
 package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.prediction.decode;

import java.util.*;

import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.dataModel.*;


/**
 * This class implements the Viterbi algorithm to decode a markov sequence, returning 
 * the most probable labelling sequence.
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class ViterbiDecoder implements Decoder {

	/* (non-Javadoc)
	 * @see uk.ac.ox.cs.diadem.roseann.aggregation.MEMM.core.util.Decoder#decode(uk.ac.ox.cs.diadem.roseann.aggregation.MEMM.core.dataModel.MarkovSequence)
	 */
	@Override
	public String[] decode(MarkovSequence markSeq) {
		if(markSeq==null)
			return null;
		int rowNum = markSeq.getSequenceBufferSize();
		ArrayList<HashMap<String,Double>> pathProb = new ArrayList<HashMap<String,Double>>();
		ArrayList<HashMap<String,String>> backPointer = new ArrayList<HashMap<String,String>>();
		
		String[] trackPath = null;
		if(rowNum>0 ){
			
			trackPath = new String[rowNum];
			MarkovSequenceUnit markSeqUnit = markSeq.getMySequence()[0];
			HashMap<String,Double> oneStep = new HashMap<String,Double>();
			HashMap<String,String> oneStepBackpoint = new HashMap<String,String>();
			pathProb.add(oneStep);
			backPointer.add(oneStepBackpoint);
			for(String oneTargetState:markSeqUnit.getTargetStates()){
				oneStep.put(oneTargetState, markSeqUnit.getTransitionProbability(markSeq.getInitState(),oneTargetState));
				oneStepBackpoint.put(oneTargetState, markSeq.getInitState());
			}
			
			double maxProb = 0.0;
			double currentProb = 0.0;
			String maxPreContributer = null;
			for(int i=1;i<rowNum;i++){
				markSeqUnit =  markSeq.getMySequence()[i];
				oneStep = new HashMap<String,Double>();
				oneStepBackpoint = new HashMap<String,String>();
				pathProb.add(oneStep);
				backPointer.add(oneStepBackpoint);
				for(String oneTargetState:markSeqUnit.getTargetStates()){
					maxPreContributer = markSeqUnit.getSourceStates().iterator().next();
					for(String oneSourceState:markSeqUnit.getSourceStates()){
						currentProb = markSeqUnit.getTransitionProbability(oneSourceState,oneTargetState);
						if(Double.compare(currentProb, 0.0)==0){
							continue;
						}else{
							currentProb *= pathProb.get(i-1).get(oneSourceState);
							if(Double.compare(maxProb*10000, currentProb*10000)<0){
								maxProb = currentProb;
								maxPreContributer = oneSourceState;
							}
						}
					}
					oneStep.put(oneTargetState, maxProb);
					oneStepBackpoint.put(oneTargetState, maxPreContributer);
					maxProb = 0.0;
				}
				
			}
			//start decoding
			maxPreContributer = pathProb.get(rowNum-1).keySet().iterator().next();
			maxProb = 0.0;
			for(String target:pathProb.get(rowNum-1).keySet()){
				
				if(Double.compare(maxProb*10000, pathProb.get(rowNum-1).get(target)*10000)<0){
					maxProb = pathProb.get(rowNum-1).get(target);
					maxPreContributer= target;
				}
			}
			trackPath[rowNum-1] = maxPreContributer;
			for(int i=rowNum-2;i>=0;i--){
				trackPath[i] = backPointer.get(i+1).get(trackPath[i+1]);
			}
			
			
			return trackPath;
		}
		return null;
	
	}

	

}
