/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.control;

import java.awt.event.*;
import java.util.*;

import javax.swing.DefaultListModel;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.*;
import javax.swing.table.TableModel;
import javax.swing.tree.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.*;
import uk.ac.ox.cs.diadem.roseann.gui.forms.*;
import uk.ac.ox.cs.diadem.roseann.gui.swing.util.*;
import uk.ac.ox.cs.diadem.roseann.gui.swingComponent.table.ConflTableModel;
import uk.ac.ox.cs.diadem.roseann.gui.swingComponent.tree.*;
import uk.ac.ox.cs.diadem.roseann.ontology.MaterializedOntologyQuery;
import uk.ac.ox.cs.diadem.roseann.util.CompareConflictByStartEnd;
import uk.ac.ox.cs.diadem.roseann.util.Constant.AnnotatorNameAgg;
import uk.ac.ox.cs.diadem.roseann.util.Constant.AnnotatorNameInd;


/**
 * Abstract business class to handling all the business of DocumentViewerPanel for 
 * plain text and web browser modes
 * 
 * The class check which annotators annotated the document and enables their button.
 * It builds the concepts tree (only of the annotated concepts) and calculate the conflict table from 
 * the input annotated table and fills the conflict table
 * @author Luying Chen
 * 
 */
public abstract class AbstractDocumentViewerBusiness extends MouseAdapter implements BusinessInterface, ActionListener, ListSelectionListener{
	static final Logger LOGGER = LoggerFactory.getLogger(AbstractDocumentViewerBusiness.class);
	
	public enum MODEL_TYPE{TextModel, BrowserModel, PDFModel};
	//the form instance that this businiess is taking care of
	DocumentViewerPnl docViewerPnl;
	//the document data model that related to this form and business
	AnnotatedDocumentModel annoDocModel;
	ConceptDetailFrame frame;

	@Override
	public void mouseClicked(MouseEvent e) {

		// for selection events from legend tree
		if (e.getSource() == docViewerPnl.legendTree  ) {
			if(SwingUtilities.isRightMouseButton(e)){
				handleLegendRightClick(e);
			}
		}
	}

	//record the set of annotation concepts of this document
	Set<String> conceptSet;
	//record the set of individual annotators finishing tasks of this document
	Set<String> annotatorSet;
	//record the set of aggregators finishing tasks of this document
	Set<String> aggregatorSet;
	public AbstractDocumentViewerBusiness(DocumentViewerPnl docViewerPnl, MODEL_TYPE modelType) {
		super();
		this.docViewerPnl = docViewerPnl;
		this.annoDocModel = docViewerPnl.getAnnoDocModel();
		this.frame = new ConceptDetailFrame();
		conceptSet =  new HashSet<String>();
		annotatorSet = new HashSet<String>();
		aggregatorSet = new HashSet<String>();
		docViewerPnl.setAnnoDocModel(annoDocModel);
		calConceptAndAnnotatorSetFromModel(modelType);
		registerHooker();
	}
	
	/**
	 * A utility method to fetch all the concepts (known and unknown) occurring in the annotated document
	 */
	protected void calConceptAndAnnotatorSetFromModel(final MODEL_TYPE modelType) {

		if(annoDocModel==null){
			LOGGER.debug(" The annotatedDocumentModel is null.");
			
			return;
		}
				
		final int num = annoDocModel.getKnownAnnotations().size();
		
		if (num>0) {
			
			String  concept = null;
			Set<Annotation> allAnnot = new HashSet<Annotation>();
			final Set<Annotation> annotations = annoDocModel.getKnownAnnotations();
			final Set<Annotation> unknownAnnotations = annoDocModel.getUnknownAnnotation();
			if(annotations!=null){
				allAnnot.addAll(annotations);
			}
			if(unknownAnnotations!=null){
				allAnnot.addAll(unknownAnnotations);
			}
			for (Annotation atom : allAnnot) {

				concept = atom.getConcept();
				conceptSet.add(concept);
				//annotatorSet.add(atom.getOrigin_annotator());
			}
			if(annoDocModel.getCompletedAnnotators()!=null)
				annotatorSet = annoDocModel.getCompletedAnnotators();
			if(annoDocModel.getCompletedAggregators()!=null)
				aggregatorSet = annoDocModel.getCompletedAggregators();
		}
	}

	@Override
	public void registerHooker() {
		docViewerPnl.opencalaisChckbox.addActionListener(this);
		docViewerPnl.alchemyChckbox.addActionListener(this);
		docViewerPnl.dbpediaChckbox.addActionListener(this);
		docViewerPnl.extractivChckbox.addActionListener(this);
		docViewerPnl.zemantaChckbox.addActionListener(this);
		docViewerPnl.goldstandardChckbox.addActionListener(this);
		docViewerPnl.memmChckbox.addActionListener(this);
		docViewerPnl.wrChckbox.addActionListener(this);
		//docViewerPnl.gateChckbox.addActionListener(this);
		docViewerPnl.yahooChckbox.addActionListener(this);
		docViewerPnl.wikimetaChckbox.addActionListener(this);
		docViewerPnl.saploChckbox.addActionListener(this);
		docViewerPnl.lupediaChckbox.addActionListener(this);
		docViewerPnl.stanfordChckbox.addActionListener(this);
		docViewerPnl.illinoisChckbox.addActionListener(this);
		(docViewerPnl.conflictTbl).getSelectionModel().addListSelectionListener(this);

	}
	@Override
	public void initialize() {
		try {
			initLegendTree();
	     	initConflictTable();
			initAnnotatorSelector();

		} catch (Exception e) {

			e.printStackTrace();
		}

	}


	private void initAnnotatorSelector() {
		if(annotatorSet!=null){

			if(!annotatorSet.contains(AnnotatorNameInd.alchemyAPI.toString())){
				docViewerPnl.alchemyChckbox.setEnabled(false);
			}
			if(!annotatorSet.contains(AnnotatorNameInd.dbpediaSpotlight.toString())){
				docViewerPnl.dbpediaChckbox.setEnabled(false);
			}
			if(!annotatorSet.contains(AnnotatorNameInd.extractiv.toString())){
				docViewerPnl.extractivChckbox.setEnabled(false);
			}
			if(!annotatorSet.contains(AnnotatorNameInd.openCalais.toString())){
				docViewerPnl.opencalaisChckbox.setEnabled(false);
			}
			if(!annotatorSet.contains(AnnotatorNameInd.zemanta.toString())){
				docViewerPnl.zemantaChckbox.setEnabled(false);
			}
			//if(!annotatorSet.contains(AnnotatorNameInd.gate.toString())){
			//docViewerPnl.gateChckbox.setEnabled(false);
			//}
			if(!annotatorSet.contains(AnnotatorNameInd.lupedia.toString())){
				docViewerPnl.lupediaChckbox.setEnabled(false);
			}
			if(!annotatorSet.contains(AnnotatorNameInd.saplo.toString())){
				docViewerPnl.saploChckbox.setEnabled(false);
			}
			if(!annotatorSet.contains(AnnotatorNameInd.wikimeta.toString())){
				docViewerPnl.wikimetaChckbox.setEnabled(false);
			}
			if(!annotatorSet.contains(AnnotatorNameInd.yahooAPI.toString())){
				docViewerPnl.yahooChckbox.setEnabled(false);
			}
			if(!annotatorSet.contains(AnnotatorNameInd.stanfordNER.toString())){
				docViewerPnl.stanfordChckbox.setEnabled(false);
			}
			if(!annotatorSet.contains(AnnotatorNameInd.illinoisNER.toString())){
				docViewerPnl.illinoisChckbox.setEnabled(false);
			}
			if(!annotatorSet.contains(AnnotatorNameAgg.gold_standard.toString())){
				docViewerPnl.goldstandardChckbox.setEnabled(false);
			}
			if(!aggregatorSet.contains(AnnotatorNameAgg.memm.toString())){
				docViewerPnl.memmChckbox.setEnabled(false);
			}
			if(!aggregatorSet.contains(AnnotatorNameAgg.wr.toString())){
				docViewerPnl.wrChckbox.setEnabled(false);
			}
		}

	}
	private void initConflictTable() {
		TableModel tableModel = constructConfTblModel();;
		docViewerPnl.conflictTbl.setModel(tableModel);
		//hide the first column which specifies the id of a conflict tuple
		docViewerPnl.conflictTbl.getColumnModel().getColumn(0).setMinWidth(0);
		docViewerPnl.conflictTbl.getColumnModel().getColumn(0).setMaxWidth(0);
		docViewerPnl.conflictTbl.getColumnModel().getColumn(0).setPreferredWidth(0);

	}
	/**
	 * A utility method to construct the model for conflict table, where
	 * the model comes from querying the global model of the annotated docs.
	 */
	public TableModel constructConfTblModel() {
		if(annoDocModel==null){
			return null;
		}
		
		//define the header of the table
		String[] columnNames = {"Id","Snippet",
				"Begin",
				"End",
				"# Omission_Conf.",
		"# Logical_Conf."};
		//specify the model data
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();
				
				Set<Conflict> conflicts = new TreeSet<Conflict>(new CompareConflictByStartEnd());
				final Set<Conflict> confs = annoDocModel.getAllConflicts();
				if(confs!=null){
					conflicts.addAll(confs);
				}
			Set<String> consideredStartEnd=new HashSet<String>();
				String text = annoDocModel.getAnnotatedText();
				
				for(Conflict conflict:conflicts){
					int start = (int) conflict.getStart();
					int end = (int) conflict.getEnd();
					String span ="span_"+start+"_"+end;
					if(consideredStartEnd.contains(span))
						continue;
					consideredStartEnd.add(span);
					
					try{
						//TO VERIFY IF CORRECT
						Vector<Object> newRow = new Vector<Object>();
						newRow.add(span);
						newRow.add("'"+text.substring(start,end)+"'");
						newRow.add(start);
						newRow.add(end);
						newRow.add(annoDocModel.getOmissionConflictByStartEnd(start, end));
						newRow.add(annoDocModel.getLogicalConflictByStartEnd(start, end));
						data.add(newRow);
					}
					catch(Exception e){
						LOGGER.warn("Exception thrown during the creation of conlficts table",e);
					}
				}
		TableModel model = new ConflTableModel(data,columnNames);
		return model;
	}

	private void initLegendTree() throws Exception {
		//Construct model for the tree. By default, the model is generated 
		TreeModel treeModel = constructLegendTreeModel();

		//render the concepts tree iff there's at least one annotation
		if(treeModel!=null){
			//Set renderer of the tree node
			docViewerPnl.legendTree.setCellRenderer(new CheckBoxTreeCellRenderer());
			docViewerPnl.legendTree.setRootVisible(true);
			docViewerPnl.legendTree.setModel(treeModel);
			docViewerPnl.legendTree.addMouseListener(this);

			docViewerPnl.legendTree.setRowHeight(20);
		}

	}

	/**
	 * (need to  overwrite!!!!!!)
	 * A utility method to construct model for the legend tree w.r.t. the
	 * annotation set of the loaded document. Currently just flat one.
	 * @return TreeModel the model of the tree
	 * @throws Exception 
	 */
	private TreeModel constructLegendTreeModel() throws Exception {
		if(conceptSet==null||conceptSet.size()==0){
			return null;
		}
		DefaultMutableTreeNode rootNode = TreeUtil.constructTree(conceptSet);
		DefaultTreeModel model = new DefaultTreeModel(rootNode);
		return model;
	}

	public DocumentViewerPnl getPanel(){
		return this.docViewerPnl;
	}
	private void handleLegendRightClick(MouseEvent e) {
		JTree tree =(JTree) e.getSource();
		int x = e.getX();
		int y = e.getY();
		int row = tree.getRowForLocation(x, y);
		TreePath path = tree.getPathForRow(row);
		if (path != null) {
			CheckBoxTreeNode node = (CheckBoxTreeNode) path
					.getLastPathComponent();
			if (node != null) {
				// get the concept name
				String predicate = node.toString();
				frame.conceptNameLbl.setText(node.toString());
				if(predicate!=null){
					try {
						Set<String> classes = MaterializedOntologyQuery.getEquivalentClassAnnotator(predicate);
						List<String> classesList = new ArrayList<String>();
						classesList.addAll(classes);
						Collections.sort(classesList);
						DefaultListModel<String> modelEqu = new DefaultListModel<String>();

						for(String oneClass:classesList){
							modelEqu.addElement(oneClass);
						}
						frame.equiList.setModel(modelEqu);
						classes = MaterializedOntologyQuery.getDisjointClasses(predicate, false);
						classesList.clear();
						classesList.addAll(classes);
						Collections.sort(classesList);
						DefaultListModel<String> modelDisjoint = new DefaultListModel<String>();

						for(String oneClass:classesList){
							modelDisjoint.addElement(oneClass);
						}
						frame.disjointList.setModel(modelDisjoint);
						frame.dispose();
						//  frame.setUndecorated(true);


						frame.setTitle("Concept Detail");

						//  frame.pack();
						frame.setVisible(true);



					} catch (Exception e1) {

						e1.printStackTrace();
					}
				}
			}
		}

	}

}
