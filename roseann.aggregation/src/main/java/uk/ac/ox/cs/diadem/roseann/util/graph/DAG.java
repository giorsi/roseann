/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.util.graph;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 
 * 
 * @author Stefano Ortona (stefano dot ortona at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 * 
 *         Utility class to model a Directed Acyclic Graph.
 *         Each Node contains a label of type T and a weight of type K
 */

public class DAG<T,K> {

	private Map<Node<T>,K> nodes;

	public DAG(){
		this.nodes=new HashMap<Node<T>,K>();

	}

	public Node<T> addNode(T label,K weight){
		Node<T> node=new Node<T>(label);
		if(this.nodes.containsKey(node)){
			for(Node<T> graphNode:this.nodes.keySet()){
				if(graphNode.getLabel().equals(label))
					node=graphNode;
			}
		}
		this.nodes.put(node, weight);
		return node;
	}

	public Node<T> getNode(T label){
		for(Node<T> node:this.nodes.keySet()){
			if(node.getLabel().equals(label))
				return node;
		}

		return null;
	}

	public Collection<Node<T>> getEdges(Node<T> node){
		return node.getNeighbours();
	}

	public Collection<Node<T>> getAllNodes(){
		return this.nodes.keySet();
	}
	
	/**
	 * Method to return all the sources' node
	 * A source node is a node without incoming edge
	 * 
	 * @return
	 * 			A collection of source node
	 */
	public Collection<Node<T>> getSources(){
		final Collection<Node<T>> allNeighbours=new HashSet<Node<T>>();
		for(Node<T> node:this.nodes.keySet()){
			allNeighbours.addAll(node.getNeighbours());
		}

		final Set<Node<T>> sources=new HashSet<Node<T>>();
		for(Node<T> node:this.nodes.keySet()){
			if(!allNeighbours.contains(node))
				sources.add(node);
		}

		return sources;
	}
	
	/**
	 * Method to return all the sinkes' node
	 * A sink node is a node without outgoing edge
	 * 
	 * @return
	 * 			A collection of sink node
	 */
	public Collection<Node<T>> getSinkes(){
		final Collection<Node<T>> sinkes=new HashSet<Node<T>>();

		for(Node<T> node:this.nodes.keySet()){
			if(node.getNeighbours().size()==0)
				sinkes.add(node);
		}
		return sinkes;
	}

	public void addEdge(Node<T> nodeSource,Node<T> nodeDestination){
		if(!this.nodes.keySet().contains(nodeDestination)||!this.nodes.keySet().contains(nodeSource))
			return;

		nodeSource.addNeighbour(nodeDestination);

	}

	public void removeNode(Node<T> node){
		this.nodes.remove(node);
		
		for(Node<T> selectedNode:this.nodes.keySet()){
			this.removeEdge(selectedNode, node);
		}
	}
	
	public void removeEdge(Node<T> node1,Node<T> node2){
		
		node1.getNeighbours().remove(node2);
		
	}
	
	/**
	 * Method to remove form the graph all the nodes which cannot reach a given node
	 * For every node n in the graph, if a path from n to node does not exist remove n from the graph
	 * @param node
	 * 			The input node to be reached
	 */
	public void removeUnreachableNode(Node<T> node){
		
		Collection<Node<T>> toRemove=new HashSet<Node<T>>();
		for(final Node<T> selectedNode:this.nodes.keySet()){
			
			if(!this.getReachableNode(selectedNode).contains(node)){
				toRemove.add(selectedNode);
			}
			
		}
		
		for(final Node<T> selectedNode:toRemove){
			this.nodes.remove(selectedNode);
		}
		
	}

	public K getWeight(Node<T> node){
		return this.nodes.get(node);
	}

	/**
	 * Method to retrieve all reachable node for a give node
	 * @param node
	 * 				The input start-node to calculate all possible path
	 * @return
	 * 				A collection of reachable nodes
	 */
	public Collection<Node<T>> getReachableNode(Node<T> node){
		final Set<Node<T>> toExplore=new HashSet<Node<T>>();
		toExplore.add(node);
		final Set<Node<T>> reachable=new HashSet<Node<T>>();
		
		while(toExplore.size()>0){
			final Node<T> actualNode=toExplore.iterator().next();
			reachable.add(actualNode);
			toExplore.remove(actualNode);
			final Collection<Node<T>> candidateReachable=this.getNeighbours(actualNode);
			candidateReachable.removeAll(reachable);
			toExplore.addAll(candidateReachable);
			
		}
		return reachable;

	}

	public Collection<Node<T>> getNeighbours(Node<T> node){
		Collection<Node<T>> neighbours=new HashSet<Node<T>>();
		neighbours.addAll(node.getNeighbours());
		return neighbours;

	}

	public String toString(){
		String toString="";
		for(Node<T> node:this.nodes.keySet()){
			toString+="["+node.toString()+", label="+this.nodes.get(node)+"]"+"\n";
		}

		return toString;
	}
	
	/**
	 * Method to merge the actual graph with a given one
	 * The method adds all the nodes and all the edges of toMerge graph
	 * If the node already existed in the original graph, the original weight of the node is preserved
	 * @param toMerge
	 * 				Input graph to be merged
	 */
	public void mergeGraph(DAG<T,K> toMerge){

		Collection<Node<T>> allNodes=toMerge.getAllNodes();

		for(Node<T> node:allNodes){
			K weigth=toMerge.getWeight(node);

			Node<T> originalNode=this.getNode(node.getLabel());
			if(originalNode!=null) weigth=this.getWeight(originalNode);
			this.addNode(node.getLabel(),weigth);
		}

		for(Node<T> node:allNodes){
			final T label=node.getLabel();
			final Collection<Node<T>> neighbours=toMerge.getEdges(node);
			final Node<T> addedNode=this.getNode(label);
			for(Node<T> mergeNeighbour:neighbours){
				final Node<T> neighbour=this.getNode(mergeNeighbour.getLabel());
				this.addEdge(addedNode, neighbour);

			}

		}



	}
	/**
	 * Method to return the closest common reachable node given a collection of input nodes
	 * Given an input collection of nodes, the method return all the nodes reachable from all the input
	 * nodes with the shortest path, where the shortest path is calculated as the sum of all the path 
	 * from every input node to the closest reachable ones
	 * @param nodes
	 * 				The input collection of nodes
	 * @return
	 * 				A collection of closest reachable node
	 */
	public Collection<Node<T>> getClosestReachableNode(Collection<Node<T>> nodes){

		final Collection<Node<T>> commonReachableNode=this.getReachableNode(nodes.iterator().next());
		Collection<Node<T>> closestReachableNode=new HashSet<Node<T>>();

		if(nodes.size()==1){
			closestReachableNode.add(nodes.iterator().next());
			return closestReachableNode;
		}

		for(Node<T> node:nodes){
			final Collection<Node<T>> reachableNode=this.getReachableNode(node);
			commonReachableNode.retainAll(reachableNode);
		}

		if(commonReachableNode.size()==0) return null;
		int minimumDistance=Integer.MAX_VALUE;

		for(Node<T> reachable:commonReachableNode){
			int distance=0;

			for(Node<T> source:nodes){
				distance+=this.distance(source, reachable);
			}

			if(distance<=minimumDistance){
				if(distance<minimumDistance){
					closestReachableNode=new HashSet<Node<T>>();
					minimumDistance=distance;

				}
				closestReachableNode.add(reachable);
			}


		}

		return closestReachableNode;



	}

	/**
	 * Method to calculate the shortest distance from a given source node to a given destination node
	 * @param source
	 * 				The source node
	 * @param destination
	 * 				The destionation node
	 * @return
	 * 				Lenght of the shortest path from source to distance
	 */
	public int distance(Node<T> source,Node<T> destination){
		Collection<Node<T>> actualNeighbour=new HashSet<Node<T>>();
		actualNeighbour.add(source);
		Collection<Node<T>> futureNeighbour=null;

		int distance=0;

		boolean found=false;

		while(actualNeighbour.size()>0&&!found){
			futureNeighbour=new HashSet<Node<T>>();
			if(actualNeighbour.contains(destination))
				found=true;
			else{
				distance++;
				for(Node<T> neighbour:actualNeighbour){
					futureNeighbour.addAll(neighbour.getNeighbours());
				}
				actualNeighbour=futureNeighbour;
				if(actualNeighbour.size()==0) return -1;		
			}

		}
		return distance;

	}
	
	/**
	 * Method to calculate the level of depth of an input node in the graph
	 * @param node
	 * 				The input node
	 * @return		The depth level of the node
	 */
	public int getLevelNode(final Node<T> node){
		int level=0;
		Collection<Node<T>> actualNodes=this.getSinkes();
		boolean reachBottom=false;
		while(!reachBottom){
			if(actualNodes.contains(node))
				return level;
			level++;
			Collection<Node<T>> nextNodes=new HashSet<Node<T>>();
			
			for(Node<T> actualNode:actualNodes){
				nextNodes.addAll(this.getUndirectedNeighbour(actualNode));
			}
			actualNodes=nextNodes;
			if(actualNodes.size()==0){
				reachBottom=true;
			}
			
		}
		return -1;
		
		
	}
	
	/**
	 * Method to retrieve all the node at level=level
	 * @param level
	 * 				The input level
	 * @return		Collection of node at such level
	 */
	public Collection<Node<T>> getNodeAtLevel(int level){
		
		final Collection<Node<T>> nodes=new HashSet<Node<T>>();
		
		for(Node<T> node:this.getAllNodes()){
			if(this.getLevelNode(node)==level)
				nodes.add(node);
			
		}
		return nodes;
	}
	
	/**
	 * Method to create a new graph, copy of this graph
	 * @return		A new graph with all the nodes and all the edges
	 */
	public DAG<T,K> createCopy(){
		DAG<T,K> graph=new DAG<T,K>();
		
		for(Node<T> node:this.nodes.keySet()){
			graph.addNode(node.getLabel(), this.nodes.get(node));		
		}
		
		for(Node<T> node:this.nodes.keySet()){
			final Node<T> actualNode=graph.getNode(node.getLabel());
			Collection<Node<T>> neighbours=this.getEdges(node);
			for(Node<T> neighbour:neighbours){
				Node<T> copyNeighbour=graph.getNode(neighbour.getLabel());
				graph.addEdge(actualNode, copyNeighbour);
				
			}
			
		}
		return graph;
	}
	
	/**
	 * Method to return all the undirected neighbor for a given node. A node a is undirected neighbor of a certain node b 
	 * iff b is neighbor of a
	 * @param node
	 * 			the input node
	 * @return	list of undirected neighbor
	 */
	public Collection<Node<T>> getUndirectedNeighbour(Node<T> inputNode){
		Collection<Node<T>> undirectedNeighbor=new HashSet<Node<T>>();
		for(Node<T> node:this.nodes.keySet()){
			if(node.getNeighbours().contains(inputNode))
				undirectedNeighbor.add(node);
		}
		return undirectedNeighbor;
		
		
	}

}
