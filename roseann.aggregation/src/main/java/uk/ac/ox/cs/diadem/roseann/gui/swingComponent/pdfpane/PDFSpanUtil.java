/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.swingComponent.pdfpane;

import java.awt.Point;
import java.util.*;

import uk.ac.ox.cs.diadem.roseann.gui.toolkit.PDFTag;




public class PDFSpanUtil {

	// record handles of annotator tags which are alive
	private Map<Integer, Map<DoubleRectangle, Set<String>>> spanAnnotators;
	// record handles of contept tags which are alive
	private Map<Integer, Map<DoubleRectangle, Set<String>>> spanConcepts;

	public PDFSpanUtil() {
		spanAnnotators = new HashMap<Integer, Map<DoubleRectangle, Set<String>>>();
		spanConcepts = new HashMap<Integer, Map<DoubleRectangle, Set<String>>>();
	}
	
	public Map<Integer, Map<DoubleRectangle, Set<String>>> getSpanAnnotators(){
		return this.spanAnnotators;
	}
	
	public Map<Integer, Map<DoubleRectangle, Set<String>>> getSpanConcepts(){
		return this.spanConcepts;
	}

	public void addAnnotatorTags(Map<Integer, Set<PDFTag>> tags) {
		addTags(spanAnnotators, tags, true);
	}

	public void addConceptTags(HashMap<Integer, Set<PDFTag>> tags) {
		addTags(spanConcepts, tags, false);
	}

	public void removeAnnotatorTags(HashMap<Integer, Set<PDFTag>> tags) {
		removeTags(spanAnnotators, tags, true);
	}

	public void removeConceptTags(HashMap<Integer, Set<PDFTag>> tags) {
		removeTags(spanConcepts, tags, false);
	}

	private void removeTags(
			Map<Integer, Map<DoubleRectangle, Set<String>>> span,
			Map<Integer, Set<PDFTag>> tags, boolean isAnnotator) {
		Iterator<Integer> it = tags.keySet().iterator();

		while (it.hasNext()) {
			int pageNum = it.next();
			Map<DoubleRectangle, Set<String>> recs = span.get(pageNum);

			Set<PDFTag> currentTags = tags.get(pageNum);
			for (PDFTag tag : currentTags) {
				DoubleRectangle rectangle = new DoubleRectangle(tag.getX1(),
						tag.getX2(), tag.getY1(), tag.getY2());
				Set<String> toolTipContents = recs.get(rectangle);
				String content = isAnnotator ? tag.getAnnotator() : tag
						.getConcept();
				toolTipContents.remove(content);
				recs.put(rectangle, toolTipContents);
			}
			span.put(pageNum, recs);
		}
	}

	private void addTags(
			Map<Integer, Map<DoubleRectangle, Set<String>>> span,
			Map<Integer, Set<PDFTag>> tags, boolean isAnnotator) {

		Iterator<Integer> it = tags.keySet().iterator();

		while (it.hasNext()) {
			int pageNum = it.next();
			Map<DoubleRectangle, Set<String>> recs = span.get(pageNum);
			if (recs == null) {
				recs = new HashMap<DoubleRectangle, Set<String>>();
			}

			Set<PDFTag> currentTags = tags.get(pageNum);
			for (PDFTag tag : currentTags) {
				DoubleRectangle rectangle = new DoubleRectangle(tag.getX1(),
						tag.getX2(), tag.getY1(), tag.getY2());

				Set<String> toolTipContents = recs.get(rectangle);
				if (toolTipContents == null) {
					toolTipContents = new HashSet<String>();
				}
				String content = isAnnotator ? tag.getAnnotator() : tag
						.getConcept();
				toolTipContents.add(content);
				recs.put(rectangle, toolTipContents);
			}

			span.put(pageNum, recs);
		}
	}

	class DoubleRectangle {
		private double x1;
		private double x2;
		private double y1;
		private double y2;

		public DoubleRectangle(double x1, double x2, double y1, double y2) {
			this.setX1(x1);
			this.setX2(x2);
			this.setY1(y1);
			this.setY2(y2);
		}

		@Override
		public boolean equals(Object obj) {
			DoubleRectangle rec = (DoubleRectangle) obj;
			return rec.getX1() == x1 && rec.getX2() == x2 && rec.getY1() == y1
					&& rec.getY2() == y2;
		}

		@Override
		public int hashCode() {
			return (int)(x1+x2+y1+y2);
		}
		
		public boolean containts(Point p){
			double verticalShift = y2 - y1;
			return x1<p.x && p.x<x2 && y1<(p.y+verticalShift) && (p.y+verticalShift)<y2;
		}

		public double getX1() {
			return x1;
		}

		public void setX1(double x1) {
			this.x1 = x1;
		}

		public double getX2() {
			return x2;
		}

		public void setX2(double x2) {
			this.x2 = x2;
		}

		public double getY1() {
			return y1;
		}

		public void setY1(double y1) {
			this.y1 = y1;
		}

		public double getY2() {
			return y2;
		}

		public void setY2(double y2) {
			this.y2 = y2;
		}
	}
}
