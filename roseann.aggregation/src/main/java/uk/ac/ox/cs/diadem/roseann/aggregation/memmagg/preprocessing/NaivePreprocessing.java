/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.preprocessing;


import java.io.File;
import java.util.*;
import java.util.concurrent.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.Memm;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.util.ModelAdapter;
import uk.ac.ox.cs.diadem.roseann.api.aggregation.AggregationException;


/**
 * An utility class to pre-process annotated documents into feature-based representation
 * @author Luying Chen
 *
 */
public class NaivePreprocessing {
	
	private Memm employer;
	/**
	 * Logger
	 */
	static final Logger LOGGER = LoggerFactory.getLogger(NaivePreprocessing.class);
	
	public NaivePreprocessing(Memm memm){
		employer = memm;
	}
	
	/**
	 * An util method to harvest annotations given a set of annotated
	 * response from annotators configured.
	 * @param annotatedFile the annotated response with a mask over annotatorName
	 * @param sourceFile the corresponding source text to be annotated
	 * @return Set<Annotation> all the annotations that harvested from the annotated documents
	 */
	public  Set<Annotation> harvestAnnotation(String annotatedFile,File sourceFile){
		 ExecutorService executor = Executors.newFixedThreadPool(employer.getMyParams().getThreadCount());
	      List<Future<Set<Annotation>>> results;
	      Harvester[] wrappers = new  Harvester[employer.getAnnotators().length+1];
	      for(int i=0;i<employer.getAnnotators().length;i++){
	    	  wrappers[i] = new  Harvester(annotatedFile, employer.getAnnotators()[i]); 	  	    
	      }
	      wrappers[wrappers.length-1] = new  Harvester(annotatedFile); 
	     
	    	
	    	  	try {
					results = executor.invokeAll(Arrays.asList(wrappers));
				
				 executor.shutdown();
			        
			      int count=0;
			     Set<Annotation> finalRes = new HashSet<Annotation>();
			      if(results==null){
			    	  return finalRes;
			      }
			      for (Future<Set<Annotation>> resultLst : results) {
			    	  if(resultLst==null){
			    		  continue;
			    	  }
			        	for(Annotation annotation:resultLst.get() ){
			        		
			        				finalRes.add(annotation);
			        				count++;
			        			}
			        		
			        	
			        }
			      System.out.println(annotatedFile+" has "+count+" annotations.") ; 
			     return finalRes;
	    	  	} catch (Exception e) {
	    	  		new HashSet<Annotation>();
				} 
			
	      return new HashSet<Annotation>();
	}
	
	public  void preProcess(String inputSource, String inputAnnotated, String outputRoot){
		
		File outputRootFolder= new File( outputRoot);
		if(!outputRootFolder.exists()){
			outputRootFolder.mkdir();
		}
		File sourceFolder = new File(inputSource);
		LOGGER.info("Start to preprocess the target folder "+ sourceFolder.getName());
		for(File oneFolder:sourceFolder.listFiles()){
			File outputFolder= new File( outputRoot+"/"+oneFolder.getName());
			String subFolderName = oneFolder.getName();
			if(!outputFolder.exists()){
				outputFolder.mkdir();
			}
		     
			System.out.println("Processing folder " + subFolderName);
			for(File oneFile:oneFolder.listFiles()){
				if(!oneFile.getName().endsWith(".txt")){
					continue;
				}
				//System.out.println("Processing document "+oneFile.getName());
				String annotatedFile = inputAnnotated+"/XXX/"+subFolderName+"/"+ oneFile.getName().replace(".txt", ".xml");
				
				File outputFile = new File(outputRoot+"/"+subFolderName+"/"+oneFile.getName().replace(".txt", ".dat"));
				if(outputFile.exists()){
					continue;
				}
				try {
					Set<Annotation> annotations = harvestAnnotation(annotatedFile,oneFile);
					ModelAdapter.convertToEventStreamForDoc(oneFile,annotations,outputFile, employer);
					
				} catch (Exception e) {
					throw new AggregationException(e.getMessage(), LOGGER);
				}
			}
			
		}
		LOGGER.info("Finish to preprocess the target folder "+ sourceFolder.getName());
		
	
		
	}

}
