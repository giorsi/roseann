/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.control;

import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;


import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.AnnotationAttribute;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.AnnotatedDocumentModel;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.AnnotatedPDFModel;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.PdfDoc;
import uk.ac.ox.cs.diadem.roseann.gui.control.AbstractDocumentViewerBusiness.MODEL_TYPE;
import uk.ac.ox.cs.diadem.roseann.gui.forms.*;
import uk.ac.ox.cs.diadem.roseann.gui.swing.util.Console;
import uk.ac.ox.cs.diadem.roseann.gui.swingComponent.pdfpane.PDFViewerPnl;
import uk.ac.ox.cs.diadem.roseann.util.Constant;



/**
 * 
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk), Department of Computer Science, Oxford University
 *
 */
public class PDFLogic {

	public static void visualizeStaticPDFDocument(final MainPageNew entryPage, final MouseEvent e,
			final Logger logger){

		TreePath tp = entryPage.pdfTree.getPathForLocation(
				e.getX(), e.getY());
		entryPage.pdfTree.setSelectionPath(tp);

		if (tp == null) {// when user doesn't click the tree itself
			return;
		}
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) tp
				.getLastPathComponent();

		if(node.isLeaf()){

			Object[] path = node.getUserObjectPath();
			String selectedFileName = "";

			for (int i = 1; i < path.length; i++) {
				selectedFileName += "/" + path[i].toString();
			}

			Console.println("Opening annotated pdf file:"+selectedFileName+"...");

			final String pdfURL = PDFLogic.class.getResource(Constant.PDF_DOCS_ROOT
					+ selectedFileName + selectedFileName
					+ ".pdf").getPath();

			logger.info("Loading file: {}", pdfURL);

			try {
				File originalPdfFile = new File(pdfURL);

				logger.debug("Creating temporary file");
				File tempPdfFile = File.createTempFile(
						FilenameUtils.getName(pdfURL), null);

				FileUtils.copyFile(originalPdfFile, tempPdfFile);

				DocumentViewerPnl newPanel = new DocumentViewerPnl();
				newPanel.visualizeSegmentationPane();

				PDFViewerPnl pdfPanel = new PDFViewerPnl(tempPdfFile);

				entryPage.documentTabPane
				.addTab(selectedFileName,
						new ImageIcon(
								MainPageNew.class
								.getResource("icons/pdf_icon.png")),
								newPanel);
				entryPage.documentTabPane
				.setSelectedIndex(entryPage.documentTabPane
						.getTabCount() - 1);

				logger.info("Annotating the textual content");
				AnnotatedDocumentModel annoDoc = loadStaticPDFDoc(selectedFileName,logger);

				if(annoDoc==null)
					return;

				logger.debug("Visualising the annotated content.");
				newPanel.setAnnoDocModel(annoDoc);
				PDFBusiness pdfBus = new PDFBusiness(newPanel,
						pdfPanel, tempPdfFile,
						originalPdfFile, tempPdfFile.getAbsolutePath(),
						MODEL_TYPE.PDFModel);
				pdfBus.initialize();

			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}



	}
	/**
	 * @param modelURL
	 * @return
	 * @throws IOException 
	 */
	private static AnnotatedPDFModel loadStaticPDFDoc(String fileName,Logger logger) throws IOException {

		logger.info("Reading the static pdf file '{}'",fileName);
		PdfDoc doc = new PdfDoc(new File(PDFLogic.class.getResource(Constant.PDF_DOCS_ROOT+ fileName+fileName+".pdf").getPath()));

		Set<Annotation> knownAnnotations = new HashSet<Annotation>();
		Set<Annotation> unknownAnnotations = new HashSet<Annotation>();
		Set<String> completedAnnotators = new HashSet<String>();
		Set<String> completedAggregators = new HashSet<String>();

		try{
			readStaticFile(fileName, knownAnnotations, completedAnnotators, 
					completedAggregators, unknownAnnotations,logger);
		}
		catch(Exception e){
			logger.warn("Unable to open and visualize the static annotated pdf file '{}'",fileName,e);
		}

		Set<Conflict> conflicts = 
				MainpageBusiness.getRoseAnnInstance().calculateConflicts(knownAnnotations, completedAnnotators);

		AnnotatedPDFModel annPdfModel = new AnnotatedPDFModel
				(knownAnnotations, conflicts, unknownAnnotations, completedAnnotators, 
						doc, completedAggregators);

		return annPdfModel;

	}

	/**
	 * Utility method to read a static file model containing the pdf annotations and turn them into the Java annotation objects model
	 * @param fileName
	 * @param knownAnnotations
	 * @param completedAnnotators
	 * @param completedAggregators
	 * @param unknownAnnotations
	 * @throws IOException
	 */
	private static void readStaticFile(String fileName, Set<Annotation> knownAnnotations, 
			Set<String> completedAnnotators, Set<String> completedAggregators, 
			Set<Annotation> unknownAnnotations,Logger logger) 
					throws IOException{

		BufferedReader br = null;


		String sCurrentLine;

		String file = Constant.PDF_DOCS_ROOT+fileName+"/"+Constant.PDF_STATIC_MODEL_NAME;
		br = new BufferedReader(new InputStreamReader(PDFLogic.class.getResourceAsStream(
				file)));

		Map<String,Annotation> knownAnnotationMap = new HashMap<String,Annotation>();
		Map<String,Annotation> unknownAnnotationMap = new HashMap<String,Annotation>();

		while ((sCurrentLine = br.readLine()) != null) {

			try{
				String beginLine = sCurrentLine.substring(0,sCurrentLine.indexOf("("));
				switch (beginLine) {
				case "KnownAnnotation":{
					sCurrentLine = sCurrentLine.replace("KnownAnnotation(", "");
					sCurrentLine = sCurrentLine.replace(")", "");
					String []split = sCurrentLine.split(",");
					Annotation knownAnnotation = new EntityAnnotation();
					knownAnnotation.setId(split[0]);
					knownAnnotation.setConcept(split[1]);
					knownAnnotation.setAnnotationClass(AnnotationClass.valueOf(split[2].toUpperCase()));
					knownAnnotation.setOriginAnnotator(split[3]);
					if(!split[4].equals("null"))
						knownAnnotation.setAnnotationSource(split[4]);
					if(!split[5].equals("null"))
						knownAnnotation.setConfidence(Double.parseDouble(split[5]));
					knownAnnotation.setStart(Long.parseLong(split[6]));
					knownAnnotation.setEnd(Long.parseLong(split[7]));
					knownAnnotations.add(knownAnnotation);
					knownAnnotationMap.put(knownAnnotation.getId(), knownAnnotation);
					break;
				}
				case "UnknownAnnotation": { 
					sCurrentLine = sCurrentLine.replace("KnownAnnotation(", "");
					sCurrentLine = sCurrentLine.replace(")", "");
					String []split = sCurrentLine.split(",");
					Annotation unkownAnnotation = new EntityAnnotation();
					unkownAnnotation.setId(split[0]);
					unkownAnnotation.setConcept(split[1]);
					unkownAnnotation.setAnnotationClass(AnnotationClass.valueOf(split[2]));
					unkownAnnotation.setOriginAnnotator(split[3]);
					if(!split[4].equals("null"))
						unkownAnnotation.setAnnotationSource(split[4]);
					if(!split[5].equals("null"))
						unkownAnnotation.setConfidence(Double.parseDouble(split[5]));
					unkownAnnotation.setStart(Long.parseLong(split[6]));
					unkownAnnotation.setEnd(Long.parseLong(split[7]));
					unknownAnnotations.add(unkownAnnotation);
					unknownAnnotationMap.put(unkownAnnotation.getId(), unkownAnnotation);
					break;
				}

				case "KnownAttribute": {
					sCurrentLine = sCurrentLine.replace("KnownAttribute(", "");
					sCurrentLine = sCurrentLine.replace(")", "");
					String []split = sCurrentLine.split(",");
					AnnotationAttribute atribute = new AnnotationAttribute(split[1], split[2]);
					String annotationId = split[0];
					Annotation annotation = knownAnnotationMap.get(annotationId);
					annotation.addAttribute(atribute);
				}
				case "UnKnownAttribute": {
					sCurrentLine = sCurrentLine.replace("UnKnownAttribute(", "");
					sCurrentLine = sCurrentLine.replace(")", "");
					String []split = sCurrentLine.split(",");
					AnnotationAttribute atribute = new AnnotationAttribute(split[1], split[2]);
					String annotationId = split[0];
					Annotation annotation = unknownAnnotationMap.get(annotationId);
					if(annotation!=null){
						annotation.addAttribute(atribute);
					}
				}
				case "CompletedAggregators": {
					sCurrentLine = sCurrentLine.replace("CompletedAggregators(", "");
					sCurrentLine = sCurrentLine.replace(")", "");
					String []split = sCurrentLine.split(",");
					for(String annotator:split)
						completedAggregators.add(annotator);
					break;
				}
				case "CompletedAnnotators": {
					sCurrentLine = sCurrentLine.replace("CompletedAnnotators(", "");
					sCurrentLine = sCurrentLine.replace(")", "");
					String []split = sCurrentLine.split(",");
					for(String annotator:split)
						completedAnnotators.add(annotator);
					break;
				}
				default : {
					logger.warn("There is un unknwon line '{}' in the static pdf file model",sCurrentLine);
					break;
				}
				}

			} catch(Exception e){
				logger.warn("Error while reading the static pdf model file",e);
			}
		}


		if (br != null)br.close();
	}

	/**
	 * Utility method that given a folder containing the pdf file, annotates it and save the annotations in a static file model
	 * @throws IOException 
	 */
	private static void createStaticAnnotationModel(File pdfFolder) throws IOException{

		File pdfFile = new File(pdfFolder.getAbsolutePath()+"/"+pdfFolder.getName()+".pdf");
		AnnotatedPDFModel model = MainpageBusiness.getRoseAnnInstance().annotateEntityPDF(pdfFile, true);

		File toWrite = new File(pdfFolder.getAbsolutePath()+"/"+Constant.PDF_STATIC_MODEL_NAME);

		FileWriter fw = new FileWriter(toWrite.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);

		//write the known annotation
		for(Annotation annotaiton:model.getKnownAnnotations()){
			bw.write("KnownAnnotation("+annotaiton.getId()+","
					+annotaiton.getConcept()+","
					+annotaiton.getAnnotationClass()+","
					+annotaiton.getOriginAnnotator()+","
					+annotaiton.getAnnotationSource()+","
					+annotaiton.getConfidence()+","
					+annotaiton.getStart()+","
					+annotaiton.getEnd()+")\n");

			//write the attribute
			for(AnnotationAttribute attribute:annotaiton.getAttributes()){
				if(attribute.getValue()==null||attribute.getValue().length()==0)
					continue;
				String newVaue = attribute.getValue().replaceAll("\n", " ");
				bw.write("KnownAttribute("+
						annotaiton.getId()+","
						+attribute.getName()+","
						+newVaue+")\n");
			}

		}

		//write the known annotation
		for(Annotation annotaiton:model.getUnknownAnnotation()){
			bw.write("UnknownAnnotation("+annotaiton.getId()+","
					+annotaiton.getConcept()+","
					+annotaiton.getAnnotationClass()+","
					+annotaiton.getOriginAnnotator()+","
					+annotaiton.getAnnotationSource()+","
					+annotaiton.getConfidence()+","
					+annotaiton.getStart()+","
					+annotaiton.getEnd()+")\n");

			//write the attribute
			for(AnnotationAttribute attribute:annotaiton.getAttributes()){
				if(attribute.getValue()==null||attribute.getValue().length()==0)
					continue;
				String newVaue = attribute.getValue().replaceAll("\n", " ");
				bw.write("UnKnownAttribute("+
						annotaiton.getId()+","
						+attribute.getName()+","
						+newVaue+")\n");
			}

		}

		if(model.getCompletedAggregators().size()>0){
			bw.write("CompletedAggregators(");
			//write the completed aggregators
			int aggregatorSize = model.getCompletedAggregators().size();
			for(String annotator:model.getCompletedAggregators()){
				if(aggregatorSize==1)
					bw.write(annotator+")");
				else
					bw.write(annotator+",");
				aggregatorSize--;
			}
			bw.write("\n");
		}

		if(model.getCompletedAnnotators().size()>0){
			bw.write("CompletedAnnotators(");
			//write the completed aggregators
			int aggregatorSize = model.getCompletedAnnotators().size();
			for(String annotator:model.getCompletedAnnotators()){
				if(aggregatorSize==1)
					bw.write(annotator+")");
				else
					bw.write(annotator+",");
				aggregatorSize--;
			}
			bw.write("\n");
		}

		bw.close();
	}

	public static void main(String[] args) throws IOException{
		File pdfFileFolder = new 
				File("/home/stefano/Documents/DIADEM/Developing/" +
						"workspace/roseann/src/main/resources/uk/ac/ox/cs/diadem/roseann/gui/control/samplePDF");
		for(File subFolder:pdfFileFolder.listFiles()){
			System.out.println("----------------------------------Folder:"+subFolder.getName());
			createStaticAnnotationModel(subFolder);
		}
	}

}
