/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.ontology;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.MissingPropertyConfigurationRuntimeException;
import uk.ac.ox.cs.diadem.roseann.util.graph.DAG;
import uk.ac.ox.cs.diadem.roseann.util.graph.Node;

/**
 * 
 * @author Stefano Ortona (stefano dot ortona at cs dot ox dot ac dot uk) - Department of Computer Science - University
 *         of Oxford
 * 
 *         Class to query a materialized version of the Ontology, thus each different statement (subclass, disjoint,
 *         equivalent...) must be stored in a specific file The folder containing files for the materialized version can
 *         be specified in the conf configuration file, or at runtime by using the method setOntologyMaterializedFolder
 *         The default materialized ontology can be found in
 *         src/main/resources/uk/ac/ox/cs/diadem/roseann/ontology/materializedOntology
 * 
 *         If a new folder is set, the file names conventions inside the folder must be exactly the same to the one of
 *         the default folder
 * 
 */

public class MaterializedOntologyQuery {

	/**
	 * Logger
	 */
	static final Logger logger = LoggerFactory.getLogger(MaterializedOntologyQuery.class);

	/**
	 * Top concept
	 */
	private static String top;

	/**
	 * Maps to save in memory the information retrieved from the Ontology
	 */
	private final static Map<String, DAG<String, Integer>> type2graph = new HashMap<String, DAG<String, Integer>>();

	private final static Map<String, String> type2globaltype = new HashMap<String, String>();

	private final static Map<String, Set<String>> type2supertype = new HashMap<String, Set<String>>();

	private final static Map<String, Set<String>> type2annotatorsupertype = new HashMap<String, Set<String>>();

	private final static Map<String, Set<String>> type2disjoint = new HashMap<String, Set<String>>();

	private final static Map<String, Set<String>> type2asserteddisjoint = new HashMap<String, Set<String>>();

	private final static Map<String, Set<String>> type2equivalentClass = new HashMap<String, Set<String>>();

	private final static Map<String, Set<String>> type2implicants = new HashMap<String, Set<String>>();

	private final static Map<String, Set<String>> annotator2types = new HashMap<String, Set<String>>();

	private final static Map<String, Set<String>> type2subclassKnowledge = new HashMap<String, Set<String>>();

	private final static Map<String, Set<String>> type2superclassKnowledge = new HashMap<String, Set<String>>();

	private final static Map<String, Set<String>> type2directedsuperclasses = new HashMap<String, Set<String>>();

	private final static Map<String, Set<String>> type2directedsubclasses = new HashMap<String, Set<String>>();

	private final static Map<String, String> type2dbpediaUri = new HashMap<String, String>();

	private static String dbpediaAnnotator;

	private static String dbpediaPrefix;

	private final static String DEFAULT_CONFIG = "conf/Configuration.xml";

	/**
	 * Variable to store the path of the materialized ontology folder
	 */
	private static String materializedOntologyFolder;

	/**
	 * Inizialize the materialized ontology folder variable by reading the configuration file
	 */
	static {


		final String confProperties = "nlp.annotator.textannotator.ontology.materialized_folder_path";
		// read from the configuration file the default path for the materialized version of the ontology value
		try {
			XMLConfiguration config = new XMLConfiguration(new File(DEFAULT_CONFIG));
			materializedOntologyFolder = config.getString(confProperties);
			// read the superclasses file and retrieve the top concept
			retrieveTopConcept();
		} catch (final MissingPropertyConfigurationRuntimeException e) {
			logger.warn("The materialized file folder is not specified in the " + "configuration file conf/ with path "
					+ confProperties);
		}
		catch (final ConfigurationException e) {
			logger.warn("The default configuration file {} does not exist",DEFAULT_CONFIG);
		}

		retrieveDbpediaInformation();

	}

	private static void retrieveDbpediaInformation() {

		// read from the configuration file the dbpedia annotator and the dbpedia url prefix
		try {
			XMLConfiguration config = new XMLConfiguration(new File(DEFAULT_CONFIG));
			dbpediaAnnotator = config.getString(
					"nlp.annotator.textannotator.ontology.dbpedia.annotator_name");
			dbpediaPrefix = config.getString(
					"nlp.annotator.textannotator.ontology.dbpedia.uri");

		} catch (final Exception e) {
			logger.warn("The dbpedia annotator or dbpedia URI prefix are not specified in the "
					+ "configuration file {}",DEFAULT_CONFIG);
		}

	}

	/**
	 * Utility method to retrieve the top concept, i.e. the concept which has only itself as superclass
	 */
	private static void retrieveTopConcept() {

		final String fileName = "all_supertype";

		final BufferedReader br = getReader(fileName);
		try {

			String line = br.readLine();

			while (line != null) {
				final String concept = line.split("\t")[0];
				if (line.equals(concept + "\t" + concept)) {
					top = concept;
					break;
				}
				line = br.readLine();
			}

			br.close();
		} catch (final Exception e) {
			throw new OntologyException("Error while trying to read the materialized file " + fileName + " of the ontology",
					e, logger);
		} finally {

			try {
				br.close();
			} catch (final IOException e) {
				throw new OntologyException(
						"Error while trying to read the materialized file " + fileName + " of the ontology", e, logger);
			}
		}

		if (top == null)
			throw new OntologyException("The materialized file " + fileName + " for the Ontology"
					+ " does not contain the top concept, i.e. the concept which has only" + " itself as superclass", logger);

	}

	private static String readFile(final String fileName, final String inputLine) throws OntologyException {

		String result = null;
		final BufferedReader br = getReader(fileName);

		try {
			String line = br.readLine();

			while (line != null && result == null) {
				if (line.startsWith(inputLine + "\t")) {
					line = line.replaceAll(inputLine + "\t", "");
					result = line;
				}
				line = br.readLine();
			}

			br.close();
		} catch (final Exception e) {
			throw new OntologyException("Error while trying to read the materialized file " + fileName + " of the ontology",
					e, logger);
		} finally {

			try {
				br.close();
			} catch (final IOException e) {
				throw new OntologyException(
						"Error while trying to read the materialized file " + fileName + " of the ontology", e, logger);
			}
		}

		if (result == null) {
			logger.warn("Couln't find the line {} for the given materialized ontology file {}", inputLine, fileName);
		}
		return result;

	}

	/**
	 * Method to retrieve the global type associated to a specific annotator's type
	 * 
	 * @param originalType
	 *          The original type name
	 * @param annotatorName
	 *          The annotator name
	 * @return The global type name, null if the input original_type is unknown
	 */
	public static String getGlobalType(final String originalType, final String annotatorName) {

		logger.info("Retrieving the global type for {} coming from annotator {}", originalType, annotatorName);

		if (type2globaltype.containsKey(annotatorName + ":" + originalType))
			return type2globaltype.get(annotatorName + ":" + originalType);

		String globalType = null;

		final String file = "global_concept_mapping";
		globalType = readFile(file, annotatorName + ":" + originalType);

		if (globalType == null)
			return null;

		// store the retrieved type in memory
		type2globaltype.put(annotatorName + ":" + originalType, globalType);

		logger.info("Global type {} retrieved", globalType);

		return globalType;
	}

	/**
	 * Method to retrieve all the supertypes for a given type
	 * 
	 * @param type
	 *          The input global type
	 * @return The set of all supertypes, null if the input type is unknown
	 */
	public static Set<String> getAllSupertypes(final String type) {
		logger.info("Retrieving the supertypes for {}", type);

		Set<String> supertypes = type2supertype.get(type);
		final Set<String> supertypesCopy = new HashSet<String>();
		// if the supertypes are saved in memory, return a copy
		if (supertypes != null) {
			supertypesCopy.addAll(supertypes);
			return supertypesCopy;
		}

		// if not, retrieve the supertypes
		supertypes = new HashSet<String>();

		final String file = "all_supertype";
		final String allSupertypes = readFile(file, type);
		if (allSupertypes == null)
			return null;

		final String[] allSupertypesSplit = allSupertypes.split(",");
		for (final String supertype : allSupertypesSplit) {
			supertypes.add(supertype);
		}

		// save the retrieve types in memory
		supertypesCopy.addAll(supertypes);
		type2supertype.put(type, supertypes);

		logger.info("Supertypes {} retrieved", supertypesCopy);

		return supertypesCopy;

	}

	/**
	 * Method to retrieve all the annotator supertypes for a given annotator type
	 * 
	 * @param type
	 *          The input annotator type
	 * @param annotatorName
	 *          The annotator name
	 * @return The set of all supertypes, null if the input type is unknown
	 */
	public static Set<String> getAllSupertypesAnnotator(final String type, final String annotatorName) {
		logger.info("Retrieving the annotator supertypes for {}", type);

		Set<String> supertypes = type2annotatorsupertype.get(type);
		final Set<String> supertypesCopy = new HashSet<String>();
		// if the supertypes are saved in memory, return a copy
		if (supertypes != null) {
			supertypesCopy.addAll(supertypes);
			return supertypesCopy;
		}

		// if not, retrieve the supertypes
		supertypes = new HashSet<String>();

		final String allSupertypes = readFile(annotatorName + "_supertype", type);
		if (allSupertypes == null)
			return null;

		final String[] allSupertypesSplit = allSupertypes.split(",");
		for (final String supertype : allSupertypesSplit) {
			supertypes.add(supertype);
		}

		// save the retrieve types in memory
		supertypesCopy.addAll(supertypes);
		type2supertype.put(type, supertypes);

		logger.info("Supertypes {} retrieved", supertypesCopy);

		return supertypesCopy;

	}

	/**
	 * Method to retrieve the disjoint classes for a given type If the parameter withInference is set to true, then also
	 * the inferred disjoint classes are returned
	 * 
	 * @param type
	 *          The input global type
	 * @param withInference
	 *          Flag to enable the inference
	 * @return The set of disjoint classes, null if the input type is unknown
	 */
	public static Set<String> getDisjointClasses(final String type, final boolean withInference) {

		logger.info("Retrieving the disjoint classes for {}", type);

		if (type == null)
			return null;

		Set<String> disjoint = null;

		if (withInference) {
			disjoint = type2disjoint.get(type);
		} else {
			disjoint = type2asserteddisjoint.get(type);
		}

		final Set<String> returnDisjoint = new HashSet<String>();

		// if disjoint classes are in memory, return a copy
		if (disjoint != null) {
			returnDisjoint.addAll(disjoint);
			return returnDisjoint;
		}

		disjoint = new HashSet<String>();

		String file = null;
		if (withInference) {
			file = "disjoint_classes";
		} else {
			file = "asserted_disjoint_classes";
		}
		final String allDisjoint = readFile(file, type);
		if (allDisjoint == null)
			return null;
		if (allDisjoint.length() > 0) {
			final String[] disjointTypeSplit = allDisjoint.split(",");
			for (final String disjontType : disjointTypeSplit) {
				disjoint.add(disjontType);
			}
		}

		// save the retrieved types in memory
		if (withInference) {
			type2disjoint.put(type, disjoint);
		} else {
			type2asserteddisjoint.put(type, disjoint);
		}
		returnDisjoint.addAll(disjoint);

		logger.info("Disjoint classes {} retrieved", returnDisjoint);

		return returnDisjoint;
	}

	/**
	 * Method to build the graph for a given type. Starting from the input type, we retrieve all the superclasses fo the
	 * type, each superclass is a new node in the graph. Eventually there is an edge between two nodes a and b if a is a
	 * subClass of b.
	 * 
	 * @param type
	 *          The input type
	 * @return The graph built with all the superclasses, null if the input type is unknown
	 */
	public static DAG<String, Integer> getSuperclassesGraph(final String type) {

		logger.info("Retrieving the superclass graph for {}", type);

		DAG<String, Integer> finalGraph = type2graph.get(type);

		if (finalGraph == null) {

			finalGraph = new DAG<String, Integer>();

			final Node<String> startNode = finalGraph.addNode(type, 0);

			// check if the concept is in the ontology
			final Set<String> supertypes = getAllSupertypes(type);
			// return null if the type is not in the ontology
			if (supertypes == null)
				return null;

			// add all the superclasse node
			addSuperclassesNode(startNode, finalGraph);

			// add the top supertype only if the type is not top
			if (!type.equalsIgnoreCase(top)) {

				// get all the sinkes and for each sink create an edge between the sink and top node
				final Collection<Node<String>> sinks = finalGraph.getSinkes();

				final Node<String> topNode = finalGraph.addNode(top, 0);

				for (final Node<String> sink : sinks) {
					finalGraph.addEdge(sink, topNode);
				}
			}
		}

		type2graph.put(type, finalGraph);

		logger.info("Superclass graph {} retrieved", finalGraph);

		// return a copy of the graph
		return finalGraph.createCopy();
	}

	/**
	 * Utiliy method to retrieve all the supertypes for a given node in a graph and add all the supertypes as neighbor
	 * 
	 * @param startNode
	 *          The input node
	 * @param graph
	 *          The graph where the node belongs
	 */
	private static void addSuperclassesNode(final Node<String> startNode, final DAG<String, Integer> graph) {

		final String type = startNode.getLabel();

		final Set<String> directSupertypes = getDirectSuperclasses(type);

		for (final String supertype : directSupertypes) {

			// if the direct superclass is top, I retrieved all the superypes and built the graph
			if (supertype.contains(top))
				return;

			// else add the supertype as a neighbor node and recursevely build all the graph until the root concept
			else {

				final Node<String> neighbour = graph.addNode(supertype, 0);

				graph.addEdge(startNode, neighbour);

				addSuperclassesNode(neighbour, graph);
			}

		}

	}

	/**
	 * Method to retrieve all the annotators which directly know a specific class
	 * 
	 * @param type
	 *          The input class
	 * @return The set of annotators which know that class, null if the type is unknown
	 */
	public static Set<String> getEquivalentClassAnnotator(final String type) {

		logger.info("Retrieving the equivalent class annotator for {}", type);

		Set<String> equivalent = type2equivalentClass.get(type);
		final Set<String> equivalentCopy = new HashSet<String>();
		if (equivalent == null) {
			equivalent = new HashSet<String>();
		} else {
			equivalentCopy.addAll(equivalent);
			return equivalentCopy;
		}

		final String file = "equivalent_class_annotator";
		final String allAnnotators = readFile(file, type);
		if (allAnnotators == null)
			return null;
		final String[] allAnnotatorsSplit = allAnnotators.split(",");
		for (final String annotator : allAnnotatorsSplit) {
			equivalent.add(annotator);
		}

		type2equivalentClass.put(type, equivalent);

		// return the copy
		equivalentCopy.addAll(equivalent);

		logger.info("Equivalent class annotator {} retrieved", equivalentCopy);

		return equivalentCopy;
	}

	/**
	 * Method to retrieve all the classes which imply an input type
	 * 
	 * @param type
	 *          The input type
	 * @return The set of clases which imply the input type, null if the input type is unknown
	 */
	public static Set<String> getImplicants(final String type) {

		logger.info("Retrieving the implicants for {}", type);

		if (type == null)
			return null;

		Set<String> implicants = type2implicants.get(type);
		final Set<String> implicantsCopy = new HashSet<String>();

		// if information already saved in memory, just return a copy
		if (implicants != null) {
			implicantsCopy.addAll(implicants);
			return implicantsCopy;
		}

		implicants = new HashSet<String>();

		final String file = "implicants";
		final String allConcepts = readFile(file, type);
		if (allConcepts == null)
			return null;
		final String[] allConceptsSplit = allConcepts.split(",");
		for (final String concept : allConceptsSplit) {
			implicants.add(concept);
		}

		// return a copy
		implicantsCopy.addAll(implicants);
		type2implicants.put(type, implicants);

		logger.info("Implicants {} retrieved", implicantsCopy);

		return implicantsCopy;

	}

	/**
	 * Method to retrieve all the annotators which know a subclass of the input type
	 * 
	 * @param type
	 *          The input class
	 * @return The set of annotators which know a subclass of the input type, null if the input type is unknown
	 */
	public static Set<String> getSubclassKnowledge(final String type) {

		logger.info("Retrieving the sub class knowledge for {}", type);

		Set<String> knowledge = type2subclassKnowledge.get(type);
		final Set<String> knowledgeCopy = new HashSet<String>();
		if (knowledge == null) {
			knowledge = new HashSet<String>();
		} else {
			knowledgeCopy.addAll(knowledge);
			return knowledgeCopy;
		}

		final String file = "sub_class_knowledge";
		final String allConcepts = readFile(file, type);
		if (allConcepts == null)
			return null;
		final String[] allConceptsSplit = allConcepts.split(",");
		for (final String concept : allConceptsSplit) {
			knowledge.add(concept);
		}

		type2subclassKnowledge.put(type, knowledge);

		// return the copy
		knowledgeCopy.addAll(knowledge);

		logger.info("Sub class knowledge {} retrieved", knowledgeCopy);

		return knowledgeCopy;
	}

	/**
	 * Method to retrieve all the annotators which know a superclass of the input type
	 * 
	 * @param type
	 *          The input class
	 * @return The set of annotators which know a superclass of the input type, null if the input type is unknown
	 */
	public static Set<String> getSuperclassKnowledge(final String type) {

		logger.info("Retrieving the super class knowledge for {}", type);

		Set<String> knowledge = type2superclassKnowledge.get(type);
		final Set<String> knowledgeCopy = new HashSet<String>();
		if (knowledge == null) {
			knowledge = new HashSet<String>();
		} else {
			knowledgeCopy.addAll(knowledge);
			return knowledgeCopy;
		}

		final String file = "super_class_knowledge";
		final String allConcepts = readFile(file, type);
		if (allConcepts == null)
			return null;
		final String[] allConceptsSplit = allConcepts.split(",");
		for (final String concept : allConceptsSplit) {
			knowledge.add(concept);
		}

		type2superclassKnowledge.put(type, knowledge);

		// return the copy
		knowledgeCopy.addAll(knowledge);

		logger.info("Superclass knowledge {} retrieved", knowledgeCopy);

		return knowledgeCopy;
	}

	/**
	 * Method to return all the classes known by an input annotator
	 * 
	 * @param annotatorName
	 *          The input annotator
	 * @return Set of all the classes known by the input annotator, null if the annotator name is unknwon
	 */
	public static Set<String> getAnnotatorKnowledge(final String annotatorName) {

		logger.info("Retrieving the annotator knowledge for {}", annotatorName);

		Set<String> knownTypes = annotator2types.get(annotatorName);
		final Set<String> knownTypesCopy = new HashSet<String>();

		// if information already saved in memory, just return a copy
		if (knownTypes != null) {
			knownTypesCopy.addAll(knownTypes);
			return knownTypesCopy;
		}

		knownTypes = new HashSet<String>();

		final String file = "annotator_knowledge";
		final String allConcepts = readFile(file, annotatorName);
		if (allConcepts == null)
			return null;
		final String[] allConceptsSplit = allConcepts.split(",");
		for (final String concept : allConceptsSplit) {
			knownTypes.add(concept);
		}

		// return a copy and save the information in memory
		knownTypesCopy.addAll(knownTypes);
		type2implicants.put(annotatorName, knownTypes);

		logger.info("Annotator knowledge {} retrieved", knownTypesCopy);

		return knownTypesCopy;
	}

	/**
	 * Method to verify whether two classes are disjoint. Two classes are disjoint if they are in disjoint constraint,or
	 * one of the types is disjoint with a super-class of the other type
	 * 
	 * @param type1
	 *          The first class
	 * @param type2
	 *          The second class
	 * @return True if the two classes are disjoint, false otherwise, null if one of the input type (or both) is unknwon
	 */
	public static Boolean areDisjoint(final String type1, final String type2) {

		logger.info("Retrieving if class {} and class {} are disjint", type1, type2);

		// check if there exist a disjoint constraint between the two classes
		final Set<String> disjoint1 = getDisjointClasses(type1, true);
		if (disjoint1 == null)
			return null;
		if (disjoint1.contains(type2))
			return true;
		final Set<String> disjoint2 = getDisjointClasses(type2, true);
		if (disjoint2 == null)
			return null;
		if (disjoint2.contains(type1))
			return true;

		// check if there exist a disjoint constraint between the superclasses of the first type and the second type
		final Set<String> superclass1 = getAllSupertypes(type1);
		if (superclass1 == null)
			return null;
		final Set<String> intersection = new HashSet<String>();
		intersection.addAll(superclass1);
		intersection.retainAll(disjoint2);
		if (intersection.size() > 0)
			return true;

		// check if there exist a disjoint constraint between the superclasses of the second type and the first type
		final Set<String> superclass2 = getAllSupertypes(type2);
		if (superclass2 == null)
			return null;
		intersection.clear();
		intersection.addAll(superclass2);
		intersection.retainAll(disjoint1);
		if (intersection.size() > 0)
			return true;

		return false;

	}

	/**
	 * Utiliy method to retrieve the directed superclasses for an input type
	 * 
	 * @param start_node
	 *          The input node
	 * @param graph
	 *          The set of directed superclasses, null if the input type is unknown
	 */
	public static Set<String> getDirectSuperclasses(final String type) {

		logger.info("Retrieving the direct superclasses for {}", type);

		Set<String> supertypes = type2directedsuperclasses.get(type);

		final Set<String> supertypesCopy = new HashSet<String>();

		// if the supertypes are saved in memory, return a copy
		if (supertypes != null) {
			supertypesCopy.addAll(supertypes);
			return supertypesCopy;
		}

		// if not, retrieve the supertypes
		supertypes = new HashSet<String>();

		final String file = "direct_superclasses";
		final String allConcepts = readFile(file, type);
		if (allConcepts == null)
			return null;
		if (allConcepts.length() > 0) {
			if (allConcepts != null) {
				final String[] allConceptsSplit = allConcepts.split(",");
				for (final String concept : allConceptsSplit) {
					supertypes.add(concept);
				}
			}
		}

		// save the supertypes and return a copy
		type2directedsuperclasses.put(type, supertypes);
		supertypesCopy.addAll(supertypes);

		logger.info("Direct superclasses {} retrieved", supertypes);

		return supertypesCopy;

	}

	/**
	 * Utiliy method to retrieve the directed subclasses for an input type
	 * 
	 * @param start_node
	 *          The input node
	 * @param graph
	 *          The set of directed subclasses, null if the input type is unknwon
	 */

	public static Set<String> getDirectSubclasses(final String type) {

		logger.info("Retrieving the direct subclasses for {}", type);

		Set<String> subtypes = type2directedsubclasses.get(type);

		final Set<String> subtypesCopy = new HashSet<String>();

		// if the supertypes are saved in memory, return a copy
		if (subtypes != null) {
			subtypesCopy.addAll(subtypes);
			return subtypesCopy;
		}

		// if not, retrieve the supertypes
		subtypes = new HashSet<String>();

		final String file = "direct_subclasses";
		final String allConcepts = readFile(file, type);

		if (allConcepts == null)
			return null;
		if (allConcepts.length() > 0) {
			final String[] allConceptsSplit = allConcepts.split(",");
			for (final String concept : allConceptsSplit) {
				subtypes.add(concept);
			}
		}

		// save the supertypes and return a copy
		type2directedsubclasses.put(type, subtypes);
		subtypesCopy.addAll(subtypes);

		logger.info("Direct subclasses {} retrieved", subtypesCopy);

		return subtypesCopy;
	}

	/**
	 * Utility method to retrieve the top concept
	 * 
	 * @return the top concept
	 */
	public static String getTopConcept() {
		return top;
	}

	/**
	 * Method to set the path to the materialized ontology folder
	 * 
	 * @param folder
	 *          The string path of the materialized ontology folder
	 */
	public static void setOntologyMaterializedFolder(final File folder) {
		materializedOntologyFolder = folder.getAbsolutePath();
		// modify the top concept
		retrieveTopConcept();
		retrieveDbpediaInformation();
	}

	/**
	 * Method to return the dbpedia URI for the given concept. Note that the annotator that is able to identify dbpedia
	 * concepts along with the dbpedia prefix URL must be specified in the configuration file in order to retrieve this
	 * 
	 * @param globalConcept
	 *          The global ontology concept
	 * @return The dbpedia URL when available, null otherwise
	 */
	public static String getDbpediaType(final String globalConcept) {
		if (dbpediaAnnotator == null || dbpediaPrefix == null) {
			logger.info("Impossible to retrieve the dbpedia URL for '{}' since the dbpedia "
					+ "annotator or the dbpedia prefix URL have not been specified in the configuration file", globalConcept);

		}

		logger.info("Retrieving the dbpedia URL for {}", globalConcept);

		if (type2dbpediaUri.containsKey(globalConcept))
			return type2dbpediaUri.get(globalConcept);

		final String fileName = "global_concept_mapping";

		String result = null;
		final BufferedReader br = getReader(fileName);
		try {

			String line = br.readLine();

			while (line != null && result == null) {
				if (line.startsWith(dbpediaAnnotator) && line.endsWith("\t" + globalConcept)) {
					line = line.replaceAll(dbpediaAnnotator + ":", "");
					line = line.replaceAll("\t" + globalConcept, "");
					result = dbpediaPrefix + line;
				}
				line = br.readLine();
			}

			br.close();
		} catch (final Exception e) {
			throw new OntologyException("Error while trying to read the materialized file " + fileName + " of the ontology",
					e, logger);
		} finally {

			try {
				br.close();
			} catch (final IOException e) {
				throw new OntologyException(
						"Error while trying to read the materialized file " + fileName + " of the ontology", e, logger);
			}
		}

		logger.info("Dbpedia URl  for {} retrieved", globalConcept);

		return result;

	}

	/**
	 * Public method to set the path to the materialized folder file
	 * 
	 * @param path
	 *          The String representing the path to be set
	 */
	public static void setMaterializedOntologyFolderFilePath(final String path) {
		materializedOntologyFolder = path;
		// modify the top concept
		retrieveTopConcept();
		retrieveDbpediaInformation();
	}

	private static BufferedReader getReader(final String file) {

		BufferedReader br = null;
		// first try to get the resource as a local resource in the class package
		if (MaterializedOntologyQuery.class.getResourceAsStream(materializedOntologyFolder + "/" + file) != null) {
			br = new BufferedReader(new InputStreamReader(
					MaterializedOntologyQuery.class.getResourceAsStream(materializedOntologyFolder + "/" + file)));
			return br;
		}

		// if the local resource is not available, look in the absolute path
		final String materializedFileName = materializedOntologyFolder + "/" + file;
		final File materializedFile = new File(materializedFileName);

		if (!materializedFile.exists())
			throw new OntologyException("The materialized file " + materializedFileName + " for the Ontology"
					+ " does not exist and therefore the top concept couldn't be retrieved", logger);
		try {
			br = new BufferedReader(new FileReader(materializedFile));
		} catch (final FileNotFoundException e) {
			throw new OntologyException("Error while tryng to read the ontology file " + file, e, logger);
		}
		return br;

	}

}
