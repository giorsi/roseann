/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.control;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.text.*;
import javax.swing.text.DefaultHighlighter.DefaultHighlightPainter;
import javax.swing.tree.*;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.roseann.gui.forms.*;
import uk.ac.ox.cs.diadem.roseann.gui.swing.util.*;
import uk.ac.ox.cs.diadem.roseann.gui.swingComponent.textpane.DocViewTextPane;
import uk.ac.ox.cs.diadem.roseann.gui.swingComponent.tree.CheckBoxTreeNode;



/**
 *  Business class to handling all the business of DocumentViewerPanel
 * @author luying Chen
 * 
 */
public class DocumentViewerBusiness extends AbstractDocumentViewerBusiness {

	public DocumentViewerBusiness(DocumentViewerPnl docViewerPnl, MODEL_TYPE modelType) {
		super(docViewerPnl, modelType);

		conceptHighlightTagHm = new HashMap<String, Set<Object>>();
		annotatorHighlightTagHm = new HashMap<String, Set<Object>>();

	}

	// record handles of concept highlights which are alive
	private Map<String, Set<Object>> conceptHighlightTagHm;
	// record handles of annotator highlights which are alive
	private Map<String, Set<Object>> annotatorHighlightTagHm;

	// record the object of the blinking highlighter
	private Object blinkingHighlightTag;
	
	public void initialize() {

		super.initialize();
		((DocViewTextPane) (docViewerPnl.annotatedDocViewTxtpane)).setAnnoDocModel(annoDocModel);
		docViewerPnl.annotatedDocViewTxtpane.setToolTipText("hello");
		decorateTxtpane(null, null);

	}

	/**
	 * A utility method to decorate the annotated document in the documentViewer
	 * textpane.
	 * 
	 * @param annotators
	 *            the set of annotators targeted at
	 * @param concepts
	 *            the set of concepts targeted at
	 */
	private void decorateTxtpane(Set<String> annotators, Set<String> concepts) {
		if(annoDocModel==null){
			return;
		}
		String showContent = annoDocModel.getAnnotatedText();

		try {
			setTextPaneStyle(showContent);
			// get set of atoms to be highlighted by annotator constraint.
			if (annotators != null) {
				for (String oneAnnotator : annotators) {
					addHighlightByAnnotator(oneAnnotator);

				}
			}
			// get set of atoms to be highlighted by concept constraint.
			if (concepts != null) {
				addHighlightByConcept(concepts);

			}

		} catch (BadLocationException e) {
			System.err
					.println("BadLocationException: Cannot insert text at offset "
							+ e.offsetRequested());
		}

	}

	/**
	 * Add the highlights from document view panel about the annotations of
	 * these concepts
	 * 
	 * @param targets
	 *            the targeting concepts
	 */
	private void addHighlightByConcept(Set<String> targets)
			throws BadLocationException {
		if(annoDocModel==null){
			return;
		}
		
		
		for (String oneConcept : targets) {
			Set<Object> tagsByConcept = this.conceptHighlightTagHm
					.get(oneConcept);
			// if these atoms have not been highlighted, then do highlighing
			if (tagsByConcept == null) {
				Set<Annotation> atomsByConcept = annoDocModel.getAnnotationByConcept(oneConcept);
				DefaultHighlightPainter myHighlightPainter = new RoundRectanglePainter(
						ColorMap.getConceptColor(oneConcept));
				tagsByConcept = addHighlights(atomsByConcept,
						myHighlightPainter,
						docViewerPnl.annotatedDocViewTxtpane);
				docViewerPnl.annotatedDocViewTxtpane.repaint();
				docViewerPnl.annotatedDocViewTxtpane.revalidate();
				conceptHighlightTagHm.put(oneConcept, tagsByConcept);

			}
		}

	}

	/**
	 * Add the highlights from document view panel about the annotations of this
	 * annotator
	 * 
	 * @param oneAnnotator
	 *            the targeting annotator
	 */
	private void addHighlightByAnnotator(String oneAnnotator)
			throws BadLocationException {
		if(annoDocModel==null){
			return;
		}
	
		Set<Object> tagsByAnnotator = this.annotatorHighlightTagHm
				.get(oneAnnotator);
		// if these atoms have not been highlighted, then do highlighting
		if (tagsByAnnotator == null) {
			Set<Annotation> atomsByAnnotator = annoDocModel.getAnnotationByAnnotator(oneAnnotator);
			DefaultHighlightPainter myHighlightPainter = new DefaultHighlightPainter(
					ColorMap.getAnnotatorColor(oneAnnotator));
			tagsByAnnotator = addHighlights(atomsByAnnotator,
					myHighlightPainter, docViewerPnl.annotatedDocViewTxtpane);
			docViewerPnl.annotatedDocViewTxtpane.repaint();
			docViewerPnl.annotatedDocViewTxtpane.revalidate();
			annotatorHighlightTagHm.put(oneAnnotator, tagsByAnnotator);

		}

	}

	/**
	 * A utility method to add highlight for a collection of annotations with
	 * pre-defined highlight painter.
	 * 
	 * @param atoms
	 *            annotation collection
	 * @param myHighlightPainter
	 *            highlight painter specified
	 * @return Set<Object> the collection of handles for the added highlights
	 */
	private Set<Object> addHighlights(Set<Annotation> atoms,
			DefaultHighlightPainter myHighlightPainter, JTextPane txtPane)
			throws BadLocationException {
		Set<Object> tags = new HashSet<Object>();
		for (Annotation oneAtom : atoms) {
			Object tag = docViewerPnl.annotatedDocViewTxtpane.getHighlighter()
					.addHighlight(
							(int)oneAtom.getStart(),
							(int)oneAtom.getEnd(),
							myHighlightPainter);
			tags.add(tag);
		}

		return tags;
	}

	/**
	 * A utility method to insert stylised text content to the textpane
	 * component
	 * 
	 * @param showContent
	 *            the text string
	 */
	private void setTextPaneStyle(String showContent)
			throws BadLocationException {
		StyledDocument paneDoc = docViewerPnl.annotatedDocViewTxtpane
				.getStyledDocument();
		Style def = StyleContext.getDefaultStyleContext().getStyle(
				StyleContext.DEFAULT_STYLE);

		paneDoc.addStyle("regular", def);
		// set line space
		MutableAttributeSet attrs = docViewerPnl.annotatedDocViewTxtpane
				.getInputAttributes();
		StyleConstants.setLineSpacing(attrs, 0.5f);
		paneDoc.setParagraphAttributes(0, showContent.length(), attrs, false);

		String[] content = { showContent };
		String[] initStyles = { "regular" };

		// clean the the textpane
		paneDoc.remove(0, paneDoc.getLength());
		// insert the text content
		for (int i = 0; i < content.length; i++) {
			paneDoc.insertString(paneDoc.getLength(), content[i],
					paneDoc.getStyle(initStyles[i]));
		}
		// set to display from beginning
		docViewerPnl.annotatedDocViewTxtpane.setCaretPosition(0);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		// handle annotator selection events.
		if (ae.getSource() instanceof JCheckBox
				&& ae.getActionCommand().startsWith("annotator_")) {
			String annotator = ae.getActionCommand().replace("annotator_", "");

			try {
				handleAnnotatorSelectionOnText(annotator,
						(JCheckBox) ae.getSource());
			} catch (BadLocationException e) {
				System.err
						.println("BadLocationException: Cannot insert text at offset "
								+ e.offsetRequested());
			}

		}

	}

	/**
	 * Handle annotator selections and re-render the document view panel
	 * 
	 * @param annotator
	 *            the annotator name that user tick/untick
	 * @param chckbox
	 *            the checkbox object related to the annotator
	 */
	private void handleAnnotatorSelectionOnText(String annotator,
			JCheckBox chckbox) throws BadLocationException {
		// get the selection status of the checkbox
		boolean isSelected = chckbox.isSelected();
		final Set<String> annotatorSet = new HashSet<String> ();
		annotatorSet.add(annotator);
		
		if (isSelected) {// if selectd, add all the annotations of this
							// annotator
			if (this.blinkingHighlightTag != null) {
				docViewerPnl.annotatedDocViewTxtpane.getHighlighter()
						.removeHighlight(blinkingHighlightTag);
			}
			addHighlightByAnnotator(annotator);
			((DocViewTextPane) (docViewerPnl.annotatedDocViewTxtpane)).addAnnotatorMember(annotatorSet);
			

		} else {// if de-selected, remove all the annotations of this annotator

			removeHighlightByAnnotator(annotator);
			((DocViewTextPane) (docViewerPnl.annotatedDocViewTxtpane)).removeAnnotatorMember(annotatorSet);

		}

	}

	/**
	 * Remove the highlights from document view panel about the annotations of
	 * this annotator
	 * 
	 * @param annotator
	 *            the annotator name
	 */
	private void removeHighlightByAnnotator(String annotator) {
		Set<Object> tags = annotatorHighlightTagHm.get(annotator);
		if (tags == null) {
			return;
		}
		// remove all the highlights
		for (Object tag : tags) {
			docViewerPnl.annotatedDocViewTxtpane.getHighlighter()
					.removeHighlight(tag);

		}
		docViewerPnl.annotatedDocViewTxtpane.repaint();
		docViewerPnl.annotatedDocViewTxtpane.revalidate();
		// remove all the corresponding tags
		annotatorHighlightTagHm.remove(annotator);

	}

	/**
	 * Remove the annotation highlights from document view panel about the
	 * annotations of these concepts
	 * 
	 * @param targets
	 *            the set of targeting concepts
	 */
	private void removeHighlightByConcept(Set<String> targets) {
		for (String oneConcept : targets) {
			Set<Object> tags = conceptHighlightTagHm.get(oneConcept);
			// remove all the highlights
			if (tags == null) {
				return;
			}
			for (Object tag : tags) {
				docViewerPnl.annotatedDocViewTxtpane.getHighlighter()
						.removeHighlight(tag);

			}
			docViewerPnl.annotatedDocViewTxtpane.repaint();
			docViewerPnl.annotatedDocViewTxtpane.revalidate();
			// remove all the corresponding tags
			conceptHighlightTagHm.remove(oneConcept);
		}

	}

	public Set<String> getConceptSet() {

		return conceptSet;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		super.mouseClicked(e);
		// for selection events from legend tree
		if (e.getSource() == docViewerPnl.legendTree  ) {
			if(SwingUtilities.isLeftMouseButton(e)){
				handleLegendTickEvent(e);
			}
			
		}
	}

	

	private void handleLegendTickEvent(MouseEvent e) {
		JTree tree =(JTree) e.getSource();
		int x = e.getX();
		int y = e.getY();
		int row = tree.getRowForLocation(x, y);
		TreePath path = tree.getPathForRow(row);
		if (path != null) {
			CheckBoxTreeNode node = (CheckBoxTreeNode) path
					.getLastPathComponent();
			if (node != null) {
				boolean isSelected = !node.isSelected();
				Set<String> targets = node.setSelected(isSelected);
				((DefaultTreeModel) tree.getModel())
						.nodeStructureChanged(node);

				// target at himself and all the descendants
				String predicate = node.toString(); // get the concept name

				targets.add(predicate);
				// TreeUtil.getAllDescConcepts(node,targets);
				
				//transform the concepts in the original name
				final Set<String> normalTargets=new HashSet<String>();
				for(String target:targets){
					normalTargets.add(target);
				}
				handleConceptsSelectionOnText(isSelected, normalTargets);

			}
		}
		
	}

	/**
	 * Handle concepts selections and re-render the document view panel
	 * 
	 * @param isSelected
	 *            the status of these concepts (tick/or untick)
	 * @param targets
	 *            the set of concepts targeting
	 */
	private void handleConceptsSelectionOnText(boolean isSelected,
			Set<String> targets) {
		if (isSelected) {

			try {
				if (this.blinkingHighlightTag != null) {
					docViewerPnl.annotatedDocViewTxtpane.getHighlighter()
							.removeHighlight(blinkingHighlightTag);
				}
				this.addHighlightByConcept(targets);
				((DocViewTextPane) (docViewerPnl.annotatedDocViewTxtpane)).addConceptMember(targets);
			} catch (BadLocationException exp) {
				System.err
						.println("BadLocationException: Cannot insert text at offset "
								+ exp.offsetRequested());
			}
		} else {
			removeHighlightByConcept(targets);
			((DocViewTextPane) (docViewerPnl.annotatedDocViewTxtpane)).removeConceptMember(targets);
		}

	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getValueIsAdjusting()) {
			return;
		}
		// for selection events from conflict table
		if (e.getSource() == docViewerPnl.conflictTbl.getSelectionModel()) {
			int selectedRow = docViewerPnl.conflictTbl.getSelectedRows()[0];

			try {
				addBlinkHighlight(selectedRow);
			} catch (BadLocationException exp) {
				System.err
						.println("BadLocationException: Cannot insert text at offset "
								+ exp.offsetRequested());
			}

		}

	}

	/**
	 * A utility method to add blinking highlight to the focused span with
	 * conflicts
	 * 
	 * @throws BadLocationException
	 */
	private void addBlinkHighlight(int selectedRow) throws BadLocationException {
		TableModel model = docViewerPnl.conflictTbl.getModel();
		int begin = Integer.parseInt(model.getValueAt(selectedRow, 2)
				.toString());
		int end = Integer.parseInt(model.getValueAt(selectedRow, 3).toString());
		Rectangle rect = docViewerPnl.annotatedDocViewTxtpane
				.modelToView(begin);

		docViewerPnl.annotatedDocViewTxtpane.scrollRectToVisible(new Rectangle(
				0, rect.y - 5, 1, rect.height + 100));
		docViewerPnl.validate();
		docViewerPnl.repaint();
		// remove previous one
		if (this.blinkingHighlightTag != null) {
			docViewerPnl.annotatedDocViewTxtpane.getHighlighter()
					.removeHighlight(blinkingHighlightTag);
		}
		// create new one
		DefaultHighlightPainter myHighlightPainter = new BlinkPainter(
				Color.CYAN, 300, docViewerPnl.annotatedDocViewTxtpane);
		blinkingHighlightTag = docViewerPnl.annotatedDocViewTxtpane
				.getHighlighter().addHighlight(begin, end, myHighlightPainter);

		docViewerPnl.annotatedDocViewTxtpane.repaint();
		docViewerPnl.annotatedDocViewTxtpane.revalidate();
		((DocViewTextPane) (docViewerPnl.annotatedDocViewTxtpane))
				.setBlinkingSpan(new int[] { begin, end });
		((DocViewTextPane) (docViewerPnl.annotatedDocViewTxtpane))
				.setBlinkSpanID(model.getValueAt(selectedRow, 2).toString()+"_"+model.getValueAt(selectedRow, 3).toString());

	}

}
