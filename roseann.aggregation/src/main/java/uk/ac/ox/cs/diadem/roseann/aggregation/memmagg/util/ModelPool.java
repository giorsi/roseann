/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.util;

import java.io.*;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import opennlp.model.*;
import uk.ac.ox.cs.diadem.roseann.api.aggregation.AggregationException;
import uk.ac.ox.cs.diadem.roseann.util.FileAdapter;


/**
 * A hashmap which keeps the trained models for all the states after loading from file system.
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class ModelPool {
	/**
	 * Logger
	 */
	static final Logger LOGGER = LoggerFactory.getLogger(ModelPool.class);
	
	private Map<String, MaxentModel> modelPool;
	private Map<Integer,String> fullStateHash;
	
	
	private Map<String,String> singleOutgoingState;
	
	public  ModelPool(String poolName, String initStateName)  {
		
		File modelSetFile = new File(poolName+"/model");
		//File statesName = new File(ConfigurationConstant.stateNameFile);
		if(modelSetFile.exists()){
			File[] models = modelSetFile.listFiles();
			modelPool = new HashMap<String, MaxentModel>();
			//statesIndex = new HashMap<String, Integer>();
			//int count = 0;
			for(File oneModel:models){
				MaxentModel model;
				try {
					model = new GenericModelReader(oneModel).getModel();
					modelPool.put(oneModel.getName().replace("Model.txt", ""), model);
					//statesIndex.put(oneModel.getName().replace("Model.txt", ""), count++);
				} catch (IOException e) {
					throw new AggregationException("Something goes wrong with loading trained model "+oneModel.getName()+ "!", LOGGER);					
				}
				
			}
			
		}else{
			throw new AggregationException("The model pool specified as "+modelSetFile.getName()+ " does not exist!", LOGGER);
		}
		File datFile = new File(poolName+"/states.txt");
		if(datFile.exists()){
			int count = 0;
			fullStateHash = new HashMap<Integer,String>();
			fullStateHash.put(count++, initStateName);
			final String[] statesStr = FileAdapter.getPureFileContents(datFile.getAbsolutePath()).split("\n");
			for(String oneState:statesStr){
				oneState = oneState.trim();
				if(!oneState.isEmpty()&&!oneState.equals(initStateName)){
					fullStateHash.put(count++,oneState);
				}
				
			}
		}
		else{
			throw new AggregationException("The model pool specified as "+modelSetFile.getName()+ " loses the materialized state file!", LOGGER);
		}
		File singleStateOutgoingFile = new File(poolName+"/singleTransition.txt");
		if(singleStateOutgoingFile.exists()){
			singleOutgoingState = new HashMap<String,String>();
			String content = FileAdapter.getPureFileContents(singleStateOutgoingFile.getAbsolutePath());
			String[] entries= content.split("\n");
			for(String oneEntry:entries){
				if(!oneEntry.trim().isEmpty()){
					final String[] pair = oneEntry.split(",");
					singleOutgoingState.put(pair[0], pair[1]);
				}
				
			}
		}
		else{
			throw new AggregationException("The model pool specified as "+modelSetFile.getName()+ " loses the materialized single transition file!", LOGGER);
		}
	}

	
	/**
	 * @param
	 * @return
	 */
	public int getStateNum() {
		
		//return modelPool.size();
		if(fullStateHash!=null)
		return fullStateHash.size();
		else
			return 0;
	}

	/**
	 * @param
	 * @return
	 */
	public Map<Integer, String> getFullStateHash() {
		
		return fullStateHash;
	}

	public Map<String, MaxentModel> getModelMap() {
		
		return modelPool;
	}

	public String getSingleOutgoingState(String onePrevState) {
		if(singleOutgoingState!=null){
			return singleOutgoingState.get(onePrevState);
		}
		return null;
	}


	
	

	
}
