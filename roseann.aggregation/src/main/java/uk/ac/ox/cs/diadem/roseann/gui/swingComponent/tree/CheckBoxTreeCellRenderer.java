/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.swingComponent.tree;

import java.awt.*;

import javax.swing.*;
import javax.swing.plaf.ColorUIResource;
import javax.swing.tree.*;

import uk.ac.ox.cs.diadem.roseann.gui.swing.util.ColorMap;


/**
 * To define the check box renderer for legend tree
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class CheckBoxTreeCellRenderer extends JPanel implements TreeCellRenderer {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JCheckBox check;  
	protected JLabel label;  
	//  protected RoundRectLabel colorLbl; 
	public CheckBoxTreeCellRenderer()  
	{  
		setLayout(null);  
		add(check = new JCheckBox());  
		add(label = new JLabel()); 
		//  add(colorLbl = new RoundRectLabel("   ",10,1234));
		check.setBackground(UIManager.getColor("Tree.textBackground"));  
		label.setForeground(UIManager.getColor("Tree.textForeground"));
		label.setIconTextGap(8);

		//  colorLbl.setOpaque(true);
		//  label.setBorder(new TextBubbleBorder(Color.MAGENTA.darker()));
		// label.setBackground(Color.pink);
	}  


	@Override  
	public Component getTreeCellRendererComponent(JTree tree, Object value,  
			boolean selected, boolean expanded, boolean leaf, int row,  
			boolean hasFocus)  
	{  
		String stringValue = tree.convertValueToText(value, selected, expanded, leaf, row, hasFocus);  
		label.setText(stringValue);
		label.setFont(tree.getFont()); 
		/*    if(((CheckBoxTreeNode)value).isRoot()){
        	check.setEnabled(false);
        	return this;
        }*/

		setEnabled(tree.isEnabled()); 
		// check.setEnabled(true);
		//if(!stringValue.equalsIgnoreCase("Thing")){
		if(value instanceof CheckBoxTreeNode)
			check.setSelected(((CheckBoxTreeNode)value).isSelected());  
		//check.setVisible(true);

		//   label.setSelected(selected);  
		//  label.setFocus(hasFocus);  

		label.setIcon(new BoxIcon(ColorMap.getConceptColor(label.getText().trim()),2));

		/*   }else{
        	check.setEnabled(false);
        }*/

		return this;  
	}  

	@Override  
	public Dimension getPreferredSize()  
	{  
		Dimension dCheck = check.getPreferredSize();  
		Dimension dLabel = label.getPreferredSize();  
		// Dimension dColorLabel = colorLbl.getPreferredSize(); 
		return new Dimension(dCheck.width + dLabel.width , dCheck.height < dLabel.height ? dLabel.height: dCheck.height);  
	}


	@Override  
	public void doLayout()  
	{  
		Dimension dCheck = check.getPreferredSize();  
		Dimension dLabel = label.getPreferredSize(); 

		int yCheck = 0;  
		int yLabel = 0;  
		if(dCheck.height < dLabel.height)  
			yCheck = (dLabel.height - dCheck.height) / 2;  
		else  
			yLabel = (dCheck.height - dLabel.height) / 2;  
		check.setLocation(0, yCheck);  
		check.setBounds(0, yCheck, dCheck.width, dCheck.height);  
		label.setLocation(dCheck.width, yLabel);  
		label.setBounds(dCheck.width, yLabel, dLabel.width, dLabel.height);  

	}  

	@Override  
	public void setBackground(Color color)  
	{  
		if(color instanceof ColorUIResource)  
			color = null;  
		super.setBackground(color);  
	}  
}



class BoxIcon implements Icon {
	private Color color;

	//  private int borderWidth;

	BoxIcon(Color color, int borderWidth) {
		this.color = color;
		//  this.borderWidth = borderWidth;
	}

	public int getIconWidth() {
		return 12;
	}

	public int getIconHeight() {
		return 8;
	}

	public void paintIcon(Component c, Graphics g, int x, int y) {
		g.setColor(color);
		//g.fillRoundRect(x, y, getIconWidth(), getIconHeight(), 2, 2);
		g.fillRect(x, y, getIconWidth(), getIconHeight());
		//  g.setColor(color);
		// g.fillRect(x + borderWidth, y + borderWidth, getIconWidth() - 2 * borderWidth,
		//  getIconHeight() - 2 * borderWidth);
	}
}
