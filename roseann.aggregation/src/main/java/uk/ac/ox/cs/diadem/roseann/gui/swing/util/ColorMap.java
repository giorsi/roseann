/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.swing.util;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import uk.ac.ox.cs.diadem.roseann.gui.control.PDFBusiness.SEG_TYPE;
import uk.ac.ox.cs.diadem.roseann.util.Constant.AnnotatorNameAgg;
import uk.ac.ox.cs.diadem.roseann.util.Constant.AnnotatorNameInd;


/**
 * To define the color mapping of all the annotators and concepts
* @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class ColorMap {

	private static Map<String, Color> annotatorColorBag;
	private static Map<SEG_TYPE, Color> segmentationColorBag;
	private static Map<SEG_TYPE, Color> originalSegmentationColorBag;

	private final static int ALPHA = 150;
	static {
		annotatorColorBag = new HashMap<String, Color>();
		annotatorColorBag.put(AnnotatorNameInd.openCalais.toString(), new Color(255, 204, 204, ALPHA));
		annotatorColorBag.put(AnnotatorNameAgg.gold_standard.toString(), new Color(255, 204, 153, ALPHA));
		annotatorColorBag.put(AnnotatorNameInd.alchemyAPI.toString(), new Color(255, 255, 204, ALPHA));
		annotatorColorBag.put(AnnotatorNameInd.extractiv.toString(), new Color(204, 255, 255, ALPHA));
		annotatorColorBag.put(AnnotatorNameInd.dbpediaSpotlight.toString(), new Color(255, 204, 255, ALPHA));
		annotatorColorBag.put(AnnotatorNameInd.wikimeta.toString(), new Color(135, 206, 250, ALPHA));
		annotatorColorBag.put(AnnotatorNameInd.saplo.toString(), new Color(255, 215, 0, ALPHA));
		annotatorColorBag.put(AnnotatorNameInd.yahooAPI.toString(), new Color(143, 188, 143, ALPHA));
		annotatorColorBag.put(AnnotatorNameInd.stanfordNER.toString(), new Color(245, 222, 179, ALPHA));
		annotatorColorBag.put(AnnotatorNameInd.illinoisNER.toString(), new Color(221, 160, 221, ALPHA));
		annotatorColorBag.put(AnnotatorNameInd.lupedia.toString(), new Color(154, 205, 50, ALPHA));
		annotatorColorBag.put(AnnotatorNameInd.zemanta.toString(), new Color(153, 255, 153, ALPHA));
		
		//annotatorColorBag.put(AnnotatorNameInd.gate.toString(), new Color(255, 153, 204, ALPHA));
		annotatorColorBag.put(AnnotatorNameAgg.memm.toString(), new Color(204, 204, 255, ALPHA));
		
		annotatorColorBag.put(AnnotatorNameAgg.wr.toString(), new Color(255, 153, 153, ALPHA));
		/*
		 * conceptColorBag = new HashMap<String, Color>(); conceptColorBag.put("Person", new Color(Color.GRAY.getRed(),
		 * Color.GRAY.getGreen(), Color.GRAY.getBlue(), ALPHA)); conceptColorBag.put("Country", new
		 * Color(Color.GREEN.getRed(), Color.GREEN.getGreen(), Color.GREEN.getBlue(), ALPHA));
		 * conceptColorBag.put("City", new Color(Color.BLUE.getRed(), Color.BLUE.getGreen(), Color.BLUE.getBlue(),
		 * ALPHA)); conceptColorBag.put("CommercialOrg", new Color(Color.MAGENTA.getRed(), Color.MAGENTA.getGreen(),
		 * Color.MAGENTA.getBlue(), ALPHA));
		 */
		
		segmentationColorBag = new HashMap<SEG_TYPE, Color>();
		segmentationColorBag.put(SEG_TYPE.LINE,  new Color(255, 204, 153));
		segmentationColorBag.put(SEG_TYPE.BLOCK, new Color(204, 204, 255));
		segmentationColorBag.put(SEG_TYPE.BLOCKGROUP, new Color(255, 153, 153));
		
		originalSegmentationColorBag = new HashMap<SEG_TYPE, Color>();
		originalSegmentationColorBag.put(SEG_TYPE.LINE, new Color(210, 210, 210));
		originalSegmentationColorBag.put(SEG_TYPE.BLOCK, new Color(180, 180, 180));
		originalSegmentationColorBag.put(SEG_TYPE.BLOCKGROUP, new Color(120, 120, 120));
	}

	public static String getColorString(final Color color) {

		final StringBuffer sb = new StringBuffer("rgba(");
		sb.append(Integer.toString(color.getRed()));
		sb.append(',');
		sb.append(Integer.toString(color.getGreen()));
		sb.append(',');
		sb.append(Integer.toString(color.getBlue()));
		sb.append(',');
		sb.append(Integer.toString(color.getAlpha()));
		sb.append(')');
		return sb.toString();
	}

	public static Color getAnnotatorColor(final String annotator) {

		Color myColor = annotatorColorBag.get(annotator);
		if (myColor == null) {
			myColor = new Color(Color.YELLOW.getRed(), Color.YELLOW.getGreen(), Color.YELLOW.getBlue(), ALPHA);

		}
		return myColor;
	}

	public static Color getConceptColor(final String oneConcept) {
		final String hexColor = Integer.toHexString(Math.abs(oneConcept.hashCode()));
		return Color.decode("#".concat(hexColor));
	}
	
	public static Color getSegmentationColor(final SEG_TYPE segType){
		Color myColor = originalSegmentationColorBag.get(segType);
		if (myColor == null) {
			myColor = new Color(Color.YELLOW.getRed(), Color.YELLOW.getGreen(), Color.YELLOW.getBlue(), ALPHA);
		}
		return myColor;
	}

}
