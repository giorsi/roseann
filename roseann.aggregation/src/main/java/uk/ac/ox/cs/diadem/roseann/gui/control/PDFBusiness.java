/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.control;

import java.awt.Color;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.TableModel;
import javax.swing.text.*;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.PDExtendedGraphicsState;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Block;
import uk.ac.ox.cs.diadem.pdfanalyser.model.BlockGroup;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.AnnotatedPDFModel;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.PdfDoc;
import uk.ac.ox.cs.diadem.roseann.gui.forms.*;
import uk.ac.ox.cs.diadem.roseann.gui.swing.util.*;
import uk.ac.ox.cs.diadem.roseann.gui.swing.util.Console;
import uk.ac.ox.cs.diadem.roseann.gui.swingComponent.pdfpane.*;
import uk.ac.ox.cs.diadem.roseann.gui.swingComponent.table.ConflTableModel;
import uk.ac.ox.cs.diadem.roseann.gui.swingComponent.tree.CheckBoxTreeNode;
import uk.ac.ox.cs.diadem.roseann.gui.toolkit.*;
import uk.ac.ox.cs.diadem.roseann.util.CompareConflictByStartEnd;


public class PDFBusiness extends AbstractDocumentViewerBusiness implements
KeyListener, MouseWheelListener, MouseMotionListener {


	enum VISUAL_TYPE {
		HIGH_LIGHT, MARK
	};

	public enum SEG_TYPE {
		LINE, BLOCK, BLOCKGROUP
	};

	public PDFBusiness(DocumentViewerPnl docViewerPnl,
			PDFViewerPnl pdfViewerPnl,
			File visualFile, File origionalFile, String pdfTarget,
			MODEL_TYPE modelType) throws IOException {
		super(docViewerPnl, modelType);

		registerSegmentationHookers();

		setPdfViewerPnl(pdfViewerPnl);

		pdfViewerPnl.getPagePanel().setAnnoDocModel((AnnotatedPDFModel)annoDocModel);

		setVisualFile(visualFile);

		setOrigionalFile(origionalFile);

		setDocument(PDDocument.load(visualFile));

		setOrigionalDocument(PDDocument.load(origionalFile));

		filePath = pdfTarget;

		highlightedAnnotator = new HashSet<String>();

		highlightedConcept = new HashSet<String>();

		conceptHighlightTagHm = new HashMap<String, HashMap<Integer, Set<PDFTag>>>();

		annotatorHighlightTagHm = new HashMap<String, HashMap<Integer, Set<PDFTag>>>();

		segmentationType = new HashSet<SEG_TYPE>();

		originaLSegmentationType = new HashSet<SEG_TYPE>();

		spanUtil = new PDFSpanUtil();
	}

	// the viewer panel of the pdf file
	private PDFViewerPnl pdfViewerPnl;
	// set of the concepts ticked by the user
	private Set<String> highlightedConcept;
	// set of the annotators ticked by the user
	private Set<String> highlightedAnnotator;
	// record handles of concept highlights which are alive
	private Map<String, HashMap<Integer, Set<PDFTag>>> conceptHighlightTagHm;
	// record handles of annotator highlights which are alive
	private Map<String, HashMap<Integer, Set<PDFTag>>> annotatorHighlightTagHm;
	// set of segmentation types ticked by the user
	private Set<SEG_TYPE> segmentationType;
	private Set<SEG_TYPE> originaLSegmentationType;
	// current pdf file
	private File visualFile;
	// origional pdf file
	private File origionalFile;
	// current pdf document
	private PDDocument visualDocument;
	// origional pdf document
	private PDDocument origionalDocument;
	// file path
	private String filePath;
	// PDFSpan utility
	private PDFSpanUtil spanUtil;

	public void initialize() {
		super.initialize();
		initializeTextViewerPanel();
		initializePDFViewerPanel();
	}

	private void initializeTextViewerPanel() {
		try {
			initSegmentationSelector();
			setTextPaneStyle(annoDocModel.getAnnotatedText());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void registerSegmentationHookers() {
		docViewerPnl.lineChckbox.addActionListener(this);
		docViewerPnl.blockChckbox.addActionListener(this);
		docViewerPnl.blockGroupChckbox.addActionListener(this);
		docViewerPnl.originalLineChckbox.addActionListener(this);
		docViewerPnl.originalBlockChckbox.addActionListener(this);
		docViewerPnl.originalBlockGroupChckbox.addActionListener(this);
	}

	private void initSegmentationSelector() {
		docViewerPnl.lineChckbox.setEnabled(true);
		docViewerPnl.blockChckbox.setEnabled(true);
		docViewerPnl.blockGroupChckbox.setEnabled(true);
		docViewerPnl.originalLineChckbox.setEnabled(true);
		docViewerPnl.originalBlockChckbox.setEnabled(true);
		docViewerPnl.originalBlockGroupChckbox.setEnabled(true);
	}

	private void initializePDFViewerPanel() {
		pdfViewerPnl.setDocument(visualDocument);
		pdfViewerPnl.showPage(0);

		// register
		pdfViewerPnl.getPagePanel().addKeyListener(this);
		pdfViewerPnl.getPagePanel().addMouseMotionListener(this);
		pdfViewerPnl.getPagePanel().addMouseWheelListener(this);

		// if without this the key event listener will only work for windowns
		// now we add key binding so it will also work for Mac
		pdfViewerPnl.getPagePanel().getInputMap()
		.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "down");
		pdfViewerPnl.getPagePanel().getInputMap()
		.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "up");
	}

	/**
	 * A utility method to insert stylised text content to the textpane
	 * component For the web page, it consists the text to annotate in the web
	 * page
	 * 
	 * @param showContent
	 *            the text string
	 */
	private void setTextPaneStyle(String showContent)
			throws BadLocationException {
		StyledDocument paneDoc = docViewerPnl.annotatedDocViewTxtpane
				.getStyledDocument();
		Style def = StyleContext.getDefaultStyleContext().getStyle(
				StyleContext.DEFAULT_STYLE);

		paneDoc.addStyle("regular", def);
		// set line space
		MutableAttributeSet attrs = docViewerPnl.annotatedDocViewTxtpane
				.getInputAttributes();
		StyleConstants.setLineSpacing(attrs, 0.5f);
		paneDoc.setParagraphAttributes(0, showContent.length(), attrs, false);

		// show content according to pages
		String[] content = {showContent};

		String[] initStyles = new String[content.length];
		for (int i = 0; i < content.length; i++) {
			initStyles[i] = "regular";
		}

		// clean the the textpane
		paneDoc.remove(0, paneDoc.getLength());

		paneDoc.insertString(paneDoc.getLength(), content[0],
				paneDoc.getStyle(initStyles[0]));
		//set to display from beginning
		docViewerPnl.annotatedDocViewTxtpane.setCaretPosition(0);
	}

	private void addMarkBySegmentation(SEG_TYPE segType) {
		visualizeSegmentation(segType);
	}

	private void addMarksByConcept(Set<String> targets) {
		if (annoDocModel == null) {
			return;
		}


		for (String oneConcept : targets) {
			HashMap<Integer, Set<PDFTag>> tagsByConcept = this.conceptHighlightTagHm
					.get(oneConcept);

			// if these annotations have not been marked, then do marking
			if (tagsByConcept == null) {
				Set<Annotation> annotationsByConcept = this.annoDocModel.getAnnotationByConcept(oneConcept);
				tagsByConcept = getTags(annotationsByConcept);
				conceptHighlightTagHm.put(oneConcept, tagsByConcept);
			}

			visualizeTags(tagsByConcept, ColorMap.getConceptColor(oneConcept),
					VISUAL_TYPE.MARK);

			// add these tags in tool tips
			spanUtil.addConceptTags(tagsByConcept);
			pdfViewerPnl.getPagePanel().setSpanConcepts(
					spanUtil.getSpanConcepts());
		}

	}

	private void addHighlightByAnnotator(String oneAnnotator) {
		if (annoDocModel == null) {
			return;
		}

		HashMap<Integer, Set<PDFTag>> tagsByAnnotator = this.annotatorHighlightTagHm
				.get(oneAnnotator);
		// if these annotations have not been highlighted, then do highlighting
		if (tagsByAnnotator == null) {

			Set<Annotation> annotationsByAnnotator = annoDocModel.getAnnotationByAnnotator(oneAnnotator);

			tagsByAnnotator = getTags(annotationsByAnnotator);
			annotatorHighlightTagHm.put(oneAnnotator, tagsByAnnotator);
		}
		visualizeTags(tagsByAnnotator,
				ColorMap.getAnnotatorColor(oneAnnotator),
				VISUAL_TYPE.HIGH_LIGHT);

		// add these tags in tool tips
		spanUtil.addAnnotatorTags(tagsByAnnotator);
		pdfViewerPnl.getPagePanel().setSpanAnnotators(
				spanUtil.getSpanAnnotators());

	}

	private void removeMarkBySegmentation() {
		// we only need to clean up all previous marks and repaint
		// everything(both segmentations and annotations)
		// except the removed segmentation type
		visualizeAll();
	}

	private void removeMarksByConcept(Set<String> targets) {
		for (String oneConcept : targets) {
			HashMap<Integer, Set<PDFTag>> tagsByConcept = conceptHighlightTagHm
					.get(oneConcept);
			if (tagsByConcept == null) {
				continue;
			}

			// remove all the highlights.
			// repaint all other annotations but those annotated by the removed
			// concept
			visualizeAll();

			// remove these tags in tool tips
			spanUtil.removeConceptTags(tagsByConcept);
			pdfViewerPnl.getPagePanel().setSpanConcepts(
					spanUtil.getSpanConcepts());
		}

	}

	private void removeHighlightByAnnotator(String oneAnnotator) {
		HashMap<Integer, Set<PDFTag>> tagsByAnnotator = annotatorHighlightTagHm
				.get(oneAnnotator);
		if (tagsByAnnotator == null) {
			return;
		}

		// remove all the highlights.
		// repaint all other annotations but those annotated by the removed
		// annotator
		visualizeAll();

		// remove these tags in tool tips
		spanUtil.removeAnnotatorTags(tagsByAnnotator);
		pdfViewerPnl.getPagePanel().setSpanAnnotators(
				spanUtil.getSpanAnnotators());
	}

	private void visualizeAll() {
		try {
			FileUtils.copyFile(origionalFile, visualFile);
			visualDocument = PDDocument.load(visualFile);
			pdfViewerPnl.setDocument(visualDocument);

			/*
			 * visualize annotations
			 */
			if (highlightedAnnotator.size() == 0
					&& highlightedConcept.size() == 0) {
				visualDocument.save(new FileOutputStream(new File(filePath)));
			}
			// highlight tags by annotator
			for (String annotator : highlightedAnnotator) {
				addHighlightByAnnotator(annotator);
			}
			// mark tags by concept
			addMarksByConcept(highlightedConcept);

			/*
			 * visualize segmentations
			 */
			if (segmentationType.size() == 0
					&& originaLSegmentationType.size() == 0) {
				visualDocument.save(new FileOutputStream(new File(filePath)));
			}
			// mark by segmentation
			for (SEG_TYPE segType : segmentationType) {
				addMarkBySegmentation(segType);
			}
			for(SEG_TYPE segType : originaLSegmentationType){
				addMarkBySegmentation(segType);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (COSVisitorException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	private void visualizeSegmentation(SEG_TYPE segType) {
		if (annoDocModel == null) {
			return;
		}

		Color color = ColorMap.getSegmentationColor(segType);
		List<PDPage> pages = visualDocument.getDocumentCatalog().getAllPages();

		PDPageContentStream contentStream = null;

		PdfDoc model = ((AnnotatedPDFModel)annoDocModel).getPdfDoc();

		int i = 0;
		for (PDPage curPage : pages) {
			float pageHeight = curPage.getMediaBox().getHeight();
			try {
				contentStream = new PDPageContentStream(visualDocument,
						curPage, true, false);
				contentStream.setStrokingColor(color);

				float space = 0;
				switch (segType) {
				case LINE:
					List<Line> lines= model.getLinesByPage(i);
					for (Line seg : lines) {
						float x1 = (float) seg.getLeft();
						float y1 = (float) seg.getTop();
						float x2 = (float) seg.getRight();
						float y2 = (float) seg.getBottom();
						float verticalShift = (float) seg.getVerticalSpace();

						// +space and -space are just for visual style (in order to
						// separate
						// the lines from the text
						x1 = x1 - space;
						x2 = x2 + space;
						y1 = pageHeight - y1 + verticalShift + space;
						y2 = pageHeight - y2 + verticalShift - space;

						contentStream.drawLine(x1, y1, x1, y2);
						contentStream.drawLine(x1, y2, x2, y2);
						contentStream.drawLine(x2, y2, x2, y1);
						contentStream.drawLine(x2, y1, x1, y1);
					}
					break;
				case BLOCK:
					List<Block> blocks= model.getBlockByPage(i);

					space = 1;
					for (Block seg : blocks) {
						float x1 = (float) seg.getLeft();
						float y1 = (float) seg.getTop();
						float x2 = (float) seg.getRight();
						float y2 = (float) seg.getBottom();
						float verticalShift = (float) seg.getVerticalSpace();

						// +space and -space are just for visual style (in order to
						// separate
						// the lines from the text
						x1 = x1 - space;
						x2 = x2 + space;
						y1 = pageHeight - y1 + verticalShift + space;
						y2 = pageHeight - y2 + verticalShift - space;

						contentStream.drawLine(x1, y1, x1, y2);
						contentStream.drawLine(x1, y2, x2, y2);
						contentStream.drawLine(x2, y2, x2, y1);
						contentStream.drawLine(x2, y1, x1, y1);
					}
					break;
				case BLOCKGROUP:
					List<BlockGroup> groups= model.getBlockGroupByPage(i);

					space = 3;
					for (BlockGroup seg : groups) {
						float x1 = (float) seg.getLeft();
						float y1 = (float) seg.getTop();
						float x2 = (float) seg.getRight();
						float y2 = (float) seg.getBottom();
						float verticalShift = (float) seg.getVerticalSpace();

						// +space and -space are just for visual style (in order to
						// separate
						// the lines from the text
						x1 = x1 - space;
						x2 = x2 + space;
						y1 = pageHeight - y1 + verticalShift + space;
						y2 = pageHeight - y2 + verticalShift - space;

						contentStream.drawLine(x1, y1, x1, y2);
						contentStream.drawLine(x1, y2, x2, y2);
						contentStream.drawLine(x2, y2, x2, y1);
						contentStream.drawLine(x2, y1, x1, y1);
					}
					break;
				}

				contentStream.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
			i++;
		}
		pdfViewerPnl.showPage();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void visualizeTags(HashMap<Integer, Set<PDFTag>> tags, Color color,
			VISUAL_TYPE type) {
		Iterator<Integer> it = tags.keySet().iterator();
		List<PDPage> pages = visualDocument.getDocumentCatalog().getAllPages();

		while (it.hasNext()) {
			int curPageNum = it.next();
			PDPage curPage = pages.get(curPageNum);
			float pageHeight = curPage.getMediaBox().getHeight();
			Set<PDFTag> curTags = tags.get(curPageNum);
			PDPageContentStream contentStream = null;

			/* set hightlight opacity */
			if (type == VISUAL_TYPE.HIGH_LIGHT) {
				PDExtendedGraphicsState graphicsState = new PDExtendedGraphicsState();
				// set the opacity of the graphics state
				graphicsState.setNonStrokingAlphaConstant(0.3f);
				// find the resources dictionary for the page. In my use case,
				// this is never null.
				PDResources resources = curPage.findResources();
				// try get the graphics state dictionary. Javadocs say it could
				// return null,
				// but I'm doubtful...
				Map<String, PDExtendedGraphicsState> graphicsStateDictionary = resources
						.getGraphicsStates();
				if (graphicsStateDictionary == null) {
					// There is no graphics state dictionary in the resources
					// dictionary, create one.
					graphicsStateDictionary = new TreeMap();
				}
				graphicsStateDictionary.put("MyGraphicsStateName",
						graphicsState);
				resources.setGraphicsStates(graphicsStateDictionary);
			}

			try {
				contentStream = new PDPageContentStream(visualDocument,
						curPage, true, false);
				contentStream.setStrokingColor(color);

				for (PDFTag tag : curTags) {
					float x1 = (float) tag.getX1();
					float y1 = (float) tag.getY1();
					float x2 = (float) tag.getX2();
					float y2 = (float) tag.getY2();
					float verticalShift = y2 - y1;
					y1 = pageHeight - y1 + verticalShift;
					y2 = pageHeight - y2 + verticalShift;

					if (type == VISUAL_TYPE.MARK) {
						contentStream.drawLine(x1, y1, x1, y2);
						contentStream.drawLine(x1, y2, x2, y2);
						contentStream.drawLine(x2, y2, x2, y1);
						contentStream.drawLine(x2, y1, x1, y1);
					} else if (type == VISUAL_TYPE.HIGH_LIGHT) {
						contentStream
						.appendRawCommands("/MyGraphicsStateName gs\n");
						contentStream.setNonStrokingColor(color);
						contentStream.fillRect(x1, y2, Math.abs(x2 - x1),
								Math.abs(y1 - y2));
					}
					contentStream.close();
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// add to the annotations on the page
		pdfViewerPnl.showPage();
	}

	private HashMap<Integer, Set<PDFTag>> getTags(Set<Annotation> annotations) {

		HashMap<Integer, Set<PDFTag>> tags = new HashMap<Integer, Set<PDFTag>>();
		@SuppressWarnings("unchecked")
		List<PDPage> pages = visualDocument.getDocumentCatalog().getAllPages();

		for (int i = 0; i < pages.size(); i++) {
			tags.put(i, new HashSet<PDFTag>());
		}



		for (Annotation annotation : annotations) {
			String annotator = annotation.getOriginAnnotator();
			String concept = annotation.getConcept();
			Pair<Glyph,Glyph> boundingGlyph = 
					((AnnotatedPDFModel) annoDocModel).getBoundingGlyphs(annotation);
			double x1 = boundingGlyph.getLeft().getLeft();
			double y1 = boundingGlyph.getLeft().getTop();
			double x2 = boundingGlyph.getRight().getRight();
			double y2 = boundingGlyph.getLeft().getBottom();

			int pageNum = boundingGlyph.getLeft().getPage();

			PDFTag tag = new PDFTag(x1, y1, x2, y2, annotator, concept);
			Set<PDFTag> curPageTags = tags.get(pageNum);
			curPageTags.add(tag);
			tags.put(pageNum, curPageTags);
		}

		return tags;
	}

	/**
	 * to select annotators, the method add (or remove) the annotator ticked
	 * (unticked) by the user
	 */
	@Override
	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() instanceof JCheckBox
				&& ae.getActionCommand().startsWith("annotator_")) {
			String annotator = ae.getActionCommand().replace("annotator_", "");
			JCheckBox checkBox = (JCheckBox) ae.getSource();
			if (checkBox.isSelected()) {
				this.highlightedAnnotator.add(annotator);
				this.addHighlightByAnnotator(annotator);
			} else {
				this.highlightedAnnotator.remove(annotator);
				this.removeHighlightByAnnotator(annotator);
			}

		} else if (ae.getSource() instanceof JCheckBox
				&& ae.getActionCommand().startsWith("segmentation_")) {
			String seg = ae.getActionCommand().replace("segmentation_", "");
			SEG_TYPE segType;
			if (seg.equals("lines")) {
				segType = SEG_TYPE.LINE;
			} else if (seg.equals("blocks")) {
				segType = SEG_TYPE.BLOCK;
			} else {
				segType = SEG_TYPE.BLOCKGROUP;
			}

			JCheckBox checkBox = (JCheckBox) ae.getSource();
			if (checkBox.isSelected()) {
				segmentationType.add(segType);
				addMarkBySegmentation(segType);
			} else {
				segmentationType.remove(segType);
				removeMarkBySegmentation();
			}
		} else if (ae.getSource() instanceof JCheckBox
				&& ae.getActionCommand().startsWith("originalSegmentation_")) {
			String seg = ae.getActionCommand().replace("originalSegmentation_",
					"");
			SEG_TYPE segType;
			if (seg.equals("lines")) {
				segType = SEG_TYPE.LINE;
			} else if (seg.equals("blocks")) {
				segType = SEG_TYPE.BLOCK;
			} else {
				segType = SEG_TYPE.BLOCKGROUP;
			}

			JCheckBox checkBox = (JCheckBox) ae.getSource();
			if (checkBox.isSelected()) {
				originaLSegmentationType.add(segType);
				addMarkBySegmentation(segType);
			} else {
				originaLSegmentationType.remove(segType);
				removeMarkBySegmentation();
			}
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// for selection events from legend tree
		if (e.getSource() == docViewerPnl.legendTree) {
			JTree tree = (JTree) e.getSource();
			int x = e.getX();
			int y = e.getY();
			int row = tree.getRowForLocation(x, y);
			TreePath path = tree.getPathForRow(row);
			if (path != null) {
				CheckBoxTreeNode node = (CheckBoxTreeNode) path
						.getLastPathComponent();
				if (node != null) {
					boolean isSelected = !node.isSelected();
					Set<String> targets = node.setSelected(isSelected);
					((DefaultTreeModel) tree.getModel())
					.nodeStructureChanged(node);

					// target at himself and all the descendants
					String predicate = node.toString(); // get the concept name

					targets.add(predicate);
					// TreeUtil.getAllDescConcepts(node,targets);

					// transform the concepts in the original name
					final Set<String> normalTargets = new HashSet<String>();
					for (String target : targets) {
						normalTargets.add(target);
					}

					if (isSelected) {
						this.highlightedConcept.addAll(normalTargets);
						addMarksByConcept(normalTargets);
					}

					else {
						this.highlightedConcept.removeAll(normalTargets);
						removeMarksByConcept(normalTargets);
					}

				}
			}
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent arg0) {

	}

	public PDDocument getDocument() {
		return visualDocument;
	}

	public void setDocument(PDDocument document) {
		this.visualDocument = document;
	}

	public PDDocument getOrigionalDocument() {
		return origionalDocument;
	}

	public void setOrigionalDocument(PDDocument origionalDocument) {
		this.origionalDocument = origionalDocument;
	}

	public File getVisualFile() {
		return visualFile;
	}

	public void setVisualFile(File visualFile) {
		this.visualFile = visualFile;
	}

	public File getOrigionalFile() {
		return origionalFile;
	}

	public void setOrigionalFile(File origionalFile) {
		this.origionalFile = origionalFile;
	}

	public PDFViewerPnl getPdfViewerPnl() {
		return pdfViewerPnl;
	}

	public void setPdfViewerPnl(PDFViewerPnl pdfViewerPnl) {
		this.pdfViewerPnl = pdfViewerPnl;
	}

	@Override
	public void keyPressed(KeyEvent ke) {
		int keyCode = ke.getKeyCode();
		switch (keyCode) {
		case KeyEvent.VK_UP:
			pdfViewerPnl.showPreviousPage();
			break;
		case KeyEvent.VK_DOWN:
			pdfViewerPnl.showNextPage();
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println();
	}

	public void mouseMoved(MouseEvent e) {
		// System.out.println(e.getX()+" "+e.getY());
	}

	public void mouseWheelMoved(MouseWheelEvent e) {
		String message;
		int notches = e.getWheelRotation();
		if (notches < 0) {
			message = "Mouse wheel moved UP ";
		} else {
			message = "Mouse wheel moved DOWN ";
		}
		Console.println(message);
	}

	@Override
	/**
	 * A utility method to construct the model for conflict table, where
	 * the model comes from querying the global model of the annotated docs.
	 */
	public TableModel constructConfTblModel() {
		if(annoDocModel==null){
			return null;
		}

		//define the header of the table
		String[] columnNames = {"Id","Snippet",
				"Begin",
				"End",
				"# Base_Conf.",
		"# Strong_Conf."};
		//specify the model data
		Vector<Vector<Object>> data = new Vector<Vector<Object>>();


		Set<Conflict> conflicts = new TreeSet<Conflict>(new CompareConflictByStartEnd());
		conflicts.addAll(annoDocModel.getAllConflicts());


		Set<String> consideredStartEnd=new HashSet<String>();
		String text = annoDocModel.getAnnotatedText();

		for(Conflict conflict:conflicts){
			int start = (int) conflict.getStart();
			int end = (int) conflict.getEnd();
			String span ="span_"+start+"_"+end;
			if(consideredStartEnd.contains(span))
				continue;
			consideredStartEnd.add(span);

			try{
				//TO VERIFY IF CORRECT
				Vector<Object> newRow = new Vector<Object>();
				newRow.add(span);
				newRow.add("\""+text.substring((int)conflict.getStart(),(int)conflict.getEnd()));
				newRow.add(start);
				newRow.add(end);
				newRow.add(annoDocModel.getOmissionConflictByStartEnd(start, end));
				newRow.add(annoDocModel.getLogicalConflictByStartEnd(start, end));
				data.add(newRow);
			}
			catch(Exception e){
				LOGGER.warn("Exception thrown during the creation of conlficts table",e);
			}
		}

		TableModel model = new ConflTableModel(data,columnNames);
		return model;
	}
}
