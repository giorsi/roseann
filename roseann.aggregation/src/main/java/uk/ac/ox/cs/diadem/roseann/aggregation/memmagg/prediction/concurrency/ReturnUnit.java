/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.prediction.concurrency;


/**
 *  A structure defined to return the output labeling result of an input sequence
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class ReturnUnit {
	private String[] labelSeq;
	private int sentID;
	private String res;
	/**
	 * @param labelSeq
	 * @param sentID
	 */
	public ReturnUnit(String[] labelSeq, int sentID) {
		super();
		this.labelSeq = labelSeq;
		this.sentID = sentID;
		this.res = getConcatOutcomes(labelSeq);
	}
	
	public static String getConcatOutcomes(String[] outcomes){
		if(outcomes!=null){
			StringBuilder sb = new StringBuilder();
			//if(labelSeq!=null){
				
				for(int i=0;i<outcomes.length;i++){
					sb.append(outcomes[i]+"\n");
				}
				
			return sb.toString();
		}else{
			return null;
		}
		
	}
	/**
	 * @return the labelSeq
	 */
	public String[] getLabelSeq() {
		return labelSeq;
	}
	/**
	 * @param labelSeq the labelSeq to set
	 */
	public void setLabelSeq(String[] labelSeq) {
		this.labelSeq = labelSeq;
	}
	/**
	 * @return the sentID
	 */
	public int getSentID() {
		return sentID;
	}
	/**
	 * @param sentID the sentID to set
	 */
	public void setSentID(int sentID) {
		this.sentID = sentID;
	}
	/**
	 * @param res the res to set
	 */
	public void setRes(String res) {
		this.res = res;
	}
	/**
	 * @return the res
	 */
	public String getRes() {
		return res;
	}
	

	/**
	 * An utility method to assign a default labeling sequence of "Other"
	 * @param size the length of sequence
	 * @return String[] an array representing the sequence of "Other"
	 */
	public static String[] labelAsNothing(int size) {
		String[] res = new String[size];
		for (int i = 0; i < res.length; i++) {
			res[i] = "Other";
		}
		return res;
	}

}
