/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.dataModel;

import java.util.*;





/**
 * This class defines the data structure of an event which consists of a set of 
 * raw features (or context/evidence) and outcome (? for testing and real label for training)
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */

public class Event {
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(String onePredicate:contextPred){
			sb.append(onePredicate);
			sb.append(" ");
		}
		sb.append(outcome);
		return sb.toString();
	}
	//record the features of a token
	private String[] contextPred;
	//record the outcome label of a token, which is '?' in testing mode and gold-standard
	//label in training mode
	private String outcome;
	//the span information of the token
	private int[] beginEnd;
	//record whether the token is a beginning token of input unit
	private boolean beginOfSentence;
	//the token word
	private String token;
	//record the opinions of each individual annotator
	private Map<String, String> contextPredHash;
	private String splitter;
	
	public Event(int contextNum,String splitter){
		contextPred = new String[contextNum];
		contextPredHash = new HashMap<String, String>();
		outcome = "Other"; // default value
	}
	/**
	 * @param res
	 */
	public Event(String[] context, String outcome,String splitter) {
		this.contextPred = context;
		this.outcome = outcome;
	}
	/**
	 * 
	 */
	public Event() {
		contextPredHash = new HashMap<String, String>();
		outcome = "Other"; // default value
	}
	/**
	 * @param contextPred the contextPred to set
	 */
	public void setContextPred(String[] contextPred) {
		this.contextPred = contextPred;
	}
	/**
	 * @return the contextPred
	 */
	public String[] getContextPred() {
		return contextPred;
	}
	/**
	 * @param outcome the outcome to set
	 */
	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}
	/**
	 * @return the outcome
	 */
	public String getOutcome() {
		return outcome;
	}
	public void setBeginEnd(int[] beginEnd) {
		this.beginEnd = beginEnd;
	}
	public int[] getBeginEnd() {
		return beginEnd;
	}
	public void setBeginOfSentence(boolean beginOfSentence) {
		this.beginOfSentence = beginOfSentence;
	}
	public boolean isBeginOfSentence() {
		return beginOfSentence;
	}
	public boolean containFeature(String feature){
		for(String oneFeature:contextPred){
			if(feature.equalsIgnoreCase(oneFeature)){
				return true;
			}
		}
		return false;
	}
	public void setContextPredHash(Map<String, String> contextPredHash) {
		this.contextPredHash = contextPredHash;
	}
	public Map<String, String> getContextPredHash() {
		return contextPredHash;
	}
	
	
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	/**
	 * @param string
	 * @return
	 */
	public String getContextValue(String key) {
		return contextPredHash.get(key);
	}
	/**
	 * @param string
	 * @return
	 */
	public static String concatSortedStr(String str, String splitter){
		String output = "";
		String[] subStrs = str.split(splitter);
		if(subStrs.length>=1){
			List<String> strsSet = Arrays.asList(subStrs);
			Collections.sort(strsSet);
			for(int i=0;i<subStrs.length;i++){
				if(!output.contains(strsSet.get(i))){
					if(i>0){
						output += splitter;
					}
					output+=strsSet.get(i);
				}
				
			}
		}
		return output;
	}
	
	
	
	/**
	 * An utility method to detect whether the current token is a beginning token
	 * of input unit (target connected component)
	 * @param myFeature the feature set of this token
	 * @return boolean  true if it is a beginning token, false otherwise
	 */
	public static boolean isBeginOfCC(Event myFeature) {
		Map<String, String> contextPredHash = myFeature.getContextPredHash();
		if(contextPredHash==null){
			return false;
		}
		if(contextPredHash.size()<1){
			return false;
		}
		if(contextPredHash.size()==1&&contextPredHash.containsKey("beginOfSentence")){
			return false;
		}
		
		for(String opinion:contextPredHash.values()){
			if(opinion.contains("I_")){
				return false;
			}
		}
		if(myFeature.getOutcome().contains("I_")){
			String temp = myFeature.getOutcome();
			myFeature.setOutcome(temp.replaceAll("I_", "B_"));
			return true;
		}
		return true;
	}
	/**
	 * @return the splitter
	 */
	public String getSplitter() {
		return splitter;
	}
	/**
	 * @param splitter the splitter to set
	 */
	public void setSplitter(String splitter) {
		this.splitter = splitter;
	}

}
