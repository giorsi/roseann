/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.control;


import java.awt.event.*;
import java.io.File;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.text.*;
import javax.swing.tree.*;

import org.apache.commons.configuration.XMLConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.roseann.gui.forms.*;
import uk.ac.ox.cs.diadem.roseann.gui.swingComponent.tree.CheckBoxTreeNode;
import uk.ac.ox.cs.diadem.roseann.gui.web.util.BrowserDecorator;

/**
 * Business class to handling all the business of the web part
 * @author luyche
 * 
 */
/**
 * @author luin
 *
 */
public class BrowserVisualizationBusiness  extends AbstractDocumentViewerBusiness{
	
	private final static String DEFAULT_CONFIG = "conf/Configuration.xml";

	private final static Logger LOGGER = LoggerFactory.getLogger(BrowserVisualizationBusiness.class);
	
	private static boolean onlyClean=false;

	static{
		try{
			XMLConfiguration conf = new XMLConfiguration(new File(DEFAULT_CONFIG));
			String onlyCleanString = conf.getString("nlp.visualization.web.only_clean");
			if(onlyCleanString.equals("true"))
				onlyClean=true;
		}
		catch(Exception e){
			LOGGER.warn("Not able to read the property nlp.visualization.web.only_clean from the configuration file {}",e);
		}

	}


	public BrowserVisualizationBusiness(DocumentViewerPnl docViewerPnl,
			final BrowserDecorator decorator, MODEL_TYPE modelType) {
		super(docViewerPnl,modelType);

		this.decorator=decorator;

		this.textToAnnotate=this.annoDocModel.getAnnotatedText();


		this.highlightedConcept=new HashSet<String>();
		this.highlightedAnnotator=new HashSet<String>();

	}


	private BrowserDecorator decorator;

	//text to annotate
	private String textToAnnotate;

	//set of the concepts ticked by the user
	private Set<String> highlightedConcept;

	//set of the annotators ticked by the user
	private Set<String> highlightedAnnotator;


	public void initialize() {
		try {

			super.initialize();
			setTextPaneStyle(this.textToAnnotate);

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	/**
	 * A utility method to insert stylised text content to the textpane component
	 * For the web page, it consists the text to annotate in the web page
	 * @param showContent the text string
	 */
	private void setTextPaneStyle(String showContent)
			throws BadLocationException {
		StyledDocument paneDoc = docViewerPnl.annotatedDocViewTxtpane
				.getStyledDocument();
		Style def = StyleContext.getDefaultStyleContext().getStyle(
				StyleContext.DEFAULT_STYLE);

		paneDoc.addStyle("regular", def);
		// set line space
		MutableAttributeSet attrs = docViewerPnl.annotatedDocViewTxtpane
				.getInputAttributes();
		StyleConstants.setLineSpacing(attrs, 0.5f);
		paneDoc.setParagraphAttributes(0, showContent.length(), attrs, false);

		String[] content = { showContent };
		String[] initStyles = { "regular" };

		// clean the the textpane
		paneDoc.remove(0, paneDoc.getLength());
		// insert the text content
		for (int i = 0; i < content.length; i++) {
			paneDoc.insertString(paneDoc.getLength(), content[i],
					paneDoc.getStyle(initStyles[i]));
		}
		//set to display from beginning
		docViewerPnl.annotatedDocViewTxtpane.setCaretPosition(0);
	}


	//to select annotators, the method add (or remove) the annotator ticked (unticked) by the user
	@Override
	public void actionPerformed(ActionEvent ae) {
		//handle annotator selection events.
		if(ae.getSource() instanceof JCheckBox && ae.getActionCommand().startsWith("annotator_")){
			String annotator = ae.getActionCommand().replace("annotator_", "");
			/*if(this.viewMode.equals(DocViewMode.TEXT)){
				try {
					handleAnnotatorSelectionOnText(annotator,(JCheckBox) ae.getSource());
				} catch (BadLocationException e) {
					System.err
					.println("BadLocationException: Cannot insert text at offset "
							+ e.offsetRequested());
				}
			}else{*/

			JCheckBox checkBox=(JCheckBox) ae.getSource();
			if(checkBox.isSelected())
				this.highlightedAnnotator.add(annotator);
			else
				this.highlightedAnnotator.remove(annotator);

			//remove previous annotation
			this.visualizeAnnotations();

			//}

		}

	}


	private void visualizeAnnotations(){
		//remove previous annotations and blinking
		this.decorator.removeAnnotations();
		this.decorator.removeBlinking();

		this.decorator.visualizeAnnotations(this.highlightedAnnotator,this.highlightedConcept,onlyClean);





	}

	@Override  


	//to select concepts, the method add (or remove) the set of concepts ticked (unticked) by the user
	public void mouseClicked(MouseEvent e)  
	{  
		//for selection events from legend tree
		if(e.getSource()== docViewerPnl.legendTree) {
			JTree tree = (JTree)e.getSource();  
			int x = e.getX();  
			int y = e.getY();  
			int row = tree.getRowForLocation(x, y);  
			TreePath path = tree.getPathForRow(row);  
			if(path != null)  
			{  
				CheckBoxTreeNode node = (CheckBoxTreeNode)path.getLastPathComponent();  
				if(node != null)  
				{  
					boolean isSelected = !node.isSelected();  
					Set<String> targets = node.setSelected(isSelected);  
					((DefaultTreeModel)tree.getModel()).nodeStructureChanged(node);  

					//target at himself and all the descendants
					String predicate = node.toString(); //get the concept name

					targets.add(predicate);
					// TreeUtil.getAllDescConcepts(node,targets);
					
					//transform the concepts in the original name
					final Set<String> normalTargets=new HashSet<String>();
					for(String target:targets){
						normalTargets.add(target);
					}

					if(isSelected)
						this.highlightedConcept.addAll(normalTargets);
					else
						this.highlightedConcept.removeAll(normalTargets);

					this.visualizeAnnotations();


				}  
			}  
		}  
	}

	//to take care of the blinking for a given span. 
	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getValueIsAdjusting()) {
			return;
		}
		//for selection events from conflict table
		if(e.getSource()==docViewerPnl.conflictTbl.getSelectionModel()){
			int selectedRow = docViewerPnl.conflictTbl.getSelectedRows()[0];
			try {
				addBlinkHighlight(selectedRow);
			} catch (BadLocationException exp) {
				System.err
				.println("BadLocationException: Cannot insert text at offset "
						+ exp.offsetRequested());
			}
		}

	}
	/**
	 * A utility method to add blinking highlight to the focused span
	 * with conflicts
	 * @throws BadLocationException 
	 */
	private void addBlinkHighlight(int selectedRow) throws BadLocationException {
		
		TableModel model = docViewerPnl.conflictTbl.getModel();
		
		int begin = Integer.parseInt(model.getValueAt(selectedRow, 2).toString());
		int end = Integer.parseInt(model.getValueAt(selectedRow, 3).toString());

		this.decorator.removeBlinking();
		this.decorator.blinkAnnotation(begin,end);

	}



}






