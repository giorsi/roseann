/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.forms;

import java.awt.*;

import javax.swing.*;


/**
 * 
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
@SuppressWarnings("serial")
public class ConceptDetailFrame extends JFrame {
	public ConceptDetailFrame() {
		initComponents();
	}

	private void initComponents() {
		panel1 = new JPanel();
		conceptNameLbl = new JLabel();
		tabbedPane = new JTabbedPane();
		scrollPane1 = new JScrollPane();
		equiList = new JList<String>();
		scrollPane2 = new JScrollPane();
		disjointList = new JList<String>();

		//======== this ========
		Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		this.setIconImage(new ImageIcon(getClass().getResource("icons/roseann-logo.png")).getImage());


		//======== panel1 ========
		{
			panel1.setLayout(new BorderLayout());

			//---- conceptNameLbl ----
			conceptNameLbl.setText("text");
			conceptNameLbl.setOpaque(true);
			conceptNameLbl.setBackground(Color.magenta);
			conceptNameLbl.setHorizontalAlignment(SwingConstants.CENTER);
			panel1.add(conceptNameLbl, BorderLayout.NORTH);

			//======== tabbedPane ========
			{

				//======== scrollPane1 ========
				{
					scrollPane1.setViewportView(equiList);
				}
				tabbedPane.addTab("Equivalent Class", scrollPane1);


				//======== scrollPane2 ========
				{
					scrollPane2.setViewportView(disjointList);
				}
				tabbedPane.addTab("Disjoint Class", scrollPane2);

			}
			panel1.add(tabbedPane, BorderLayout.CENTER);
		}
		contentPane.add(panel1, BorderLayout.CENTER);
		pack();
		setLocationRelativeTo(getOwner());
		}

	private JPanel panel1;
	public JLabel conceptNameLbl;
	private JTabbedPane tabbedPane;
	private JScrollPane scrollPane1;
	public JList<String> equiList;
	private JScrollPane scrollPane2;
	public JList<String> disjointList;
	
}
