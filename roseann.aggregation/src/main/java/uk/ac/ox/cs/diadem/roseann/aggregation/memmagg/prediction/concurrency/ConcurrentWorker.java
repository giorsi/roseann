/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.prediction.concurrency;

import java.util.*;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.Memm;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.dataModel.*;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.prediction.*;



/**
 * A worker class who is in charge of labeling one input sequence. The process includes
 * two major phases: 1) construct a markov sequence and 2) invoke a concrete decoder to
 * generate the labeling sequence.
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class ConcurrentWorker implements Callable<ReturnUnit>{
	/**
	 * Logger
	 */
	static final Logger LOGGER = LoggerFactory.getLogger(Memm.class);
	
	/**
	 * The id of this worker
	 */
	private int taskID;
	
	/**
	 * The input event sequence to be annotated 
	 */
	private List<Event> eventSequence;
	
	/**
	 * The memm instance which employs this concurrent worker
	 */
	private Memm memmEmployer;




	/* (non-Javadoc)
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public ReturnUnit call() throws Exception {
		String[] labelSeq = null;
	//	long begin = System.currentTimeMillis();
		if(memmEmployer.getModelPool()==null){
			LOGGER.debug("The model pool is empty...please check");
			labelSeq = ReturnUnit.labelAsNothing(eventSequence.size());
		}else{
			MarkovSequence markSeq = MarkovSequenceProducer.constructMarkovSequence(eventSequence, null,memmEmployer);		
			//long end = System.currentTimeMillis();
			//System.out.println("generating markov sequence costs "+(end-begin));
			//begin = System.currentTimeMillis();
			
			if(markSeq!=null)
				labelSeq = memmEmployer.getDecoder().decode(markSeq);
			else{
				labelSeq = ReturnUnit.labelAsNothing(eventSequence.size());
			}
		}
		
		
	//	end = System.currentTimeMillis();
	//	System.out.println("decode markov sequence costs "+(end-begin));
		ReturnUnit res = new ReturnUnit(labelSeq,taskID);
		return res;
	}
	
	
	

	




	/**
	 * @param taskID
	 * @param eventSequence
	 * 						Input event sequence
	 * @param decoder
	 * 				The decoder specified for MEMM
	 * @param pool
	 * 				The model pool which used by MEMM for prediction when generating Markov sequence
	 */
	public ConcurrentWorker(int taskID,List<Event> eventSequence, Memm memm){		
		this.taskID = taskID;
		this.eventSequence = eventSequence;	
		this.memmEmployer = memm;
		
	}

	
	
	

}
