/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.toolkit;

import java.io.*;

import uk.ac.ox.cs.diadem.roseann.util.Constant;


/**
 * @author Luying Chen(luying dot chen at cs dot ox dot ac dot uk) - Department
 *         of Computer Science - University of Oxford
 * 
 *         This is the toolkit to finish file operations such as reading and
 *         writing.
 */
public class FileAdapter {

	public static void writeToFile(File f, String content) {
		FileOutputStream stream;
		OutputStreamWriter writer;
		try {
			stream = new FileOutputStream(f.getAbsolutePath(), true);
			writer = new OutputStreamWriter(stream);
			writer.write(content);
			writer.close();
			stream.close();
		} catch (FileNotFoundException e) {
			System.err.println("FileNotFoundException: " + e.getMessage());
		} catch (IOException e) {

			System.err.println("IOException: " + e.getMessage());

		}

		/*
		 * RandomAccessFile fos=new RandomAccessFile("test.xml);
		 * fos.seek(fos.length()); fos.writeBytes("\n"+shr); fos.close();
		 */
	}

	// utility function
	public static String getPureFileContents(String filename)

	{
		
		File file = new File(filename);
		StringBuilder contents = new StringBuilder();
		BufferedReader input = null;
		try {

			input = new BufferedReader(new FileReader(file));

			String line = null;

			while ((line = input.readLine()) != null) {

				contents.append(line);

				contents.append(Constant.LINESEPARATOR);
			}
		} catch (FileNotFoundException e) {

			System.err.println(e.getMessage());
		} catch (IOException e) {

			System.err.println(e.getMessage());
		} finally {
			try {
				input.close();
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}

		return contents.toString();
	}

}
