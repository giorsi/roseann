/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.util;

import java.util.Comparator;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;


/**
 * Class to implement a comparison between 2 annotations according to start and end.
 * An annotation o2 is greater than an annotation o1 if start(o2)>start(o1), if start(o1)=start(o2) then o2 is 
 * greater than o1 if end(o2)>end(o1); eventually if both annotation have same start end the id will be compared
 * 
 * @author Stefano Ortona (stefano dot ortona at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */

public class CompareAnnotationByStartEnd implements Comparator<Annotation>{

	@Override
	public int compare(Annotation o1, Annotation o2) {
		final long start1=o1.getStart();
		final long start2=o2.getStart();
		final long end1=o1.getEnd();
		final long end2=o2.getEnd();

		if(start1==start2){

			//if both start-end are equal, compare the ids
			if(end1==end2){


				final String id1=o1.getId();
				final String id2=o2.getId();
				if(id1.equals(id2))
					return 0;
				//return the first annotation as first
				return id1.compareTo(id2);

			}			
			return (int) (end1-end2);
		}
		return (int) (start1-start2);
	}
}
