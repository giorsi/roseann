/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.annotator.wrapper;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.wrapper.AnnotatorDecorator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.yahoocontentanalyser.YahooYQLAnnotator;
import uk.ac.ox.cs.diadem.roseann.ontology.MaterializedOntologyQuery;
import uk.ac.ox.cs.diadem.roseann.ontology.OntologyException;

public class YahooYQLAnnotatorWrapper extends AnnotatorDecorator{

	/**
	 * Logger
	 */
	static final Logger LOGGER = LoggerFactory.getLogger(YahooYQLAnnotatorWrapper.class);

	/**
	 * Singleton instance
	 */
	private static Annotator instance;


	/**
	 * Private constructor
	 */
	private YahooYQLAnnotatorWrapper() {
		this.decoratee=YahooYQLAnnotator.getInstance();
	}

	@Override
	public Set<Annotation> annotateEntity(String text,int timeout){
		Set<Annotation> allAnnotations=this.decoratee.annotateEntity(text,timeout);

		//group the annotations by start-end
		Map<String,Set<Annotation>> id2annotation=new HashMap<String,Set<Annotation>>();
		for(Annotation annotation:allAnnotations){
			String id=annotation.getStart()+"_"+annotation.getEnd();
			Set<Annotation> sameSpanAnnotations=id2annotation.get(id);
			if(sameSpanAnnotations==null)
				sameSpanAnnotations=new HashSet<Annotation>();
			sameSpanAnnotations.add(annotation);
			id2annotation.put(id, sameSpanAnnotations);
		}

		Set<Annotation> cleanAnnotations=new HashSet<Annotation>();
		//to do-keep only the most specific class
		for(String id:id2annotation.keySet()){
			cleanAnnotations.addAll(getMostSpecificConceptAnnotation(id2annotation.get(id)));
		}
		return cleanAnnotations;
	}


	private Set<Annotation> getMostSpecificConceptAnnotation(Set<Annotation> annotations){

		Set<String> allSupertypes=new HashSet<String>();
		Set<String> deepestType=new HashSet<String>();
		for(Annotation annotation:annotations){
			final String type=annotation.getConcept();
			deepestType.add(type);
			try{
				Set<String> supertypes=MaterializedOntologyQuery.getAllSupertypesAnnotator(type, getAnnotatorName());
				if(supertypes==null){
					LOGGER.error("The annotator {} type {} is unknown in the ontology annotator superclass file",
							getAnnotatorName(),type);
					deepestType.remove(type);
				}
				else{
					supertypes.remove(type);
					allSupertypes.addAll(supertypes);
				}
			}
			catch(OntologyException e){
				LOGGER.error("Error while retrieving the annotator {} supertype for the type {}",getAnnotatorName(),type);
				deepestType.remove(type);
			}
		}
		//remove all the supertypes
		deepestType.removeAll(allSupertypes);

		//keep only the annotations with deepest type
		Set<Annotation> deepestAnnotation=new HashSet<Annotation>();
		for(Annotation annotation:annotations){
			if(deepestType.contains(annotation.getConcept()))
				deepestAnnotation.add(annotation);
		}
		return deepestAnnotation;
	}

	/**
	 * Method to get the singleton instance
	 * @return singleton instance
	 */
	public static synchronized Annotator getInstance() {
		if (instance == null) {
			instance = new YahooYQLAnnotatorWrapper();
		}
		return instance;
	}

}
