/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.swingComponent.tree;



import java.util.*;

import javax.swing.tree.DefaultMutableTreeNode;  
/**
 * To define the check box tree node for the legend tree model
  * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class CheckBoxTreeNode extends DefaultMutableTreeNode  
{  
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected boolean isSelected;  
      
    public CheckBoxTreeNode()  
    {  
        this(null);  
    }  
      
    public CheckBoxTreeNode(Object userObject)  
    {  
        this(userObject, true, false);  
    }  
      
    public CheckBoxTreeNode(Object userObject, boolean allowsChildren, boolean isSelected)  
    {  
        super(userObject, allowsChildren);  
        this.isSelected = isSelected;  
    }  
  
    public boolean isSelected()  
    {  
        return isSelected;  
    }  
      
    /**
     * @param isSelected
     * @return Set<String> 
     * 					All the affected nodes concepts by this tick/un-tick
     */
    public Set<String> setSelected(boolean isSelected)  
    {  
        this.isSelected = isSelected;  
        Set<String> affectedConcept = new HashSet<String>();
        if(isSelected)  
        {  
            // if selected, all the children are selected as well
            if(children != null)  
            {  
                for(Object obj : children)  
                {  
                    CheckBoxTreeNode node = (CheckBoxTreeNode)obj;  
                    if(isSelected != node.isSelected())  {
                    	affectedConcept.addAll(node.setSelected(isSelected)) ; 
                    	affectedConcept.add(node.toString());
                    }
                }  
            }  
            
            // up toward, if all the children are selected, then parent are selected as well
            CheckBoxTreeNode pNode = (CheckBoxTreeNode)parent;  
          
            // start to check whether all the children of pNode are selected
            if(pNode != null)  
            {  
                int index = 0;  
                for(; index < pNode.children.size(); ++ index)  
                {  
                    CheckBoxTreeNode pChildNode = (CheckBoxTreeNode)pNode.children.get(index);  
                    if(!pChildNode.isSelected())  
                        break;  
                }  
            
                /*  
                 * all the children of pNode are selected, so pNode is selected
                 * it's done recursively 
                 */  
                if(index == pNode.children.size())  
                {  
                    if(pNode.isSelected() != isSelected)  {
                    	affectedConcept.addAll( pNode.setSelected(isSelected)); 
                        affectedConcept.add(pNode.toString());
                    }
                }  
            }  
        }  
        else   
        {  
           
            if(children != null)  
            {  
                int index = 0;  
                for(; index < children.size(); ++ index)  
                {  
                    CheckBoxTreeNode childNode = (CheckBoxTreeNode)children.get(index);  
                    if(!childNode.isSelected())  
                        break;  
                }  
                // when down toward de-selected  
                if(index == children.size())  
                {  
                    for(int i = 0; i < children.size(); ++ i)  
                    {  
                        CheckBoxTreeNode node = (CheckBoxTreeNode)children.get(i);  
                        if(node.isSelected() != isSelected)  {
                        	affectedConcept.addAll( node.setSelected(isSelected));
                             affectedConcept.add(node.toString());
                        }
                           
                    }  
                }  
            }  
             //up toward de-select, once a child is de-selected, then de-select parent 
          
            CheckBoxTreeNode pNode = (CheckBoxTreeNode)parent;  
            if(pNode != null && pNode.isSelected() != isSelected) { 
            	affectedConcept.addAll(pNode.setSelected(isSelected));
                affectedConcept.add(pNode.toString());
            }
        }
		return affectedConcept;  
    }  
}  