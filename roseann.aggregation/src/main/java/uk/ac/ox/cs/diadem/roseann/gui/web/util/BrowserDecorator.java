/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.web.util;

import java.io.File;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.model.Atom;
import uk.ac.ox.cs.diadem.model.Model;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.roseann.api.RoseAnnException;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict.ConflictType;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.LogicalConflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.OmissionConflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web.AnnotatedHTMLModel;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web.AnnotatedHtmlSpan;
import uk.ac.ox.cs.diadem.roseann.gui.swing.util.ColorMap;
import uk.ac.ox.cs.diadem.roseann.gui.swing.util.Console;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.css.StyledNode;
import uk.ac.ox.cs.diadem.webapi.css.StyledOverlay;
import uk.ac.ox.cs.diadem.webapi.css.StyledOverlayBuilder;

import com.google.common.collect.Sets;

/**
 * @author Stefano Ortona (stefano.ortona@gmail.com) Oxford University, Dipartimento di Informatica e Automazione -
 *         Universita di Roma Tre.
 * @author Giorgio Orsi (giorgio dot orsi at cs dot ox dot ac dot uk) Department of Computer Science - University of
 *         Oxford and Institute for the Future of Computing - Oxford Martin School. Class to visualize annotationd in
 *         the browser.
 */
public class BrowserDecorator {

  private final static Logger LOGGER = LoggerFactory.getLogger(BrowserDecorator.class);

  private final WebBrowser wb;
  private StyledOverlay overlayAnnotationTree;
  private StyledOverlay overlayBlinkingTree;

  private final AnnotatedHTMLModel htmlModel;

  private final static String DEFAULT_CONFIGURATION = "conf/Configuration.xml";

  /**
   * Constructor to build the decorator instance
   * 
   * @param diademBrowser
   *          the browser to navigate the web-page to be highlighted
   */
  public BrowserDecorator(final WebBrowser browser, final AnnotatedHTMLModel htmlModel) {
    wb = browser;
    overlayAnnotationTree = null;
    overlayBlinkingTree = null;
    this.htmlModel = htmlModel;
  }

  /**
   * Method to visualize the annotation inside the browser with pre-computed visible annotation model
   * 
   * @param browserModel
   *          model containing browser facts
   * @param visibleAnnotations
   *          model containing fact for visible annotations, @see computeVisibleAnnotations
   * @param annotators
   *          set of annotators to be visualized (if null, all annotators will be visualized)
   * @param concepts
   *          set of concepts to be visualized (if null, all concepts will be visualized)
   * @param annotation_raw_mode
   *          the annotation raw model to enrich the tooltip information. If null, no information will be displayed
   * @param text
   *          the string text annotated, to enrich the tooltip. If null, no information will be displayed
   */
  public void visualizeAnnotations(final Set<String> annotators, final Set<String> concepts, final boolean onlyClean) {

    final StyledOverlayBuilder builder = wb.getContentDOMWindow().getOverlayBuilder();

    final StyledNode rootNode = builder.createNode("/html/body", "annotation-span");

    // Get the visible annotated spans
    final Set<AnnotatedHtmlSpan> visibleAnnotatedSpans = htmlModel.getAnnotatedSpan();
    LOGGER.trace("visualizing {} annotated spans.", visibleAnnotatedSpans.size());

    // Get the overlay builder.
    // final StyledOverlayBuilder builder = wb.getContentDOMWindow().getOverlayBuilder();

    // A set of overlay nodes (with infoboxes) to be drawn for each visible annotated span
    final Set<StyledNode> overlayNodes = new HashSet<StyledNode>();

    // Each overlay node might have specific associated CSS rules.
    final Set<String> spanCSSRules = new HashSet<String>();

    for (final AnnotatedHtmlSpan span : visibleAnnotatedSpans) {

      // get start and end node id
      final String startNodeId = span.getStartNodeId();
      final String endNodeId = span.getEndNodeId();
      LOGGER.trace("Start nodeID {} -- End nodeId {}", startNodeId, endNodeId);

      // retrieve the nodes using XPath locators.
      final String startNodeLocator = getLocator(startNodeId, htmlModel.getHtmlDoc().getBrowserModel());
      String endNodeLocator = startNodeLocator;
      if (startNodeId.compareTo(endNodeId) != 0) {
        endNodeLocator = getLocator(endNodeId, htmlModel.getHtmlDoc().getBrowserModel());
      }

      LOGGER.trace("Start node locator: {} -- End node locator: {}", startNodeLocator, endNodeLocator);

      // get the range of the span.
      final int from = (int) span.getOffsetStart();
      final int to = (int) span.getOffsetEnd();
      final Pair<Integer, Integer> spanRange = Pair.of(from, to);
      LOGGER.trace("Start node offset: {} -- End node offset: {}", spanRange.getLeft(), spanRange.getRight());

      // Get the annotations insisting on the current span,only with desired type
      final Set<Annotation> spanConceptAnnotations = getSpanConceptAnnotations(span, concepts);

      // Get the annotations insisting on the current span,only with desired annotator
      final Set<Annotation> spanAnnotatorAnnotations = getSpanAnnotatorAnnotations(span, annotators);

      // A set of annotator to be associated with the current span.
      final Set<String> spanAnnotators = getSpanAnnotators(spanAnnotatorAnnotations);

      // A set of annotation types to be associated with the current span.
      final Set<String> spanAnnotationTypes = getSpanAnnotationTypes(spanConceptAnnotations);

      if (spanConceptAnnotations.size() == 0 && spanAnnotatorAnnotations.size() == 0) {
        continue;
      }

      // to visualize only those annotations that span over a single node, skip those spans that have different
      // start and end node
      if (onlyClean && !startNodeId.equals(endNodeId)) {
        Console.println("You are trying to visualize some annotations that span over multiple dom nodes,"
            + " to abilitate this function edit in the configuraiton file the web clean_only property");
        continue;
      }

      // A set of CSS classes for the annotated span.
      final Set<String> spanCSSClasses = Sets.newHashSet(spanAnnotationTypes);
      spanCSSClasses.addAll(spanAnnotators);
      spanCSSClasses.add("annotation-span");

      if (spanAnnotationTypes.size() > 0) {

        // Add a CSS rule for each annotation type, only among the desidered type
        for (final String type : spanAnnotationTypes) {
          spanCSSRules.add(StringUtils.join("span.", type, " { border: 2px;", " border-style: solid;",
              " -moz-border-radius: .5em;", " border-radius: .5em;", " border-color: ",
              ColorMap.getColorString(ColorMap.getConceptColor(type)), "; }"));
        }

      }

      LOGGER.trace("Creating a node range for start node: {} and end node: {}", startNodeLocator, endNodeLocator);
      final StyledNode overlayNode = builder.createRangeNode(startNodeLocator, endNodeLocator, spanRange,
          spanCSSClasses.toArray(new String[1]));

      // check whether the span has already been constructed for some other annotation.
      if (!overlayNodes.contains(overlayNode)
          && (spanConceptAnnotations.size() > 0 || spanAnnotatorAnnotations.size() > 0)) {

        // add the span to the builder
        overlayNodes.add(overlayNode);

        builder.addNode(rootNode, overlayNode);

        LOGGER.trace("Created a new styled node: {}", overlayNode.toString());

        // construct a new infobox for all the annotations covering that span
        LOGGER.trace("Constructing the infobox for the span.");
        final Set<Annotation> allAnnotations = getSpanConceptAnnotations(span, null);
        final List<Pair<String, String>> infobox = constructInfoboxEntries(allAnnotations);

        if (allAnnotations.size() > 0) {
          final Annotation annotation = allAnnotations.iterator().next();
          final int start = (int) annotation.getStart();
          final int end = (int) annotation.getEnd();
          try {
            final String annotatedText = htmlModel.getAnnotatedText().substring(start, end);
            final Pair<String, String> entry = Pair.of(annotatedText, "");
            builder.addInfobox(overlayNode, entry, "infobox", "text-span");
          } catch (final IndexOutOfBoundsException e) {
            LOGGER.warn("The annotation with id {} and start-end '{}' has an unknown bounding text in the original"
                + " web page text", annotation.getId(), start + ", " + end);
          }

        }

        LOGGER.trace("Link the infobox to the span.");
        for (final Pair<String, String> entry : infobox) {
          if (htmlModel.getCompletedAggregators().contains(entry.getLeft())) {
            builder.addInfobox(overlayNode, entry, "infobox", "aggregator");
          } else {
            builder.addInfobox(overlayNode, entry, "infobox", "annotator");
          }
        }

      }
    }

    // Load the CSS rules.
    LOGGER.trace("Loading CSS rules.");
    final String basicCSSRules = loadCSSRules();

    // Build the overlays.
    LOGGER.trace("Bulding the tree and painting the overlays.");
    overlayAnnotationTree = builder.build(basicCSSRules);

    // Visualise the overlays
    overlayAnnotationTree.attach();

    // Hide the infoboxes.
    spanCSSRules.add("span.annotation-span + div { display:none;}");
    for (final String rule : spanCSSRules) {
      overlayAnnotationTree.getCSSStyleSheet().appendRule(rule);
    }

  }

  /**
   * Method to blink a specific span in the browse
   * 
   * @param annotation_id
   *          the id of one annotation related to the span to visualize * @param visibleAnnotations model containing
   *          fact for visible annotations, @see computeVisibleAnnotations
   * @param browserModel
   *          model containing browser facts
   * @param raw_annotation_model
   *          model containing raw facts for the annotation, including logical and omission conflict
   * @param text
   *          the string text annotated, to enrich the tooltip. If null, no information will be displayed
   */
  public void blinkAnnotation(final int start, final int end) {

    final Set<Conflict> omissionConflict = htmlModel.getConflictByStartEndType(start, end, ConflictType.OMISSION);
    final Set<Conflict> logicalConflict = htmlModel.getConflictByStartEndType(start, end, ConflictType.LOGICAL);

    final StyledOverlayBuilder builder = wb.getContentDOMWindow().getOverlayBuilder();

    final StyledNode rootNode = builder.createNode("/html/body");

    final Set<Annotation> visibleAnnotationSet = htmlModel.getAnnotationByStartEnd(start, end);

    if (visibleAnnotationSet == null || visibleAnnotationSet.size() == 0) {
      LOGGER.warn("Error trying to find the visible annotation with start-end {},{}", start, end);
      return;
    }

    final AnnotatedHtmlSpan annotatedSpan = htmlModel.getSpanByAnnotation(visibleAnnotationSet.iterator().next());
    if (annotatedSpan == null) {
      LOGGER.warn("Unable to find the annotated span with start-end {},{}", start, end);
      return;
    }

    // Get the overlay builder.
    // final StyledOverlayBuilder builder = wb.getContentDOMWindow().getOverlayBuilder();

    // get start and end node id
    final String startNodeId = annotatedSpan.getStartNodeId();
    final String endNodeId = annotatedSpan.getEndNodeId();
    LOGGER.trace("Start nodeID {} -- End nodeId {}", startNodeId, endNodeId);

    // retrieve the nodes using XPath locators.
    final String startNodeLocator = getLocator(startNodeId, htmlModel.getHtmlDoc().getBrowserModel());
    String endNodeLocator = startNodeLocator;
    if (startNodeId.compareTo(endNodeId) != 0) {
      endNodeLocator = getLocator(endNodeId, htmlModel.getHtmlDoc().getBrowserModel());
    }
    LOGGER.trace("Start node locator: {} -- End node locator: {}", startNodeLocator, endNodeLocator);

    final int startWithinNode = (int) annotatedSpan.getOffsetStart();
    final int endWithinNode = (int) annotatedSpan.getOffsetEnd();

    final Pair<Integer, Integer> spanRange = Pair.of(startWithinNode, endWithinNode);
    LOGGER.trace("Start node offset: {} -- End node offset: {}", spanRange.getLeft(), spanRange.getRight());

    // A set of CSS classes for the annotated span.
    final Set<String> spanCSSClasses = new HashSet<String>();
    spanCSSClasses.add("animate");
    spanCSSClasses.add("annotation-span");

    LOGGER.trace("Creating a node range for start node: {} and end node: {}", startNodeLocator, endNodeLocator);
    final StyledNode overlayNode = builder.createRangeNode(startNodeLocator, endNodeLocator, spanRange,
        spanCSSClasses.toArray(new String[1]));

    builder.addNode(rootNode, overlayNode);
    LOGGER.trace("Created a new styled node: {}", overlayNode.toString());

    LOGGER.trace("Constructing the infobox for the span.");
    // construct a new infobox for all the annotations covering that span

    final List<Pair<String, String>> infobox = constructInfoboxEntries(visibleAnnotationSet);

    LOGGER.trace("Link the infobox to the span.");

    try {
      final String annotatedText = htmlModel.getAnnotatedText().substring(start, end);
      final Pair<String, String> entry = Pair.of(annotatedText, "");
      builder.addInfobox(overlayNode, entry, "infobox", "text-span");
    } catch (final IndexOutOfBoundsException e) {
      LOGGER.warn("The span with  start-end '{},{}' has an unknown bounding text in the original" + " web page text",
          start, end);
    }

    // infobox for annotated types
    for (final Pair<String, String> entry : infobox) {
      if (htmlModel.getCompletedAggregators().contains(entry.getLeft())) {
        builder.addInfobox(overlayNode, entry, "infobox", "aggregator");
      } else {
        builder.addInfobox(overlayNode, entry, "infobox", "annotator");
      }
    }

    // infobox for omission conflicts
    if (omissionConflict.size() > 0) {
      Pair<String, String> omissionConf = Pair.of("Omission Conflicts", "");
      builder.addInfobox(overlayNode, omissionConf, "infobox", "menu-span");
      omissionConf = Pair.of("", "");
      builder.addInfobox(overlayNode, omissionConf, "infobox", "menu-span");
    }

    for (final Conflict omission : omissionConflict) {
      try {
        final String concept = ((OmissionConflict) omission).getConcept();
        final int agreed = ((OmissionConflict) omission).getAgreedAnnotators().size();
        final int omitted = ((OmissionConflict) omission).getOmittedAnnotators().size();

        final Pair<String, String> omissionConf = Pair.of(concept, agreed + " (of " + (agreed + omitted)
            + " competent)");
        builder.addInfobox(overlayNode, omissionConf, "infobox", "warning");
      } catch (final ClassCastException e) {
        LOGGER.warn("The conflict with id {} cannot be casted to a omission conflict", omission.getId());
      }

    }

    // infobox for logical conflicts
    if (logicalConflict.size() > 0) {
      Pair<String, String> logicalConf = Pair.of("Logical Conflicts", "");
      builder.addInfobox(overlayNode, logicalConf, "infobox", "menu-span");
      logicalConf = Pair.of("", "");
      builder.addInfobox(overlayNode, logicalConf, "infobox", "menu-span");

    }
    for (final Conflict logical : logicalConflict) {
      final Annotation annotation1 = ((LogicalConflict) logical).getConflictingAnnotation().getLeft();
      final Annotation annotation2 = ((LogicalConflict) logical).getConflictingAnnotation().getRight();

      if (annotation1 == null || annotation2 == null) {
        LOGGER.warn("Error trying to find the conflicting annotation for logical conflict with id {}", logical.getId());
        continue;
      }
      final String firstOpinion = annotation1.getOriginAnnotator() + ":" + annotation1.getConcept();

      final String secondOpinion = annotation2.getOriginAnnotator() + ":" + annotation2.getConcept();
      final Pair<String, String> logicalConf = Pair.of(firstOpinion + " vs", secondOpinion);
      builder.addInfobox(overlayNode, logicalConf, "infobox", "critical");

    }

    // Load the CSS rules.
    LOGGER.trace("Loading CSS rules.");
    final String basicCSSRules = loadCSSRules();

    // Build the overlays.
    LOGGER.trace("Bulding the tree and painting the overlays.");
    overlayBlinkingTree = builder.build(basicCSSRules);

    // Visualise the overlays
    overlayBlinkingTree.attach();

    // Hide the infoboxes.
    overlayBlinkingTree.getCSSStyleSheet().appendRule("span.annotation-span + div { display:none;}");

  }

  private Set<String> getSpanAnnotators(final Set<Annotation> annotations) {

    final Set<String> annotators = new HashSet<String>();
    for (final Annotation annotation : annotations) {
      annotators.add(annotation.getOriginAnnotator());
    }
    return annotators;
  }

  private Set<Annotation> getSpanConceptAnnotations(final AnnotatedHtmlSpan span, final Set<String> concepts) {

    final Set<Annotation> annotations = htmlModel.getAnnotationByAnnotatedSpan(span);

    // remove the annotations with not desidered type
    if (concepts != null) {
      final Set<Annotation> toRemove = new HashSet<Annotation>();
      for (final Annotation annotation : annotations) {
        if (!concepts.contains(annotation.getConcept())) {
          toRemove.add(annotation);
        }
      }
      annotations.removeAll(toRemove);
    }
    return annotations;
  }

  private Set<Annotation> getSpanAnnotatorAnnotations(final AnnotatedHtmlSpan span, final Set<String> annotators) {

    final Set<Annotation> annotations = htmlModel.getAnnotationByAnnotatedSpan(span);

    // remove the annotations with not desidered annotator
    if (annotators != null) {
      final Set<Annotation> toRemove = new HashSet<Annotation>();
      for (final Annotation annotation : annotations) {
        if (!annotators.contains(annotation.getOriginAnnotator())) {
          toRemove.add(annotation);
        }
      }
      annotations.removeAll(toRemove);
    }
    return annotations;
  }

  private Set<String> getSpanAnnotationTypes(final Set<Annotation> annotations) {

    final Set<String> types = new HashSet<String>();
    for (final Annotation annotation : annotations) {
      types.add(annotation.getConcept());
    }

    return types;
  }

  private String loadCSSRules() {
    final StringWriter writer = new StringWriter();
    try {
      final XMLConfiguration conf = new XMLConfiguration(new File(DEFAULT_CONFIGURATION));
      IOUtils.copy(BrowserDecorator.class.getResourceAsStream(conf.getString("nlp.visualization.cssrules")), writer);
    } catch (final Exception ioe) {
      throw new RoseAnnException("Unable to read the CSS rules file.", LOGGER);
    }
    return writer.toString();
  }

  // CORRECT
  private String getLocator(final String nodeId, final Model browserModel) {

    @SuppressWarnings("unchecked")
    final Set<Atom> locatorAtoms = browserModel.getAtomsByMask("dom1__node0__id", null,
        browserModel.getConstantTerm(nodeId), null);

    if (locatorAtoms.size() == 1)
      return locatorAtoms.iterator().next().getSubTerm(2).getValue();
    else {
      LOGGER.error("Missing XPath locator for node {}", nodeId);
      return "";
    }
  }

  public List<Pair<String, String>> constructInfoboxEntries(final Set<Annotation> spanAnnotations) {

    // Construct the infobox for this span
    final List<Pair<String, String>> infos = new ArrayList<Pair<String, String>>();
    for (final Annotation annotation : spanAnnotations) {
      final String annotator = annotation.getOriginAnnotator();
      final String aType = annotation.getConcept();

      final Pair<String, String> p = Pair.of(annotator, aType);
      if (!infos.contains(p)) {
        infos.add(p);
      }
    }

    // sort the list
    Collections.sort(infos);

    return infos;
  }

  public void removeAnnotations() {
    // destroy the overlay nodes.

    if (overlayAnnotationTree != null) {
      overlayAnnotationTree.detach();
    }
  }

  public void removeBlinking() {
    if (overlayBlinkingTree != null) {
      overlayBlinkingTree.detach();
    }
  }

}