/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.toolkit;

import java.io.*;
import java.util.*;

import javax.xml.parsers.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationType;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.AnnotatedDocumentModel;
import uk.ac.ox.cs.diadem.roseann.ontology.MaterializedOntologyQuery;



/**
 * An adapter which deals with utility tasks such as harvesting annotations from
 * annotated documents in predefined XML documents.
 * 
 * @author Luying Chen(luying dot chen at cs dot ox dot ac dot uk) - Department
 *         of Computer Science - University of Oxford
 * 
 */
public class AnnotatedDocXMLAdatpter {
	/**
	 * An utility method to harvest annotations from predefined XML files (in
	 * GATE output format) and load all the annotations into the annotateddocumentModel, where
	 * the annotations are divided into known ones and unknown ones.
	 * 
	 * @param annotatedDoc
	 *            : an annotated document in designed output format
	 * @param annotator
	 *            : annotator's name
	 * @param model
	 *            : the object representing the annotated document
	 */
	public static int harvestAnnotationXML(AnnotatedDocumentModel model, File annotatedDoc, String annotator) {
		int count = 0;
		if (annotatedDoc.getAbsolutePath().endsWith(".xml")) {
			// System.out.println(annotatedDoc.getName() +
			// " is an XML file, now harvesting annotations...");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db;

			try {
				db = dbf.newDocumentBuilder();
				Document dom = db.parse(annotatedDoc.getAbsolutePath());
				// get the root element
				Element docEle = dom.getDocumentElement();

				// get a nodelist of elements
				NodeList nl = docEle.getElementsByTagName("AnnotationSet");
				int nodeNum = nl.getLength();
				if (nl != null && nodeNum > 0) {
					Set<Annotation> knownAnnotations = new HashSet<Annotation>();
					Set<Annotation> unknownAnnotations = new HashSet<Annotation>();
					
					for (int i = 0; i < nodeNum; i++) {
						Element el = (Element) nl.item(i);
						String annotationGroup = el.getAttribute("Name");
						// only entity annotations are harvested
						if (annotationGroup.equalsIgnoreCase("")) {
							NodeList annotationList = el.getElementsByTagName("Annotation");
							//create a new model
							
							for (int j = 0; j < annotationList.getLength(); j++) {
								Element annotationElem = (Element) annotationList.item(j);
								final Annotation oneAnno = fillAnnotationObj( annotationElem, annotator);
								if(oneAnno!=null){
									if(MaterializedOntologyQuery.getAllSupertypes(oneAnno.getConcept())==null){
										unknownAnnotations.add(oneAnno);
									}else{
										knownAnnotations.add(oneAnno);
									}
									count++;
									
								}
							}
							
						}
					}
					Set<Annotation> known = model.getKnownAnnotations();
					if(known==null){
						model.setKnownAnnotations(knownAnnotations);
					}else{
						known.addAll(knownAnnotations);
					}
					Set<Annotation> unknown = model.getUnknownAnnotation();
					if(unknown==null){
						model.setUnknownAnnotation(unknownAnnotations);
					}else{
						unknown.addAll(unknownAnnotations);
					}
					
					
				}
			} catch (ParserConfigurationException e) {
				System.err.println(e.getMessage());
			} catch (SAXException e) {
				System.err.println(e.getMessage());
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}

		} else {
			System.out.println("The file is not and XML file, skipping...");
		}
	return count;

	}

	/**
	 * A util method to harvest basic information (span and label) from an
	 * annotation element
	 * @param annotationElem
	 * @param annotator
	 */
	private static Annotation fillAnnotationObj(Element annotationElem,String annotator) {
		String startNodeVal = annotationElem.getAttribute("StartNode");
		String endNodeVal = annotationElem.getAttribute("EndNode");
		if(startNodeVal.isEmpty()){
			return null;
		}
		if(endNodeVal.isEmpty()){
			return null;
		}
		Annotation oneAnno = new EntityAnnotation();
		int startPos = Integer.parseInt(startNodeVal);
		int endPos = Integer.parseInt(endNodeVal);
		String id = annotationElem.getAttribute("Id");
		String predStr = annotationElem.getAttribute("Type");
		if (predStr == null || predStr.isEmpty()) {
			return null;
		}
		oneAnno.setId(id);
		oneAnno.setAnnotationClass(AnnotationClass.INSTANCE);
		oneAnno.setConcept(predStr);
		oneAnno.setOriginAnnotator(annotator);
		oneAnno.setStart(startPos);
		oneAnno.setEnd(endPos);
		oneAnno.setType(AnnotationType.ENT);
		return oneAnno;
	}
	/**
	 * An utility method to harvest annotations from predefined XML files (in
	 * GATE output format)
	 * 
	 * @param annotatedDoc
	 *            : an annotated document in designed output format
	 * @param annotator
	 *            : annotator's name
	 */
	public static Set<Annotation> harvestAnnotationXML( File annotatedDoc, String annotator) {
		
		Set<Annotation> annotations = new HashSet<Annotation>();
		if (annotatedDoc.getAbsolutePath().endsWith(".xml")) {
			// System.out.println(annotatedDoc.getName() +
			// " is an XML file, now harvesting annotations...");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db;

			try {
				db = dbf.newDocumentBuilder();
				Document dom = db.parse(annotatedDoc.getAbsolutePath());
				// get the root element
				Element docEle = dom.getDocumentElement();

				// get a nodelist of elements
				NodeList nl = docEle.getElementsByTagName("AnnotationSet");
				int nodeNum = nl.getLength();
				if (nl != null && nodeNum > 0) {
					
					for (int i = 0; i < nodeNum; i++) {
						Element el = (Element) nl.item(i);
						String annotationGroup = el.getAttribute("Name");
						// only entity annotations are harvested
						if (annotationGroup.equalsIgnoreCase("")) {
							NodeList annotationList = el.getElementsByTagName("Annotation");
							//create a new model
							
							for (int j = 0; j < annotationList.getLength(); j++) {
								Element annotationElem = (Element) annotationList.item(j);
								final Annotation oneAnno = fillAnnotationObj( annotationElem, annotator);
								if(oneAnno!=null){
									annotations.add(oneAnno);
									
								}
							}
							
						}
					}
					
					
					
				}
			} catch (ParserConfigurationException e) {
				System.err.println(e.getMessage());
			} catch (SAXException e) {
				System.err.println(e.getMessage());
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}

		} else {
			System.out.println("The file is not and XML file, skipping...");
		}
	return annotations;

	}


}
