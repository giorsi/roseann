/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.training;

import java.io.*;
import java.util.*;

import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.dataModel.Event;






/**
 * This class defines a data structure to keep the event-based training data
 * for each state.
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class TrainingDataPerState {
	private Map<String, List<Event>> eventBag;
	private String initState;
	public TrainingDataPerState(String initState){
		eventBag = new HashMap<String, List<Event>>();	
		this.initState = initState;
	}
	/**
	 * @param preEvent 
	 * @param
	 * @return
	 */
	public void put(Event oneEvent, Event preEvent) {
		//String outcome = oneEvent.getOutcome();
		String preOutcome = preEvent==null?initState:preEvent.getOutcome();
		
		if(oneEvent.isBeginOfSentence()){
			preOutcome = initState;
			
		}
		//Only pairs wihin sentences are considered. 
		
		List<Event> myEventStreamObj = eventBag.get(preOutcome);
		if(myEventStreamObj==null){
			myEventStreamObj = new ArrayList<Event>();
			eventBag.put(preOutcome, myEventStreamObj);
		}
		myEventStreamObj.add(oneEvent);
		
		
	}
	public Iterator<String> getStateIterator(){
		return eventBag.keySet().iterator();
	}
	/**
	 * @param
	 * @return
	 */
	public List<Event> get(String stateName) {
		
		return eventBag.get(stateName);
	}
	
	public boolean flushToFile(String folderName){
		File rawEventsDir = new File(folderName);
		if(!rawEventsDir.exists()){
			rawEventsDir.mkdir();
		}
		Iterator<String> keyItr = eventBag.keySet().iterator();
		while(keyItr.hasNext()){
			String oneKey = keyItr.next();
			File keyFile = new File(folderName+"/"+oneKey);
			FileOutputStream stream;
			try {
				stream = new FileOutputStream(keyFile, true);
				OutputStreamWriter writer = new OutputStreamWriter(stream, "UTF-8");
				List<Event> value = eventBag.get(oneKey);
				for(Event oneEvent:value){
					writer.write(oneEvent.toString()+"\n");
				}
				 writer.close();
		         stream.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		   
		}
		
		return true;
	}
	
	public static TrainingDataPerState getEventsByPrevState(List<Event> eventStream, String initState){
		TrainingDataPerState eventBag = new TrainingDataPerState(initState);
		for(int i=0;i<eventStream.size();i++){
			Event currentEvent=eventStream.get(i);
			Event prevEvent = null;
			if(i==0){
				prevEvent = null;
			}else{
				prevEvent = eventStream.get(i-1);
				
			}
			
			eventBag.put(currentEvent,prevEvent);
			
		}
		
		return eventBag;
	}
	
	public static List<Event> getEventStream(String stateName, TrainingDataPerState eventBag){
		return eventBag.get(stateName);
	}
	public static String removeBIOTag(String conceptWithBIO){
		String res = conceptWithBIO.replace("B_", "");
		res = res.replace("I_", "");
		return res;
	}
}
