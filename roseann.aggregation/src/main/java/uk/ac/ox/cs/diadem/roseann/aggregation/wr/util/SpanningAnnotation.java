/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.wr.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.roseann.ontology.MaterializedOntologyQuery;
import uk.ac.ox.cs.diadem.roseann.util.*;
import uk.ac.ox.cs.diadem.roseann.util.graph.*;

/**
 * Utility class to work on the annotations over a document. Method such as retrieve spanning set or conflicting
 * set are offered
 * 
 * @author Stefano Ortona (stefano dot ortona at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */

public class SpanningAnnotation {

	/**
	 * Method to retrieve the spanning sets over a set of annotation.
	 * A spanning set is a set where all the annotations share at least one token among each other
	 * @param annotations
	 * 				the input set of annotations
	 * @return		set of spanning sets
	 */
	public static Set<Set<Annotation>> calculateSpanningSet(final Set<Annotation> annotations){
		Set<Set<Annotation>> setSpanningSet=new HashSet<Set<Annotation>>();

		//order annotation by start-end
		Set<Annotation> orderedAnnotations=new TreeSet<Annotation>(new CompareAnnotationByStartEnd());
		orderedAnnotations.addAll(annotations);

		Set<Annotation> actualSpanningSet=null;
		Set<Annotation> previousSpanningSet=new HashSet<Annotation>();
		final Set<Annotation> nextAnnotations=new TreeSet<Annotation>(new CompareAnnotationByStartEnd());
		nextAnnotations.addAll(orderedAnnotations);

		//iterate over all the annotations, ordered by start-end
		for(Annotation annotation:orderedAnnotations){

			//calculate the actual spanning set, so the spanning set where the actual annotation share at least one token with all the others
			actualSpanningSet=new HashSet<Annotation>();
			actualSpanningSet.add(annotation);
			final long actualEnd=annotation.getEnd();
			nextAnnotations.remove(annotation);
			long nextStart=-1;
			Iterator<Annotation> nextAnnotationsIterator=nextAnnotations.iterator();

			while(nextAnnotationsIterator.hasNext()&&nextStart<actualEnd){

				final Annotation nextAnnotation=nextAnnotationsIterator.next();


				nextStart=nextAnnotation.getStart();
				if(nextStart<actualEnd){
					actualSpanningSet.add(nextAnnotation);
				}


			}

			final Set<Annotation> differenceSet=new HashSet<Annotation>();
			differenceSet.addAll(actualSpanningSet);
			differenceSet.removeAll(previousSpanningSet);
			if(differenceSet.size()==0){
				continue;
			}
			previousSpanningSet=actualSpanningSet;

			setSpanningSet.add(actualSpanningSet);

		}

		return setSpanningSet;

	}

	/**
	 * Given a set of overlapping annotations, return a set of set of conflicting annotations
	 * Every element of the set is another set containing the conflicting annotations
	 * We build the annotation conflict graph, where every annotation identifies a node and there's an edge 
	 * between a node a and b iff isConflcit(a,b) is true
	 * Once we built the graph every set represents a connected component of the graph
	 * @param annotations
	 * 				Set of overlapping annotations
	 * @return		Set of set of conflicting annotation, where each element is a set of conflicting annotations
	 */
	public static Set<Set<Annotation>> getConflictingSet(final Collection<Annotation> annotations){

		//build the conflict graph
		DAG<Annotation,Integer> conflictGraph=new DAG<Annotation,Integer>();		
		for(Annotation annotation:annotations){
			final Node<Annotation> actualNode=conflictGraph.addNode(annotation, 0);
			for(Annotation nestedAnnotation:annotations){

				final Node<Annotation> possibleNeighbour=conflictGraph.addNode(nestedAnnotation, 0);

				Boolean areConflict = isConflict(annotation, nestedAnnotation);
				//if the 
				if(areConflict==null)
					continue;
				//add the edge between two annotations if and only if they are in conflict
				if(!nestedAnnotation.equals(annotation)&&areConflict)
					conflictGraph.addEdge(actualNode, possibleNeighbour);
			}
		}

		//calculate the connected components
		final Set<Annotation> consideredAnnotation=new HashSet<Annotation>();
		final Set<Set<Annotation>> conflictingSet=new HashSet<Set<Annotation>>();
		Set<Annotation> actualConflcitingSet=new HashSet<Annotation>();

		//Iterating over all the annotations, calculate all the connected components and add each connected component to the conflicting_set
		for(Annotation annotation:annotations){
			if(!consideredAnnotation.contains(annotation)){
				actualConflcitingSet=new HashSet<Annotation>();
				final Node<Annotation> actualNode=conflictGraph.getNode(annotation);
				Collection<Node<Annotation>> connectedComponents=conflictGraph.getReachableNode(actualNode);
				for(Node<Annotation> reachable:connectedComponents){
					actualConflcitingSet.add(reachable.getLabel());
					consideredAnnotation.add(reachable.getLabel());
				}
				conflictingSet.add(actualConflcitingSet);

			}

		}

		return conflictingSet.size()>0 ? conflictingSet : null;
	}


	/**
	 * Method to check wether or not two annotations a and b are in conflict.
	 * Two annotations are in conflict if we are in one of the following 3 cases:
	 * a) a is fully contained in b (or b is fully contained in a) and types of a and b are one the superclass of the other
	 * b) a and b cover exactly the same span and type of a and b are either disjoint or one superclass of the other
	 * c) a and b share one or more token, but both a and b cover other tokens not shared with other annotation
	 * @param firstAnnotation
	 * 				The first annotation to compare
	 * @param secondAnnotation
	 * 				The second annotation to compare
	 * @return		True iff the two annotations are in conflict, false if they are not in conflict or if one of the two types is unknown
	 */

	private static Boolean isConflict(final Annotation firstAnnotation,final Annotation secondAnnotation){
		final String firstType=firstAnnotation.getConcept();
		final String secondType=secondAnnotation.getConcept();
		final long firstStart=firstAnnotation.getStart();
		final long firstEnd=firstAnnotation.getEnd();
		final long secondStart=secondAnnotation.getStart();
		final long secondEnd=secondAnnotation.getEnd();

		//calculate the superclasses for each type
		Set<String> firstSuperclasses=MaterializedOntologyQuery.getAllSupertypes(firstType);
		Set<String> secondSuperclasses=MaterializedOntologyQuery.getAllSupertypes(secondType);
		//if one of the two types is unknown, return true
		if(firstSuperclasses==null||secondSuperclasses==null){
			return true;
		}

		Boolean areDisjoint = MaterializedOntologyQuery.areDisjoint(firstType, secondType);
		//if one of the two type is unknwon, return true
		if(areDisjoint==null)
			return true;
		//wrong overlapping, c)
		if((firstStart<secondStart&&secondStart<firstEnd&&firstEnd<secondEnd)||
				(secondStart<firstStart&&firstStart<secondEnd&&secondEnd<firstEnd)){
			if(secondSuperclasses.contains(firstType)||firstSuperclasses.contains(secondType)||
					areDisjoint)
				return true;

		}

		//fully contained, a)
		if((firstStart>=secondStart&&firstEnd<=secondEnd)||(secondStart>=firstStart&&secondEnd<=firstEnd)){
			if(secondSuperclasses.contains(firstType)||firstSuperclasses.contains(secondType))
				return true;

			//same span, b)
			if(firstStart==secondStart&&firstEnd==secondEnd&&areDisjoint){
				return true;
			}

		}

		//if we're not in any of above 3 cases, the annotation are not in conflict
		return false;


	}

}
