/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.training;

import java.io.*;
import java.util.*;

import uk.ac.ox.cs.diadem.roseann.util.FileAdapter;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.Memm;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.dataModel.Event;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.util.Line2Event;




/**
 * An util class to construct training data for each state of memm
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class NaiveTrainingPrepare {

	
	
	public static void prepare(final String inputFolder, final String outputFolder, final Memm memm) throws IOException  {
		System.out.println("Begin to collect training data for each states of MEMM.");

		
		File poolFolder = new File(outputFolder);
		if(!poolFolder.exists()){
			poolFolder.mkdir();
		}
		File docTobeProcessed = new File(inputFolder);
		if(!docTobeProcessed.exists()){
			System.out.println("The training data is empty.");
			return;
		}

		List<Event> res = null;
		TrainingDataPerState eventBag = null;
		StringBuilder sb = null;
		int count = 0;
		
		for (File doc : docTobeProcessed.listFiles()) {
			System.out.println("Now process document " + (count++)+":"+doc.getName());

			System.out.println();
			res = new ArrayList<Event>();
			BufferedReader input = null;
			

				input = new BufferedReader(new FileReader(doc));

				String line = null;

				while ((line = input.readLine()) != null) {
					if (line.trim().length() > 0) {
						Event oneRawEvent = Line2Event.line2Event(true, line.trim(), memm.getMyParams().getColSkipNum(), memm.getMyParams().getFeatureSplitter());
						if(oneRawEvent!=null){
							res.add(oneRawEvent);
						}			
					}
				}
				input.close();
				eventBag = TrainingDataPerState.getEventsByPrevState(res,memm.getMyParams().getInitialState());
				Iterator<String> keyItr = eventBag.getStateIterator();
				while (keyItr.hasNext()) {
					String state = keyItr.next();
					
					sb = new StringBuilder();
					List<Event> rawEventsPerState = eventBag
							.get(state);

					for (Event oneRawEvent : rawEventsPerState) {

						
						sb.append(oneRawEvent);
						sb.append(System.getProperty("line.separator"));
					}

					appendTrainingSet(state, sb.toString(), outputFolder);
				}		

		}

	}

	/**
	 * @param flushRootFolder
	 * @param
	 * @return
	 * @throws IOException 
	 */
	private static void appendTrainingSet(String state, String content,
			String flushRootFolder) throws IOException {
		File eventsDir = new File(flushRootFolder);
		if (!eventsDir.exists()) {
			eventsDir.mkdir();
		}
		File datDir = new File(flushRootFolder + "/dat");
		if (!datDir.exists()) {
			datDir.mkdir();
		}
		File stateTraining = new File(flushRootFolder + "/dat/" + state
				+ ".dat");
		FileOutputStream stream;
	
			stream = new FileOutputStream(stateTraining, true);
			OutputStreamWriter writer = new OutputStreamWriter(stream, "UTF-8");
			writer.write(content);
			writer.close();
			stream.close();
	

	}
	public static void main(String[] args) throws IOException{
		prepare("", "", new Memm());
	}
	public static void materializeStatesFile(Set<String> states, String outputFile, String initState){
		StringBuilder sb = new StringBuilder();
		for(String oneState:states){
			if(oneState.contains(initState)){
				continue;
			}
			sb.append(oneState);
			sb.append("\n");
		}
	
		//System.out.println(sb.toString());
		File writeTo = new File(outputFile);
		if(writeTo.exists()){
			writeTo.delete();
		}
		FileAdapter.writeToFile(writeTo, sb.toString());
		
	}
	public static void materializeSingleTransitionFile(Map<String, String> transMap, String outputFile){
		StringBuilder sb = new StringBuilder();
		for(Map.Entry<String, String> oneEntry:transMap.entrySet()){
			sb.append(oneEntry.getKey()+","+oneEntry.getValue());
			sb.append("\n");
		}
	
		//System.out.println(sb.toString());
		File writeTo = new File(outputFile);
		if(writeTo.exists()){
			writeTo.delete();
		}
		FileAdapter.writeToFile(writeTo, sb.toString());
		
	} 
	public static void materializeSingleOutgoing(Map<String, String> singleTransitionStateMap, File nulldat){
		
			
			String content = FileAdapter.getPureFileContents(nulldat.getAbsolutePath());
			String[] subStr = content.split(" ");
			singleTransitionStateMap.put(nulldat.getName().replace(".dat", ""), subStr[subStr.length-1].trim());
			
			
		
	}
	public static void materializeAllState(Set<String> states, File stateTrainData){
		
		states.add(stateTrainData.getName().replace(".dat", ""));
	
}

}
