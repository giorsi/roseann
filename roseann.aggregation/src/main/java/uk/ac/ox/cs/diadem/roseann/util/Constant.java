/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.util;

/**
 * @author Luying Chen(luying dot chen at cs dot ox dot ac dot uk) - Department
 *         of Computer Science - University of Oxford
 */
public class Constant {
	

	// Document parser
	public static final String EMPTY_STR = "";
	public static final String PARA_BDRY = "";
	public static final Object DOC_BDRY = null;
	public static final char SENT_BDRY = '.';
	public static final char SPACE = ' ';
	// public static final String LINESEPARATOR =
	// System.getProperty("line.separator");
	public static final String LINESEPARATOR = "\n";

	public enum AnnotatorNameInd {
		openCalais,alchemyAPI,extractiv,dbpediaSpotlight,zemanta,gate,lupedia,yahooAPI,illinoisNER,wikimeta,stanfordNER,saplo;
		public static boolean isValidValue(String identifier){
			for (AnnotatorNameInd oneValue : AnnotatorNameInd.values()) {
		        if(oneValue.toString().equals(identifier))
		           return true;
		    }
		    return false;
		}
	}
	public enum AnnotatorNameAgg {
		memm,wr,gold_standard;
		public static boolean isValidValue(String identifier){
			for (AnnotatorNameAgg oneValue : AnnotatorNameAgg.values()) {
		        if(oneValue.toString().equals(identifier))
		           return true;
		    }
		    return false;
		}
	}

	// gui
	public static final int MOUSE_MOVEMENT_TIMER_DELAY = 1000;


	public static final int ALPHA = 100;

	
	//Corpus tree
	//public static final String STATIC_CORPUS_ROOT = "mockDoc";
	//public static final String STATIC_ANNO_CORPUS_ROOT = "mockAnnoDoc";
	public static final String STATIC_CORPUS_ROOT = 
			"sampleText/SampleCorpus";
	public static final String STATIC_ANNO_CORPUS_ROOT = 
			"sampleText/SampleAnnotatedDoc";
	
	
	public static final String WEB_PAGES_ROOT = "sampleWeb";
	
	public static final String PDF_DOCS_ROOT = "samplePDF";
	
public static final String PDF_STATIC_MODEL_NAME = "static_model";
	
	public static final String WEB_STATIC_MODEL_NAME = "static_model";

}
