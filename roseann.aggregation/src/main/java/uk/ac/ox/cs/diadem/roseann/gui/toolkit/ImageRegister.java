/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.toolkit;

import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * @author Luying Chen(luying dot chen at cs dot ox dot ac dot uk) - Department
 *         of Computer Science - University of Oxford
 */
public class ImageRegister {

	public static Icon gotoObject = new ImageIcon("./myBin/gui/images/gotoobj_tsk.gif");
	public static Icon gotoGroup = new ImageIcon("./myBin/gui/images/correction_change.gif");
	public static Icon groupBy = new ImageIcon("./myBin/gui/images/templates.gif");
	public static Icon groupByType = new ImageIcon("./myBin/gui/images/javaassist_co.gif");
	public static Icon groupByObj = new ImageIcon("./myBin/gui/images/definingtype_sort_co.gif");

	public static Icon gotoSampleQuery = new ImageIcon("./myBin/gui/images/softweb_back.png");

	// MENU BAR
	public static Icon flatViewMenu = new ImageIcon("./myBin/gui/images/stckframe_obj.gif");
	public static Icon colorPaneView = new ImageIcon("./myBin/gui/images/highlightall.gif");
	public static Icon flatViewIcon = new ImageIcon("./myBin/gui/images/history_list.gif");

	// COLOR TREE
	public static Icon leafNodeIcon = new ImageIcon("./myBin/gui/images/welcome16.gif");
	public static Icon rootNodeIcon = new ImageIcon("./myBin/gui/images/samples.gif");
	public static Icon annotatorNodeIcon = new ImageIcon("./myBin/gui/images/attribute_obj.gif");
	public static Icon entityNodeIcon = new ImageIcon("./myBin/gui/images/brkp_obj.gif");
	public static Icon relationNodeIcon = new ImageIcon("./myBin/gui/images/breakpoint_view.gif");
	public static Icon sentimentNodeIcon = new ImageIcon("./myBin/gui/images/person.gif");

	public static Icon letterSort = new ImageIcon("./myBin/gui/images/alphab_sort_co.gif");

	// annotation detail
	public static Icon browseerUri = new ImageIcon("./myBin/gui/images/internal_browser.gif");

	// corpus tree
	public static Icon corpuraRootIcon = new ImageIcon("./myBin/gui/images/library_obj.gif");
	public static Icon docNodeIcon = new ImageIcon("./myBin/gui/images/sourceEditor.gif");
	public static Icon corpusNodeIcon = new ImageIcon("./myBin/gui/images/packd_obj.gif");

	// popup menu for corpus register
	public static Icon addNodeIcon = new ImageIcon("./myBin/gui/images/add_correction.gif");
	public static Icon deleteNodeIcon = new ImageIcon("./myBin/gui/images/remove_correction.gif");
	public static Icon delNodeIcon = new ImageIcon("./myBin/gui/images/remove_correction.gif");
	public static Icon refreshNodeIcon = new ImageIcon("./myBin/gui/images/refresh_nav.gif");
	public static Icon helpNodeIcon = new ImageIcon("./myBin/gui/images/help.gif");

	public static Icon kbRootIcon = new ImageIcon("./myBin/gui/images/db.Schema.many.16x16.png");

	public static Icon kbNodeIcon = new ImageIcon("./myBin/gui/images/db.Schema.16x16.png");
	public static Icon kbConfigIcon = new ImageIcon("./myBin/gui/images/debugt_obj.gif");

	public static Icon reasoningIcon = new ImageIcon("./myBin/gui/images/readwrite_obj.gif");

	// KB tree
	public static Icon vocIcon = new ImageIcon("./myBin/gui/images/jar_obj.gif");
	// deep panel
	public static Icon preNevIcon = new ImageIcon("./myBin/gui/images/prev_nav.gif");
	public static Icon nextNevIcon = new ImageIcon("./myBin/gui/images/next_nav.gif");
	public static Icon homeIcon = new ImageIcon("./myBin/gui/images/home_nav.gif");

	// Entry panel
	public static Icon quasarIcon = new ImageIcon("./myBin/gui/images/QUASAR.jpg");
	public static Icon openTaskIcon = new ImageIcon("./myBin/gui/images/open-task.gif");
	public static Icon clearIcon = new ImageIcon("./myBin/gui/images/clear_co.gif");
	public static Icon runIcon = new ImageIcon("./myBin/gui/images/run_exc.gif");

}
