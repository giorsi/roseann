/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.swing.util;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.text.DefaultHighlighter.DefaultHighlightPainter;
/**
 * A simple highlight painter that renders a blinking rectangle around the
 * text area to be highlighted.
 * @author Luying Chen
 * 
 */
public class BlinkPainter extends DefaultHighlightPainter {
    Color blinkColor;
    Color activeColor;
    JComponent owner;
    Timer t;
    public BlinkPainter(Color c, int blinkRate, JComponent comp) {
        super(null);
        blinkColor=c;
        owner = comp;
        t =new Timer(blinkRate, new ActionListener() {
        	int count = 20;
            public void actionPerformed(ActionEvent e) {
                changeColor();
                count--; //shut down by itself
                if(count==0){
                	stopTimer();
                }
            }
        });
        t.start();
    }

    protected void changeColor() {
        if (activeColor==blinkColor) {
            activeColor= owner.getBackground();
        }
        else {
            activeColor=blinkColor;
        }
        owner.repaint();
    }

    public Color getColor() {
        return activeColor;
    }
    public void stopTimer(){
    	t.stop();
    }
    public void startTimer(){
    	t.start();
    }

}
