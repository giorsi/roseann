/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg;

import java.io.File;
import java.util.*;
import java.util.concurrent.*;

import org.apache.commons.configuration.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;

import uk.ac.ox.cs.diadem.roseann.api.aggregation.AbstractAggregator;
import uk.ac.ox.cs.diadem.roseann.api.aggregation.AggregationException;
import uk.ac.ox.cs.diadem.roseann.api.aggregation.Trainable;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.dataModel.Event;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.prediction.concurrency.*;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.prediction.decode.Decoder;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.prediction.decode.ViterbiDecoder;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.training.ModelTrainingInvoker;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.training.NaiveTrainingPrepare;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.util.*;

import uk.ac.ox.cs.diadem.roseann.ontology.MaterializedOntologyQuery;


/**
 * This class defines a maximum entropy markov model transducer which aggregates annotations
 * from individual annotators.
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class Memm extends AbstractAggregator implements Trainable {

	/**
	 * Logger
	 */
	static final Logger LOGGER = LoggerFactory.getLogger(Memm.class);
	/**
	 * In the constructor, user parameters are set if applicable. Otherwise,
	 * the default parameters are used which are defined in MemmParam.java
	 */
	public Memm() {
		ConfigurationFacility.getConfiguration();
		this.setAggregatorName("memm");
		//default setting
		setDecoder(new ViterbiDecoder());
		setFeatureExtractor(new NaiveFeatureExtractorImpl());
		try {
			//set the user specified parameters
			readConfigurationFile();
			myParams = initParams();
		} catch (ConfigurationException e) {
			
			throw new AggregationException("Fail to read configuration " +
					"file for MEMM which is " +
					"specified in AggregatorConfiguration.xml.",e,LOGGER);
		}
		
		
	}
	/**
	 * An util method to initialize the pool of trained models, which loading
	 * the model files from file system with specified root folder.
	 * @return ModelPool the poole of trained models
	 */
	private ModelPool initPool() {
		ModelPool newPool = new ModelPool(myParams.getPoolName(), myParams.getInitialState());
		return newPool ;
	}
	//the parameters used in Memm training/testing
	private MemmParam myParams;
	//set of annotators in alphabetical  order that memm takes into consider
	private String[] annotators;
	/**
	 * Method to load the user-specified parameters into parameter object
	 * @return  MEMMParam the parameter object
	 */
	private MemmParam initParams() {
		myParams = new MemmParam();
		final Map<String, Set<String>> userParams = this.getParametersMap();
		if(userParams!=null){
			Set<String> values = userParams.get(MemmParam.EVIDENCE_CUTOFF);
			if(values!=null&&values.size()>0){
				final int cutoff = Integer.parseInt(values.iterator().next());
				if(cutoff>=0)
					myParams.setCutoff(cutoff);
				else{
					LOGGER.debug("Invalid parameter value of cutoff for MEMM.");
				}
			}
			values = userParams.get(MemmParam.INITSTATE_NAME);
			if(values!=null&&values.size()>0){
				myParams.setInitialState(values.iterator().next());
			}
			values = userParams.get(MemmParam.MODEL_POOL);
			if(values!=null&&values.size()>0){
				myParams.setPoolName(values.iterator().next());
			}
			values = userParams.get(MemmParam.ONTOLOGY_INVOLVED);
			if(values!=null&&values.size()>0){
				myParams.setCheckStateConsistency(Boolean.parseBoolean(values.iterator().next()));
				
			}
			values = userParams.get(MemmParam.PRUNING_UPBOUND);
			if(values!=null&&values.size()>0){
				myParams.setThresholdMax(Double.parseDouble(values.iterator().next()));
				
			}
			values = userParams.get(MemmParam.PRUNING_LOWBOUND);
			if(values!=null&&values.size()>0){
				myParams.setThresholdMin(Double.parseDouble(values.iterator().next()));
				
			}
			values = userParams.get(MemmParam.THREAD_COUNT);
			if(values!=null&&values.size()>0){
				final int count = Integer.parseInt(values.iterator().next());
				if(count>=0)
				myParams.setThreadCount(count);
				else{
					LOGGER.debug("Invalid parameter value of threadCount for MEMM.");
				}
			}
			values = userParams.get(MemmParam.RETURN_SAFE_CONCEPT);
			if(values!=null&&values.size()>0){
				myParams.setReturnSafyConcept(Boolean.parseBoolean(values.iterator().next()));
				
			}
			values = userParams.get(MemmParam.TERMINAL_CONDITION);
			if(values!=null&&values.size()>0){
				final int count = Integer.parseInt(values.iterator().next());
				if(count>=0)
				myParams.setIterationNum(count);
				else{
					LOGGER.debug("Invalid parameter value of iterationNum for MEMM.");
				}
				
			}
			values = userParams.get(MemmParam.MULT_OPINION_SPLITTER);
			if(values!=null&&values.size()>0){
				myParams.setOpinionSplitter(values.iterator().next());
			}
			values = userParams.get(MemmParam.FEATURE_COL_SPLITTER);
			if(values!=null&&values.size()>0){
				myParams.setFeatureSplitter(values.iterator().next());
			}
			values = userParams.get(MemmParam.SKIP_COLUMN_NUM);
			if(values!=null&&values.size()>0){
				myParams.setColSkipNum(Integer.parseInt(values.iterator().next()));
			}
		}
		return myParams;
	}
	
	
	/**
	 * @return the myParams
	 */
	public MemmParam getMyParams() {
		
		return myParams==null?new MemmParam():myParams;
	}

	/**
	 * @param myParams the myParams to set
	 */
	public void setMyParams(MemmParam myParams) {
		this.myParams = myParams;
	}
	/**
	 * The model pool configured for this MEMM instance
	 */
	private ModelPool modelPool;
	/**
	 * The decoder of MEMM to decode a markov sequence, by default we use
	 * ViterbiDecoder to generate the best labeling sequence
	 */
	private Decoder decoder;
	/**
	 * The feature extractor for MEMM to train/test data
	 */
	private FeatureExtractor featureExtractor ;


	/**
	 * @return the featureExtractor
	 */
	public FeatureExtractor getFeatureExtractor() {
		return featureExtractor;
	}

	/**
	 * @param featureExtractor the featureExtractor to set
	 */
	public void setFeatureExtractor(FeatureExtractor featureExtractor) {
		this.featureExtractor = featureExtractor;
	}

	@Override
	public Set<Annotation> reconcile(String originalText,
			Set<Annotation> originalAnnotation, Set<String> involvedAnnotators) {
		LOGGER.info("MEMM starts to reconcile...");
		if(originalText==null||originalAnnotation==null ||involvedAnnotators==null || originalAnnotation.size()==0){
			return new HashSet<Annotation>();
		}
		//init the model pool
		if(this.modelPool==null){
			modelPool = initPool();
		}
		
		// chop the annotated doc into feature-based sequence
		List<Event> res;
		List<String> baseAnnotators = new ArrayList<String>();
		baseAnnotators.addAll(involvedAnnotators);
		Collections.sort(baseAnnotators);
		String[] annotatorsToAgg = baseAnnotators
				.toArray(new String[baseAnnotators.size()]);
		this.annotators = annotatorsToAgg;
		NaiveFeatureExtractorImpl fe = (NaiveFeatureExtractorImpl)getFeatureExtractor();
		fe.setAnnotatorNames(annotators);
		res = ModelAdapter.convertToEventStream(originalAnnotation,
				originalText, false, this);
		// run memm and viterbi

		String outcomeSeqStr = runMEMMCore(res);

		// reconstruct normal atoms from B-I-O tag sequence
		final Set<Annotation> resAnnotations = flushAtomToModel(res,
				outcomeSeqStr);
		LOGGER.info("MEMM finished to reconcile and found "+resAnnotations.size()+ " annotations.");
		return resAnnotations;

	}

	
	/* (non-Javadoc)
	 * @see uk.ac.ox.cs.diadem.roseann.api.aggregation.Trainable#train(java.lang.String, java.lang.String)
	 */
	@Override
	public void train(String trainingData, String outputFileName) {
		Set<String> states = new HashSet<String>();
		Map<String, String> singleTrans = new HashMap<String, String>();
		File root = new File(trainingData);
		File[] files = root.listFiles();
		
		for (File oneFile : files) {

			LOGGER.info("Now train data for " + oneFile);

			final boolean isOk = ModelTrainingInvoker.trainModel(oneFile,
					this, outputFileName);
			if (!isOk) {
				NaiveTrainingPrepare.materializeSingleOutgoing(singleTrans,
						oneFile);
			}
			NaiveTrainingPrepare.materializeAllState(states, oneFile);
		}
		LOGGER.info("Materializing all the states...");
		NaiveTrainingPrepare.materializeStatesFile(states, outputFileName
				+ "/states.txt", myParams.getInitialState());
		LOGGER.info("Materializing all the single transitions...");
		NaiveTrainingPrepare.materializeSingleTransitionFile(singleTrans,
				outputFileName + "/singleTransition.txt");
	}

	/**
	 * The core procedure to run memm over the the event sequence and get the 
	 * corresponding BIO labeling sequence.
	 * @param res a ordered list of token sequence
	 * @return String  the resulting labeling sequence
	 */
	@SuppressWarnings("unchecked")
	private String runMEMMCore(final List<Event> res) {
		LOGGER.info("Running MEMM over input token sequence...");
		// ArrayList<String> sentence = new ArrayList<String>();
		List<Event> oneSentEvents = new ArrayList<Event>();
		// System.out.println("Now process document: ");
		final int bufferSize = (int) myParams.getThreadCount();
		int bufferCount = myParams.getThreadCount();
		final List<Event>[] buffer = new ArrayList[bufferSize];
		int seqCount = 0;
		bufferCount = bufferSize;
		for (int i = 0; i < bufferSize; i++) {
			buffer[i] = null;
		}
		// FeatureExtractorInterface featureExtractor = new
		// OpenNLPFeatureExtractorImpl();
		final StringBuilder sb = new StringBuilder();
		for (final Event qe : res) {

			if (qe.isBeginOfSentence()) {
				if (oneSentEvents.size() > 0) {
					seqCount++;
					if (bufferCount == 0) {
						sb.append(decode(seqCount - bufferSize, buffer));
						for (int i = 0; i < bufferSize; i++) {
							buffer[i] = null;
						}
						bufferCount = bufferSize;
					}
					buffer[bufferSize - bufferCount] = oneSentEvents;
					bufferCount--;

				}
				oneSentEvents = new ArrayList<Event>();

			}
			oneSentEvents.add(qe);

		}
		if (oneSentEvents.size() > 0) {
			seqCount++;
			if (bufferCount == 0) {
				sb.append(decode(seqCount - bufferSize, buffer));
				for (int i = 0; i < bufferSize; i++) {
					buffer[i] = null;
				}
				bufferCount = bufferSize;
			}
			buffer[bufferSize - bufferCount] = oneSentEvents;
			bufferCount--;
			sb.append(decode(seqCount - bufferSize, buffer));

		}
		// System.out.println(sb.toString());
		return sb.toString();
	}

	/**
	 * An util method to invoke a number of tasks to labeling input sequences parallelly.
	 * @param taskID the id of the subtask
	 * @param eventsSeqArray a number of sequences to be annotated
	 * @return String a concat resulting output labeling string
	 */
	private String decode(final int taskID,
			final List<Event>[] eventsSeqArray) {
		final ExecutorService executor = Executors
				.newFixedThreadPool(myParams.getThreadCount());
		List<Future<ReturnUnit>> results;
		try {
			final List<ConcurrentWorker> tasks = new ArrayList<ConcurrentWorker>();
			for (int i = 0; i < eventsSeqArray.length; i++) {
				if (eventsSeqArray[i] != null) {
					tasks.add(new ConcurrentWorker(taskID + i,
							eventsSeqArray[i], this));
				}

			}
			results = executor.invokeAll(tasks);
			executor.shutdown();
			final Map<Integer, String> resCollector = new TreeMap<Integer, String>();
			for (final Future<ReturnUnit> result : results) {
				if (result != null) {
					final ReturnUnit oneRes = result.get();

					resCollector.put(oneRes.getSentID(), oneRes.getRes());
				}

			}
			final StringBuilder sb = new StringBuilder();
			for (final String oneResStr : resCollector.values()) {
				sb.append(oneResStr);
			}
			return sb.toString();
		} catch (final InterruptedException |ExecutionException e) {
			final String[] otherLbls  =  ReturnUnit.labelAsNothing(eventsSeqArray.length);
			return ReturnUnit.getConcatOutcomes(otherLbls);
		} 
		
	}
	
	/**
	 * This method is invoked if the parameter of "returnSafeConcept" is set with ture,
	 * which means only superclasses of those concepts in the evidence are shown
	 * @param origConcepts the set of original decoded concepts
	 * @param event the evidence of the token
	 * @return
	 */
	private Set<String> cleanConcept(final String[] origConcepts,final Event event){
		if(origConcepts==null){
			return new HashSet<String>();
		}
		Set<String> allTypes = new HashSet<String>();
		Set<String> toRemove = new HashSet<String>();
		for(final String oneConcept:origConcepts){
			if(oneConcept.isEmpty()){
				continue;
			}
			Set<String> saveConcepts = returnSafeConcept(oneConcept, event);
			if(saveConcepts!=null){
				allTypes.addAll(saveConcepts);
			}
			for(final String oneSaveConcept:saveConcepts){
				final Set<String> myAnc = MaterializedOntologyQuery.getAllSupertypes(oneSaveConcept);
				if(myAnc!=null){
					toRemove.addAll(myAnc);
					toRemove.remove(oneSaveConcept);
				}
			}
		}
		allTypes.removeAll(toRemove);
		return allTypes;
	}

	/**
	 * A method to construct final annotations according to the output labelling
	 * seuqence of MEMM
	 * @param res the input token sequence to be decoded
	 * @param outcomeStr the output labelling sequence
	 * @return Set<Annotation> final output annotations
	 */
	private Set<Annotation> flushAtomToModel(final List<Event> res,
			final String outcomeStr) {
		final String[] outcome = outcomeStr.split("\n");
		int i = 0;
		int count = 0;
		Set<Annotation> resAnno = new HashSet<Annotation>();
		final List<String> mockStack = new ArrayList<String>();
		final Set<Integer> hash = new HashSet<Integer>();
		final Set<String> beginSet = new HashSet<String>();
		final Set<String> inSet = new HashSet<String>();
		while (i <= outcome.length) {
			String iOutcome = i==outcome.length?"Other":outcome[i];
			if (iOutcome.equalsIgnoreCase("Other")) {// harvest all the
														// previous suspending
														// annotation
				for (int j = 0; j < mockStack.size(); j++) {
					if (!hash.contains(j) && mockStack.get(j).contains("B_")) {
						final String[] oneConcepts = mockStack.get(j)
								.replaceAll("B_", "").split(this.getMyParams().getOpinionSplitter());
						final Set<String> types = cleanConcept(oneConcepts,res.get(j));
						for (String aSaveConcept : types) {
							if(aSaveConcept.equalsIgnoreCase("_MediaOrg")||aSaveConcept.equalsIgnoreCase("B")){
								System.out.println("debug here");
							}
								if (!aSaveConcept.isEmpty()) {
									Annotation anno = new EntityAnnotation(
											this.getAggregatorName() + "_"
													+ (count++), aSaveConcept,
											res.get(j).getBeginEnd()[0],
											res.get(i - 1).getBeginEnd()[1],
											this.getAggregatorName(), null,
											1.0, AnnotationClass.INSTANCE);
									resAnno.add(anno);
								}

						}
						hash.add(j);
					}

				}
				mockStack.add("Other");
				hash.add(i);
			} else {// with "B_I_" decoration
				final String[] splitStrs = iOutcome.split(this.getMyParams().getOpinionSplitter());
				beginSet.clear();
				inSet.clear();

				for (final String oneConcept : splitStrs) {
					if (oneConcept.startsWith("B_")) {
						beginSet.add(oneConcept);
					} else {
						inSet.add(oneConcept);
					}
				}
				// some "I_" terms and some "B_" terms
				if (!inSet.isEmpty()) {// harvest some previous suspended
										// annotation
					
					for (int j = i - 1; j >= 0; j--) {
						
						if (!hash.contains(j)) {
							final String[] oneConcepts = mockStack.get(j)
									.split(this.getMyParams().getOpinionSplitter());
							Set<String> allTypes = new HashSet<String>();
							Set<String> toRemove = new HashSet<String>();
							for ( String oneConcept : oneConcepts) {
								if(oneConcept.isEmpty()){
									continue;
								}
								if (oneConcept.contains("B_")) {
									if (inSet.contains(oneConcept.replace("B_",
											"I_"))) {
										// do nothing
									} else { // harvesting it.
										oneConcept = oneConcept.replace("B_", "");
										if (!oneConcept
												.trim().isEmpty()) {
											final Set<String> safeConcepts = returnSafeConcept(
													oneConcept, res.get(j));
											if(safeConcepts!=null)
												allTypes.addAll(safeConcepts);
											for(final String oneSafe:safeConcepts){
												final Set<String> myAnc = MaterializedOntologyQuery.getAllSupertypes(oneSafe);
												if(myAnc!=null){
													toRemove.addAll(myAnc);
													toRemove.remove(oneSafe);
												}
											}
											

										}
										final String newOccupier = mockStack
												.get(j).replaceAll(
														"B_"+oneConcept, "");
										mockStack.remove(j);
										mockStack.add(j, newOccupier);

									}
								}

							}
							allTypes.removeAll(toRemove);
							for (final String aSaveConcept : allTypes) {
								if(aSaveConcept.equalsIgnoreCase("_MediaOrg")||aSaveConcept.equalsIgnoreCase("B")){
									System.out.println("debug here");
								}
								Annotation anno = new EntityAnnotation(
										this.getAggregatorName()
												+ "_"
												+ (count++),
										aSaveConcept,
										res.get(j)
												.getBeginEnd()[0],
										res.get(i - 1)
												.getBeginEnd()[1],
										this.getAggregatorName(),
										null,
										1.0,
										AnnotationClass.INSTANCE);
								resAnno.add(anno);

							}
							if (!mockStack.get(j).contains("B_")) {
								hash.add(j);
							}
						}
					}
					if (beginSet.isEmpty()) {
						mockStack.add("allWithI");
						hash.add(i);
					}
				} else {// the same as seeing "Other", only "B_" terms
					for (int j = 0; j < i; j++) {
						if (!hash.contains(j) && !beginSet.isEmpty()) {
							final String[] oneConcepts = mockStack.get(j)
									.replaceAll("B_", "").split(this.getMyParams().getOpinionSplitter());
							final Set<String> types = cleanConcept(oneConcepts,res.get(j));
							
							
									
									for (final String aSaveConcept : types) {
										if(aSaveConcept.equalsIgnoreCase("_MediaOrg")||aSaveConcept.equalsIgnoreCase("B")){
											System.out.println("debug here");
										}
										Annotation anno = new EntityAnnotation(
												this.getAggregatorName() + "_"
														+ (count++),
												aSaveConcept, res.get(j)
														.getBeginEnd()[0], res
														.get(i - 1)
														.getBeginEnd()[1],
												this.getAggregatorName(), null,
												1.0, AnnotationClass.INSTANCE);
										resAnno.add(anno);
									

								

							}
							hash.add(j);
						}

					}
					// mockStack.add(outcome[i]);
				}
				if (!beginSet.isEmpty()) {
					int countTemp = 0;
					String newOccup = "";
					for (final String oneBegin : beginSet) {
						if (countTemp > 0) {
							newOccup += this.getMyParams().getOpinionSplitter() + oneBegin;
						} else {
							newOccup += oneBegin;
						}
						countTemp++;
					}
					mockStack.add(newOccup);
				}

			}
			i++;

		}
		return resAnno;

	}

	private Set<String> returnSafeConcept( String currentLabel, final Event evidence) {
		currentLabel = currentLabel.replaceAll("B_", "").replaceAll("I_", "").trim();
		Map<String, String> contextPredHash = evidence.getContextPredHash();

		Set<String> res = new HashSet<String>();
		if (!myParams.isReturnSafyConcept()) {
			res.add(currentLabel);
			return res;
		}
		Set<String> mySupers = new HashSet<String>();
		int mySupClassCount = 0, opinionCounter = 0;
		for (String oneAnnotator : this.annotators) {
			String hisOpinion = contextPredHash.get(oneAnnotator);
			if (hisOpinion != null) {
				opinionCounter++;
				final String[] hisOpinions = hisOpinion
						.replaceAll("B_", "").replaceAll("I_", "").trim().split(this.getMyParams().getOpinionSplitter());
				for (final String oneHisOpinion : hisOpinions) {
					Set<String> supers = MaterializedOntologyQuery
							.getAllSupertypes(oneHisOpinion);
					if (supers != null && supers.contains(currentLabel)) {// already
																			// safe;
						res.add(currentLabel);
						return res;
					}
					supers = MaterializedOntologyQuery
							.getAllSupertypes(currentLabel);
					if (supers != null && supers.contains(oneHisOpinion)) {
						mySupClassCount++;
						mySupers = addLeastCommonAnc(mySupers, oneHisOpinion);
					}
				}
			}
		}
		if (mySupClassCount == 0 && opinionCounter >= 1) {
			res.add(currentLabel);
			return res;
		} else {
			return mySupers;
		}
		//return mySupers;
	}

	private Set<String> addLeastCommonAnc(Set<String> mySupers,
			String newOpinion) {
		Set<String> superOfNewOpinion = MaterializedOntologyQuery
				.getAllSupertypes(newOpinion);
		Set<String> tobeDelete = new HashSet<String>();
		for (String oneMember : mySupers) {
			final Set<String> superOfOneMember = MaterializedOntologyQuery
					.getAllSupertypes(oneMember);
			if (superOfOneMember != null
					&& superOfOneMember.contains(newOpinion)) {// newOpinion is
																// redundant
				return mySupers;
			}
			if (superOfNewOpinion != null
					&& superOfNewOpinion.contains(oneMember)) {// oneMember is
																// redundant
				tobeDelete.add(oneMember);
			}
		}
		mySupers.removeAll(tobeDelete);
		mySupers.add(newOpinion);
		return mySupers;
	}

	/**
	 * @return the decoder
	 */
	public Decoder getDecoder() {
		return decoder;
	}

	/**
	 * @param decoder the decoder to set
	 */
	public void setDecoder(Decoder decoder) {
		this.decoder = decoder;
	}

	/**
	 * @return the modelPool
	 */
	public ModelPool getModelPool() {
		return modelPool;
	}

	/**
	 * @param modelPool the modelPool to set
	 */
	public void setModelPool(ModelPool modelPool) {
		this.modelPool = modelPool;
	}


	/**
	 * @return the annotators
	 */
	public String[] getAnnotators() {
		return annotators;
	}


	/**
	 * @param annotators the annotators to set
	 */
	public void setAnnotators(String[] annotators) {
		this.annotators = annotators;
	}

}
