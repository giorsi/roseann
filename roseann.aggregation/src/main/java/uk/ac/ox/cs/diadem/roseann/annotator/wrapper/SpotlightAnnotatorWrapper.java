/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.annotator.wrapper;

import java.util.HashSet;
import java.util.Set;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.wrapper.AnnotatorDecorator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.spotlight.SpotlightAnnotator;

/**
 * This decorator modifies the entity annotation results harvested from
 * DBpediaSpotlight annotator, which only harvests the deepest annotation concepts
 * according to querying DBpedia Ontology.
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class SpotlightAnnotatorWrapper extends AnnotatorDecorator {
	/**
	 * counter to increment the annotation id
	 */
	private long idCounter;
	/**
	 * Private constructor
	 */
	private SpotlightAnnotatorWrapper() {
		idCounter = 0;
		this.decoratee = SpotlightAnnotator.getInstance();
	}

	/* (non-Javadoc)
	 * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter#annotateEntity(java.lang.String)
	 */
	@Override
	public Set<Annotation> annotateEntity(String text,int timeout) {
		final String response = submitToAnnotator(text,timeout);
		if(response == null)
			return new HashSet<Annotation>();
		if(decoratee instanceof SpotlightAnnotator){
			// save the annotations and increment the id counter
			final Set<Annotation> entityAnnotations = ((SpotlightAnnotator) decoratee).retrieveEntityAnnotations(response, idCounter, true);
			final int numAnnotations=entityAnnotations.size();
			idCounter += numAnnotations;
			return entityAnnotations;
		}
		return null;
	}
	
	/**
	 * singleton instance
	 */
	private static SpotlightAnnotatorWrapper instance;
	/**
	 * static method to get the singleton instance
	 * @return the singleton instance
	 */
	public static synchronized Annotator getInstance() {
		if (instance == null) {
			instance = new SpotlightAnnotatorWrapper();
		}
		return instance;
	}
}
