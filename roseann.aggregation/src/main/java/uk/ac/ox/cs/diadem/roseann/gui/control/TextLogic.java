/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.control;

import java.awt.event.MouseEvent;
import java.io.*;
import java.util.HashSet;
import java.util.Set;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;







import uk.ac.ox.cs.diadem.roseann.ROSeAnn;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.AnnotatedDocumentModel;
import uk.ac.ox.cs.diadem.roseann.gui.control.AbstractDocumentViewerBusiness.MODEL_TYPE;
import uk.ac.ox.cs.diadem.roseann.gui.forms.*;
import uk.ac.ox.cs.diadem.roseann.gui.swing.util.Console;
import uk.ac.ox.cs.diadem.roseann.gui.toolkit.*;
import uk.ac.ox.cs.diadem.roseann.gui.toolkit.xmlViewer.*;
import uk.ac.ox.cs.diadem.roseann.util.Constant;
import uk.ac.ox.cs.diadem.roseann.util.FileAdapter;
import uk.ac.ox.cs.diadem.roseann.util.Constant.AnnotatorNameAgg;
import uk.ac.ox.cs.diadem.roseann.util.Constant.AnnotatorNameInd;


public class TextLogic {
	

	// Utility method to open an existing text from file system
	public static void openTextFile(final MainPageNew entryPage) {


		// Create a file chooser
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(entryPage);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			Console.println("Opening file: "+file.getName()+" ...");
			DocumentOpenPnl docPnl = new DocumentOpenPnl();
			String content = FileAdapter.getPureFileContents(file
					.getAbsolutePath());
			if (content != null) {
				docPnl.docTxtpane.setText(content);
				entryPage.documentTabPane
				.addTab(file.getName(),
						new ImageIcon(MainPageNew.class
								.getResource("icons/resource_obj.gif")),
								docPnl);

				entryPage.documentTabPane
				.setSelectedIndex(entryPage.documentTabPane
						.getTabCount() - 1);

			}

		}

	}
	
	// Utility method to open the file of configuration
		public static void openConfigFile(final MainPageNew entryPage, File file) {

			
			Console.println("Opening the configuration file...");
			
			if(!file.exists()){
				Console.println("The configuration file does not exist:{} "+file.getAbsolutePath());
				return;
			}
			


			final String content =FileAdapter.getPureFileContents(file.getAbsolutePath());
			DocumentOpenPnl docPnl = new DocumentOpenPnl(new XmlTextPane());
			docPnl.docTxtpane.setText(content);
			entryPage.documentTabPane.addTab("Configuration", new ImageIcon(
					MainPageNew.class.getResource("icons/debugt_obj.gif")),
					docPnl);

			entryPage.documentTabPane.setSelectedIndex(entryPage.documentTabPane
					.getTabCount() - 1);

		}

	// Utility method to open the file of merged ontology
	public static void openOntologyFile(final MainPageNew entryPage) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		Console.println("Opening file of merged ontology...");

		if(MainpageBusiness.class.
				getResourceAsStream("annotation_types_mapping.owl")==null){
			Console.println("Sorry, the ontology file does not exist...");
			return;
		}
		try{

			br = new BufferedReader(new InputStreamReader(MainpageBusiness.class.
					getResourceAsStream("annotation_types_mapping.owl")));
			while((line = br.readLine())!=null)
				sb.append(line+"\n");
		}
		catch(IOException e ){
			Console.println("Error while opening the ntology file: "+e.getMessage());
			return;
		}
		finally{
			if(br!=null){
				try{
					br.close();
				}
				catch(IOException e1){
					Console.println("Error while opening the ntology file: "+e1.getMessage());
					return;
				}
			}

		}


		final String content = sb.toString();
		DocumentOpenPnl docPnl = new DocumentOpenPnl(new XmlTextPane());
		docPnl.docTxtpane.setText(content);
		entryPage.documentTabPane.addTab("MergedOntology", new ImageIcon(
				MainPageNew.class.getResource("icons/samples.gif")),
				docPnl);

		entryPage.documentTabPane.setSelectedIndex(entryPage.documentTabPane
				.getTabCount() - 1);

	}

	// Utility method to open a new text to edit
	public static void newTextFile(final MainPageNew entryPage) {
		Console.println("Opening a new text file...");

		DocumentOpenPnl docPnl = new DocumentOpenPnl();
		entryPage.documentTabPane.addTab("New text", new ImageIcon(
				MainPageNew.class.getResource("icons/resource_obj.gif")),
				//MainPageNew.class.getResource("icons/big-flower.gif")),
				docPnl);

		entryPage.documentTabPane.setSelectedIndex(entryPage.documentTabPane
				.getTabCount() - 1);
	}

	public static void visualizeStaticAnnotatedText(final MainPageNew entryPage,MouseEvent e){

		TreePath tp = entryPage.corpusTree.getPathForLocation(
				e.getX(), e.getY());
		if (tp != null) {
			entryPage.corpusTree.setSelectionPath(tp);

			DefaultMutableTreeNode node = (DefaultMutableTreeNode) tp
					.getLastPathComponent();

			if (node.isLeaf()) {
				Object[] path = node.getUserObjectPath();
				String selectedFileName = "";

				for (int i = 1; i < path.length; i++) {
					selectedFileName += "/" + path[i].toString();
				}
				Console.println("Opening annotated text file:"+ selectedFileName
						+ "...");

				// init a documentViewPanel to display the annotated
				// doc selected by user
				DocumentViewerPnl newPanel = new DocumentViewerPnl();

				entryPage.documentTabPane
				.addTab(selectedFileName,
						new ImageIcon(
								MainPageNew.class
								.getResource("icons/sourceEditor.gif")),
								newPanel);
				entryPage.documentTabPane
				.setSelectedIndex(entryPage.documentTabPane
						.getTabCount() - 1);

				AnnotatedDocumentModel annoDoc = loadStaticDocument(
						MainpageBusiness.class.getResource(Constant.STATIC_CORPUS_ROOT).getPath(),
								selectedFileName);
				// bind the business instance with the source text
				// and GUI form
				newPanel.setAnnoDocModel(annoDoc);
				BusinessInterface docViewBus = new DocumentViewerBusiness(
						newPanel, MODEL_TYPE.TextModel);

				docViewBus.initialize();

			}

		}
	}

	public static void annotateText(MainPageNew entryPage){
		Console.println("Annotating text document...");
		int selectedTabIdx = entryPage.documentTabPane.getSelectedIndex();
		final ROSeAnn roseannIns = MainpageBusiness.getRoseAnnInstance();
		if(selectedTabIdx>=0){
			JComponent jp = (JComponent) entryPage.documentTabPane.getComponentAt(selectedTabIdx);
			if(jp instanceof DocumentOpenPnl){
				final String textToAnnotate = ((DocumentOpenPnl)jp).docTxtpane.getText();
				// annotate the text
				final AnnotatedDocumentModel annDocModel = roseannIns.annotateEntityPlainText(textToAnnotate, true);
						
				// init a documentViewPanel to display the annotated
				// doc selected by user
				DocumentViewerPnl newPanel = new DocumentViewerPnl();

				entryPage.documentTabPane
				.addTab(entryPage.documentTabPane.getTitleAt(selectedTabIdx),
						new ImageIcon(
								MainPageNew.class
								.getResource("icons/sourceEditor.gif")),
								newPanel);
				entryPage.documentTabPane
				.setSelectedIndex(entryPage.documentTabPane
						.getTabCount() - 1);


				// bind the business instance with the source text
				// and GUI form
				newPanel.setAnnoDocModel(annDocModel);
				BusinessInterface docViewBus = new DocumentViewerBusiness(
						newPanel, MODEL_TYPE.TextModel);

				docViewBus.initialize();
				return;
			}
		}
	}
	/**
	 * To load static documents and create the annotated model
	 * @param pathName
	 * @param selectedFileName
	 */
	public static AnnotatedDocumentModel loadStaticDocument(final String pathName, final String selectedFileName) {
		final File selectedDoc = new File(pathName + selectedFileName);
		final Set<String> annotatorSet = new HashSet<String>();
		// add all the individual annotators name (predefined)
		for (final AnnotatorNameInd oneValue : AnnotatorNameInd.values()) {
			annotatorSet.add(oneValue.toString());
		}
		// add all the aggregators name
		for (final AnnotatorNameAgg oneValue : AnnotatorNameAgg.values()) {
			annotatorSet.add(oneValue.toString());
		}
		final AnnotatedDocumentModel model = new AnnotatedDocumentModel();
		// harvest annotations from all the relevant annotated docs
		for (final String oneAnnotator : annotatorSet) {
			final String annotatedDocUrl =MainpageBusiness.class.getResource(Constant.STATIC_ANNO_CORPUS_ROOT).getPath() + "/"
					+ selectedFileName.replace(".txt", "") + "_" + oneAnnotator + ".xml";
			if (annotatedDocUrl != null) {// only care when the annotated doc of
				// the annotator exists.
				final File selectAnnoDoc = new File(annotatedDocUrl);
				if (selectAnnoDoc.exists()) {
					final int count = AnnotatedDocXMLAdatpter.harvestAnnotationXML(model, selectAnnoDoc, oneAnnotator);
					if(count>0){//only annotators/aggregators which return annotations are recorded as completed annotators/aggregators
						
						if(AnnotatorNameInd.isValidValue(oneAnnotator) ){
							Set<String> annotators = model.getCompletedAnnotators();
							if(annotators==null){
								annotators = new HashSet<String>();
								model.setCompletedAnnotators(annotators);
							}
							annotators.add(oneAnnotator);
						}else if(AnnotatorNameAgg.isValidValue(oneAnnotator)){
							Set<String> annotators = model.getCompletedAggregators();
							if(annotators==null){
								annotators = new HashSet<String>();
								model.setCompletedAggregators(annotators);
							}
							annotators.add(oneAnnotator);
						}
					}
					
				
				}

			}

		}
		// calculate the conflict model
		final Set<Conflict> conflicts = MainpageBusiness.getRoseAnnInstance().calculateConflicts(model.getKnownAnnotations(), model.getCompletedAnnotators());
		final String text = FileAdapter.getPureFileContents(selectedDoc.getAbsolutePath());
		model.setAnnotatedText(text);
		model.setAllConflicts(conflicts);
		return model;

	}
	

}
