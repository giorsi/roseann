/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann;

import java.io.File;
import java.net.URI;
import java.util.List;
import java.util.Set;

import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.lang3.tuple.Pair;


import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.AnnotationAttribute;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Glyph;
import uk.ac.ox.cs.diadem.pdfanalyser.model.Line;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.OmissionConflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.LogicalConflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict.ConflictType;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.AnnotatedDocumentModel;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.AnnotatedPDFModel;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.PdfDoc;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web.AnnotatedHTMLModel;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web.AnnotationDomRange;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web.HtmlDoc;
import uk.ac.ox.cs.diadem.webapi.dom.DOMDocument;
import uk.ac.ox.cs.diadem.webapi.dom.DOMElement;

/**
 * The class shows an example of programmatically using the ROSeAnn api.
 * In order to correctly use the api, two configuration files must be specified:
 * TextannotatorConfiguration.xml, contaning the information about the individual annotator
 * Configuration.xml, containing the ROSeAnn-specific parameters
 * Both the configuration files must be specified in the conf folder
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk) Department of Computer Science, University of Oxford
 *
 */
public class ROSeAnnClient {

	@SuppressWarnings("unused")
	public static void invokeRoseAnn() throws Exception{


		//instantiate ROSeAnn, that will read all the parameters from the (both) configuration files (default ones)
		//the default files are in conf folder
		ROSeAnn roseann=new ROSeAnn();

		//instantiate roseann with user-created-file
		XMLConfiguration roseannConfig= new XMLConfiguration(new File("path_to_file"));
		String textAnnotatorConfPath = "";
		roseann = new ROSeAnn(roseannConfig,textAnnotatorConfPath);

		//set the timeout parameter
		roseann.setTimeout(180000);

		//create a materialized ontology folder that could be given to roseann
		//two parameters must be specified:
		//the output folder where the ontology will be created
		File folder = new File("path_folder");
		//the ontology file, can be either local or remote
		URI ontologyFile=new URI("http://your_ontology.owl");
		//create the ontology file, an exception is thrown if the are problems (like the file does not exist)
		roseann.createMaterializedOntologyFolder(folder, ontologyFile);

		//set the folder of the materialized ontology. If it's not set, roseann will use the default ontology
		roseann.setMaterializedOntologyFolder(
				new File("src/main/resources/uk/ac/ox/cs/diadem/roseann/ontology/materializedOntology"));

		String text="Barack Obama is the president of United States.";
		//annotate the text without reconciling it, second parameter
		AnnotatedDocumentModel model = roseann.annotateEntityPlainText(text, false);
		//annotate the text and reconcile it
		model = roseann.annotateEntityPlainText(text, true);

		//the information about the annotations found can be retrieved from the AnnotatedDocumentModel
		//returned by roseann

		//get the annotated text
		String annotatedText = model.getAnnotatedText();

		//get all the annotations found
		Set<Annotation> entityAnnotations = model.getAllAnnotations();

		//print all the information for each annotation
		System.out.println("--------------------ANNOTATIONS------------------------");
		for(Annotation annotation:entityAnnotations){
			System.out.print("Id:"+annotation.getId()+"\t");
			System.out.print("Concept:"+annotation.getConcept()+"\t");
			System.out.print("Start_Offset:"+annotation.getStart()+"\t");
			System.out.print("End_Offset:"+annotation.getEnd()+"\t");
			System.out.print("Annotator:"+annotation.getOriginAnnotator()+"\t");
			System.out.print("Confidence:"+annotation.getConfidence()+"\t");
			System.out.print("Source:"+annotation.getAnnotationSource()+"\t");
			System.out.print("Class:"+annotation.getAnnotationClass()+"\t");
			System.out.println("Type:"+annotation.getType());
			System.out.println("--ATTRIBUTE--");

			//print also the attributes
			Set<AnnotationAttribute> attributes = annotation.getAttributes();
			for(AnnotationAttribute attribute:attributes){
				System.out.println("Name:"+attribute.getName()+",Value:"+attribute.getValue());
			}
			System.out.println("*******************************************");

		}

		//get all the annotations from one specific annotator
		Set<Annotation> calaisAnnotation = model.getAnnotationByAnnotator("openCalais");

		//get all the annotations with a specific concept
		Set<Annotation> personAnnotation = model.getAnnotationByConcept("Person");

		//get all the annotations that are included in the range [10,20]
		Set<Annotation> annotationRange = model.getAnnotationByIncludingStartEnd(10, 20);

		//get all the annotations with start=10 and end=20
		Set<Annotation> startEndAnnotaiton = model.getAnnotationByStartEnd(10, 20);

		//get only those annotations with a concept known in the ontology
		Set<Annotation> knownAnnotation = model.getKnownAnnotations();

		//get only those annotations with a concept unknown in the ontology
		Set<Annotation> unknownAnnotation = model.getUnknownAnnotation();

		//get the annotators which completed the annotation process succesfully
		Set<String> completedAnnotators = model.getCompletedAnnotators();

		//get the aggregators which completed the reconciliation process succesfully
		Set<String> completedAggregators = model.getCompletedAggregators();


		//get all the conflicts found by ROSeAnn
		Set<Conflict> conflicts = model.getAllConflicts();

		//print all the information about the conflicts
		System.out.println("--------------------CONFLICTS------------------------");
		for(Conflict conflict:conflicts){

			System.out.print("Id:"+conflict.getId()+"\t");
			System.out.print("Type:"+conflict.getType()+"\t");
			System.out.print("Start_Offset:"+conflict.getStart()+"\t");
			System.out.print("End_Offset:"+conflict.getEnd()+"\t");

			//get the annotations involved in the conflict
			Set<Annotation> involvedAnnotation = conflict.getInvolvedAnnotations();
		}

		//get all the omission conflict
		Set<OmissionConflict> omission = model.getOmissionConflicts();
		//with a omission conflict, besides the functionalities of a normal conflict, we can also do more complicated things 
		OmissionConflict oneConflict = omission.iterator().next();
		//get the involved concept
		String concept = oneConflict.getConcept();
		//get the agreed annotators
		Set<String> agreedAnnotators = oneConflict.getAgreedAnnotators();
		//get the omitted annotators
		Set<String> omittedAnnotators = oneConflict.getOmittedAnnotators();

		//get all the logical conflicts
		Set<LogicalConflict> logical = model.getLogicalConflicts();

		//get the conflict with start=10 and end=20
		Set<Conflict> startEndConflict = model.getConflictByStartEnd(10, 20);

		//get all the conflict with start=10 and end=20 and type=omission
		Set<Conflict> startEndTypeConflict = model.getConflictByStartEndType(10, 20, ConflictType.OMISSION);

		//get the number of omission conflict with start=10 and end=20
		int omissionStartEndConflict = model.getOmissionConflictByStartEnd(10, 20);

		//get the number of logical conflict with start=10 and end=20
		int logicalStartEndConflict = model.getLogicalConflictByStartEnd(10, 20);

		//save the document, along with the annotations and conflicts found, in a xml format
		model.save(new File("path_to_file"));
		
		//save the document according to the gate format, only the annotations will be saved (not conflicts)
		model.saveGateFormat(new File("path_to_file"));

		//annotate and disambiguate a web page, keep the browser window alive (to use more sophisticated methods)
		URI webUrl = new URI("http://google.co.uk");
		AnnotatedHTMLModel htmlModel = roseann.annotateEntityWebPage(webUrl, true,true);

		//The object AnnotatedHTMLModel is a subclass of AnnotatedDocumentModel and offers other functionalities
		//web specific

		//get the html doc
		HtmlDoc htmlDoc = htmlModel.getHtmlDoc();
		//get the textual content of the web page
		String textualContent = htmlDoc.getTextualContent();
		//get the DOM representation of the web page. 
		//The DOMDocument returned is compatible with W3C standard. This will return null if the browser window 
		//has been shut down
		DOMDocument domDocument = htmlDoc.getDOMDocument();
		
		//get the annotation contained in a given dom node. This will return null if the browser window
		//has been shut down
		DOMElement node = domDocument.getDocumentElement();
		Set<Annotation> nodeIncludedAnnotation = htmlModel.getAnnotationNodeContained(node);
		
		//get the annotation contained in a node with a give xpath. This will return null if the browser window
		//has been shut down
		Set<Annotation> xpathIncludedAnnotation = htmlModel.getAnnotationXpathContained("your_xpath_expression");
		//!!!WARNING: the two methods above have not been optimized and for very large page they might take up
		//to 1 minute to complete the process
		
		//for a given annotation, get the AnnotationDomRange
		Annotation annotation = htmlModel.getAllAnnotations().iterator().next();
		AnnotationDomRange range = htmlModel.getAnnotationDomRange(annotation);

		//an annotation dom range represents the starting and ending node of the annotation
		//get the xpath of the starting node
		String startXpath = range.getXpathStart();
		//get the offset of the beginning of the annotation within the starting node
		int offsetStart = range.getStartOffset();
		//get the xpath of the ending node
		String endXpath = range.getXpathEnd();
		//get the offset of the end of the annotation within the ending node
		int offsetEnd = range.getEndOffset();

		//annotate and disambiguate a pdf file
		File pdfFile = new File("path_to_your_pdf");
		AnnotatedPDFModel pdfModel = roseann.annotateEntityPDF(pdfFile, true);

		//The object AnnotatedPDFModel is a subclass of AnnotatedDocumentModel and offers other functionalities
		//pdf specific
		
		//get the pdf doc, a pdf doc is an object made by glyphs, lines, blocks and block groups
		PdfDoc pdfDoc = pdfModel.getPdfDoc();
		//get all the lines of the pdf
		List<Line> lines = pdfDoc.getLines();
		//for a given line, get the bounding coordinates of the line
		Line oneLine = lines.iterator().next();
		oneLine.getBottom();
		oneLine.getTop();
		oneLine.getLeft();
		oneLine.getRight();
		
		//get the original pdf file
		File originalPdfFile = pdfDoc.getPdfFile();
		
		//for a given annotation, get the glyphs where the annotation start
		//and where it ends as a pair of glyphs
		Pair<Glyph,Glyph> startEndAnnotationGlyphs = 
				pdfModel.getBoundingGlyphs(annotation);
		
		//get the annotation having coordinates 10,20,30,40
		Set<Annotation> coordinatesAnnotations = pdfModel.getAnnotationByBoundingBox(10, 20, 30, 40);
		
		//get the annotations that are contained within given coordinates
		Set<Annotation> containedAnnotations = pdfModel.getCotainedAnnotation(10, 20, 30, 40);
		
		//eventually, use roseann GUI to visualize the annotations over the document
		ROSeAnn.visualizeAnnotatedDocument(model);
		ROSeAnn.visualizeAnnotatedDocument(htmlModel);
		ROSeAnn.visualizeAnnotatedDocument(pdfModel);
		
		
	}

}
