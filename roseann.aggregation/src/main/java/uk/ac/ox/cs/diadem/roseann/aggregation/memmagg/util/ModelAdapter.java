/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.util;

import java.io.*;
import java.util.*;

import opennlp.tools.tokenize.*;
import opennlp.tools.util.*;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.Memm;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.dataModel.Event;
import uk.ac.ox.cs.diadem.roseann.util.*;

/**
 * An adapter transform document+annotationSet into chopped events about tokens
 * 
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) - Department
 *         of Computer Science - University of Oxford
 */
public class ModelAdapter {

	private static Tokenizer tokenizer = SimpleTokenizer.INSTANCE;

	/**
	 * A method to convert an annotated document into a sequence of event, token
	 * by token.
	 * 
	 * @param annotSet
	 *            The set of annotations
	 * @param text
	 *            The text to annotate
	 * @param forTrain
	 *            Boolean flag indicating that whether it is in training or
	 *            prediction scenario
	 * @param memm
	 *            The MEMM instance which invoke this adapter
	 * @return List<Event> A list of token events
	 */
	public static List<Event> convertToEventStream(Set<Annotation> annotSet,
			String text, boolean forTrain, Memm memm) {
		List<Event> res = new ArrayList<Event>();
		List<Event> raw = new ArrayList<Event>();
		// chop the sentence into tokens
		Span[] spans = chopIntoTokenSpans(text);
		// append the token sequence
		for (int i = 0; i < spans.length; i++) {
			Event oneEvent = new Event();
			String token = spans[i].getCoveredText(text).toString();

			oneEvent.setToken(token);
			raw.add(i, oneEvent);
			int[] beginEnd = new int[2];
			beginEnd[0] = spans[i].getStart();
			beginEnd[1] = spans[i].getEnd();
			oneEvent.setBeginEnd(beginEnd);

		}
		// handle the annotations
		for (Annotation oneAtom : annotSet) {
			boolean ok = fillInEvent(raw, spans, oneAtom, memm);
			if (!ok) {
				System.err
						.println("Can not find the token span of the annotations!!-> "
								+ oneAtom + " " + oneAtom.getOriginAnnotator());
			}
		}
		Event oneEvent = null;
		for (int i=0;i<raw.size();i++) {
			Event oneRawEvent = raw.get(i);
			// System.out.println(oneRawEvent);
			if(oneRawEvent.getToken().equalsIgnoreCase("The")&& i+1<raw.size()){
				Event nextRawEvent = raw.get(i+1);
				Map<String, String> context = nextRawEvent.getContextPredHash();
				for(String key:context.keySet()){
					context.put(key, context.get(key).replaceAll("I_", "B_"));
				}
				oneRawEvent.getContextPredHash().clear();
			}
			oneEvent = memm.getFeatureExtractor().extractFeature(oneRawEvent, forTrain);
			oneEvent.setBeginEnd(oneRawEvent.getBeginEnd());
			res.add(oneEvent);
			oneEvent.setBeginOfSentence(Event.isBeginOfCC(oneRawEvent));
			// oneRawEvent.cleanup();
			oneRawEvent = null;
		}
		return res;
	}

	
	/**
	 * @param res
	 * @param spans
	 * @param myNEAnnotation
	 * @param memm
	 * @return
	 */
	private static boolean fillInEvent(List<Event> res, Span[] spans,
			Annotation myNEAnnotation, Memm memm) {
		String annotator = myNEAnnotation.getOriginAnnotator();
		String label = myNEAnnotation.getConcept();

		int[] beginEnd = new int[2];
		beginEnd[0] = (int) myNEAnnotation.getStart();
		beginEnd[1] = (int) myNEAnnotation.getEnd();
		int[] begeinEndToken = getTokenSpan(beginEnd, spans);
		if (begeinEndToken[0] < 0 || begeinEndToken[1] < 0) {
			return false;
		}
		Event targetEvent = null;
		final String splitter = memm.getMyParams().getOpinionSplitter();
		// from token i to token j, fill in event content
		for (int k = begeinEndToken[0]; k <= begeinEndToken[1]; k++) {

			targetEvent = res.get(k);
			// for outcome
			// if (annotator.equalsIgnoreCase(goldStandardAnnotator)&&
			// (label.equalsIgnoreCase("ORGANIZATION")||label.equalsIgnoreCase("Person")||label.equalsIgnoreCase("LOCATION")||label.equalsIgnoreCase("Money")))
			// {//for muc
			if (annotator
					.equalsIgnoreCase(Constant.AnnotatorNameAgg.gold_standard
							.toString())) {

				if (k == begeinEndToken[0]) {
					if (targetEvent.getOutcome().equalsIgnoreCase("Other")) {
						targetEvent.setOutcome("B_" + label);
					} else {
						targetEvent.setOutcome(targetEvent.getOutcome()
								+ splitter + "B_" + label);
					}

				} else {
					if (targetEvent.getOutcome().equalsIgnoreCase("Other")) {
						targetEvent.setOutcome("I_" + label);
					} else {
						targetEvent.setOutcome(targetEvent.getOutcome()
								+ splitter + "I_" + label);
					}
					// not allow I_xxx being assigned in the beginning of
					// sentence
					if (targetEvent.getContextPredHash().get("beginOfSentence") != null) {
						targetEvent.getContextPredHash().remove(
								"beginOfSentence");
					}
				}
			} else {// for evidence vector
				Map<String, String> contextPred = targetEvent
						.getContextPredHash();
				Object obj = contextPred.get(annotator);
				if (k == begeinEndToken[0]) {

					if (obj != null) {
						if (!obj.toString().contains("B_" + label)) {
							contextPred.put(
									annotator,
									Event.concatSortedStr(obj.toString()
											+ splitter + "B_"
											+ label, memm.getMyParams().getOpinionSplitter()));
						}

					} else {
						contextPred.put(annotator, "B_" + label);
					}

				} else {

					if (obj != null) {
						if (!obj.toString().contains("I_" + label)) {
							contextPred.put(
									annotator,
									Event.concatSortedStr(obj.toString()
											+ splitter + "I_"
											+ label, memm.getMyParams().getOpinionSplitter()));
						}

					} else {
						contextPred.put(annotator, "I_" + label);
					}
				}

			}

		}

		return true;
	}

	/**
	 * @param
	 * @return
	 */
	private static int[] getTokenSpan(int[] beginEnd, Span[] spans) {
		int[] beginEndToken = { -1, -1 };
		int start = -1, end = -1;
		int i = 0, j = 0;
		// locate beginning index
		for (i = 0; i < spans.length; i++) {
			start = spans[i].getStart();
			if (start >= beginEnd[0] && start <= beginEnd[1]) {
				beginEndToken[0] = i;
				break;
			}
		}
		if (beginEndToken[0] < 0) {
			for (i = spans.length - 1; i >= 0; i--) {
				start = spans[i].getStart();
				if (start < beginEnd[0]) {
					beginEndToken[0] = i;
					break;
				}
			}
		}
		if (beginEndToken[0] < 0) {
			return beginEndToken;
		}
		// locate ending index
		for (j = beginEndToken[0]; j < spans.length; j++) {
			end = spans[j].getEnd();
			if (end >= beginEnd[1]) {
				beginEndToken[1] = j;
				break;
			}
		}

		return beginEndToken;
	}

	/**
	 * @param
	 * @return
	 */
	private static Span[] chopIntoTokenSpans(String content) {

		Span[] tokenSpans = tokenizer.tokenizePos(content);

		return tokenSpans;

	}

	public static void convertToEventStreamForDoc(File doc,
			Set<Annotation> annotations, File output,
			Memm memm) throws Exception {
		String content = FileAdapter.getPureFileContents(doc.getAbsolutePath());
		// Span[] sentSpans = chopIntoSentence(content);
		final String spliter = memm.getMyParams().getFeatureSplitter();
		Span[] spans = chopIntoTokenSpans(content);
		List<Event> myRes = chopIntoEventStreamForDoc(content, annotations,
				spans, memm);
		StringBuilder sb = new StringBuilder();
		final FeatureExtractor featureExtractor = memm.getFeatureExtractor();
		for (Event oneEvent : myRes) {
			Event qe = featureExtractor.extractFeature(oneEvent, true);
			// ((TrustOpenNLPFeatureExtractorImpl)absFeatExt).recordIntrestingEvidence(oneEvent,out,doc);
			sb.append(oneEvent.getToken() + spliter + oneEvent.getBeginEnd()[0]
					+ spliter + oneEvent.getBeginEnd()[1]);
			// String beginOfSentence = oneEvent.isBeginOfSentence();

			sb.append(oneEvent.isBeginOfSentence() ? spliter + "true" : spliter
					+ "false");
			for (String oneFeature : qe.getContextPred()) {
				sb.append(spliter + oneFeature);
			}
			// sb.append(spliter+oneEvent.getOutcome()+"\n");
			sb.append(spliter + qe.getOutcome() + Constant.LINESEPARATOR);// for
																			// trust
																			// who-based
																			// method
		}
		FileAdapter.writeToFile(output, sb.toString());
	}

	private static List<Event> chopIntoEventStreamForDoc(String content,
			Set<Annotation> annotations, Span[] tokenSpans, Memm memm) {
		List<Event> myRes = new ArrayList<Event>();
		// append the token sequence
		for (int i = 0; i < tokenSpans.length; i++) {
			Event oneEvent = new Event();
			String token = tokenSpans[i].getCoveredText(content).toString();

			oneEvent.setToken(token);
			myRes.add(i, oneEvent);
			int[] beginEnd = new int[2];
			beginEnd[0] = tokenSpans[i].getStart();
			beginEnd[1] = tokenSpans[i].getEnd();
			oneEvent.setBeginEnd(beginEnd);

		}
		// handle the annotations
		if (annotations != null) {
			for (Annotation oneAnno : annotations) {

				boolean ok = fillInEvent(myRes, tokenSpans, oneAnno, memm);
				if (!ok) {
					System.err
							.println("Can not find the token span of the annotations!!-> "
									+ oneAnno
									+ " "
									+ oneAnno.getOriginAnnotator());
				}
				// }

			}
		}

		for (Event oneEvent : myRes) {
			oneEvent.setBeginOfSentence(Event.isBeginOfCC(oneEvent));
		}
		return myRes;
	}

}
