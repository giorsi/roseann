/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.toolkit.xmlViewer;


import java.awt.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
 
import javax.swing.text.*;
 
/**
 * Thanks: http://groups.google.com/group/de.comp.lang.java/msg/2bbeb016abad270
 *
 * IMPORTANT NOTE: regex should contain 1 group.
 *
 * Using PlainView here because we don't want line wrapping to occur.
 *
 * @author kees
 * @date 13-jan-2006
 *
 */
public class XmlView extends PlainView {
 
    private static HashMap<Pattern, Color> patternColors;
  //  private static String TAG_PATTERN = "(</?[a-z]*)\\s?>?";
    private static String tagPattern = "(</?\\w+)\\s*>?";
    private static String tagEndPattern = "(/>)";
    private static String tagAttributePattern = "\\s(\\w*)\\=";
    private static String tagAttributeValue = "[a-z-]*\\=(\"[^\"]*\")";
    private static String tagComment = "(<!--.*-->)";
    private static String tagCdataStart = "(\\<!\\[CDATA\\[).*";
    private static String tagCdataEnd = ".*(]]>)";
 
    static {
        // NOTE: the order is important!
        patternColors = new HashMap<Pattern, Color>();
        patternColors.put(Pattern.compile(tagCdataStart), new Color(128, 128, 128));
        patternColors.put(Pattern.compile(tagCdataEnd), new Color(128, 128, 128));
        patternColors
                .put(Pattern.compile(tagPattern), new Color(63, 127, 127));
        patternColors.put(Pattern.compile(tagAttributePattern), new Color(
                127, 0, 127));
        patternColors.put(Pattern.compile(tagEndPattern), new Color(63, 127,
                127));
        patternColors.put(Pattern.compile(tagAttributeValue), new Color(42,
                0, 255));
        patternColors.put(Pattern.compile(tagComment), new Color(63, 95, 191));
    }
 
    public XmlView(Element element) {
 
        super(element);
 
        // Set tabsize to 4 (instead of the default 8)
        getDocument().putProperty(PlainDocument.tabSizeAttribute, 4);
    }
 
    @Override
    protected int drawUnselectedText(Graphics graphics, int x, int y, int p0,
            int p1) throws BadLocationException {
 
        Document doc = getDocument();
        String text = doc.getText(p0, p1 - p0);
 
        Segment segment = getLineBuffer();
 
        SortedMap<Integer, Integer> startMap = new TreeMap<Integer, Integer>();
        SortedMap<Integer, Color> colorMap = new TreeMap<Integer, Color>();
 
        // Match all regexes on this snippet, store positions
        for (Map.Entry<Pattern, Color> entry : patternColors.entrySet()) {
 
            Matcher matcher = entry.getKey().matcher(text);
 
            while (matcher.find()) {
                startMap.put(matcher.start(1), matcher.end());
                colorMap.put(matcher.start(1), entry.getValue());
            }
        }
 
        // TODO: check the map for overlapping parts
         
        int i = 0;
 
        // Colour the parts
        for (Map.Entry<Integer, Integer> entry : startMap.entrySet()) {
            int start = entry.getKey();
            int end = entry.getValue();
 
            if (i < start) {
                graphics.setColor(Color.black);
                doc.getText(p0 + i, start - i, segment);
                x = Utilities.drawTabbedText(segment, x, y, graphics, this, i);
            }
 
            graphics.setColor(colorMap.get(start));
            i = end;
            doc.getText(p0 + start, i - start, segment);
            x = Utilities.drawTabbedText(segment, x, y, graphics, this, start);
        }
 
        // Paint possible remaining text black
        if (i < text.length()) {
            graphics.setColor(Color.black);
            doc.getText(p0 + i, text.length() - i, segment);
            x = Utilities.drawTabbedText(segment, x, y, graphics, this, i);
        }
 
        return x;
    }
 
}
