/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.forms;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;


import uk.ac.ox.cs.diadem.roseann.gui.swing.util.SwingUtil;


/**
 * 
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
@SuppressWarnings("serial")
public class MainPageNew extends JPanel {
	public MainPageNew() {
		initComponents();
	}

	private void initComponents() {
		menuBar1 = new JMenuBar();
		menuCorpora = new JMenu();
		menuNewText = new JMenuItem();
		menuLoadText = new JMenuItem();
		menuWeb = new JMenuItem();
		menuAnnotate = new JMenuItem();
		menuOntology = new JMenuItem();
		menuConfig = new JMenu();
		menuGlobalConfig = new JMenuItem();
		menuAggregatorConfig = new JMenuItem();
		menuHelp = new JMenuItem();
		menuAnnotator= new JMenuItem();
		
		annotatorBtn = new JButton();
		mainpageToolbar = new JToolBar();
		corpusOpenBtn = new JButton();
		corpusNewBtn = new JButton();
		browseBtn = new JButton();
		configBtn = new JButton();
		configSubBtn = new JButton();
		legendBtn = new JButton();
		annotateBtn = new JButton();
		cancelBtn = new JButton();
		saveBtn = new JButton();
		saveAllBtn = new JButton();
		ontologyBtn = new JButton();
		ontologySubBtn = new JButton();
		scrollPane8 = new JScrollPane();
		mainSplitPane = new JSplitPane();
		topSplitPane = new JSplitPane();
		splitPane5 = new JSplitPane();
		panel5 = new JPanel();
		scrollPane3 = new JScrollPane();
		corpusTree = new JTree();
		toolBar1 = new JToolBar();
		panel6 = new JPanel();
		scrollPane4 = new JScrollPane();
		webpageTree = new JTree();
		panel7 = new JPanel();
		scrollPane5 = new JScrollPane();
		pdfTree = new JTree();
		splitPane6 = new JSplitPane();
		panel1 = new JPanel();
		documentTabPane = new JTabbedPane();
		consolePnl = new JPanel();

		//======== this ========
		setLayout(new GridBagLayout());
		((GridBagLayout)getLayout()).columnWidths = new int[] {309};
		((GridBagLayout)getLayout()).rowHeights = new int[] {20, 0, 142};
		((GridBagLayout)getLayout()).columnWeights = new double[] {1.0};
		((GridBagLayout)getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0};
		this.setPreferredSize(new Dimension(900,700));

		//======== menuBar1 ========
		{
			menuBar1.setBorderPainted(false);
			menuBar1.setOpaque(false);

			//======== corpusMenu ========
			{
				menuCorpora.setText("Corpora");
				//menuCorpora.setMnemonic('C');
				menuCorpora.validate();
				menuCorpora.setMaximumSize(new Dimension(menuCorpora.getPreferredSize().width,100));
				
				menuLoadText.setText("Open...");
				menuCorpora.add(menuLoadText);
				
				menuNewText.setText("New...");
				menuCorpora.add(menuNewText);
			}
			menuBar1.add(menuCorpora);

			//======== webMenu ========
			{
				menuWeb.setText("Web");
				//menuWeb.setMnemonic('W');
				menuWeb.validate();
				menuWeb.setMaximumSize(new Dimension(menuWeb.getPreferredSize().width,100));
			}
			menuBar1.add(menuWeb);

			//======== menu3 ========
			{
				menuAnnotate.setText("Annotate");
				//menuAnnotators.setMnemonic('n');
				menuAnnotate.validate();
				menuAnnotate.setMaximumSize(new Dimension(menuAnnotate.getPreferredSize().width,100));
			}
			menuBar1.add(menuAnnotate);

			//======== menu1 ========
			{	
				
				menuOntology.setText("Ontology");
				//menuOntology.setMnemonic('O');
				menuOntology.validate();
				menuOntology.setMaximumSize(new Dimension(menuOntology.getPreferredSize().width,100));
				
			}
			menuBar1.add(menuOntology);

			//======== menu2 ========
			{
				menuConfig.setText("Configuration");
				//menuAnnotate.setMnemonic('A');
				menuConfig.validate();
				menuConfig.setMaximumSize(new Dimension(menuConfig.getPreferredSize().width,100));
				
				menuGlobalConfig.setText("Open ROSeAnn config...");
				menuConfig.add(menuGlobalConfig);
				
				menuAggregatorConfig.setText("Open aggregator config...");
				menuConfig.add(menuAggregatorConfig);
			}
			menuBar1.add(menuConfig);
			
			//======== menu5 ========
			{	
				
				menuAnnotator.setText("Annotators");
				//menuOntology.setMnemonic('O');
				menuAnnotator.validate();
				menuAnnotator.setMaximumSize(new Dimension(menuAnnotator.getPreferredSize().width,100));
				
			}
			menuBar1.add(menuAnnotator);

			//======== menu4 ========
			{
				menuHelp.setText("About");
				//menuHelp.setMnemonic('H');
				menuHelp.validate();
				menuHelp.setMaximumSize(new Dimension(menuHelp.getPreferredSize().width,100));
			}
			menuBar1.add(menuHelp);
		}
		add(menuBar1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
			GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));

		//======== mainpageToolbar ========
		{
			mainpageToolbar.setRollover(true);
			mainpageToolbar.setFloatable(false);
			mainpageToolbar.setOpaque(true);
			mainpageToolbar.addSeparator();

			//---- corpusOpenBtn ----
			corpusOpenBtn = SwingUtil.createMyButton(new ImageIcon(getClass().getResource("icons/open-task.gif")), "Open corpus...");
			
			mainpageToolbar.add(corpusOpenBtn);
			//mainpageToolbar.addSeparator();

			//---- corpusNewBtn ----
			corpusNewBtn = SwingUtil.createMyButton(new ImageIcon(getClass().getResource("icons/propertiesedit.gif")), "Open a new text document...");			
			
			mainpageToolbar.add(corpusNewBtn);
			//mainpageToolbar.addSeparator();

			//---- browseBtn ----
			browseBtn = SwingUtil.createMyButton(new ImageIcon(getClass().getResource("icons/newwebprj_wiz.gif")), "Open a new Web document...");			
			
			mainpageToolbar.add(browseBtn);
			
			
			
			//---- configBtn ----
			configBtn = SwingUtil.createMyButton(new ImageIcon(getClass().getResource("icons/debugt_obj.gif")), "Open ROSeAnn configuration file...");			
			
			//configBtn.setPreferredSize(new Dimension(configBtn.getPreferredSize().width+20,configBtn.getPreferredSize().height));
			mainpageToolbar.add(configBtn);

			//---- annotatorBtn ----
			annotatorBtn = SwingUtil.createMyButton(new ImageIcon(getClass().getResource("icons/attribute_obj.gif")), "Show the list of annotators activated...");			
			
			//annotatorBtn.setPreferredSize(new Dimension(annotatorBtn.getPreferredSize().width+20,configBtn.getPreferredSize().height));
			mainpageToolbar.add(annotatorBtn);

			//---- legendBtn ----
			//legendBtn.setIcon(new ImageIcon(getClass().getResource("icons/palette_view.gif")));
			//legendBtn.setFocusPainted(false);
			//legendBtn.setBorderPainted(false);
			//legendBtn.setContentAreaFilled(false);
			//mainpageToolbar.add(legendBtn);
			
			//---- annotateBtn ----
			annotateBtn= SwingUtil.createMyButton(new ImageIcon(getClass().getResource("icons/run_exc.gif")), "Run annotating...");			
			
			mainpageToolbar.add(annotateBtn);

			//---- cancelBtn ----
			cancelBtn.setIcon(new ImageIcon(getClass().getResource("icons/close_16.gif")));
			
			cancelBtn.setVisible(false);
			mainpageToolbar.add(cancelBtn);
			
			//---- saveBtn ----
			saveBtn= SwingUtil.createMyButton(new ImageIcon(getClass().getResource("icons/save_edit.gif")), "Save as...");			
			
			mainpageToolbar.add(saveBtn);

			//---- saveAllBtn ----
			saveAllBtn= SwingUtil.createMyButton(new ImageIcon(getClass().getResource("icons/saveall_edit.gif")), "Save all as...");			
			
			mainpageToolbar.add(saveAllBtn);
			
			//---- ontologyBtn ----
			ontologyBtn= SwingUtil.createMyButton(new ImageIcon(getClass().getResource("icons/samples.gif")), "Open the ontology file...");			
			
			mainpageToolbar.add(ontologyBtn);

			//---- ontologySubBtn ----
			/*ontologySubBtn.setIcon(new ImageIcon(getClass().getResource("icons/handler.gif")));
			ontologySubBtn.setFocusPainted(false);
			ontologySubBtn.setBorderPainted(false);
			ontologySubBtn.setContentAreaFilled(false);
			ontologySubBtn.setVisible(false);
			mainpageToolbar.add(ontologySubBtn);*/
		}
		add(mainpageToolbar, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
			GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));

		//======== scrollPane8 ========
		{
			//scrollPane8.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

			//======== mainSplitPane ========
			{
				mainSplitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
				mainSplitPane.setOneTouchExpandable(true);
				mainSplitPane.setResizeWeight(0.7);

				//======== topSplitPane ========
				{
					topSplitPane.setOneTouchExpandable(true);

					//======== splitPane5 ========
					{
						splitPane5.setOrientation(JSplitPane.VERTICAL_SPLIT);
						splitPane5.setOneTouchExpandable(true);
						splitPane6.setOrientation(JSplitPane.VERTICAL_SPLIT);
						splitPane6.setOneTouchExpandable(true);

						//======== panel5 ========
						{
							panel5.setBorder(new TitledBorder(null, "Text", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Arial", Font.BOLD, 12)));
							panel5.setLayout(new GridBagLayout());
							((GridBagLayout)panel5.getLayout()).columnWidths = new int[] {0, 0};
							((GridBagLayout)panel5.getLayout()).rowHeights = new int[] {0, 0, 0};
							((GridBagLayout)panel5.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
							((GridBagLayout)panel5.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

							//======== scrollPane3 ========
							{

								//---- corpusTree ----
								corpusTree.setFont(new Font("Arial", Font.PLAIN, 12));
								corpusTree.setModel(null);
								corpusTree.setVisibleRowCount(10);
								corpusTree.setShowsRootHandles(true);
								scrollPane3.setViewportView(corpusTree);
							}
							panel5.add(scrollPane3, new GridBagConstraints(0, 0, 1, 1, 0.5, 0.5,
								GridBagConstraints.CENTER, GridBagConstraints.BOTH,
								new Insets(0, 0, 0, 0), 0, 0));

							//======== toolBar1 ========
							{
								toolBar1.setFloatable(false);
							}
							panel5.add(toolBar1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
								GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
								new Insets(0, 0, 0, 0), 0, 0));
						}
						splitPane5.setTopComponent(panel5);

						//======== panel6 ========
						{
							panel6.setBorder(new TitledBorder(null, "Webpage", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Arial", Font.BOLD, 12)));
							panel6.setLayout(new GridBagLayout());
							((GridBagLayout)panel6.getLayout()).columnWidths = new int[] {0, 0};
							((GridBagLayout)panel6.getLayout()).rowHeights = new int[] {0, 0, 0};
							((GridBagLayout)panel6.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
							((GridBagLayout)panel6.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

							//======== scrollPane4 ========
							{

								//---- webpageTree ----
								webpageTree.setFont(new Font("Arial", Font.PLAIN, 12));
								webpageTree.setModel(null);
								webpageTree.setVisibleRowCount(10);
								webpageTree.setShowsRootHandles(true);
								scrollPane4.setViewportView(webpageTree);
							}
							panel6.add(scrollPane4, new GridBagConstraints(0, 0, 1, 1, 0.5, 0.5,
								GridBagConstraints.CENTER, GridBagConstraints.BOTH,
								new Insets(0, 0, 0, 0), 0, 0));
						}
						splitPane5.setBottomComponent(panel6);
					}
					
					//======== panel7 ========
					{
						panel7.setBorder(new TitledBorder(null, "pdf documents", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Arial", Font.BOLD, 12)));
						panel7.setLayout(new GridBagLayout());
						((GridBagLayout)panel7.getLayout()).columnWidths = new int[] {0, 0};
						((GridBagLayout)panel7.getLayout()).rowHeights = new int[] {0, 0, 0};
						((GridBagLayout)panel7.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
						((GridBagLayout)panel7.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};
					
						//======== scrollPane5 ========
						{

							//---- webpageTree ----
							pdfTree.setFont(new Font("Arial", Font.PLAIN, 12));
							pdfTree.setModel(null);
							pdfTree.setVisibleRowCount(5);
							pdfTree.setShowsRootHandles(true);
							scrollPane5.setViewportView(pdfTree);
						}
						panel7.add(scrollPane5, new GridBagConstraints(0, 0, 1, 1, 0.5, 0.5,
							GridBagConstraints.CENTER, GridBagConstraints.BOTH,
							new Insets(0, 0, 0, 0), 0, 0));
					}
					splitPane6.setTopComponent(splitPane5);
					splitPane6.setBottomComponent(panel7);
					
					topSplitPane.setLeftComponent(splitPane6);

					//======== panel1 ========
					{
						panel1.setLayout(new BorderLayout());

						//======== documentTabPane ========
						{
							documentTabPane.setFont(new Font("Arial", Font.PLAIN, 12));
						}
						panel1.add(documentTabPane, BorderLayout.CENTER);
					}
					topSplitPane.setRightComponent(panel1);
				}
				mainSplitPane.setTopComponent(topSplitPane);

				//======== consolePnl ========
				{
					consolePnl.setLayout(new BorderLayout());
				}
				mainSplitPane.setBottomComponent(consolePnl);
			}
			scrollPane8.setViewportView(mainSplitPane);
		}
		add(scrollPane8, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
			GridBagConstraints.CENTER, GridBagConstraints.BOTH,
			new Insets(0, 0, 0, 0), 0, 0));
		}

	private JMenuBar menuBar1;
	private JMenu menuCorpora;
	public JMenuItem menuNewText;
	public JMenuItem menuLoadText;
	public JMenuItem menuWeb;
	public JMenuItem menuAnnotate;
	public JMenuItem menuOntology;
	private JMenu menuConfig;
	public JMenuItem menuGlobalConfig;
	public JMenuItem menuAggregatorConfig;
	public JMenuItem menuHelp;
	public JMenuItem menuAnnotator;
	public JToolBar mainpageToolbar;
	public JButton annotatorBtn;
	public JButton corpusOpenBtn;
	public JButton corpusNewBtn;
	public JButton browseBtn;
	public JButton configBtn;
	public JButton configSubBtn;
	public JButton legendBtn;
	public JButton annotateBtn;
	public JButton cancelBtn;
	public JButton saveBtn;
	public JButton saveAllBtn;
	public JButton ontologyBtn;
	public JButton ontologySubBtn;
	private JScrollPane scrollPane8;
	public JSplitPane mainSplitPane;
	private JSplitPane topSplitPane;
	private JSplitPane splitPane5;
	private JPanel panel5;
	private JScrollPane scrollPane3;
	public JTree corpusTree;
	private JToolBar toolBar1;
	private JPanel panel6;
	private JScrollPane scrollPane4;
	public JTree webpageTree;
	public JSplitPane splitPane6;
	private JPanel panel7;
	public JTree pdfTree;
	private JScrollPane scrollPane5;
	private JPanel panel1;
	public JTabbedPane documentTabPane;
	public JPanel consolePnl;

	}
