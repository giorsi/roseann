/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.training;

import java.io.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import opennlp.maxent.*;
import opennlp.maxent.io.GISModelWriter;
import opennlp.maxent.io.SuffixSensitiveGISModelWriter;
import opennlp.model.*;
import opennlp.perceptron.PerceptronTrainer;
import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.Memm;
import uk.ac.ox.cs.diadem.roseann.api.aggregation.AggregationException;




/**
 * This class is used as a caller to invoke the training task of the opennlp trainer 
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class ModelTrainingInvoker {
	
	public static boolean useSmoothing = false;
	public static double somothingObservation = 0.1;
	
	/**
	 * Logger
	 */
	static final Logger LOGGER = LoggerFactory.getLogger(ModelTrainingInvoker.class);

	
	/**
	 * Invoke the opennlp maxent GIS trainer
	 * @param trainningData the file of trainning data
	 * @param iterationNum the iteration threshold for GIS model (see maxEnt documentation)
	 * @param outputFileName the corresponding file of trained model
	 */
	public static boolean trainModel(final File trainningData, final Memm memm, final String outputFileName) {

		
		boolean real = false;
		String type = "maxent";
		String dataFileName = new String(trainningData.getName());
		File temp = new File(outputFileName+"/model");
		if(!temp.exists()){
			temp.mkdir();
		}
		String modelFileName = outputFileName+"/model/"+dataFileName.substring(0,
				dataFileName.lastIndexOf('.'))
				+ "Model.txt";
		LOGGER.info("MEMM starts to train "+dataFileName.substring(0,dataFileName.lastIndexOf('.')));
		try {
			FileReader datafr = new FileReader(trainningData);
			EventStream es;
			if (!real) {
				es = new BasicEventStream(new PlainTextByLineDataStream(datafr));
			} else {
				es = new RealBasicEventStream(new PlainTextByLineDataStream(
						datafr));
			}
			GIS.SMOOTHING_OBSERVATION = somothingObservation;
			AbstractModel model;
			final int iterationNum = memm.getMyParams().getIterationNum();
			final int cutoff = memm.getMyParams().getCutoff();
			if (type.equals("maxent")) {

				if (!real) {
					//model = GIS.trainModel(es, USE_SMOOTHING);
					model = GIS.trainModel(es, iterationNum ,cutoff);
				} else {
					model = GIS.trainModel(iterationNum,
							new OnePassRealValueDataIndexer(es, cutoff),
							useSmoothing);
				}
			} else if (type.equals("perceptron")) {
				System.err.println("Perceptron training");
				model = new PerceptronTrainer().trainModel(10,
						new OnePassDataIndexer(es, 0), 0);
			} else {
				System.err.println("Unknown model type: " + type);
				model = null;
			}

			File outputFile = new File(modelFileName);
			if(outputFile.exists()){
				outputFile.delete();
	        }
			LOGGER.info("MEMM starts to write model file "+dataFileName.substring(0,dataFileName.lastIndexOf('.')));
			
			GISModelWriter writer = new SuffixSensitiveGISModelWriter(model,
					outputFile);
			writer.persist();
			LOGGER.info("MEMM finishes to write model file "+dataFileName.substring(0,dataFileName.lastIndexOf('.')));
			return true;
		} catch (Exception e) {
			if(e instanceof NullPointerException){
				System.out.print("catch null exception: ");
				return false;
			}else{
				throw new AggregationException("Unable to create model due to exception: \n"+e, LOGGER);
			}
			
		}
		

	}

}
