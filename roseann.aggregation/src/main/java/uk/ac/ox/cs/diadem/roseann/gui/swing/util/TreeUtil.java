/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.swing.util;

import java.util.*;

import javax.swing.tree.*;

import uk.ac.ox.cs.diadem.roseann.gui.swingComponent.tree.CheckBoxTreeNode;
import uk.ac.ox.cs.diadem.roseann.ontology.MaterializedOntologyQuery;
import uk.ac.ox.cs.diadem.roseann.util.graph.DAG;
import uk.ac.ox.cs.diadem.roseann.util.graph.Node;



/**
 * 
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class TreeUtil {


	public static DefaultMutableTreeNode constructTree(Set<String> concepts)
			throws Exception {


		DAG<String, Integer> mergedGraph = new DAG<String, Integer>();

		//create the node for the concepts that are not in the ontology
		final Node<String> unknownConcept=mergedGraph.addNode("UnknownConcepts", 0);
		//construct the graph for the families of giving concepts
		for (String concept : concepts) {
			final DAG<String,Integer> superclassGraph=MaterializedOntologyQuery.getSuperclassesGraph(concept);
			if(superclassGraph!=null){
				mergedGraph.mergeGraph(superclassGraph);
			}

			//if the concept is uknown, add it as uknown
			else{
				final Node<String> nodeConept=mergedGraph.addNode(concept, 0);
				mergedGraph.addEdge(nodeConept, unknownConcept);

			}
		}
		
		
		final Node<String> thing=mergedGraph.getNode(MaterializedOntologyQuery.getTopConcept());
		Node<String> top = thing;
		//add the uknown concepts only if there is at least one uknown concept
		if(mergedGraph.getUndirectedNeighbour(unknownConcept).size()>0){
			top=mergedGraph.addNode("Concepts", 0);
			mergedGraph.addEdge(unknownConcept, top);
			mergedGraph.addEdge(thing, top);
		}

		return createNodeTree(top, mergedGraph);
	}

	public static DefaultMutableTreeNode createNodeTree(final Node<String> conceptNode,final DAG<String,Integer> conceptGraph){

		String label=conceptNode.getLabel();

		final DefaultMutableTreeNode node=new CheckBoxTreeNode(label);
		final TreeSet<Node<String>> descendant=new TreeSet<Node<String>>();
		descendant.addAll(conceptGraph.getUndirectedNeighbour(conceptNode));

		if(descendant==null||descendant.size()==0){
			return node;
		}
		
		//node to put the uknown concepts at the bottom of the tree
		Node<String> unknownConcept=null;
		
		for(Node<String> oneDescendat:descendant){
			if(oneDescendat.getLabel().equals("UnknownConcepts")){
				unknownConcept=oneDescendat;
				continue;
			}
			
			node.add(createNodeTree(oneDescendat, conceptGraph));
			
		}
		
		//insert the uknown concepts at the bottom of the tree
		if(unknownConcept!=null){
			final Node<String> unknown=conceptGraph.getNode("UnknownConcepts");
			node.add(createNodeTree(unknown, conceptGraph));
		}

		return node;
	}

	/**
	 * @param concepts
	 * @return
	 */
	/*public static DefaultMutableTreeNode constructTree(Set<String> concepts){
		DefaultMutableTreeNode root = new CheckBoxTreeNode("Thing");//the root
		for(String concept:concepts){
			//System.out.println(concept);
			try {
				if(!concept.equalsIgnoreCase("Thing"))
				{

					insertNodeToTree(root,concept,false);
				}

			} catch (Exception e) {

				e.printStackTrace();
			}
		}
		return root;
	}

	private static void insertNodeToTree(DefaultMutableTreeNode root, String concept, boolean hasInsertedBefore) throws Exception {
		//boolean inserted = false;

		DefaultMutableTreeNode newNode = new CheckBoxTreeNode(concept);	
		int childIdx = 0;
		while(childIdx<root.getChildCount()){
			TreeNode oneChild = root.getChildAt(childIdx);
			String childConcept = ((DefaultMutableTreeNode)oneChild).getUserObject().toString();
			Set<String> family = OntologyQuery.getAllSupertypes(childConcept);
			//if the node to be inserted is a superclass of the child
			if(family.contains(concept)){
				//insert it between root and the child
				 root.remove(childIdx);			
				 newNode.add((DefaultMutableTreeNode)oneChild);
				 continue;

			}else{
				family = OntologyQuery.getImplicants(childConcept); 
				//if the new node is a subclass of the child
				if(family.contains(concept)){
					//navigate the child node to find a suitable place to insert it
					insertNodeToTree((DefaultMutableTreeNode)oneChild,concept,false);
					hasInsertedBefore = true;
				}

			}
			childIdx++;
		}
		//if can not insert as ancestor or descendant of at least one child of root
		if(!hasInsertedBefore){
			//insert as a sibling (new child of root)
			//DefaultMutableTreeNode newNode = new CheckBoxTreeNode(concept);
			root.add(newNode);
		}

	}

	public static void getAllDescConcepts(DefaultMutableTreeNode node,
			Set<String> targets) {
		 for(int i=0;i<node.getChildCount();i++){
         	TreeNode oneChild = node.getChildAt(i);
 			String childConcept = ((DefaultMutableTreeNode)oneChild).getUserObject().toString();
 			targets.add(childConcept);
 			getAllDescConcepts((DefaultMutableTreeNode)oneChild,targets);
         }

	}*/



}
