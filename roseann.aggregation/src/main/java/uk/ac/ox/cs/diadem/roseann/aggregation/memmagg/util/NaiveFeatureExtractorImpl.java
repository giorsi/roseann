/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.util;

import java.util.*;

import uk.ac.ox.cs.diadem.roseann.aggregation.memmagg.dataModel.Event;





/**
 * A concrete featureExtractor used by default which takes the individual opinion 
 * and pairwise opinions of annotators.
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
public class NaiveFeatureExtractorImpl implements FeatureExtractor {
	
	/**
	 * @param annotatorNames the annotatorNames to set
	 */
	public void setAnnotatorNames(String[] annotatorNames) {
		this.annotatorNames = annotatorNames;
	}

	private String[] annotatorNames;
	public NaiveFeatureExtractorImpl(String[] annotators){
		annotatorNames= annotators;
	}
	/* (non-Javadoc)
	 * @see uk.ac.ox.cs.diadem.roseann.aggregation.MEMM.core.training.util.FeatureExtractorInterface#extractFeature(uk.ac.ox.cs.diadem.roseann.aggregation.MEMM.core.dataModel.RawEvent, boolean)
	 */

	/**
	 * 
	 */
	public NaiveFeatureExtractorImpl() {
		
	}

	@Override
	public Event extractFeature(Event event, boolean forTraining) {
		Map<String, String> contextPred = event.getContextPredHash();
		Set<String> realContext = new HashSet<String>();
		//1. construct context vector.
		for (int i = 0; i < annotatorNames.length; i++) {
			String oneAnnotator = annotatorNames[i];
			// for(String oneAnnotator:annotatorNames){
			String opinionX = contextPred.get(oneAnnotator);
			if (opinionX == null) {// annotator says nothing
				opinionX = "Other";
			}else{
				opinionX = opinionX.replaceAll("(\\.|-)", "_");
			}
			if(!opinionX.equals("Other")){
				realContext.add((oneAnnotator.toLowerCase() + "_" + opinionX)); // individual's
																// opinion
			}
		
			for (int j = i + 1; j < annotatorNames.length; j++) {
				String opinionY = contextPred.get(annotatorNames[j]);
				if (opinionY == null) {// annotator says nothing
					opinionY = "Other";
				}else{
					opinionY = opinionY.replaceAll("(\\.|-)", "_");
				}
				realContext.add((oneAnnotator.toLowerCase() + "_" + opinionX+"_"+opinionY+"_"+annotatorNames[j].toLowerCase()));
				
				
			}
		}
		//2. add outcome if applicable
		String[] res = realContext.toArray(new String[0]);
		String outcome = forTraining?event.getOutcome():"?";
		event.setOutcome(outcome);
		event.setContextPred(res);
		return event;
	}

	


}
