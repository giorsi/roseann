/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.ontology.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.roseann.ontology.OntologyException;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.ModelFactory;

/**
 * 
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk), Department of Computer Science, Oxford University
 * 
 *         Utility class to create the materialized files that represent the ontology to be used in RoseAnn. Given an
 *         output folder, the onotology file and the prefixes used in the onotlogy, the class creates in the output
 *         folder the materialized files that represent the ontology. The ontology must contains, for each annotator,
 *         all the concepts recognized by the annotator with the same prefix. Othe than individual annotator concepts,
 *         the ontology must also contains, for each single concept recognized by each annotator, a global concept with
 *         a global prefix. The global concept is used to map the local annotator representation to a global
 *         representation, which is shared by all the annotators
 * 
 */
public class CreateMaterializedOntology {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(CreateMaterializedOntology.class);

  /**
   * Global prefix
   */
  private static String globalPrefix;

  /**
   * The ontology model
   */
  private static OntModel model;

  /**
   * The output folder
   */
  private static File folder;

  /**
   * Map to keep trace, for each annotator, the name with the prefix in the ontology
   */
  private static Map<String, String> annotator2prefix;

  /**
   * Method that, given the input folder, creates the needed materialized files that represent the onotlogical
   * contraints WARNING: the ontology must define a prefix called global that is the prefix used to represent the global
   * concepts of the ontology. If this is not define, an exception will be thrown
   * 
   * @param folder
   *          The folder where the materialized files will be created
   * @param annotator2prefix
   *          A map where the key is the annotator name, the value is the prefix used for the conceptes recognized by
   *          the annotator key in the ontology
   * @param globalPrefix
   *          The prefix used for global concepts in the ontology
   * @param ontologyFile
   *          The URI of the location of the ontology (can be either a local or remote file)
   * @throws IOException
   *           If there are problems reading or creating the ontology files
   * @throws OntologyException
   *           If the input ontology does not specify a namespace prefix called global
   */
  public static void createMaterializedOntologyFiles(final File folder, final Map<String, String> annotator2prefix,
      final URI ontologyFile) throws IOException {

    ConfigurationFacility.getConfiguration();
    model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_MICRO_RULE_INF);
    model.read(ontologyFile.toString());

    CreateMaterializedOntology.globalPrefix = model.getNsPrefixURI("global");
    if (globalPrefix == null)
      throw new OntologyException(
          "The ontology does not define a prefix called global, impossible to create the materialized"
              + " files of the ontology", logger);

    // model.read("file:"+ontologyFile.getAbsolutePath());
    CreateMaterializedOntology.annotator2prefix = annotator2prefix;
    CreateMaterializedOntology.folder = folder;

    // create sequentially all the ontology files
    retrieveAllSuperclasses();
    retrieveAnnotatorKnowledge();
    retrieveAnnotatorSupertype();
    retrieveAssertedDisjointedClasses();
    retrieveDirectSubclasses();
    retrieveDirectSuperclasses();
    retrieveDisjointedClasses();
    retrieveEquivalentClassAnnotator();
    retrieveGlobalConceptMapping();
    retrieveImplicants();
    retrieveSubClassKnowledge();
    retrieveSuperClassKnowledge();

  }

  private static String getGlobalType(final String prefix, final String type) {

    logger.info("Retrieving the global type for {}{}", prefix, type);

    // Create a new query
    final String queryString = "PREFIX :<http://www.w3.org/2002/07/owl#> SELECT ?equivalent " + "WHERE {<" + prefix
        + type + "> :equivalentClass ?equivalent " + "FILTER( REGEX (STR(?equivalent), '" + globalPrefix + "*' ))}\n";
    final Query query = QueryFactory.create(queryString);

    // Execute the query and obtain results
    final QueryExecution qe = QueryExecutionFactory.create(query, model);
    final ResultSet results = qe.execSelect();

    String result = null;
    while (results.hasNext()) {
      result = results.next().getResource("?equivalent").toString().replace(globalPrefix, "");
    }

    qe.close();

    if (result == null) {
      logger.warn("No global type has been found for class {}{}", prefix, type);
    } else {
      logger.info("Global type {} retrieved for class {}", result, prefix + type);
    }
    return result;

  }

  private static Set<String> getAllSuperclasses(final String type) {
    logger.info("Retrieving superypes for the type for {}", type);

    // Create a new query
    final String queryString = "PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> "
        + "SELECT ?AllSuperClasses WHERE {<" + globalPrefix + type + "> rdfs:subClassOf ?AllSuperClasses "
        + "FILTER( REGEX (STR(?AllSuperClasses), '" + globalPrefix + "*' ))}\n";
    final Query query = QueryFactory.create(queryString);

    // Execute the query and obtain results
    final QueryExecution qe = QueryExecutionFactory.create(query, model);
    final ResultSet results = qe.execSelect();

    if (!results.hasNext()) {
      logger.warn("No supertypes have been found for class {}", type);
      return null;
    }
    final Set<String> superClasses = new HashSet<String>();
    while (results.hasNext()) {
      final String supertype = results.next().getResource("?AllSuperClasses").toString().replace(globalPrefix, "");
      superClasses.add(supertype);
    }

    qe.close();
    logger.info("Supertypes {} retrieved for class {}", superClasses, type);
    return superClasses;
  }

  private static Set<String> getDisjointClasses(final String type, final boolean withInference) {
    logger.info("Retrieving disjoint types for the type for {}", type);

    // Create a new query
    String queryString = "PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> PREFIX :<http://www.w3.org/2002/07/owl#> "
        + "SELECT distinct ?global_disjoint "
        + "WHERE { { <"
        + globalPrefix
        + type
        + "> :disjointWith ?disjoint. "
        + "?disjoint :equivalentClass ?global_disjoint. "
        + "FILTER( REGEX (STR(?global_disjoint), '"
        + globalPrefix
        + "*' ))} "
        + "UNION { <"
        + globalPrefix
        + type
        + "> :equivalentClass ?equivalent. "
        + "?equivalent :disjointWith ?disjoint. "
        + "?disjoint :equivalentClass ?global_disjoint. "
        + "FILTER( REGEX (STR(?global_disjoint), '" + globalPrefix + "*' ))} ";
    if (withInference) {
      queryString += "UNION { <" + globalPrefix + type + "> rdfs:subClassOf ?superClass. "
          + "?superClass :disjointWith ?disjoint. " + "?disjoint :equivalentClass ?global_disjoint. "
          + "FILTER( REGEX (STR(?global_disjoint), '" + globalPrefix + "*' ))}}";
    } else {
      queryString += "}";
    }
    final Query query = QueryFactory.create(queryString);

    // Execute the query and obtain results
    final QueryExecution qe = QueryExecutionFactory.create(query, model);
    final ResultSet results = qe.execSelect();

    final Set<String> disjointClasses = new HashSet<String>();
    while (results.hasNext()) {
      final String supertype = results.next().getResource("?global_disjoint").toString().replace(globalPrefix, "");
      disjointClasses.add(supertype);
    }

    qe.close();
    logger.info("Disjoint classes {} retrieved for class {}", disjointClasses, type);
    return disjointClasses;

  }

  private static Set<String> getAnnotatorSuperclass(final String prefix, final String type) {
    logger.info("Retrieving the supertypes (with same prefix) for the type for {}{}", prefix, type);

    // Create a new query
    final String queryString = "PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> "
        + "SELECT ?AnnotatorSuperclass WHERE {<" + prefix + type + "> rdfs:subClassOf ?AnnotatorSuperclass "
        + "FILTER( REGEX (STR(?AnnotatorSuperclass), '" + prefix + "*' ))}\n";

    final Query query = QueryFactory.create(queryString);

    // Execute the query and obtain results
    final QueryExecution qe = QueryExecutionFactory.create(query, model);
    final ResultSet results = qe.execSelect();

    final Set<String> supertypes = new HashSet<String>();
    while (results.hasNext()) {
      final String supertype = results.next().getResource("?AnnotatorSuperclass").toString().replace(prefix, "");
      supertypes.add(supertype);
    }

    qe.close();
    logger.info("Supertypes with same prefix {} retrieved for class {}", supertypes, prefix + type);
    return supertypes;
  }

  private static Set<String> getEquivalentClassAnnotator(final String type) {
    logger.info("Retrieving the annotators which directly know the class {}", type);

    // Create a new query
    final String queryString = "PREFIX :<http://www.w3.org/2002/07/owl#> " + "select ?equivalentClassAnnotator "
        + "where {<" + globalPrefix + type + "> :equivalentClass ?equivalentClassAnnotator.}";

    final Query query = QueryFactory.create(queryString);

    // Execute the query and obtain results
    final QueryExecution qe = QueryExecutionFactory.create(query, model);
    final ResultSet results = qe.execSelect();

    if (!results.hasNext()) {
      logger.warn("The global type {} has not been found in the ontology", type);
      return null;
    }

    final Set<String> annotator = new HashSet<String>();
    while (results.hasNext()) {
      final String retrievedType = results.next().getResource("?equivalentClassAnnotator").toString();
      // skip the global concept
      if (retrievedType.contains(globalPrefix)) {
        continue;
      }
      for (final String annotatorName : annotator2prefix.keySet()) {
        final String prefix = annotator2prefix.get(annotatorName);
        if (retrievedType.contains(prefix)) {
          annotator.add(annotatorName);
        }
      }
    }

    qe.close();
    logger.info("Annotator {} retrieved which directly know the class {}", annotator, type);
    return annotator;
  }

  private static Set<String> getImplicants(final String type) {
    logger.info("Retrieving the implicant classes for the class {}", type);

    // Create a new query
    final String queryString = "PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> " + "SELECT ?implicant WHERE {"
        + "?implicant rdfs:subClassOf <" + globalPrefix + type + "> " + "FILTER( REGEX (STR(?implicant), '"
        + globalPrefix + "*' ))}";

    final Query query = QueryFactory.create(queryString);

    // Execute the query and obtain results
    final QueryExecution qe = QueryExecutionFactory.create(query, model);
    final ResultSet results = qe.execSelect();

    if (!results.hasNext()) {
      logger.warn("The global type {} has not been found in the ontology", type);
      return null;
    }

    final Set<String> implicants = new HashSet<String>();
    while (results.hasNext()) {
      final String retrievedType = results.next().getResource("?implicant").toString().replace(globalPrefix, "");
      implicants.add(retrievedType);
    }

    qe.close();
    logger.info("Implicants {} retrieved for the class {}", implicants, type);
    return implicants;

  }

  private static Set<String> getSubclassKnowledge(final String type) {
    logger.info("Retrieving the annotators which know a sub-class of the class {}", type);

    // Create a new query
    final String queryString = "PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> " + "select ?subClassKnowledge "
        + "where {?subClassKnowledge rdfs:subClassOf <" + globalPrefix + type + ">.}";

    final Query query = QueryFactory.create(queryString);

    // Execute the query and obtain results
    final QueryExecution qe = QueryExecutionFactory.create(query, model);
    final ResultSet results = qe.execSelect();

    if (!results.hasNext()) {
      logger.warn("The global type {} has not been found in the ontology", type);
      return null;
    }

    final Set<String> knowledge = new HashSet<String>();
    while (results.hasNext()) {
      final String retrievedYype = results.next().getResource("?subClassKnowledge").toString();
      for (final String annotatorName : annotator2prefix.keySet()) {
        final String prefix = annotator2prefix.get(annotatorName);
        // not include the global type
        if (retrievedYype.contains(globalPrefix)) {
          continue;
        }
        if (retrievedYype.contains(prefix)) {
          knowledge.add(annotatorName);
        }

      }
    }

    qe.close();
    logger.info("Annotators {} retrieved which know a sub-class of the class {}", knowledge, type);
    return knowledge;

  }

  private static Set<String> getSuperclassKnowledge(final String type) {
    logger.info("Retrieving the annotators which know a super-class of the class {}", type);

    // check if it's the top concept
    boolean isTop = false;
    final Set<String> supertypes = getAllSuperclasses(type);
    if (supertypes.size() == 1 && supertypes.contains(type)) {
      isTop = true;
    }

    // Create a new query
    String queryString = "PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> "
        + "PREFIX :<http://www.w3.org/2002/07/owl#> " + "select ?superClassKnowledge " + "where {<" + globalPrefix
        + type + "> rdfs:subClassOf ?superClassKnowledge.";
    if (isTop) {
      queryString += "}";
    } else {
      queryString += "FILTER NOT EXISTS {?superClassKnowledge :equivalentClass :Thing " + "}}";
    }

    final Query query = QueryFactory.create(queryString);

    // Execute the query and obtain results
    final QueryExecution qe = QueryExecutionFactory.create(query, model);
    final ResultSet results = qe.execSelect();

    if (!results.hasNext()) {
      logger.warn("The global type {} has not been found in the ontology", type);
      return null;
    }

    final Set<String> knowledge = new HashSet<String>();
    while (results.hasNext()) {
      final String retrievedType = results.next().getResource("?superClassKnowledge").toString();
      for (final String annotatorName : annotator2prefix.keySet()) {
        final String prefix = annotator2prefix.get(annotatorName);
        // not include the global type and the top concept
        if (retrievedType.contains(globalPrefix)) {
          continue;
        }
        if (retrievedType.contains(prefix)) {
          knowledge.add(annotatorName);
        }

      }
    }

    qe.close();
    logger.info("Annotators {} retrieved which know a sub-class of the class {}", knowledge, type);
    return knowledge;

  }

  private static Set<String> getAnnotatorKnowledge(final String annotatatorName) {
    logger.info("Retrieving the concepts known by the annotator {}", annotatatorName);
    final String prefix = annotator2prefix.get(annotatatorName);

    // Create a new query
    final String queryString = "PREFIX :<http://www.w3.org/2002/07/owl#>" + "SELECT ?annotatorKnowledge " + "WHERE "
        + "{?annotator :equivalentClass ?annotatorKnowledge. " + "FILTER(REGEX (STR(?annotator), '" + prefix + "' )) "
        + "FILTER(REGEX (STR(?annotatorKnowledge), '" + globalPrefix + "' ))}";

    final Query query = QueryFactory.create(queryString);

    // Execute the query and obtain results
    final QueryExecution qe = QueryExecutionFactory.create(query, model);
    final ResultSet results = qe.execSelect();

    final Set<String> knowledge = new HashSet<String>();
    while (results.hasNext()) {
      final String retrievedType = results.next().getResource("?annotatorKnowledge").toString()
          .replace(globalPrefix, "");
      knowledge.add(retrievedType);
    }

    qe.close();
    logger.info("Types {} retrieved known by the annotator {}", knowledge, annotatatorName);
    return knowledge;

  }

  private static Set<String> getDirectSuperclasses(final String type) {
    logger.info("Retrieving direct superclasses for the type {}", type);
    // get the superclasses
    final Set<String> superclasses = getAllSuperclasses(type);
    if (superclasses == null) {
      logger.warn("The type {} is not present in the Ontology", type);
      return null;
    }
    superclasses.remove(type);

    // for each superclass, get the superclasses
    final Set<String> allSuperclasses = new HashSet<String>();
    for (final String superclass : superclasses) {
      final Set<String> superclassSuperclasses = getAllSuperclasses(superclass);
      superclassSuperclasses.remove(superclass);
      allSuperclasses.addAll(superclassSuperclasses);
    }

    // remove the superclasses
    superclasses.removeAll(allSuperclasses);
    logger.info("Direct superclasses {} retrieved for the type {}", superclasses, type);

    return superclasses;

  }

  private static Set<String> getDirectSubclasses(final String type) {
    logger.info("Retrieving direct sub-classes for the type {}", type);
    // get the superclasses
    final Set<String> subclasses = getImplicants(type);
    if (subclasses == null) {
      logger.warn("The type {} is not present in the Ontology", type);
      return null;
    }
    subclasses.remove(type);

    // for each superclass, get the superclasses
    final Set<String> allSubclasses = new HashSet<String>();
    for (final String subclass : subclasses) {
      final Set<String> subclassSubclasses = getImplicants(subclass);
      subclassSubclasses.remove(subclass);
      allSubclasses.addAll(subclassSubclasses);
    }

    // remove the superclasses
    subclasses.removeAll(allSubclasses);
    logger.info("Direct subclasses {} retrieved for the type {}", subclasses, type);

    return subclasses;

  }

  public static void main(final String[] args) throws IOException, URISyntaxException {

    // String globalPrefix="http://WebAnnotation/global/";

    final Map<String, String> annotator2prefix = new HashMap<String, String>();
    annotator2prefix.put("extractiv", "http://extractiv.com/");
    annotator2prefix.put("dbpediaSpotlight", "http://dbpedia.org/ontology/");
    annotator2prefix.put("openCalais", "http://www.opencalais.com/");
    annotator2prefix.put("alchemyAPI", "http://www.alchemyapi.com/");
    annotator2prefix.put("stanfordNER", "http://nlp.stanford.edu/");
    annotator2prefix.put("illinoisNER", "http://cogcomp.cs.illinois.edu/");
    annotator2prefix.put("lupedia", "http://dbpedia.org/ontology/");
    annotator2prefix.put("yahooAPI", "http://develop.yahoo.com/");
    annotator2prefix.put("saplo", "http://develop.saplo.com/");
    annotator2prefix.put("zemanta", "http://developer.zemanta.com/");
    annotator2prefix.put("wikimeta", "http://www.wikimeta.com/");

    final File folder = new File("materializedOntology");
    final URI ontologyFile = new URI("file:ontology.owl");

    createMaterializedOntologyFiles(folder, annotator2prefix, ontologyFile);

  }

  private static Set<String> getAllPrefixClasses(final String prefix) {
    logger.info("Retrieving all the concepts with prefix {}", prefix);

    // Create a new query
    final String queryString = "PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> "
        + "SELECT ?class WHERE {?class rdf:type rdfs:Class. " + "FILTER( REGEX (STR(?class), '" + prefix + "*' ))}\n";

    final Query query = QueryFactory.create(queryString);

    // Execute the query and obtain results
    final QueryExecution qe = QueryExecutionFactory.create(query, model);
    final ResultSet results = qe.execSelect();

    final Set<String> concepts = new HashSet<String>();
    while (results.hasNext()) {
      final String retrievedType = results.next().getResource("?class").toString().replace(prefix, "");
      concepts.add(retrievedType);
    }

    qe.close();
    logger.info("All types with prefix {} retrieved {}", prefix, concepts);
    return concepts;
  }

  private static void retrieveGlobalConceptMapping() throws IOException {
    final String fileName = "global_concept_mapping";
    logger.info("Writing the file '{}' ...", fileName);

    final Set<String> toWrite = new HashSet<String>();

    for (final String annotatorName : annotator2prefix.keySet()) {
      final String prefix = annotator2prefix.get(annotatorName);
      final Set<String> annotatorClasses = getAllPrefixClasses(prefix);
      for (final String localClass : annotatorClasses) {
        final String globalType = getGlobalType(prefix, localClass);
        toWrite.add(annotatorName + ":" + localClass + "\t" + globalType);
      }
    }

    writeLineToFile(folder.getAbsolutePath() + File.separator + fileName, toWrite);
  }

  private static void retrieveAllSuperclasses() throws IOException {
    final String fileName = "all_supertype";
    logger.info("Writing the file '{}' ...", fileName);

    final Set<String> allGlobalConcept = getAllPrefixClasses(globalPrefix);
    final Set<String> toWrite = new HashSet<String>();
    for (final String globalClass : allGlobalConcept) {
      final Set<String> superclasses = getAllSuperclasses(globalClass);
      String line = globalClass + "\t";
      for (final String superclass : superclasses) {
        line += superclass + ",";
      }
      if (line.contains(",")) {
        line = line.substring(0, line.length() - 1);
      }
      toWrite.add(line);
    }
    writeLineToFile(folder.getAbsolutePath() + File.separator + fileName, toWrite);

  }

  private static void retrieveAnnotatorKnowledge() throws IOException {
    final String fileName = "annotator_knowledge";
    logger.info("Writing the file '{}' ...", fileName);

    final Set<String> toWrite = new HashSet<String>();
    for (final String annotatorName : annotator2prefix.keySet()) {
      final Set<String> annotatorKnowledge = getAnnotatorKnowledge(annotatorName);
      String line = annotatorName + "\t";
      for (final String knownClass : annotatorKnowledge) {
        line += knownClass + ",";
      }
      if (line.contains(",")) {
        line = line.substring(0, line.length() - 1);
      }
      toWrite.add(line);
    }

    // for each annotator get the annotator knowledge
    writeLineToFile(folder.getAbsolutePath() + File.separator + fileName, toWrite);

  }

  private static void retrieveAssertedDisjointedClasses() throws IOException {
    final String fileName = "asserted_disjoint_classes";
    logger.info("Writing the file '{}' ...", fileName);

    final Set<String> allClasses = getAllPrefixClasses(globalPrefix);
    final Set<String> toWrite = new HashSet<String>();
    for (final String globalClass : allClasses) {
      final Set<String> disjointClasses = getDisjointClasses(globalClass, false);
      String line = globalClass + "\t";
      for (final String disjointClass : disjointClasses) {
        line += disjointClass + ",";
      }
      if (line.contains(",")) {
        line = line.substring(0, line.length() - 1);
      }
      toWrite.add(line);
    }

    // for each annotator get the annotator knowledge
    writeLineToFile(folder.getAbsolutePath() + File.separator + fileName, toWrite);
  }

  private static void retrieveDisjointedClasses() throws IOException {
    final String fileName = "disjoint_classes";
    logger.info("Writing the file '{}' ...", fileName);

    final Set<String> allClasses = getAllPrefixClasses(globalPrefix);
    final Set<String> toWrite = new HashSet<String>();
    for (final String globalClass : allClasses) {
      final Set<String> disjointClasses = getDisjointClasses(globalClass, true);
      String line = globalClass + "\t";
      for (final String disjointClass : disjointClasses) {
        line += disjointClass + ",";
      }
      if (line.contains(",")) {
        line = line.substring(0, line.length() - 1);
      }
      toWrite.add(line);
    }

    // for each annotator get the annotator knowledge
    writeLineToFile(folder.getAbsolutePath() + File.separator + fileName, toWrite);
  }

  private static void retrieveDirectSubclasses() throws IOException {
    final String fileName = "direct_subclasses";
    logger.info("Writing the file '{}' ...", fileName);

    final Set<String> allClasses = getAllPrefixClasses(globalPrefix);
    final Set<String> toWrite = new HashSet<String>();
    for (final String globalClass : allClasses) {
      final Set<String> directSubclasses = getDirectSubclasses(globalClass);
      String line = globalClass + "\t";
      for (final String subclass : directSubclasses) {
        line += subclass + ",";
      }
      if (line.contains(",")) {
        line = line.substring(0, line.length() - 1);
      }
      toWrite.add(line);
    }

    // for each annotator get the annotator knowledge
    writeLineToFile(folder.getAbsolutePath() + File.separator + fileName, toWrite);
  }

  private static void retrieveDirectSuperclasses() throws IOException {
    final String fileName = "direct_superclasses";
    logger.info("Writing the file '{}' ...", fileName);

    final Set<String> allClasses = getAllPrefixClasses(globalPrefix);
    final Set<String> toWrite = new HashSet<String>();
    for (final String globalClass : allClasses) {
      final Set<String> directSuperclasses = getDirectSuperclasses(globalClass);
      String line = globalClass + "\t";
      for (final String superclass : directSuperclasses) {
        line += superclass + ",";
      }
      if (line.contains(",")) {
        line = line.substring(0, line.length() - 1);
      }
      toWrite.add(line);
    }

    // for each annotator get the annotator knowledge
    writeLineToFile(folder.getAbsolutePath() + File.separator + fileName, toWrite);
  }

  private static void retrieveEquivalentClassAnnotator() throws IOException {
    final String fileName = "equivalent_class_annotator";
    logger.info("Writing the file '{}' ...", fileName);

    final Set<String> allClasses = getAllPrefixClasses(globalPrefix);
    final Set<String> toWrite = new HashSet<String>();
    for (final String globalClass : allClasses) {
      final Set<String> annotators = getEquivalentClassAnnotator(globalClass);
      String line = globalClass + "\t";
      for (final String annotator : annotators) {
        line += annotator + ",";
      }
      if (line.contains(",")) {
        line = line.substring(0, line.length() - 1);
      }
      toWrite.add(line);
    }

    // for each annotator get the annotator knowledge
    writeLineToFile(folder.getAbsolutePath() + File.separator + fileName, toWrite);

  }

  private static void retrieveImplicants() throws IOException {
    final String fileName = "implicants";
    logger.info("Writing the file '{}' ...", fileName);

    final Set<String> allClasses = getAllPrefixClasses(globalPrefix);
    final Set<String> toWrite = new HashSet<String>();
    for (final String globalClass : allClasses) {
      final Set<String> implicants = getImplicants(globalClass);
      String line = globalClass + "\t";
      for (final String implicant : implicants) {
        line += implicant + ",";
      }
      if (line.contains(",")) {
        line = line.substring(0, line.length() - 1);
      }
      toWrite.add(line);
    }

    // for each annotator get the annotator knowledge
    writeLineToFile(folder.getAbsolutePath() + File.separator + fileName, toWrite);
  }

  private static void retrieveSubClassKnowledge() throws IOException {
    final String fileName = "sub_class_knowledge";
    logger.info("Writing the file '{}' ...", fileName);

    final Set<String> allClasses = getAllPrefixClasses(globalPrefix);
    final Set<String> toWrite = new HashSet<String>();
    for (final String globalClass : allClasses) {
      final Set<String> annotators = getSubclassKnowledge(globalClass);
      String line = globalClass + "\t";
      for (final String annotator : annotators) {
        line += annotator + ",";
      }
      if (line.contains(",")) {
        line = line.substring(0, line.length() - 1);
      }
      toWrite.add(line);
    }

    // for each annotator get the annotator knowledge
    writeLineToFile(folder.getAbsolutePath() + File.separator + fileName, toWrite);

  }

  private static void retrieveSuperClassKnowledge() throws IOException {
    final String fileName = "super_class_knowledge";
    logger.info("Writing the file '{}' ...", fileName);

    final Set<String> allClasses = getAllPrefixClasses(globalPrefix);
    final Set<String> toWrite = new HashSet<String>();
    for (final String globalClass : allClasses) {
      final Set<String> annotators = getSuperclassKnowledge(globalClass);
      String line = globalClass + "\t";
      for (final String annotator : annotators) {
        line += annotator + ",";
      }
      if (line.contains(",")) {
        line = line.substring(0, line.length() - 1);
      }
      toWrite.add(line);
    }

    // for each annotator get the annotator knowledge
    writeLineToFile(folder.getAbsolutePath() + File.separator + fileName, toWrite);
  }

  private static void retrieveAnnotatorSupertype() throws IOException {

    for (final String annotaotrName : annotator2prefix.keySet()) {
      final String fileName = annotaotrName + "_supertype";
      logger.info("Writing the file '{}' ...", fileName);
      final Set<String> toWrite = new HashSet<String>();
      final String prefix = annotator2prefix.get(annotaotrName);

      final Set<String> annotatorClasses = getAllPrefixClasses(prefix);
      for (final String annotatorClass : annotatorClasses) {
        final Set<String> annotatorSuperclasses = getAnnotatorSuperclass(prefix, annotatorClass);
        String line = annotatorClass + "\t";
        for (final String annotatorSuperclass : annotatorSuperclasses) {
          line += annotatorSuperclass + ",";
        }
        if (line.contains(",")) {
          line = line.substring(0, line.length() - 1);
        }
        toWrite.add(line);
      }
      writeLineToFile(folder.getAbsolutePath() + File.separator + fileName, toWrite);

    }

  }

  private static void writeLineToFile(final String filePath, final Set<String> lines) throws IOException {
    // write on the file
    final File fileToWrite = new File(filePath);

    final FileWriter fw = new FileWriter(fileToWrite.getAbsoluteFile());
    final BufferedWriter bw = new BufferedWriter(fw);
    for (final String line : lines) {
      bw.write(line + "\n");
    }
    bw.flush();
    bw.close();
    fw.close();
    logger.info("...'{}' written succesuflly", filePath);
  }

}
