/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.util.graph;

import java.util.Collection;
import java.util.HashSet;

/**
 * 
 * 
 * @author Stefano Ortona (stefano dot ortona at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 * 
 *         Utility class to model a Graph Node
 *         Each node has a label of type T, two nodes are considered equals if they have the same label
 */

public class Node<T> implements Comparable<T>{
	
	private T label;
	private Collection<Node<T>> neighbours;
	
	/**
	 * A node can be created only in a context of a graph, not alone
	 * @param label	
	 * 				The node label
	 */
	protected Node(T label){
		this.label=label;
		this.neighbours=new HashSet<Node<T>>();
	}
	
	public T getLabel(){
		return this.label;
	}
	
	protected Collection<Node<T>> getNeighbours(){
		return this.neighbours;
	}
	
	protected void addNeighbour(Node<T> neighbour){
		if(!this.neighbours.contains(neighbour))
			this.neighbours.add(neighbour);
	}
	
	public int hashCode(){
		return this.label.hashCode();
	}
	
	public boolean equals(Object obj){
		@SuppressWarnings("unchecked")
		Node<T> node=(Node<T>) obj;
		return this.label.equals(node.getLabel());
		
	}
	
	public String toString(){
		if(this.neighbours.size()==0) return "["+this.label+"]";
		String toString="["+this.label+"-->";
		for(Node<T> neighbour:this.neighbours){
			toString+=neighbour.getLabel()+",";
		}
		toString=toString.substring(0,toString.length()-1)+"]";
		
		return toString;
	}

	@Override
	public int compareTo(Object o) {
		@SuppressWarnings("unchecked")
		final Node<T> n = (Node<T>) o; 
		return this.getLabel().toString().compareTo(n.getLabel().toString());
	}

}
