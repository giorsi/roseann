/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.swing.util;

import java.awt.BorderLayout;
import java.io.IOException;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Console{

	private static JTextArea textArea;
	private static JPanel concolePnl;
	private static JScrollPane jScrollPane1;

	public static JPanel init() throws IOException {
		textArea=new JTextArea();
		textArea.setFont(java.awt.Font.decode("monospaced"));

		concolePnl = new JPanel();
		concolePnl.setLayout(new BorderLayout());
		jScrollPane1 = new JScrollPane();
		jScrollPane1.setViewportView(textArea);
		concolePnl.add(jScrollPane1, BorderLayout.CENTER);
		return concolePnl;

	}

	public static void print(String text){
		if(textArea==null)
			return;
		textArea.append(text);
		scrollConsole();

	}

	public static void print(){
		if(textArea==null)
			return;
		textArea.append("");
		scrollConsole();

	}

	public static void println(final String text){
		if(textArea==null)
			return;
		textArea.append(text+"\n");
		scrollConsole();
	}

	public static void println(){
		if(textArea==null)
			return;
		textArea.append("\n");
		scrollConsole();
		

	}
	
	private static void scrollConsole(){
		concolePnl.revalidate();
		int height = (int) textArea.getPreferredSize().getHeight();
		jScrollPane1.getVerticalScrollBar().setValue(height);
		
	}

}