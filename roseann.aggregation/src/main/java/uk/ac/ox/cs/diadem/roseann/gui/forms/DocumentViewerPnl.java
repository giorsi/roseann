/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.forms;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.tree.*;

import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.AnnotatedDocumentModel;
import uk.ac.ox.cs.diadem.roseann.gui.swingComponent.textpane.DocViewTextPane;





/**
 * 
 * @author Luying Chen (luying dot chen at cs dot ox dot ac dot uk) -
 *         Department of Computer Science - University of Oxford
 */
@SuppressWarnings("serial")
public class DocumentViewerPnl extends JPanel {
	

	public DocumentViewerPnl() {
		initComponents();
	}

	private void initComponents() {
		splitPane1 = new JSplitPane();
		splitPane4 = new JSplitPane();
		bottomPnl = new JPanel();
		scrollPane2 = new JScrollPane();
		conflictTbl = new JTable();
		scrollPane6 = new JScrollPane();
		annotatedDocViewTxtpane = new DocViewTextPane();
		splitPane3 = new JSplitPane();
		annotatorsPnl = new JPanel();
		scrollPane1 = new JScrollPane();
		panel4 = new JPanel();
		baseAnLbl = new JLabel();
		separator1 = new JSeparator();
		aggregatorLbl = new JLabel();
		alchemyChckbox = new JCheckBox();
		goldstandardChckbox = new JCheckBox();
		extractivChckbox = new JCheckBox();
		wrChckbox = new JCheckBox();
		dbpediaChckbox = new JCheckBox();
		memmChckbox = new JCheckBox();
		zemantaChckbox = new JCheckBox();
		opencalaisChckbox = new JCheckBox();
		//gateChckbox = new JCheckBox();
		lupediaChckbox = new JCheckBox();
		saploChckbox = new JCheckBox();
		wikimetaChckbox = new JCheckBox();
		yahooChckbox = new JCheckBox();
		stanfordChckbox = new JCheckBox();
		illinoisChckbox = new JCheckBox();
		annotationSelPane = new JPanel();
		scrollPane5 = new JScrollPane();
		legendTree = new JTree();
		label1 = new JLabel();
		splitPane5 = new JSplitPane();
		segmentationPane = new JPanel();
		lineChckbox = new JCheckBox();
		blockChckbox = new JCheckBox();
		blockGroupChckbox = new JCheckBox();
		panel5 = new JPanel();
		scrollPane7 = new JScrollPane();
		refinedSegLbl = new JLabel();
		separator2 = new JSeparator();
		originalSegLbl = new JLabel();
		originalLineChckbox = new JCheckBox();
		originalBlockChckbox = new JCheckBox();
		originalBlockGroupChckbox = new JCheckBox();

		//======== this ========
		setLayout(new BorderLayout());

		//======== splitPane1 ========
		{
			splitPane1.setResizeWeight(1.0);
			splitPane1.setOneTouchExpandable(true);

			//======== splitPane5 ========
			{
				splitPane5.setOrientation(JSplitPane.VERTICAL_SPLIT);
				splitPane5.setOneTouchExpandable(true);
				splitPane5.setResizeWeight(0.75);

				//======== bottomPnl ========
				{
					bottomPnl.setBorder(new TitledBorder(null, "Conflicts", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Arial", Font.BOLD, 12)));
					bottomPnl.setLayout(new BorderLayout());

					//======== scrollPane2 ========
					{

						//---- conflictTbl ----
						conflictTbl.setFont(new Font("Arial", Font.PLAIN, 12));
						conflictTbl.setModel(new DefaultTableModel(
							new Object[][] {
								{"abc", "abc", "abc", "abc", null},
								{"abc", "abc", "abc", "abc", null},
								{"abc", "abc", "abc", "abc", null},
								{null, null, null, null, null},
								{null, null, null, null, null},
								{null, null, null, null, null},
								{null, null, null, null, null},
								{null, null, null, null, null},
								{null, null, null, null, null},
							},
							new String[] {
								null, null, null, null, null
							}
						));
						{
							TableColumnModel cm = conflictTbl.getColumnModel();
							cm.getColumn(0).setResizable(false);
						}
						conflictTbl.setPreferredScrollableViewportSize(new Dimension(450, 60));
						conflictTbl.setFillsViewportHeight(true);
						scrollPane2.setViewportView(conflictTbl);
					}
					bottomPnl.add(scrollPane2, BorderLayout.CENTER);
				}
				splitPane4.setBottomComponent(bottomPnl);

				//======== scrollPane6 ========
				{
					scrollPane6.setPreferredSize(new Dimension(400,300));

					//---- annotatedDocViewTxtpane ----
					annotatedDocViewTxtpane.setFont(new Font("Arial", Font.PLAIN, 12));
					annotatedDocViewTxtpane.setEditable(false);
					annotatedDocViewTxtpane.setText("Display the annotated document...");
					scrollPane6.setViewportView(annotatedDocViewTxtpane);
				}
				splitPane4.setTopComponent(scrollPane6);
			}
			splitPane1.setLeftComponent(splitPane4);
			//======= splitPane4 =======
			{
				splitPane4.setOrientation(JSplitPane.VERTICAL_SPLIT);
				splitPane4.setOneTouchExpandable(true);
				splitPane4.setDividerLocation(200);
				
				//======== splitPane3 ========
				{
					splitPane3.setOrientation(JSplitPane.VERTICAL_SPLIT);
					splitPane3.setOneTouchExpandable(true);
					splitPane3.setDividerLocation(200);
	
					//======== annotatorsPnl ========
					{
						annotatorsPnl.setBorder(new TitledBorder(null, "AnnotatorSelector", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Arial", Font.BOLD, 12)));
						annotatorsPnl.setLayout(new BorderLayout());
	
						//======== scrollPane1 ========
						{
							scrollPane1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	
							//======== panel4 ========
							{
								panel4.setLayout(new GridBagLayout());
								((GridBagLayout)panel4.getLayout()).columnWidths = new int[] {101, 0, 102, 0};
								((GridBagLayout)panel4.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
								((GridBagLayout)panel4.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
								((GridBagLayout)panel4.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
	
								//---- baseAnLbl ----
								baseAnLbl.setText("Base annotators:");
								baseAnLbl.setFont(new Font("Arial", Font.PLAIN, 12));
								baseAnLbl.setForeground(new Color(153, 153, 255));
								baseAnLbl.setHorizontalAlignment(SwingConstants.CENTER);
								panel4.add(baseAnLbl, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 0, 0), 0, 0));
	
								//---- separator1 ----
								separator1.setOrientation(SwingConstants.VERTICAL);
								panel4.add(separator1, new GridBagConstraints(1, 0, 1, 13, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
									new Insets(0, 5, 0, 5), 0, 0));
	
								//---- aggregatorLbl ----
								aggregatorLbl.setText("Aggregators:");
								aggregatorLbl.setForeground(new Color(255, 153, 153));
								aggregatorLbl.setFont(new Font("Arial", Font.PLAIN, 12));
								aggregatorLbl.setHorizontalAlignment(SwingConstants.CENTER);
								panel4.add(aggregatorLbl, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 0, 0), 0, 0));
	
								//---- alchemyChckbox ----
								alchemyChckbox.setText("AlchemyAPI");
								alchemyChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
								alchemyChckbox.setBackground(new Color(255, 255, 204));
								alchemyChckbox.setFocusPainted(false);
								alchemyChckbox.setActionCommand("annotator_alchemyAPI");
								panel4.add(alchemyChckbox, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
									GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
									new Insets(3, 0, 3, 0), 0, 0));
	
								//---- goldstandardChckbox ----
								goldstandardChckbox.setText("GS");
								goldstandardChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
								goldstandardChckbox.setBackground(new Color(255, 204, 153));
								goldstandardChckbox.setFocusPainted(false);
								goldstandardChckbox.setActionCommand("annotator_goldstandard");
								panel4.add(goldstandardChckbox, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
									GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
									new Insets(0, 0, 3, 0), 0, 0));
	
								//---- extractivChckbox ----
								extractivChckbox.setText("Extractiv");
								extractivChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
								extractivChckbox.setBackground(new Color(204, 255, 255));
								extractivChckbox.setFocusPainted(false);
								extractivChckbox.setActionCommand("annotator_extractiv");
								panel4.add(extractivChckbox, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 3, 0), 0, 0));
	
								//---- wrChckbox ----
								wrChckbox.setText("WR");
								wrChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
								wrChckbox.setBackground(new Color(255, 153, 153));
								wrChckbox.setFocusPainted(false);
								wrChckbox.setActionCommand("annotator_wr");
								panel4.add(wrChckbox, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 3, 0), 0, 0));
	
								//---- dbpediaChckbox ----
								dbpediaChckbox.setText("Spotlight");
								dbpediaChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
								dbpediaChckbox.setBackground(new Color(255, 204, 255));
								dbpediaChckbox.setFocusPainted(false);
								dbpediaChckbox.setActionCommand("annotator_dbpediaSpotlight");
								panel4.add(dbpediaChckbox, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 3, 0), 0, 0));
	
								//---- memmChckbox ----
								memmChckbox.setText("MEMM");
								memmChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
								memmChckbox.setBackground(new Color(204, 204, 255));
								memmChckbox.setFocusPainted(false);
								memmChckbox.setActionCommand("annotator_memm");
								panel4.add(memmChckbox, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 3, 0), 0, 0));
	
								//---- zemantaChckbox ----
								zemantaChckbox.setText("Zemanta");
								zemantaChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
								zemantaChckbox.setBackground(new Color(153, 255, 153));
								zemantaChckbox.setFocusPainted(false);
								zemantaChckbox.setActionCommand("annotator_zemanta");
								panel4.add(zemantaChckbox, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 3, 0), 0, 0));
	
								//---- opencalaisChckbox ----
								opencalaisChckbox.setText("OpenCalais");
								opencalaisChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
								opencalaisChckbox.setBackground(new Color(255, 204, 204));
								opencalaisChckbox.setFocusPainted(false);
								opencalaisChckbox.setActionCommand("annotator_openCalais");
								panel4.add(opencalaisChckbox, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 3, 0), 0, 0));
	
								//---- gateChckbox ----
								//gateChckbox.setText("Gate");
								//gateChckbox.setBackground(new Color(255, 153, 204));
								//gateChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
								//gateChckbox.setFocusPainted(false);
								//gateChckbox.setActionCommand("annotator_gate");
								//panel4.add(gateChckbox, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
									//GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									//new Insets(0, 0, 3, 0), 0, 0));
	
								//---- lupediaChckbox ----
								lupediaChckbox.setText("Lupedia");
								lupediaChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
								lupediaChckbox.setBackground(new Color(154, 205, 50));
								lupediaChckbox.setFocusPainted(false);
								lupediaChckbox.setActionCommand("annotator_lupedia");
								panel4.add(lupediaChckbox, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 3, 0), 0, 0));
	
								//---- saploChckbox ----
								saploChckbox.setText("Saplo");
								saploChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
								saploChckbox.setBackground(new Color(255, 215, 0));
								saploChckbox.setFocusPainted(false);
								saploChckbox.setActionCommand("annotator_saplo");
								panel4.add(saploChckbox, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 3, 0), 0, 0));
	
								//---- wikimetaChckbox ----
								wikimetaChckbox.setText("Wikimeta");
								wikimetaChckbox.setBackground(new Color(135, 206, 250));
								wikimetaChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
								wikimetaChckbox.setFocusPainted(false);
								wikimetaChckbox.setActionCommand("annotator_wikimeta");
								panel4.add(wikimetaChckbox, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 3, 0), 0, 0));
	
								//---- yahooChckbox ----
								yahooChckbox.setText("YahooYQL");
								yahooChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
								yahooChckbox.setBackground(new Color(143, 188, 143));
								yahooChckbox.setFocusPainted(false);
								yahooChckbox.setActionCommand("annotator_yahooAPI");
								panel4.add(yahooChckbox, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 3, 0), 0, 0));
	
								//---- stanfordChckbox ----
								stanfordChckbox.setText("StanfordNer");
								stanfordChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
								stanfordChckbox.setBackground(new Color(245, 222, 179));
								stanfordChckbox.setFocusPainted(false);
								stanfordChckbox.setActionCommand("annotator_stanfordNER");
								panel4.add(stanfordChckbox, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 3, 0), 0, 0));
	
								//---- illinoisChckbox ----
								illinoisChckbox.setText("IllinoisNer");
								illinoisChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
								illinoisChckbox.setBackground(new Color(221, 160, 221));
								illinoisChckbox.setFocusPainted(false);
								illinoisChckbox.setActionCommand("annotator_illinoisNER");
								panel4.add(illinoisChckbox, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 3, 0), 0, 0));
							}
							scrollPane1.setViewportView(panel4);
						}
						annotatorsPnl.add(scrollPane1, BorderLayout.CENTER);
					}
					splitPane3.setTopComponent(annotatorsPnl);
	
					//======== annotationSelPane ========
					{
						annotationSelPane.setBorder(new TitledBorder(null, "AnnotationSelector", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Arial", Font.BOLD, 12)));
						annotationSelPane.setLayout(new BorderLayout());
	
						//======== scrollPane5 ========
						{
	
							//---- legendTree ----
							legendTree.setRowHeight(20);
							legendTree.setFont(new Font("Arial", Font.PLAIN, 12));
							legendTree.setModel(new DefaultTreeModel(
								new DefaultMutableTreeNode("No Annotations Found")));
							legendTree.setVisibleRowCount(15);
							legendTree.setShowsRootHandles(true);
							scrollPane5.setViewportView(legendTree);
						}
						annotationSelPane.add(scrollPane5, BorderLayout.CENTER);
					}
					splitPane3.setBottomComponent(annotationSelPane);
				}
				splitPane5.setTopComponent(splitPane3);
				
				//======= segmentationPanel =======
				{
					segmentationPane.setBorder(new TitledBorder(null, "SegmentationSelector", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Arial", Font.BOLD, 12)));
					segmentationPane.setLayout(new BorderLayout());
					
					//======== panel5 ========
					{
						panel5.setLayout(new GridBagLayout());
						((GridBagLayout)panel5.getLayout()).columnWidths = new int[] {101, 0, 102, 0};
						((GridBagLayout)panel5.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
						((GridBagLayout)panel5.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
						((GridBagLayout)panel5.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

						//---- refinedLbl ----
						refinedSegLbl.setText("Refined:");
						refinedSegLbl.setFont(new Font("Arial", Font.PLAIN, 12));
						refinedSegLbl.setForeground(new Color(255, 153, 153));
						refinedSegLbl.setHorizontalAlignment(SwingConstants.CENTER);
						panel5.add(refinedSegLbl, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
							GridBagConstraints.CENTER, GridBagConstraints.BOTH,
							new Insets(0, 0, 0, 0), 0, 0));

						//---- separator2 ----
						separator2.setOrientation(SwingConstants.VERTICAL);
						panel5.add(separator2, new GridBagConstraints(1, 0, 1, 13, 0.0, 0.0,
							GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
							new Insets(0, 5, 0, 5), 0, 0));

						//---- originalLbl ----
						originalSegLbl.setText("Original:");
						originalSegLbl.setForeground(new Color(153, 153, 255));
						originalSegLbl.setFont(new Font("Arial", Font.PLAIN, 12));
						originalSegLbl.setHorizontalAlignment(SwingConstants.CENTER);
						panel5.add(originalSegLbl, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
							GridBagConstraints.CENTER, GridBagConstraints.BOTH,
							new Insets(0, 0, 0, 0), 0, 0));
						
						// ------- refined line check box ------
						{
							lineChckbox.setText("Lines");
							lineChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
							lineChckbox.setBackground(new Color(255, 204, 153));
							lineChckbox.setFocusPainted(false);
							lineChckbox.setActionCommand("segmentation_lines");
							panel5.add(lineChckbox, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
									GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
									new Insets(3, 0, 3, 0), 0, 0));
						}
						
						// ------- refined block check box -------
						{
							blockChckbox.setText("Blocks");
							blockChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
							blockChckbox.setBackground(new Color(204, 204, 255));
							blockChckbox.setFocusPainted(false);
							blockChckbox.setActionCommand("segmentation_blocks");
							panel5.add(blockChckbox, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 3, 0), 0, 0));
						}
						
						// ------- refined block group check box -------
						{
							blockGroupChckbox.setText("BlockGroups");
							blockGroupChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
							blockGroupChckbox.setBackground(new Color(255, 153, 153));
							blockGroupChckbox.setFocusPainted(false);
							blockGroupChckbox.setActionCommand("segmentation_blockGroups");
							panel5.add(blockGroupChckbox, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
									GridBagConstraints.CENTER, GridBagConstraints.BOTH,
									new Insets(0, 0, 3, 0), 0, 0));
						}
						
						
						//---- original line check box  ----
						{
							originalLineChckbox.setText("Line");
							originalLineChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
							originalLineChckbox.setBackground(new Color(210, 210, 210));
							originalLineChckbox.setFocusPainted(false);
							originalLineChckbox.setActionCommand("originalSegmentation_lines");
							panel5.add(originalLineChckbox, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
								GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
								new Insets(0, 0, 3, 0), 0, 0));
						}
						
						
						//---- original block check box ----
						{
							originalBlockChckbox.setText("Block");
							originalBlockChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
							originalBlockChckbox.setBackground(new Color(180, 180, 180));
							originalBlockChckbox.setFocusPainted(false);
							originalBlockChckbox.setActionCommand("originalSegmentation_blocks");
							panel5.add(originalBlockChckbox, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
								GridBagConstraints.CENTER, GridBagConstraints.BOTH,
								new Insets(0, 0, 3, 0), 0, 0));
						}
						
						//---- original blockgroup check box ----
						{
							originalBlockGroupChckbox.setText("BlockGroup");
							originalBlockGroupChckbox.setFont(new Font("Arial", Font.PLAIN, 12));
							originalBlockGroupChckbox.setBackground(new Color(120, 120, 120));
							originalBlockGroupChckbox.setFocusPainted(false);
							originalBlockGroupChckbox.setActionCommand("originalSegmentation_blockGroups");
							panel5.add(originalBlockGroupChckbox, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
								GridBagConstraints.CENTER, GridBagConstraints.BOTH,
								new Insets(0, 0, 3, 0), 0, 0));
						}
						
						panel5.setBackground(Color.white);
						scrollPane7.setViewportView(panel5);
						
					}
					segmentationPane.add(scrollPane7, BorderLayout.CENTER);
					segmentationPane.setVisible(false);
				}
				splitPane5.setBottomComponent(segmentationPane);
			}
			splitPane1.setRightComponent(splitPane5);
		}
		add(splitPane1, BorderLayout.CENTER);

		//---- label1 ----
		label1.setFont(new Font("Arial", Font.PLAIN, 12));
		label1.setForeground(Color.gray);
		add(label1, BorderLayout.NORTH);
		}

	private JSplitPane splitPane1;
	private JSplitPane splitPane4;
	public JPanel bottomPnl;
	private JScrollPane scrollPane2;
	public JTable conflictTbl;
	private JScrollPane scrollPane6;
	public JTextPane annotatedDocViewTxtpane;
	private JSplitPane splitPane3;
	public JPanel annotatorsPnl;
	private JScrollPane scrollPane1;
	private JPanel panel4;
	private JLabel baseAnLbl;
	private JSeparator separator1;
	private JLabel aggregatorLbl;
	public JCheckBox alchemyChckbox;
	public JCheckBox goldstandardChckbox;
	public JCheckBox extractivChckbox;
	public JCheckBox wrChckbox;
	public JCheckBox dbpediaChckbox;
	public JCheckBox memmChckbox;
	public JCheckBox zemantaChckbox;
	public JCheckBox opencalaisChckbox;
	//public JCheckBox gateChckbox;
	public JCheckBox lupediaChckbox;
	public JCheckBox saploChckbox;
	public JCheckBox wikimetaChckbox;
	public JCheckBox yahooChckbox;
	public JCheckBox stanfordChckbox;
	public JCheckBox illinoisChckbox;
	public JPanel annotationSelPane;
	private JScrollPane scrollPane5;
	public JTree legendTree;
	private JLabel label1;
	private JSplitPane splitPane5;
	private JPanel segmentationPane;
	public JCheckBox lineChckbox;
	public JCheckBox blockChckbox;
	public JCheckBox blockGroupChckbox;
	private JPanel panel5;
	private JScrollPane scrollPane7;
	private JLabel refinedSegLbl;
	private JSeparator separator2;
	private JLabel originalSegLbl;
	public JCheckBox originalLineChckbox;
	public JCheckBox originalBlockChckbox;
	public JCheckBox originalBlockGroupChckbox;
	private AnnotatedDocumentModel annoDocModel;
	
	public void visualizeSegmentationPane(){
		segmentationPane.setVisible(true);
	}

	public void setAnnoDocModel(AnnotatedDocumentModel annoDocModel) {
		this.annoDocModel = annoDocModel;
	}

	public AnnotatedDocumentModel getAnnoDocModel() {
		return annoDocModel;
	}
}
