/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.tree.ConfigurationNode;
import org.apache.commons.configuration.tree.xpath.XPathExpressionEngine;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.parallel.ParallelTextAnnotator;
import uk.ac.ox.cs.diadem.roseann.api.RoseAnnException;
import uk.ac.ox.cs.diadem.roseann.api.aggregation.AbstractAggregator;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.Conflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.LogicalConflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.annotation.OmissionConflict;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.AnnotatedDocumentModel;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.AnnotatedPDFModel;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.PdfDoc;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web.AnnotatedHTMLModel;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.web.HtmlDoc;
import uk.ac.ox.cs.diadem.roseann.gui.ROSeAnnGUI;
import uk.ac.ox.cs.diadem.roseann.gui.control.AbstractDocumentViewerBusiness.MODEL_TYPE;
import uk.ac.ox.cs.diadem.roseann.gui.control.BrowserVisualizationBusiness;
import uk.ac.ox.cs.diadem.roseann.gui.control.BusinessInterface;
import uk.ac.ox.cs.diadem.roseann.gui.control.DocumentViewerBusiness;
import uk.ac.ox.cs.diadem.roseann.gui.control.MainpageBusiness;
import uk.ac.ox.cs.diadem.roseann.gui.control.PDFBusiness;
import uk.ac.ox.cs.diadem.roseann.gui.forms.About;
import uk.ac.ox.cs.diadem.roseann.gui.forms.DocumentViewerPnl;
import uk.ac.ox.cs.diadem.roseann.gui.forms.MainPageNew;
import uk.ac.ox.cs.diadem.roseann.gui.swingComponent.pdfpane.PDFViewerPnl;
import uk.ac.ox.cs.diadem.roseann.gui.web.util.BrowserDecorator;
import uk.ac.ox.cs.diadem.roseann.ontology.MaterializedOntologyQuery;
import uk.ac.ox.cs.diadem.roseann.ontology.OntologyException;
import uk.ac.ox.cs.diadem.roseann.ontology.util.CreateMaterializedOntology;
import uk.ac.ox.cs.diadem.roseann.util.CompareAnnotationByStartEnd;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;

/**
 * 
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk), Department of Computer Science, Oxford University
 * 
 *         ROSeAnn main class, use this class to invoke all the functionalities of ROSeAnn. ROSeAnn reads all the
 *         parameters from two configuration files, both of them can be found at conf/ TextAnnotatorConfiguration.xml ->
 *         This file contains all the parameters regarding the individual annotators to be invoked by ROSeAnn. Specify
 *         here the key of the web service (when needed), the url of the web service and all of the other parameters.
 *         Specify in the annotator with name "parallel_annotator" which annotator class will be invoked by ROSeAnn (the
 *         class must implement the Annotator interface and define a method getInstance) and the timeout value. If an
 *         individual annotator takes longer than timeout, its process will be killed and its annotations will not be
 *         considered. Note: some of the parameters (like the key value) are mandatory, if not specified ROSeAnn will
 *         not be able to invoke the service and therefore the annotator won't be included Configuration.xml -> Specify
 *         here the folder of the materialized ontology file and the aggregators to be invoked. The materialized
 *         ontology folder is used by ROSeAnn to compute all the reasoning of the ontological hierarchy of the concepts
 *         to be annotated. This folder contains several files, such as subclass relationship files or disjoint
 *         relationship files. The default folder (with default files) can be found at
 *         src/main/resources/uk/ac/ox/cs/diadem/roseann/ontology/materializedOntology. You can specify your own
 *         materialized folder, however the files inside the folder must respect exactly the same name_conventions of
 *         the files in the default folder. Check the method createMaterializedOntologyFolder to see how to create a new
 *         materialized ontology folder from an ontology file. The aggregators pool can also be configured in the
 *         Configuration.xml file, here you can specify the name of the aggregator class to be invoked, this class must
 *         extend the abstract class {@link AbstractAggregator}
 * 
 *         Other than the methods to set the ROSeAnn parameters (like to set the timeout, individual annotators pool or
 *         aggregators pool), ROSeAnn includes three main methods: annotateEntityPlainText, annotateEntityWebPage and
 *         annotateEntityPDF. Use these methods to annotate a plain text string, an html web page or a pdf file. These
 *         methods returns an AnnotatedDocumentModel that can be queried to retrieve all the annotations found.
 *         Eventually, once an AnnotatedDocumentModel has been created, there are three different methods to graphically
 *         visualize the entity annotations retrieved over the document.
 * 
 *         ROSeAnn can be instantiated also specifying different configuration files.
 * 
 */
public class ROSeAnn {

  /**
   * @return the textAnnotatorConfigurationFile
   */
  public String getTextAnnotatorConfigurationFile() {
    return textAnnotatorConfigurationFile;
  }

  private final String textAnnotatorConfigurationFile;

  /**
   * Logger
   */
  static final Logger logger = LoggerFactory.getLogger(ROSeAnn.class);

  /**
   * The parallel annotator
   */
  private ParallelTextAnnotator annotator;

  /**
   * The set of aggregator to be used in the reconciliation process
   */
  private Set<AbstractAggregator> aggregators;

  /**
   * Constructor
   */
  public ROSeAnn() {
    ConfigurationFacility.getConfiguration();

    // read the configuration file
    XMLConfiguration conf = null;
    try {
      conf = new XMLConfiguration("conf/Configuration.xml");
    } catch (final ConfigurationException e) {
      logger.error("Couldn't read the configuration file located at {}", "conf/Configuration.xml", e);
      throw new AnnotatorException("Couldn't read the configuration file located at " + "conf/Configuration.xml",
          logger);
    }

    readConfigurationFile(conf);
    textAnnotatorConfigurationFile = "conf/TextannotatorConfiguration.xml";

    // this.annotator=(ParallelTextAnnotator)ParallelTextAnnotator.getInstance();
  }

  /**
   * Constructor to specify the configuration file to read
   * 
   * @param configuration
   *          ROSeAnn specific parameters configuration
   * @param text_annotator
   *          Individual annotators parameters configuration
   */
  public ROSeAnn(final XMLConfiguration configuration, final String textAnnotConfig) {
    ConfigurationFacility.getConfiguration();

    readConfigurationFile(configuration);

    initializeAnnotator();
    // set the parameters for the individual text annotators

    // initialize the configuration
    final AnnotatorConfiguration conf = new AnnotatorConfiguration();
    conf.initializeConfiguration(annotator.getAnnotatorName(), textAnnotConfig);
    annotator.setConfig(conf);

    textAnnotatorConfigurationFile = textAnnotConfig;

  }

  private void readConfigurationFile(final XMLConfiguration configuration) {

    // get the materialized ontology file folder
    configuration.setExpressionEngine(new XPathExpressionEngine());
    final String ontologyFolder = configuration
        .getString("nlp/annotator/textannotator/ontology/materialized_folder_path");

    if (ontologyFolder == null)
      throw new OntologyException("The materialized ontology folder must be specified in the"
          + " Configuration.xml file under the path nlp/annotator/textannotator/ontology/materialized_folder_path",
          logger);

    // check the ontology folder actually exists
    if (MaterializedOntologyQuery.class.getResourceAsStream(ontologyFolder) == null
        && !new File(ontologyFolder).exists())
      throw new OntologyException("The materialized ontology folder specified in the"
          + " Configuration.xml file under the path nlp/annotator/textannotator/ontology/materialized_folder_path "
          + "does not exist", logger);

    // set the ontologyFolder
    MaterializedOntologyQuery.setMaterializedOntologyFolderFilePath(ontologyFolder);

    // read the aggregator classes
    final Set<AbstractAggregator> aggregators = new HashSet<AbstractAggregator>();
    try {
      final HierarchicalConfiguration pr = configuration.configurationAt("nlp/reconcile/aggregator_pool");

      @SuppressWarnings("unchecked")
      final List<ConfigurationNode> nodeAggregators = pr.getRootNode().getChildren();
      for (final ConfigurationNode node : nodeAggregators) {
        final String name = node.getName();
        final String value = node.getValue().toString();

        if (!name.equals("aggregator")) {
          continue;
        }

        try {
          // try to instantiate the aggregator class
          final Class<?> clazz = Class.forName(value);
          final AbstractAggregator aggregator = (AbstractAggregator) clazz.newInstance();
          aggregators.add(aggregator);
          logger.info("Aggregator class '{}' succesufully added to the aggregators pool", value);

        } catch (final Exception e) {
          logger.warn("The aggregator class '{}' cannot be instantaied and will not be added "
              + "to the aggregator pool", value, e);
        }
      }
    } catch (final Exception e) {
      logger.warn("Not able to locate in the configuration file '{}' the aggregator classes names",
          configuration.getBasePath(), e);
    }
    this.aggregators = aggregators;

    if (aggregators.size() == 0) {
      logger.warn("The aggregators pool is empty at the moment");
    }
  }

  /**
   * Method to retrieve the entity annotations from an input plain text
   * 
   * @param text
   *          The text to be annotated
   * @param disambiguate
   *          If true, also the disambiguation algorithm will be run and the disambiguated annotations will be returned
   *          as well, along with the computed conflicts. If false, only the annotations from the individual annotators
   *          will be returned
   * @return An AnnotatedDocumentModel containing all the annotations found over the text
   */
  public AnnotatedDocumentModel annotateEntityPlainText(final String text, final boolean disambiguate) {

    initializeAnnotator();

    logger.info("Invoking ROSeAnn on plain text with disambiguate set to {}", disambiguate);
    AnnotatedDocumentModel modelToReturn = null;

    final Set<Annotation> entityAnnotations = annotator.annotateEntity(text);
    logger.info("Individual annotator invoked and entities retrieved");
    // get the annotations with global concept and unknown annotations
    final Pair<Set<Annotation>, Set<Annotation>> globalUnknown = modifyAnnotationGlobalConcept(entityAnnotations);
    // get the annotators involved
    final Set<String> annotators = this.getAnnotatorInvolved(entityAnnotations);

    final Set<Annotation> globalAnnotations = globalUnknown.getLeft();

    // get the conflicts
    final Set<Conflict> conflicts = calculateConflicts(globalAnnotations, annotators);

    // if there is no need to disambiguate, just return the entity annotations found and set to empty the set of
    // aggregators involved
    final Set<String> involvedAggregators = new HashSet<String>();
    if (!disambiguate) {
      modelToReturn = new AnnotatedDocumentModel(text, globalAnnotations, conflicts, globalUnknown.getRight(),
          annotators, involvedAggregators);

      logger.info("Disambiguation process disabled, only the original annotations returned");
      return modelToReturn;
    }

    logger.info("Disambiguation process enabled, starting the computation of reconciled annotations");
    // if there's the need to disambiguate, disambiguate and add the disambiguated conflicts and annotations
    // to the model

    // iterate over each disambiguation method and add the disambiguated annotations to the set of all the annotations
    final Set<Annotation> allDisambiguatedAnnotations = new HashSet<Annotation>();
    for (final AbstractAggregator aggregator : aggregators) {

      try {
        final Set<Annotation> actualDisambiguatedAnnotations = aggregator
            .reconcile(text, globalAnnotations, annotators);
        // add the aggregator to the set of involved aggregator
        if (actualDisambiguatedAnnotations.size() > 0) {
          involvedAggregators.add(aggregator.getAggregatorName());
          allDisambiguatedAnnotations.addAll(actualDisambiguatedAnnotations);
        }
      } catch (final Exception e) {
        logger.error("Error while disambiguating the annotations using the class {}", aggregator.getClass().getName(),
            e);
        // continue
      }
    }
    globalAnnotations.addAll(allDisambiguatedAnnotations);
    logger.info("Disambiguation methods invoked and reconciled annotations computed");

    // build the final model

    modelToReturn = new AnnotatedDocumentModel(text, globalAnnotations, conflicts, globalUnknown.getRight(),
        annotators, involvedAggregators);

    return modelToReturn;
  }

  /**
   * Method to retrieve the entity annotations from an input web page
   * 
   * @param pageUr
   *          The URI of the web page to annotate (it can be a local file or a remote resource)
   * @param disambiguate
   *          If true, also the disambiguation algorithm will be run and the disambiguated annotations will be returned
   *          as well, along with the computed conflicts. If false, only the annotations from the individual annotators
   *          will be returned
   * @param keepBrowserAlive
   *          The web engine uses a mozilla web browser to navigate the page to annotate. During the annotation process
   *          a small mozilla windows will be opene. If keepBrowserAlive is set to false, the windows will be closed
   *          after the annotation process. If the browser window is closed, then it will be impossible to uses those
   *          methods that require the navigation of the dom tree (like AnnotatedHTMLModel.getAnnotationNodeContained
   *          method). If you want to use such methods, the browser window must be kept alive and the keepBrowserAlive
   *          set to true. If the browser window is kept alive, make sure to close it after all the processing by using
   *          the method AnnotatedHTMLModel.shutDownBrowser, otherwise the browser window will stay open
   * @return An AnnotatedHTMLModel containing all the annotations found over the text and the object representing the
   *         web page
   * @throws IOException
   *           If there are any problems when extracting the textual content from the web page
   */
  public AnnotatedHTMLModel annotateEntityWebPage(final URI pageUri, final boolean disambiguate,
      final boolean keepBrowserAlive) throws IOException {
    logger.info("Retrieving the entity annotations from the web page '{}'", pageUri);

    // build the HTML object
    HtmlDoc document = null;
    try {
      document = new HtmlDoc(pageUri, keepBrowserAlive);
    } catch (final Exception e) {
      throw new RoseAnnException("Was not able to intialize the HTML web page, launch the browser and extract the "
          + "textual content", e, logger);
    }

    // try to get the textual content
    String text = null;
    try {
      text = document.getTextualContent();
    } catch (final Exception e) {
      throw new RoseAnnException("Was not able to get the textual content from the web page", e, logger);
    }

    // annotate the text
    final AnnotatedDocumentModel model = annotateEntityPlainText(text, disambiguate);

    // create the html annotated model
    final AnnotatedHTMLModel htmlModel = new AnnotatedHTMLModel(model.getKnownAnnotations(), model.getAllConflicts(),
        model.getUnknownAnnotation(), model.getCompletedAnnotators(), document, model.getCompletedAggregators());

    logger.info("Annotating the web page '{}' process done", pageUri);

    return htmlModel;
  }

  /**
   * Method to retrieve the entity annotations from an input pdf file
   * 
   * @param pdfFile
   *          The file pointing the pdf file to annotate. Only a local file is allowed and not a remote pdf file
   * @param disambiguate
   *          If true, also the disambiguation algorithm will be run and the disambiguated annotations will be returned
   *          as well, along with the computed conflicts. If false, only the annotations from the individual annotators
   *          will be returned
   * @return An AnnotatedPDFModel containing all the annotations found over the text and the object representing the pdf
   *         file
   */
  public AnnotatedPDFModel annotateEntityPDF(final File pdfFile, final boolean disambiguate) {
    logger.info("Retrieving the entity annotations from the pdf file '{}'", pdfFile.getAbsolutePath());

    // try to build the PDF object
    PdfDoc document = null;
    try {
      document = new PdfDoc(pdfFile);
    } catch (final IOException e) {
      throw new RoseAnnException("Was not able to intialize the PDF object, read and segment the pdf file", e, logger);
    }

    // get the textual content
    final String text = document.getTextualContent();

    // annotate the text
    final AnnotatedDocumentModel model = annotateEntityPlainText(text, disambiguate);

    // create the pdf annotated model
    final AnnotatedPDFModel pdfModel = new AnnotatedPDFModel(model.getKnownAnnotations(), model.getAllConflicts(),
        model.getUnknownAnnotation(), model.getCompletedAnnotators(), document, model.getCompletedAggregators());

    logger.info("Annotating the pdf file '{}' process done", pdfFile.getAbsolutePath());

    return pdfModel;
  }

  /*
   * The first set in the returned pair are the annotations with global concept, the second set is the annotations with
   * the original type that is not present in the ontology
   * 
   * @param annotations
   * 
   * @return
   */
  private Pair<Set<Annotation>, Set<Annotation>> modifyAnnotationGlobalConcept(final Set<Annotation> annotations) {

    final Set<Annotation> globalAnnotation = new HashSet<Annotation>();
    final Set<Annotation> localAnnotation = new HashSet<Annotation>();
    for (final Annotation annotation : annotations) {
      final String type = annotation.getConcept();
      String annotator = annotation.getOriginAnnotator();

      // in the case the annotation source is dbpedia_ontology, the annotators become lupedia
      if (annotation.getAnnotationSource() != null && annotation.getAnnotationSource().equals("dbpedia_ontology")) {
        annotator = "lupedia";
      }
      final String globalConcept = MaterializedOntologyQuery.getGlobalType(type, annotator);
      if (globalConcept == null) {
        logger.warn(
            "The concept {} from the annotator {} " + "is unknown in the ontology and will not be disambiguate", type,
            annotator);
        localAnnotation.add(annotation);
      } else {
        annotation.setConcept(globalConcept);
        globalAnnotation.add(annotation);
      }
    }
    return Pair.of(globalAnnotation, localAnnotation);

  }

  /*
   * Get the annotators involved in the annotation process
   */
  private Set<String> getAnnotatorInvolved(final Set<Annotation> annotations) {
    final Set<String> annotators = new HashSet<String>();
    for (final Annotation annotation : annotations) {
      final String annotator = annotation.getOriginAnnotator();
      if (annotator != null) {
        annotators.add(annotator);
      }
    }
    return annotators;
  }

  /**
   * Method to calculate logical and omission conflicts for a given set of annotation. The method calculate logical and
   * omission conflict and return a set containing those conflicts
   * 
   * @param annotations
   *          The set of annotation
   * @param annotatorsInvolved
   *          The set of involved annotator's name
   * @return The set of both logical and omission conflicts
   */
  public Set<Conflict> calculateConflicts(final Set<Annotation> annotations, final Set<String> annotatorsInvolved) {

    final Set<Conflict> conflicts = new HashSet<Conflict>();

    if (annotations == null || annotations.size() == 0)
      return conflicts;

    final Set<Annotation> orderedAnnotations = new TreeSet<Annotation>(new CompareAnnotationByStartEnd());
    orderedAnnotations.addAll(annotations);

    long start = orderedAnnotations.iterator().next().getStart();
    long end = orderedAnnotations.iterator().next().getEnd();
    Set<Annotation> annotatedSpan = new HashSet<Annotation>();

    int omissionConflictProgressive = 0;
    int logicalConflictProgressive = 0;
    // iterate over all the annotations, calculate omission and logical span and add the information to the input model
    for (final Annotation annotation : orderedAnnotations) {

      // skip the annotation if not found by one of the original annotators
      if (!annotatorsInvolved.contains(annotation.getOriginAnnotator())) {
        continue;
      }

      final long actualStart = annotation.getStart();
      final long actulEnd = annotation.getEnd();

      if (actualStart != start || actulEnd != end) {

        // calculate soft and logical conflict and increade the progressive count
        final Set<Conflict> omissionConflict = calculateOmissionConflict(annotatedSpan, annotatorsInvolved,
            omissionConflictProgressive);
        conflicts.addAll(omissionConflict);
        omissionConflictProgressive += omissionConflict.size();
        final Set<Conflict> logicalConflict = calculateLogicalConflict(annotatedSpan, logicalConflictProgressive);
        conflicts.addAll(logicalConflict);
        logicalConflictProgressive += logicalConflict.size();

        annotatedSpan = new HashSet<Annotation>();
        annotatedSpan.add(annotation);
        start = actualStart;
        end = actulEnd;

      } else {
        annotatedSpan.add(annotation);
      }
    }

    // calculate the conflict for the last span as well
    final Set<Conflict> omissionConflict = calculateOmissionConflict(annotatedSpan, annotatorsInvolved,
        omissionConflictProgressive);
    conflicts.addAll(omissionConflict);
    omissionConflictProgressive += omissionConflict.size();
    final Set<Conflict> logicalConflict = calculateLogicalConflict(annotatedSpan, logicalConflictProgressive);
    conflicts.addAll(logicalConflict);
    logicalConflictProgressive += logicalConflict.size();

    return conflicts;

  }

  /**
   * Given a set of annotations with same start-end, calculate the omission conflicts among those annotations
   * 
   * @param annotatedSpan
   * @param involvedAnnotators
   * @param progressiveId
   * @return
   */
  private Set<Conflict> calculateOmissionConflict(final Set<Annotation> annotatedSpan,
      final Set<String> involvedAnnotators, final int progressiveId) {

    final Set<Conflict> omissionConflicts = new HashSet<Conflict>();
    if (annotatedSpan.size() == 0)
      return omissionConflicts;

    final long start = annotatedSpan.iterator().next().getStart();
    final long end = annotatedSpan.iterator().next().getEnd();

    final Set<String> consideredConcept = new HashSet<String>();

    int count = 0;
    // calculate the omission conflicts
    for (final Annotation annotation : annotatedSpan) {

      // the concept C
      final String concept = annotation.getConcept();
      if (consideredConcept.contains(concept)) {
        continue;
      }
      consideredConcept.add(concept);

      final Set<Annotation> agreedAnnotation = new HashSet<Annotation>();
      final Set<String> agreedAnnotators = new HashSet<String>();

      // get all the annotations that annotated C or a subclass of C
      for (final Annotation otherAnnotation : annotatedSpan) {
        final String otherConcept = otherAnnotation.getConcept();

        // get the superclass of the concept
        final Set<String> superclasses = MaterializedOntologyQuery.getAllSupertypes(otherConcept);
        if (superclasses == null) {
          logger.warn("Could not retrieve the superclasses for the type {}", otherConcept);
          continue;
        }
        // if the annotation annotated the span with a subclass of the concept, then agreed with it
        if (superclasses.contains(concept)) {
          agreedAnnotation.add(annotation);
          agreedAnnotators.add(annotation.getOriginAnnotator());
        }
      }

      // get all the annotator which knows C or a superclass of C, remove the agreed annotators
      final Set<String> omittedAnnotators = MaterializedOntologyQuery.getSuperclassKnowledge(concept);
      if (omittedAnnotators == null) {
        logger.warn("Could not retrieve the annotator which know a superclass of the type {}", concept);
        continue;
      }
      omittedAnnotators.retainAll(involvedAnnotators);
      omittedAnnotators.removeAll(agreedAnnotators);

      final Set<String> conceptSuperclasses = MaterializedOntologyQuery.getAllSupertypes(concept);
      // for each annotator left that knows a superclass of C, check whether it annotated with the superclass of C
      for (final Annotation otherAnnotation : annotatedSpan) {
        final String originAnnotator = otherAnnotation.getOriginAnnotator();
        if (omittedAnnotators.contains(originAnnotator) && conceptSuperclasses.contains(otherAnnotation.getConcept())) {
          // if a left annotator has annotated the span with a superclass of C, remove it from the omitted_annotators
          omittedAnnotators.remove(originAnnotator);
          agreedAnnotators.add(originAnnotator);
          agreedAnnotation.add(otherAnnotation);
        }

      }

      // if there is one or more annotators which know a superclass of C and didn't annotate with it, create a conflict
      if (omittedAnnotators.size() > 0) {
        final Conflict omissionConflict = new OmissionConflict("omission_conflict_" + (progressiveId + count), start,
            end, agreedAnnotation, agreedAnnotators, omittedAnnotators, concept);
        omissionConflicts.add(omissionConflict);
        count++;
      }

    }
    return omissionConflicts;

  }

  /**
   * Given a set of annotations with same start-end, calculate the logical conflicts among those annotations
   * 
   * @param annotatedSpan
   * @param involved_annotators
   * @param progressiveId
   * @return
   */
  private Set<Conflict> calculateLogicalConflict(final Set<Annotation> annotatedSpan, final int progressiveId) {

    final Set<Conflict> conflicts = new HashSet<Conflict>();
    if (annotatedSpan.size() == 0)
      return conflicts;

    final Set<String> consideredAnnotation = new HashSet<String>();

    final long start = annotatedSpan.iterator().next().getStart();
    final long end = annotatedSpan.iterator().next().getEnd();

    int count = 0;
    for (final Annotation annotation : annotatedSpan) {
      final String annotationId = annotation.getId();
      consideredAnnotation.add(annotationId);

      final String type = annotation.getConcept();

      // check the remaining annotations and see if there's a logical conflict, that is an annotation which is disjoint
      // with the current one
      for (final Annotation nextAnnotation : annotatedSpan) {

        final String nextId = nextAnnotation.getId();

        // if the annotation has already been visited, skip it
        if (consideredAnnotation.contains(nextId)) {
          continue;
        }

        final String nextType = nextAnnotation.getConcept();

        // check if they are in conflict
        final Boolean areDisjoint = MaterializedOntologyQuery.areDisjoint(type, nextType);
        if (areDisjoint == null) {
          logger.warn("Could not retrieve determine whether type {} and type {} are disoint", type, nextType);
          continue;
        }
        if (areDisjoint) {
          final Set<Annotation> involvedAnnotations = new HashSet<Annotation>();
          involvedAnnotations.add(annotation);
          involvedAnnotations.add(nextAnnotation);
          final Conflict logicalConflict = new LogicalConflict("logical_conflict_" + (progressiveId + count), start,
              end, involvedAnnotations);
          conflicts.add(logicalConflict);
          count++;
        }

      }

    }

    return conflicts;

  }

  /**
   * Get the annotators invoked by ROSeAnn
   * 
   * @return The set of annotators to be invoked by ROSeAnn
   */
  public Set<Annotator> getAnnotatorInvolved() {
    initializeAnnotator();
    return annotator.getAnnotatorPool();
  }

  /**
   * Method to set the annotators to be invoked by ROSeAnn
   * 
   * @param annotators
   */
  public void setAnnotators(final Set<Annotator> annotators) {
    initializeAnnotator();
    annotator.setAnnotatorPool(annotators);
  }

  /**
   * Method to set the aggregators to be invoked by ROSeAnn
   * 
   * @param aggregators
   */
  public void setAggregators(final Set<AbstractAggregator> aggregators) {
    this.aggregators = aggregators;
  }

  /**
   * Method to get the aggregators to be invoked by ROSeAnn
   * 
   * @return The set of aggregators to be invoked by ROSeAnn
   */
  public Set<AbstractAggregator> getAggregators() {
    return aggregators;
  }

  /**
   * Method to set the ROSeAnn timeout. If an individual annotator takes longer than timeout value, then the annotator
   * is killed and ROSeAnn does not consider its annotations
   * 
   * @param timeout
   *          The value in milliseconds of the timeout
   */
  public void setTimeout(final int timeout) {
    initializeAnnotator();
    annotator.setTimeout(timeout);
  }

  /**
   * Method to set the folder where to find the materialized ontology file
   * 
   * @see uk.ac.ox.cs.diadem.roseann.ontology.MaterializedOntologyQuery
   * @param ontologyFolder
   *          The folder containing the materialized files
   */
  public void setMaterializedOntologyFolder(final File ontologyFolder) {
    MaterializedOntologyQuery.setOntologyMaterializedFolder(ontologyFolder);
  }

  /**
   * Method that given an ontology file (local or remote), create the materialized ontology folder and set that folder
   * to be the materialized ontology folder used by ROSeAnn Note that in order to create the ontology, in the
   * TextAnnotatorConfiguration file you must specify an element called ontology_prefix for each annotator where it's
   * specified the prefix used by that annotator in the ontology file. These prefixes will be read and used for the
   * creation of the materialized ontology file. Also the namespace of the ontology file must be exactly the same of the
   * one used as a prefix for the global concepts in the ontology file WARNING: the ontology must define a prefix called
   * global that is the prefix used to represent the global concepts of the ontology. If this is not define, an
   * exception will be thrown
   * 
   * @see uk.ac.ox.cs.diadem.roseann.ontology.util.CreateMaterializedOntology
   * @param folder
   *          The folder file where the materialized files will be created
   * @param ontologyFile
   *          The URI of the ontology file (can be either local or remote)
   * @throws IOException
   *           If an exceptions is thrown during the creation or reading of the file
   * @throws OntologyException
   *           If the input ontology does not define a namespace prefix called global
   */
  public void createMaterializedOntologyFolder(final File folder, final URI ontologyFile) throws IOException {

    final Map<String, String> annotator2prefixes = new HashMap<String, String>();
    // read from the text annotator configuration file the annotator ontology prefixes
    // read the key
    XMLConfiguration conf = null;
    try {
      conf = new XMLConfiguration(textAnnotatorConfigurationFile);
    } catch (final ConfigurationException e) {
      logger.error("Couldn't read the text annotator configuration file located at {}", textAnnotatorConfigurationFile,
          e);
      return;
    }

    conf.setExpressionEngine(new XPathExpressionEngine());

    try {
      final HierarchicalConfiguration pr = conf.configurationAt("nlp/textannotator");
      @SuppressWarnings("unchecked")
      final List<ConfigurationNode> annotators = pr.getRootNode().getChildren();
      for (final ConfigurationNode annotator : annotators) {
        try {
          final String annotatorName = ((ConfigurationNode) annotator.getAttributes("name").iterator().next())
              .getValue().toString();
          if (!annotator.getName().equals("annotator")) {
            continue;
          }

          final ConfigurationNode prefix = (ConfigurationNode) annotator.getChildren("ontology_prefix").iterator()
              .next();
          final String value = prefix.getValue().toString();

          if (value != null && value.length() > 0) {
            annotator2prefixes.put(annotatorName, value);
          }
        } catch (final Exception e) {
          logger.info("Unable to read the prefix value from the xml configuration node {}", annotator.getName());
        }

      }
    } catch (final Exception e) {
      logger.warn(
          "Not able to read in the configuration text annotator configuration file " + "{} textannotator nodes",
          textAnnotatorConfigurationFile, e);
    }

    if (annotator2prefixes.size() == 0) {
      logger
          .warn(
              "Not able to read the annotator name and prefixes from the text annotator configuration file {}."
                  + " This file must contain, for each annotator, an xml element nlp/textannotator/annotator with an attribute name (the name"
                  + " of the annotator) and a child element ontology_prefix, containing the prefix used in the ontology",
              textAnnotatorConfigurationFile);
      return;
    }

    CreateMaterializedOntology.createMaterializedOntologyFiles(folder, annotator2prefixes, ontologyFile);
    setMaterializedOntologyFolder(folder);
  }

  /**
   * An util method to launch the visualization GUI for a given annotated text
   * 
   * @param model
   *          the object representing the annotated text
   */
  private static void visualizeAnnotatedText(final AnnotatedDocumentModel model) {
    if (model == null)
      return;
    // init a documentViewPanel to display the annotated doc
    final DocumentViewerPnl newPanel = new DocumentViewerPnl();
    if (ROSeAnnGUI.myMainPanel.documentTabPane == null) {
      System.out.println("is null");
    }
    ROSeAnnGUI.myMainPanel.documentTabPane.addTab("Text" + ROSeAnnGUI.myMainPanel.documentTabPane.getTabCount(),
        new ImageIcon(MainPageNew.class.getResource("icons/sourceEditor.gif")), newPanel);
    ROSeAnnGUI.myMainPanel.documentTabPane.setSelectedIndex(ROSeAnnGUI.myMainPanel.documentTabPane.getTabCount() - 1);

    // bind the business instance with the source text
    // and GUI form
    newPanel.setAnnoDocModel(model);
    final BusinessInterface docViewBus = new DocumentViewerBusiness(newPanel, MODEL_TYPE.TextModel);

    docViewBus.initialize();

  }

  /**
   * An utility method to initialize the GUI
   */
  private static void initGUI() {
    try {
      UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
      // nothing need to be done
    }
    javax.swing.ToolTipManager.sharedInstance().setDismissDelay(100000);
    // javax.swing.SwingUtilities.invokeLater(new Runnable() {
    // @Override
    // public void run() {
    ROSeAnnGUI.myMainPanel = new MainPageNew();
    ROSeAnnGUI.myMainPanel.setPreferredSize(new Dimension(1000, 800));
    // myMainPanel.annotatedDocViewTxtpnl.setPreferredSize(new Dimension(400,300));
    final BusinessInterface mainBusiness = new MainpageBusiness(ROSeAnnGUI.myMainPanel);
    // further decorate
    mainBusiness.initialize();

    final JFrame frame = new JFrame("ROSeAnn: Reconciling Opinions of Semantic Annotators");
    frame.setIconImage(new ImageIcon(About.class.getResource("icons/roseann-logo.png")).getImage());

    frame.getContentPane().add(new JScrollPane(ROSeAnnGUI.myMainPanel), BorderLayout.CENTER);
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    frame.pack();

    // Center the frame

    final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    final Dimension frameSize = frame.getSize();
    if (frameSize.height > screenSize.height) {
      frameSize.height = screenSize.height;
    }
    if (frameSize.width > screenSize.width) {
      frameSize.width = screenSize.width;
    }
    frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
    frame.setVisible(true);
  }//
   // });

  public static void visualizeAnnotatedDocument(final AnnotatedDocumentModel model) {
    if (ROSeAnnGUI.myMainPanel == null) {
      initGUI();
    }
    if (model instanceof AnnotatedHTMLModel) {
      visualizeAnnotatedHTML((AnnotatedHTMLModel) model);
    } else if (model instanceof AnnotatedPDFModel) {
      visualizeAnnotatedPDF((AnnotatedPDFModel) model);
    } else {
      visualizeAnnotatedText(model);
    }
  }

  /**
   * An util method to launch the visualization GUI for a given annotated HTML
   * 
   * @param model
   *          the object representing the annotated HTML document
   */
  private static void visualizeAnnotatedHTML(final AnnotatedHTMLModel model) {

    if (model == null)
      return;
    final WebBrowser wb = BrowserFactory.newWebBrowser(Engine.WEBDRIVER_FF, true);
    wb.setWindowSize(900, 700);

    final URI uri = model.getHtmlDoc().getPageUri();

    wb.navigate(uri.toString(), true);

    final BrowserDecorator decorator = new BrowserDecorator(wb, model);

    final DocumentViewerPnl newPanel = new DocumentViewerPnl();

    ROSeAnnGUI.myMainPanel.documentTabPane.addTab("Webpage" + ROSeAnnGUI.myMainPanel.documentTabPane.getTabCount(),
        new ImageIcon(MainPageNew.class.getResource("icons/internal_browser.gif")), newPanel);

    ROSeAnnGUI.myMainPanel.documentTabPane.setSelectedIndex(ROSeAnnGUI.myMainPanel.documentTabPane.getTabCount() - 1);

    newPanel.setAnnoDocModel(model);
    final BrowserVisualizationBusiness docViewBus = new BrowserVisualizationBusiness(newPanel, decorator,
        MODEL_TYPE.BrowserModel);

    docViewBus.initialize();

  }

  /**
   * An util method to launch the visualization GUI for a given annotated PDF
   * 
   * @param model
   *          the object representing the annotated PDF
   */
  private static void visualizeAnnotatedPDF(final AnnotatedPDFModel model) {
    if (model == null)
      return;
    final DocumentViewerPnl newPanel = new DocumentViewerPnl();
    newPanel.visualizeSegmentationPane();
    File tempPdfFile;
    try {
      tempPdfFile = File.createTempFile(FilenameUtils.getName(model.getPdfDoc().getPdfFile().getAbsolutePath()), null);

      FileUtils.copyFile(model.getPdfDoc().getPdfFile(), tempPdfFile);
      final PDFViewerPnl pdfPanel = new PDFViewerPnl(tempPdfFile);

      ROSeAnnGUI.myMainPanel.documentTabPane.addTab("PDF" + ROSeAnnGUI.myMainPanel.documentTabPane.getTabCount(),
          new ImageIcon(MainPageNew.class.getResource("icons/pdf_icon.png")), newPanel);
      ROSeAnnGUI.myMainPanel.documentTabPane.setSelectedIndex(ROSeAnnGUI.myMainPanel.documentTabPane.getTabCount() - 1);

      newPanel.setAnnoDocModel(model);
      final PDFBusiness pdfBus = new PDFBusiness(newPanel, pdfPanel, tempPdfFile, model.getPdfDoc().getPdfFile(),
          tempPdfFile.getAbsolutePath(), MODEL_TYPE.PDFModel);
      pdfBus.initialize();
    } catch (final Exception e) {
      // do nothing
      e.printStackTrace();
    }

  }

  private void initializeAnnotator() {
    if (annotator == null) {
      annotator = (ParallelTextAnnotator) ParallelTextAnnotator.getInstance();
    }
  }

  /**
   * Get the timeout value used in ROSeAnn. Return null if it has not been initialized yet
   * 
   * @return
   */
  public Integer getTimeout() {
    if (annotator == null)
      return null;
    return (int) annotator.getTimeout();
  }

  /**
   * Get the annotators in the annotator pool, returnning empty set if fails to access the pool
   * 
   * @return
   */
  public Set<String> getAnnotators() {
    initializeAnnotator();
    if (annotator == null || annotator.getAnnotatorPool() == null)
      return new HashSet<String>();
    final Set<Annotator> pool = annotator.getAnnotatorPool();
    final Set<String> annotators = new HashSet<String>();
    for (final Annotator oneAnnotator : pool) {
      annotators.add(oneAnnotator.getAnnotatorName());
    }
    return annotators;
  }

}
