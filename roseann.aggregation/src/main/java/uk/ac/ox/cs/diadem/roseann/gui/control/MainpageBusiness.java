/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.roseann.gui.control;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.apache.commons.configuration.XMLConfiguration;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.roseann.ROSeAnn;
import uk.ac.ox.cs.diadem.roseann.api.datamodel.document.AnnotatedDocumentModel;
import uk.ac.ox.cs.diadem.roseann.gui.forms.About;
import uk.ac.ox.cs.diadem.roseann.gui.forms.DocumentOpenPnl;
import uk.ac.ox.cs.diadem.roseann.gui.forms.DocumentViewerPnl;
import uk.ac.ox.cs.diadem.roseann.gui.forms.MainPageNew;
import uk.ac.ox.cs.diadem.roseann.gui.swing.util.Console;
import uk.ac.ox.cs.diadem.roseann.gui.toolkit.xmlViewer.XmlTextPane;
import uk.ac.ox.cs.diadem.roseann.util.Constant;
import uk.ac.ox.cs.diadem.roseann.util.FileAdapter;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;

/**
 * Main class which deals with the main event business of the components in the main page.
 * 
 * @author Luying Chen
 * 
 */
public class MainpageBusiness implements BusinessInterface, ActionListener {
	private final MainPageNew entryPage;

	private final static Logger LOGGER = LoggerFactory.getLogger(MainpageBusiness.class);

	private boolean enableBrowser = false;

	private WebBrowser wb;

	private static ROSeAnn roseAnnInstance;

	private final static String DEFAULT_CONF = "conf/Configuration.xml";

	public MainpageBusiness(final MainPageNew mainPnl) {
		ConfigurationFacility.getConfiguration();

		try{
			XMLConfiguration conf = new XMLConfiguration(new File(DEFAULT_CONF));

			// check if browser is enabled
			if (conf.containsKey("nlp.visualization.enable_browser")) {
				final String enBrowser = conf.getString("nlp.visualization.enable_browser");
				if (enBrowser.equals("true")) {
					enableBrowser = true;
				}

			}
		}
		catch(Exception e){
			LOGGER.warn("Not able to read the configuration file {}",DEFAULT_CONF);
		}

		roseAnnInstance = new ROSeAnn();
		entryPage = mainPnl;
		wb = null;
		registerHooker();
	}

	private void furtherDecorate() throws IOException {
		// decorate corpus tree
		decorateCorpusTree();
		decorateWebCorpusTree();
		decoratePdfTree();
		// install console
		decorateConsole();
		Console.println("Welcome to ROSeAnn GUI");

	}

	private void decorateConsole() throws IOException {
		final JTabbedPane consoleTabPane = new JTabbedPane();
		consoleTabPane.insertTab("Console", new ImageIcon(MainPageNew.class.getResource("icons/console_view.gif")),
				Console.init(), null, 0);

		entryPage.consolePnl.add(consoleTabPane, BorderLayout.CENTER);

	}

	// to add pdf documents sample to be visualized
	// each document must be inside the PDF_DOC_ROOT directory in the resources
	private void decoratePdfTree() {
		final File rootFile = new File(MainpageBusiness.class.getResource(Constant.PDF_DOCS_ROOT).getPath());
		if (!rootFile.exists())
			return;
		final DefaultMutableTreeNode root = new DefaultMutableTreeNode(rootFile.getName());

		final File[] children = rootFile.listFiles();

		for (final File child : children) {
			if (!child.isDirectory()) {
				continue;
			}

			final DefaultMutableTreeNode node = new DefaultMutableTreeNode(child.getName());
			root.add(node);
		}

		entryPage.pdfTree.setModel(new DefaultTreeModel(root));

		entryPage.pdfTree.collapsePath(new TreePath(root.getPath()));
	}

	// to add documents sample to be visualized.
	// each document must be inside the WEB_PAGES_ROOT directory in the
	// resources
	private void decorateWebCorpusTree() {
		final File rootFile = new File(MainpageBusiness.class.getResource(Constant.WEB_PAGES_ROOT).getPath());
		if (!rootFile.exists())
			return;
		final DefaultMutableTreeNode root = new DefaultMutableTreeNode(rootFile.getName());

		final File[] children = rootFile.listFiles();

		for (final File child : children) {
			// web pages must be a directory
			if (!child.isDirectory()) {
				continue;
			}

			final DefaultMutableTreeNode node = new DefaultMutableTreeNode(child.getName());
			root.add(node);
		}
		entryPage.webpageTree.setModel(new DefaultTreeModel(root));
		entryPage.webpageTree.collapsePath(new TreePath(root.getPath()));

	}

	private void decorateCorpusTree() {
		// default corpus root
		final File corpusDict = new File(MainpageBusiness.class.getResource(Constant.STATIC_CORPUS_ROOT).getPath());
		if (!corpusDict.exists())
			return;
		final String name = corpusDict.getName();

		final DefaultMutableTreeNode root = new DefaultMutableTreeNode(name);
		final File[] docs = new File(corpusDict.getAbsolutePath()).listFiles();

		final TreeSet<File> orderedDocs = new TreeSet<File>(new OrderFileByName());
		orderedDocs.addAll(Arrays.asList(docs));

		constructTree(root, orderedDocs);
		entryPage.corpusTree.setModel(new DefaultTreeModel(root));
		entryPage.corpusTree.setRootVisible(false);

	}

	/**
	 * Utility method to construct corpus tree recursively
	 */
	private void constructTree(final DefaultMutableTreeNode root, final Set<File> docs) {
		for (final File oneDoc : docs) {
			if (oneDoc.isHidden()) {
				continue;
			}
			DefaultMutableTreeNode innerNode = null;
			if (oneDoc.isDirectory()) {// ignore the empty directory
				final File[] subDocs = oneDoc.listFiles();
				final TreeSet<File> subOrderedDocs = new TreeSet<File>(new OrderFileByName());
				subOrderedDocs.addAll(Arrays.asList(subDocs));
				if (subDocs.length == 0) {
					continue;
				} else {
					innerNode = new DefaultMutableTreeNode(oneDoc.getName());
					constructTree(innerNode, new TreeSet<File>(subOrderedDocs));
				}
			} else {
				innerNode = new DefaultMutableTreeNode(oneDoc.getName());
			}
			root.add(innerNode);

		}

	}

	@Override public void registerHooker() {
		// register hooker for text corpus tree
		entryPage.corpusTree.addMouseListener(new MouseAdapter() {
			@Override public void mousePressed(final MouseEvent e) {
				if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) {// double click
					// from text
					// corpus
					// tree
					// showMockAnnotatedDoc();
					TextLogic.visualizeStaticAnnotatedText(entryPage, e);

				}
			}

		});
		entryPage.documentTabPane.addMouseListener(new MouseAdapter() {
			@Override public void mousePressed(final MouseEvent e) {
				// if double click, then close the tab
				if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) {
					final int tabIndex = entryPage.documentTabPane.indexAtLocation(e.getX(), e.getY());
					if (tabIndex != -1) {
						entryPage.documentTabPane.removeTabAt(tabIndex);
					}
				}
			}
		});

		// register hooker for pdf corpus tree, load static pdf documents
		entryPage.pdfTree.addMouseListener(new MouseAdapter() {
			@Override public void mousePressed(final MouseEvent e) {
				if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) {
					PDFLogic.visualizeStaticPDFDocument(entryPage, e, LOGGER);
				}
			}
		});

		// register hooker for web corpus tree, load static web page
		entryPage.webpageTree.addMouseListener(new MouseAdapter() {
			@Override public void mousePressed(final MouseEvent e) {
				if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) {

					new Thread() {
						@Override public void run() {
							wb = WebLogic.visualizeStaticWebPage(LOGGER, wb, enableBrowser, entryPage, e);
						}
					}.start();
				}
			}

		});

		// register buttons
		entryPage.annotateBtn.addActionListener(this);
		entryPage.browseBtn.addActionListener(this);
		entryPage.corpusNewBtn.addActionListener(this);
		entryPage.corpusOpenBtn.addActionListener(this);
		entryPage.saveBtn.addActionListener(this);
		entryPage.saveAllBtn.addActionListener(this);
		entryPage.ontologyBtn.addActionListener(this);
		entryPage.configBtn.addActionListener(this);
		entryPage.menuOntology.addActionListener(this);
		entryPage.menuAnnotate.addActionListener(this);
		entryPage.menuAggregatorConfig.addActionListener(this);
		entryPage.menuGlobalConfig.addActionListener(this);
		entryPage.menuHelp.addActionListener(this);
		entryPage.menuWeb.addActionListener(this);
		entryPage.menuLoadText.addActionListener(this);
		entryPage.menuNewText.addActionListener(this);
		entryPage.annotatorBtn.addActionListener(this);
		entryPage.menuAnnotator.addActionListener(this);

	}

	@Override public void initialize() {
		try {
			furtherDecorate();
		} catch (final IOException e) {

			e.printStackTrace();
		}
	}

	/*
	 * Handle the event to open the browser and navigate over the web with the browser window and annotate that page
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override public void actionPerformed(final ActionEvent e) {

		// button event to navigate to a new website
		if (e.getSource() == entryPage.browseBtn) {
			handleBrowserButton();
		}

		// button event to open a text file
		else if (e.getSource() == entryPage.corpusOpenBtn) {
			TextLogic.openTextFile(entryPage);
		}
		// button event to create a new text file
		else if (e.getSource() == entryPage.corpusNewBtn) {
			TextLogic.newTextFile(entryPage);
		}
		// button event to display ontology file
		else if (e.getSource() == entryPage.ontologyBtn) {

			TextLogic.openOntologyFile(entryPage);
		}

		// button event to annotate the current web page and display the
		// annotation
		else if (e.getSource() == entryPage.annotateBtn) {
			handleAnnotateButton();
		}

		// button to save the current annotated document
		else if (e.getSource() == entryPage.saveBtn) {
			handleSaveEvent();

		}

		// button to save all the current annotated document
		else if (e.getSource() == entryPage.saveAllBtn) {
			handleSaveAllEvent();
		}

		else if (e.getSource() == entryPage.menuOntology) {
			TextLogic.openOntologyFile(entryPage);
		} else if (e.getSource() == entryPage.menuWeb) {
			handleBrowserButton();
		} else if (e.getSource() == entryPage.menuHelp) {
			final JFrame tobi = new About();
			tobi.setTitle("About ROSeAnn...");
			tobi.setVisible(true);
			tobi.pack();

			tobi.setSize(400, 500);
			tobi.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			Console.println("Visit http://diadem.cs.ox.ac.uk/roseann for more information");
		}
		// show annotator set in the pool
		else if (e.getSource() == entryPage.annotatorBtn || e.getSource() == entryPage.menuAnnotator) {

			// final DocumentOpenPnl docPnl = new DocumentOpenPnl(new XmlTextPane());
			String content = "List of Annotators to be invoked:\n\n";
			for (final String annotator : roseAnnInstance.getAnnotators()) {
				content += "- " + annotator + "\n";
			}
			JOptionPane.showMessageDialog(null, content);
		} else if (e.getSource() == entryPage.menuAnnotate) {
			handleAnnotateButton();
		} else if (e.getSource() == entryPage.menuNewText) {
			TextLogic.newTextFile(entryPage);
		} else if (e.getSource() == entryPage.menuLoadText) {
			TextLogic.openTextFile(entryPage);
		}
		// show the global configuration files
		else if (e.getSource() == entryPage.configBtn || e.getSource() == entryPage.menuGlobalConfig) {

			Console.println("Opening global configuration file...");
			final String configFile = roseAnnInstance.getTextAnnotatorConfigurationFile();
			loadConfigFile(configFile);
		}

		// show the aggregator configuration files
		else if (e.getSource() == entryPage.menuAggregatorConfig) {

			Console.println("Opening the default aggregator configuration file...");
			loadConfigFile(DEFAULT_CONF);
		}

	}

	// utility method to handle the event of loading configuration files.
	private void loadConfigFile(final String configFile) {
		if (configFile != null && new File(configFile).exists()) {
			final JTextPane xmlPane = new XmlTextPane();
			xmlPane.setEditable(false);
			final DocumentOpenPnl docPnl = new DocumentOpenPnl(xmlPane);
			final String content = FileAdapter.getPureFileContents(configFile);
			docPnl.docTxtpane.setText(content);
			entryPage.documentTabPane.addTab("ConfigurationFile",
					new ImageIcon(MainPageNew.class.getResource("icons/debugt_obj.gif")), docPnl);

			entryPage.documentTabPane.setSelectedIndex(entryPage.documentTabPane.getTabCount() - 1);
		} else {
			Console.print("Can not find the configuration file!");
		}

	}

	// utility method to handle save event which basically saves plain text or annotated document
	private void handleSaveEvent() {

		final int selectedTabIndex = entryPage.documentTabPane.getSelectedIndex();
		if (selectedTabIndex >= 0) {
			String previousFileName = entryPage.documentTabPane.getTitleAt(selectedTabIndex);

			AnnotatedDocumentModel annoDocModel = null;
			String text = null;
			final JComponent jp = (JComponent) entryPage.documentTabPane.getComponentAt(selectedTabIndex);
			if (jp instanceof DocumentOpenPnl) {// save plain file
				text = ((DocumentOpenPnl) jp).docTxtpane.getText();
				previousFileName += ".txt";
			}

			else if (jp instanceof DocumentViewerPnl) {

				annoDocModel = ((DocumentViewerPnl) jp).getAnnoDocModel();

				if (annoDocModel == null) {
					LOGGER.error("Sorry but the selected tab contains a text which has not been annotated, "
							+ "impossible to save!");
					Console.println("Sorry but the selected tab contains a text which has not been annotated, "
							+ "impossible to save!");
					return;

				}

				// get source text
				text = annoDocModel.getAnnotatedText();

				if (text == null || annoDocModel.getKnownAnnotations() == null) {
					LOGGER.error("Sorry but the selected tab contains a text which has not been annotated, "
							+ "impossible to save!");
					Console.println("Sorry but the selected tab contains a text which has not been annotated, "
							+ "impossible to save!");
					return;

				}
				previousFileName += ".xml";
			}

			final JFileChooser saveFile = new JFileChooser();
			saveFile.setSelectedFile(new File(previousFileName));

			final int returnVal = saveFile.showSaveDialog(entryPage);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				final String fileName = saveFile.getSelectedFile().getAbsolutePath();
				final File toSave = new File(fileName);
				if (toSave.exists()) {
					final int result = JOptionPane.showConfirmDialog(entryPage, "The file already exists,"
							+ " are you sure you want to overwrite it?", "File exists", JOptionPane.YES_NO_CANCEL_OPTION);

					if (result == JOptionPane.YES_OPTION) {
						toSave.delete();

						try {
							if (annoDocModel != null) {
								annoDocModel.save(toSave);
							} else {
								FileAdapter.writeToFile(toSave, text);
							}
						} catch (final Exception ex) {
							LOGGER.error("Sorry, not able to save the file due to {}", ex);
							Console.println("Sorry, not able to save the file due to " + ex.getMessage());
							return;
						}
					}

				}

				else {
					final int result = JOptionPane.showConfirmDialog(entryPage, "Are you sure to save the " + "file here?",
							"Save File", JOptionPane.YES_NO_CANCEL_OPTION);

					if (result == JOptionPane.YES_OPTION) {
						try {
							if (annoDocModel != null) {
								annoDocModel.save(toSave);
							} else {
								FileAdapter.writeToFile(toSave, text);
							}
						} catch (final Exception ex) {
							LOGGER.error("Sorry, not able to save the file due to {}", ex);
							return;
						}
					}
				}
			}
		}

	}

	// utility method to save all the open documents
	private void handleSaveAllEvent() {

		final int componentNumber = entryPage.documentTabPane.getComponentCount();

		if (componentNumber <= 0)
			return;

		final int result = JOptionPane.showConfirmDialog(entryPage, "Are you sure you want to save"
				+ " all the open files?", "Save all files", JOptionPane.YES_NO_CANCEL_OPTION);

		if (result == JOptionPane.YES_OPTION) {
			final JFileChooser saveFile = new JFileChooser();
			saveFile.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			saveFile.setDialogTitle("Save all the files");

			final int returnVal = saveFile.showSaveDialog(entryPage);

			if (returnVal == JFileChooser.APPROVE_OPTION) {

				for (int i = 0; i < componentNumber; i++) {
					final Component currentComponent = entryPage.documentTabPane.getComponentAt(i);
					String name = entryPage.documentTabPane.getTitleAt(i);
					if (name.contains("/")) {
						final String split[] = name.split("/");
						name = split[split.length - 1];
					}

					if (currentComponent instanceof DocumentOpenPnl) {
						name = name + ".txt";
						final String text = ((DocumentOpenPnl) currentComponent).docTxtpane.getText();
						if (text == null) {
							LOGGER.warn("Unable to save the file {} because the source text is null", name);
							Console.println("Unable to save the file " + name + " because the source text is null");
							continue;
						}

						final File previousFile = new File(saveFile.getSelectedFile().getAbsolutePath() + "/" + name);
						if (previousFile.exists()) {
							previousFile.delete();
						}
						FileAdapter.writeToFile(previousFile, text);

					}

					else if (currentComponent instanceof DocumentViewerPnl) {
						name = name += ".xml";
						final AnnotatedDocumentModel annoDocModel = ((DocumentViewerPnl) currentComponent).getAnnoDocModel();

						if (annoDocModel == null) {
							LOGGER.warn("Unable to save the file {} because the AnnotedDocumentModel is null", name);
							Console.println("Unable to save the file " + name + " because the AnnotedDocumentModel is null");
							continue;
						}

						final String text = annoDocModel.getAnnotatedText();
						if (annoDocModel == null || text == null) {
							LOGGER.warn("Unable to save the file {} because either the annotation model or"
									+ " the source text are null", name);
							Console.println("Unable to save the file " + name + " because either the annotation model or"
									+ " the source text are null");
							continue;
						}

						final File previousFile = new File(saveFile.getSelectedFile().getAbsolutePath() + "/" + name);
						if (previousFile.exists()) {
							previousFile.delete();
						}

						try {
							annoDocModel.save(previousFile);

						} catch (final Exception e1) {
							LOGGER.warn("Unable to save the file {}", previousFile.getAbsolutePath(), e1);
							Console.println("Unable to save the file " + previousFile.getAbsolutePath() + " due to "
									+ e1.getMessage());
						}

					}
				}
			}
		}

	}

	private void handleBrowserButton() {
		new Thread() {
			@Override public void run() {
				wb = WebLogic.openWebBrowser(LOGGER, wb, enableBrowser);
			}
		}.start();
	}

	private void handleAnnotateButton() {
		// create a pop-up dialog to inform the user that we are annotating the document
		final Frame ancestorFrame = (JFrame) SwingUtilities.getAncestorOfClass(JFrame.class, entryPage);
		final JDialog dialog = new JDialog(ancestorFrame, "", false);
		final JPanel messagePane = new JPanel();
		// messagePane.setBackground(Color.CYAN);

		final JLabel text = new JLabel("Annotating the document, please wait....");
		text.setIcon(new ImageIcon(MainPageNew.class.getResource("icons/big-flower.gif")));
		final Font font = new Font("Verdana", Font.ITALIC, 24);
		text.setFont(font);
		text.setOpaque(false);

		messagePane.add(text);

		final Point location = ancestorFrame.getLocation();
		final Dimension dimension = ancestorFrame.getSize();
		final Point dialogLocation = new Point();

		dialog.setSize(dimension.width / 2 + 30, dimension.height / 12);
		dialog.getContentPane().add(messagePane);
		dialog.setUndecorated(true);

		// set the location in the middle of the gui
		dialogLocation.x = location.x + dimension.width / 2 - dialog.getWidth() / 2;
		dialogLocation.y = location.y + dimension.height / 2 - dialog.getHeight() / 2;
		dialog.setLocation(dialogLocation);
		dialog.setVisible(true);

		new Thread() {
			@Override public void run() {
				try {
					boolean browserIsEnabled = true;
					// check if the browser is enabled and not has been closed
					if (enableBrowser && wb != null) {

						try {
							wb.getLocationURL();
							// if everything goes fine, annotate the web page
							WebLogic.annotateWebPage(LOGGER, wb, entryPage);

						}
						// if fail the browser window has been closed, therefore try to annotate the text panel
						catch (final UnreachableBrowserException ex) {
							browserIsEnabled = false;
						}

					} else {
						browserIsEnabled = false;
					}

					if (!browserIsEnabled) {

						// check that the text has not been annotated
						final int selectedTabIdx = entryPage.documentTabPane.getSelectedIndex();
						if (selectedTabIdx >= 0) {
							final JComponent jp = (JComponent) entryPage.documentTabPane.getComponentAt(selectedTabIdx);
							if (jp instanceof DocumentOpenPnl) {
								// in this case the text can be annotated
								TextLogic.annotateText(entryPage);
							} else {
								LOGGER.info("The selected text has already been annotated, you can already visualize the "
										+ "annotations!");
								Console.println("The selected text has already been annotated, you can already visualize the "
										+ "annotations!");
							}
						}
					}
				} catch (final Exception e) {
					Console.println("Sorry, unable to complete the annotation process due to the exception " + e.getMessage());
				} catch (final Error e) {
					e.printStackTrace();
					Console.println("Sorry, unable to complete the annotation process due to the error " + e.getMessage());
				} finally {
					dialog.dispose();
				}
			}
		}.start();
	}

	public static ROSeAnn getRoseAnnInstance() {
		if (roseAnnInstance == null) {
			roseAnnInstance = new ROSeAnn();
		}
		return roseAnnInstance;
	}

}

class OrderFileByName implements Comparator<File> {

	@Override public int compare(final File o1, final File o2) {
		// TODO Auto-generated method stub
		return o1.getName().compareTo(o2.getName());
	}

}
