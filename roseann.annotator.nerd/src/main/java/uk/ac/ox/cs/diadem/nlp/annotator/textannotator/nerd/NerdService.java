package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.nerd;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import argo.jdom.JdomParser;
import argo.jdom.JsonRootNode;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;

/**
 * 
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk) Oxford University,
 *         Department of Computer Science
 * 
 *         Use this class to send a request to Nerd web service and get a
 *         response as a string.
 * 
 */
class NerdService {

	static final Logger logger = LoggerFactory.getLogger(NerdService.class);

	/**
	 * Method to invoke the nerd service with the input text and the parameters
	 * If the input text is null, one among uri and timedtext parameters must be specified
	 * @param text
	 * 			Text to analyze
	 * @param parameters 
	 * 			The AnnotatorConfiguration object that contains the parameters for nerd service.
	 * 			If the extractor parameter has not been specified (or it has an unknown value), the default extractor 
	 * 			value is combined
	 * @return	The response from Nerd as a string according to the set format
	 * @throws IOException
	 * 			If something goes wrong during the invocation of the service
	 * @throws IllegalArgumentException
	 * 			If one of the mandatory parameter has not been specified or it has been badly specified
	 */
	public String invokeService(String text, final AnnotatorConfiguration parameters, int timeout) throws IOException,
	IllegalArgumentException {
		
		boolean there_is_timeout=true;
		if(timeout==0)
			there_is_timeout=false;
		URL endpoint=parameters.getURlendpoint();
		if(endpoint==null)
			throw new IllegalArgumentException("The endpoint url has not been specified");

		final String key=parameters.getApiKey();
		if(key==null)
			throw new IllegalArgumentException("The api key has not been specified");
		
		//check what kind of text we need to analyze
		String text_to_analyze=null;
		if(text!=null)
			text_to_analyze="text="+URLEncoder.encode(text,"UTF-8");
		if(text==null)
			if(parameters.getSpecificParameters().get("uri")!=null)
				text_to_analyze="uri="+URLEncoder.encode(parameters.getSpecificParameters().get("uri"),"UTF-8");
		if(text_to_analyze==null)
			if(parameters.getSpecificParameters().get("timedtext")!=null)
				text_to_analyze="timedtext="+URLEncoder.encode(parameters.getSpecificParameters().get("timedtext"),"UTF-8");
			else
				throw new IllegalArgumentException("If the text is null one between uri and timedtext must be" +
						" specified in the configuration file");
			
		//once the parameters have been specified we need to send 3 different http requests to the Nerd Service

		//first send the request with the document
		URL document_endpoint = new URL(endpoint.toString()+"document");

		String params="key="+key +
				"&"+text_to_analyze;
		
		long start = System.currentTimeMillis();
		String response=postRequest(params,document_endpoint,timeout);
		long end = System.currentTimeMillis();
		int time_waited = (int) (end-start);
		if(there_is_timeout)
			timeout=this.decreaseTimeout(timeout, time_waited);
		
		//parse the response and get the id of the document

		String idDocument = null;
		try{
			JsonRootNode document = new JdomParser().parse(response);

			idDocument=document.getNumberValue("idDocument");
		}
		catch(Exception e){
			throw new AnnotatorException("The response from the document Nerd api can't be parse" +
					" as a Json node or the element idDocument has not been found", e, logger);
		}

		//second send the request with the annotation
		URL annotation_endpoint = new URL(endpoint.toString()+"annotation");

		params="key="+key +
				"&idDocument="+idDocument;

		//get the extractor type, if not specified use combined
		String extractor=parameters.getSpecificParameters().get("extractor");
		if(extractor==null)
			extractor="combined";
		params+="&extractor="+extractor;

		String ontology=parameters.getSpecificParameters().get("ontology");
		if(ontology!=null)
			params+="&ontology="+ontology;

		String second_timeout=parameters.getSpecificParameters().get("timeout");
		if(second_timeout!=null)
			params+="&timeout="+second_timeout;
		
		start = System.currentTimeMillis();
		response=postRequest(params,annotation_endpoint,timeout);
		end = System.currentTimeMillis();
		time_waited = (int) (end-start);
		if(there_is_timeout)
			timeout=this.decreaseTimeout(timeout, time_waited);
		
		//parse the response and get the id of the annotations

		String idAnnotation = null;
		try{
			JsonRootNode document = new JdomParser().parse(response);

			idAnnotation=document.getNumberValue("idAnnotation");
		}
		catch(Exception e){
			throw new AnnotatorException("The response from the annotation Nerd api can't be parse" +
					" as a Json node or the element idAnnotation has not been found", e, logger);
		}

		//third send the request with the entities
		params="key="+key +
				"&idAnnotation="+idAnnotation;
		
		String granularity=parameters.getSpecificParameters().get("granularity");
		if(ontology==null)
			params+="&granularity="+granularity;
		
		URL entity_endpoint = new URL(endpoint.toString()+"entity?"+params);
		
		response=getRequest(entity_endpoint,timeout);
		

		return response;
	}

	/**
	 * Send the post request
	 * @param params
	 * @param endpoint
	 * @return
	 * @throws IOException
	 */
	private String postRequest(final String params, final URL endpoint,int timeout) throws IOException {

		HttpURLConnection handle = (HttpURLConnection) endpoint.openConnection();

		handle.setDoOutput(true);
		handle.setRequestMethod("POST");
		handle.setConnectTimeout(timeout);

		StringBuilder data = new StringBuilder();

		data.append(params);
		handle.addRequestProperty("Content-Length", Integer.toString(data.length()));

		DataOutputStream ostream = new DataOutputStream(handle.getOutputStream());
		ostream.write(data.toString().getBytes());
		ostream.close();

		// get the response.
		BufferedReader rd = new BufferedReader(new InputStreamReader(handle.getInputStream()));
		String line, result = "";

		while ((line = rd.readLine()) != null) {
			result += line;
		}
		handle.disconnect();

		return result;
	}

	/**
	 * Send GET request
	 * @param endpoint
	 * @return
	 * @throws IOException
	 */
	private String getRequest(final URL endpoint,int timeout) throws IOException {

		// connect and send the HTTP request.
		HttpURLConnection connection = (HttpURLConnection) endpoint.openConnection();
		//connection.setDoOutput(true);
		connection.setRequestMethod("GET");
		connection.setConnectTimeout(timeout);

		// get the response.
		BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String line, result = "";

		while ((line = rd.readLine()) != null) {
			result += line;
		}
		connection.disconnect();

		return result;
	}
	
	private int decreaseTimeout(int timeout,int decrease){
		int new_timeout=timeout-decrease;
		if(new_timeout<1)
			new_timeout=1;
		return new_timeout;
	}

}
