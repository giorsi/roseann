package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.nerd;

import java.net.SocketTimeoutException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.AnnotationAttribute;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;

/**
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk) Oxford University, Department of Computer Science 
 * The wrapper submit the text to Nerd service, take the response as String representing JSon Object, builds a JSon Object and
 * select only the entity annotations to be returned.
 */

public class NerdAnnotator extends AnnotatorAdapter {

	static final Logger logger = LoggerFactory.getLogger(NerdAnnotator.class);

	/**
	 * id counter to increment the annotation id
	 */
	private long id_counter;

	/**
	 * Singleton instance
	 */
	private static NerdAnnotator INSTANCE;

	/**
	 * The Nerd service endpoint
	 */
	private NerdService service;

	/**
	 * private constructor to build the instance
	 */
	private NerdAnnotator() {
		ConfigurationFacility.getConfiguration();
		setAnnotatorName("nerd");

		// start the id from 0
		id_counter = 0;

		// get the api keys
		AnnotatorConfiguration conf=new AnnotatorConfiguration();
		conf.initializeConfiguration(getAnnotatorName());
		this.setConfig(conf);

		this.service=new NerdService();
	}

	/**
	 * method to get the singleton instance
	 * @return singleton instance
	 */
	public static synchronized Annotator getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new NerdAnnotator();
		}
		return INSTANCE;
	}

	@Override
	/**
	 * If the input text is null, one among uri and timedtext parameters must be specified
	 */
	public Set<Annotation> annotateEntity(final String text) {

		final String response = this.submitToAnnotator(text);

		// save the annotations and increment the id counter
		final Set<Annotation> entityAnnotations=retrieveEntityAnnotations(response, id_counter);
		final int num_annotations=entityAnnotations.size();
		id_counter += num_annotations;
		return entityAnnotations;

	}

	@Override
	public Set<Annotation> annotateEntity(final String text,int timeout) {

		final String response = this.submitToAnnotator(text,timeout);
		if(response==null)
			return new HashSet<Annotation>();

		// save the annotations and increment the id counter
		final Set<Annotation> entityAnnotations=retrieveEntityAnnotations(response, id_counter);
		final int num_annotations=entityAnnotations.size();
		id_counter += num_annotations;
		return entityAnnotations;

	}

	@Override
	/**
	 * If the input text is null, one among uri and timedtext parameters must be specified
	 */
	public String submitToAnnotator(String text){
		return this.submitToAnnotator(text,0);
	}

	@Override
	public String submitToAnnotator(String text,int timeout){
		if (text == null || text.length() < 1) {
			logger.error("Trying to submit an empty text to Nerd service");
			throw new AnnotatorException("Trying to submit an empty text to Nerd service", logger);
		}

		String res = null;
		try {

			logger.debug("Submitting document to Nerd service.");

			// eliminate from the original text special characters
			text = text.replaceAll("\u0003", " ");

			res = service.invokeService(text,getConfig(),timeout);
			logger.debug("Nerd Process done and response received.");

			return res;
		}//check if the timeout has expired
		catch(final SocketTimeoutException e) {
			logger.warn("{} was not able to receive the web response within timeout {} milliseconds",
					getAnnotatorName(),timeout);
			return null;
		}
		catch (final Exception e) {
			logger.error("Error submitting text to Nerd");
			throw new AnnotatorException("Error submitting text to Nerd", e, logger);
		}
	}

	private Set<Annotation> retrieveEntityAnnotations(final String json_response, final long progressive){
		Set<Annotation> entityAnnotation=new HashSet<Annotation>();
		JsonRootNode doc;

		logger.debug("Processing the Nerd response.");
		try {
			// create a JSon node parsing the text response
			doc = new JdomParser().parse(json_response);

		} catch (final InvalidSyntaxException e) {
			logger.error("The String response from Nerd can't be parse as JSonNode");
			throw new AnnotatorException("The String response from Nerd " + "can't be parse as JSonNode", e,
					logger);
		}

		// select only the entity annotations
		final List<JsonNode> selected_annotations = doc.getElements();

		if (selected_annotations == null||selected_annotations.size()==0) {
			logger.debug("Nerd service did not find any entity annotations.");
			return entityAnnotation;
		}

		// to count the annotation
		int count = 0;

		for(JsonNode json_annotation:selected_annotations){
			//build the annotation object
			Annotation annotation = new EntityAnnotation();
			try{
				annotation.setAnnotationClass(AnnotationClass.INSTANCE);
				annotation.setConcept(json_annotation.getStringValue("nerdType").replace("http://nerd.eurecom.fr/ontology#", ""));
				annotation.setConfidence(Double.parseDouble(json_annotation.getNumberValue("confidence")));
				annotation.setAnnotationSource(json_annotation.getStringValue("extractor"));
				annotation.setStart(Long.parseLong(json_annotation.getNumberValue("startChar")));
				annotation.setEnd(Long.parseLong(json_annotation.getNumberValue("endChar")));
				annotation.setId(getAnnotatorName()+"_"+(progressive+count));
				annotation.setOriginAnnotator(getAnnotatorName());

				entityAnnotation.add(annotation);
				count++;

			}
			catch(Exception e){
				logger.error("Not able to convert the json object into an annotation object",e);
			}

			try{
				//try to retrieve the attribute
				//create the annotation attribute
				AnnotationAttribute attribute=new AnnotationAttribute("uri", json_annotation.getStringValue("uri"));
				annotation.addAttribute(attribute);
			}
			catch(Exception e){
				//if the attribute is not found, simply continue
			}
		}

		return entityAnnotation;
	}
	
	public static void main(String[] args){
		System.out.println(NerdAnnotator.getInstance().annotateEntity("Barack Obama is the new president of United States.",1));
	}

}
