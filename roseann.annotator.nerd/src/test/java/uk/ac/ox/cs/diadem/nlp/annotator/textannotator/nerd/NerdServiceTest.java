package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.nerd;

import java.util.List;
import java.io.IOException;
import java.net.URL;

import junit.framework.Assert;

import org.junit.Test;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;

public class NerdServiceTest {

	/**
	 * The url endpoint
	 */
	final private static String endpoint="http://nerd.eurecom.fr/api/";
	
	/**
	 * The api key
	 */
	final private static String api_key="10l3qnpgab7iid06saioooo9q31m4a2a";


	/**
	 * Test to inovoke the service and check that a json response is returned, with specific elements
	 * @throws IOException 
	 * @throws IllegalArgumentException 
	 * @throws InvalidSyntaxException 
	 */
	@Test
	public void invokeServiceTest() throws IllegalArgumentException, IOException, InvalidSyntaxException{

		String text="Barack Obama is the new president of United States of America.";

		//initialize the configuration
		AnnotatorConfiguration config=new AnnotatorConfiguration();

		//set the url endpoint
		config.setUrlendpoint(new URL(endpoint));
		config.setApiKey(api_key);

		NerdService service = new NerdService();
		String response = service.invokeService(text, config,0);

		Assert.assertNotNull(response);
		Assert.assertTrue(response.length()>0);

		//parse the response as json object and verify the presence of some json elements
		JsonRootNode doc = new JdomParser().parse(response);

		List<JsonNode> entities = doc.getElements();
		Assert.assertNotNull(entities);
		Assert.assertTrue(entities.size()>0);
		
		//check the annotation has specific field
		JsonNode annotation = entities.iterator().next();
		Assert.assertNotNull(annotation.getStringValue("nerdType"));
		Assert.assertNotNull(annotation.getStringValue("extractor"));
		Assert.assertNotNull(annotation.getNumberValue("confidence"));
		Assert.assertNotNull(annotation.getNumberValue("startChar"));
		Assert.assertNotNull(annotation.getNumberValue("endChar"));
	}

	/**
	 * Check that when no url-ednpoint or key has been specified the service cannot be invoked
	 * @throws IOException 
	 * @throws IllegalArgumentException 
	 */
	@Test(expected=IllegalArgumentException.class)
	public void invokeServiceTestWithoutParameter() throws IllegalArgumentException, IOException{
		String text="Barack Obama is the new president of United States of America.";

		//initialize the configuration
		AnnotatorConfiguration config=new AnnotatorConfiguration();
		config.setApiKey(api_key);

		NerdService service = new NerdService();
		service.invokeService(text, config,0);
		
		config.setApiKey(null);
		config.setUrlendpoint(new URL(endpoint));
		service.invokeService(text, config,0);

	}

}
