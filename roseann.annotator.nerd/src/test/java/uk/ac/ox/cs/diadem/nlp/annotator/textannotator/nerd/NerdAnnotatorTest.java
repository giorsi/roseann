package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.nerd;

import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;

public class NerdAnnotatorTest {

	private Annotator nerd;

	private String text;

	@Before
	public void bringUp(){
		nerd=NerdAnnotator.getInstance();
		text="Barack Obama is the new president of the United States.";
	}

	/**
	 * Test to check that a text can be submitted to nerd annotator service
	 * @throws InvalidSyntaxException
	 */
	@Test
	public void testSubmitToAnnotator() throws InvalidSyntaxException{

		//check the url-ednpoint has been specified in the configuration
		Assert.assertNotNull(nerd.getConfig().getURlendpoint());
		//check the key has been specified in the configuration
		Assert.assertNotNull(nerd.getConfig().getApiKey());

		//check that the text can be submitted to the annotator and no exceptions are thrown
		String response = nerd.submitToAnnotator(text);

		Assert.assertNotNull(response);
		Assert.assertTrue(response.length()>0);

		//check the response can be parse as json node and contains an element entities and an element relations
		JsonRootNode doc = new JdomParser().parse(response);

		List<JsonNode> entities = doc.getElements();
		Assert.assertNotNull(entities);
		Assert.assertTrue(entities.size()>0);

		//check the annotation has specific field
		JsonNode annotation = entities.iterator().next();
		Assert.assertNotNull(annotation.getStringValue("nerdType"));
		Assert.assertNotNull(annotation.getStringValue("extractor"));
		Assert.assertNotNull(annotation.getNumberValue("confidence"));
		Assert.assertNotNull(annotation.getNumberValue("startChar"));
		Assert.assertNotNull(annotation.getNumberValue("endChar"));
	}

	/**
	 * Test to verify the entities retrieved from extractiv web service
	 */
	@Test
	public void testAnnotateEntities(){
		Set<Annotation> annotations = nerd.annotateEntity(text);
		Assert.assertNotNull(annotations);
		Assert.assertTrue(annotations.size()>0);
	}

	/**
	 * Test to verify that no entitie ares retrieved from extractiv web service with an senseless text
	 */
	@Test
	public void testAnnotateNoEntities(){
		text="Hello hello hello";

		Set<Annotation> annotations = nerd.annotateEntity(text);
		Assert.assertNotNull(annotations);
		Assert.assertTrue(annotations.size()==0);
	}

}
