/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.util;

import java.util.HashMap;
import java.util.Map;


/**
 * 
 * 
 * @author Stefano Ortona (stefano.ortona@gmail.com)
 * 		   Oxford University, Department of Computer Science
 * 
 * Utility class to find an offset of a given piece of text over a sourceText
 * Useful for those annotators which don't provide the annotation offset such as Zemanta and AlchemyAPI
 */
public class Offset {

	/**
	 * Static method to find start-end of a given string over a sourceText
	 * 
	 * @param occurence
	 * 				the number of occurences of the given text over the sourceText, -1 if uknown 
	 * @param sourceText
	 * 				the sourceText containing the string text
	 * @param text
	 * 				text to be found over the sourceText
	 * @return map where key is start and value is end of the snippet found over the sourceText
	 */	
	public static Map<Integer,Integer> getInstancePosition(final int occurence,final String sourceText,final String text){

		final int annotationLength=text.length();
		//the position in the sourceText
		int textPosition=0;

		//set to false when all occurences has been found or reached the end of the text
		boolean instancesFound=true;

		//instances found
		int instances=0;

		Map<Integer,Integer> start2end=new HashMap<Integer,Integer>();
		int offset=0;
		//find offset for every instance
		while(instancesFound){
			offset=getOffset(text,sourceText.substring(textPosition));
			//if can't find any match of the instance, just skip to the next instance
			if(offset!=-1) {
				start2end.put(textPosition+offset,textPosition+offset+annotationLength);
				textPosition+=offset+annotationLength;
				instances++;
			}
			if(offset==-1||instances==occurence) instancesFound=false;
		}

		//return null if can't find any matching text
		return (start2end.size()>0) ? start2end : null;

	}
	
	/**
	 * Utiliy method to find the offset of a given text representing an annotation over a sourceText
	 * @param annotation
	 * 				text representing the annotation
	 * @param text
	 * 				the sourceText where the annotation has been found
	 * @return the annotation offset over the text
	 */
	private static int getOffset(String annotation,String text){

		//calculate the offset, case insensitive
		int offset=(text.toLowerCase()).indexOf(annotation.toLowerCase());

		//check if character before and after are not letter, otherwise don't consider it
		if(offset!=-1){
			String before=" ";
			String after=" ";
			if(offset!=0) 
				before=Character.toString(text.charAt(offset-1));
			if (offset+annotation.length()<text.length()) 
				after=Character.toString(text.charAt(offset+annotation.length()));
			
			if(before.matches("[a-z|A-Z|0-9]")||after.matches("[a-z|A-Z]")){
				
				//if before the annotation there's a alphanumeric character or after the annotation there's an alpha character,that is no an annotation
				offset+=annotation.length();
				if(offset>=text.length()) return -1;
				int newOffset=getOffset(annotation,text.substring(offset));
				if(newOffset==-1) return -1;
				offset+=newOffset;
				
			}
		}

		return offset;
	}

}
