/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation;

import java.util.Set;

/**
 * @author Luying Chen
 * This interface defines the set of apis of general annotations
 */
public interface Annotation {
	
	//the annotation type
	public enum AnnotationType  {ENT,REL,SENTIMENT};
	
	//the annotation class
	public enum AnnotationClass	{INSTANCE,LABEL,HELPER};
	
	/**
	 * Get the annotation id
	 * @return	the annotation id
	 */
	public String getId();
	
	/**
	 * Get the annotation concept
	 * @return	the annotation concept
	 */
	public String getConcept();
	/**
	 * Get the annotation offset start
	 * @return	the annotation offset start
	 */
	public long getStart();
	/**
	 * Get the annotation offset end
	 * @return	the annotation offset end
	 */
	public long getEnd();
	
	/**
	 * Get the annotator name providing the annotation
	 * @return	the annotator name providing the annotation
	 */
	public String getOriginAnnotator();
	
	/**
	 * Get the annotation confidence provided by the original annotator
	 * @return	the annotation confidence provided by the original annotator
	 */
	public Double getConfidence();
	
	/**
	 * Get the annotation attributes
	 * @return	the annotation attribute set
	 */
	public Set<AnnotationAttribute> getAttributes();
	
	/**
	 * Get the annotation type
	 * @return	the annotation type
	 */
	public AnnotationType getType();
	
	/**
	 * Set the annotation type
	 * @param annotation_type the type to be set
	 */
	public void setType(AnnotationType type);
	
	/**
	 * Set the annotation id
	 * @param id
	 */
	public void setId(final String id);
	
	/**
	 * Set the annotator where the annotation comes from
	 * @param annotator
	 */
	public void setOriginAnnotator(final String annotator);
	
	/**
	 * Get the source of information for the annotation
	 * @return	the source of information
	 */
	public String getAnnotationSource();
	
	/**
	 * Set the source of information
	 * @param source
	 */
	public void setAnnotationSource(final String source);
	
	/**
	 * Set the concept
	 * @param concept
	 */
	public void setConcept(final String concept);
	
	/**
	 * Hash Code method
	 * @return
	 */
	public int hashCode();
	
	/**
	 * Equals method
	 * @param o
	 * @return
	 */
	public boolean equals(Object o);
	
	/**
	 * Add an attribute
	 * @param attribute	the attribute to add
	 */
	public void addAttribute(AnnotationAttribute attribute);
	
	/**
	 * Get the annotation class
	 * @return	the annotation class
	 */
	public AnnotationClass getAnnotationClass();
	
	/**
	 * Set the annotation class
	 * @param annotationClass the class to be set
	 */
	public void setAnnotationClass(AnnotationClass annotationClass);
	
	/**
	 * Set the start-offset
	 * @param start
	 */
	public void setStart(long start);
	
	/**
	 * Set the end-offset
	 * @param end
	 */
	public void setEnd(long end);
	
	/**
	 * Set the confidence
	 * @param confidence
	 */
	public void setConfidence(double confidence);
}
