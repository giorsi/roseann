/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.tree.ConfigurationNode;
import org.apache.commons.configuration.tree.xpath.XPathExpressionEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk) Oxford University,
 *         Department of Computer Science
 * 
 *         Utility class to manage the parameters of each annotator
 * 
 */

public class AnnotatorConfiguration {
	/**
	 * The logger
	 */
	static final Logger LOGGER = LoggerFactory.getLogger(AnnotatorConfiguration.class);
	
	/**
	 * The configuration file location
	 */
	private String confFile="conf/TextannotatorConfiguration.xml";
	
	/**
	 * The api_key, for those web services which require it
	 */
	private String apiKey;
	
	/**
	 * The url endpoint
	 */
	private URL endpoint;
	
	/**
	 * The annotator-specific parameters maps, key is the name of the parameter, values is the value of the parameter
	 */
	private Map<String,String> specificParameters;
	
	/**
	 * Constructor
	 */
	public AnnotatorConfiguration(){
		this.specificParameters=new HashMap<String,String>();
	}
	
	/**
	 * Method to read the configuration parameters of the annotator from the input specified configuration file
	 * @param annotatorName	The name of the annotator to retrieve the parameters from the configuratione file
	 * @param confFile			The location of the configuration file
	 */
	public void initializeConfiguration(final String annotatorName,final String confFile){
		this.confFile=confFile;
		initializeConfiguration(annotatorName);
		
	}
	/**
	 * Method to read the configuration parameters of the annotator from the configuration file
	 * @param annotatorName	The name of the annotator to retrieve the parameters from the configuratione file
	 */
	public void initializeConfiguration(final String annotatorName){

		//read the key
		XMLConfiguration conf=null;
		try{
			conf = new XMLConfiguration(confFile);
		}
		catch(ConfigurationException e){
			LOGGER.error("Couldn't read the configuration file located at {}",confFile,e);
			return;
		}

		conf.setExpressionEngine(new XPathExpressionEngine());
		final String apiKey=conf.getString("nlp/textannotator/annotator[@name='"+annotatorName+"']/keys/key[1]");

		if (apiKey!=null){
			this.setApiKey(apiKey);
		}

		//read the url endpoint
		final String urlEndpoint=conf.getString("nlp/textannotator/annotator[@name='"+annotatorName+"']" +
				"/endpoint/url");

		if (urlEndpoint!=null){
			try {
				URL endpoint=new URL(urlEndpoint);
				this.setUrlendpoint(endpoint);
			} catch (MalformedURLException e) {
				LOGGER.warn("The specified endpoint {} for the web service is not a valid url",urlEndpoint,e);
			}
		}

		//read the other parameters
		Map<String,String> parametersMap=new HashMap<String,String>();
		try{
			HierarchicalConfiguration pr=conf.configurationAt("nlp/textannotator/annotator" +
					"[@name='"+annotatorName+"']/parameters");
			@SuppressWarnings("unchecked")
			final List<ConfigurationNode> parameters=pr.getRootNode().getChildren();
			for(ConfigurationNode node:parameters){
				final String name=node.getName();
				String value=node.getValue().toString();

				if(value!=null&&value.length()>0)
					parametersMap.put(name, value);
			}
		}
		catch(final Exception e){
			LOGGER.warn("Not able to locate in the configuration file {} the parameters values",confFile,e);
		}
		
		if(parametersMap.size()>0){
			this.setSpecificParameters(parametersMap);
		}

	}
	
	/**
	 * Method to set the api key of the web service
	 * @param apiKey
	 */
	public void setApiKey(final String apiKey){
		this.apiKey=apiKey;
	}
	
	/**
	 * Method to get the api key of the service. 
	 * @return	the api key, null if the api key has not been specified (like for those services which do not require one)
	 */
	public String getApiKey(){
		return this.apiKey;
	}
	
	/**
	 * Method to set the url endpoint for the web service
	 * @param endpoint
	 */
	public void setUrlendpoint(URL endpoint){
		this.endpoint=endpoint;
	}
	
	/**
	 * Method to get the url endpoint of the web service
	 * @return	the url endpoint, null if has not been specified
	 */
	public URL getURlendpoint(){
		return this.endpoint;
	}
	
	/**
	 * Method to set the specific-annotator parameters
	 * @param parameters	A map where the key is the parameter name and the value is the parameter value
	 */
	public void setSpecificParameters(Map<String,String> parameters){
		this.specificParameters=parameters;
	}
	
	/**
	 * Method to get the specific-annotator parameters
	 * @return a map where the key is the parameter name and the value is the parameter value
	 */
	public Map<String,String> getSpecificParameters(){
		return this.specificParameters;
	}
	
	/**
	 * Get the location of the configuration file
	 * @return	The string representing the location of the configuration file
	 */
	public String getConfigurationFileLocation(){
		return this.confFile;
	}

}
