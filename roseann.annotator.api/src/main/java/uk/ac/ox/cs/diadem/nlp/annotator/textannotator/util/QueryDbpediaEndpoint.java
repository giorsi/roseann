/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;


import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;


/**
 * 
 * 
 * @author Stefano Ortona (stefano.ortona@gmail.com) Oxford University,
 *         Department of Computer Science
 * 
 *         Utility class to query the Dbpedia SPARQL endpoint
 *         The class can be used to retrieve the dbpedia ontology types given a dbpedia resource link
 */

public class QueryDbpediaEndpoint {
	
	/**
	 * Logger
	 */
	static final Logger logger = LoggerFactory.getLogger(QueryDbpediaEndpoint.class);

	/**
	 * Set to store locally the supertypes for a each type
	 */
	static final Map<String,Set<String>> supertypes=new HashMap<String,Set<String>>();
	
	/**
	 * The dbpedia url endpoint
	 */
	private final static String dbpedia_endpoint="http://dbpedia.org/sparql";
	
	/**
	 * The dbpedia classes prefix
	 */
	private final static String dbpedia_prefix="http://dbpedia.org/ontology/";
	
	/**
	 * Initilize the logger
	 */
	static{
		ConfigurationFacility.getConfiguration();
	}

	/**
	 * Public method to retrieve dbpedia ontology types of given link
	 * Given a input resource dbpedia link, retrieve the dbpedia ontology types
	 * @param link
	 * 			dbpedia resource link
	 * @return list of all dbpedia ontology types, null if the type doesn't belong to dbpedia ontology
	 */
	public static Set<String> getTypes(String link){
		List<String> oneType=new LinkedList<String>();
		oneType.add(link);

		final Map<String,Set<String>> multipleTypes=getMultipleTypes(oneType);
		if(multipleTypes!=null)
			return multipleTypes.get(link);
		return null;
	}


	public static Map<String,Set<String>> getMultipleTypes(List<String> links){


		if(links==null||links.size()==0) return null;

		//map to be returned
		Map<String,Set<String>> link2types=new HashMap<String,Set<String>>();

		/**
		 * Build the query
		 * ----------------------------------------------------
		 */

		logger.debug("Building the query to submit to DBPedia SPARQL endpoint...");
		StringBuilder sparqlQueryBuilder=new StringBuilder();

		sparqlQueryBuilder.append("PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>" +
				" SELECT ?resource ?type WHERE { ?resource rdf:type ?type. FILTER ( ?resource IN (");
		for(String link:links){	
			sparqlQueryBuilder.append("<"+link+">, ");
		}

		final String sparqlQuery=sparqlQueryBuilder.toString().substring(0, sparqlQueryBuilder.length()-2)+"))}";
		logger.debug("Query built.");

		/**
		 * ----------------------------------------------------
		 */

		logger.debug("Submitting query to DBpedia SPARQL endpoint.");
		//execute query
		Query query = QueryFactory.create(sparqlQuery);

		//dbpedia endpoint can be substitued with local endpoints
		QueryExecution qexec = QueryExecutionFactory.sparqlService(dbpedia_endpoint, query);


		ResultSet results = qexec.execSelect();

		//process the response and save the results into a map
		while(results.hasNext()){

			QuerySolution result=results.next();
			final String resource=result.getResource("resource").toString();
			final String type=result.getResource("type").toString();


			//get only db pedia ontology type
			if(!type.contains(dbpedia_prefix)) continue;

			Set<String> types=link2types.get(resource);
			if(types==null) types=new HashSet<String>();
			types.add(type.replaceAll(dbpedia_prefix, ""));
			link2types.put(resource, types);
		}

		logger.debug("Response received by DBPedia SPARQL endpoint and types saved" );

		qexec.close();

		return link2types.size()>0 ? link2types : null;


	}

	/**
	 * Public method to retrieve dbpedia ontology direct supertypes of a given type
	 * @param type
	 * 			the input dbpedia class
	 * @return list of all supertypes of that given type, empty list if there are none
	 */
	public static Set<String> getSupertypes(String type){
		if(type==null) return null;

		//first check if the sypertypes are in memory
		Set<String> supertypesCopy=supertypes.get(type);

		if(supertypesCopy!=null)
			return supertypesCopy;

		supertypesCopy=new HashSet<String>();

		//list to be returned
		final Set<String> supertypes=new HashSet<String>();

		/**
		 * Build the query
		 * ----------------------------------------------------
		 */

		logger.debug("Build the query to submit to DBPedia SPARQL endpoint...");


		final String sparqlQuery="PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> " +
				"SELECT ?superClass WHERE {<"+dbpedia_prefix+type+"> " +
				"rdfs:subClassOf ?superClass.}";

		logger.debug("Query built.");

		/**
		 * ----------------------------------------------------
		 */

		logger.debug("Submitting query to DBpedia SPARQL endpoint.");
		//execute query
		Query query = QueryFactory.create(sparqlQuery);

		//dbpedia endpoint can be substitued with local endpoints
		QueryExecution qexec = QueryExecutionFactory.sparqlService(dbpedia_endpoint, query);


		ResultSet results = qexec.execSelect();

		//process the response and save the results into a map
		while(results.hasNext()){

			QuerySolution result=results.next();
			final String supertype=result.getResource("superClass").toString();

			if(supertype.contains(dbpedia_prefix))
				supertypes.add(supertype.replaceAll(dbpedia_prefix, ""));
		}

		logger.debug("Response received by DBPedia SPARQL endpoint and types saved" );

		qexec.close();

		supertypesCopy.addAll(supertypes);

		//store the retrieved supertypes in memory
		QueryDbpediaEndpoint.supertypes.put(type, supertypes);

		return supertypesCopy;

	}

	public static Set<String> getAllDbpediaClasses(){

		//list to be returned
		final Set<String> allClasses=new HashSet<String>();

		/**
		 * Build the query
		 * ----------------------------------------------------
		 */

		final String sparqlQuery="PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> " +
				"SELECT ?subClass WHERE {?subClass rdfs:subClassOf ?superClass.}";


		/**
		 * ----------------------------------------------------
		 */
		//execute query
		Query query = QueryFactory.create(sparqlQuery);

		//dbpedia endpoint can be substitued with local endpoints
		QueryExecution qexec = QueryExecutionFactory.sparqlService(dbpedia_endpoint, query);


		ResultSet results = qexec.execSelect();

		//process the response and save the results into a map
		while(results.hasNext()){

			QuerySolution result=results.next();
			final String dbpediaClass=result.getResource("subClass").toString();

			if(dbpediaClass.contains(dbpedia_prefix))
				allClasses.add(dbpediaClass.replaceAll(dbpedia_prefix, ""));
		}

		qexec.close();

		return allClasses;
	}


}