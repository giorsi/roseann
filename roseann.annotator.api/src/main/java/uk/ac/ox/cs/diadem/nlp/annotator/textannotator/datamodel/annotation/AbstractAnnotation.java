/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation;


import java.util.*;

/**
 * @author Luying Chen
 * This class defines an abstract annotation which contains common
 * attributes and operations of various concrete annotation classes.
 * 
 */
public abstract class AbstractAnnotation implements Annotation{

	//the annotation id
	protected String id;

	//the annotation concept
	protected String concept;

	//the offset start
	protected long start;

	//the offset end
	protected long end;

	//the annotation type
	protected AnnotationType type;

	//the annotation class
	protected AnnotationClass annotationClass;

	//the source of information
	protected String annotationSource;

	public void setType(AnnotationType type) {
		this.type = type;
	}

	/**
	 * Empty constructor
	 */
	public AbstractAnnotation(){
		//Initialise the attributes set
		this.attributes=new HashSet<AnnotationAttribute>();
	}

	/**
	 * Constructor
	 */
	public AbstractAnnotation(String id, String concept, long start, long end,
			String originAnnotator, String annotationSource,Double confidence,
			AnnotationClass annotationClass) {
		super();
		this.id = id;
		this.concept = concept;
		this.start = start;
		this.end = end;
		this.originAnnotator = originAnnotator;
		this.confidence = confidence;
		this.annotationSource=annotationSource;
		this.annotationClass=annotationClass;
		//Initialise the attributes set
		this.attributes=new HashSet<AnnotationAttribute>();
	}


	//the original annotator name
	protected String originAnnotator;

	//the confidence value
	protected Double confidence;

	//the attribute set
	protected Set<AnnotationAttribute> attributes;


	/**
	 * Get the annotation id
	 * @return	the annotation id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Get the annotation concept
	 * @return	the annotation concept
	 */
	public String getConcept() {
		return concept;
	}

	/**
	 * Get the annotation offset start
	 * @return	the annotation offset start
	 */
	public long getStart() {
		return start;
	}

	/**
	 * Get the annotation offset end
	 * @return	the annotation offset end
	 */
	public long getEnd() {
		return end;
	}

	/**
	 * Get the annotator name providing the annotation
	 * @return	the annotator name providing the annotation
	 */
	public String getOriginAnnotator() {
		return originAnnotator;
	}

	/**
	 * Get the annotation confidence provided by the original annotator
	 * @return	the annotation confidence provided by the original annotator
	 */
	public Double getConfidence() {
		return confidence;
	}

	/**
	 * Get the annotation attributes
	 * @return	the annotation attribute set
	 */
	public Set<AnnotationAttribute> getAttributes(){
		return attributes;
	}

	public AnnotationType getType(){
		return type;
	}

	public void setId(final String id){
		this.id=id;
	}

	public void setOriginAnnotator(final String annotator){
		this.originAnnotator=annotator;
	}

	public String getAnnotationSource(){
		return this.annotationSource;
	}

	public void setAnnotationSource(final String source){
		this.annotationSource=source;
	}

	public String toString(){
		return this.id+"_"+this.concept+"_"+this.start+"_"+this.end;
	}

	public void setConcept(final String concept){
		this.concept=concept;
	}

	public int hashCode(){
		int hashCode=0;
		if(this.id!=null)
			hashCode+=this.id.hashCode();
		if(this.annotationSource!=null)
			hashCode+=this.annotationSource.hashCode();
		if(this.concept!=null)
			hashCode+=this.concept.hashCode();
		if(this.confidence!=null)
			hashCode+=confidence.intValue();
		hashCode+=(int) end;
		hashCode+=(int) start;
		if(this.originAnnotator!=null)
			hashCode+=this.originAnnotator.hashCode();
		return hashCode;
	}

	/**
	 * Two annotations are equal iff they have all the fields equal
	 */
	public boolean equals(Object o){
		Annotation otherAnnotation = (Annotation) o;
		boolean areEquals=true;
		if(this.id!=null)
			areEquals=areEquals&&this.id.equals(otherAnnotation.getId());
		if(this.annotationSource!=null)
			areEquals=areEquals&&this.annotationSource.equals(otherAnnotation.getAnnotationSource());
		if(this.concept!=null)
			areEquals=areEquals&&this.concept.equals(otherAnnotation.getConcept());
		if(this.confidence!=null)
			areEquals=areEquals&&this.confidence.equals(otherAnnotation.getConfidence());
		areEquals=areEquals&&this.start==otherAnnotation.getStart();
		areEquals=areEquals&&this.end==otherAnnotation.getEnd();
		if(this.originAnnotator!=null)
			areEquals=areEquals&&this.originAnnotator.equals(otherAnnotation.getOriginAnnotator());
		return areEquals;

	}

	public void addAttribute(AnnotationAttribute attribute){
		this.attributes.add(attribute);
	}

	public AnnotationClass getAnnotationClass(){
		return this.annotationClass;
	}

	public void setAnnotationClass(AnnotationClass annotationClass){
		this.annotationClass=annotationClass;
	}

	public void setStart(long start){
		this.start=start;
	}

	public void setEnd(long end){
		this.end=end;
	}

	public void setConfidence(double confidence){
		this.confidence=confidence;
	}

}
