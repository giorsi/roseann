/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator;

import java.util.Set;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;

/**
 * An abstract adapter class for annotation methods which are empty in this class.  This class exists as
 * convenience for creating individual annotators which may only support
 * a subset of functions  listed in Annotator interface. 
 * @author luying chen
 *
 */
public abstract class AnnotatorAdapter implements Annotator{
	
	/**
	 * Default implementation for the method is to return null
	 */
	@Override
	public Set<Annotation> annotateEntity(String text) {
		// TODO Auto-generated method stub
		return null;
	}


	/**
	 * The bound configuration object
	 */
	protected AnnotatorConfiguration config;
	
	/**
	 * The annotator name
	 */
	protected  String annotatorName;

	
	@Override
	public void setConfig(AnnotatorConfiguration config){
		this.config = config;
	}
	
	@Override
	public AnnotatorConfiguration getConfig(){
		return config;
	}

	@Override
	public void setAnnotatorName(String name) {
		this.annotatorName = name;
		
	}

	@Override
	public String getAnnotatorName() {
		return this.annotatorName;
	}
	
	@Override
	/**
	 * Default implementation for the method is to return null
	 */
	public Set<Annotation>  annotateRelation(String text){
		return null;
	}
	
	@Override
	/**
	 * Default implementation for the method is to return null
	 */
	public Set<Annotation>  annotateRelation(String text,int timeout){
		return null;
	}
	
	/**
	 * Default implementation for the method is to return null
	 */
	@Override
	public Set<Annotation> annotateEntity(String text,int timoeut) {
		// TODO Auto-generated method stub
		return null;
	}


	

	
	
	
}
