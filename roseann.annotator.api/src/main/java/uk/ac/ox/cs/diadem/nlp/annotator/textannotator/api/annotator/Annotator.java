/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator;

import java.util.*;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.*;

/**
 * This interface defines a set of commonly-supported IE tasks.
 * @author Luying Chen
 *
 */
public interface Annotator {

	/**
	 * A method to extract named entities for a given text.
	 * @param text
	 * 			The input text to be annotated
	 * @return Set<Annotation> 
	 * 			A set of annotations
	 */
	public Set<Annotation> annotateEntity(String text);

	/**
	 * A method to extract the facts of entity relationships for a given text.
	 * @param text
	 * 			The input text to be annotated
	 * @return Set<Annotation> 
	 * 			A set of annotations
	 */
	public Set<Annotation>  annotateRelation(String text);

	/**
	 * A method to set the configuration
	 * @param config
	 */
	public void setConfig(AnnotatorConfiguration config);

	/**
	 * A method to get the configuration object
	 * @return AnnotatorConfiguration
	 * 				The configuration object
	 */
	public AnnotatorConfiguration getConfig();
	/**
	 * A method to set the annotator's name
	 * @param name
	 */
	public void setAnnotatorName(String name);

	/**
	 * A method to get the annotator's name
	 * @return String
	 * 				The name of the annotator
	 */
	public String getAnnotatorName();

	/**
	 * A method to submit the content to be annotated and get the corresponding response
	 * @param freeText
	 * 			The text to be annotated
	 * @return String
	 * 			The response string
	 */
	public String submitToAnnotator(String freeText);

	/**
	 * A method to submit the content to be annotated and get the corresponding response with a timeout
	 * @param freeText
	 * 			The text to be annotated
	 * @param timeout
	 * 			The timeout (milliseconds) to wait for
	 * @return String
	 * 			The response string
	 */
	public String submitToAnnotator(String freeText,int timeout);

	/**
	 * A method to extract named entities for a given text with a timeout to wait
	 * @param text
	 * 			The input text to be annotated
	 * @param timeout
	 * 			The timeout (milliseconds) to wait for
	 * @return Set<Annotation> 
	 * 			A set of annotations
	 */
	public Set<Annotation> annotateEntity(String text,int timeout);

	/**
	 * A method to extract the facts of entity relationships for a given text with a timeout to wait
	 * @param text
	 * 			The input text to be annotated
	 * @param timeout
	 * 			The timeout (milliseconds) to wait for
	 * @return Set<Annotation> 
	 * 			A set of annotations
	 */
	public Set<Annotation>  annotateRelation(String text,int timeout);

}
