/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.wrapper;



import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;

/**
 * This abstract class is a general decorator of Annotator objects, which decorate
 * the behaviors of a given ananotator.
 * @author Luying Chen
 *
 */
public abstract class AnnotatorDecorator extends AnnotatorAdapter {
	
	protected Annotator decoratee;
	

	

	


	/* (non-Javadoc)
	 * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter#setConfig(uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration)
	 */
	@Override
	public void setConfig(AnnotatorConfiguration config) {
		if(decoratee!=null){
			decoratee.setConfig(config);
		}
	}


	/* (non-Javadoc)
	 * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter#getConfig()
	 */
	@Override
	public AnnotatorConfiguration getConfig() {
		if(decoratee!=null){
			return decoratee.getConfig();
		}
		return this.config;
	}


	/* (non-Javadoc)
	 * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter#setAnnotatorName(java.lang.String)
	 */
	@Override
	public void setAnnotatorName(String name) {
		if(decoratee!=null){
			decoratee.setAnnotatorName(name);
		}
	}


	/* (non-Javadoc)
	 * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter#getAnnotatorName()
	 */
	@Override
	public String getAnnotatorName() {
		if(decoratee!=null){
			return decoratee.getAnnotatorName();
		}
		return this.annotatorName;
	}


	/* (non-Javadoc)
	 * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator#submitToAnnotator(java.lang.String)
	 */
	@Override
	public String submitToAnnotator(String freeText) {
		if(decoratee!=null){
			return decoratee.submitToAnnotator(freeText);
		}
		return null;
	}
	
	@Override
	public String submitToAnnotator(String freeText,int timeout) {
		if(decoratee!=null){
			return decoratee.submitToAnnotator(freeText,timeout);
		}
		return null;
	}

}
