/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.util;

import java.util.Set;

import junit.framework.Assert;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk) Oxford University,
 *         Department of Computer Science
 * Test class to test all the methods to query the dbpedia endpoint
 *
 */
public class QueryDbpediaEndpointTest {

	static final Logger LOGGER = LoggerFactory.getLogger(QueryDbpediaEndpointTest.class);



	/**
	 * Test to retrieve the dbpedia class types of the resource Barack Obama
	 */
	@Test
	public void testGetDbpediaTypes(){
		final String dbpediaResource="http://dbpedia.org/resource/Barack_Obama";

		final Set<String> types=QueryDbpediaEndpoint.getTypes(dbpediaResource);

		Assert.assertNotNull(types);
		Assert.assertTrue(types.size()==3);
		Assert.assertTrue(types.contains("OfficeHolder"));
		Assert.assertTrue(types.contains("Person"));
		Assert.assertTrue(types.contains("Agent"));
	}

	/**
	 * Test to retrieve the dbpedia class types for unknown resource
	 */
	@Test
	public void testGetDbpediaTypesUnknownResource(){
		final String dbpediaResource="unknown";

		final Set<String> types=QueryDbpediaEndpoint.getTypes(dbpediaResource);

		Assert.assertNull(types);
	}
	
	/**
	 * Test to retrieve the dbpedia superclasses for a given type
	 */
	@Test
	public void testGetDbpediaSupertypes(){
		final String type="OfficeHolder";

		final Set<String> supertypes=QueryDbpediaEndpoint.getSupertypes(type);

		Assert.assertNotNull(supertypes);
		Assert.assertTrue(supertypes.size()==1);
		Assert.assertTrue(supertypes.contains("Person"));

	}
	
	/**
	 * Test to retrieve the dbpedia superclasses for a top-class type
	 */
	@Test
	public void testGetDbpediaSupertypesTopClass(){
		final String type="Agent";

		final Set<String> supertypes=QueryDbpediaEndpoint.getSupertypes(type);

		Assert.assertNotNull(supertypes);
		Assert.assertTrue(supertypes.size()==0);

	}
	
	/**
	 * Test to retrieve all the dbpedia classes
	 */
	@Test
	public void testGetDbpediaClasses(){

		final Set<String> allClasses=QueryDbpediaEndpoint.getAllDbpediaClasses();

		Assert.assertNotNull(allClasses);
		Assert.assertTrue(allClasses.size()>0);

	}

}
