/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.util;

import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;

/**
 * 
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk) Oxford University,
 *         Department of Computer Science
 * Test class to test the utility Offset methods
 *
 */

public class OffsetTest extends StandardTestcase{
	static final Logger LOGGER = LoggerFactory.getLogger(OffsetTest.class);

	private final static String text="Hello, home cat vegetarian test homeless another cat with a blue cat";

	/**
	 * Test to get the position of a word repeated many times, both with known and unknown occurences times
	 */
	@Test
	public void testGetPosition(){

		//unknown occurences
		Map<Integer,Integer> position=Offset.getInstancePosition(-1, text, "cat");

		Assert.assertNotNull(position);
		Assert.assertTrue(position.size()==3);
		Assert.assertTrue(position.containsKey(12));
		Assert.assertEquals(new Integer(15),position.get(12));
		Assert.assertTrue(position.containsKey(49));
		Assert.assertEquals(new Integer(52),position.get(49));
		Assert.assertTrue(position.containsKey(65));
		Assert.assertEquals(new Integer(68),position.get(65));

		//known occurences, only the first two
		position=Offset.getInstancePosition(2, text, "cat");

		Assert.assertNotNull(position);
		Assert.assertTrue(position.size()==2);
		Assert.assertTrue(position.containsKey(12));
		Assert.assertEquals(new Integer(15),position.get(12));
		Assert.assertTrue(position.containsKey(49));
		Assert.assertEquals(new Integer(52),position.get(49));
	}

	/**
	 * Test to retrieve the positions of a word that is unknown in the text
	 */
	@Test
	public void testGetPositionNoWord(){

		//unknown occurences
		Map<Integer,Integer> position=Offset.getInstancePosition(-1, text, "veg");

		Assert.assertNull(position);

		//known occurences, only the first two
		position=Offset.getInstancePosition(2, text, "veg");

		Assert.assertNull(position);
	}
	
	/**
	 * Test to retrieve the positions of a word that is also repeated inside other words
	 */
	@Test
	public void testGetPositionRepeatedWords(){

		//unknown occurences
		Map<Integer,Integer> position=Offset.getInstancePosition(-1, text, "home");

		Assert.assertNotNull(position);
		Assert.assertTrue(position.size()==1);
		Assert.assertTrue(position.containsKey(7));
		Assert.assertEquals(new Integer(11),position.get(7));

		//known occurences, only the first one
		position=Offset.getInstancePosition(1, text, "home");

		Assert.assertNotNull(position);
		Assert.assertTrue(position.size()==1);
		Assert.assertTrue(position.containsKey(7));
		Assert.assertEquals(new Integer(11),position.get(7));
	}

}
