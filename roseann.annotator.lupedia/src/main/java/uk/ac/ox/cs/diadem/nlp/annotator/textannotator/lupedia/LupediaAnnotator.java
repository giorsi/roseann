/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.lupedia;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import argo.jdom.*;
import argo.saj.InvalidSyntaxException;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.*;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.*;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;


/**
 *         The adapter submit the text to Lupedia, take the response as
 *         String representing JSon Object, builds a JSon Object and select the
 *         annotations to be returned. We can retrieve entity annotations.
 *          @author Luying Chen
 */
public class LupediaAnnotator extends AnnotatorAdapter {
	static final Logger logger = LoggerFactory
			.getLogger(LupediaAnnotator.class);
	/**
	 * Low layer api of LupediaAnnotator service
	 */
	private LupediaAPI lupedia;
	/**
	 * counter to increment the annotation id
	 */
	private long id_counter;
	/**
	 * singleton instance
	 */
	private static LupediaAnnotator INSTANCE;
	/**
	 * to store the annotation during the annotation process
	 */
	private Set<String> stored_annotations;
	/**
	 * method to get the singleton instance
	 * 
	 * @return singleton instance
	 */
	public static synchronized Annotator getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new LupediaAnnotator();
		}
		return INSTANCE;
	}

	/**
	 * private constructor to build the singleton instance
	 */
	private LupediaAnnotator() {
		ConfigurationFacility.getConfiguration();
		setAnnotatorName("lupedia");

		// start the counter from 0
		this.id_counter = 0;

		// initialize the parametrs
		AnnotatorConfiguration configuration = new AnnotatorConfiguration();
		configuration.initializeConfiguration(getAnnotatorName());
		setConfig(configuration);

		stored_annotations=new HashSet<String>();

	}
	@Override
	public String submitToAnnotator(String freeText) {
		return this.submitToAnnotator(freeText, 0);
	}

	@Override
	public String submitToAnnotator(String freeText,int timeout) {
		if (freeText == null || freeText.length() < 1) {
			logger.error("Trying to submit an empty text to Lupedia");
			throw new AnnotatorException("Trying to submit an empty text to Lupedia", logger);
		}

		logger.debug("Submitting document to Lupedia service.");

		// eliminate from the original text special characters
		freeText = freeText.replaceAll("\u0003", " ");


		String res;
		try {
			res = lupedia.invokeNEService(freeText, this.config,timeout);
			logger.debug("Lupedia Process done and response received.");
		}//check if the timeout has expired
		catch(final SocketTimeoutException e) {
			logger.warn("{} was not able to receive the web response within timeout {} milliseconds",
					getAnnotatorName(),timeout);
			return null;
		}
		catch (IllegalArgumentException | IOException e) {

			logger.error("Error submitting text to lupedia");
			throw new AnnotatorException("Error submitting text to Luepdia", e, logger);
		}

		return res;
	}


	/* (non-Javadoc)
	 * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter#annotateEntity(java.lang.String)
	 */
	@Override
	public Set<Annotation> annotateEntity(String text) {


		final String res = submitToAnnotator(text);
		final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(res, text);

		return entityAnnotations;
	}
	
	@Override
	public Set<Annotation> annotateEntity(String text,int timeout) {


		final String res = submitToAnnotator(text,timeout);
		if(res==null)
			return new HashSet<Annotation>();
		final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(res, text);

		return entityAnnotations;
	}

	/**
	 * An utility method to harvest annotations from response string.
	 * @param response
	 * 			The annotation response from the service
	 * @param text
	 * 			The text to be annotated
	 * @return Set<Annotation>
	 * 			The harvested annotation set
	 */
	private Set<Annotation> retrieveEntityAnnotations(String response, String text) {
		JsonRootNode doc;

		logger.debug("Processing the Lupedia response.");
		try {
			// create a JSon node parsing the text response
			doc = new JdomParser().parse(response);
		} catch (final InvalidSyntaxException e) {
			logger.error("The String response from Lupedia can't be parse as JSonNode");
			throw new AnnotatorException("The String response from Lupedia " + "can't be parse as JSonNode", e, logger);
		}
		Set<Annotation> entityAnnotation = new HashSet<Annotation>();
		// select only the entity annotations
		final List<JsonNode> selected_annotations = doc.getArrayNode("Entities");

		if (selected_annotations == null){
			logger.debug("Lupedia service did not find any entity annotations.");
			return entityAnnotation;
		}
		// iterate over all annotation
		for (final JsonNode annotation : selected_annotations) {
			final Annotation oneAnnotation = constructIndividualNE(annotation);
			if(oneAnnotation!=null){
				entityAnnotation.add(oneAnnotation);
				id_counter++;
			}
		}
		logger.debug("Response processed and annotations saved.");
		return entityAnnotation;


	}

	/**
	 * Utiliy method to construct an annotation object from json format
	 * @param annotation
	 * 			The annotation information in json format
	 * @return Annotation
	 * 			The resulting annotation object.
	 */
	private Annotation constructIndividualNE(JsonNode annotation) {
		Annotation oneAnnotation = null;
		try {

			//start-end
			final int begin = Integer.parseInt(annotation.getNumberValue("startOffset"));
			final int end = Integer.parseInt(annotation.getNumberValue("endOffset"));

			// assertion
			String label = annotation.getStringValue("instanceClass"); // set predicate


			// transform the concept dlv compatible
			label = label.replaceAll("http://dbpedia.org/ontology/", "");

			// confidence(if available)
			final Object valueObj = annotation.getNumberValue("weight");
			Double score = null;
			if (valueObj != null) {
				score = Double.parseDouble(valueObj.toString());

			}

			// check if a equal annotation has been already added

			final String annotation_composed_id=label+(begin+"")+(end+"");
			if(stored_annotations.contains(annotation_composed_id))
				return null;

			//if not found before an equal annotation, store the annotation
			stored_annotations.add(annotation_composed_id);

			final String id= annotatorName + "_" + (id_counter);


			// create the new entity annotation
			oneAnnotation = new EntityAnnotation(id,label,begin,end,annotatorName,null,score,AnnotationClass.INSTANCE);


			// retrieve the attributes for the annotation
			final Map<String, String> attribute2value = this.getAnnotationAttributes(annotation);
			// create a fact for every attribute, for every instance
			if (attribute2value != null) {
				// for every attribute create a object
				for (final String attribute_name : attribute2value.keySet()) {
					final String value =attribute2value.get(attribute_name) ;


					AnnotationAttribute attribute = new AnnotationAttribute(
							attribute_name, value);
					oneAnnotation.addAttribute(attribute);

				}
			}


		} 

		catch (final Exception e) {
			logger.warn("Not able to store the annotation in the model from the json object", e.getMessage());
			return null;
		}
		return oneAnnotation;

	}

	/**
	 * Method to find the annotation attribute. Currently in LupediaAPI we
	 * extract the URI of the class name for an annotation
	 * @param annotation
	 *            JsonNode representing the annotation
	 * @return map where key is the name of the attribute, value all values
	 *         associated with that specific attribute 
	 */
	private Map<String, String> getAnnotationAttributes(final JsonNode annotation) {
		final Map<String, String> attribute2value = new HashMap<String, String>();


		String label = annotation.getStringValue("instanceClass");
		attribute2value.put("class_uri", label);

		return attribute2value.size() > 0 ? attribute2value : null;
	}
	/* (non-Javadoc)
	 * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter#setConfig(uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration)
	 */
	@Override
	public void setConfig(AnnotatorConfiguration config) {
		// TODO Auto-generated method stub
		super.setConfig(config);
		lupedia = LupediaAPI.GetInstanceFromConfig(config);
	}

}
