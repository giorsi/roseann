/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.lupedia;
import java.io.*;

import java.net.*;
import java.util.Map;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
/**
 * Use this class to send a request to Lupedia web service and get a
 *         response as a string.
 * 
 * @author Luying Chen
 */
class LupediaAPI {
	private String _requestUri = "http://lupedia.ontotext.com/lookup/text2json";
	private LupediaAPI() {
	}
	/**
	 * Get LupediaAPI instance from configuration object(No key is required for this service).
	 * @param config
	 * 			Input configuration object of this LupediaAPI instance
	 * @return LupediaAPI
	 * 			The LupediaAPI instance with the specific configuration
	 */
	static public LupediaAPI GetInstanceFromConfig(AnnotatorConfiguration config) {
		LupediaAPI api = new LupediaAPI();
		final URL url = config.getURlendpoint();
		if(url!=null){
			api.set_requestUri(url.toString());
		}

		return api;
	}
	/**
	 * @return the _requestUri
	 */
	String get_requestUri() {
		return _requestUri;
	}
	/**
	 * @param _requestUri the _requestUri to set
	 */
	void set_requestUri(String _requestUri) {
		this._requestUri = _requestUri;
	}
	/**
	 * @param _args
	 *            : the parameters of the url request.
	 * @return
	 * 
	 * @throws IOException
	 */

	private String getResponseByPOST(String params,int timeout) throws IOException

	{
		URL url = new URL(_requestUri);

		HttpURLConnection handle = (HttpURLConnection) url.openConnection();
		handle.setDoOutput(true);
		handle.setConnectTimeout(timeout);

		StringBuilder data = new StringBuilder();

		data.append(params);

		handle.addRequestProperty("Content-Length",
				Integer.toString(data.length()));
		handle.setRequestMethod("POST");
		DataOutputStream ostream = new
				DataOutputStream(handle.getOutputStream());
		ostream.write(data.toString().getBytes());
		ostream.close();

		// get the response.
		BufferedReader rd = new BufferedReader(new InputStreamReader(
				handle.getInputStream()));
		String line, result = "";

		while ((line = rd.readLine()) != null) {
			result += line;
		}
		rd.close();
		handle.disconnect();


		return result;
	}

	/**
	 * Build the parameter string to send along with the request
	 * @param text
	 * @param configuration
	 * @return
	 * @throws IllegalArgumentException
	 * @throws UnsupportedEncodingException
	 */
	private String buildParameterString(String text, AnnotatorConfiguration configuration) throws 
	IllegalArgumentException, UnsupportedEncodingException{
		final Map<String, String> params = configuration.getSpecificParameters();

		String parameters = "";
		if(params==null){
			return parameters;
		}
		for(final String name:params.keySet()){

			String value=params.get(name).toString();

			if(value!=null&&value.length()>0){

				parameters+="&"+name+"="+URLEncoder.encode(value, "UTF8");

			}
		}
		return parameters;




	}
	/**
	 * Utility method to invoke NE service and return the response string.
	 * @param text
	 * 			The source text to be annotated
	 * @param config
	 * 			The input configration object
	 * @return String
	 * 			The response string of the service
	 * @throws IllegalArgumentException 
	 * @throws IOException 
	 */
	public String invokeNEService(String freeText, final AnnotatorConfiguration config,int timeout) throws IllegalArgumentException, IOException {
		if (null == freeText || freeText.length() < 1)
			throw new IllegalArgumentException("Enter some text to analyze.");

		final String parameters = buildParameterString(freeText, config);
		String _args = null;
		try {

			_args = "&" + URLEncoder.encode("lookupText", "UTF-8") + "=" + URLEncoder.encode(freeText, "UTF-8");

		} catch (final UnsupportedEncodingException e) {
			_args = "";
		}
		_args = _args+parameters;
		String result;



		result = getResponseByPOST(_args,timeout);
		final int len = result.length();
		result = "{ \"Entities\"  :[" + result.substring(1, len - 1) + "]}";

		return result;

	}


}
