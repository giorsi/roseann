/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.zemanta;

import org.apache.commons.io.IOUtils;

/**
 * Class representing suggested categories
 * 
 * @author Mateja Verlic
 * @since November 2011
 */
class Category extends Item {

	/** Category name */
	public String name;

	/** Confidence on 0.0 to 1.0 scale */
	public float confidence;

	/** Categorization scheme (default scheme is dmoz) */
	public String categorization;

	/**
	 * Default constructor for category
	 * 
	 * @param name
	 *            Name of the category
	 * @param confidence
	 *            Confidence of the category
	 * @param categorization
	 *            Categorization scheme of the category
	 */
	public Category(String name, String confidence, String categorization) {
		this.name = name;
		this.confidence = Float.parseFloat(confidence);
		this.categorization = categorization;
	}

	@Override
	public String toString() {
		return new StringBuilder().append("Category:").append(IOUtils.LINE_SEPARATOR).append("  name=").append(name)
		        .append(IOUtils.LINE_SEPARATOR).append("  confidence=").append(confidence)
		        .append(IOUtils.LINE_SEPARATOR).append("  categorization=").append(categorization).toString();
	}

}
