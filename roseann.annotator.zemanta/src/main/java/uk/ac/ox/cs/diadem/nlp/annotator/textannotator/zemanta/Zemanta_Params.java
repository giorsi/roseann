/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.zemanta;

import java.util.*;



/**
 * 
 * @author Stefano Ortona (stefano.ortona@gmail.com) Oxford University,
 *         Department of Computer Science
 * 
 *         Use this class to set the Parameters for Zemanta web-service
 *         invocation. Set up the response mode, default is JSon Set up the
 *         method to invoke, default is suggest Specify the value of api_key in
 *         the file api_key.txt, or change the location of the file to read the
 *         key from another file
 * 
 */

class Zemanta_Params {

	public static final String OUTPUT_XML = "xml";
	public static final String OUTPUT_RDF = "rdfxml";
	public static final String OUTPUT_JSON = "json";
	public static final String OUTPUT_WNJSON = "wnjson";

	private String method = "zemanta.suggest"; //default method
	private String api_key;
	private String text;
	private String outputMode = OUTPUT_JSON; //default output
	private String return_rdf; // boolean
	private String return_images; // boolean
	private String return_keywords; // boolean
	private String emphasis;
	private String title;
	private int markup_limit;
	private int images_limit;
	private int article_limit;
	private int article_age;
	private int markup_highlitgh;
	private int image_w;
	private int image_h;
	private Map<String, String> parameters;

	public Zemanta_Params(final String api_key) {
		parameters = new HashMap<String, String>();
		parameters.put("method", this.method);
		parameters.put("format", this.outputMode);
		this.api_key=api_key;
		parameters.put("api_key", this.api_key);
	}

	

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
		this.parameters.put("text", this.text);
	}

	public String getOutputMode() {
		return outputMode;
	}

	public void setOutputMode(String outputMode) {
		if (outputMode != null && !outputMode.equals(OUTPUT_XML) && !outputMode.equals(OUTPUT_RDF)
		        && !outputMode.equals(OUTPUT_JSON) && !outputMode.equals(OUTPUT_WNJSON)) {
			throw new RuntimeException("Invalid setting " + outputMode + " for parameter outputMode");
		}
		this.outputMode = outputMode;
		this.parameters.put("format", this.outputMode);
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
		this.parameters.put("method", this.method);
	}

	public String getApi_key() {
		return api_key;
	}

	public void setApi_key(String api_key) {
		this.api_key = api_key;
		this.parameters.put("api_key", this.api_key);
	}

	public String isReturn_rdf() {
		return return_rdf;
	}

	public void setReturn_rdf(String return_rdf) {
		if (!(return_rdf.equals("0") || return_rdf.equals("1"))) {
			throw new RuntimeException("Invalid setting " + return_rdf + " for parameter return_rdf");
		}
		this.return_rdf = return_rdf;
		this.parameters.put("return_rdf_links", return_rdf);
	}

	public String isReturn_images() {
		return return_images;
	}

	public void setReturn_images(String return_images) {
		if (!(return_images.equals("0") || return_images.equals("1"))) {
			throw new RuntimeException("Invalid setting " + return_images + " for parameter return_images");
		}
		this.return_images = return_images;
		this.parameters.put("return_images", return_images);
	}

	public String isReturn_keywords() {
		return return_keywords;
	}

	public void setReturn_keywords(String return_keywords) {
		if (!(return_keywords.equals("0") || return_keywords.equals("1"))) {
			throw new RuntimeException("Invalid setting " + return_keywords + " for parameter return_keywords");
		}
		this.return_keywords = return_keywords;
		this.parameters.put("return_keywords", return_keywords);
	}

	public String getEmphasis() {
		return emphasis;
	}

	public void setEmphasis(String emphasis) {
		this.emphasis = emphasis;
		this.parameters.put("emphasis", emphasis);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
		this.parameters.put("text_title", title);
	}

	public int getMarkup_limit() {
		return markup_limit;
	}

	public void setMarkup_limit(int markup_limit) {
		this.markup_limit = markup_limit;
		this.parameters.put("markup_limit", markup_limit + "");
	}

	public int getImages_limit() {
		return images_limit;
	}

	public void setImages_limit(int images_limit) {
		this.images_limit = images_limit;
		this.parameters.put("images_limit", images_limit + "");
	}

	public int getArticle_limit() {
		return article_limit;
	}

	public void setArticle_limit(int article_limit) {
		this.article_limit = article_limit;
		this.parameters.put("articles_limit", article_limit + "");
	}

	public int getArticle_age() {
		return article_age;
	}

	public void setArticle_age(int article_age) {
		this.article_age = article_age;
		this.parameters.put("articles_max_age_days", article_age + "");
	}

	public int getMarkup_highlitgh() {
		return markup_highlitgh;
	}

	public void setMarkup_highlitgh(int markup_highlitgh) {
		this.markup_highlitgh = markup_highlitgh;
		this.parameters.put("articles_highlight", markup_highlitgh + "");
	}

	public int getImage_w() {
		return image_w;
	}

	public void setImage_w(int image_w) {
		this.image_w = image_w;
		this.parameters.put("image_max_w", image_w + "");
	}

	public int getImage_h() {
		return image_h;
	}

	public void setImage_h(int image_h) {
		this.image_h = image_h;
		this.parameters.put("image_max_h", image_h + "");
	}

	public void setParametrs(Map<String, String> parameters) {
		this.parameters = parameters;
	}

	public Map<String, String> getParameters() {
		return this.parameters;
	}
	
	public void addParameter(final String name,final String value){
		this.parameters.put(name, value);
	}
	
	public void addParameters(final Map<String,String> parameters){
		this.parameters.putAll(parameters);
	}
}
