/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.zemanta;


import java.io.*;
import java.net.*;
import java.util.*;

import javax.xml.parsers.*;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 * Utiligy class for establishing connection and parsing response XML
 * 
 * @author Luying Chen
 */
class Request {

	/**
	 * Enumeration of request types (methods) available (<a target="_blank"
	 * href="http://developer.zemanta.com/docs/suggest/"> more information</a>)
	 * 
	 * <p>
	 * The form of the data sent to the server depends of this type.
	 * 
	 */
	protected static enum RequestType {
		/** Default request type */
		SUGGEST("zemanta.suggest"), MARKUP("zemanta.suggest_markup"), PREFERENCES("zemanta.preferences");

		private final String method;

		private RequestType(final String method) {
			this.method = method;
		}

		/**
		 * Returns literal string representing type of request to be used in web
		 * query (e.g.&nbsp;"zemanta.suggest").
		 */
		@Override
		public String toString() {
			return this.method;
		}
	}

	/**
	 * Encodes parameters and builds a query string from these parameters.
	 * 
	 * <p>
	 * Parameters are url encoded
	 * 
	 * @param parameters
	 *            Collection of (key, value) parameter pairs
	 * @return String representing url encoded web query
	 * @throws Exception
	 *             No parameters were defined.
	 */
	private static String buildWebQuery(final Map<String, String> parameters) throws IllegalArgumentException {

		if (parameters.size() < 1) {
			throw new IllegalArgumentException("No parameters for web query defined.");
		}

		final StringBuilder sb = new StringBuilder();
		try {
			for (final Map.Entry<String, String> entry : parameters.entrySet()) {
				final String key = URLEncoder.encode(entry.getKey(), "UTF-8");
				final String value = URLEncoder.encode(entry.getValue(), "UTF-8");
				sb.append(key).append('=').append(value).append('&');
			}
		} catch (final UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		// remove trailing &
		return sb.toString().substring(0, sb.length() - 1);

	}

	/**
	 * Checks whether required parameters are included and adds default method
	 * and format if not present.
	 * 
	 * <p>
	 * If no method or format are defined, default values are used.
	 * <p>
	 * Default method: <code>RequestType.SUGGEST</code> <br/>
	 * Default format: <code>RequestFormat.FORMAT_XML</code>
	 * 
	 * @param parameters
	 *            List of parameters for the web query.
	 * @return <code>Map</code> of required parameter pairs.
	 * @throws <code>Exception</code> in case there are no API key or text
	 *         defined in parameters.
	 */
	private static Map<String, String> checkRequiredParameters(final Map<String, String> parameters)
	        throws IllegalArgumentException {
		// check if required parameters are valid
		if (parameters == null) {
			throw new IllegalArgumentException("No parameters to build query from");
		}
		if (!parameters.containsKey("api_key")) {
			throw new IllegalArgumentException("No API key in parameters.");
		}
		if (!parameters.containsKey("text")) {
			throw new IllegalArgumentException("No text to analyse.");
		}

		if (!parameters.containsKey("method")) {
			// use default method
			parameters.put("method", "zemanta.suggest");
		}

		if (!parameters.containsKey("format")) {
			// use default format
			parameters.put("format", "xml");
		}

		return parameters;

	}

	/**
	 * Posts the request to the server and waits for reply
	 * 
	 * @param u
	 *            Url of the server to send the request to (gateway to the
	 *            service).
	 * @param parameters
	 *            Collection of parameters in the form of (key, value)
	 * @return XML Document
	 * @throws IOException
	 * @throws IllegalArgumentException
	 * @throws UnsupportedEncodingException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws Exception
	 */
	protected static Document request(final String u, Map<String, String> parameters,int timeout) throws IOException,
	        IllegalArgumentException, SAXException, ParserConfigurationException {

		parameters = checkRequiredParameters(parameters);

		if (u.equals("")) {
			throw new MalformedURLException("No URL for request");
		}

		final URL url = new URL(u);
		final HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setDoOutput(true);
		con.setConnectTimeout(timeout);

		// url encode parameters
		final String query = buildWebQuery(parameters);

		con.addRequestProperty("Content-Length", Integer.toString(query.length()));

		// Send the request
		final DataOutputStream dos = new DataOutputStream(con.getOutputStream());
		dos.write(query.toString().getBytes());
		dos.close();

		// Parse the response
		final DataInputStream is = new DataInputStream(con.getInputStream());
		final Document d = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is);

		// clean up
		is.close();
		con.disconnect();

		return d;

	}

	/**
	 * Posts the request to the server for formats other than XML (e.g.&nbsp;
	 * JSON) and waits for reply.
	 * 
	 * <p>
	 * Other formats currently supported are JSON, WJSON, RDF/XML.
	 * 
	 * @param u
	 *            Url of the server to send the request to (gateway to the
	 *            service).
	 * @param parameters
	 *            Collection of parameters in form of (key, value)
	 * @return <code>String</code> representing result in format requested
	 *         (other than XML)
	 * @throws IOException
	 *             I/O errors obtaining results
	 * @throws Exception
	 *             Exceptions raised when building web query, if no parameters
	 *             were defined.
	 */
	protected static String requestString(final String u, Map<String, String> parameters,int timeout) throws IOException,
	        IllegalArgumentException {

		String result = "";

		parameters = checkRequiredParameters(parameters);

		final URL url = new URL(u);
		final HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setDoOutput(true);
		con.setConnectTimeout(timeout);

		final String query = buildWebQuery(parameters);

		con.addRequestProperty("Content-Length", Integer.toString(query.length()));

		/* Send the request */
		final DataOutputStream dos = new DataOutputStream(con.getOutputStream());
		dos.write(query.toString().getBytes());
		dos.close();

		/* Parse the response */
		final BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));

		final StringBuilder sb = new StringBuilder();
		String line;

		while ((line = br.readLine()) != null) {
			sb.append(line);
			sb.append(IOUtils.LINE_SEPARATOR);
		}

		br.close();
		con.disconnect();

		result = sb.toString();

		return result;
	}

	

	/**
	 * Gets text content of a specific XML tag or <code>null</code>
	 * 
	 * @param e
	 *            Parent element in which the requested element should be
	 * @param name
	 *            Name of the requested tag
	 * @return Text content of the tag. <code>null</code> if specified tag does
	 *         not exist or it contains no text.
	 */
	protected static String getElementText(final Element e, final String name) {
		if (e != null) {
			final NodeList nl = e.getElementsByTagName(name);

			if (nl.getLength() == 1) {
				final Element el = (Element) nl.item(0);
				final String text = el.getTextContent();
				return text;
			}
		}
		return null;
	}

}
