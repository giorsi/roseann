/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.zemanta;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonNodeDoesNotMatchJsonNodeSelectorException;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.AnnotationAttribute;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.util.Offset;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.util.QueryDbpediaEndpoint;

/**
 * The adapter submit the text to Zemanta, take the response as String
 * representing JSon Object, builds a JSon Object and select the annotations to
 * be returned. We can retrieve entity annotations.
 *
 * @author Luying Chen
 */
public class ZemantaAnnotator extends AnnotatorAdapter {
  static final Logger logger = LoggerFactory.getLogger(ZemantaAnnotator.class);
  /**
   * Low layer api of zemanta service
   */
  private ZemantaAPI zemanta;
  /**
   * counter to increment the annotation id
   */
  private long id_counter;
  /**
   * singleton instance
   */
  private static ZemantaAnnotator INSTANCE;

  /**
   * method to get the singleton instance
   *
   * @return singleton instance
   */
  public static synchronized Annotator getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new ZemantaAnnotator();
    }
    return INSTANCE;
  }

  /**
   * private constructor to build the singleton instance
   */
  private ZemantaAnnotator() {
    ConfigurationFacility.getConfiguration();
    setAnnotatorName("zemanta");

    // start the counter from 0
    id_counter = 0;

    // initialize the parametrs
    final AnnotatorConfiguration configuration = new AnnotatorConfiguration();
    configuration.initializeConfiguration(getAnnotatorName());
    setConfig(configuration);

  }

  /*
   * (non-Javadoc)
   *
   * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.
   * AnnotatorAdapter#annotateEntity(java.lang.String)
   */
  @Override
  public Set<Annotation> annotateEntity(final String text) {

    final String res = submitToAnnotator(text);
    final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(res, text, id_counter);
    final int num_annotations = entityAnnotations.size();
    id_counter += num_annotations;
    return entityAnnotations;
  }

  @Override
  public Set<Annotation> annotateEntity(final String text, final int timeout) {

    final String res = submitToAnnotator(text, timeout);
    if (res == null)
      return new HashSet<Annotation>();
    final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(res, text, id_counter);
    final int num_annotations = entityAnnotations.size();
    id_counter += num_annotations;
    return entityAnnotations;
  }

  /**
   * An utility method to harvest entity annotations from annotator response
   *
   * @param text
   *          Text to be annotated
   * @param res
   *          Response to be interpreted
   * @param progressive
   * @return Set<Annotation> Set of entity annotations
   */
  private Set<Annotation> retrieveEntityAnnotations(final String res, final String text, final long progressive) {
    JsonRootNode doc;
    logger.debug("Processing the AlchemyAPI response.");
    try {
      // create a JSon node parsing the text response
      doc = new JdomParser().parse(res);
    } catch (final InvalidSyntaxException e) {
      logger.error("The String response from Zemanta can't be parse as JSonNode");
      throw new AnnotatorException("The String response from Zemanta " + "can't be parse as JSonNode", e, logger);
    }

    // select only the entity annotations
    final List<JsonNode> selected_annotations = selectAnnotations(doc, "markup");

    // the count the annotation
    int count = 0;
    final Set<Annotation> entityAnnotation = new HashSet<Annotation>();
    if (selected_annotations != null) {
      for (final JsonNode annotation : selected_annotations) {
        // increment the annotation id and save the annotations found
        final Set<Annotation> found_annotations = buildAnnotationFromJson(annotation, text, count + progressive);
        if (found_annotations != null) {
          count += found_annotations.size();
          entityAnnotation.addAll(found_annotations);
        }

      }
      logger.debug("Response processed and annotations saved.");
    } else {
      logger.debug("Zemanta service did not find any entity annotations.");
    }

    return entityAnnotation;
  }

  private Set<Annotation> buildAnnotationFromJson(final JsonNode annotation, final String doc,
      final long annotation_id) {
    final Set<Annotation> annotations_found = new HashSet<Annotation>();
    // get start-end for every instance of the given annotation since
    // Zemanta doesn't provide this information
    final Map<Integer, Integer> instances = Offset.getInstancePosition(-1, doc, annotation.getStringValue("anchor"));

    // confidence given by the annotator
    final double confidence_double = Double.parseDouble(annotation.getNumberValue("confidence"));

    // Zemanta uses knowledge database, machine learning algorithm to
    // understand text and context, so for the general type source is uknown
    String source = "null";

    // if the system is not able to identify instances over the text just
    // skip the annotation
    if (instances == null) {
      final String text = annotation.getStringValue("anchor");
      logger.debug("The system is not able to find the instances of {} annotated.", text);
      return null;
    }

    // retrieve the attributes for the annotation
    final Map<String, String> attribute2value = getAnnotationAttributes(annotation);

    // if the system can provide the wikipedia link, source is wikipedia
    if (attribute2value.containsKey("wikipedia")) {
      source = "wikipedia";
    }

    // to count the annotation
    int progressive = 0;

    // the entity_class is always an instance
    // final String entity_class = "instance";

    long start;
    long end;
    final String annotator_name = annotatorName;
    String id = null;

    // retrieve main types for the annotation
    List<JsonNode> types = null;

    // map to save type and source for the annotation
    final Map<String, String> type2source = new HashMap<String, String>();

    try {
      types = annotation.getArrayNode("entity_type");
      // save every main type,
      for (final JsonNode j : types) {

        // get the type without slash
        final String type = j.getText().split("/")[2];

        type2source.put(type, source);
      }
    } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
      // if there are no main types, simply continue
    }

    String new_source;
    // retrieve geolocation or businesscom or dbpedia type, if available, only
    // if not able to find other type

    if ((types == null) || types.isEmpty()) {
      try {
        final List<JsonNode> sub_types = annotation.getArrayNode("target");
        // create a fact annotation for every subtype,for every instance
        for (final JsonNode j : sub_types) {

          Set<String> other_types = new HashSet<String>();
          final String first_type = j.getStringValue("type");
          other_types.add(first_type);
          new_source = source;
          if (!(first_type.equals("geolocation") || first_type.equals("businesscom") || first_type.equals("rdf"))) {
            continue;
          }

          // if a rdf, check if it's a dbpedia link and retrieve the
          // dbpedia type
          if (first_type.equals("rdf")) {
            final String link = j.getStringValue("url");

            if (link.contains("dbpedia")) {
              try {
                other_types = QueryDbpediaEndpoint.getTypes(j.getStringValue("url"));
                if (other_types == null) {
                  continue;
                }
              } catch (final Exception e) {
                logger.warn("Error while retrieving the dbpedia types for Zemanta concepts", e);
                continue;
              }
              new_source = "dbpedia";
            } else {
              continue;
            }
          }

          for (final String type : other_types) {
            type2source.put(type, new_source);
          }
        }
      } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
        // if there are no other types, simply continue
      }
    }

    // if there are no types, save the annotation as thing
    if (type2source.size() == 0) {
      type2source.put("thing", source);
    }

    for (final String type : type2source.keySet()) {

      final String annotation_source = type2source.get(type);

      for (final int offset : instances.keySet()) {

        // retrieve all the information for the annotation
        start = offset;
        end = instances.get(offset);
        progressive++;
        id = getAnnotatorName() + "_" + (annotation_id + progressive);

        // create the new entity annotation
        // create the new entity annotation
        final Annotation oneAnnotation = new EntityAnnotation(id, type, start, end, annotator_name, annotation_source,
            confidence_double, AnnotationClass.INSTANCE);

        // create a fact for every attribute, for every instance
        if (attribute2value != null) {
          // for every attribute create a object
          for (final String attribute_name : attribute2value.keySet()) {
            final String value = attribute2value.get(attribute_name);

            final AnnotationAttribute attribute = new AnnotationAttribute(attribute_name, value);
            oneAnnotation.addAttribute(attribute);

          }
        }
        annotations_found.add(oneAnnotation);
      }
    }

    // return the annotations number saved
    return annotations_found;
  }

  /**
   * Method to retrieve all the attributes associated to a given annotation
   * Currently Zemanta provides the relevance belonging to [0,1], wikipedia link
   * and homepage (if found)
   *
   * @param annotation
   *          the JsonNode representing the annotation
   * @return map where key is the attribute name, value is the value of the
   *         attribute
   */
  private Map<String, String> getAnnotationAttributes(final JsonNode annotation) {
    final Map<String, String> attribute2value = new HashMap<String, String>();

    try {
      final String relevance = annotation.getNumberValue("relevance");
      attribute2value.put("relevance", relevance);
    }
    // if not able to find the relevance value, just continue
    catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
      logger.debug("System is not able to find the relevance value for Zemanata annotations.");
      // continue
    }

    try {
      final List<JsonNode> targets = annotation.getArrayNode("target");
      for (final JsonNode target : targets) {

        try {
          final String type = target.getStringValue("type");
          final String url = target.getStringValue("url");
          if (type.equals("wikipedia") || type.equals("homepage")) {
            attribute2value.put(type, url);
          }
        } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
          logger.debug("Subchild target of annotation node doesn't contation type or url element.");
          // continue
        }

      }
    }

    catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
      // if not able to find the target childern, continue
    }

    return attribute2value.size() > 0 ? attribute2value : null;
  }

  @Override
  public String submitToAnnotator(String freeText, final int timeout) {
    if ((freeText == null) || (freeText.length() < 1)) {
      logger.error("Trying to submit an empty text to Zemanta");
      throw new AnnotatorException("Trying to submit an empty text to Zemanta", logger);
    }

    logger.debug("Submitting document to Zemanta service.");

    // eliminate from the original text special characters
    freeText = freeText.replaceAll("\u0003", " ");

    final String res = zemanta.invokeNEService(freeText, config, timeout);

    // check the timeout has not expired
    if (res.equals("timeout")) {
      logger.warn("{} was not able to receive the web response within timeout {} milliseconds", getAnnotatorName(),
          timeout);
      return null;
    }
    logger.debug("Zemanta Process done and response received.");

    return res;
  }

  @Override
  public String submitToAnnotator(final String freeText) {
    return this.submitToAnnotator(freeText, 0);
  }

  /*
   * (non-Javadoc)
   *
   * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.
   * AnnotatorAdapter#setConfig(uk.ac.ox.cs.diadem.nlp.annotator.textannotator.
   * api.configure.AnnotatorConfiguration)
   */
  @Override
  public void setConfig(final AnnotatorConfiguration config) {
    // TODO Auto-generated method stub
    super.setConfig(config);
    try {
      zemanta = ZemantaAPI.GetInstanceFromConfig(config);
    } catch (final Exception e) {
      logger.error("Not able to read the configuration file for {}, at the moment the service cannot be invoked",
          getAnnotatorName(), e);
    }
  }

  /**
   * Select in the annotated document only the annottation of a certain type
   *
   * @param root
   *          the JSon node representing the annotated document
   * @param type
   *          type of the annotations to be selected
   * @return a list of selected annotations represented as JSon node
   */
  private List<JsonNode> selectAnnotations(JsonNode root, String type) {

    // In Zemanta we need to go deeper inside a node
    if (type.equals("markup")) {
      try {
        root = root.getNode("markup");
      } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException j) {
        // if there are no annotation of a certain type return null
        return null;
      }
      type = "links";
    }
    List<JsonNode> selected_annotations = new LinkedList<JsonNode>();
    try {
      selected_annotations = root.getArrayNode(type);
    } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
      // if there are no annotation of a certain type return null
      return null;

    }
    return selected_annotations.size() > 0 ? selected_annotations : null;
  }

}
