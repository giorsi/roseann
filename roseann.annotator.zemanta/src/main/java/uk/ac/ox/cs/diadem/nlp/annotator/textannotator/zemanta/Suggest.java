/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.zemanta;

import java.io.IOException;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.zemanta.Markup.Link;


/**
 * Class representing suggestion request.
 * 
 * @author Matej Usaj
 */
class Suggest extends Request {

	/**
	 * Parses XML document and maps it to Zemanta object model
	 * 
	 * @param root
	 *            Root node of XML document
	 * @param parseArticles
	 *            True to parse articles node
	 * @param parseKeywords
	 *            True to parse keywords node
	 * @param parseImages
	 *            True to parse images node
	 * @param parseMarkup
	 *            True to parse markup node
	 * @return {@link ZemantaResult} containing suggested articles, keywords,
	 *         images, markup or categories
	 */
	private static ZemantaResult parseXMLToModel(final Element root, final boolean parseArticles,
	        final boolean parseKeywords, final boolean parseImages, final boolean parseMarkup) {

		final ZemantaResult res = new ZemantaResult();

		/* Parse BASIC response data */
		final String status = getElementText(root, "status");
		if (status == null || !status.equals("ok")) {
			res.isError = true;
			res.errorString = "Bad status";
			res.status = status;
			return res;
		}
		res.rid = getElementText(root, "rid");
		res.signature = getElementText(root, "signature");

		NodeList list;

		/* Parse suggested ARTICLES */
		if (parseArticles) {
			list = root.getElementsByTagName("articles");
			if (list.getLength() == 1) {
				final Element n = (Element) list.item(0);
				final NodeList nl = n.getElementsByTagName("article");
				for (int i = 0; i < nl.getLength(); i++) {
					final Element articleNode = (Element) nl.item(i);
					res.articles.add(new Article(getElementText(articleNode, "url"), getElementText(articleNode,
					        "title"), getElementText(articleNode, "published_datetime"), getElementText(articleNode,
					        "confidence"), getElementText(articleNode, "zemified")));
				}
			}
		}

		/* Parse suggested KEYWORDS */
		if (parseKeywords) {
			list = root.getElementsByTagName("keywords");
			if (list.getLength() == 1) {
				final Element n = (Element) list.item(0);
				final NodeList nl = n.getElementsByTagName("keyword");
				for (int i = 0; i < nl.getLength(); i++) {
					final Element articleNode = (Element) nl.item(i);
					res.keywords.add(new Keyword(getElementText(articleNode, "name"), getElementText(articleNode,
					        "confidence"), getElementText(articleNode, "schema")));
				}
			}
		}

		/* Parse suggested IMAGES */
		if (parseImages) {
			list = root.getElementsByTagName("images");
			if (list.getLength() == 1) {
				final Element n = (Element) list.item(0);
				final NodeList nl = n.getElementsByTagName("image");
				for (int i = 0; i < nl.getLength(); i++) {
					final Element articleNode = (Element) nl.item(i);
					res.images.add(new Image(getElementText(articleNode, "url_l"), getElementText(articleNode,
					        "url_l_w"), getElementText(articleNode, "url_l_h"), getElementText(articleNode, "url_m"),
					        getElementText(articleNode, "url_m_w"), getElementText(articleNode, "url_m_h"),
					        getElementText(articleNode, "url_s"), getElementText(articleNode, "url_s_w"),
					        getElementText(articleNode, "url_s_h"), getElementText(articleNode, "source_url"),
					        getElementText(articleNode, "license"), getElementText(articleNode, "description"),
					        getElementText(articleNode, "attribution"), getElementText(articleNode, "confidence")));
				}
			}
		}

		if (parseMarkup) {
			/* Parse suggested MARKUP */
			list = root.getElementsByTagName("markup");
			if (list.getLength() == 1) {
				final Element n = (Element) list.item(0);
				res.markup.text = getElementText(n, "text");

				list = root.getElementsByTagName("links");
				if (list.getLength() == 1) {
					final Element link = (Element) list.item(0);

					final NodeList nl = link.getElementsByTagName("link");
					for (int i = 0; i < nl.getLength(); i++) {
						final Element linkNode = (Element) nl.item(i);
						final Link l = new Markup.Link(getElementText(linkNode, "anchor"), getElementText(linkNode,
						        "confidence"), getElementText(linkNode, "relevance"));
						res.markup.links.add(l);

						final String entityT = getElementText(linkNode, "entity_type");
						if (entityT != null) {
							l.entityType = entityT;
						}

						final NodeList targetList = linkNode.getElementsByTagName("target");
						for (int j = 0; j < targetList.getLength(); j++) {
							final Element targetNode = (Element) targetList.item(j);
							l.targets.add(new Markup.Target(getElementText(targetNode, "url"), getElementText(
							        targetNode, "type"), getElementText(targetNode, "title")));
						}
					}
				}
			}
		}

		/* Parse CATEGORIES */
		list = root.getElementsByTagName("categories");
		if (list.getLength() == 1) {
			final Element catlist = (Element) list.item(0);

			final NodeList nl = catlist.getElementsByTagName("category");

			for (int m = 0; m < nl.getLength(); m++) {
				final Element n = (Element) nl.item(m);
				final Category c = new Category(getElementText(n, "name"), getElementText(n, "confidence"),
				        getElementText(n, "categorization"));

				res.categories.add(c);
			}
		}

		return res;

	}

	

	

	/* NEW SIMPLIFIED VERSION */

	/**
	 * Makes the request and builds the result
	 * 
	 * @param u
	 *            Url to Zemanta api service
	 * @param parameters
	 *            List of web query parameters
	 * @return String representation of result ready to be parsed.
	 * @throws IOException
	 * @throws IllegalArgumentException
	 */
	protected static String getResults(final String u, final Map<String, String> parameters,int timeout)
	        throws IllegalArgumentException, IOException {

		return requestString(u, parameters,timeout);

	}

	/**
	 * Makes the request (either suggest or markup) and builds the result.
	 * Default method is suggest and format xml.
	 * 
	 * @param inst
	 *            Instance of Zemanta object
	 * @param parameters
	 *            List of query parameters
	 * @return {@link ZemantaResult} object, containing all suggestions found.
	 */
	protected static ZemantaResult getResults(final ZemantaAPI inst, final Map<String, String> parameters,int timeout) {

		ZemantaResult res = new ZemantaResult();

		try {

			// check for required parameters
			parameters.put("api_key", inst.getApiKey());

			// change format to XML, because only xml is parsed into model
			parameters.put("format", "xml");

			final Document doc = request(inst.getGateway(), parameters,timeout);

			final Element root = doc.getDocumentElement();

			// parsing flags
			boolean parseArticles, parseKeywords, parseImages, parseMarkup;

			// default values for SUGGEST
			parseArticles = true;
			parseKeywords = true;
			parseImages = true;
			parseMarkup = true;

			// settings for MARKUP
			if (parameters.get("method").equals("zemanta.suggest_markup")) {
				parseArticles = false;
				parseKeywords = false;
				parseImages = false;
			}

			// parse result to ZemantaResult
			res = parseXMLToModel(root, parseArticles, parseKeywords, parseImages, parseMarkup);

			return res;

		} catch (final IOException e) {
			res.isError = true;
			res.errorString = "Input/Output error";
			e.printStackTrace();
		} catch (final SAXException e) {
			res.isError = true;
			res.errorString = "Malformed xml returned";
		} catch (final ParserConfigurationException e) {
			res.isError = true;
			res.errorString = "Malformed xml returned";
		} catch (final Exception e) {
			e.printStackTrace();
		}

		return res;
	}
}
