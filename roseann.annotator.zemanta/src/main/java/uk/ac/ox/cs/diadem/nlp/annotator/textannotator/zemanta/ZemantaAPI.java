/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.zemanta;



import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.*;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;

/**
 * The <code>ZemantaAPI</code> class represents java implementation of <a
 * href="http://www.zemanta.com">Zemanta ltd.</a> API. Zemanta is a suggestion
 * engine that works on any given text. Read more in the link given above or in
 * <a href="http://developer.zemanta.com">developers</a> pages.
 * 
 * @author Luying Chen
 */
class ZemantaAPI {
	/** API host url */
	private static  String API_HOST = "api.zemanta.com";

	/** Gateway for API calls */
	private static  String GATEWAY = "/services/rest/0.0/";

	/** Currently used API key for API calls */
	private String apiKey;

	/** Currently used gateway for API calls */
	private  String apiServiceURL;


	/**
	 * Constructor
	 */
	private ZemantaAPI() {
		this.apiServiceURL = new StringBuilder().append("http://").append(API_HOST).append(GATEWAY).toString();;
	}

	/**
	 * Query Zemanta service for suggestions or markup in format other than XML.
	 * 
	 * <p>
	 * Other supported formats: JSON, WJSON, RDF/XML
	 * 
	 * @param parameters
	 *            Collection of parameters in the form of(key, value)
	 * @return String representation of results in format requested.
	 */
	public String getRawData(Map<String, String> parameters,int timeout) {
		String result = "";

		try {
			result = Request.requestString(this.getGateway(), parameters,timeout);

		} 
		//check if the timeout has expired
		catch(final SocketTimeoutException e) {
			return "timeout";
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * Query the Zemanta service for markup with default settings.
	 * 
	 * @param text
	 *            The text that suggestion engine will parse.
	 * 
	 * @return {@link ZemantaResult} object, containing all suggestions found
	 */
	public ZemantaResult suggestMarkup(String text,int timeout) {

		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("method", "zemanta.suggest_markup");
		parameters.put("text", text);

		return Suggest.getResults(this, parameters,timeout);

	}


	/**
	 * Query the Zemanta service for suggestions with default settings: method
	 * 'suggest' and format 'xml'
	 * 
	 * @param text
	 *            The text that suggestion engine will parse.
	 * 
	 * @return {@link ZemantaResult} object, containing all found suggestions
	 */
	public ZemantaResult suggest(String text,int timeout) {

		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("method", "zemanta.suggest");
		parameters.put("text", text);

		return Suggest.getResults(this, parameters,timeout);
	}



	/**
	 * Query the Zemanta service for suggestions with a set of parameters New
	 * from November 2011
	 * 
	 * @param parameters
	 *            Pairs of (key,value) parameters for query
	 * @return {@link ZemantaResult} object, containing all suggestions found.
	 */
	public ZemantaResult suggest(Map<String, String> parameters,int timeout) {
		return Suggest.getResults(this, parameters,timeout);
	}





	@Override
	public String toString() {

		if (!apiServiceURL.equals(""))
			return apiServiceURL;

		return new StringBuilder().append("ZemAPI - host: ").append(API_HOST).append(' ').append("gateway: ")
				.append(this.apiServiceURL).toString();
	}

	public String getGateway() {
		return apiServiceURL;
	}

	/**
	 * Get zemantaapi instance from configuration object.
	 * @param config
	 * 			Input configuration object of this zemanta instance
	 * @return ZemantaAPI
	 * 			The zemanta instance with the specific configuration
	 */
	static public ZemantaAPI GetInstanceFromConfig(AnnotatorConfiguration config) {
		ZemantaAPI api = new ZemantaAPI();
		final String key =config.getApiKey();
		if(key==null)
			throw new IllegalArgumentException("The api key has not been specified");

		api.setApiKey(key);
		final URL url = config.getURlendpoint();
		if(url!=null){
			api.setApiServiceURL(url.toString());
		}

		return api;
	}

	void setApiKey(String key) {
		this.apiKey = key;

	}

	/**
	 * @return the apiServiceURL
	 */
	String getApiServiceURL() {
		return apiServiceURL;
	}

	/**
	 * @return the apiKey
	 */
	String getApiKey() {
		return apiKey;
	}

	/**
	 * @param apiServiceURL the apiServiceURL to set
	 */
	private void setApiServiceURL(String apiServiceURL) {
		this.apiServiceURL = apiServiceURL;
	}
	/**
	 * Utility method to invoke NE service and return the response string.
	 * @param text
	 * 			The source text to be annotated
	 * @param config
	 * 			The input configration object
	 * @return String
	 * 			The response string of the service
	 */
	public String invokeNEService(String freeText, final AnnotatorConfiguration config,int timeout) {
		if (null == freeText || freeText.length() < 1)
			throw new IllegalArgumentException("Enter some text to analyze.");

		final Zemanta_Params parameters = new Zemanta_Params(apiKey);
		Map<String, String> paramConfig = getParamsFromConfigurationFile(config);
		if(paramConfig!=null)
			parameters.addParameters(getParamsFromConfigurationFile(config));
		parameters.setText(freeText);
		final String res = getRawData(parameters.getParameters(),timeout);
		return res;
	}

	/**
	 * Utility method to load user-specific configuration
	 * @param config 
	 * 			The input configuration object
	 * @return Map<String, String>
	 * 			The configration map
	 */
	private Map<String, String> getParamsFromConfigurationFile(final AnnotatorConfiguration config) {

		return config.getSpecificParameters();
	}

}
