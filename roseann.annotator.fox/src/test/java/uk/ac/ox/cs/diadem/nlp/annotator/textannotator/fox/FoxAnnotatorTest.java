package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.fox;

import java.io.UnsupportedEncodingException;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;

public class FoxAnnotatorTest {
	
	private Annotator fox;
	
	private String text;
	
	@Before
	public void bringUp(){
		fox=FoxAnnotator.getInstance();
		text="Barack Obama is the new president of the United States.";
	}

	/**
	 * Test to check that a text can be submitted to  annotator service
	 * @throws UnsupportedEncodingException 
	 * @throws InvalidSyntaxException
	 */
	@Test
	public void testSubmitToAnnotator() throws UnsupportedEncodingException {

	
		//check that the text can be submitted to the annotator and no exceptions are thrown
		String response = fox.submitToAnnotator(text);

		Assert.assertNotNull(response);
		Assert.assertTrue(response.length()>0);

		//check the response can be parse 
		Set<NamedEntity> annotations = TurtleReader.read(response);
		Assert.assertNotNull(annotations);
		Assert.assertTrue(annotations.size()>0);

	/*	List<JsonNode> relations = doc.getArrayNode("relations");
		Assert.assertNotNull(relations);
		Assert.assertTrue(relations.size()>0);	*/
	}

	/**
	 * Test to verify the entities retrieved from fox web service
	 */
	@Test
	public void testAnnotateEntities(){
		Set<Annotation> annotations = fox.annotateEntity(text);
		Assert.assertNotNull(annotations);
		Assert.assertTrue(annotations.size()>0);
	}
	
	/**
	 * Test to verify that no fox ares retrieved from fox web service with an senseless text
	 */
	@Test
	public void testAnnotateNoEntities(){
		text="cfrgfvdfgbfdgdgdfgdfgdgdg";

		Set<Annotation> annotations =fox.annotateEntity(text);
		Assert.assertNotNull(annotations);
		Assert.assertTrue(annotations.size()==0);
	}

}
