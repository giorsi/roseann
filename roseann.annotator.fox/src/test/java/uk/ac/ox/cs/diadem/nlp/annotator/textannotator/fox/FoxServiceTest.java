package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.fox;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Test;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;

public class FoxServiceTest {

	
	/**
	 * Test to invoke the service and check that a  response is returned, with specific elements
	 * @throws IOException 
	 */
	@Test
	public void invokeServiceTest() throws IOException {

		String text="Barack Obama is the new president of United States of America.";

		//initialize the configuration
		AnnotatorConfiguration config=new AnnotatorConfiguration();
		//set the url endpoint
		//config.setUrlendpoint(new URL(endpoint));

		Fox service = Fox.GetInstanceFromConfig(config);
		String response = service.invokeNEService(text,config,0 );

		Assert.assertNotNull(response);
		Assert.assertTrue(response.length()>0);

		
		
	}

	

}
