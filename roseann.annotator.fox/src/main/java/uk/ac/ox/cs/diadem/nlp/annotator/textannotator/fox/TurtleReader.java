package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.fox;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.NodeIterator;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.ResIterator;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author ngonga
 */
public class TurtleReader {

	public static final String ANN = "http://www.w3.org/2000/10/annotation-ns#";
	public static final String SCMS = "http://ns.aksw.org/scms/";
	public static final String SCMSANN = "http://ns.aksw.org/scms/annotations/";
	public static final String RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	public static Set<NamedEntity> read(String input) throws UnsupportedEncodingException {

		TreeSet<NamedEntity> nes = new TreeSet<NamedEntity>();
		Model model = ModelFactory.createDefaultModel();
		//StringReader reader = new StringReader(input);
		ByteArrayInputStream stream;
		stream = new ByteArrayInputStream(input.getBytes("UTF-8"));
		model.read(stream, "", "TTL");
		//System.out.println(model);
		//get properties
		Property body = model.getProperty(ANN, "body");
		Property index = model.getProperty(SCMS, "beginIndex");
		Property subclass = model.getProperty(RDF, "type");

		ResIterator nodeIter = model.listResourcesWithProperty(body);
		String label, offset, type, types;
		while (nodeIter.hasNext()) {
			type = null;
			//get label
			RDFNode n = nodeIter.next();
			NodeIterator iter = model.listObjectsOfProperty(n.asResource(), body);                
			label = iter.next().toString();
			label = label.substring(0, label.lastIndexOf("^^http://www.w3.org/2001/XMLSchema#string"));
			//get type
			iter = model.listObjectsOfProperty(n.asResource(), subclass);                
			while(iter.hasNext())
			{
				types = iter.next().toString();
				if(types.contains(SCMSANN))
					type = types.substring(types.lastIndexOf("/")+1);
			}
			//get indexes
			iter = model.listObjectsOfProperty(n.asResource(), index);


			while (iter.hasNext()) {
				offset = iter.next().toString();
				offset = offset.substring(0, offset.lastIndexOf("^^http://www.w3.org/2001/XMLSchema#int"));
				nes.add(new NamedEntity(label, type, Integer.parseInt(offset)));
			}
		}
		// write it to standard out        
		return nes;
	}
}
