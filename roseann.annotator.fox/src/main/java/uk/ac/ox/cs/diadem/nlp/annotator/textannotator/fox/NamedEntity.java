package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.fox;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ngonga
 */
public class NamedEntity implements Comparable<NamedEntity> {

  public String label;
  public int offset;
  public String type;

  /**
   * Creates a new entity and ensures that it can be disambiguated from entities
   * with the same label by also storing the offset of the said entity
   * 
   * @param _label
   *          Label of the entity
   * @param _offset
   *          Offset with the text
   */
  public NamedEntity(final String _label, final int _offset) {
    label = _label;
    offset = _offset;
    type = null;
  }

  public NamedEntity(final String _label, final String _type, final int _offset) {
    label = _label;
    offset = _offset;
    type = _type;
  }

  /**
   * Implements comparison
   * 
   * @param o
   *          Other named entity to compare with
   * @return 0 if the entities are the same, else 1
   */
  public int compareTo(final NamedEntity e) {
    if (e.offset == offset) {
      if (e.label.length() == label.length())
        return 0;
      if (e.label.length() > label.length())
        return -1;
      else
        return 1;
    } else {
      if (offset > e.offset)
        return 1;
      else
        return -1;
    }
  }

  /**
   * Returns the string representation of a named entity
   * 
   * @return String representation of a named entity
   */
  @Override
  public String toString() {
    if (type == null)
      return "(" + label + ", " + offset + ")";
    else
      return "(" + label + ", " + offset + ", " + type + ")";
  }

}
