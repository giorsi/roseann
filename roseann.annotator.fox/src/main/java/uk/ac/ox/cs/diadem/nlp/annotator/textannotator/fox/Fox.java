package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.fox;



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.*;
import java.net.*;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;


/**
 *
 * @author ngonga
 */
class Fox {
	
	private static String URL_ENDPOINT = "http://139.18.2.164:4444/api";

	private String getAnnotatedText(final String input,int timeout) throws IOException {
		
		boolean error = true;
		String buffer="";
		while (error) {
			// Construct data
			String data = URLEncoder.encode("type", "UTF-8") + "=" + URLEncoder.encode("text", "UTF-8");
			data += "&" + URLEncoder.encode("task", "UTF-8") + "=" + URLEncoder.encode("ner", "UTF-8");
			data += "&" + URLEncoder.encode("output", "UTF-8") + "=" + URLEncoder.encode("turtle", "UTF-8");
			data += "&" + URLEncoder.encode("task", "UTF-8") + "=" + URLEncoder.encode("ner", "UTF-8");
			data += "&" + URLEncoder.encode("text", "UTF-8") + "=" + URLEncoder.encode(input, "UTF-8");

			// Send data
			URL url = new URL(URL_ENDPOINT);
			URLConnection conn = url.openConnection();
			conn.setConnectTimeout(timeout);
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(data);
			wr.flush();

			// Get the response
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = rd.readLine()) != null) {
				buffer = buffer + line + "\n";
			}

			wr.close();
			rd.close();
			error = false;

		}
		
		return buffer;
	}

	/**
	 * Get Fox instance from configuration object(No key is required for this service).
	 * @param config
	 * 			Input configuration object of this Fox instance
	 * @return Fox
	 * 			The Fox instance with the specific configuration
	 */

	public static Fox GetInstanceFromConfig(AnnotatorConfiguration config) {
		Fox api = new Fox();
		final URL url = config.getURlendpoint();
		if(url!=null){
			URL_ENDPOINT = url.toString();
		}

		return api;
	}

	public String invokeNEService(String freeText, AnnotatorConfiguration config,int timeout) throws IOException {
		
		return getAnnotatedText(freeText,timeout);
	}
}