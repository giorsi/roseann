package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.fox;

import java.net.SocketTimeoutException;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.util.Offset;


/**
 * @author Stefano Ortona (stefano.ortona@gmail.com) Oxford University,
 *         Department of Computer Science The wrapper submit the text to Fox
 *         service, take the response as String representing JSon Object, builds
 *         a JSon Object and select only the entity annotations to be saved in
 *         the model.
 */
public class FoxAnnotator extends AnnotatorAdapter {

	static final Logger logger = LoggerFactory.getLogger(FoxAnnotator.class);
	private Fox fox;
	/**
	 * counter to increment the annotation id
	 */
	private long id_counter;

	/**
	 * singleton instance
	 */
	private static FoxAnnotator INSTANCE;

	/**
	 * private constructor to build the singleton instance
	 */
	private FoxAnnotator() {
		ConfigurationFacility.getConfiguration();
		this.annotatorName = "fox";

		// start the counter from 0
		id_counter = 0;
		// initialize the parametrs
		AnnotatorConfiguration configuration = new AnnotatorConfiguration();
		configuration.initializeConfiguration(getAnnotatorName());
		setConfig(configuration);
	}

	/**
	 * static method to get the singleton instance
	 * 
	 * @return the singleton instance
	 */
	public static synchronized Annotator getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new FoxAnnotator();
		}
		return INSTANCE;
	}
	/* (non-Javadoc)
	 * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter#setConfig(uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration)
	 */
	@Override
	public void setConfig(AnnotatorConfiguration config) {
		// TODO Auto-generated method stub
		super.setConfig(config);
		fox = Fox.GetInstanceFromConfig(config);
	}



	/* (non-Javadoc)
	 * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter#annotateEntity(java.lang.String)
	 */
	@Override
	public Set<Annotation> annotateEntity(String text) {
		final String response = submitToAnnotator(text);
		// annotate the model and increment the counter
		final Set<Annotation> res = retrieveEntityAnnotations(response,  id_counter,
				text);
		if(res!=null){
			id_counter += res.size();
		}
		
		return res;
	}
	
	@Override
	public Set<Annotation> annotateEntity(String text,int timeout) {
		final String response = submitToAnnotator(text,timeout);
		
		if(response==null)
			return new HashSet<Annotation>();
		// annotate the model and increment the counter
		final Set<Annotation> res = retrieveEntityAnnotations(response,  id_counter,
				text);
		if(res!=null){
			id_counter += res.size();
		}
		
		return res;
	}

	

	public String submitToAnnotator(String freeText) {
		return invokeService(freeText, 0);
	}
	
	public String submitToAnnotator(String freeText, int timeout){
		return invokeService(freeText, timeout);
	}
	
	private String invokeService(String freeText, int timeout){
		if ((freeText == null) || (freeText.length() < 1)) {
			logger.error("Trying to submit an empty text to Fox");
			throw new AnnotatorException(
					"Trying to submit an empty text to Fox", logger);
		}

		String res = null;
	
		try {
			logger.debug("Submitting document to Fox service.");

			// eliminate from the original text special characters
			freeText = freeText.replaceAll("\u0003", " ");

			// invoke the service
			res = fox.invokeNEService(freeText, this.config,timeout);

		}//check if the timeout has expired
		catch(final SocketTimeoutException e) {
			logger.warn("{} was not able to receive the web response within timeout {} milliseconds",
					getAnnotatorName(),timeout);
			return null;
		}
		catch (final Exception secondException) {
			logger.error("Error submitting text to Fox");
			throw new AnnotatorException("Error submitting text to Fox",
					secondException, logger);

		}
		logger.debug("Fox Process done and response received.");
		return res;
	}
	/**
	 * An utility method to harvest entity annotations from annotator response
	 * 
	 * @param sourceText
	 *            Text to be annotated
	 * @param response
	 *            Response to be interpreted
	 * @param progressive
	 * @return Set<Annotation> Set of entity annotations
	 */
	private Set<Annotation> retrieveEntityAnnotations(final String response,
			final long progressive, final String sourceText) {

		logger.debug("Processing the Fox response.");
		Set<Annotation> annotatation_found = new HashSet<Annotation>();
		// check if some annotations has been found
		if (response.contains("<!-- nothing found -->")) {
			logger.debug("Fox service did not find any entity annotations.");
			return annotatation_found;
		}

		Set<NamedEntity> annotations = null;
		try {
			// read the annotations in turtle format
			annotations = TurtleReader.read(response);
		} catch (final Exception e) {
			logger.error("The String response from Fox can't be parse as turtle format");
			throw new AnnotatorException("The String response from Fox "
					+ "can't be parse as turtle format", e, logger);
		}

		if ((annotations == null) || (annotations.size() == 0)) {
			logger.debug("Fox service did not find any entity annotations.");
			return annotatation_found;
		}
		
		int count = 0;
		int actual_index = 0;
		for (final NamedEntity annotation : annotations) {
			
			actual_index = annotation.offset;
			final Annotation oneAnno = buildAnnotationFromJson(annotation,  count
					+ progressive, this.annotatorName , actual_index,
					sourceText.substring(actual_index));
			if(oneAnno!=null){
				annotatation_found.add(oneAnno);
			}
		}
		logger.debug("Response processed and annotations saved.");

		return annotatation_found;
	}

	/**
	 * Construct annotation for every instance of the
	 * annotation Currently Fox doesn't provide any attribute for the
	 * annotation
	 * 
	 * @param annotation
	 *            NamedEntity object representing the instance of annotation
	 * @param inputModel
	 *            outputModel to save the facts
	 * @param annotator_id
	 *            the annotator id
	 * @param annotation_id
	 *            Annotation_id for the given annotation
	 * @param sourceText
	 *            The source text annotated
	 * @return Annotation
	 * 			  The resulting annotation object
	 * 
	 */

	private Annotation buildAnnotationFromJson(final NamedEntity annotation,
			 final long annotation_id,
			final String annotator_id, final int actual_index,
			final String sourceText) {

		// retrieve all the information for the annotation

		String type = annotation.type;
		final String label = annotation.label;

		// if the label is null, do not save the annotation
		if (label == null)
			return null;

		// compute the start offset in the current source text
		final Map<Integer, Integer> start2end = Offset.getInstancePosition(1,
				sourceText, label);
		if ((start2end == null) || (start2end.keySet().size() == 0))
			return null;

		// add to the computed start the actual_index
		final int start = start2end.keySet().iterator().next() + actual_index;
		final int end = start + label.length();

		// if it doesn't have the type, return 0 because no annotations were
		// found
		if (type == null)
			return null;

		// source for the main type is fox knowledge
		final String source = "fox_knowledge";

		// create a new fact for the annotation

		// transform the concept dlv compatible
		//type = AnnotationUtil.modifyConcept(type);

		// id of annotation: annotatorId_annotationId
		final String id = annotator_id + "_" + (annotation_id);
		Annotation oneAnnotation = new EntityAnnotation(id,label,start,end,annotatorName,source,1.0,AnnotationClass.INSTANCE);
		
		// if the annotation has been saved correctly, return it
		return oneAnnotation;
	}

	

	public void resetId() {
		id_counter = 0;
	}
	
	public static void main(String[] args){
		FoxAnnotator.getInstance().annotateEntity("Barack Obama.",1);
	}

	
}