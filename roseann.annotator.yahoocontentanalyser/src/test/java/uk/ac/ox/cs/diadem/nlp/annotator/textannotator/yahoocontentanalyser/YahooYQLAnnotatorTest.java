/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.yahoocontentanalyser;

import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;

public class YahooYQLAnnotatorTest {
	
	private Annotator yahoo;
	
	private String text;
	
	@Before
	public void bringUp(){
		yahoo=YahooYQLAnnotator.getInstance();
		text="Barack Obama is the new president of the United States.";
	}

	/**
	 * Test to check that a text can be submitted to extractiv annotator service
	 * @throws InvalidSyntaxException
	 */
	@Test
	public void testSubmitToAnnotator() throws InvalidSyntaxException{

		//check the url-ednpoint has been specified in the configuration
		Assert.assertNotNull(yahoo.getConfig().getURlendpoint());

		//check that the text can be submitted to the annotator and no exceptions are thrown
		String response = yahoo.submitToAnnotator(text);

		Assert.assertNotNull(response);
		Assert.assertTrue(response.length()>0);

		//check the response can be parse as json node and contains an element entities and an element relations
		JsonRootNode doc = new JdomParser().parse(response);

		JsonNode entities = doc.getNode("query","results","entities");
		Assert.assertNotNull(entities);

		List<JsonNode> annotations = entities.getArrayNode("entity");
		Assert.assertNotNull(annotations);
		Assert.assertTrue(annotations.size()>0);
		
		//check the annotation elements
		JsonNode annotation = annotations.iterator().next();
		Assert.assertNotNull(annotation.getStringValue("score"));
		Assert.assertNotNull(annotation.getNode("text").getStringValue("endchar"));
		Assert.assertNotNull(annotation.getNode("text").getStringValue("startchar"));
	}

	/**
	 * Test to verify the entities retrieved from extractiv web service
	 */
	@Test
	public void testAnnotateEntities(){
		Set<Annotation> annotations = yahoo.annotateEntity(text);
		Assert.assertNotNull(annotations);
		Assert.assertTrue(annotations.size()>0);
	}
	
	/**
	 * Test to verify that no entitie ares retrieved from extractiv web service with an senseless text
	 */
	@Test
	public void testAnnotateNoEntities(){
		text="cfrgfvdfgbfdgdgdfgdfgdgdg";

		Set<Annotation> annotations = yahoo.annotateEntity(text);
		Assert.assertNotNull(annotations);
		Assert.assertTrue(annotations.size()==0);
	}

}
