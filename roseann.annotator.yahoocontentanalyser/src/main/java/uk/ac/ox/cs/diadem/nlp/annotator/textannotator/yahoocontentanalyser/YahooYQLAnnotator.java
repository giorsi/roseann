/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.yahoocontentanalyser;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.AnnotationAttribute;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;
import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonRootNode;
import argo.saj.InvalidSyntaxException;

/**
 * @author Stefano Ortona (name.surname at cs.ox.ac.uk) Oxford University,
 *         Department of Computer Science The wrapper submit the text to
 *         YahooYQL, take the response as String representing JSon Object,
 *         builds a JSon Object and select only the entity annotations to be
 *         returned
 * 
 *         Note: there are two files that must be specified into the folder
 *         src/main/resources/uk/ac/ox/cs/diadem/nlp/annotator/textannotator/
 *         yahoocontentanalyser The file yahoo_types contains a mapping between
 *         the original types returned by yahooAPI and a more human readable
 *         concept representation The file typeStructure contains the concepts
 *         yahoo hierarchy in order to return only the annotations with the most
 *         specific concepts Whenever a type is not found in these files an
 *         error is recorder in the logger
 */

public class YahooYQLAnnotator extends AnnotatorAdapter {

	/**
	 * The logger
	 */
	static final Logger	             logger	= LoggerFactory.getLogger(YahooYQLAnnotator.class);

	/**
	 * to count the annotation
	 */
	private long	                 id_counter;

	/**
	 * The Yahoo service to be invoked
	 */
	private YahooYQLService	         service;

	/**
	 * Singleton instance
	 */
	private static YahooYQLAnnotator	INSTANCE;

	/**
	 * private constructor to build the instance
	 */
	protected YahooYQLAnnotator() {
		ConfigurationFacility.getConfiguration();
		setAnnotatorName("yahooAPI");

		// initialize the configuration parameters
		AnnotatorConfiguration conf = new AnnotatorConfiguration();
		conf.initializeConfiguration(getAnnotatorName());
		setConfig(conf);
		// start the id from 0
		id_counter = 0;

		service = new YahooYQLService();
	}

	/**
	 * method to get the singleton instance
	 * 
	 * @return singleton instance
	 */
	public static synchronized Annotator getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new YahooYQLAnnotator();
		}
		return INSTANCE;
	}

	@Override
	public Set<Annotation> annotateEntity(String text) {
		text = text.replaceAll("'", " "); // the character ' is a reserved char
		                                  // in YQL.
		final String response = submitToAnnotator(text);
		// save the annotations and increment the id counter
		final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(response, id_counter);
		final int num_annotations = entityAnnotations.size();
		id_counter += num_annotations;
		return entityAnnotations;

	}

	@Override
	public Set<Annotation> annotateEntity(String text, int timeout) {
		text = text.replaceAll("'", " "); // the character ' is a reserved char
		                                  // in YQL.
		final String response = submitToAnnotator(text, timeout);
		if (response == null)
			return new HashSet<Annotation>();
		// save the annotations and increment the id counter
		final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(response, id_counter);
		final int num_annotations = entityAnnotations.size();
		id_counter += num_annotations;
		return entityAnnotations;

	}

	private Set<Annotation> retrieveEntityAnnotations(String response, long progressive) {

		Set<Annotation> entityAnnotations = new HashSet<Annotation>();
		JsonRootNode doc;

		logger.debug("Processing the YahooYQL response.");
		try {
			// create a JSon node parsing the text response
			doc = new JdomParser().parse(response);
		} catch (final InvalidSyntaxException e) {
			logger.error("The String response from YahooYQL can't be parse as JSonNode");
			throw new AnnotatorException("The String response from YahooYQLAnnotator " + "can't be parse as JSonNode",
			        e, logger);
		}
		if (!doc.isNullableObjectNode("query", "results", "entities")) { // empty
			                                                             // entity
			                                                             // set
			logger.debug("YahooYQLAnnotator service did not find any entity annotations.");
			return entityAnnotations;
		}

		List<JsonNode> selected_annotations = null;

		try {

			if (doc.isArrayNode("query", "results", "entities", "entity")) {
				selected_annotations = doc.getArrayNode("query", "results", "entities", "entity");
			} else if (doc.isObjectNode("query", "results", "entities", "entity")) {
				selected_annotations = new ArrayList<JsonNode>();
				selected_annotations.addAll(doc.getObjectNode("query", "results", "entities").values());
			} else
				return entityAnnotations;
		} catch (Exception e) {
			logger.warn("Service YahooYQL was not able to process the response.", e.getMessage());
		}

		int num_annotations = 0;
		// iterate over all annotation
		for (final JsonNode annotation : selected_annotations) {
			Set<Annotation> foundAnnotations = fromJsonToAnnotation(annotation, num_annotations + progressive);
			num_annotations += foundAnnotations.size();
			entityAnnotations.addAll(foundAnnotations);
		}
		logger.debug("Response processed and annotations saved.");

		return entityAnnotations;

	}

	private Set<Annotation> fromJsonToAnnotation(final JsonNode root, final long progressive) {

		// create the annotations
		Set<Annotation> foundAnnotations = new HashSet<Annotation>();

		final Set<String> myTypes = new HashSet<String>();
		List<JsonNode> types = null;
		if (!root.isNullableObjectNode("types")) {
			logger.warn("Not able to locate the types element in the Yahoo Json response {}", root);
			return foundAnnotations;
		} else {

			if (root.isArrayNode("types", "type")) {
				types = root.getArrayNode("types", "type");
				if (types == null) {// entity without specifying types
					logger.warn("Not able to locate the type element in the Yahoo Json response {}", root);
					return foundAnnotations;
				} else {
					for (final JsonNode typeNode : types) {
						if (typeNode.isStringValue("content")) {
							try {
								final String origLabel = typeNode.getStringValue("content").trim();
								myTypes.add(origLabel);
							} catch (IllegalArgumentException ex) {
								logger.warn(
								        "There was no element content specifying the type in the Yahoo Json response {}",
								        root);
							}
						}

					}
				}
			} else if (root.isObjectNode("types", "type")) {
				try {
					final String origLabel = root.getStringValue("types", "type", "content").trim();
					myTypes.add(origLabel);
				} catch (IllegalArgumentException ex) {
					logger.warn("There was no element content specifying the type in the Yahoo Json response {}", root);
				}

			}

		}

		// add thing if no types have been found
		if (myTypes.size() == 0) {
			logger.warn("There were no types in the Yahoo Json response {}", root);
			return foundAnnotations;
		}

		int count = 0;

		for (String type : myTypes) {
			Annotation annotation = null;
			try {
				annotation = new EntityAnnotation();
				annotation.setId(getAnnotatorName() + "_" + (count + progressive));
				annotation.setConcept(type);
				if (root.isStringValue("score")) { // check whether contains a
					                               // score
					annotation.setConfidence(Double.parseDouble(root.getStringValue("score")));
				}
				annotation.setStart(Long.parseLong(root.getStringValue("text", "startchar")));
				annotation.setEnd(Long.parseLong(root.getStringValue("text", "endchar")) + 1);
				annotation.setAnnotationClass(AnnotationClass.INSTANCE);
				annotation.setOriginAnnotator(getAnnotatorName());

				foundAnnotations.add(annotation);
				count++;
			} catch (Exception e) {
				logger.error("Not able to build the annotation object from the JSon response");
			}
			try {
				AnnotationAttribute wiki_attribute = new AnnotationAttribute("wiki_url",
				        root.getStringValue("wiki_url"));
				annotation.addAttribute(wiki_attribute);
			} catch (Exception e) {
				// if the attribute has not been found, simply continue
			}

		}

		return foundAnnotations;

	}

	@Override
	public String submitToAnnotator(String text) {
		return this.submitToAnnotator(text, 0);

	}

	@Override
	public String submitToAnnotator(String text, int timeout) {
		if (text == null || text.length() < 1) {
			logger.error("Trying to submit an empty text to Extractiv");
			throw new AnnotatorException("Trying to submit an empty text to Extractiv", logger);
		}

		String res = null;
		try {

			logger.debug("Submitting document to YahooYQL service.");

			// eliminate from the original text special characters
			text = text.replaceAll("\u0003", " ");

			res = service.invokeService(text, getConfig(), timeout);
			logger.debug("YahooYQL Process done and response received.");

			return res;
		}// check if the timeout has expired
		catch (final SocketTimeoutException e) {
			logger.warn("{} was not able to receive the web response within timeout {} milliseconds",
			        getAnnotatorName(), timeout);
			return null;
		}

		catch (final Exception e) {
			logger.error("Error submitting text to YahooYQL");
			throw new AnnotatorException("Error submitting text to YahooYQL", e, logger);
		}

	}

}
