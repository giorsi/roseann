/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */

package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.yahoocontentanalyser;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Set;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;

/**
 * 
 * @author Stefano Ortona (stefano.ortona at cs.ox.ac.uk) Oxford University,
 *         Department of Computer Science
 * 
 *         Use this class to send a request to yahooAPI web service and get a
 *         response as a string.
 * 
 */
public class YahooYQLService {

	/**
	 * Method to invoke the yahooAPI service with the input text and the parameters
	 * @param text
	 * 			Text to analyze
	 * @param parameters 
	 * 			The AnnotatorConfiguration object that contains the parameters for yahooAPI service.
	 * 			If not format has been specified, we'll use json format 
	 * @return	The response from yahooAPI as a string representing a json object
	 * @throws IOException
	 * 			If something goes wrong during the invocation of the service
	 * @throws IllegalArgumentException
	 * 			If one of the mandatory parameter has not been specified or it has been badly specified
	 */
	public String invokeService(final String text, final AnnotatorConfiguration parameters,int timeout) throws IOException,
	IllegalArgumentException {

		if (null == text || text.length() < 1)
			throw new IllegalArgumentException("Enter some text to analyze.");

		final String encoded_params=buildParameterString(text, parameters);

		final URL endpoint=parameters.getURlendpoint();
		if(endpoint==null)
			throw new IllegalArgumentException("The endpoint url has not been specified");

		return postRequest(encoded_params, endpoint,timeout);
	}

	/**
	 * Send the post request to the specific user
	 * @param param
	 * @param url_endpoint
	 * @return
	 * @throws IOException
	 */
	private String postRequest(final String param, final URL url_endpoint,int timeout) throws IOException
	{

		HttpURLConnection handle = (HttpURLConnection) url_endpoint.openConnection();
		handle.setDoOutput(true);
		handle.setConnectTimeout(timeout);

		StringBuilder data = new StringBuilder();
		data.append(param);

		handle.addRequestProperty("Content-Length",
				Integer.toString(data.length()));
		handle.setRequestMethod("POST");
		DataOutputStream ostream = new
				DataOutputStream(handle.getOutputStream());
		ostream.write(data.toString().getBytes());
		ostream.close();

		// get the response.
		BufferedReader rd = new BufferedReader(new InputStreamReader(
				handle.getInputStream()));
		String line, result = "";

		while ((line = rd.readLine()) != null) {
			result += line;
		}
		rd.close();
		handle.disconnect();


		return result;
	}

	/**
	 * Build the parameters string
	 * @param text
	 * @param parameters
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private String buildParameterString(final String text,final AnnotatorConfiguration parameters) throws UnsupportedEncodingException{
		Set<String> query_parameters=new HashSet<String>();
		query_parameters.add("max");
		query_parameters.add("related_entities");
		query_parameters.add("show_metadata");
		query_parameters.add("enable_categorizer");
		query_parameters.add("unique");


		String parameter="q="+URLEncoder.encode("select * " +
				"from contentanalysis.analyze where " + "text=" + "'" + text + "'", "UTF-8");

		//first some parameters must be encoded directly in the query
		for(String parameter_name:parameters.getSpecificParameters().keySet()){
			if(query_parameters.contains(parameter_name)){
				String value=parameters.getSpecificParameters().get(parameter_name);
				if(value!=null)
					parameter+=URLEncoder.encode(" and "+parameter_name+"='"+value+"'","UTF-8");	
			}
		}

		//the rest of the parameters are normal request parameters
		for(String parameter_name:parameters.getSpecificParameters().keySet()){
			if(!query_parameters.contains(parameter_name)){
				String value=parameters.getSpecificParameters().get(parameter_name);
				if(value!=null)
					parameter+="&"+parameter_name+"="+URLEncoder.encode(value,"UTF-8");	
			}
		}
		
		//if not format has been specified, we'll use json
		if(parameters.getSpecificParameters().get("format")==null)
			parameter+="&format=json";

		return parameter;

	}

}
