/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.opencalais;

/**
 * 
 * @author Stefano Ortona (stefano.ortona@gmail.com) Oxford University,
 *         Department of Computer Science
 * 
 *         Use this class to set the Parameters for Open Calais web-service
 *         invocation. You can set up the response mode and many other
 *         parameters. Default parameters are: Input: text Response: JSon Get
 *         Relevance: true Metadata: Relations AllowDistribution: true
 *         AllowSearch: true External Id: cabs901 Submitter: ABC
 * 
 */
class OpenCalais_Params {

	public static final String JSON_OUTPUT = "Application/JSon";
	public static final String XML_OUTPUT = "XML/RDF";
	public static final String SIMPLE_OUTPUT = "Text/Simple";
	public static final String MICRO_OUTPUT = "Text/Microformats";
	public static final String N3_OUTPUT = "text/N3";

	public static final String CONTENT_TEXT = "TEXT/RAW";
	public static final String CONTENT_XML = "TEXT/XML";
	public static final String CONTENT_HTMLRAW = "TEXT/HTMLRAW";
	public static final String CONTENT_HTML = "TEXT/HTML";

	public static final String GENERIC_RELATION = "GenericRelations";
	public static final String SOCIAL_TAG = "SocialTags";
	

	private String content_type = CONTENT_TEXT;
	private String outputFormat = JSON_OUTPUT;
	private String reltagBaseURL;
	private boolean relevance = true;
	private String enable_metatype = GENERIC_RELATION;
	private boolean docRDF = false;
	private boolean omittingOriginalText = true;
	private boolean allowDistribution = false;
	private boolean allowSearch = false;
	private String external_id;
	private String submitter;

	public String getContent_type() {
		return content_type;
	}

	public void setContent_type(String content_type) {
		if (!content_type.equals(CONTENT_TEXT) || !content_type.equals(CONTENT_XML)
		        || !content_type.equals(CONTENT_HTMLRAW) || !content_type.equals(CONTENT_HTML)) {
			throw new IllegalArgumentException("Unkown type for Content Type.");
		}
		this.content_type = content_type;
	}

	public String getEnable_metatype() {
		return enable_metatype;
	}

	public void setEnable_metatype(String enable_metatype) {
		if (!enable_metatype.equals(GENERIC_RELATION) && !enable_metatype.equals(SOCIAL_TAG)
		        && !enable_metatype.equals(GENERIC_RELATION + "," + SOCIAL_TAG)) {
			throw new IllegalArgumentException("Unknown type for EnableMetadataType.");
		}
		this.enable_metatype = enable_metatype;
	}

	public boolean isOmittingOriginalText() {
		return omittingOriginalText;
	}

	public void setOmittingOriginalText(boolean omittingOriginalText) {
		this.omittingOriginalText = omittingOriginalText;
	}

	public String getOutputFormat() {
		return outputFormat;
	}

	public void setOutputFormat(String outputFormat) {
		if (!outputFormat.equals(JSON_OUTPUT) || !outputFormat.equals(MICRO_OUTPUT) || !outputFormat.equals(N3_OUTPUT)
		        || !outputFormat.equals(SIMPLE_OUTPUT) || !outputFormat.equals(XML_OUTPUT)) {
			throw new IllegalArgumentException("Unknown type for Output Format.");
		}
		this.outputFormat = outputFormat;
	}

	public boolean isAllowDistribution() {
		return allowDistribution;
	}

	public void setAllowDistribution(boolean allowDistribution) {
		this.allowDistribution = allowDistribution;
	}

	public boolean isAllowSearch() {
		return allowSearch;
	}

	public void setAllowSearch(boolean allowSearch) {
		this.allowSearch = allowSearch;
	}

	public String getExternal_id() {
		return external_id;
	}

	public void setExternal_id(String external_id) {
		this.external_id = external_id;
	}

	public String getBaseURL() {
		return reltagBaseURL;
	}

	public void setBaseURL(String base_url) {
		this.reltagBaseURL = base_url;
	}

	public boolean getRelevance() {
		return relevance;
	}

	public void setrelevance(boolean relevance) {
		this.relevance = relevance;
	}

	public boolean getDocRDF() {
		return docRDF;
	}

	public void setDocRDF(boolean docRDF) {
		this.docRDF = docRDF;
	}

	public String getSubmitter() {
		return submitter;
	}

	public void setSubmitter(String submitter) {
		this.submitter = submitter;
	}

	public String getXMLParameters() {
		String additionalParameters = "";
		if (reltagBaseURL != null) {
			additionalParameters = additionalParameters + "c:reltagBaseURL=\"" + this.reltagBaseURL + "\" ";
		}
		if (!relevance) {
			additionalParameters = additionalParameters + "c:calculateRelevanceScore=\"" + this.relevance + "\" ";
		}
		if (docRDF) {
			additionalParameters = additionalParameters + "c:docRDFaccessible=\"" + this.docRDF + "\" ";
		}

		String xmlParameters = "<c:params xmlns:c=\"http://s.opencalais.com/1/pred/\" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">"
		        + "<c:processingDirectives c:contentType=\""
		        + this.content_type
		        + "\" "
		        + "c:enableMetadataType=\""
		        + this.enable_metatype
		        + "\" "
		        + "c:omitOutputtingOriginalText=\""
		        + this.omittingOriginalText
		        + "\" "
		        + "c:outputFormat=\""
		        + this.outputFormat
		        + "\" "
		        + additionalParameters
		        + "></c:processingDirectives>"
		        + "<c:userDirectives c:allowDistribution=\""
		        + this.allowDistribution
		        + "\" "
		        + "c:allowSearch=\""
		        + this.allowSearch
		        + "\" ";
		
		if(external_id!=null) xmlParameters+="c:externalID=\""+ this.external_id+ "\" ";
		if(submitter!=null) xmlParameters+="c:submitter=\""+ this.submitter + "\"";
		
		xmlParameters+="></c:userDirectives>" + "<c:externalMetadata></c:externalMetadata></c:params>";

		return xmlParameters;
	}

}
