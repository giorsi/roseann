/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.opencalais;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import argo.jdom.JdomParser;
import argo.jdom.JsonNode;
import argo.jdom.JsonNodeDoesNotMatchJsonNodeSelectorException;
import argo.jdom.JsonRootNode;
import argo.jdom.JsonStringNode;
import argo.saj.InvalidSyntaxException;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.AnnotatorException;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.AnnotatorAdapter;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.Annotation.AnnotationClass;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.AnnotationAttribute;
import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.datamodel.annotation.EntityAnnotation;

/**
 *
 *
 * @author Stefano Ortona (stefano.ortona@gmail.com) Oxford University,
 *         Department of Computer Science
 *
 *         The wrapper submit the text to OpenCalais, take the response as
 *         String representing JSon Object, builds a JSon Object and select the
 *         annotations to be saved in the model. You can extract entity
 *         annotations and also relation annotations
 */

public class OpenCalaisAnnotator extends AnnotatorAdapter {

  private static final Logger logger = LoggerFactory.getLogger(OpenCalaisAnnotator.class);

  /**
   * the prefix of enitity type's URI
   */
  final String entityType_prefix;
  /**
   * the prefix of relation type's URI
   */
  final String rel_prefix;
  /**
   * the predefined short form of predicate URI prefix
   */
  final String pref_short;

  /**
   * the prefix of the id assigned by OpenCalais entity
   */
  final String pref_id;

  /**
   * Map to keep all the entity annotations found in the text Key is the id of
   * the entity annotation given by OpenCalais, value is a list of all instances
   * id given by the system
   */
  private final Map<String, List<String>> entity2id;

  /**
   * counter to increment the annotation id
   */
  private long id_counter;
  /**
   * singleton instance
   */
  private static OpenCalaisAnnotator INSTANCE;
  /**
   * Low layer api of opencalais service
   */
  private OpenCalais opencalais;

  /**
   * private constructor to build the singleton instance
   */
  private OpenCalaisAnnotator() {
    ConfigurationFacility.getConfiguration();
    setAnnotatorName("openCalais");
    entityType_prefix = "http://s.opencalais.com/1/type/em/e/";
    rel_prefix = "http://s.opencalais.com/1/type/em/r/";
    pref_short = "c";
    pref_id = "http://d.opencalais.com/";
    entity2id = new HashMap<String, List<String>>();

    // start the counter from 0
    id_counter = 0;

    // initialize the parametrs
    final AnnotatorConfiguration configuration = new AnnotatorConfiguration();
    configuration.initializeConfiguration(getAnnotatorName());
    setConfig(configuration);
  }

  /**
   * method to get the singleton instance
   *
   * @return singleton instance
   */
  public static synchronized Annotator getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new OpenCalaisAnnotator();
    }
    return INSTANCE;
  }

  /*
   * (non-Javadoc)
   *
   * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.
   * AnnotatorAdapter#annotateEntity(java.lang.String)
   */
  @Override
  public Set<Annotation> annotateEntity(final String text) {

    // eliminate from the original text special characters

    final String res = this.submitToAnnotator(text);

    final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(res, text, id_counter);
    if (entityAnnotations != null) {
      final int num_annotations = entityAnnotations.size();
      id_counter += num_annotations;
    }
    // reset the context
    resetContext();
    return entityAnnotations;
  }

  @Override
  public Set<Annotation> annotateEntity(final String text, final int timeout) {

    // eliminate from the original text special characters

    final String res = this.submitToAnnotator(text, timeout);
    if (res == null)
      return new HashSet<Annotation>();

    final Set<Annotation> entityAnnotations = retrieveEntityAnnotations(res, text, id_counter);
    if (entityAnnotations != null) {
      final int num_annotations = entityAnnotations.size();
      id_counter += num_annotations;
    }
    // reset the context
    resetContext();
    return entityAnnotations;
  }

  /**
   * An utility method to harvest entity annotations from annotator response
   *
   * @param res
   *          Response to be interpreted
   * @param text
   *          Text to be annotated
   * @param id_counter
   * @return Set<Annotation> Set of entity annotations
   */

  private Set<Annotation> retrieveEntityAnnotations(final String res, final String text, final long id_counter) {
    Set<Annotation> entities = null;
    JsonRootNode doc;

    logger.debug("Processing the OpenCalais response.");

    // to count the annotation
    int count = 0;
    try {
      // create a JSon node parsing the text response
      doc = new JdomParser().parse(res);
    } catch (final InvalidSyntaxException e) {
      logger.error("The String response from OpenCalais can't be parse as JSonNode");
      throw new AnnotatorException("The String response from OpenCalais " + "can't be parse as JSonNode", e, logger);
    }

    // select only the entity annotations
    final Map<String, JsonNode> entity_annotations = selectAnnotations(doc, "entities");
    entities = new HashSet<Annotation>();
    if (entity_annotations != null) {

      for (final String id_annotation : entity_annotations.keySet()) {
        final Set<Annotation> subset = buildAnnotationFromJson(entity_annotations.get(id_annotation), annotatorName,
            id_annotation, count + id_counter);
        if (subset != null) {
          entities.addAll(subset);
          count += subset.size();
        }
      }
      logger.debug("Response processed and entity annotation annotations saved.");
    } else {
      logger.debug("OpenCalais service did not find any entity annotations.");
    }
    return entities;
  }

  /*
   * (non-Javadoc)
   *
   * @see
   * uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.Annotator#
   * submitToAnnotator(java.lang.String)
   */
  @Override
  public String submitToAnnotator(final String freeText) {

    return this.submitToAnnotator(freeText, 0);

  }

  @Override
  public String submitToAnnotator(String freeText, final int timeout) {

    if ((freeText == null) || (freeText.length() < 1)) {
      logger.error("Trying to submit an empty text to OpenCalais");
      throw new AnnotatorException("Trying to submit an empty text to OpenCalais", logger);
    }

    logger.debug("Submitting document to OpenCalais service.");

    // eliminate from the original text special characters
    freeText = freeText.replaceAll("\u0003", " ");
    String result = null;
    try {
      result = opencalais.invokeNEService(freeText, config, timeout);

      // check the timeout has not expired
      if (result.equals("timeout")) {
        logger.warn("{} was not able to receive the web response within timeout {} milliseconds", getAnnotatorName(),
            timeout);
        return null;
      }
    } catch (final IllegalArgumentException ce) {
      logger.error("Error retrieving the key for OpenCalais");
      throw new AnnotatorException("Error retrieving the key for OpenCalais", ce, logger);
    } catch (final Exception e) {
      logger.error("Error submitting text to OpenCalais");
      throw new AnnotatorException("Error submitting text to OpenCalais", e, logger);
    }
    // check if there has been error submitting the text
    if (result.isEmpty()) {
      logger.warn("OpenCalais annotator returned an empty result");
    }

    logger.debug("OpenCalais Process done and response received.");
    return result;

  }

  /**
   * Select in the annotated document only the annotation of a certain type
   *
   * @param root
   *          the JSon node representing the annotated document
   * @param type
   *          type of the annotations to be selected
   * @return map of selected annotations, key is the id given by OpenCalais and
   *         value is the Json object representing the annotation
   */
  private Map<String, JsonNode> selectAnnotations(final JsonNode root, final String type) {
    // extract all the fields from JSon object
    final Map<JsonStringNode, JsonNode> field2node = root.getFields();
    final Map<String, JsonNode> selected_nodes = new HashMap<String, JsonNode>();
    JsonNode annotation;

    // iterate over all filed, every field is a particular annotation. Among
    // them select only
    // the annotation of a certain type, using the _typeGroup field
    String id = null;
    for (final JsonStringNode field : field2node.keySet()) {
      try {
        annotation = field2node.get(field);
        id = field.getText();
        // if it's an annotation the id contatins the OpenCalais prefix
        if (id.contains(pref_id)) {
          id = id.split(pref_id)[1];
        }
        // check if the type is the one desiderd
        if (annotation.getStringValue("_typeGroup").equals(type)) {
          selected_nodes.put(id, annotation);
        }
      } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
        // if the annotation doesn't contatin the field _typeGroup
        // simply continue
        continue;

      }
    }
    return selected_nodes.size() > 0 ? selected_nodes : null;
  }

  /**
   * Seriealizes entity annotation in dlv facts One fact for every instance of
   * every annotation
   *
   * @param annotation
   *          JSon object representing the istance of annotation
   * @param outputModel
   *          outputModel to save the facts
   * @param annotator_id
   *          the annotator id
   * @param annotation_id
   *          Annotation_id for the given annotation, given by OpenCalais
   * @param id
   *          progressive to assign the annotation id
   * @return the annotation number saved
   */
  private Set<Annotation> buildAnnotationFromJson(final JsonNode annotation, final String annotator_id,
      final String annotation_id, final long id) {
    final Set<Annotation> annotations_found = new HashSet<Annotation>();
    // list to put every id for every instances found
    final List<String> instances_id = new LinkedList<String>();

    // get instances of the given annotation
    final List<JsonNode> instances = getInstances(annotation);

    // to count the annotation
    long progressive = 0;

    // the annotator type
    final String annotator_term = annotation.getStringValue("_type");
    // transform the concept dlv compatible
    // annotator_term = AnnotationUtil.modifyConcept(annotator_term);

    long start;
    long end;
    final String annotator_name = annotatorName;

    // confidence given by the annotator
    Double confidenceDouble = null;
    try {
      confidenceDouble = Double.parseDouble(annotation.getNumberValue("relevance"));

    } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException e) {
      // just continue if the annotor can't provide the confidence
    }

    // the source of knowledge is unknow in OpenCalais
    final String source = "null";

    // the entity class is always instance
    // final String entity_class = "instance";

    // retrieve all the attributes for the annotation
    Map<String, String> attribute2value = getAnnotationAttributes(annotation);

    // iterate over all instances and create a fact for every instance
    for (final JsonNode instance : instances) {

      // retrieve all the attribute for the instance
      final Map<String, String> instance_attribute = getInstanceAttributes(instance);

      if (instance_attribute != null) {
        if (attribute2value != null) {
          attribute2value.putAll(instance_attribute);
        } else {
          attribute2value = instance_attribute;
        }
      }

      // increment the annotation number
      progressive++;

      // retrieve all the information for the annotation istance

      // id of annotation: annotatorId_OpenCalaisId_progressive
      final String annotation_instance_id = annotator_id + "_" + (id + progressive);

      start = Long.parseLong(instance.getNumberValue("offset"));
      end = start + Long.parseLong(instance.getNumberValue("length"));

      // create the new entity annotation
      final Annotation oneAnnotation = new EntityAnnotation(annotation_instance_id, annotator_term, start, end,
          annotator_name, source, confidenceDouble, AnnotationClass.INSTANCE);

      annotations_found.add(oneAnnotation);

      // save the id
      instances_id.add(annotation_instance_id);

      // create a new fact for every attribute of the instance
      if (attribute2value != null) {
        for (final String attribute_name : attribute2value.keySet()) {
          final String value = attribute2value.get(attribute_name);

          final AnnotationAttribute attribute = new AnnotationAttribute(attribute_name, value);
          oneAnnotation.addAttribute(attribute);
        }
      }

    }

    // save all the instances found
    entity2id.put(pref_id + annotation_id, instances_id);
    return annotations_found;

  }

  /**
   * Method to extract all the instances for a given annotation
   *
   * @param annotation
   *          JSon object representing the annotation
   * @return list of all JSon istances of the given annotation
   */
  private List<JsonNode> getInstances(final JsonNode annotation) {
    return annotation.getArrayNode("instances");
  }

  /**
   * Method to retrieve all the attributes associated to a given instance
   * annotation Currently OpenCalais provides just 2 attributes,prefix and
   * suffix and not for all annotations
   *
   * @param annotation
   *          the JsonNode representing the instance annotation
   * @return map where key is the attribute name, value is the value of the
   *         attribute
   */
  private Map<String, String> getInstanceAttributes(final JsonNode instance) {
    final Map<String, String> attribute2value = new HashMap<String, String>();

    // if can't find the attribute, just continue
    try {
      attribute2value.put("prefix", instance.getStringValue("prefix"));
    } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
      // simply continue if it's not able to find the attribute
      logger.debug("System is not able to retrive the attribute prefix " + "for the instance annotation in OpenCalais");
    }
    // if can't find the attribute, just continue
    try {
      attribute2value.put("suffix", instance.getStringValue("suffix"));
    } catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
      // simply continue if it's not able to find the attribute
      logger.debug("System is not able to retrive the attribute " + "suffix for the instance annotation in OpenCalais");
    }
    return attribute2value.size() > 0 ? attribute2value : null;

  }

  private void resetContext() {
    entity2id.clear();

  }

  public void resetId() {
    id_counter = 0;
  }

  /*
   * (non-Javadoc)
   *
   * @see uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.annotator.
   * AnnotatorAdapter
   * #setConfig(uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.
   * AnnotatorConfiguration)
   */
  @Override
  public void setConfig(final AnnotatorConfiguration config) {
    // TODO Auto-generated method stub
    super.setConfig(config);
    // Instantiate the alchemyAPI with the new configuration

    opencalais = OpenCalais.GetInstanceFromConfig(config);
  }

  /**
   * Method to retrieve all the attributes associated to a given annotation
   *
   * @param annotation
   *          the JsonNode representing the annotation
   * @return map where key is the attribute name, value is the value of the
   *         attribute
   */
  private Map<String, String> getAnnotationAttributes(final JsonNode annotation) {
    final Map<String, String> attribute2value = new HashMap<String, String>();

    // find all the attributes which are not already extracted
    // iterate over all the annotation fields
    for (final JsonStringNode field : annotation.getFields().keySet()) {
      String attribute = field.getText();
      if (!attribute.equals("_type") && !attribute.equals("_typeGroup") && !attribute.equals("_typeReference")
          && !attribute.equals("relevance") && !attribute.equals("resolutions") && !attribute.equals("instances")
          && !attribute.equals("relationsubject") && !attribute.equals("relationobject")) {
        try {
          final String value = annotation.getStringValue(attribute);

          // skip the attribute which references an entity
          if (!value.contains(pref_id)) {

            // covert to lower case the first character
            attribute = attribute.substring(0, 1).toLowerCase() + attribute.substring(1);
            attribute2value.put(attribute, value);
          }
        }
        // if an attribute is not a string value, just skip it
        catch (final JsonNodeDoesNotMatchJsonNodeSelectorException je) {
          // continue
        }
      }
    }

    return attribute2value.size() > 0 ? attribute2value : null;

  }
}
