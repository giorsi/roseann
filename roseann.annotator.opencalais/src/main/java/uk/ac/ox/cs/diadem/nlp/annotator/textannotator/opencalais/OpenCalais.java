/*
Copyright (c) 2014, DIADEM Team (http://diadem.cs.ox.ac.uk)

All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project. 
 */
package uk.ac.ox.cs.diadem.nlp.annotator.textannotator.opencalais;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.xml.sax.SAXException;

import uk.ac.ox.cs.diadem.nlp.annotator.textannotator.api.configure.AnnotatorConfiguration;



/**
 * 
 *         Use this class to send a request to OpenCalais web service and get a
 *         response as a string. Set here your license_id
 *         @author Luying Chen
 * 
 */
class OpenCalais {



	/**
	 * The service host URL
	 */
	private  String CALAIS_URL = "http://api.opencalais.com/enlighten/rest/";



	/**
	 * @param cALAIS_URL the cALAIS_URL to set
	 */
	void setCALAIS_URL(String cALAIS_URL) {
		CALAIS_URL = cALAIS_URL;
	}


	/**
	 * @param licenseID the licenseID to set
	 */
	void setLicenseID(String licenseID) {
		this.licenseID = licenseID;
	}
	/**
	 * @return the cALAIS_URL
	 */
	String getCALAIS_URL() {
		return CALAIS_URL;
	}
	/**
	 * Set here your license id
	 */
	private String licenseID;

	private OpenCalais() {

	}


	/**
	 * Utility method to check the validity of the source text.
	 * @param text
	 * 			The text to be annotated
	 */
	private void CheckText(String text) {
		if (null == text || text.length() < 2)
			throw new IllegalArgumentException("Enter some text to analyze.");
	}

	private HttpPost createPostMethod(OpenCalais_Params parameters, String content) {


		HttpPost request = new HttpPost(CALAIS_URL);


		List<NameValuePair> params = new ArrayList<NameValuePair>();

		// set parameters
		try {
			String encoding = "UTF-8";
			params.add(new BasicNameValuePair("licenseID", licenseID));

			params.add(new BasicNameValuePair("content", content));

			params.add(new BasicNameValuePair("paramsXML", parameters.getXMLParameters()));

			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(params, encoding);
			request.setEntity(entity);

		} catch (UnsupportedEncodingException e) {

		}

		return request;
	}

	private String doRequest(HttpPost method,int timeout) {

		final HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, timeout);

		HttpClient client = new DefaultHttpClient(httpParams);
		client.getParams().setParameter("http.useragent", "Calais Rest Client");


		try {
			HttpResponse response = client.execute(method);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == HttpStatus.SC_OK) {
				return this.readAnswer(response);
			} else {

				return "";
			}
		} catch (ClientProtocolException cpe) {
			return "";
		} catch(ConnectTimeoutException e){
			return "timeout";
		}
		catch (IOException ioe) {
			return "";
		} finally {
			client.getConnectionManager().shutdown();
		}
	}

	/**
	 * Read the answer and return it as a string
	 * 
	 * @param method
	 * @return the String response from Open Calais
	 */
	private String readAnswer(HttpResponse response) {

		StringBuilder sb = new StringBuilder();
		try {

			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String line;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} catch (UnsupportedEncodingException uee) {

		} catch (IOException ioe) {

		}
		return sb.toString();
	}
	/**
	 * Get OpenCalais instance from configuration object.
	 * @param config
	 * 			Input configuration object of this OpenCalais instance
	 * @return OpenCalais
	 * 			The OpenCalais instance with the specific configuration
	 */
	public static OpenCalais GetInstanceFromConfig(AnnotatorConfiguration config) {

		OpenCalais api = new OpenCalais();
		final String key =config.getApiKey();
		if(key==null)
			throw new IllegalArgumentException("The api key has not been specified");

		api.setLicenseID(key);
		final URL url = config.getURlendpoint();
		if(url!=null){
			api.setCALAIS_URL(url.toString());
		}

		return api;
	}
	/**
	 * Utility method to invoke NE service and return the response string.
	 * @param text
	 * 			The source text to be annotated
	 * @param config
	 * 			The input configration object
	 * @return String
	 * 			The response string of the service
	 * @throws XPathExpressionException
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	public String invokeNEService(String text, final AnnotatorConfiguration config,
			int timeout) throws XPathExpressionException, IOException, SAXException, ParserConfigurationException {
		final OpenCalais_Params entityParams = getParamsFromConfigurationFile(config);
		CheckText(text);
		HttpPost request = createPostMethod(entityParams, text);

		return doRequest(request,timeout);

	}

	/**
	 * Utility method to assign the configuration items from annotator configuration object
	 * @param config
	 * 			The annotator configuration object
	 * @return OpenCalais_Params
	 * 			OpenCalais parameters
	 */
	private OpenCalais_Params getParamsFromConfigurationFile(
			final AnnotatorConfiguration config) {
		final OpenCalais_Params params= new OpenCalais_Params();
		final Map<String, String> myParams = config.getSpecificParameters();
		if(myParams==null){
			return params;
		}

		for(String key:myParams.keySet()){
			final String value = myParams.get(key);
			switch(key) {

			case "reltagBaseURL": if(value!=null) params.setBaseURL(value);
			break;

			case "calculateRelevanceScore": if(value!=null){
				params.setrelevance(Boolean.valueOf(value));	
			}
			break;

			case "enableMetadataType": if(value!=null) params.setEnable_metatype(value);
			break;

			case "docRDFaccesible": if(value!=null){
				params.setDocRDF(Boolean.valueOf(value));	
			}
			break;

			case "allowDistribution": if(value!=null){
				params.setAllowDistribution(Boolean.valueOf(value));	

			}
			break;

			case "allowSearch": if(value!=null){
				params.setAllowSearch(Boolean.valueOf(value));	

			}
			break;

			case "externalID": if(value!=null) params.setExternal_id(value);
			break;

			case "submitter": if(value!=null) params.setSubmitter(value);
			break;

			case "omitOutputtingOriginalText": if(value!=null){
				params.setOmittingOriginalText(Boolean.valueOf(value));	

			}
			break;

			/*case "extractRelation": if(value!=null){
					params.SETR(Boolean.valueOf(value));	
					if(node.getValue().equals("true")) this.relation=true;
					if(node.getValue().equals("false")) this.relation=false;	
				}
				break;*/

			}
		}







		return params;
	}


}
