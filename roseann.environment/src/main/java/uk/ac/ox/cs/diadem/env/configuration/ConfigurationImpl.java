package uk.ac.ox.cs.diadem.env.configuration;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.util.collect.IterableIterator;
import uk.ac.ox.cs.diadem.util.collect.WrappingIterableIterator;

import com.google.common.base.Predicate;

// TODO add a TestCase which does not refer to the really used configuration but to a test configuration

// TODO test the package/optional configuration

// TODO add improved dump (take care of multiplicities)

// TODO right now, the configurations are not checked for overlaps at all

// TODO splitting into RootConfiguration and SubConfiguration is probably an overkill, leading to too much redundency

/**
 * @author christian TESTING: except for the delegated wrapping, all methods have been used
 */
class ConfigurationImpl implements Configuration {

  static Logger LOGGER = LoggerFactory.getLogger(ConfigurationImpl.class);

  /**
   * Delimiter for string arrays
   */
  private static final String LIST_DELIMITER = ",";

  private static ConfigurationImpl instance = null;

  /**
   * wrapped configuration
   */
  private org.apache.commons.configuration.Configuration config = null;
  private String[] path;

  // //////////////////////////////////////////////////////////////////////////////////////

  static ConfigurationImpl instance() {
    if (instance == null) {
      instance = new ConfigurationImpl();
    }
    return instance;
  }

  private ConfigurationImpl() {
  }

  private ConfigurationImpl(final org.apache.commons.configuration.Configuration config) {
    this.config = config;
    path = new String[0];
  }

  private ConfigurationImpl(final org.apache.commons.configuration.Configuration config, final String[] path) {
    this.config = config;
    this.path = path;
  }

  ConfigurationImpl setConfiguration(final org.apache.commons.configuration.Configuration config) {
    this.config = config;
    path = new String[0];
    return this;
  }

  boolean isTestrun() {
    return ConfigurationLoader.isTestrun(); // TODO kill
  }

  // /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  public String toString(final String prefix) {
    assert prefix != null;
    final StringBuilder sb = new StringBuilder();
    for (final String key : getKeys(prefix)) {
      if (key.equals(prefix)) {
        continue;
      }

      sb.append(key);
      sb.append(": ");
      final char[] filler = new char[key.length() + 2];
      for (int i = 0; i < filler.length; filler[i] = ' ', ++i) {
        ;
      }
      filler[key.length()] = '|';

      final String[] values = getStringArray(key);
      if (values.length > 0) {
        sb.append(values[0]);
        sb.append('\n');
        for (int i = 1; i < values.length; ++i) {
          sb.append(filler);
          sb.append(values[i]);
          sb.append('\n');
        }
      }
    }
    return sb.toString();
  }

  // ////////////////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  public boolean containsKey(final String key) {
    assert key != null;
    return config.containsKey(key);
  }

  @Override
  public BigDecimal getBigDecimal(final String key) {
    assert key != null;
    if (!config.containsKey(key)) {
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", LOGGER);
    }
    return config.getBigDecimal(key);
  }

  @Override
  public BigDecimal getBigDecimal(final String key, final BigDecimal defaultValue) {
    assert key != null;
    return config.getBigDecimal(key, defaultValue);
  }

  @Override
  public BigInteger getBigInteger(final String key) {
    assert key != null;
    if (!config.containsKey(key)) {
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", LOGGER);
    }
    return config.getBigInteger(key);
  }

  @Override
  public BigInteger getBigInteger(final String key, final BigInteger defaultValue) {
    assert key != null;
    return config.getBigInteger(key, defaultValue);
  }

  @Override
  public boolean getBoolean(final String key) {
    assert key != null;
    if (!config.containsKey(key)) {
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", LOGGER);
    }
    return config.getBoolean(key);
  }

  @Override
  public boolean getBoolean(final String key, final boolean defaultValue) {
    assert key != null;
    return config.getBoolean(key, defaultValue);
  }

  @Override
  public Boolean getBoolean(final String key, final Boolean defaultValue) {
    assert key != null;
    return config.getBoolean(key, defaultValue);
  }

  @Override
  public byte getByte(final String key) {
    assert key != null;
    if (!config.containsKey(key)) {
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", LOGGER);
    }
    return config.getByte(key);
  }

  @Override
  public byte getByte(final String key, final byte defaultValue) {
    assert key != null;
    return config.getByte(key, defaultValue);
  }

  @Override
  public Byte getByte(final String key, final Byte defaultValue) {
    assert key != null;
    return config.getByte(key, defaultValue);
  }

  @Override
  public double getDouble(final String key) {
    assert key != null;
    if (!config.containsKey(key)) {
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", LOGGER);
    }
    return config.getDouble(key);
  }

  @Override
  public double getDouble(final String key, final double defaultValue) {
    assert key != null;
    return config.getDouble(key, defaultValue);
  }

  @Override
  public Double getDouble(final String key, final Double defaultValue) {
    assert key != null;
    return config.getDouble(key, defaultValue);
  }

  @Override
  public float getFloat(final String key) {
    assert key != null;
    if (!config.containsKey(key)) {
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", LOGGER);
    }
    return config.getFloat(key);
  }

  @Override
  public float getFloat(final String key, final float defaultValue) {
    assert key != null;
    return config.getFloat(key, defaultValue);
  }

  @Override
  public Float getFloat(final String key, final Float defaultValue) {
    assert key != null;
    return config.getFloat(key, defaultValue);
  }

  @Override
  public int getInt(final String key) {
    assert key != null;
    if (!config.containsKey(key)) {
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", LOGGER);
    }
    return config.getInt(key);
  }

  @Override
  public int getInt(final String key, final int defaultValue) {
    assert key != null;
    return config.getInt(key, defaultValue);
  }

  @Override
  public Integer getInteger(final String key, final Integer defaultValue) {
    assert key != null;
    return config.getInteger(key, defaultValue);
  }

  @SuppressWarnings("unchecked")
  @Override
  public IterableIterator<String> getKeys() {
    return WrappingIterableIterator.create(config.getKeys(), new Predicate<String>() {
      @Override
      public boolean apply(final String input) {
        return input.length() > 0;
      }
    });
  }

  @SuppressWarnings("unchecked")
  @Override
  public IterableIterator<String> getKeys(final String prefix) {
    return WrappingIterableIterator.create(config.getKeys(prefix));
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<String> getList(final String key) {
    assert key != null;
    return config.getList(key);
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<String> getList(final String key, final List<String> defaultValue) {
    assert key != null;
    return config.getList(key, defaultValue);
  }

  @Override
  public long getLong(final String key) {
    assert key != null;
    if (!config.containsKey(key)) {
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", LOGGER);
    }
    return config.getLong(key);
  }

  @Override
  public long getLong(final String key, final long defaultValue) {
    assert key != null;
    return config.getLong(key, defaultValue);
  }

  @Override
  public Long getLong(final String key, final Long defaultValue) {
    assert key != null;
    return config.getLong(key, defaultValue);
  }

  @Override
  public short getShort(final String key) {
    assert key != null;
    if (!config.containsKey(key)) {
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", LOGGER);
    }
    return config.getShort(key);
  }

  @Override
  public short getShort(final String key, final short defaultValue) {
    assert key != null;
    return config.getShort(key, defaultValue);
  }

  @Override
  public Short getShort(final String key, final Short defaultValue) {
    assert key != null;
    return config.getShort(key, defaultValue);
  }

  @Override
  public String getString(final String key) {
    assert key != null;
    if (!config.containsKey(key)) {
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", LOGGER);
    }
    return StringUtils.join(config.getStringArray(key), LIST_DELIMITER);
  }

  @Override
  public String getString(final String key, final String defaultValue) {
    assert key != null;
    if (!config.containsKey(key)) {
      return defaultValue;
    }
    return getString(key);
  }

  @Override
  public String[] getStringArray(final String key) {
    final String[] result = config.getStringArray(key);
    if ((result.length == 1) && result[0].isEmpty()) {
      return new String[] {};
    }
    return result;
  }

  @Override
  public boolean isEmpty() {
    return config.isEmpty();
  }

  @SuppressWarnings({ "unchecked", "static-access" })
  @Override
  public <E extends Enum<E>> E getEnum(final String key, final E defaultValue) {
    assert key != null;
    final String valueName = getString(key, null);
    if (valueName == null) {
      return defaultValue;
    }
    try {
      return (E) defaultValue.valueOf(defaultValue.getClass(), valueName);
    } catch (final IllegalArgumentException e) {
      throw new MalformedPropertyConfigurationRuntimeException("Got '" + valueName + "' for property '" + key
          + "' of type '" + defaultValue.getClass().toString() + "'.", LOGGER);
    }
  }

  @Override
  public <E extends Enum<E>> E getEnumNoDefault(final String key, final E prototype) {
    assert key != null;
    if (!containsKey(key)) {
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", LOGGER);
    }
    return getEnum(key, prototype);
  }

  @Override
  public Configuration getSubConfiguration(final String prefix) {
    assert prefix != null;
    assert !prefix.endsWith(".");
    if (!hasSubConfiguration(prefix)) {
      throw new MissingPropertyConfigurationRuntimeException("Subconfiguration '" + prefix + "' does not exist. ",
          LOGGER);
    }
    final String[] prefixSteps = prefix.split("\\.");
    final String[] newPath = new String[path.length + prefixSteps.length];
    int i = -1;
    for (final String step : path) {
      newPath[++i] = step;
    }
    for (final String step : prefixSteps) {
      newPath[++i] = step;
    }
    return new ConfigurationImpl(config.subset(prefix), newPath);
  }

  @SuppressWarnings("unchecked")
  @Override
  public IterableIterator<String> getChildKeys() {
    return WrappingIterableIterator.create(config.getKeys(), new Predicate<String>() {
      @Override
      public boolean apply(final String input) {
        return !((input.length() == 0) || input.contains("."));
      };
    });
  }

  @Override
  public String toString() {
    return toString("");
  }

  @Override
  public Configuration[] getSubConfigurationArray(final String prefix) {
    assert prefix != null;
    assert !prefix.endsWith(".");
    if (!hasSubConfiguration(prefix)) {
      throw new MissingPropertyConfigurationRuntimeException("Subconfiguration '" + prefix + "' does not exist. ",
          LOGGER);
    }
    final int numSubConfigs = getStringArray(prefix).length;
    final Configuration[] result = new Configuration[numSubConfigs];
    for (int i = 0; i < numSubConfigs; ++i) {
      result[i] = getSubConfiguration(prefix + "(" + i + ")");
    }
    return result;
  }

  @Override
  public Configuration[] splitIntoArrays() {
    final ArrayList<Configuration> resultList = new ArrayList<Configuration>();
    for (final String childKey : getChildKeys()) {
      // final org.apache.commons.configuration.Configuration subConfiguration = getApacheSubConfiguration(childKey);
      // final CombinedConfiguration wrappedConfiguration = new CombinedConfiguration();
      // wrappedConfiguration.addConfiguration(subConfiguration, null, childKey);
      // resultList.add(new ConfigurationImpl(wrappedConfiguration));
      resultList.add(getSubConfiguration(childKey));
    }
    final Configuration[] result = new Configuration[resultList.size()];
    for (int i = 0; i < resultList.size(); ++i) {
      result[i] = resultList.get(i);
    }
    return result;
  }

  @Override
  public boolean hasSubConfiguration(final String prefix) {
    assert prefix != null;
    assert !prefix.endsWith(".");
    // TODO I am not happy with this.
    return (config.getProperty(prefix) != null) || !config.subset(prefix).isEmpty();

    // final org.apache.commons.configuration.Configuration subConf = config.subset(prefix);
    // return subConf!=null && !subConf.isEmpty()
    //
    // final String extendedPrefix = prefix + ".";
    // for (final String key : getKeys())
    // if (key.startsWith(extendedPrefix))
    // return true;
    // return false;
  }

  @Override
  public String[] getPath() {
    return path;
  }

}
