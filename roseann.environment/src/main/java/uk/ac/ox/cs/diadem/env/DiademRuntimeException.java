package uk.ac.ox.cs.diadem.env;

import org.slf4j.Logger;

/**
 * Root of all RuntimeException's thrown in uk.ac.ox.diadem.
 * @author christian
 */
@SuppressWarnings("serial")
public class DiademRuntimeException extends RuntimeException {
  public DiademRuntimeException(final String message, final Logger logger) {
    super(message);
    if (logger != null) {
      logger.error(message);
    }
  }

  public DiademRuntimeException(final String message, final Throwable cause, final Logger logger) {
    super(message, cause);
    if (logger != null) {
      logger.error(message);
    }
  }
}
