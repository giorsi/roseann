package uk.ac.ox.cs.diadem.env.configuration;

import org.slf4j.Logger;

/**
 *
 * Thrown if two properties in a configuration have identical keys.
 * This might be a feature and not an error -- We use it right now
 * to ensure that no two distributed configurations overlap accidentally.
 * 
 * @author christian
 *
 */
@SuppressWarnings("serial")
public class AmbiguousPropertyConfigurationRuntimeException extends
		ConfigurationRuntimeException {
	public AmbiguousPropertyConfigurationRuntimeException(String message, Logger logger) {
		super(message, logger);
	}
	public AmbiguousPropertyConfigurationRuntimeException(String message, Throwable cause, Logger logger) {
		super(message, cause, logger);
	}
}
