package uk.ac.ox.cs.diadem.env.configuration;

import org.slf4j.Logger;

/** 
 * Thrown for properties which could not be found. Typically, this exception
 * is thrown by {@link Configuration} upon getting an unconfigred property.
 * 
 * If you miss some property in your own iterations over the keys of a 
 * configuration, throw this exception in your client code yourself as well.
 * 
 * @author christian
 *
 */
@SuppressWarnings("serial")
public class MissingPropertyConfigurationRuntimeException extends
		ConfigurationRuntimeException {
	public MissingPropertyConfigurationRuntimeException(String message, Logger logger) {
		super(message, logger);
	}
	public MissingPropertyConfigurationRuntimeException(String message, Throwable cause, Logger logger) {
		super(message, cause, logger);
	}
}
