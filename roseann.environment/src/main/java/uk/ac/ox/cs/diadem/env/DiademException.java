package uk.ac.ox.cs.diadem.env;

import org.slf4j.Logger;

/**
 * 
 * Root of all (non-Runtime)Exception's thrown in uk.ac.ox.diadem.
 */
@SuppressWarnings("serial")
public class DiademException extends Exception {
  public DiademException(final String message, final Logger logger) {
    super(message);
    logger.error(message);
  }

  public DiademException(final String message, final Throwable cause, final Logger logger) {
    super(message, cause);
    logger.error(message);
  }
}
