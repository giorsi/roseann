package uk.ac.ox.cs.diadem.env.configuration;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * This interface is used to instantiate objects for package local classes, as these classes cannot be instantiated from
 * within the environment.
 * 
 * @author Christian Schallhart <christian@schallhart.net>
 * 
 */
public interface ConstructorInvokator {
  /**
   * Must call constructor.newInstance(config) -- but from a context which has permission to invoke the constructor. All
   * exceptions are just thrown as they come.
   * 
   * @param constructor
   * @param config
   * @return
   * @throws IllegalArgumentException
   * @throws InstantiationException
   * @throws IllegalAccessException
   * @throws InvocationTargetException
   */
  Object invoke(Constructor<?> constructor, Configuration config) throws IllegalArgumentException,
      InstantiationException, IllegalAccessException, InvocationTargetException;
}
