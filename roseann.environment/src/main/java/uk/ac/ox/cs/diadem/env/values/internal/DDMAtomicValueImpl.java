/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.env.values.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.env.values.DDMAtomicValue;

/**
 * Abstract class for {@link DDMAtomicValue} that sets all testers to false.
 */
abstract public class DDMAtomicValueImpl implements DDMAtomicValue {

  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(DDMAtomicValueImpl.class);

  @SuppressWarnings("unused")
  private static final Configuration config = ConfigurationFacility.getConfiguration();

  @Override
  public boolean isAtomicValue() {
    return true;
  }

  @Override
  public boolean isBoolean() {
    return false;
  }

  @Override
  public boolean isDouble() {
    return false;
  }

  @Override
  public boolean isLong() {
    return false;
  }

  @Override
  public boolean isNode() {
    return false;
  }

  @Override
  public boolean isSequence() {
    return false;
  }

  @Override
  public boolean isString() {
    return false;
  }

}
