package uk.ac.ox.cs.diadem.env.configuration;

import org.slf4j.Logger;

import uk.ac.ox.cs.diadem.env.DiademRuntimeException;

/**
 * Root of all RuntimeExceptions caused by the configuration
 * 
 * @author christian
 *
 */
@SuppressWarnings("serial")
public class ConfigurationRuntimeException extends DiademRuntimeException {
	public ConfigurationRuntimeException(String message, Logger logger) {
		super(message,logger);
	}
	public ConfigurationRuntimeException(String message, Throwable cause, Logger logger) {
		super(message,cause,logger);
	}
}
