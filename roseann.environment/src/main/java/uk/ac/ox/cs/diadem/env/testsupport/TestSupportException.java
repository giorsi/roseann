package uk.ac.ox.cs.diadem.env.testsupport;

import org.slf4j.Logger;

import uk.ac.ox.cs.diadem.env.DiademRuntimeException;

/**
 * Thrown upon errors in the testsupport package. Right now,
 * this is most likely caused by IO errors for the {@link StringDatabase}.
 * @author christian
 *
 */
@SuppressWarnings("serial")
public class TestSupportException extends DiademRuntimeException {
	public TestSupportException(String message, Logger logger) { 
		super(message, logger);
	}
	public TestSupportException(String message, Throwable cause, Logger logger) { 
		super(message, cause, logger);
	}
}
