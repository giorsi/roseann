package uk.ac.ox.cs.diadem.env.configuration;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import uk.ac.ox.cs.diadem.util.collect.IterableIterator;

/**
 * The main interface to the configuration. It is hierarchically structured and uses the same key syntax as the
 * configurations of apache commons (as it wraps apache commons).
 * 
 * As main difference to apache commons configurations, the getter methods throw an
 * {@link MissingPropertyConfigurationRuntimeException} if an unknown key is provided.
 * 
 * The only way to obtain an instance of Configuration is through {@link ConfigurationFacility}. See the class
 * description there for how the provided configuration is built.
 * 
 * 
 * @author Christian Schallhart <christian@schallhart.net>
 * @version $Revision: 1.0 $
 * 
 */
public interface Configuration {
  /**
   * Returns true, if this contains an entry for key.
   * 
   * @param key
   * @return true if a property for key is configured.
   */
  boolean containsKey(String key);

  /**
   * Returns true, if this contains no keys at all.
   * 
   * @return true if the configuration contains no keys.
   */
  boolean isEmpty();

  /**
   * Returns an iterator over all keys.
   * 
   * @return an iterator over all keys.
   */
  IterableIterator<String> getKeys();

  /**
   * Returns an iterator over all root keys, i.e., keys not further nested in other keys. Be aware that multiple
   * occurences of the same key are only returned as a single key.
   */
  IterableIterator<String> getChildKeys();

  /**
   * Returns an iterator over all keys which start with prefix.
   * 
   * @param prefix
   * @return an iterator over all keys, with a key of the form prefix*
   */
  IterableIterator<String> getKeys(String prefix);

  /**
   * Returns a string describing the configuration of all entries starting with prefix.
   * 
   * @param prefix
   * @return a string describing the contents of the configuration tree below the given prefix.
   */
  String toString(String prefix);

  /**
   * Returns the array of all values (as strings) configured for a given key. If this contains none, the empty array is
   * returned. As an alternative, see {@see #getList(String)} and {@see #getList(String, List)}.
   * 
   * @param key
   * @return the array of strings configured for a given key. CAREFUL: if key is not configured, it does NOT throw but
   *         returns the empty array.
   */
  String[] getStringArray(String key);

  /**
   * Returns the boolean value configured at key.
   * 
   * @param key
   * @return the boolean value configured at key.
   * @throws MissingPropertyConfigurationRuntimeException
   *           if key is not configured.
   * @throws org.apache.commons.configuration.ConversionException
   *           if a value for key is present but is not convertible to Boolean. NOTE: TODO This exception needs to be
   *           wrapped in the future.
   */
  boolean getBoolean(String key);

  /**
   * Returns the boolean value configured at key with defaultValue as default if key does not exist.
   * 
   * @param key
   * @param defaultValue
   * @throws org.apache.commons.configuration.ConversionException
   *           if a value for key is present but is not convertible to Boolean. NOTE: TODO This exception needs to be
   *           wrapped in the future.
   * @return the boolean value configured at key, and if not present, the defaultValue instead.
   */
  boolean getBoolean(String key, boolean defaultValue);

  /**
   * Returns the boolean value configured at key with defaultValue as default if key does not exist.
   * 
   * @param key
   * @param defaultValue
   * @throws org.apache.commons.configuration.ConversionException
   *           if a value for key is present but is not convertible to Boolean. NOTE: TODO This exception needs to be
   *           wrapped in the future.
   * @return the boolean value configured at key, and if not present, the defaultValue instead.
   */
  Boolean getBoolean(String key, Boolean defaultValue);

  /**
   * See {@see getBoolean(String)}
   */
  BigDecimal getBigDecimal(String key);

  /**
   * See {@see getBoolean(String,Boolean)}
   */
  BigDecimal getBigDecimal(String key, BigDecimal defaultValue);

  /**
   * See {@see getBoolean(String)}
   */
  BigInteger getBigInteger(String key);

  /**
   * See {@see getBoolean(String,Boolean)}
   */
  BigInteger getBigInteger(String key, BigInteger defaultValue);

  /**
   * See {@see getBoolean(String)}
   */
  byte getByte(String key);

  /**
   * See {@see getBoolean(String,boolean)}
   */
  byte getByte(String key, byte defaultValue);

  /**
   * See {@see getBoolean(String,Boolean)}
   */
  Byte getByte(String key, Byte defaultValue);

  /**
   * See {@see getBoolean(String)}
   */
  double getDouble(String key);

  /**
   * See {@see getBoolean(String,boolean)}
   */
  double getDouble(String key, double defaultValue);

  /**
   * See {@see getBoolean(String,Boolean)}
   */
  Double getDouble(String key, Double defaultValue);

  /**
   * See {@see getBoolean(String)}
   */
  float getFloat(String key);

  /**
   * See {@see getBoolean(String,boolean)}
   */
  float getFloat(String key, float defaultValue);

  /**
   * See {@see getBoolean(String,Boolean)}
   */
  Float getFloat(String key, Float defaultValue);

  /**
   * See {@see getBoolean(String)}
   */
  int getInt(String key);

  /**
   * See {@see getBoolean(String,boolean)}
   */
  int getInt(String key, int defaultValue);

  /**
   * See {@see getBoolean(String,Boolean)}
   */
  Integer getInteger(String key, Integer defaultValue);

  /**
   * Returns a List of Strings containing each individual value set for key. If there are no such values, the returned
   * List is empty. See also {@see #getStringArray(String)}.
   * 
   * @param key
   *          the key to search values for.
   * @return the list of values found for key.
   */
  List<String> getList(String key);

  /**
   * Returns a List of Strings containing each individual value set for key. If there are no such values, defaultValue
   * is returned. See also {@see #getStringArray(String)}.
   * 
   * @param key
   *          the key to search values for.
   * @param defaultValue
   *          the default value in case there are no values set for key.
   * @return the list of values found for key.
   */
  List<String> getList(String key, List<String> defaultValue);

  /**
   * See {@see getBoolean(String)}
   */
  long getLong(String key);

  /**
   * See {@see getBoolean(String,boolean)}
   */
  long getLong(String key, long defaultValue);

  /**
   * See {@see getBoolean(String,Boolean)}
   */
  Long getLong(String key, Long defaultValue);

  /**
   * See {@see getBoolean(String)}
   */
  short getShort(String key);

  /**
   * See {@see getBoolean(String,boolean)}
   */
  short getShort(String key, short defaultValue);

  /**
   * See {@see getBoolean(String,Boolean)}
   */
  Short getShort(String key, Short defaultValue);

  /**
   * See {@see getBoolean(String)}
   */
  String getString(String key);

  /**
   * See {@see getBoolean(String,Boolean)}
   */
  String getString(String key, String defaultValue);

  /**
   * Methods of the Apache commons interface, we did not wrap
   */
  // void setProperty(String key, Object arg1);
  // Properties getProperties(String key);
  // Object getProperty(String key);

  /**
   * Returns the enum of type E at key, if existing, and defaultValue otherwise.
   * 
   * @param key
   *          the key to search for
   * @param defaultValue
   *          the default to be supplied if the key does not exist
   * @throws MalformedPropertyConfigurationRuntimeException
   *           if the found string does not match exactly the name of an enumeration value (no whitespaces allowed)
   * @return the Enumeration value at key
   */
  <E extends Enum<E>> E getEnum(String key, E defaultValue);

  /**
   * 
   * Same as {@link getEnum(String,E)}, but throwing if no value for key is given.
   * 
   * @param key
   *          the key to search for
   * @param prototype
   *          a value used to access class methods of E
   * @throws MalformedPropertyConfigurationRuntimeException
   *           if the found string does not match exactly a value (no whitespaces allowed)
   * @throws MissingPropertyConfigurationRuntimeException
   *           if there is no value for key
   * @return the Enumeration value at key
   */
  <E extends Enum<E>> E getEnumNoDefault(String key, E prototype);

  /**
   * Produces a subconfiguration where all keys are prefixed with the given prefix. If more than one subconfiguration
   * matches, their contents are merged and returned in a single subconfiguration (in contrast to
   * getSubConfigurationList).
   * 
   * RELATION TO ROOTCONFIG: Each call to getSubConfiguraiton produces a NEW instance. However, a change to the system
   * wide configuration, e.g., via setApplicationConfiguration, also changes a subconfiguration, as it is only filtering
   * the system wide configuration.
   * 
   * PREFIXES: The prefix must include the terminal dot (.), if you want to obtain a subconfiguration for a subtree of
   * the original configuration. You may also use arbitrary prefixes, e.g., "some" to include "something.XYZ" and
   * "somethingelse.XYZ".
   * 
   * @param prefix
   * @return
   */
  Configuration getSubConfiguration(String prefix);

  /**
   * Produces an array with all subconfiguration matching prefix. In difference to getSubConfiguration(prefix), this
   * method does not merge all matched subconfigurations into a single one but returns each matching one individually.
   * 
   * @param prefix
   * @return
   */
  Configuration[] getSubConfigurationArray(String prefix);

  /**
   * @return an array with all subsconfigurations contained in this.
   */
  Configuration[] splitIntoArrays();

  /**
   * Returns true if there is a (possibly empty) subconfiguration at prefix.
   * 
   * @param prefix
   *          a prefix, not ending in .
   * @return true, if there is an xml element in the configuration located at prefix. This element might be empty or
   *         contain further elements which form the corresponding subconfiguration.
   */
  boolean hasSubConfiguration(String prefix);

  /**
   * Returns the path leading to this configuration.
   */
  String[] getPath();
}
