package uk.ac.ox.cs.diadem.env.configuration;

import org.slf4j.Logger;

/**
 * Thrown if a requested configuration file (on the file system
 * or as resource) does not exist. 
 * @author christian
 *
 */
@SuppressWarnings("serial")
public class InvalidFileConfigurationRuntimeException extends
		ConfigurationRuntimeException {
	public InvalidFileConfigurationRuntimeException(String message, Logger logger) {
		super(message, logger);
	}
	public InvalidFileConfigurationRuntimeException(String message, Throwable cause, Logger logger) {
		super(message, cause, logger);
	}
}
