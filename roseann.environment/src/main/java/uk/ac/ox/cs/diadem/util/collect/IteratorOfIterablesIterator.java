package uk.ac.ox.cs.diadem.util.collect;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An iterator that iterates through a an Iterator of Iterables, and iterates through each of the enumerated Iterables.
 * 
 * @author Christian Schallhart <christian@schallhart.net>
 * 
 * @param <Value>
 * @param <NestedIterable>
 */
public class IteratorOfIterablesIterator<Value, NestedIterable extends Iterable<Value>> implements
    IterableIterator<Value> {

  final private Iterator<NestedIterable> baseIterator;
  private Iterator<Value> current = null;
  private Iterator<Value> last = null;

  public IteratorOfIterablesIterator(Iterator<NestedIterable> baseIterator) {
    assert baseIterator != null;
    this.baseIterator = baseIterator;
    advanceBaseIterator();
  }

  private void advanceBaseIterator() {
    last = current;
    while (baseIterator.hasNext()) {
      final NestedIterable nestedIterable = baseIterator.next();
      if (nestedIterable == null)
        continue;
      current = nestedIterable.iterator();
      if (current.hasNext())
        return;
    }
    current = null;
  }

  @Override
  public boolean hasNext() {
    return current != null;
  }

  @Override
  public Value next() {
    if (current == null)
      throw new NoSuchElementException();
    Value result = current.next();
    if (!current.hasNext())
      advanceBaseIterator();
    else
      last = null;
    return result;
  }

  @Override
  public void remove() {
    // throw new UnsupportedOperationException("IteratorOfIterablesIterator does not support remove.");
    if (last == null)
      current.remove();
    else
      last.remove();
  }

  @Override
  public Iterator<Value> iterator() {
    return this;
  }

}
