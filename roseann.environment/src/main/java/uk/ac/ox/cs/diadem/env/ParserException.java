package uk.ac.ox.cs.diadem.env;

import org.slf4j.Logger;

/**
 * 
 * Root of all (non-Runtime)Exception's thrown in uk.ac.ox.diadem.
 * 
 * @author christian
 *
 */
@SuppressWarnings("serial")
public class ParserException extends DiademException {
	public ParserException(String message, Logger logger) {
		super(message,logger);
	}
	public ParserException(String message, Throwable cause, Logger logger) {
		super(message, cause,logger);
	}
}
