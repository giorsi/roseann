package uk.ac.ox.cs.diadem.env.configuration;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Loads an object from a configuration.
 * 
 * TODO check for cyclic aliases
 * 
 * TODO introduce inheritance (and check for cycles)
 * 
 * @author Christian Schallhart <christian@schallhart.net>
 * 
 */
class ObjectLoader {
  private static final Logger LOGGER = LoggerFactory.getLogger(ObjectLoader.class);
  private static final ConstructorInvokator DEFAULT_CONSTRUCTOR_INVOKATOR = new ConstructorInvokator() {
    @Override
    public Object invoke(final Constructor<?> constructor, final Configuration config) throws IllegalArgumentException,
        InstantiationException, IllegalAccessException, InvocationTargetException {
      return constructor.newInstance(config);
    }
  };

  private ObjectLoader() {

  }

  public static <T> T createObject(final Configuration config, final Class<?> type) {
    return createObject(config, type, DEFAULT_CONSTRUCTOR_INVOKATOR);
  }

  @SuppressWarnings("unchecked")
  public static <T> T createObject(final Configuration config, final Class<?> type,
      final ConstructorInvokator constructorInvokator) {
    assert config != null;
    assert type != null;
    assert constructorInvokator != null;

    if (config.containsKey("alias")) {
      final String aliasName = config.getString("alias");
      return createObject(ConfigurationImpl.instance().getSubConfiguration(aliasName), type, constructorInvokator);
    }

    final String className = config.getString("type");
    final Class<?> classObject;
    try {
      classObject = Class.forName(className);
    } catch (final ClassNotFoundException e) {
      throw new ConfigurationRuntimeException("Unknown generator '" + className + "'.", e, LOGGER);
    }
    final Constructor<?> constructor;
    try {
      constructor = classObject.getConstructor(Configuration.class);
    } catch (final SecurityException e) {
      throw new ConfigurationRuntimeException("Generator '" + className
          + "': Constructor (Configuration) is inaccessible.", e, LOGGER);
    } catch (final NoSuchMethodException e) {
      throw new ConfigurationRuntimeException("Generator '" + className + "': No constructor (Configuration)", e,
          LOGGER);
    }
    Object result;
    try {
      // result = constructor.newInstance(config);
      result = constructorInvokator.invoke(constructor, config);
    } catch (final IllegalArgumentException e) {
      throw new ConfigurationRuntimeException("Generator '" + className + "': failed invoking constructor.", e, LOGGER);
    } catch (final InstantiationException e) {
      throw new ConfigurationRuntimeException("Generator '" + className + "': failed invoking constructor.", e, LOGGER);
    } catch (final IllegalAccessException e) {
      throw new ConfigurationRuntimeException("Generator '" + className + "': failed invoking constructor.", e, LOGGER);
    } catch (final InvocationTargetException e) {
      throw new ConfigurationRuntimeException("Generator '" + className + "': failed invoking constructor.", e, LOGGER);
    }
    if (type.isAssignableFrom(result.getClass()))
      return (T) result;
    throw new ConfigurationRuntimeException(
        "Generator '" + className + "' not of right type '" + type.getName() + "'.", LOGGER);
  }
}
