/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.env.values.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;

/**
 * A long value as returned by {@link DDMXPathProcessor}.
 */
public class DDMLongValue extends DDMAtomicValueImpl {
  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(DDMLongValue.class);
  @SuppressWarnings("unused")
  private static final Configuration config = ConfigurationFacility.getConfiguration();
  private final Long val;

  public DDMLongValue(final long l) {
    val = l;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = (prime * result) + (val == null ? 0 : val.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    final DDMLongValue other = (DDMLongValue) obj;
    if (val == null) {
      if (other.val != null)
        return false;
    } else if (!val.equals(other.val))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return val.toString();
  }

  @Override
  public boolean isLong() {
    return true;
  }

  @Override
  public Object getValue() {
    return val;
  }

  @Override
  public boolean getBooleanValue() {
    return val.longValue() != 0;
  }

  @Override
  public long getLongValue() {
    return val.longValue();
  }

  @Override
  public double getDoubleValue() {
    return val.longValue();
  }

  @Override
  public String getStringValue() {
    return val.toString();
  }
}
