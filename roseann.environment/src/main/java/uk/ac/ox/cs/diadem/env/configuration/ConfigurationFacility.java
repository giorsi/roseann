package uk.ac.ox.cs.diadem.env.configuration;

import java.net.URL;

// @formatter:off
/**
 * <p>
 * SUMMARY: Provides access to the configuration package. This encompasses the
 * current {@link Configuration}, allows for checking whether the current
 * execution is performed through JUnit.
 * 
 * TODO WARNING WARNING DOCU OUT OF SYNC
 * </p>
 * 
 * 
 * <p>
 * CONFIGURATION CONSTRUCTION: The configuration is constructed in stages (the
 * reading order you observe on the log stream might differ). Each stage is
 * overwritten by the subsequent ones, such that the initial stage can provide
 * general settings and the final stage can provide settings specific to the
 * application.
 * <ol>
 * <li>GLOBAL STAGE: We load
 * uk.ac.ox.cs.diadem.env.configuration.DefaultConfiguration.xml. This file
 * contains the general configuration of Diadem, which is not specific to a
 * package, platform, or an application. If the current run is a test run
 * (checked via {@link #isTestRun()}), then
 * uk.ac.ox.cs.diadem.env.configuration.TestConfiguration.xml is loaded as well,
 * overriding the settings in DefaultConfiguration.xml in case of an overlap.</li>
 * 
 * <li>PLATFORM STAGE: The platform configuration is loaded. The choice of the
 * platform configuration file is determined by the global stage: Each
 * platform-tag found in the global stage describes one platform to choose from
 * (stating its name and associated configuration file). No platforms of the
 * same name are allowed -- however, two platform might refer to the same
 * configuration file. The system tries to determine the platform it runs on,
 * and chooses the corresponding platform. Alternatively, the user can specify
 * the platform by providing -DPlatform=MyPlatformName to the VM.</li>
 * 
 * <li>PACKAGE STAGE: All packages can provide an additional configuration file
 * for their own purposes which is loaded whenever the package is in the build
 * path. This package specific configuration file must be registered at
 * uk.ac.ox.cs.diadem.env.configuration.PackageConfigurationDefinition.xml.
 * Therein, for example
 * 
 * {@code
 * <xml fileName="uk/ac/ox/cs/diadem/util/Configuration.xml" config-optional="true"/>
 * }
 * 
 * registers the configuration for the util package. By convention, we place the
 * package configuration at src/main/resources/package-path/Configuration.xml.
 * In principle, more and differently placed configuration files could be used
 * -- do not do so without prior discussion. Note the config-optional setting:
 * By setting it to true, the file is ignored, if cannot be found, i.e., if the
 * util package is not in the build path. When you add your configuration file,
 * test once with config-optional set to false, to make sure, that the file name
 * is correct.</li>
 * 
 * <li>OPTIONALS STAGE: We collect all optional-tags in the global, platform,
 * and package configurations, each having a name, a default, and a number of
 * alternatives. Each alternative contains a name and refers to a configuration
 * file. For example, uk.ac.ox.cs.diadem.env.configuration.DefaultConfiguration
 * contains an optional for the domain.
 * 
 * <pre>
 * {@code
 * <optional>
 * <name>Domain</name>
 *   <default>RealEstate</default>
 *      <alternative>
 *         <name>RealEstate</name>
 *         <file>RealEstateDomainConfiguration.xml</file>
 *      </alternative>
 *      <alternative>
 *         <name>UsedCar</name>
 *         <file>UsedCarDomainConfiguration.xml</file>
 *      </alternative>
 *    </optional>
 * }
 * </pre>
 * 
 * By setting {@code -DDomain=UsedCar} in the VM arguments, the
 * UsedCarDomainConfiguration.xml file will be loaded. Without that, as
 * RealEaste is the default, and hence RealEstateDomainConfiguration.xml will be
 * loaded.
 * 
 * For each optional occurring in the global, platform, and package
 * configurations, a configuration file is selected and loaded. This is the ONLY
 * CASE when different stages do not override each other but are merged
 * together, i.e., an optional specified at one stage never overrides another
 * optional. But no optionals of the same name, and no alternatives of the same
 * name within the same optional are allowed. However, different alternatives
 * might refer to the same file.</li>
 * <li>APPLICATION STAGE: Your application may provide an additional
 * configuration dynamically through
 * {@link #setApplicationConfiguration(String)} and
 * {@link #resetApplicationConfiguration()}. Initially, there is no application
 * configuration set. We use this configuration for specific settings of a
 * testcase or to configure a specific client.</li>
 * 
 * <li>LOCAL STAGE: At last, the file (i.e., not the resource)
 * src/main/config/DefaultConfiguration.xml is loaded, and if
 * {@link #isTestRun()} is true, afterwards
 * src/test/config/TestConfiguration.xml is loaded as well. Use these
 * configuration files for local settings, which should not become part of the
 * common configurations.</li>
 * </ol>
 * </p>
 * 
 * TESTING: all methods have been used.
 * 
 * @author Christian Schallhart <christian@schallhart.net>
 * 
 */
// @formatter:on
public class ConfigurationFacility {
  /**
   * @return the current configuration. It is a singleton and guaranteed not to change during runtime.
   * @throws InvalidFileConfigurationRuntimeException
   *           if any configuration file is missing
   */
  public synchronized static Configuration getConfiguration() {
    updateConfiguration();
    return ConfigurationImpl.instance();
  }

  /**
   * @return the same as getConfiguration().getSubConfiguration(prefix)
   * @throws InvalidFileConfigurationRuntimeException
   *           if any configuration file is missing
   */
  public synchronized static Configuration getConfiguration(final String prefix) {
    updateConfiguration();
    return ConfigurationImpl.instance().getSubConfiguration(prefix);
  }

  /**
   * Loads an object from a configuration. TODO to be documented
   * 
   * @param config
   * @param type
   * @return
   */
  public synchronized static <T> T createObject(final Configuration config, final Class<?> type) {
    updateConfiguration();
    return ObjectLoader.createObject(config, type);
  }

  /**
   * Loads an object from a configuration. TODO to be documented
   * 
   * @param config
   * @param type
   * @return
   */
  public synchronized static <T> T createObject(final Configuration config, final Class<?> type,
      final ConstructorInvokator constructorInvokator) {
    updateConfiguration();
    return ObjectLoader.createObject(config, type, constructorInvokator);
  }

  /**
   * Equivalent to createObjecT(getConfiguration(prefix),type). TODO to be documented
   * 
   * @param config
   * @param type
   * @return
   */
  public synchronized static <T> T createObject(final String prefix, final Class<?> type) {
    updateConfiguration();
    return ObjectLoader.createObject(ConfigurationImpl.instance().getSubConfiguration(prefix), type);
  }

  /**
   * Equivalent to createObjecT(getConfiguration(prefix),type). TODO to be documented
   * 
   * @param config
   * @param type
   * @return
   */
  public synchronized static <T> T createObject(final String prefix, final Class<?> type,
      final ConstructorInvokator constructorInvokator) {
    updateConfiguration();
    return ObjectLoader.createObject(ConfigurationImpl.instance().getSubConfiguration(prefix), type,
        constructorInvokator);
  }

  /**
   * Sets an application/test case specific configuration located a resource path resourceName. In doing so, it first
   * removes the last application specific configuration (if any), and then sets the new one.
   * 
   * This configuration overrides all global and package distributed configurations, but not the local one.
   * 
   * @param resourceName
   * @throws InvalidFileConfigurationRuntimeException
   *           if the configuration file cannot be located and parsed successfully.
   */
  public synchronized static void setApplicationConfiguration(final String resourceName) {
    assert resourceName != null;
    ConfigurationLoader.instance().setApplicationConfiguration(resourceName);
    updateConfiguration();
  }

  /**
   * Same as String variant, but takes a URL as input, so that the user can actually determine where the configuration
   * file comes from (rather than assuming that it is in a jar.
   * 
   * @see #setApplicationConfiguration(String)
   */
  public synchronized static void setApplicationConfiguration(final URL resource) {
    assert resource != null;
    ConfigurationLoader.instance().setApplicationConfiguration(resource);
    updateConfiguration();
  }

  /**
   * Removes the application/test case specific configuration, if any.
   */
  public synchronized static void resetApplicationConfiguration() {
    ConfigurationLoader.instance().resetApplicationConfiguration();
    updateConfiguration();
  }

  /**
   * Adds an optional configuration from the given resource to the configuration.
   */
  public synchronized static void addOptionalConfiguration(final String resourceName) {
    assert resourceName != null;
    ConfigurationLoader.instance().addOptionalConfiguration(resourceName);
    updateConfiguration();
  }

  /**
   * Adds an optional configuration from the given resource to the configuration.
   */
  public synchronized static void addOptionalConfiguration(final URL resource) {
    assert resource != null;
    ConfigurationLoader.instance().addOptionalConfiguration(resource);
    updateConfiguration();
  }

  /**
   * Checks whether the current execution is a test run. DO NOT USE THIS METHOD IN PRODUCTION CODE. Some frameware code
   * needs to know whether we are in testing or non-testing mode (at the time of writing, the configuration package
   * itself and the testsupport). To ensure a consistent behaviour, I decided to provide a publicly accessible check.
   * 
   * @return true iff run through JUnit as test case.
   */
  public synchronized static boolean isTestRun() {
    return ConfigurationLoader.isTestrun();
  }

  private static void updateConfiguration() {
    ConfigurationImpl.instance().setConfiguration(ConfigurationLoader.instance().getConfiguration());
  }

  public synchronized static void resetLoggingConfiguration() {
    updateConfiguration();
    ConfigurationLoader.instance().loadLoggingConfiguration();
  }
}
