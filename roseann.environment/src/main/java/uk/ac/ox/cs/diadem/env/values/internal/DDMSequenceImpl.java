/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.env.values.internal;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.env.values.DDMSequence;
import uk.ac.ox.cs.diadem.env.values.DDMValue;

/**
 * Straightforward implementation of {@link DDMSequence} as {@link ArrayList}.
 */
public class DDMSequenceImpl extends ArrayList<DDMValue> implements DDMSequence {
  private static final long serialVersionUID = -7174355675971990628L;
  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(DDMSequenceImpl.class);
  @SuppressWarnings("unused")
  private static final Configuration config = ConfigurationFacility.getConfiguration();

  @Override
  public boolean isAtomicValue() {
    return false;
  }

  @Override
  public boolean isNode() {
    return false;
  }

  @Override
  public boolean isSequence() {
    return true;
  }

  @Override
  public String getStringValue() {
    if (size() > 0)
      return itemAt(0).getStringValue();
    return "";
  }

  @Override
  public DDMValue itemAt(final int n) {
    return get(n);
  }

}
