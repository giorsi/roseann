/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.env.values;

import java.util.List;

/**
 * Sequences are {@link DDMValue}s that represent lists of other {@link DDMValue}. In principle, sequences may be
 * nested, but XPath never returns nested sequences.
 */
public interface DDMSequence extends DDMValue, List<DDMValue> {

  /**
   * Return the item at index <code>n</code>.
   */
  DDMValue itemAt(int n);

  @Override
  int size();
}
