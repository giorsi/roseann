package uk.ac.ox.cs.diadem.env.configuration;

import org.slf4j.Logger;

/**
 * Thrown if a property is configured but with a malformed value.
 * Use this exception in your client code in these cases. 
 * @author christian
 *
 */
@SuppressWarnings("serial")
public class MalformedPropertyConfigurationRuntimeException extends
		ConfigurationRuntimeException {
	public MalformedPropertyConfigurationRuntimeException(String message, Logger logger) {
		super(message, logger);
	}
	public MalformedPropertyConfigurationRuntimeException(String message, Throwable cause, Logger logger) {
		super(message, cause, logger);
	}
}
