package uk.ac.ox.cs.diadem.util.collect;

import java.util.Iterator;

import com.google.common.base.Predicate;

/**
 * Wraps a given Iterator into a IterableIterator. Includes filter support via
 * 
 * @author Christian Schallhart <christian@schallhart.net>
 * 
 * @param <E>
 */
public final class WrappingIterableIterator<E> implements IterableIterator<E> {

  private final Iterator<E> wrappedIterator;
  private final Predicate<E> filter;
  private E current;

  public WrappingIterableIterator(final Iterator<E> wrappedIterator, final Predicate<E> filter) {
    this.wrappedIterator = wrappedIterator;
    this.filter = filter;
    doNext();
  }

  public static <E> WrappingIterableIterator<E> create(final Iterator<E> wrappedIterator) {
    return new WrappingIterableIterator<E>(wrappedIterator, new Predicate<E>() {
      @Override
      public boolean apply(final E input) {
        return true;
      }
    });
  }

  public static <E> WrappingIterableIterator<E> create(final Iterator<E> wrappedIterator, final Predicate<E> filter) {
    return new WrappingIterableIterator<E>(wrappedIterator, filter);
  }

  @Override
  public boolean hasNext() {
    return current != null;
  }

  @Override
  public E next() {
    final E temp = current;
    doNext();
    return temp;
  }

  private void doNext() {
    while (true) {
      if (!wrappedIterator.hasNext()) {
        current = null;
        break;
      }
      current = wrappedIterator.next();
      if (filter.apply(current)) {
        break;
      }
    }
  }

  @Override
  public void remove() {
    wrappedIterator.remove();
  }

  @Override
  public Iterator<E> iterator() {
    return this;
  }

}
