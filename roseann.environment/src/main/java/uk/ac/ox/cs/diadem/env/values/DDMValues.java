/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.env.values;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.env.values.internal.DDMBooleanValue;
import uk.ac.ox.cs.diadem.env.values.internal.DDMDoubleValue;
import uk.ac.ox.cs.diadem.env.values.internal.DDMLongValue;
import uk.ac.ox.cs.diadem.env.values.internal.DDMStringValue;

/**
 * Parses {@link String}s into {@link DDMValue}s.
 */
public class DDMValues {
  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(DDMValues.class);
  @SuppressWarnings("unused")
  private static final Configuration config = ConfigurationFacility.getConfiguration();

  private DDMValues() {
    throw new IllegalAccessError();
  }

  /**
   * Parses a string into a {@link DDMValue}.
   */
  public static DDMAtomicValue getValueByParsing(final String s) {
    try {
      final long l = Long.parseLong(s);
      return new DDMLongValue(l);
    } catch (final NumberFormatException er) {
      try {
        final double d = Double.parseDouble(s);
        return new DDMDoubleValue(d);
      } catch (final NumberFormatException e) {
        if (s.equals("true"))
          return new DDMBooleanValue(true);
        if (s.equals("false"))
          return new DDMBooleanValue(false);
        return new DDMStringValue(s);
      }
    }
  }

}
