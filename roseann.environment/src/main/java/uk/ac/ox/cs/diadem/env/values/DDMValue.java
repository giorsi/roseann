/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.env.values;

/**
 * A value is either an atomic value such as a String, a {@link DDMNode} or a {@link DDMSequence}. It can be returned,
 * e.g., by an XPath evaluation.
 */
public interface DDMValue {

  /**
   * Tests if the value is an atomic value.
   */
  boolean isAtomicValue();

  /**
   * Tests if the value is a node.
   */
  boolean isNode();

  /**
   * Tests if the value is a sequence.
   */
  boolean isSequence();

  /**
   * Returns the string value of the value.
   */
  String getStringValue();

}
