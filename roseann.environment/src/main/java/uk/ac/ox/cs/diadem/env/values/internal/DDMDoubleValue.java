/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.env.values.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.DiademRuntimeException;
import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.env.values.DDMAtomicValue;

/**
 * Double value as returned by {@link DDMXPathProcessor}.
 */
public class DDMDoubleValue extends DDMAtomicValueImpl implements DDMAtomicValue {
  @SuppressWarnings("unused")
  private static final Logger logger = LoggerFactory.getLogger(DDMDoubleValue.class);
  @SuppressWarnings("unused")
  private static final Configuration config = ConfigurationFacility.getConfiguration();
  private final Double val;

  public DDMDoubleValue(final double d) {
    val = d;
  }

  @Override
  public Double getValue() {
    return val;
  }

  @Override
  public boolean getBooleanValue() {
    return val.doubleValue() != 0.0d;
  }

  @Override
  public long getLongValue() {
    throw new DiademRuntimeException("Can't get long value of double:" + this, logger);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = (prime * result) + (val == null ? 0 : val.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    final DDMDoubleValue other = (DDMDoubleValue) obj;
    if (val == null) {
      if (other.val != null)
        return false;
    } else if (!val.equals(other.val))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return val + "d";
  }

  @Override
  public double getDoubleValue() {
    return val;
  }

  @Override
  public String getStringValue() {
    return val.toString();
  }
}
