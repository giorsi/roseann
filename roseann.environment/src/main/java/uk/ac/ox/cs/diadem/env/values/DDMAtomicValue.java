/* 
 * COPYRIGHT (C) 2010-2015 DIADEM Team, Department of Computer Science, Oxford University. All Rights Reserved. 
 * 
 * This software is the confidential and proprietary information of the DIADEM project ("DIADEM"), Department of Computer Science, 
 * Oxford University ("Confidential Information").  You shall not disclose such Confidential Information and shall use 
 * it only in accordance with the terms of the license agreement you entered into with DIADEM.
 */

package uk.ac.ox.cs.diadem.env.values;

/**
 * Interface for generic atomic values, as returned, e.g., by {@link DDMXPathProcessor#evaluate(String, DDMNode)}.
 */
public interface DDMAtomicValue extends DDMValue {

  /**
   * Tests if this value represents a {@link String}
   */
  boolean isString();

  /**
   * Tests if this value represents a {@link Long}
   */
  boolean isLong();

  /**
   * Tests if this value represents a {@link Double}
   */
  boolean isDouble();

  /**
   * Tests if this value represents a {@link Boolean}
   */
  boolean isBoolean();

  /**
   * Returns the actual value represented by this object.
   */
  Object getValue();

  /**
   * Returns the value as a boolean. It is not necessary that {@link #isBoolean()} is true, since {@link String}s and
   * numbers are converted by the effective boolean value rules of XPath.
   */
  boolean getBooleanValue();

  /**
   * Returns the value as a long.
   */
  long getLongValue();

  /**
   * Returns the value as a boolean.
   */
  double getDoubleValue();

  @Override
  String getStringValue();

}
