package uk.ac.ox.cs.diadem.env;

import org.slf4j.Logger;


public class IncompatibleSystemException extends DiademRuntimeException {
    public IncompatibleSystemException(final String message, final Logger logger) {
        super(message, logger);
    }

    public IncompatibleSystemException(final String message, final Throwable cause, final Logger logger) {
        super(message, cause, logger);
    }

    private static final long serialVersionUID = 1L; // TODO

}