package uk.ac.ox.cs.diadem.env;

import org.slf4j.Logger;

/**
 * 
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public class FailedInvocationException extends DiademRuntimeException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public FailedInvocationException(final String message, final Logger logger) {

    super(message, logger);
  }

  public FailedInvocationException(final String message, final Throwable cause, final Logger logger) {

    super(message, cause, logger);
  }
}