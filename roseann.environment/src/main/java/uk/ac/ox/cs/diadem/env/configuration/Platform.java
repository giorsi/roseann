package uk.ac.ox.cs.diadem.env.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author christian
 * 
 *         This class is a quick hack to determine the current platform. We are not using automatic platform detection
 *         but rely instead on a user provided configuration setting. We could determine the default setting of platform
 *         configuration with this class.
 * 
 *         TODO To be thrown out or to be integrated sanely.
 */
class Platform {

  static final private Logger logger = LoggerFactory.getLogger(Platform.class);

  public enum OSType {
    Linux, Unix, MacOSX, Windows, Unknown
  }

  private static final String propertyName = "Platform";

  public static void main(String[] args) {
    System.out.println(osType().name());
  }

  private final static String systemString = getSystemString();

  private static String getSystemString() {
    final String sysStr = System.getProperty("os.name").toLowerCase();
    return sysStr;
  }

  private static boolean isWindows() {
    return (systemString.indexOf("win") >= 0);
  }

  private static boolean isMac() {
    return (systemString.indexOf("mac") >= 0);
  }

  private static boolean isLinux() {
    return ((systemString.indexOf("nix") >= 0) || (systemString.indexOf("nux") >= 0));
  }

  // TODO define this more clearly in distinction from isLinux
  private static boolean isUnix() {
    return (systemString.indexOf("unix") >= 0);
  }

  public static OSType osType() {
    final String setProperty = System.getProperty(propertyName);
    if (setProperty != null) {
      for (OSType osType : OSType.values())
        if (osType.name().equals(setProperty))
          return osType;
      throw new ConfigurationRuntimeException("Unknown Platform type '" + setProperty + "'", logger);
    }

    if (isWindows())
      return OSType.Windows;
    if (isMac())
      return OSType.MacOSX;
    if (isLinux())
      return OSType.Linux;
    if (isUnix())
      return OSType.Unix;
    return OSType.Unknown;
  }
}
