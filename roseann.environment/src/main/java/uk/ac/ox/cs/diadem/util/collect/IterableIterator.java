package uk.ac.ox.cs.diadem.util.collect;

import java.util.Iterator;

public interface IterableIterator<E> extends Iterator<E>,Iterable<E> {
}
