package uk.ac.ox.cs.diadem.env.configuration;

import java.io.File;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.CombinedConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.tree.OverrideCombiner;
import org.apache.commons.configuration.tree.UnionCombiner;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.helpers.NullEnumeration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO add a TestCase which does not refer to the really used configuration but to a test configuration

// TODO test the package/optional configuration

// TODO add improved dump (take care of multiplicities)

// TODO right now, the configurations are not checked for overlaps at all

/**
 * Loads and maintains the root configuration of the systems. The configuration is loaded once and then modified with
 * addition optional configurations and application configurations. The resulting configuration is provided via the
 * load() method -- to be wrapped into a Configuration interface.
 * @author Christian Schallhart <christian@schallhart.net>
 */
class ConfigurationLoader {
  private static Logger logger = LoggerFactory.getLogger(ConfigurationLoader.class);

  /**
   * Name of default (i.e., non-testing) test configurations
   */
  private static final String DEFAULT_NAME = "";
  /**
   * Name of test configurations
   */
  private static final String TEST_NAME = "Test";

  /**
   * Resource names for global base configuration files
   */
  private static final String GLOBAL_POSTFIX = "Configuration.xml";
  private static final String GLOBAL_DEFAULT_RESOURCE_NAME = DEFAULT_NAME + GLOBAL_POSTFIX;
  private static final String GLOBAL_TEST_RESOURCE_NAME = TEST_NAME + GLOBAL_POSTFIX;

  /**
   * File names for local configuration files
   */
  private static final String LOCAL_DEFAULT_FILE_NAME = "src" + File.separator + "main" + File.separator + "config"
      + File.separator + DEFAULT_NAME + "Configuration.xml";
  private static final String LOCAL_TEST_FILE_NAME = "src" + File.separator + "test" + File.separator + "config"
      + File.separator + TEST_NAME + "Configuration.xml";

  /**
   * Keys for Optional Configurations
   */
  private static final String OPTIONAL_KEY = "optional";
  private static final String OPTIONAL_NAME_KEY = ".name";
  private static final String OPTIONAL_DEFAULT_KEY = ".default";
  private static final String OPTIONAL_FILE_KEY = ".file";
  private static final String OPTIONAL_ALTERNATIVE_KEY = ".alternative";

  /**
   * Keys for Sub-Configurations
   */
  private static final String SUB_CONFIGURATION_TEMPLATE = "{0}__config/SubConfiguration_{1}.xml";

  /**
   * Keys for Package Configurations
   */
  private static final String PACKAGE_KEY = "package";

  /**
   * Keys for Platform Configurations
   */
  private static final String PLATFORM_KEY = "platform";
  private static final String PLATFORM_NAME_KEY = ".name";
  private static final String PLATFORM_FILE_KEY = ".file";

  /**
   * String searched for in fully qualified class name of the class which started the main thread. If this substring is
   * found, then the test configurations are loaded, otherwise the default configurations chosen.
   */
  private static final String[] unitTestMainClassSubstrings = { "junit.runner", "org.apache.maven.surefire.booter" };

  /**
   * Initial configuration of log4j during loading the configuration
   */
  private static final String initialLog4jResourceName = "ErrorOnlyLog4j.properties";
  private static final String debugLog4jResourceName = "DebugLog4j.properties";
  /**
   * The property searched for the log4j configuration used after loading the configuration
   */
  private static final String log4jConfigFileConfigurationProperty = "logfile";

  private static final String mainClassName = getMainclassName();

  private static final boolean isTestrun = isTestrunCheck();

  private static ConfigurationLoader instance = null;

  /**
   * wrapped configuration
   */
  private org.apache.commons.configuration.Configuration config = null;

  private org.apache.commons.configuration.CombinedConfiguration globalConfig = null;
  private org.apache.commons.configuration.CombinedConfiguration localConfig = null;
  private org.apache.commons.configuration.AbstractConfiguration applicationConfig = null;
  private org.apache.commons.configuration.CombinedConfiguration optionalConfig = null;
  private org.apache.commons.configuration.CombinedConfiguration packageConfig = null;
  private org.apache.commons.configuration.AbstractConfiguration platformConfig = null;

  /**
   * Marker class for loading resources
   */
  private final Class<?> markerClass;

  // //////////////////////////////////////////////////////////////////////////////////////

  public static ConfigurationLoader instance() {
    if (instance == null) {
      instance = new ConfigurationLoader(ConfigurationLoader.class);
    }
    return instance;
  }

  private ConfigurationLoader(final Class<?> markerClass) {
    this.markerClass = markerClass;
    loadInternally();
  }

  // //////////////////////////////////////////////////////////////////////////

  public static boolean isTestrun() {
    return isTestrun;
  }

  public org.apache.commons.configuration.Configuration getConfiguration() {
    return config;
  }

  // /////////////////////////////////////////////////////////////////////////////////////

  private URL getResource(final String name) {
    return markerClass.getResource(name);
  }

  private static String getMainclassName() {
    final Map<Thread, StackTraceElement[]> traces = Thread.getAllStackTraces();
    for (final Map.Entry<Thread, StackTraceElement[]> entry : traces.entrySet()) {
      if (entry.getKey().getName().equals("main")) {
        return entry.getValue()[entry.getValue().length - 1].getClassName();
      }
    }
    assert false : "Violated Invariance.";
    return null;
  }

  private static boolean isTestrunCheck() {
    for (final String unitTestMainClassSubstring : unitTestMainClassSubstrings) {
      if ((mainClassName != null) && mainClassName.contains(unitTestMainClassSubstring)) {
        return true;
      }
    }
    return false;
  }

  private static boolean containsWhitespaceOnly(final String s) {
    for (final char c : s.toCharArray()) {
      if (!Character.isWhitespace(c)) {
        return false;
      }
    }
    return true;
  }

  private static boolean containsWhitespaceOnly(final String[] ss) {
    for (final String s : ss) {
      if (!containsWhitespaceOnly(s)) {
        return false;
      }
    }
    return true;
  }

  private void logOverlap(final AbstractConfiguration config1, final String configName1,
      final AbstractConfiguration config2, final String configName2, final String message) {
    if ((config1 == null) || (config2 == null)) {
      return;
    }
    @SuppressWarnings("unchecked")
    final Iterator<String> keys = config1.getKeys();
    while (keys.hasNext()) {
      final String key = keys.next();
      // this key is always present... so we would report a lot of
      // pseudo-overlap
      if (key.equals("[@xml:space]")) {
        continue;
      }
      if (config2.containsKey(key)) {
        if (containsWhitespaceOnly(config1.getStringArray(key))
            && containsWhitespaceOnly(config2.getStringArray(key))) {
          continue;
        }
        logger.info("Configurations '{}' and '{}' have an overlap for key '" + key + "': " + message, configName1,
            configName2);
      }
    }
  }

  private CombinedConfiguration loadLocalConfiguration() {
    final CombinedConfiguration result = new CombinedConfiguration(new OverrideCombiner());
    AbstractConfiguration defaultConfig = null;
    AbstractConfiguration testConfig = null;

    // Local Test Configuration
    if (isTestrun) {
      if (new File(LOCAL_TEST_FILE_NAME).exists()) {
        logger.info("Reading local configuration file '{}'...", LOCAL_TEST_FILE_NAME);
        try {
          result.addConfiguration(testConfig = new XMLConfiguration(LOCAL_TEST_FILE_NAME));
        } catch (final Exception e) {
          throw new InvalidFileConfigurationRuntimeException(
              "Failed loading configuration '" + LOCAL_TEST_FILE_NAME + "'", e, logger);
        }
        logger.info("finished reading configuration file.");
      } else {
        logger.info("No local configuration file '{}'.", LOCAL_TEST_FILE_NAME);
      }
    }

    // Local Default Configuration
    if (new File(LOCAL_DEFAULT_FILE_NAME).exists()) {
      logger.info("Reading local configuration file '{}'...", LOCAL_DEFAULT_FILE_NAME);
      try {
        result.addConfiguration(defaultConfig = new XMLConfiguration(LOCAL_DEFAULT_FILE_NAME));
      } catch (final Exception e) {
        throw new InvalidFileConfigurationRuntimeException(
            "Failed loading configuration '" + LOCAL_DEFAULT_FILE_NAME + "'", e, logger);
      }
      logger.info("finished reading configuration file.");
    } else {
      logger.info("No local configuration file '{}'.", LOCAL_DEFAULT_FILE_NAME);
    }

    if (isTestrun) {
      logOverlap(testConfig, LOCAL_TEST_FILE_NAME, defaultConfig, LOCAL_DEFAULT_FILE_NAME,
          "The test configuration overrides.");
    }

    return result;
  }

  private CombinedConfiguration loadDefaultTestConfiguration(String basePath, final boolean optionalDefault,
      final boolean optionalTest) {
    assert basePath != null;

    // if the path is non-empty but does not end in "/" add "/"
    if (!(basePath.isEmpty() || basePath.endsWith("/"))) {
      basePath = basePath + "/";
    }

    AbstractConfiguration testConfig = null;
    AbstractConfiguration defaultConfig = null;
    final CombinedConfiguration result = new CombinedConfiguration(new OverrideCombiner());

    // Test Configuration
    final String testConfigPath = basePath + GLOBAL_TEST_RESOURCE_NAME;

    if (isTestrun) {
      testConfig = loadConfigurationFile(optionalTest, result, testConfigPath, null, "test");
    }
    // Default Configuration
    final String defaultConfigPath = basePath + GLOBAL_DEFAULT_RESOURCE_NAME;
    defaultConfig = loadConfigurationFile(optionalTest, result, defaultConfigPath, basePath, "default");

    if (isTestrun) {
      logOverlap(testConfig, testConfigPath, defaultConfig, defaultConfigPath, "The test configuration overrides.");
    }

    return result;
  }

  /**
   * Loads the configuration file given by configPath, adds it to result and returns it separately.
   * @param optionalTest indicates if a log messages should be written, if the config file doesn't exist or if an
   *          exception is thrown.
   * @param typeOfConfiguration used to display log messages and errors (e.g. "test")
   */
  private AbstractConfiguration loadConfigurationFile(final boolean optionalTest, final CombinedConfiguration result,
      final String configPath, final String basePath, final String typeOfConfiguration) {
    final CombinedConfiguration resultConfig = new CombinedConfiguration(new UnionCombiner());
    final URL configResource = getResource(configPath);
    if (configResource != null) {
      logger.info("Reading {} configuration file '{}'...", typeOfConfiguration, configPath);
      try {
        resultConfig.addConfiguration(new XMLConfiguration(configResource));
      } catch (final Exception e) {
        throw new InvalidFileConfigurationRuntimeException("Failed loading configuration '" + configPath + "'", e,
            logger);
      }
      readSubConfigurations(resultConfig, basePath, typeOfConfiguration);
      logger.info("finished reading configuration file.");
    } else if (optionalTest) {
      logger.info("No {} configuration file '{}'...", typeOfConfiguration, configPath);
    } else {
      throw new InvalidFileConfigurationRuntimeException(
          "Failed loading mandatory " + typeOfConfiguration + " configuration '" + configPath, logger);
    }
    result.addConfiguration(resultConfig);
    return resultConfig;
  }

  private void readSubConfigurations(final CombinedConfiguration result, final String basePath,
      final String typeOfConfiguration) {
    // Check if config directory exists and read all config{i}.xml files in that directory where i = [0,...k] such
    // that all resources config0.xml, ... configk.xml exist, but config(k+1).xml does not.
    if (basePath != null) {
      int i = 0;
      String subConfigPath = MessageFormat.format(SUB_CONFIGURATION_TEMPLATE, basePath, i);
      URL subConfigResource = getResource(subConfigPath);
      while (subConfigResource != null) {
        try {
          logger.info("Reading {} sub-configuration file '{}'...", typeOfConfiguration, subConfigPath);
          result.addConfiguration(new XMLConfiguration(subConfigResource));
        } catch (final ConfigurationException e) {
          throw new InvalidFileConfigurationRuntimeException("Failed loading sub configuration '" + subConfigPath + "'",
              e, logger);
        }
        subConfigPath = MessageFormat.format(SUB_CONFIGURATION_TEMPLATE, basePath, ++i);
        subConfigResource = getResource(subConfigPath);
      }
    }
  }

  private CombinedConfiguration loadGlobalConfiguration() {
    return loadDefaultTestConfiguration("", false, false);
  }

  private AbstractConfiguration loadPlatformConfiguration(
      final org.apache.commons.configuration.Configuration baseConfig) {
    final int numPlatforms = baseConfig.getStringArray(PLATFORM_KEY).length;
    String fileName = null;
    final String platformChoice = Platform.osType().name();
    final Set<String> seenPlatforms = new HashSet<String>();
    for (int i = 0; i < numPlatforms; ++i) {
      final String individualOptionalKey = PLATFORM_KEY + "(" + i + ")";
      final String platformName = baseConfig.getString(individualOptionalKey + PLATFORM_NAME_KEY);
      if (seenPlatforms.contains(platformName)) {
        throw new InvalidFileConfigurationRuntimeException("Platform '" + platformName + "' exists twice.'", logger);
      }
      seenPlatforms.add(platformName);

      final String platformFile = baseConfig.getString(individualOptionalKey + PLATFORM_FILE_KEY);
      logger.info("Platform '{}' has configuration file '{}'", platformName, platformFile);
      if (platformName.equals(platformChoice)) {
        fileName = platformFile;
      }
    }

    XMLConfiguration result;
    if (fileName == null) {
      throw new ConfigurationRuntimeException("Platform '" + platformChoice + "' ist not supported.", logger);
    } else {
      logger.info("Reading platform configuration file '{}'...", fileName);
      try {
        result = new XMLConfiguration(getResource(fileName));
      } catch (final Exception e) {
        throw new InvalidFileConfigurationRuntimeException(
            "Failed loading configuration '" + fileName + "' for platform '" + platformChoice + "'.", e, logger);
      }
      logger.info("finished reading configuration file.");
    }
    return result;
  }

  private CombinedConfiguration loadPackageConfiguration(
      final org.apache.commons.configuration.Configuration baseConfig) {
    assert baseConfig != null;
    final CombinedConfiguration result = new CombinedConfiguration(new UnionCombiner());
    final int numPackages = baseConfig.getStringArray(PACKAGE_KEY).length;
    final Set<String> seenPackageNames = new HashSet<String>();
    for (int i = 0; i < numPackages; ++i) {
      final String individualPackageKey = PACKAGE_KEY + "(" + i + ")";
      final String packageName = baseConfig.getString(individualPackageKey);
      if (seenPackageNames.contains(packageName)) {
        throw new InvalidFileConfigurationRuntimeException("Package '" + packageName + "' exists twice.", logger);
      }
      seenPackageNames.add(packageName);
      final AbstractConfiguration newPackageConfig = loadDefaultTestConfiguration(packageName, true, true);
      logOverlap(result, "Collected package configuration so far", newPackageConfig,
          "Package configuration for " + packageName, "Union is taken.");
      result.addConfiguration(newPackageConfig);
    }
    return result;
  }

  private Map<String, String> collectOptionals(final org.apache.commons.configuration.Configuration baseConfig) {
    assert baseConfig != null;

    logger.info(
        "Collection optionals from global, plattform, and package configuration (NOTE: optionals NEVER override each other!)...");

    final Map<String, String> result = new HashMap<String, String>();
    final int numOptionals = baseConfig.getStringArray(OPTIONAL_KEY).length;
    final Set<String> seenNames = new HashSet<String>();
    for (int i = 0; i < numOptionals; ++i) {
      final String individualOptionalKey = OPTIONAL_KEY + "(" + i + ")";
      final String optionalName = baseConfig.getString(individualOptionalKey + OPTIONAL_NAME_KEY);
      if (seenNames.contains(optionalName)) {
        throw new InvalidFileConfigurationRuntimeException("Optional '" + optionalName + "' exists twice.", logger);
      }
      seenNames.add(optionalName);
      final String optionalDefault = baseConfig.getString(individualOptionalKey + OPTIONAL_DEFAULT_KEY);
      final String optionalChoice = getOptionalChoice(optionalName, optionalDefault);
      logger.info("Optional configuration slot '{}' defaults to '{}' and is set to '" + optionalChoice + "'.",
          optionalName, optionalDefault);

      final String compoundAlternativeKey = individualOptionalKey + OPTIONAL_ALTERNATIVE_KEY;
      final int numAlternatives = baseConfig.getStringArray(compoundAlternativeKey).length;
      String fileName = null;
      final Set<String> seenAlternatives = new HashSet<String>();
      for (int j = 0; j < numAlternatives; ++j) {
        final String individualAlternativeKey = compoundAlternativeKey + "(" + j + ")";
        final String alternativeName = baseConfig.getString(individualAlternativeKey + OPTIONAL_NAME_KEY);
        if (seenAlternatives.contains(alternativeName)) {
          throw new InvalidFileConfigurationRuntimeException(
              "Optional '" + optionalName + "' contains alternative '" + alternativeName + "' twice.", logger);
        }
        seenAlternatives.add(alternativeName);
        final String alternativeFile = baseConfig.getString(individualAlternativeKey + OPTIONAL_FILE_KEY);
        logger.info("Optional configuration slot '{}' has option '{}' with file '" + alternativeFile + "'.",
            optionalName, alternativeName);
        if (alternativeName.equals(optionalChoice)) {
          fileName = alternativeFile;
        }
      }

      if (fileName == null) {
        throw new ConfigurationRuntimeException(
            "Optional configuratoin slot '" + optionalName + "' has no optional setting '" + optionalChoice + "'.",
            logger);
      } else {
        result.put(optionalName, fileName);
      }
    }

    logger.info("finished collecting optionals from global, plattform, and package configurations.");

    return result;
  }

  private CombinedConfiguration loadOptionalConfiguration(final Map<String, String> optionalMap, String basePath,
      final boolean optional) {
    assert optionalMap != null;
    assert basePath != null;

    // if the path is non-empty but does not end in "/" add "/"
    if (!(basePath.isEmpty() || basePath.endsWith("/"))) {
      basePath = basePath + "/";
    }

    final CombinedConfiguration result = new CombinedConfiguration(new UnionCombiner());

    for (final Map.Entry<String, String> optionalEntry : optionalMap.entrySet()) {
      final String filePath = basePath + optionalEntry.getValue();
      final String optionalName = optionalEntry.getKey();
      final URL configPath = getResource(filePath);
      if (configPath != null) {
        logger.info("Reading optional configuration file '{}' ...", filePath);
        try {
          result.addConfiguration(new XMLConfiguration(configPath));
        } catch (final Exception e) {
          throw new InvalidFileConfigurationRuntimeException(
              "Failed loading configuration '" + filePath + "' for configuration slot '" + optionalName + "'.", e,
              logger);
        }
        logger.info("finished reading configuration file.");
      } else if (optional) {
        logger.info("No configuration '{}' for configuration slot '{}'.", filePath, optionalName);
      } else {
        throw new InvalidFileConfigurationRuntimeException(
            "No configuration '" + filePath + "' for configuration slot '" + optionalName + "'.", logger);
      }

    }
    return result;
  }

  private CombinedConfiguration loadOptionalConfiguration(
      final org.apache.commons.configuration.Configuration baseConfig) {
    // return loadOptionalConfiguration(baseConfig, "", true);

    final CombinedConfiguration result = new CombinedConfiguration(new UnionCombiner());
    final Map<String, String> optionalMap = collectOptionals(baseConfig);

    result.addConfiguration(loadOptionalConfiguration(optionalMap, "", true));
    final int numPackages = baseConfig.getStringArray(PACKAGE_KEY).length;
    final Set<String> seenPackageNames = new HashSet<String>();
    for (int i = 0; i < numPackages; ++i) {
      final String individualPackageKey = PACKAGE_KEY + "(" + i + ")";
      final String packageName = baseConfig.getString(individualPackageKey);
      if (seenPackageNames.contains(packageName)) {
        throw new InvalidFileConfigurationRuntimeException("Package '" + packageName + "' exists twice.", logger);
      }
      seenPackageNames.add(packageName);
      final AbstractConfiguration newPackageConfig = loadOptionalConfiguration(optionalMap, packageName, true);
      logOverlap(result, "Collected optional configurations so far", newPackageConfig,
          "Optional configuration for " + packageName, "Union is taken.");
      result.addConfiguration(newPackageConfig);
    }
    return result;
  }

  private void loadInternally() {
    // We want to avoid overriding existing (maybe externally provided)
    // logging configurations, so we check before setting our initial.
    String resource = initialLog4jResourceName;
    boolean debugConfig = false;
    if (System.getProperty("LOG_CONFIG_LOADER") != null) {
      final String prop = System.getProperty("LOG_CONFIG_LOADER");
      debugConfig = prop.equals("debug");
      if (debugConfig) {
        resource = debugLog4jResourceName;
      } else {
        resource = prop;
      }
    }
    final boolean existsExternalLogging = overrideLoggingConfIfNotSetYet(resource);

    logger.info("The current main class is '{}', isTestRun={}.", mainClassName, isTestrun);
    logger.info("The system type is identified as '{}'.", Platform.osType().name());

    globalConfig = loadGlobalConfiguration();
    platformConfig = loadPlatformConfiguration(globalConfig);
    packageConfig = loadPackageConfiguration(globalConfig);
    // here we use a UnionCombiner to get the optionals -- to collect all
    // optionals. NOTE that these configurations are
    // later combined via an OverrideCombinder
    final CombinedConfiguration cc = new CombinedConfiguration(new UnionCombiner());
    cc.addConfiguration(packageConfig);
    cc.addConfiguration(platformConfig);
    cc.addConfiguration(globalConfig);
    optionalConfig = loadOptionalConfiguration(cc);
    applicationConfig = null;
    localConfig = loadLocalConfiguration();

    // Check that the configuration contains no conflicts
    /*
     * @SuppressWarnings("unchecked") Iterator<String> keys=config.getKeys(); while(keys.hasNext()) { String
     * key=keys.next(); if(config.getStringArray(key).length!=1) { throw new
     * AmbiguousPropertyConfigurationRuntimeException ("Key '"+key+"' multiple defined.", logger); } }
     */
    // TODO We need a check against overlapping configurations (or not)

    // Putting the configuration in place
    buildConfiguration();

    // If we did set our initial logging successfully, it means there was no
    // external configuration. Therefore we can load a local configuration
    // if any.
    if (!debugConfig && !existsExternalLogging) {
      loadLoggingConfiguration();
    }
  }

  private boolean overrideLoggingConfIfNotSetYet(final String log4jConfigFile) {
    final Enumeration<?> allAppenders = org.apache.log4j.Logger.getRootLogger().getAllAppenders();
    // check if some appenders are already defined
    if (!(allAppenders instanceof NullEnumeration) && false) { // TODO hu -- why did we add this check
      logger.warn("Logging configuration '{}' not loaded because appenders did already exist.", log4jConfigFile);
      return true;
    }
    try {
      if (getResource(log4jConfigFile) != null) {
        PropertyConfigurator.configure(getResource(log4jConfigFile));
      } else {
        PropertyConfigurator.configure(log4jConfigFile);
      }
      return false;
    } catch (final Exception e) {
      throw new InvalidFileConfigurationRuntimeException(
          "Failed loading logging configuration '" + log4jConfigFile + "'", e, logger);
    }
  }

  public void loadLoggingConfiguration() {
    if (config.containsKey(log4jConfigFileConfigurationProperty)) {
      final String log4jConfigFile = config.getString(log4jConfigFileConfigurationProperty);
      try {
        if (getResource(log4jConfigFile) != null) {
          logger.info("Switching to logging configuration in file '{}'.", log4jConfigFile);
          BasicConfigurator.resetConfiguration();
          PropertyConfigurator.configure(getResource(log4jConfigFile));
        } else {
          logger.info("Switching to logging configuration in resource '{}'.", log4jConfigFile);
          BasicConfigurator.resetConfiguration();
          PropertyConfigurator.configure(log4jConfigFile);
        }
      } catch (final Exception e) {
        throw new InvalidFileConfigurationRuntimeException(
            "Failed loading local logging configuration '" + log4jConfigFile + "'", e, logger);
      }
    }
  }

  // ////////////////////////////////////////////////////////////////////////////////////////////////////////////

  private String getOptionalChoice(final String optionalName, final String optionalDefault) {
    assert optionalName != null;
    assert optionalDefault != null;
    final String setProperty = System.getProperty(optionalName);
    if (setProperty == null) {
      return optionalDefault;
    }
    return setProperty;
  }

  public synchronized void setApplicationConfiguration(final String resourceName) {
    assert resourceName != null;

    logger.info("Reading application configuration file '{}'...", resourceName);
    if (getResource(resourceName) == null) {
      throw new InvalidFileConfigurationRuntimeException(
          "Application configuration '" + resourceName + "' does not exist.", logger);
    }
    setApplicationConfiguration(getResource(resourceName));
  }

  public synchronized void setApplicationConfiguration(final URL input) {
    assert input != null;

    // final String oldLog4jConfigFile = config.getString(log4jConfigFileConfigurationProperty);
    try {
      applicationConfig = new XMLConfiguration(input);
    } catch (final Exception e) {
      throw new InvalidFileConfigurationRuntimeException(
          "Failed loading application configuration '" + input.toString() + "'", e, logger);
    }
    logger.info("finished reading application configuration file.");

    buildConfiguration();
    logger.info("added '{}' as application configuration to the configuration.", input.toString());

    // final String newLog4jConfigFile = config.getString(log4jConfigFileConfigurationProperty);
    // if (!oldLog4jConfigFile.equals(newLog4jConfigFile)) {
    loadLoggingConfiguration();
    // }
  }

  public synchronized void resetApplicationConfiguration() {
    final String oldLog4jConfigFile = config.getString(log4jConfigFileConfigurationProperty);

    applicationConfig = null;
    buildConfiguration();
    logger.info("reset application configuration.");

    final String newLog4jConfigFile = config.getString(log4jConfigFileConfigurationProperty);
    if (!oldLog4jConfigFile.equals(newLog4jConfigFile)) {
      loadLoggingConfiguration();
    }
  }

  private void logOverlap() {
    final AbstractConfiguration[] configs = new AbstractConfiguration[] { globalConfig, platformConfig, packageConfig,
        optionalConfig, applicationConfig, localConfig };
    final String[] configNames = new String[] { "global", "platform", "package", "optional", "local", "application" };
    assert configs.length == configNames.length;

    for (int i = 0; i < configs.length; ++i) {
      for (int j = i + 1; j < configs.length; ++j) {
        logOverlap(configs[i], configNames[i] + " configuration", configs[j], configNames[j] + " configuration",
            configNames[j] + " wins");
      }
    }
  }

  private void buildConfiguration() {
    logOverlap(); // TODO be conscious that the list of configurations
    // reoccurs ins logOverlap.
    final CombinedConfiguration cc = new CombinedConfiguration(new OverrideCombiner());
    if (applicationConfig != null) {
      cc.addConfiguration(applicationConfig);
    }
    if (localConfig != null) {
      cc.addConfiguration(localConfig);
    }
    cc.addConfiguration(optionalConfig);
    cc.addConfiguration(packageConfig);
    cc.addConfiguration(platformConfig);
    cc.addConfiguration(globalConfig);
    config = cc;
  }

  // /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  public void addOptionalConfiguration(final String resourceName) {
    assert resourceName != null;

    logger.info("Reading application configuration file '{}'...", resourceName);
    if (ClassLoader.getSystemResource(resourceName) == null) {
      throw new InvalidFileConfigurationRuntimeException(
          "Optional configuration '" + resourceName + "' does not exist.", logger);
    }
    addOptionalConfiguration(getResource(resourceName));

  }

  public void addOptionalConfiguration(final URL resource) {
    assert resource != null;

    // final String oldLog4jConfigFile =
    // config.getString(log4jConfigFileConfigurationProperty);
    try {
      optionalConfig.addConfiguration(new XMLConfiguration(resource));
    } catch (final Exception e) {
      throw new InvalidFileConfigurationRuntimeException(
          "Failed loading optional configuration '" + resource.toString() + "'", e, logger);
    }
    logger.info("finished reading optional configuration file.");

    buildConfiguration();
    logger.info("added '{}' as optional configuration to the configuration.", resource.toString());
  }
}
