package uk.ac.ox.cs.diadem.env.testsupport;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.env.testsupport.StringDatabase.Mode;

public class StringDatabaseTest extends StandardTestcase {

  @Before
  public void tearUp() {
    ConfigurationFacility
        .setApplicationConfiguration("/uk/ac/ox/cs/diadem/env/testsupport/StringDatabaseTestConfiguration.xml");
  }

  /**
   * SDBITest feeds queries from the StringDatabaseImplementation with a client supplied sequence of answers.
   */
  private static class SDBITest extends StringDatabaseImplementation {

    protected final static Map<Class<?>, SDBITest> instances = new HashMap<Class<?>, SDBITest>();

    private final Queue<Boolean> answers = new LinkedList<Boolean>();

    private SDBITest(Class<?> classObject) throws TestSupportException {
      super(classObject);
    }

    static public SDBITest getInstance(final Class<?> classObject) {
      assert classObject != null;
      SDBITest result = instances.get(classObject);
      if (result == null)
        instances.put(classObject, result = new SDBITest(classObject));
      return result;
    }

    @Override
    protected boolean askUserBoolean(String question) {
      if (answers.isEmpty())
        throw new TestSupportException("No more answers left.", logger);
      return answers.poll();
    }

    public void provideUserAnswers(boolean... moreAnswers) {
      for (boolean answer : moreAnswers)
        answers.add(answer);
    }
  }

  // ////////////////////////////////////////////////////////////////////////////////

  @Test
  public void testScratch() throws TestSupportException {
    database.setMethodKeyPrefix();
    SDBITest db = SDBITest.getInstance(Integer.class);
    db.clear();
    db.setMode(Mode.INTERACTIVE);
    assertEquals(Mode.INTERACTIVE, db.getMode());

    db.provideUserAnswers(false, false);
    assertFalse(db.check("a", "b"));
    assertFalse(db.check("aa", "b\nb"));
  }

  private void testTestOrLockedMode(Mode mode) throws TestSupportException {
    database.setMethodKeyPrefix();
    SDBITest db = SDBITest.getInstance(Double.class);
    db.clear();
    db.setMode(Mode.RECORD);
    db.check("aa", "bb");
    assertEquals(db.lookup("aa"), "bb");
    db.setMode(mode);
    assertEquals(mode, db.getMode());

    assertFalse(db.check("a", "b"));
    assertFalse(db.check("a", "b"));
    assertEquals(db.lookup("a"), null);
    assertEquals(db.lookup("aa"), "bb");

    assertTrue(db.check("aa", "bb"));
    assertFalse(db.check("aa", "bb1"));
    assertEquals(db.lookup("aa"), "bb");
  }

  @Test
  public void testTestMode() throws TestSupportException {
    database.setMethodKeyPrefix();
    testTestOrLockedMode(Mode.TEST);
  }

  @Test
  public void testLockedMode() throws TestSupportException {
    database.setMethodKeyPrefix();
    testTestOrLockedMode(Mode.LOCKED);
  }

  @Test(expected = TestSupportException.class)
  public void testLockedMode2() throws TestSupportException {
    database.setMethodKeyPrefix();
    SDBITest db = SDBITest.getInstance(Double.class);
    db.clear();
    db.setMode(Mode.LOCKED);
    assertEquals(Mode.LOCKED, db.getMode());

    db.setMode(Mode.RECORD);
  }

  @Test
  public void testInteractiveMode() throws TestSupportException {
    database.setMethodKeyPrefix();
    SDBITest db = SDBITest.getInstance(Integer.class);
    db.clear();
    db.setMode(Mode.INTERACTIVE);
    assertEquals(Mode.INTERACTIVE, db.getMode());

    db.provideUserAnswers(false, false);
    assertFalse(db.check("a", "b"));
    assertFalse(db.check("a", "b"));
    assertEquals(db.lookup("a"), null);
    db.provideUserAnswers(true);
    assertTrue(db.check("a", "b"));
    assertTrue(db.check("a", "b"));
    assertEquals(db.lookup("a"), "b");

    db.provideUserAnswers(true);
    assertEquals(db.lookup("aa"), null);
    assertTrue(db.check("aa", "bb"));
    assertEquals(db.lookup("aa"), "bb");
    assertTrue(db.check("aa", "bb"));

    assertEquals(db.lookup("aaa"), null);

    assertTrue(db.check("aa", "bb"));
    db.provideUserAnswers(false, true);
    assertFalse(db.check("aa", "bb1"));
    assertEquals(db.lookup("aa"), "bb");
    assertTrue(db.check("aa", "bb1"));
    assertEquals(db.lookup("aa"), "bb1");
  }

  @Test
  public void testRecordMode() throws TestSupportException {
    database.setMethodKeyPrefix();
    SDBITest db = SDBITest.getInstance(Integer.class);
    db.clear();
    db.setMode(Mode.RECORD);
    assertEquals(Mode.RECORD, db.getMode());

    assertEquals(db.lookup("a"), null);
    assertTrue(db.check("a", "b"));
    assertEquals(db.lookup("a"), "b");
    assertTrue(database.check("dump", db.toString()));

    assertEquals(db.lookup("aa"), null);
    assertTrue(db.check("aa", "bb"));
    assertTrue(database.check("dump1", db.toString()));
    assertEquals(db.lookup("aa"), "bb");
    assertTrue(db.check("aa", "bb"));
    assertTrue(database.check("dump1", db.toString()));
    assertEquals(db.lookup("aa1"), null);
    assertTrue(db.check("aa1", "bb1"));
    assertTrue(database.check("dump2", db.toString()));

    assertEquals(db.lookup("aaa"), null);

    assertTrue(db.check("aa", "bb"));
    assertTrue(db.check("aa", "bb1"));
    assertEquals(db.lookup("aa"), "bb1");
  }

  @Test
  public void testPrefix() throws TestSupportException {
    database.setMethodKeyPrefix();
    SDBITest db = SDBITest.getInstance(Integer.class);
    db.clear();
    db.setMode(Mode.INTERACTIVE);
    assertEquals(Mode.INTERACTIVE, db.getMode());

    assertEquals("", db.getKeyPrefix());
    db.setKeyPrefix("alpha/");
    assertEquals("alpha/", db.getKeyPrefix());
    db.provideUserAnswers(true, true);
    assertTrue(db.check("a", "b"));
    assertTrue(db.check("aa", "b\nb"));

    db.setKeyPrefix("");
    db.setMode(Mode.TEST);
    assertTrue(db.check("alpha/a", "b"));
    assertTrue(db.check("alpha/aa", "b\nb"));
    assertFalse(db.check("alpha/aa", "b"));
    assertFalse(db.check("a", "b"));
    assertFalse(db.check("aa", "b\nb"));

    db.setKeyPrefix("alpha/");
    assertTrue(db.check("a", "b"));
    assertFalse(db.check("a", "b1"));
    assertTrue(db.check("aa", "b\nb"));
    assertEquals("alpha/", db.getKeyPrefix());
    db.resetMethodKeyPrefix();
    assertEquals("", db.getKeyPrefix());
  }

  @Test
  public void testCheckSorted() {
    database.setMethodKeyPrefix();
    SDBITest db = SDBITest.getInstance(Short.class);
    db.clear();
    db.setMode(Mode.RECORD);
    db.check("1", "1\n2\n3\n");
    assertEquals("1\n2\n3\n", db.lookup("1"));
    db.setMode(Mode.TEST);
    assertTrue(db.check("1", "1\n2\n3\n"));
    assertTrue(db.checkSorted("1", "1\n2\n2\n3"));
    assertTrue(db.checkSorted("1", "1\n3\n1\n2"));
    assertTrue(db.check("1", "1\n2\n3\n"));

    db.setMode(Mode.RECORD);
    assertTrue(db.checkSorted("2", "3\n1\n1\n2\n"));
    db.setMode(Mode.TEST);
    assertTrue(db.check("2", "1\n2\n3\n"));
  }
}
