package uk.ac.ox.cs.diadem.env.testsupport;

import static org.junit.Assert.*;


import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;
import uk.ac.ox.cs.diadem.env.testsupport.StringDatabase;
import uk.ac.ox.cs.diadem.env.testsupport.StringDatabaseImplementation;
import uk.ac.ox.cs.diadem.env.testsupport.TestFacilities;
import uk.ac.ox.cs.diadem.env.testsupport.TestSupportException;

public class TestFacilitiesTest extends StandardTestcase {
		
	@Test
	public void testgetStringDatabase() throws TestSupportException  {
		StringDatabase db1=TestFacilities.getStringDatabase(this.getClass());
		StringDatabase db2=StringDatabaseImplementation.getInstance(TestFacilitiesTest.class);
		assertTrue(db1==db2);
		StringDatabase db3=TestFacilities.getStringDatabase(Integer.class);
		assertTrue(db1!=db3);	
	}	
}
