package uk.ac.ox.cs.diadem.util.collect;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;

public class IteratorOfIterablesIteratorTest extends StandardTestcase {

  @Test(expected = AssertionError.class)
  public void testIteratorOfIterablesIterator() {
    new IteratorOfIterablesIterator<Integer, Set<Integer>>(null);
  }

  @Test
  public void testHasNext() {
    assertFalse(getNullListIterator().hasNext());
    assertFalse(getEmptyBaseIterator().hasNext());
    assertTrue(getSmallIterator().hasNext());
  }

  @Test
  public void testNext() {
    Iterator<Integer> iter = getSmallIterator();
    int i;
    for (i = 1; iter.hasNext(); ++i)
      assertEquals(iter.next(), new Integer(i));
    assertEquals(i, 7);
  }

  @Test
  public void testRemove() {
    for (int pos = 1; pos < 7; ++pos) {
      Set<Set<Integer>> setSet = getSmallSetSet();
      Iterator<Integer> iter = new IteratorOfIterablesIterator<Integer, Set<Integer>>(setSet.iterator());
      for (int i = 0; i < pos; ++i)
        iter.next();
      iter.remove();
      iter = new IteratorOfIterablesIterator<Integer, Set<Integer>>(setSet.iterator());
      int i;
      for (i = 1; i < 7; ++i) {
        if (i == pos)
          continue;
        assertEquals(iter.next(), new Integer(i));
      }
      assertEquals(i, 7);
    }
  }

  @Test(expected = IllegalStateException.class)
  public void testRemoveNoNext() {
    final Iterator<Integer> iter = getSmallIterator();
    iter.remove();
  }

  @Test(expected = IllegalStateException.class)
  public void testRemoveDoubleDouble() {
    final Iterator<Integer> iter = getSmallIterator();
    iter.next();
    try {
      iter.remove();
    } catch (Exception e) {
      assert false;
    }
    iter.remove();
  }

  private static Set<Set<Integer>> getSmallSetSet() {
    Set<Set<Integer>> enclosingSet = new HashSet<Set<Integer>>();

    Set<Integer> set = new HashSet<Integer>();
    set.add(1);
    set.add(2);
    set.add(3);
    set.add(4);
    enclosingSet.add(set);
    enclosingSet.add(null);
    set = new HashSet<Integer>();
    set.add(5);
    set.add(6);
    enclosingSet.add(set);
    enclosingSet.add(new HashSet<Integer>());

    return enclosingSet;
  }

  private static Iterator<Integer> getSmallIterator() {
    return new IteratorOfIterablesIterator<Integer, Set<Integer>>(getSmallSetSet().iterator());
  }

  private static Iterator<Integer> getNullListIterator() {
    Set<Set<Integer>> enclosingSet = new HashSet<Set<Integer>>();

    enclosingSet.add(null);
    enclosingSet.add(null);
    enclosingSet.add(null);

    return new IteratorOfIterablesIterator<Integer, Set<Integer>>(enclosingSet.iterator());
  }

  private static Iterator<Integer> getEmptyBaseIterator() {
    Set<Set<Integer>> enclosingSet = new HashSet<Set<Integer>>();
    return new IteratorOfIterablesIterator<Integer, Set<Integer>>(enclosingSet.iterator());
  }

}
