package uk.ac.ox.cs.diadem.env.configuration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;

// TODO use a local configuration instead of the global one

public class ConfigurationTest extends StandardTestcase {

  @Before
  public void tearUp() {
    ConfigurationFacility
        .setApplicationConfiguration("/uk/ac/ox/cs/diadem/env/configuration/ConfigurationTestConfiguration.xml");
  }

  @Test
  public void testConfigurationExistance() {
    database.setMethodKeyPrefix();
    assertTrue(config != null);
    assertEquals(config, ConfigurationImpl.instance());
  }

  @Test
  public void testConfigurationAccess() {
    database.setMethodKeyPrefix();
    assertTrue(config != null);
    assertFalse(config.isEmpty());
    assertEquals(1, config.getInt("playground.p1"));
    assertEquals(2, config.getInt("playground.p2"));

    final StringBuilder sb = new StringBuilder();
    final Iterator<String> keys = config.getKeys();
    while (keys.hasNext()) {
      final String key;
      sb.append(key = keys.next());
      final String[] values = config.getStringArray(key);
      sb.append(':');
      sb.append(Integer.toString(values.length));
      sb.append('\n');
      for (final String value : values) {
        sb.append("  ");
        sb.append(value);
        sb.append("\n");
      }
    }
    // assertTrue(database.check("keylist", sb.toString()));

    // assertTrue(database.check("dump", config.toString("playground")));
  }

  @Test
  public void testGetStringArray() {
    database.setMethodKeyPrefix();
    assertTrue(config != null);
    assertFalse(config.isEmpty());
    assertEquals(config.getInt("playground.p1"), 1);
    assertEquals(config.getInt("playground.p2"), 2);

    assertEquals(1, config.getStringArray("playground").length);
    assertEquals(1, config.getStringArray("playground.p1").length);
    assertEquals(1, config.getStringArray("playground.p2").length);

    assertTrue(config.getStringArray("doesNotExist") != null);
    assertEquals(0, config.getStringArray("doesNotExist").length);
  }

  @Test(expected = org.apache.commons.configuration.ConversionException.class)
  public void testGetMissformatedString() {
    database.setMethodKeyPrefix();
    assertTrue(config != null);
    assertFalse(config.isEmpty());
    assertEquals(config.getString("playground.enumvalue"), "ABA");
    assertEquals(config.getBoolean("playground.enumvalue"), true);
  }

  @Test
  public void testGetList() {
    database.setMethodKeyPrefix();
    assertTrue(config != null);
    assertFalse(config.isEmpty());

    final Iterator<String> iter1 = config.getList("playground.list1").iterator();
    assertTrue(iter1.hasNext());
    assertEquals(iter1.next(), "a b");
    assertFalse(iter1.hasNext());

    final Iterator<String> iter2 = config.getList("playground.list2").iterator();
    assertTrue(iter2.hasNext());
    assertEquals(iter2.next(), "a");
    assertTrue(iter2.hasNext());
    assertEquals(iter2.next(), "b");
    assertFalse(iter2.hasNext());

    final Iterator<String> iter3 = config.getList("playground.list-non-existing").iterator();
    assertFalse(iter3.hasNext());

    final List<String> list = new ArrayList<String>();
    list.add("cain");
    list.add("abel");
    list.add("seth");
    assertEquals(3, config.getList("playground.list-non-existing", list).size());
  }

  @Test
  public void testGetStringArray2() {
    database.setMethodKeyPrefix();
    assertTrue(config != null);
    assertFalse(config.isEmpty());

    assertEquals(1, config.getStringArray("playground.list1").length);
    assertEquals(config.getStringArray("playground.list1")[0], "a b");

    assertEquals(2, config.getStringArray("playground.list2").length);
    assertEquals(config.getStringArray("playground.list2")[0], "a");
    assertEquals(config.getStringArray("playground.list2")[1], "b");

    assertEquals(0, config.getStringArray("playground.list-non-existing").length);
  }

  @Test(expected = MissingPropertyConfigurationRuntimeException.class)
  public void testMissingConfigurationAccess() {
    database.setMethodKeyPrefix();
    assertTrue(config != null);
    assertEquals(config.getInt("testing.p3"), 2);
  }

  @Test(expected = InvalidFileConfigurationRuntimeException.class)
  public void testMissingConfigFile() {
    try {
      database.setMethodKeyPrefix();
      assertTrue(config != null);
      ConfigurationFacility.setApplicationConfiguration("does/not/exist.xml");
    } finally {
      assertTrue(database.check("dump", config.toString("playground")));
    }
  }

  @Test(expected = InvalidFileConfigurationRuntimeException.class)
  public void testMalformedConfigFile() {
    try {
      database.setMethodKeyPrefix();
      assertTrue(config != null);
      ConfigurationFacility
          .setApplicationConfiguration("/uk/ac/ox/diadem/env/configuration/MalformedTestConfiguration.xml");
    } finally {
      assertTrue(database.check("dump", config.toString("playground")));
    }
  }

  @Test
  public void testIsTestRun() {
    assertTrue(ConfigurationFacility.isTestRun());
  }

  enum TestType {
    ABA, ABB, ABC
  };

  @Test
  public void testGetEnum() {
    assertEquals(TestType.ABA, config.getEnum("playground.enumvalue", TestType.ABA));
    assertEquals(TestType.ABB, config.getEnum("playground.enumvalueother", TestType.ABC));
    assertEquals(TestType.ABC, config.getEnum("playground.does/not/exist", TestType.ABC));

    assertEquals(TestType.ABA, config.getEnumNoDefault("playground.enumvalue", TestType.ABA));
    assertEquals(TestType.ABB, config.getEnumNoDefault("playground.enumvalueother", TestType.ABA));
  }

  @Test(expected = MissingPropertyConfigurationRuntimeException.class)
  public void testGetEnumNoDefaultMissing() {
    assertEquals(TestType.ABB, config.getEnumNoDefault("playground.does/not/exist", TestType.ABB));
  }

  @Test(expected = MalformedPropertyConfigurationRuntimeException.class)
  public void testGetEnumNoDefaultMalformed() {
    assertEquals(TestType.ABB, config.getEnumNoDefault("playground.enumvaluemalformed", TestType.ABB));
  }

  @Test(expected = MalformedPropertyConfigurationRuntimeException.class)
  public void testGetEnumNoDefaultEmpty() {
    assertEquals(TestType.ABB, config.getEnumNoDefault("playground.enumvalueempty", TestType.ABB));
  }

  @Test(expected = MalformedPropertyConfigurationRuntimeException.class)
  public void testGetEnumNoDefaultWhitespace() {
    assertEquals(TestType.ABB, config.getEnumNoDefault("playground.enumvaluewhitespace", TestType.ABB));
  }

  @Test(expected = MalformedPropertyConfigurationRuntimeException.class)
  public void testGetEnumMalformed() {
    assertEquals(TestType.ABB, config.getEnum("playground.enumvaluemalformed", TestType.ABB));
  }

  @Test(expected = MalformedPropertyConfigurationRuntimeException.class)
  public void testGetEnumEmpty() {
    assertEquals(TestType.ABB, config.getEnum("playground.enumvalueempty", TestType.ABB));
  }

  @Test(expected = MalformedPropertyConfigurationRuntimeException.class)
  public void testGetEnumWhitespace() {
    assertEquals(TestType.ABB, config.getEnum("playground.enumvaluewhitespace", TestType.ABB));
  }

  @Test
  public void testSubConfiguration() {
    database.setMethodKeyPrefix();
    // assertTrue(database.check("config", config.toString()));
    final Configuration subConfig = config.getSubConfiguration("playground");
    assertTrue(database.check("subconfig", subConfig.toString()));
    for (final String key : new String[] { "p1", "p2", "enumvalue", "enumvaluewhitespace", "list1", "list2" }) {
      assertTrue(database.check("subconfig/" + key, subConfig.getString(key)));
    }
  }

  @Test
  public void testSubConfigurationListSingle() {
    database.setMethodKeyPrefix();
    // assertTrue(database.check("config", config.toString()));
    final Configuration[] subConfigs = config.getSubConfigurationArray("playground");
    assertTrue(database.check("subconfig", subConfigs[0].toString()));
    for (final String key : new String[] { "p1", "p2", "enumvalue", "enumvaluewhitespace", "list1", "list2" }) {
      assertTrue(database.check("subconfig/" + key, subConfigs[0].getString(key)));
    }
  }

  @Test
  public void testSubConfigurationListMultiple() {
    database.setMethodKeyPrefix();
    // assertTrue(database.check("config", config.toString()));
    final Configuration[] subConfigs = config.getSubConfigurationArray("a");
    assertEquals(4, subConfigs.length);
    int i = -1;
    for (final Configuration subConfig : subConfigs) {
      assertTrue(database.check("subconfigs-" + ++i, subConfig.toString()));
    }
  }

  @Test
  public void testSplitIntoArray() {
    database.setMethodKeyPrefix();
    final Configuration playground = config.getSubConfiguration("pseudoprocessors");
    assertTrue(database.check("split-base", playground.toString()));
    final ArrayList<String> keys = new ArrayList<String>();
    for (final String key : playground.getChildKeys()) {
      keys.add(key);
    }
    assertTrue(database.check("keys", keys.toString()));
    final Configuration[] subConfigs = playground.splitIntoArrays();
    int i = -1;
    for (final Configuration subConfig : subConfigs) {
      assertTrue(database.check("split-" + ++i, subConfig.toString()));
    }
  }

  @Test(expected = MissingPropertyConfigurationRuntimeException.class)
  public void testSubConfigurationMissingInnerKey() {
    database.setMethodKeyPrefix();
    final Configuration subConfig = config.getSubConfiguration("playground");
    try {
      config.getString("logfile");
    } catch (final Exception e) {
      assert false;
    }
    subConfig.getString("logfile");
  }

  @Test(expected = MissingPropertyConfigurationRuntimeException.class)
  public void testSubConfigurationMissingKey() {
    database.setMethodKeyPrefix();
    @SuppressWarnings("unused")
    final Configuration subConfig = config.getSubConfiguration("playground");
    @SuppressWarnings("unused")
    final Configuration subConfigFAIL = config.getSubConfiguration("playground1");
  }

  @Test(expected = AssertionError.class)
  public void testSubConfigurationViaKeyWithDot() {
    database.setMethodKeyPrefix();
    @SuppressWarnings("unused")
    final Configuration subConfig = config.getSubConfiguration("playground.");
  }

  @Test
  public void testHasSubConfiguration() {
    database.setMethodKeyPrefix();
    assertTrue(config.hasSubConfiguration("playground"));
    assertTrue(config.hasSubConfiguration("playground(0)"));
    assertFalse(config.hasSubConfiguration("playgroun"));

    final Configuration subConfig = config.getSubConfiguration("should-be-the-same");
    assertTrue(database.check("hasSame", subConfig.toString()));
    assertTrue(subConfig.hasSubConfiguration("same1"));
    assertTrue(subConfig.hasSubConfiguration("same2"));
    final Configuration subConfig1 = subConfig.getSubConfiguration("same1");
    final Configuration subConfig2 = subConfig.getSubConfiguration("same2");
    assertTrue(database.check("same1", subConfig1.toString()));
    assertTrue(database.check("same2", subConfig2.toString()));

    assertTrue(database.check("same1p", toString(subConfig1.getPath())));
    assertTrue(database.check("same2p", toString(subConfig2.getPath())));
  }

  @Test
  public void testChildKeys() {
    database.setMethodKeyPrefix();

    final Configuration subConfig = config.getSubConfiguration("childkeys");
    assertTrue(database.check("config", subConfig.toString()));
    int i = -1;
    for (final String key : subConfig.getChildKeys()) {
      assertTrue(database.check("childkey" + ++i, key));
      assertEquals(0, i);
    }
  }

  @Test
  public void testObjectCreation() {
    @SuppressWarnings("unused")
    final ConfigurableInterface configTest = ConfigurationFacility.createObject("testObject",
        ConfigurableInterface.class);
  }

  // TODO add test cases for the other access methods

  // TODO add test cases for overridden load
}
