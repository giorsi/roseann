package uk.ac.ox.cs.diadem.env.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigurableObject implements ConfigurableInterface {
  private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurableObject.class);
  private final String parameter;

  public ConfigurableObject(final Configuration config) {
    assert config != null;
    parameter = config.getString("parameter");
    LOGGER.info("parameter: {}", parameter);

    // someComponent=ConfigurationFacility.createObject(config.getSubConfiguration("someComponent"), Component.class);
  }
}
