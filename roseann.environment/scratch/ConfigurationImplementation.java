package uk.ac.ox.cs.diadem.env.configuration;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.CombinedConfiguration;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.configuration.tree.OverrideCombiner;
import org.apache.commons.configuration.tree.UnionCombiner;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.helpers.NullEnumeration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.util.collect.IterableIterator;
import uk.ac.ox.cs.diadem.util.collect.WrappingIterableIterator;

// TODO add a TestCase which does not refer to the really used configuration but to a test configuration

// TODO test the package/optional configuration

// TODO add improved dump (take care of multiplicities)

// TODO right now, the configurations are not checked for overlaps at all

// TODO separate configuration from configuration construction

/**
 * 
 * @author christian
 * 
 *         TESTING: except for the delegated wrapping, all methods have been used
 * 
 */
class ConfigurationImplementation implements uk.ac.ox.cs.diadem.env.configuration.Configuration {

  private static Logger logger = LoggerFactory.getLogger(ConfigurationImplementation.class);

  /**
   * Name of default (i.e., non-testing) test configurations
   */
  private static final String DEFAULT_NAME = "";
  /**
   * Name of test configurations
   */
  private static final String TEST_NAME = "Test";

  /**
   * Delimiter for string arrays
   */
  private static final String LIST_DELIMITER = ",";

  /**
   * Resource names for global base configuration files
   */
  private static final String GLOBAL_POSTFIX = "Configuration.xml";
  private static final String GLOBAL_DEFAULT_RESOURCE_NAME = DEFAULT_NAME + GLOBAL_POSTFIX;
  private static final String GLOBAL_TEST_RESOURCE_NAME = TEST_NAME + GLOBAL_POSTFIX;

  /**
   * File names for local configuration files
   */
  private static final String LOCAL_DEFAULT_FILE_NAME = "src" + File.separator + "main" + File.separator + "config"
      + File.separator + DEFAULT_NAME + "Configuration.xml";
  private static final String LOCAL_TEST_FILE_NAME = "src" + File.separator + "test" + File.separator + "config"
      + File.separator + TEST_NAME + "Configuration.xml";

  /**
   * Keys for Optional Configurations
   */
  private static final String OPTIONAL_KEY = "optional";
  private static final String OPTIONAL_NAME_KEY = ".name";
  private static final String OPTIONAL_DEFAULT_KEY = ".default";
  private static final String OPTIONAL_FILE_KEY = ".file";
  private static final String OPTIONAL_ALTERNATIVE_KEY = ".alternative";

  /**
   * Keys for Package Configurations
   */
  private static final String PACKAGE_KEY = "package";

  /**
   * Keys for Platform Configurations
   */
  private static final String PLATFORM_KEY = "platform";
  private static final String PLATFORM_NAME_KEY = ".name";
  private static final String PLATFORM_FILE_KEY = ".file";

  /**
   * String searched for in fully qualified class name of the class
   * 
   * which started the main thread. If this substring is found, then the test configurations are loaded, otherwise the
   * default configurations chosen.
   */
  private static final String[] unitTestMainClassSubstrings = { "junit.runner", "org.apache.maven.surefire.booter" };

  /**
   * Initial configuration of log4j during loading the configuration
   */
  private static final String initialLog4jResourceName = "InitialLog4j.properties";
  /**
   * The property searched for the log4j configuration used after loading the configuration
   */
  private static final String log4jConfigFileConfigurationProperty = "logfile";

  private static final String mainClassName = getMainclassName();

  private static final boolean isTestrun = isTestrunCheck();

  private static ConfigurationImplementation instance = null;

  /**
   * wrapped configuration
   */
  private org.apache.commons.configuration.Configuration config = null;

  private org.apache.commons.configuration.CombinedConfiguration globalConfig = null;
  private org.apache.commons.configuration.CombinedConfiguration localConfig = null;
  private org.apache.commons.configuration.AbstractConfiguration applicationConfig = null;
  private org.apache.commons.configuration.CombinedConfiguration optionalConfig = null;
  private org.apache.commons.configuration.CombinedConfiguration packageConfig = null;
  private org.apache.commons.configuration.AbstractConfiguration platformConfig = null;

  /**
   * Marker class for loading resources
   */
  private final Class<?> markerClass;

  // //////////////////////////////////////////////////////////////////////////////////////

  public static ConfigurationImplementation instance() {
    if (instance == null)
      instantiate();
    assert instance != null : "Violated Invariance";
    return instance;
  }

  private synchronized static void instantiate() { // TODO doubled checked
                                                   // singleton are not safe.
    if (instance == null)
      instance = new ConfigurationImplementation(ConfigurationImplementation.class);
  }

  private ConfigurationImplementation(final Class<?> markerClass) {
    this.markerClass = markerClass;
    load();
  }

  // private ConfigurationImplementation() {
  //
  // }

  public static boolean isTestrun() {
    return isTestrun;
  }

  // /////////////////////////////////////////////////////////////////////////////////////

  private URL getResource(final String name) {
    return markerClass.getResource(name);
  }

  private static String getMainclassName() {
    final Map<Thread, StackTraceElement[]> traces = Thread.getAllStackTraces();
    for (final Map.Entry<Thread, StackTraceElement[]> entry : traces.entrySet())
      if (entry.getKey().getName().equals("main"))
        return entry.getValue()[entry.getValue().length - 1].getClassName();
    assert false : "Violated Invariance.";
    return null;
  }

  private static boolean isTestrunCheck() {
    for (final String unitTestMainClassSubstring : unitTestMainClassSubstrings)
      if (mainClassName.contains(unitTestMainClassSubstring))
        return true;
    return false;
  }

  private static boolean containsWhitespaceOnly(final String s) {
    for (final char c : s.toCharArray())
      if (!Character.isWhitespace(c))
        return false;
    return true;
  }

  private static boolean containsWhitespaceOnly(final String[] ss) {
    for (final String s : ss)
      if (!containsWhitespaceOnly(s))
        return false;
    return true;
  }

  private void logOverlap(final AbstractConfiguration config1, final String configName1,
      final AbstractConfiguration config2, final String configName2, final String message) {
    if ((config1 == null) || (config2 == null))
      return;
    @SuppressWarnings("unchecked")
    final Iterator<String> keys = config1.getKeys();
    while (keys.hasNext()) {
      final String key = keys.next();
      // this key is always present... so we would report a lot of
      // pseudo-overlap
      if (key.equals("[@xml:space]"))
        continue;
      if (config2.containsKey(key)) {
        if (containsWhitespaceOnly(config1.getStringArray(key)) && containsWhitespaceOnly(config2.getStringArray(key)))
          continue;
        logger.info("Configurations '{}' and '{}' have an overlap for key '" + key + "': " + message, configName1,
            configName2);
      }
    }
  }

  private CombinedConfiguration loadLocalConfiguration() {
    final CombinedConfiguration result = new CombinedConfiguration(new OverrideCombiner());
    AbstractConfiguration defaultConfig = null;
    AbstractConfiguration testConfig = null;

    // Local Test Configuration
    if (isTestrun)
      if (new File(LOCAL_TEST_FILE_NAME).exists()) {
        logger.info("Reading local configuration file '{}'...", LOCAL_TEST_FILE_NAME);
        try {
          result.addConfiguration(testConfig = new XMLConfiguration(LOCAL_TEST_FILE_NAME));
        } catch (final Exception e) {
          throw new InvalidFileConfigurationRuntimeException("Failed loading configuration '" + LOCAL_TEST_FILE_NAME
              + "'", e, logger);
        }
        logger.info("finished reading configuration file.");
      } else
        logger.info("No local configuration file '{}'.", LOCAL_TEST_FILE_NAME);

    // Local Default Configuration
    if (new File(LOCAL_DEFAULT_FILE_NAME).exists()) {
      logger.info("Reading local configuration file '{}'...", LOCAL_DEFAULT_FILE_NAME);
      try {
        result.addConfiguration(defaultConfig = new XMLConfiguration(LOCAL_DEFAULT_FILE_NAME));
      } catch (final Exception e) {
        throw new InvalidFileConfigurationRuntimeException("Failed loading configuration '" + LOCAL_DEFAULT_FILE_NAME
            + "'", e, logger);
      }
      logger.info("finished reading configuration file.");
    } else
      logger.info("No local configuration file '{}'.", LOCAL_DEFAULT_FILE_NAME);

    if (isTestrun)
      logOverlap(testConfig, LOCAL_TEST_FILE_NAME, defaultConfig, LOCAL_DEFAULT_FILE_NAME,
          "The test configuration overrides.");

    return result;
  }

  private CombinedConfiguration loadDefaultTestConfiguration(String basePath, final boolean optionalDefault,
      final boolean optionalTest) {
    assert basePath != null;

    // if the path is non-empty but does not end in "/" add "/"
    if (!(basePath.isEmpty() || basePath.endsWith("/")))
      basePath = basePath + "/";

    AbstractConfiguration testConfig = null;
    AbstractConfiguration defaultConfig = null;
    final CombinedConfiguration result = new CombinedConfiguration(new OverrideCombiner());

    // Global Test Configuration
    final String testConfigPath = basePath + GLOBAL_TEST_RESOURCE_NAME;

    if (isTestrun) {
      final URL configResource = getResource(testConfigPath);
      if (configResource != null) {
        logger.info("Reading test configuration file '{}'...", testConfigPath);
        try {
          result.addConfiguration(testConfig = new XMLConfiguration(configResource));
        } catch (final Exception e) {
          throw new InvalidFileConfigurationRuntimeException("Failed loading configuration '" + testConfigPath + "'",
              e, logger);
        }
        logger.info("finished reading configuration file.");
      } else if (optionalTest)
        logger.info("No test configuration file '{}'...", testConfigPath);
      else
        throw new InvalidFileConfigurationRuntimeException("Failed loading mandatory test configuration '"
            + testConfigPath, logger);
    }
    // Global Default Configuration
    final String defaultConfigPath = basePath + GLOBAL_DEFAULT_RESOURCE_NAME;
    final URL configResource = getResource(defaultConfigPath);
    if (configResource != null) {
      logger.info("Reading default configuration file '{}'...", defaultConfigPath);
      try {
        result.addConfiguration(defaultConfig = new XMLConfiguration(getResource(defaultConfigPath)));
      } catch (final Exception e) {
        throw new InvalidFileConfigurationRuntimeException("Failed loading configuration '" + defaultConfigPath + "'",
            e, logger);
      }
      logger.info("finished reading configuration file.");
    } else if (optionalDefault)
      logger.info("No default configuration file '{}'...", defaultConfigPath);
    else
      throw new InvalidFileConfigurationRuntimeException("Failed loading mandatory default configuration '"
          + defaultConfigPath, logger);

    if (isTestrun)
      logOverlap(testConfig, testConfigPath, defaultConfig, defaultConfigPath, "The test configuration overrides.");

    return result;
  }

  private CombinedConfiguration loadGlobalConfiguration() {
    return loadDefaultTestConfiguration("", false, false);
  }

  private AbstractConfiguration loadPlatformConfiguration(
      final org.apache.commons.configuration.Configuration baseConfig) {
    final int numPlatforms = baseConfig.getStringArray(PLATFORM_KEY).length;
    String fileName = null;
    final String platformChoice = Platform.osType().name();
    final Set<String> seenPlatforms = new HashSet<String>();
    for (int i = 0; i < numPlatforms; ++i) {
      final String individualOptionalKey = PLATFORM_KEY + "(" + i + ")";
      final String platformName = baseConfig.getString(individualOptionalKey + PLATFORM_NAME_KEY);
      if (seenPlatforms.contains(platformName))
        throw new InvalidFileConfigurationRuntimeException("Platform '" + platformName + "' exists twice.'", logger);
      seenPlatforms.add(platformName);

      final String platformFile = baseConfig.getString(individualOptionalKey + PLATFORM_FILE_KEY);
      logger.info("Platform '{}' has configuration file '{}'", platformName, platformFile);
      if (platformName.equals(platformChoice))
        fileName = platformFile;
    }

    XMLConfiguration result;
    if (fileName == null)
      throw new ConfigurationRuntimeException("Platform '" + platformChoice + "' ist not supported.", logger);
    else {
      logger.info("Reading platform configuration file '{}'...", fileName);
      try {
        result = new XMLConfiguration(getResource(fileName));
      } catch (final Exception e) {
        throw new InvalidFileConfigurationRuntimeException("Failed loading configuration '" + fileName
            + "' for platform '" + platformChoice + "'.", e, logger);
      }
      logger.info("finished reading configuration file.");
    }
    return result;
  }

  private CombinedConfiguration loadPackageConfiguration(final org.apache.commons.configuration.Configuration baseConfig) {
    assert baseConfig != null;
    final CombinedConfiguration result = new CombinedConfiguration(new UnionCombiner());
    final int numPackages = baseConfig.getStringArray(PACKAGE_KEY).length;
    final Set<String> seenPackageNames = new HashSet<String>();
    for (int i = 0; i < numPackages; ++i) {
      final String individualPackageKey = PACKAGE_KEY + "(" + i + ")";
      final String packageName = baseConfig.getString(individualPackageKey);
      if (seenPackageNames.contains(packageName))
        throw new InvalidFileConfigurationRuntimeException("Package '" + packageName + "' exists twice.", logger);
      seenPackageNames.add(packageName);
      final AbstractConfiguration newPackageConfig = loadDefaultTestConfiguration(packageName, true, true);
      logOverlap(result, "Collected package configuration so far", newPackageConfig, "Package configuration for "
          + packageName, "Union is taken.");
      result.addConfiguration(newPackageConfig);
    }
    return result;
  }

  private Map<String, String> collectOptionals(final org.apache.commons.configuration.Configuration baseConfig) {
    assert baseConfig != null;

    logger
        .info("Collection optionals from global, plattform, and package configuration (NOTE: optionals NEVER override each other!)...");

    final Map<String, String> result = new HashMap<String, String>();
    final int numOptionals = baseConfig.getStringArray(OPTIONAL_KEY).length;
    final Set<String> seenNames = new HashSet<String>();
    for (int i = 0; i < numOptionals; ++i) {
      final String individualOptionalKey = OPTIONAL_KEY + "(" + i + ")";
      final String optionalName = baseConfig.getString(individualOptionalKey + OPTIONAL_NAME_KEY);
      if (seenNames.contains(optionalName))
        throw new InvalidFileConfigurationRuntimeException("Optional '" + optionalName + "' exists twice.", logger);
      seenNames.add(optionalName);
      final String optionalDefault = baseConfig.getString(individualOptionalKey + OPTIONAL_DEFAULT_KEY);
      final String optionalChoice = getOptionalChoice(optionalName, optionalDefault);
      logger.info("Optional configuration slot '{}' defaults to '{}' and is set to '" + optionalChoice + "'.",
          optionalName, optionalDefault);

      final String compoundAlternativeKey = individualOptionalKey + OPTIONAL_ALTERNATIVE_KEY;
      final int numAlternatives = baseConfig.getStringArray(compoundAlternativeKey).length;
      String fileName = null;
      final Set<String> seenAlternatives = new HashSet<String>();
      for (int j = 0; j < numAlternatives; ++j) {
        final String individualAlternativeKey = compoundAlternativeKey + "(" + j + ")";
        final String alternativeName = baseConfig.getString(individualAlternativeKey + OPTIONAL_NAME_KEY);
        if (seenAlternatives.contains(alternativeName))
          throw new InvalidFileConfigurationRuntimeException("Optional '" + optionalName + "' contains alternative '"
              + alternativeName + "' twice.", logger);
        seenAlternatives.add(alternativeName);
        final String alternativeFile = baseConfig.getString(individualAlternativeKey + OPTIONAL_FILE_KEY);
        logger.info("Optional configuration slot '{}' has option '{}' with file '" + alternativeFile + "'.",
            optionalName, alternativeName);
        if (alternativeName.equals(optionalChoice))
          fileName = alternativeFile;
      }

      if (fileName == null)
        logger.error("Optional configuratoin slot '{}' has no optional setting '{}'", optionalName, optionalChoice);
      else
        result.put(optionalName, fileName);
    }

    logger.info("finished collecting optionals from global, plattform, and package configurations.");

    return result;
  }

  private CombinedConfiguration loadOptionalConfiguration(final Map<String, String> optionalMap, String basePath,
      final boolean optional) {
    assert optionalMap != null;
    assert basePath != null;

    // if the path is non-empty but does not end in "/" add "/"
    if (!(basePath.isEmpty() || basePath.endsWith("/")))
      basePath = basePath + "/";

    final CombinedConfiguration result = new CombinedConfiguration(new UnionCombiner());

    for (final Map.Entry<String, String> optionalEntry : optionalMap.entrySet()) {
      final String filePath = basePath + optionalEntry.getValue();
      final String optionalName = optionalEntry.getKey();
      final URL configPath = getResource(filePath);
      if (configPath != null) {
        logger.info("Reading optional configuration file '{}' ...", filePath);
        try {
          result.addConfiguration(new XMLConfiguration(configPath));
        } catch (final Exception e) {
          throw new InvalidFileConfigurationRuntimeException("Failed loading configuration '" + filePath
              + "' for configuration slot '" + optionalName + "'.", e, logger);
        }
        logger.info("finished reading configuration file.");
      } else if (optional)
        logger.info("No configuration '{}' for configuration slot '{}'.", filePath, optionalName);
      else
        throw new InvalidFileConfigurationRuntimeException("No configuration '" + filePath
            + "' for configuration slot '" + optionalName + "'.", logger);

    }
    return result;
  }

  private CombinedConfiguration loadOptionalConfiguration(
      final org.apache.commons.configuration.Configuration baseConfig) {
    // return loadOptionalConfiguration(baseConfig, "", true);

    final CombinedConfiguration result = new CombinedConfiguration(new UnionCombiner());
    final Map<String, String> optionalMap = collectOptionals(baseConfig);

    result.addConfiguration(loadOptionalConfiguration(optionalMap, "", true));
    final int numPackages = baseConfig.getStringArray(PACKAGE_KEY).length;
    final Set<String> seenPackageNames = new HashSet<String>();
    for (int i = 0; i < numPackages; ++i) {
      final String individualPackageKey = PACKAGE_KEY + "(" + i + ")";
      final String packageName = baseConfig.getString(individualPackageKey);
      if (seenPackageNames.contains(packageName))
        throw new InvalidFileConfigurationRuntimeException("Package '" + packageName + "' exists twice.", logger);
      seenPackageNames.add(packageName);
      final AbstractConfiguration newPackageConfig = loadOptionalConfiguration(optionalMap, packageName, true);
      logOverlap(result, "Collected optional configurations so far", newPackageConfig, "Optional configuration for "
          + packageName, "Union is taken.");
      result.addConfiguration(newPackageConfig);
    }
    return result;
  }

  private void load() {
    // We want to avoid overriding existing (maybe externally provided)
    // logging configurations, so we check before setting our initial.
    final boolean existsExternalLogging = overrideLoggingConfIfNotSetYet(initialLog4jResourceName);

    logger.info("The current main class is '{}', isTestRun={}.", mainClassName, isTestrun);
    logger.info("The system type is identified as '{}'.", Platform.osType().name());

    globalConfig = loadGlobalConfiguration();
    platformConfig = loadPlatformConfiguration(globalConfig);
    packageConfig = loadPackageConfiguration(globalConfig);
    // here we use a UnionCombiner to get the optionals -- to collect all
    // optionals. NOTE that these configurations are
    // later combined via an OverrideCombinder
    final CombinedConfiguration cc = new CombinedConfiguration(new UnionCombiner());
    cc.addConfiguration(packageConfig);
    cc.addConfiguration(platformConfig);
    cc.addConfiguration(globalConfig);
    optionalConfig = loadOptionalConfiguration(cc);
    applicationConfig = null;
    localConfig = loadLocalConfiguration();

    // Check that the configuration contains no conflicts
    /*
     * @SuppressWarnings("unchecked") Iterator<String> keys=config.getKeys(); while(keys.hasNext()) { String
     * key=keys.next(); if(config.getStringArray(key).length!=1) { throw new
     * AmbiguousPropertyConfigurationRuntimeException ("Key '"+key+"' multiple defined.", logger); } }
     */
    // TODO We need a check against overlapping configurations (or not)

    // Putting the configuration in place
    buildConfiguration();

    // If we did set our initial logging successfully, it means there was no
    // external configuration. Therefore we can load a local configuration
    // if any.
    if (!existsExternalLogging)
      loadLoggingConfiguration();
  }

  private boolean overrideLoggingConfIfNotSetYet(final String log4jConfigFile) {
    final Enumeration<?> allAppenders = org.apache.log4j.Logger.getRootLogger().getAllAppenders();
    // check if some appenders are already defined
    if (!(allAppenders instanceof NullEnumeration))
      return true;
    try {
      if (getResource(log4jConfigFile) != null)
        PropertyConfigurator.configure(getResource(log4jConfigFile));
      else
        PropertyConfigurator.configure(log4jConfigFile);
      return false;
    } catch (final Exception e) {
      throw new InvalidFileConfigurationRuntimeException("Failed loading logging configuration '" + log4jConfigFile
          + "'", e, logger);
    }
  }

  private void loadLoggingConfiguration() {
    final String log4jConfigFile = config.getString(log4jConfigFileConfigurationProperty);
    try {
      if (getResource(log4jConfigFile) != null)
        PropertyConfigurator.configure(getResource(log4jConfigFile));
      else
        PropertyConfigurator.configure(log4jConfigFile);
    } catch (final Exception e) {
      throw new InvalidFileConfigurationRuntimeException("Failed loading local logging configuration '"
          + log4jConfigFile + "'", e, logger);
    }
  }

  // ////////////////////////////////////////////////////////////////////////////////////////////////////////////

  private String getOptionalChoice(final String optionalName, final String optionalDefault) {
    assert optionalName != null;
    assert optionalDefault != null;
    final String setProperty = System.getProperty(optionalName);
    if (setProperty == null)
      return optionalDefault;
    return setProperty;
  }

  public void setApplicationConfiguration(final String resourceName) {
    assert resourceName != null;

    logger.info("Reading application configuration file '{}'...", resourceName);
    if (getResource(resourceName) == null)
      throw new InvalidFileConfigurationRuntimeException("Application configuration '" + resourceName
          + "' does not exist.", logger);
    setApplicationConfiguration(getResource(resourceName));
  }

  public synchronized void setApplicationConfiguration(final URL input) {
    assert input != null;

    final String oldLog4jConfigFile = config.getString(log4jConfigFileConfigurationProperty);
    try {
      applicationConfig = new XMLConfiguration(input);
    } catch (final Exception e) {
      throw new InvalidFileConfigurationRuntimeException("Failed loading application configuration '"
          + input.toString() + "'", e, logger);
    }
    logger.info("finished reading application configuration file.");

    buildConfiguration();
    logger.info("added '{}' as application configuration to the configuration.", input.toString());

    final String newLog4jConfigFile = config.getString(log4jConfigFileConfigurationProperty);
    if (!oldLog4jConfigFile.equals(newLog4jConfigFile))
      loadLoggingConfiguration();
  }

  public synchronized void resetApplicationConfiguration() {
    final String oldLog4jConfigFile = config.getString(log4jConfigFileConfigurationProperty);

    applicationConfig = null;
    buildConfiguration();
    logger.info("reset application configuration.");

    final String newLog4jConfigFile = config.getString(log4jConfigFileConfigurationProperty);
    if (!oldLog4jConfigFile.equals(newLog4jConfigFile))
      loadLoggingConfiguration();
  }

  private void logOverlap() {
    final AbstractConfiguration[] configs = new AbstractConfiguration[] { globalConfig, platformConfig, packageConfig,
        optionalConfig, applicationConfig, localConfig };
    final String[] configNames = new String[] { "global", "platform", "package", "optional", "application", "local" };
    assert configs.length == configNames.length;

    for (int i = 0; i < configs.length; ++i)
      for (int j = i + 1; j < configs.length; ++j)
        logOverlap(configs[i], configNames[i] + " configuration", configs[j], configNames[j] + " configuration",
            configNames[j] + " wins");
  }

  private void buildConfiguration() {
    logOverlap(); // TODO be conscious that the list of configurations
                  // reoccurs ins logOverlap.
    final CombinedConfiguration cc = new CombinedConfiguration(new OverrideCombiner());
    if (localConfig != null)
      cc.addConfiguration(localConfig);
    if (applicationConfig != null)
      cc.addConfiguration(applicationConfig);
    cc.addConfiguration(optionalConfig);
    cc.addConfiguration(packageConfig);
    cc.addConfiguration(platformConfig);
    cc.addConfiguration(globalConfig);
    config = cc;
  }

  // /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  public String toString() {
    return toString("");
  }

  @Override
  public String toString(final String prefix) {
    assert prefix != null;
    final StringBuilder sb = new StringBuilder();
    final Iterator<String> keys = getKeys(prefix);
    while (keys.hasNext()) {
      final String key = keys.next();
      sb.append(key);
      sb.append(": ");
      final char[] filler = new char[key.length() + 2];
      for (int i = 0; i < filler.length; filler[i] = ' ', ++i)
        ;
      filler[key.length()] = '|';

      final String[] values = getStringArray(key);
      if (values.length > 0) {
        sb.append(values[0]);
        sb.append('\n');
        for (int i = 1; i < values.length; ++i) {
          sb.append(filler);
          sb.append(values[1]);
          sb.append('\n');
        }
      }
    }
    return sb.toString();
  }

  // ////////////////////////////////////////////////////////////////////////////////////////////////////////////

  @Override
  public boolean containsKey(final String key) {
    assert key != null;
    return config.containsKey(key);
  }

  @Override
  public BigDecimal getBigDecimal(final String key) {
    assert key != null;
    if (!config.containsKey(key))
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", logger);
    return config.getBigDecimal(key);
  }

  @Override
  public BigDecimal getBigDecimal(final String key, final BigDecimal defaultValue) {
    assert key != null;
    return config.getBigDecimal(key, defaultValue);
  }

  @Override
  public BigInteger getBigInteger(final String key) {
    assert key != null;
    if (!config.containsKey(key))
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", logger);
    return config.getBigInteger(key);
  }

  @Override
  public BigInteger getBigInteger(final String key, final BigInteger defaultValue) {
    assert key != null;
    return config.getBigInteger(key, defaultValue);
  }

  @Override
  public boolean getBoolean(final String key) {
    assert key != null;
    if (!config.containsKey(key))
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", logger);
    return config.getBoolean(key);
  }

  @Override
  public boolean getBoolean(final String key, final boolean defaultValue) {
    assert key != null;
    return config.getBoolean(key, defaultValue);
  }

  @Override
  public Boolean getBoolean(final String key, final Boolean defaultValue) {
    assert key != null;
    return config.getBoolean(key, defaultValue);
  }

  @Override
  public byte getByte(final String key) {
    assert key != null;
    if (!config.containsKey(key))
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", logger);
    return config.getByte(key);
  }

  @Override
  public byte getByte(final String key, final byte defaultValue) {
    assert key != null;
    return config.getByte(key, defaultValue);
  }

  @Override
  public Byte getByte(final String key, final Byte defaultValue) {
    assert key != null;
    return config.getByte(key, defaultValue);
  }

  @Override
  public double getDouble(final String key) {
    assert key != null;
    if (!config.containsKey(key))
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", logger);
    return config.getDouble(key);
  }

  @Override
  public double getDouble(final String key, final double defaultValue) {
    assert key != null;
    return config.getDouble(key, defaultValue);
  }

  @Override
  public Double getDouble(final String key, final Double defaultValue) {
    assert key != null;
    return config.getDouble(key, defaultValue);
  }

  @Override
  public float getFloat(final String key) {
    assert key != null;
    if (!config.containsKey(key))
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", logger);
    return config.getFloat(key);
  }

  @Override
  public float getFloat(final String key, final float defaultValue) {
    assert key != null;
    return config.getFloat(key, defaultValue);
  }

  @Override
  public Float getFloat(final String key, final Float defaultValue) {
    assert key != null;
    return config.getFloat(key, defaultValue);
  }

  @Override
  public int getInt(final String key) {
    assert key != null;
    if (!config.containsKey(key))
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", logger);
    return config.getInt(key);
  }

  @Override
  public int getInt(final String key, final int defaultValue) {
    assert key != null;
    return config.getInt(key, defaultValue);
  }

  @Override
  public Integer getInteger(final String key, final Integer defaultValue) {
    assert key != null;
    return config.getInteger(key, defaultValue);
  }

  @SuppressWarnings("unchecked")
  @Override
  public IterableIterator<String> getKeys() {
    return WrappingIterableIterator.create(config.getKeys());
  }

  @SuppressWarnings("unchecked")
  @Override
  public IterableIterator<String> getKeys(final String prefix) {
    return WrappingIterableIterator.create(config.getKeys(prefix));
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<String> getList(final String key) {
    assert key != null;
    return config.getList(key);
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<String> getList(final String key, final List<String> defaultValue) {
    assert key != null;
    return config.getList(key, defaultValue);
  }

  @Override
  public long getLong(final String key) {
    assert key != null;
    if (!config.containsKey(key))
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", logger);
    return config.getLong(key);
  }

  @Override
  public long getLong(final String key, final long defaultValue) {
    assert key != null;
    return config.getLong(key, defaultValue);
  }

  @Override
  public Long getLong(final String key, final Long defaultValue) {
    assert key != null;
    return config.getLong(key, defaultValue);
  }

  @Override
  public short getShort(final String key) {
    assert key != null;
    if (!config.containsKey(key))
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", logger);
    return config.getShort(key);
  }

  @Override
  public short getShort(final String key, final short defaultValue) {
    assert key != null;
    return config.getShort(key, defaultValue);
  }

  @Override
  public Short getShort(final String key, final Short defaultValue) {
    assert key != null;
    return config.getShort(key, defaultValue);
  }

  @Override
  public String getString(final String key) {
    assert key != null;
    if (!config.containsKey(key))
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", logger);
    return StringUtils.join(config.getStringArray(key), LIST_DELIMITER);
  }

  @Override
  public String getString(final String key, final String defaultValue) {
    assert key != null;
    if (!config.containsKey(key))
      return defaultValue;
    return getString(key);
  }

  @Override
  public String[] getStringArray(final String key) {
    return config.getStringArray(key);
  }

  @Override
  public boolean isEmpty() {
    return config.isEmpty();
  }

  @SuppressWarnings({ "unchecked", "static-access" })
  @Override
  public <E extends Enum<E>> E getEnum(final String key, final E defaultValue) {
    assert key != null;
    final String valueName = getString(key, null);
    if (valueName == null)
      return defaultValue;
    try {
      return (E) defaultValue.valueOf(defaultValue.getClass(), valueName);
    } catch (final IllegalArgumentException e) {
      throw new MalformedPropertyConfigurationRuntimeException("Got '" + valueName + "' for property '" + key
          + "' of type '" + defaultValue.getClass().toString() + "'.", logger);
    }
  }

  @Override
  public <E extends Enum<E>> E getEnumNoDefault(final String key, final E prototype) {
    assert key != null;
    if (!containsKey(key))
      throw new MissingPropertyConfigurationRuntimeException("Configuration Property '" + key + "' not found.", logger);
    return getEnum(key, prototype);
  }

  public void addOptionalConfiguration(final String resourceName) {
    assert resourceName != null;

    logger.info("Reading application configuration file '{}'...", resourceName);
    if (ClassLoader.getSystemResource(resourceName) == null)
      throw new InvalidFileConfigurationRuntimeException("Optional configuration '" + resourceName
          + "' does not exist.", logger);
    addOptionalConfiguration(getResource(resourceName));

  }

  public void addOptionalConfiguration(final URL resource) {
    assert resource != null;

    // final String oldLog4jConfigFile =
    // config.getString(log4jConfigFileConfigurationProperty);
    try {
      optionalConfig.addConfiguration(new XMLConfiguration(resource));
    } catch (final Exception e) {
      throw new InvalidFileConfigurationRuntimeException("Failed loading optional configuration '"
          + resource.toString() + "'", e, logger);
    }
    logger.info("finished reading optional configuration file.");

    buildConfiguration();
    logger.info("added '{}' as optional configuration to the configuration.", resource.toString());
  }

  @Override
  public Configuration getSubConfiguration(String prefix) {
    // TODO Auto-generated method stub
    return null;
  }
}
