package uk.ac.ox.cs.diadem.env.configuration;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.util.collect.IterableIterator;
import uk.ac.ox.cs.diadem.util.collect.WrappingIterableIterator;

import com.google.common.base.Predicate;

/**
 * A SubConfiguration filters a given Configuration with a given prefix.
 * 
 * NOTE: The prefix must include the terminal dot (.), if you want to obtain a subconfiguration for a subtree of the
 * original configuration. You may also use arbitrary prefixes, e.g., "some" to include "something.XYZ" and
 * "somethingelse.XYZ".
 * 
 * @author Christian Schallhart <christian@schallhart.net>
 * 
 */
class SubConfiguration extends StdConfiguration implements Configuration {

  private static final Logger LOGGER = LoggerFactory.getLogger(SubConfiguration.class);
  private final Configuration baseConfig;
  private final String basePrefix;

  public SubConfiguration(final Configuration baseConfig, final String basePrefix) {
    assert baseConfig != null;
    assert basePrefix != null;
    assert basePrefix.endsWith(".");
    this.baseConfig = baseConfig;
    this.basePrefix = basePrefix;
  }

  @Override
  public boolean containsKey(final String key) {
    return baseConfig.containsKey(basePrefix + key);
  }

  @Override
  public boolean isEmpty() {
    return getKeys().hasNext();
  }

  @Override
  public IterableIterator<String> getKeys() {
    final List<String> keys = new LinkedList<String>();
    final Iterator<String> iter = baseConfig.getKeys(basePrefix);
    while (iter.hasNext()) {
      final String key = iter.next().substring(basePrefix.length());
      if (key.isEmpty())
        continue;
      keys.add(key);
    }
    return WrappingIterableIterator.create(keys.iterator());
  }

  @Override
  public IterableIterator<String> getKeys(final String prefix) {
    return WrappingIterableIterator.create(baseConfig.getKeys(basePrefix + prefix));
  }

  @Override
  public String toString(final String prefix) {
    return baseConfig.toString(basePrefix + prefix);
  }

  @Override
  public String[] getStringArray(final String key) {
    return baseConfig.getStringArray(basePrefix + key);
  }

  @Override
  public boolean getBoolean(final String key) {
    return baseConfig.getBoolean(basePrefix + key);
  }

  @Override
  public boolean getBoolean(final String key, final boolean defaultValue) {
    return baseConfig.getBoolean(basePrefix + key, defaultValue);
  }

  @Override
  public Boolean getBoolean(final String key, final Boolean defaultValue) {
    return baseConfig.getBoolean(basePrefix + key, defaultValue);
  }

  @Override
  public BigDecimal getBigDecimal(final String key) {
    return baseConfig.getBigDecimal(basePrefix + key);
  }

  @Override
  public BigDecimal getBigDecimal(final String key, final BigDecimal defaultValue) {
    return baseConfig.getBigDecimal(basePrefix + key, defaultValue);
  }

  @Override
  public BigInteger getBigInteger(final String key) {
    return baseConfig.getBigInteger(basePrefix + key);
  }

  @Override
  public BigInteger getBigInteger(final String key, final BigInteger defaultValue) {
    return baseConfig.getBigInteger(basePrefix + key, defaultValue);
  }

  @Override
  public byte getByte(final String key) {
    return baseConfig.getByte(basePrefix + key);
  }

  @Override
  public byte getByte(final String key, final byte defaultValue) {
    return baseConfig.getByte(basePrefix + key, defaultValue);
  }

  @Override
  public Byte getByte(final String key, final Byte defaultValue) {
    return baseConfig.getByte(basePrefix + key, defaultValue);
  }

  @Override
  public double getDouble(final String key) {
    return baseConfig.getDouble(basePrefix + key);
  }

  @Override
  public double getDouble(final String key, final double defaultValue) {
    return baseConfig.getDouble(basePrefix + key, defaultValue);
  }

  @Override
  public Double getDouble(final String key, final Double defaultValue) {
    return baseConfig.getDouble(basePrefix + key, defaultValue);
  }

  @Override
  public float getFloat(final String key) {
    return baseConfig.getFloat(basePrefix + key);
  }

  @Override
  public float getFloat(final String key, final float defaultValue) {
    return baseConfig.getFloat(basePrefix + key, defaultValue);
  }

  @Override
  public Float getFloat(final String key, final Float defaultValue) {
    return baseConfig.getFloat(basePrefix + key, defaultValue);
  }

  @Override
  public int getInt(final String key) {
    return baseConfig.getInt(basePrefix + key);
  }

  @Override
  public int getInt(final String key, final int defaultValue) {
    return baseConfig.getInt(basePrefix + key, defaultValue);
  }

  @Override
  public Integer getInteger(final String key, final Integer defaultValue) {
    return baseConfig.getInteger(basePrefix + key, defaultValue);
  }

  @Override
  public List<String> getList(final String key) {
    return baseConfig.getList(basePrefix + key);
  }

  @Override
  public List<String> getList(final String key, final List<String> defaultValue) {
    return baseConfig.getList(basePrefix + key, defaultValue);
  }

  @Override
  public long getLong(final String key) {
    return baseConfig.getLong(basePrefix + key);
  }

  @Override
  public long getLong(final String key, final long defaultValue) {
    return baseConfig.getLong(basePrefix + key, defaultValue);
  }

  @Override
  public Long getLong(final String key, final Long defaultValue) {
    return baseConfig.getLong(basePrefix + key, defaultValue);
  }

  @Override
  public short getShort(final String key) {
    return baseConfig.getShort(basePrefix + key);
  }

  @Override
  public short getShort(final String key, final short defaultValue) {
    return baseConfig.getShort(basePrefix + key, defaultValue);
  }

  @Override
  public Short getShort(final String key, final Short defaultValue) {
    return baseConfig.getShort(basePrefix + key, defaultValue);
  }

  @Override
  public String getString(final String key) {
    return baseConfig.getString(basePrefix + key);
  }

  @Override
  public String getString(final String key, final String defaultValue) {
    return baseConfig.getString(basePrefix + key, defaultValue);
  }

  @Override
  public <E extends Enum<E>> E getEnum(final String key, final E defaultValue) {
    return baseConfig.getEnum(basePrefix + key, defaultValue);
  }

  @Override
  public <E extends Enum<E>> E getEnumNoDefault(final String key, final E prototype) {
    return baseConfig.getEnumNoDefault(basePrefix + key, prototype);
  }

  @Override
  public Configuration getSubConfiguration(final String prefix) {
    assert prefix != null;
    assert !prefix.endsWith(".");
    if (!hasSubConfiguration(prefix))
      throw new MissingPropertyConfigurationRuntimeException("Subconfiguration '" + prefix + "' does not exist. ",
          LOGGER);
    return new SubConfiguration(baseConfig, basePrefix + prefix + ".");
  }

  @Override
  public IterableIterator<String> getChildKeys() {
    return WrappingIterableIterator.create(getKeys(), new Predicate<String>() {
      @Override
      public boolean apply(final String input) {
        return !input.contains(".");
      };
    });
  }
}
