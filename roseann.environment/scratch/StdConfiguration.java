package uk.ac.ox.cs.diadem.env.configuration;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation carrier for Root and SubConfiguration -- those classes should be merged again.
 * 
 * @author Christian Schallhart <christian@schallhart.net>
 * 
 */
abstract class StdConfiguration implements Configuration {

  static Logger LOGGER = LoggerFactory.getLogger(StdConfiguration.class);

  @Override
  public String toString() {
    return toString("");
  }

  @Override
  public Configuration[] getSubConfigurationArray(final String prefix) {
    assert prefix != null;
    assert !prefix.endsWith(".");
    if (!containsKey(prefix))
      throw new MissingPropertyConfigurationRuntimeException("Subconfiguration '" + prefix + "' does not exist. ",
          LOGGER);
    final int numSubConfigs = getStringArray(prefix).length;
    final Configuration[] result = new Configuration[numSubConfigs];
    for (int i = 0; i < numSubConfigs; ++i)
      result[i] = getSubConfiguration(prefix + "(" + i + ")");
    return result;
  }

  @Override
  public Configuration[] splitIntoArrays() {
    final ArrayList<Configuration> resultList = new ArrayList<Configuration>();
    for (final String childKey : getChildKeys())
      resultList.add(getSubConfiguration(childKey));
    final Configuration[] result = new Configuration[resultList.size()];
    for (int i = 0; i < resultList.size(); ++i)
      result[i] = resultList.get(i);
    return result;
  }

  @Override
  public boolean hasSubConfiguration(final String prefix) {
    assert prefix != null;
    assert !prefix.endsWith(".");
    final String extendedPrefix = prefix + ".";
    for (final String key : getKeys())
      if (key.startsWith(extendedPrefix))
        return true;
    return false;
  }
}