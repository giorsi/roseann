<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright [2016] [University of Oxford, University of Birmingham] Licensed 
	under the Apache License, Version 2.0 (the "License"); you may not use this 
	file except in compliance with the License. You may obtain a copy of the 
	License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by 
	applicable law or agreed to in writing, software distributed under the License 
	is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY 
	KIND, either express or implied. See the License for the specific language 
	governing permissions and limitations under the License. -->
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd"
	xmlns="http://maven.apache.org/POM/4.0.0">

	<modelVersion>4.0.0</modelVersion>

	<!-- Project information -->

	<groupId>org.roseann</groupId>
	<artifactId>parent</artifactId>
	<version>1.0</version>
	<packaging>pom</packaging>

	<modules>
		<module>../roseann.aggregation</module>
		<module>../roseann.api</module>
		<module>../roseann.annotator.api</module>
		<module>../roseann.webapi</module>
		<module>../roseann.util</module>
		<module>../roseann.webdriver_env</module>
		<module>../roseann.environment</module>
		<module>../roseann.domFactsGenerator</module>
		<module>../roseann.model</module>
		<module>../roseann.annotator.alchemyapi</module>
		<module>../roseann.annotator.extractiv</module>
		<module>../roseann.annotator.fox</module>
		<module>../roseann.annotator.illinoisner</module>
		<module>../roseann.annotator.lupedia</module>
		<module>../roseann.annotator.nerd</module>
		<module>../roseann.annotator.opencalais</module>
		<module>../roseann.annotator.parallel</module>
		<module>../roseann.annotator.saplo</module>
		<module>../roseann.annotator.spotlight</module>
		<module>../roseann.annotator.stanfordner</module>
		<module>../roseann.annotator.wikimeta</module>
		<module>../roseann.annotator.yahoocontentanalyser</module>
		<module>../roseann.annotator.zemanta</module>
		<module>../roseann.pdfanalyser</module>
	</modules>

	<name>parent</name>
	<url>http://diadem.cs.ox.ac.uk/roseann</url>

	<organization>
		<name>Department of Computer Science, University of Oxford</name>
		<url>http://diadem.cs.ox.ac.uk</url>
	</organization>

	<!-- Licensing -->
	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<url>https://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>manual</distribution>
			<comments>A business-friendly OSS license</comments>
		</license>
	</licenses>

	<!-- Developers and contributors -->

	<developers>
		<developer>
			<id>orsig</id>
			<name>Giorgio Orsi</name>
			<email>g.orsi@cs.bham.ac.uk</email>
			<url>http://www.cs.bham.ac.uk/about/people/Giorgio%20Orsi</url>
			<organization>School of Computer Science - University of Birmingham</organization>
			<organizationUrl>http://cs.bham.ac.uk</organizationUrl>
			<roles>
				<role>developer</role>
				<role>researcher</role>
			</roles>
		</developer>

		<developer>
			<id>ortona</id>
			<name>Stefano Ortona</name>
			<email>stefano.ortona@cs.ox.ac.uk</email>
			<url>http://www.cs.ox.ac.uk/people/stefano.ortona</url>
			<organization>DIADEM Lab - Department of Computer Science, University of Oxford</organization>
			<organizationUrl>http://diadem.cs.ox.ac.uk</organizationUrl>
			<roles>
				<role>developer</role>
				<role>researcher</role>
			</roles>
		</developer>

		<developer>
			<id>chen</id>
			<name>Luying Chen</name>
			<email>luying.chen@cs.ox.ac.uk</email>
			<url>http://www.cs.ox.ac.uk/people/luying.chen</url>
			<organization>DIADEM Lab - Department of Computer Science, University of Oxford</organization>
			<organizationUrl>http://diadem.cs.ox.ac.uk</organizationUrl>
			<roles>
				<role>developer</role>
				<role>researcher</role>
			</roles>
		</developer>
	</developers>

	<contributors>
		<contributor>
			<name>Michael Benedikit</name>
			<email>michael.benedikt@cs.ox.ac.uk</email>
			<url>http://www.cs.ox.ac.uk/people/michael.bemedikt</url>
			<organization>DIADEM Lab - Department of Computer Science, University of Oxford</organization>
			<organizationUrl>http://diadem.cs.ox.ac.uk</organizationUrl>
			<roles>
				<role>researcher</role>
			</roles>
		</contributor>
	</contributors>

	<!-- Dependencies -->

	<dependencies>

		<dependency>
			<groupId>org.roseann</groupId>
			<artifactId>environment</artifactId>
			<version>1.0</version>
		</dependency>

		<dependency>
			<groupId>org.roseann</groupId>
			<artifactId>util</artifactId>
			<version>1.0</version>
		</dependency>

		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.12</version>
			<scope>test</scope>
		</dependency>

	</dependencies>

	<!-- Repositories for dependencies -->

	<repositories>

		<repository>
			<id>central</id>
			<name>Maven Repository Switchboard</name>
			<layout>default</layout>
			<url>http://repo1.maven.org/maven2</url>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>

	</repositories>

	<pluginRepositories>

		<pluginRepository>
			<id>central</id>
			<name>Maven Plugin Repository</name>
			<url>http://repo1.maven.org/maven2</url>
			<layout>default</layout>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<releases>
				<updatePolicy>never</updatePolicy>
			</releases>
		</pluginRepository>

	</pluginRepositories>

	<!-- Forcing UTF-8 as default encoding for sources -->
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<!-- Repositories for distribution -->
	<distributionManagement />



	<!-- Build configuration -->

	<build>

		<!-- Plugin management -->
		<pluginManagement>

			<plugins>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-site-plugin</artifactId>
					<dependencies>
						<dependency>
							<groupId>org.apache.maven.wagon</groupId>
							<artifactId>wagon-ssh</artifactId>
							<version>2.10</version>
						</dependency>
					</dependencies>
				</plugin>

				<!--This plugin's configuration is used to store Eclipse m2e settings 
					only. It has no influence on the Maven build itself. -->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>
											org.apache.maven.plugins
										</groupId>
										<artifactId>
											maven-compiler-plugin
										</artifactId>
										<versionRange>[3.5.1,)</versionRange>
										<goals>
											<goal>compile</goal>
											<goal>testCompile</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore />
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
			</plugins>

		</pluginManagement>

		<!-- Plugins used to modify build-lifecycle behaviour -->
		<plugins>

			<!-- Generate and install source, bound to verify phase, it is active 
				in case of install and deploy -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>3.0.1</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<phase>verify</phase>
						<goals>
							<goal>jar-no-fork</goal>
							<goal>test-jar-no-fork</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- Generate javadoc, bound to deploy phase -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>2.10.4</version>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<phase>verify</phase>
						<goals>
							<goal>jar</goal>
						</goals>
						<configuration>
							<additionalparam>-Xdoclint:none</additionalparam>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<!-- Forces the compiler to 1.8 (source and target) -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.5.1</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>

			<plugin>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>2.6</version>
				<configuration>
					<descriptorRefs>
						<descriptorRef>jar-with-dependencies</descriptorRef>
					</descriptorRefs>
				</configuration>
				<executions>
					<execution>
						<id>make-assembly</id> <!-- this is used for inheritance merges -->
						<phase>package</phase> <!-- bind to the packaging phase -->
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
</project>
