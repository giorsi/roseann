

function doPopups() {
  var links = document.getElementsByTagName("a");
  for (var i=0; i < links.length; i++) {
	switch (links[i].className) {
		case "DetailsFloorLink" :
			links[i].onclick = function() {
				window.open(this.getAttribute("href"),'floorplan','width=750,height=450,scrollbars=yes,menubar=no,toolbar=no,resizable=yes');
				return false;
			}
			break;
		case "Vebra360Link" :
			links[i].onclick = function() {
				window.open(this.getAttribute("href"),'vtour','copyhistory=no,toolbar=no,width=550,height=490,scrollbars=no,resizable=no,location=no');
				return false;
			}
			break;
		case "IPIXLink" :
			links[i].onclick = function() {
				window.open(this.getAttribute("href"),'ipix','copyhistory=no,toolbar=no,width=370,height=320,scrollbars=no,resizable=no,location=no');
				return false;
			}
			break;
		case "EhouseLink" :
			links[i].onclick = function() {
				window.open(this.getAttribute("href"),'vtour','copyhistory=no,toolbar=no,width=400,height=330,scrollbars=no,resizable=no,location=no');
				return false;
			}
			break;
		case "ResultsCustomLink" :
			links[i].onclick = function() {
				window.open(this.getAttribute("href"),'custom','copyhistory=no,toolbar=yes,scrollbars=yes,resizable=yes,location=yes');
				return false;
			}
			break;		
		case "ResultsBar" :
			if ( links[i].href.indexOf("branch.asp") > -1 ) {
				links[i].onclick = function() {
					window.open(this.getAttribute("href"),'agent','copyhistory=0,toolbar=0,width=300,height=200,scrollbars=0,resizable=1,location=0');
					return false;
				}
			}
			break;		
		case "Vebra360Solex" :
				links[i].onclick = function() {
					window.open(this.getAttribute("href"),'vtour','width=640,height=460');
					return false;
				}
			break;		
		default :
			 break;		
    }
  }
}

function popup( mylink,windowname, opt ) {
	if (opt == null) {
		var childw = window.open(mylink.href,windowname);
	} else  {
		var childw = window.open(mylink.href,windowname, 'copyhistory=no,toolbar=yes,scrollbars=yes,resizable=yes,location=yes');
	}
	if (childw.focus) childw.focus();
	return false;
} 
