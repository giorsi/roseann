package uk.ac.ox.cs.diadem.domFactsGenerator;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;

public class SpaceTestFactExtraction {

  /**
   * @param args
   * @throws IOException
   * @throws InterruptedException
   */
  public static void main(final String[] args) throws IOException, InterruptedException {

    ConfigurationFacility.getConfiguration();
    final Engine type = Engine.WEBDRIVER_FF;
    final WebBrowser b = BrowserFactory.newWebBrowser(type, true);

    // b.navigate(url);
    final Writer el_writer = new StringWriter();
    final Writer el_writer2 = new StringWriter();
    final Writer el_writer3 = new StringWriter();
    try {
      final String url2 = "http://diadem.cs.ox.ac.uk/test/unit/borderline/spacing-issues.html";
      b.navigate(url2);
      final DOMFactGenerator generator2 = DOMFactGenerator.newDOMFactGenerator(b);
      generator2.factsFor().allElements(new StringWriter()).with().rawDOM(el_writer2).and().text(el_writer)
          .withElementClob(el_writer3);
      generator2.generateFacts("d1");

      System.out.println("---TEXT-------------");
      System.out.println(el_writer);
      System.out.println("---CLOB-------------");
      System.out.println(el_writer3);
      System.out.println("---RAW-------------");
      System.out.println(el_writer2);
      // final Iterable<String> split = Splitter.on("\n").split(string);
      // final TreeSet<String> newTreeSet = Sets.newTreeSet(split);
      // for (final String line : newTreeSet) {
      // System.out.println(line);
      // }

    } finally {
      el_writer.close();
      el_writer2.close();
    }

    // final DOMFactGenerator generator = new DOMFactGenerator(b);
    // final Writer el_writer = new StringWriter();
    // String returnStatement = "";
    // if (type == Engine.SWT_MOZILLA)
    // returnStatement = "return";
    //
    // try {
    //
    // StringBuilder s = new StringBuilder();
    // for (int i = 0; i < 10; i++) {
    // final Stopwatch w = new Stopwatch();
    // generator.factsFor().allElements(el_writer);
    // w.start();
    // generator.generateFacts("d1");
    // w.stop();
    // s.append(w.elapsedMillis() + "ms, ");
    // }
    //
    // System.out.println("WebAPI:" + s.toString());
    //
    // final Object evaluate = b.evaluate("var clob; " + "function walk(n,level){ "
    // + "for (var i = 0; i < n.childNodes.length; i++) { " + "var child = n.childNodes[i];" + "clob+=n.nodeName;"
    // + "walk(child,level+1);}}; " + "function walkTree(){" + "var s = new Date().getMilliseconds(); "
    // + "walk(document.documentElement,0); " + "var e = new Date().getMilliseconds(); " + "return e-s;} "
    // + "var res = new Array();var str='';" + "for(var k=0;k<10;k++){res[k]=walkTree();str+=res[k]+'ms,';} "
    // + returnStatement + " str;");
    // System.out.println("Javascript:" + evaluate);
    //
    // s = new StringBuilder();
    // for (int i = 0; i < 1; i++) {
    // final Stopwatch w = new Stopwatch();
    // generator.factsFor().allElements(el_writer);
    // w.start();
    // generator.generateFacts("d1");
    // w.stop();
    // s.append(w.elapsedMillis() + "ms, ");
    // }
    //
    // System.out.println("WebAPI:" + s.toString());
    // } finally {
    // el_writer.close();
    // }
    b.shutdown();
  }
}
