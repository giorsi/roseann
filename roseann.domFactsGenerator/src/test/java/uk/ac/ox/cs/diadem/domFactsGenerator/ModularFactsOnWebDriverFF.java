// $codepro.audit.disable closeWhereCreated, debuggingCode
/**
 * Header
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import uk.ac.ox.cs.diadem.webapi.dom.DOMCSS2Properties.CssProperty;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;
import uk.ac.ox.cs.diadem.webapi.utils.XPathUtil;

/**
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public class ModularFactsOnWebDriverFF extends AbstractFactGeneratorTest {

  @BeforeClass
  public static void oneTimesetUp() {

    Browser = BrowserFactory.newWebBrowser(Engine.WEBDRIVER_FF, true);
    Browser.manageOptions().configureXPathLocatorHeuristics(true, true);
  }

  @AfterClass
  public static void shutdown() {
    Browser.shutdown();
  }

  @Test
  public void testSameHostLink() throws IOException {
    Browser.navigate("http://diadem.cs.ox.ac.uk/test/re/fast/wwagency/result1.html");
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().onlyElements(elem, "IMG", "A").with().properties(attr);
      generator.generateFacts("d1");
      assertTrue("A properties", database.check("A_properties", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  @Ignore
  public void testVisibleNode() throws IOException {
    Browser.navigate("http://www.struttandparker.com/", true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().onlyElements(attr, "INPUT").with().ids(attr).and().properties(attr).and().css()
      .onlyBoundingBoxes(attr);
      generator.generateFacts("d1");
      assertTrue("js properties", database.check("js properties", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  @Ignore
  public void testBackgroundImage() throws IOException {
    Browser.navigate(
        "http://www.rightmove.co.uk/property-for-sale/find.html?locationIdentifier=REGION^1036&insId=1&viewType=GRID",
        true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().onlyElements(attr, "DIV").with().css().onlyProperties(attr, CssProperty.background_image);
      generator.generateFacts("d1");
      assertTrue("css bgimg", database.check("css bgimg", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void testAllElements_and_Properties() throws IOException {
    Browser.navigate("http://diadem.cs.ox.ac.uk/test/re/fast/wwagency/", true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().properties(attr);
      generator.generateFacts("d1");
      assertTrue("all jsProperties", database.check("jsProperties", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void testPageHRef() throws IOException {
    Browser.navigate("http://diadem.cs.ox.ac.uk/test/re/fast/wwagency/", true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().properties(attr).and().css().allProperties(elem);
      generator.generateFacts("d1");
      assertTrue("all jsProperties", database.check("jsProperties", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void testAllElements_and_Properties_on_frameset() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("frameset.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().properties(attr);
      generator.generateFacts("d1");
      assertTrue("all jsProperties", database.check("src_property", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void testInvisibleChildren() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("invisible-children.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().text(elem).withElementClob(elem);
      generator.generateFacts("d1");
      assertTrue("DOM Facts: ", database.check("src_property", elem.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void testVisibleChildren() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("visible-children.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().text(elem).withElementClob(elem);
      generator.generateFacts("d1");
      assertTrue("DOM Facts: ", database.check("src_property", elem.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void testAllElements_and_Properties_on_iframe() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("iframe.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().properties(attr);
      generator.generateFacts("d1");
      assertTrue("all jsProperties", database.check("src_property", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void testIds() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("verysimple.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    try {
      generator.factsFor().allElements(el_writer).with().text(el_writer).and().ids(el_writer);
      generator.generateFacts("d1");

      assertTrue(database.check("ids", el_writer.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testMappingIds() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("verysimple.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    try {
      generator.factsFor().allElements(new StringWriter()).with().ids(new StringWriter()).and()
      .text(new StringWriter()).and().attributes().forAll(new StringWriter());
      generator.generateFacts("d1");
      final DOMNode b = XPathUtil.getFirstNode("/html/body/p/b", Browser);
      b.getParentNode().removeChild(b);

      // final DOMElement createElement = Browser.getContentDOMWindow().getDocument().createElement("new");
      // createElement.setAttribute("new_attr", "diadem");
      // createElement.setTextContent("some text");

      generator.factsFor().allElements(new StringWriter()).with().ids(el_writer).and().text(el_writer).and()
      .attributes().forAll(el_writer);
      generator.generateFacts("d2");

      assertTrue(database.check("idMapping", el_writer.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testSpans() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("span.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer text = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().text(text).withElementClob(text);
      generator.generateFacts("d1");
      assertTrue("all text nodes and clob", database.check("text+clob", text.toString()));

    } finally {
      elem.close();
      text.close();
    }
  }

  @Test
  public void testSingletonText() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("singletonTextNode.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer text = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().text(text).withElementClob(text);
      generator.generateFacts("d1");
      assertTrue("all text nodes and clob", database.check("text+clob", text.toString()));

    } finally {
      elem.close();
      text.close();
    }
  }

  @Test
  @Ignore
  public void testOnXMLPage() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("xmlPage.xml").toString();
    Browser.navigate(url);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    try {
      generator.dumpAll(el_writer);
      generator.generateFacts("d1");
      assertTrue(database.check("dump_result_page", el_writer.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testDetectXMLPage() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("xmlPage.xml").toString();
    Browser.navigate(url);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final String nodeName = Browser.getContentDOMWindow().getDocument().getDocumentElement().getNodeName();
    System.err.println(nodeName);

  }

  @Override
  @Test
  public void test_allElements_and_CSS_text_decoration() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().css().onlyProperties(attr, CssProperty.text_decoration);
      generator.generateFacts("d1");
      assertTrue("all elements width", database.check("attributes", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void testBeryl() throws IOException {
    Browser.navigate("http://diadem.cs.ox.ac.uk/test/re/fast/wwagency/", true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());

    final Writer browserFactsWriter = new StringWriter();
    final Writer textBoxWriter = new StringWriter();
    final DOMFactGenerator generator = getDOMFactGenerator();
    generator.factsFor().allElementsExcept(browserFactsWriter, "script").with().text(browserFactsWriter)
    .withElementClob(browserFactsWriter).and().attributes()
    .onlyFor(browserFactsWriter, "src", "href", "style", "id", "name", "value", "class", "alt", "title", "onclick")
    .and().css().onlyBoundingBoxes(textBoxWriter);
    generator.generateFacts("d1");
    assertTrue(database.check("text_boxes", textBoxWriter.toString()));
  }

  // http://www.anscombes.co.uk/properties/search?area=&longitude=%3E&latitude=&radius=&saleType=Sale&branchName=St+Johns+Wood+Property&filter[]=branch%3ASt+Johns+Wood+Property

  @Test
  public void testNormalization() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("normalization.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    try {
      generator.factsFor().allElementsExcept(el_writer, "STYLE").with().text(el_writer).withElementClob(el_writer)
      .and().rawDOM(el_writer);
      generator.generateFacts("d1");
      assertTrue(database.check("elements", el_writer.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testTrimming() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("trimming.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    try {
      generator.factsFor().allElementsExcept(el_writer, "STYLE").with().text(el_writer).withElementClob(el_writer);
      // .and().rawDOM(el_writer);
      generator.generateFacts("d1");
      assertTrue(database.check("elements", el_writer.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testBR_Spacing() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("test_br.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    try {
      generator.factsFor().allElementsExcept(el_writer, "STYLE").with().text(el_writer).withElementClob(el_writer);
      // .and().rawDOM(el_writer);
      generator.generateFacts("d1");
      assertTrue(database.check("br_spaced", el_writer.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testBRClob() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("test_br.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    try {
      generator.factsFor().allElementsExcept(new StringWriter(), "STYLE").with().text(new StringWriter())
      .withElementClob(el_writer);
      // .and().rawDOM(el_writer);
      generator.generateFacts("d1");
      assertTrue(database.check("br_clobspaced", el_writer.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testFidelity() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("verysimple.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    final Writer junk = new StringWriter();
    try {

      generator.factsFor().allElements(el_writer).with().attributes().forAll(el_writer).and().ids(el_writer).and()
      .text(el_writer).withElementClob(el_writer).and().rawDOM(junk);
      generator.generateFacts("d1");

      assertTrue(database.check("requested_elements", el_writer.toString()));
      assertTrue(database.check("all_The_Rest", junk.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testFidelityWithGarbage() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("verysimple.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    final Writer junk = new StringWriter();
    try {

      generator.factsFor().allElements(el_writer).with().attributes().forAll(el_writer).and().ids(el_writer).and()
      .text(el_writer).withElementClob(el_writer).and().rawDOM(junk);
      generator.generateFacts("d1");

      assertTrue(database.check("requested_elements", el_writer.toString()));
      assertTrue(database.check("all_The_Rest", junk.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testFidelity_2() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("verysimple.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    final Writer junk = new StringWriter();
    try {

      generator.factsFor().onlyElements(el_writer, "head").with().attributes().forAll(el_writer).and().ids(el_writer)
      .and().text(el_writer).withElementClob(el_writer).and().rawDOM(junk);
      generator.generateFacts("d1");

      assertTrue(database.check("requested_elements", el_writer.toString()));
      assertTrue(database.check("allTheRest", junk.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testFidelity_3() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("verysimple.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    final Writer junk = new StringWriter();
    try {

      generator.factsFor().onlyElements(el_writer, "p").with().attributes().onlyFor(el_writer, "class").and()
      .ids(el_writer).and().text(el_writer).withElementClob(el_writer).and().rawDOM(junk);
      generator.generateFacts("d1");

      assertTrue(database.check("requested_elements", el_writer.toString()));
      assertTrue(database.check("allTheRest", junk.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testFidelity_4() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("verysimple.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    final Writer junk = new StringWriter();
    try {

      generator.factsFor().onlyElements(el_writer, "p").with().attributes().onlyFor(el_writer, "class").and()
      .ids(el_writer).and().text(el_writer).and().rawDOM(junk);
      generator.generateFacts("d1");

      assertTrue(database.check("requested_elements", el_writer.toString()));
      assertTrue(database.check("allTheRest", junk.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testFidelity_5() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("verysimple.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    final Writer junk = new StringWriter();
    try {

      generator.factsFor().onlyElements(el_writer, "p").with().attributes().onlyFor(el_writer, "class").and()
      .ids(el_writer).and().rawDOM(junk);
      generator.generateFacts("d1");

      assertTrue(database.check("requested_elements", el_writer.toString()));
      assertTrue(database.check("allTheRest", junk.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testFidelity_6() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("verysimple.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    final Writer junk = new StringWriter();
    try {

      generator.factsFor().onlyElements(el_writer, "p").with().ids(el_writer).and().rawDOM(junk);
      generator.generateFacts("d1");

      assertTrue(database.check("requested_elements", el_writer.toString()));
      assertTrue(database.check("allTheRest", junk.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testFidelity_7() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("verysimple.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    final Writer junk = new StringWriter();
    try {

      generator.factsFor().onlyElements(el_writer, "p").with().rawDOM(junk);
      generator.generateFacts("d1");

      assertTrue(database.check("requested_elements", el_writer.toString()));
      assertTrue(database.check("allTheRest", junk.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testFidelity_8() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("verysimple.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    assertTrue(database.check("elements", el_writer.toString()));
    final Writer junk = new StringWriter();
    try {

      generator.factsFor().allElementsExcept(el_writer, "p").with().rawDOM(junk);
      generator.generateFacts("d1");

      assertTrue(database.check("requested_elements", el_writer.toString()));
      assertTrue(database.check("allTheRest", junk.toString()));

    } finally {
      el_writer.close();
    }
  }

}
