package uk.ac.ox.cs.diadem.domFactsGenerator;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.TreeSet;

import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;

import com.google.common.base.Splitter;
import com.google.common.collect.Sets;

public class BugFactExtraction {

  /**
   * @param args
   * @throws IOException
   * @throws InterruptedException
   */
  public static void main(final String[] args) throws IOException, InterruptedException {

    ConfigurationFacility.getConfiguration();
    final Engine type = Engine.HTMLUNIT;
    final WebBrowser b = BrowserFactory.newWebBrowser(type, true);
    final String url = "http://diadem.cs.ox.ac.uk/test/re/fast/wwagency/";
    b.navigate(url);

    // b.navigate(url);
    final DOMFactGenerator generator = new DOMFactGenerator(b);
    final Writer el_writer = new StringWriter();
    final Writer el_writer2 = new StringWriter();
    try {
      generator.factsFor().allElements(new StringWriter()).with().ids(el_writer).and().text(new StringWriter());
      generator.generateFacts("d1");
      final String url2 = "http://diadem.cs.ox.ac.uk/test/re/fast/wwagency/result1.html?sale_type_id=1&prop_type_id=0&postcode_area_id=0&beds=1&min=0&max=6000000&search=1";
      b.navigate(url2);
      final DOMFactGenerator generator2 = new DOMFactGenerator(b);
      generator2.factsFor().allElements(new StringWriter()).with().ids(el_writer2).and().text(new StringWriter());
      generator2.generateFacts("d1");

      final String string = el_writer2.toString();
      final Iterable<String> split = Splitter.on("\n").split(string);
      final TreeSet<String> newTreeSet = Sets.newTreeSet(split);
      for (final String line : newTreeSet) {
        System.out.println(line);
      }

    } finally {
      el_writer.close();
      el_writer2.close();
    }

    // final DOMFactGenerator generator = new DOMFactGenerator(b);
    // final Writer el_writer = new StringWriter();
    // String returnStatement = "";
    // if (type == Engine.SWT_MOZILLA)
    // returnStatement = "return";
    //
    // try {
    //
    // StringBuilder s = new StringBuilder();
    // for (int i = 0; i < 10; i++) {
    // final Stopwatch w = new Stopwatch();
    // generator.factsFor().allElements(el_writer);
    // w.start();
    // generator.generateFacts("d1");
    // w.stop();
    // s.append(w.elapsedMillis() + "ms, ");
    // }
    //
    // System.out.println("WebAPI:" + s.toString());
    //
    // final Object evaluate = b.evaluate("var clob; " + "function walk(n,level){ "
    // + "for (var i = 0; i < n.childNodes.length; i++) { " + "var child = n.childNodes[i];" + "clob+=n.nodeName;"
    // + "walk(child,level+1);}}; " + "function walkTree(){" + "var s = new Date().getMilliseconds(); "
    // + "walk(document.documentElement,0); " + "var e = new Date().getMilliseconds(); " + "return e-s;} "
    // + "var res = new Array();var str='';" + "for(var k=0;k<10;k++){res[k]=walkTree();str+=res[k]+'ms,';} "
    // + returnStatement + " str;");
    // System.out.println("Javascript:" + evaluate);
    //
    // s = new StringBuilder();
    // for (int i = 0; i < 1; i++) {
    // final Stopwatch w = new Stopwatch();
    // generator.factsFor().allElements(el_writer);
    // w.start();
    // generator.generateFacts("d1");
    // w.stop();
    // s.append(w.elapsedMillis() + "ms, ");
    // }
    //
    // System.out.println("WebAPI:" + s.toString());
    // } finally {
    // el_writer.close();
    // }
    b.shutdown();
  }
}
