/**
 * Header
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.EnumMap;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import uk.ac.ox.cs.diadem.domFactsGenerator.modular.XPathOptionSelector.Axis;
import uk.ac.ox.cs.diadem.env.testsupport.StandardTestcase;
import uk.ac.ox.cs.diadem.env.testsupport.StringDatabase.Mode;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.DOMCSS2Properties.CssProperty;
import uk.ac.ox.cs.diadem.webapi.dom.DOMElement;
import uk.ac.ox.cs.diadem.webapi.dom.finder.DOMNodeFinderService;
import uk.ac.ox.cs.diadem.webapi.dom.finder.XPathNodePointerRanking;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;

import com.google.common.base.Splitter;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 *
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public abstract class AbstractFactGeneratorTest extends StandardTestcase {

  protected static WebBrowser Browser;

  @Before
  public void resetPage() {
    final String url = ModularFactsOnWebDriverFF.class.getResource("simple.html").toString();
    Browser.navigate(url, true);
  }

  void setParametricMethodKeyPrefix() {

    database.setKeyPrefix(Thread.currentThread().getStackTrace()[2].getMethodName() + "/" + Browser.getEngine() + "/");
  }

  DOMFactGenerator getDOMFactGenerator() {
    if (Browser.getEngine() == Engine.WEBDRIVER_FF)
      return new DOMFactGeneratorOnJS(Browser);
    else
      return new DOMFactGenerator(Browser);
  }

  @Test
  public void testAllElements() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    try {
      generator.factsFor().allElements(el_writer);
      generator.generateFacts("d1");
      assertTrue(database.check("elements", el_writer.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  @Ignore
  public void testAllPossibleOnResultPage() throws IOException {
    Browser
    .navigate(
        "http://diadem.cs.ox.ac.uk/test/re/fast/wwagency/result.php?sale_type_id=1&prop_type_id=0&postcode_area_id=0&beds=1&min=0&max=6000000&search=1",
        true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    try {
      generator.dumpAll(el_writer);
      generator.generateFacts("d1");
      assertTrue(database.check("dump_result_page", el_writer.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  @Ignore
  public void testAA_Only_Head_and_TEXT_on_resultPage() throws IOException {
    Browser
    .navigate(
        "http://diadem.cs.ox.ac.uk/test/re/fast/wwagency/result.php?sale_type_id=1&prop_type_id=0&postcode_area_id=0&beds=1&min=0&max=6000000&search=1",
        true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer text = new StringWriter();
    final Writer junk = new StringWriter();
    try {
      generator.factsFor().onlyElements(text, "HEAD").with().text(text).withElementClob(text).and().ids(text).and()
      .rawDOM(junk);
      generator.generateFacts("d1");

      assertTrue("text+clob for P", database.check("head_text+clob", junk.toString()));

    } finally {
      text.close();
    }
  }

  @Test
  public void testAllElementsIterative() throws IOException {
    // final String url =
    // "http://diadem.cs.ox.ac.uk/test/re/fast/wwagency/result1.html?sale_type_id=1&prop_type_id=0&postcode_area_id=0&beds=1&min=0&max=6000000&search=1";
    String previous = "";
    for (int i = 0; i < 4; i++) {
      final DOMFactGenerator generator = getDOMFactGenerator();
      final Writer el_writer = new StringWriter();
      try {
        generator.factsFor().allElements(el_writer);
        generator.generateFacts("d1");
        final String current = el_writer.toString();
        if (i > 0) {
          assertTrue("", previous.equals(current));
        }
        previous = current;

      } finally {
        el_writer.close();
      }
    }
  }

  @Test
  public void testAllElementsAndIds() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    try {
      generator.factsFor().allElements(el_writer).with().text(el_writer).and().ids(el_writer);
      generator.generateFacts("d1");
      assertTrue(database.check("elements+Id", el_writer.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testAllElementsExcept() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    try {
      generator.factsFor().allElementsExcept(el_writer, "A");
      generator.generateFacts("d1");
      assertTrue(database.check("notA", el_writer.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testElementsExcept_A_with_Ids() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer el_writer = new StringWriter();
    try {
      generator.factsFor().allElementsExcept(el_writer, "A").with().text(el_writer).and().ids(el_writer);
      generator.generateFacts("d1");
      assertTrue(database.check("notA+ids", el_writer.toString()));

    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testAllElementsExcept_XX_IsEqualsToAll() throws IOException {
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer noXX = new StringWriter();
    final Writer all = new StringWriter();
    try {
      generator.factsFor().allElementsExcept(noXX, "XX");
      generator.factsFor().allElements(all);
      generator.generateFacts("d1");
      assertEquals("Same content", noXX.toString(), all.toString());

    } finally {
      all.close();
      noXX.close();
    }
  }

  @Test
  public void testOnlyElements_A_UpperAndLowerCase() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    Writer el_writer = new StringWriter();
    try {
      generator.factsFor().onlyElements(el_writer, "A");
      generator.generateFacts("d1");
      final String only_A = el_writer.toString();
      assertTrue(database.check("elementsA", only_A));
      el_writer = new StringWriter();
      generator.factsFor().onlyElements(el_writer, "a");
      generator.generateFacts("d1");
      final String only_a = el_writer.toString();
      assertTrue("same facts regardless lower or upper case tag", only_A.equals(only_a));
    } finally {
      el_writer.close();
    }
  }

  @Test
  public void testAllElements_XPATH() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer xpath = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().xpath().allRelations(xpath);
      generator.generateFacts("d1");
      assertTrue("all elements", database.check("elements", elem.toString()));
      assertTrue("all xpath relations", database.checkSorted("xpath", xpath.toString()));

    } finally {
      elem.close();
      xpath.close();
    }
  }

  @Test
  public void test_allElements_and_Child_Descendant() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer xpath = new StringWriter();
    try {
      final EnumMap<Axis, Writer> map = Maps.newEnumMap(Axis.class);
      map.put(Axis.CHILD, xpath);
      map.put(Axis.DESCENDANT, xpath);
      generator.factsFor().allElements(elem).with().xpath().onlyRelations(map);
      generator.generateFacts("d1");
      // final String expected = database.lookup("child+descendant");
      // assertEquals(expected, xpath.toString());//
      assertTrue("all elements child+descendant", database.checkSorted("child+descendant", xpath.toString()));

    } finally {
      elem.close();
      xpath.close();
    }
  }

  @Test
  public void testAllElements_and_Descendant() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer xpath = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().xpath().onlyRelations(xpath, Axis.DESCENDANT);
      generator.generateFacts("d1");

      assertTrue("descendant", database.checkSorted("descendant", xpath.toString()));
      // final String elements = database.lookup("elements");
      // assertEquals("all elements", elements, elem.toString());
      // final String descendant = database.lookup("descendant");
      // assertEquals("descendant", descendant, xpath.toString());

    } finally {
      elem.close();
      xpath.close();
    }
  }

  @Test
  public void testAllElements_and_DescendantSelf() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer xpath = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().xpath().onlyRelations(xpath, Axis.DESCENDANT_OR_SELF);
      generator.generateFacts("d1");

      assertTrue("DESCENDANT_OR_SELF", database.checkSorted("DESCENDANT_OR_SELF", xpath.toString()));
      // final String elements = database.lookup("elements");
      // assertEquals("all elements", elements, elem.toString());
      // final String descendant = database.lookup("descendant");
      // assertEquals("descendant", descendant, xpath.toString());

    } finally {
      elem.close();
      xpath.close();
    }
  }

  @Test
  public void testAllElements_and_Child() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer xpath = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().xpath().onlyRelations(xpath, Axis.CHILD);
      generator.generateFacts("d1");

      assertTrue("CHILD", database.checkSorted("CHILD", xpath.toString()));
      // final String elements = database.lookup("elements");
      // assertEquals("all elements", elements, elem.toString());
      // final String descendant = database.lookup("descendant");
      // assertEquals("descendant", descendant, xpath.toString());

    } finally {
      elem.close();
      xpath.close();
    }
  }

  @Test
  public void testAllElements_and_Following() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer xpath = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().xpath().onlyRelations(xpath, Axis.FOLLOWING);
      generator.generateFacts("d1");

      assertTrue("FOLLOWING", database.checkSorted("FOLLOWING", xpath.toString()));
      // final String elements = database.lookup("elements");
      // assertEquals("all elements", elements, elem.toString());
      // final String descendant = database.lookup("descendant");
      // assertEquals("descendant", descendant, xpath.toString());

    } finally {
      elem.close();
      xpath.close();
    }
  }

  @Test
  public void testAllElements_and_FollowingSibling() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer xpath = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().xpath().onlyRelations(xpath, Axis.FOLLOWING_SIBLING);
      generator.generateFacts("d1");

      assertTrue("FOLLOWING_SIBLING", database.checkSorted("FOLLOWING_SIBLING", xpath.toString()));
      // final String elements = database.lookup("elements");
      // assertEquals("all elements", elements, elem.toString());
      // final String descendant = database.lookup("descendant");
      // assertEquals("descendant", descendant, xpath.toString());

    } finally {
      elem.close();
      xpath.close();
    }
  }

  @Test
  public void testOnly_P_and_DescendantSelf() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer xpath = new StringWriter();
    try {
      generator.factsFor().onlyElements(elem, "P").with().xpath().onlyRelations(xpath, Axis.DESCENDANT_OR_SELF);
      generator.generateFacts("d1");

      assertTrue("DESCENDANT_OR_SELF", database.check("DESCENDANT_OR_SELF", xpath.toString()));

    } finally {
      elem.close();
      xpath.close();
    }
  }

  @Test
  public void testAllElements_and_TEXT() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer text = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().text(text).withElementClob(text);
      generator.generateFacts("d1");
      assertTrue("all text nodes and clob", database.check("text+clob", text.toString()));

    } finally {
      elem.close();
      text.close();
    }
  }

  Mode getTestMode() {
    return Mode.RECORD;
  }

  @Test
  public void testOnly_P_and_TEXT() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer text = new StringWriter();
    try {
      generator.factsFor().onlyElements(elem, "P").with().text(text).withElementClob(text);
      generator.generateFacts("d1");

      assertTrue("text+clob for P", database.check("text+clob", text.toString()));

    } finally {
      elem.close();
      text.close();
    }
  }

  // @Test
  // public void testAllElements_and_GARBAGE_TEXT() throws IOException {
  // setParametricMethodKeyPrefix();
  // database.setMode(Mode.INTERACTIVE);
  // final DOMFactGenerator generator = new DOMFactGenerator(Browser);
  // final Writer elem = new StringWriter();
  // final Writer text = new StringWriter();
  // try {
  // generator.factsFor().allElements(elem).with().text(text).withElementClob(text).withGarbageNodes();
  // generator.generateFacts("d1");
  // assertTrue("all text nodes including garbage", database.check("text+garbage", text.toString()));
  //
  // } finally {
  // elem.close();
  // text.close();
  // }
  // }

  @Test
  public void testAllElements_and_Attributes() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().attributes().forAll(attr);
      generator.generateFacts("d1");
      assertTrue("all attributes", database.check("attributes", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void testWeirdAttributes() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("attr-names.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().attributes().forAll(attr);
      generator.generateFacts("d1");
      assertTrue("weird attributes", database.check("weird-attributes", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void test_allElements_and_CSS_width() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().css().onlyProperties(attr, CssProperty.width);
      generator.generateFacts("d1");
      assertTrue("all elements width", database.check("attributes", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void test_allElements_and_CSS_text_decoration() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().css().onlyProperties(attr, CssProperty.text_decoration);
      generator.generateFacts("d1");
      assertTrue("all elements width", database.check("attributes", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void test_allElements_and_CSS_boxes() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().css().onlyProperties(elem, CssProperty.accelerator).withBoxes(attr);
      generator.generateFacts("d1");
      assertTrue("all elements boxes", database.check("boxes", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void test_allElements_and_CSS_width_height() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      final EnumMap<CssProperty, Writer> map = Maps.newEnumMap(CssProperty.class);
      map.put(CssProperty.width, attr);
      map.put(CssProperty.height, attr);
      generator.factsFor().allElements(elem).with().css().onlyProperties(map);
      generator.generateFacts("d1");
      assertTrue("all elements width+width", database.check("width+heigth", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void test_P_Elements_and_CSS() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().onlyElements(elem, "P").with().css().allProperties(attr);
      generator.generateFacts("d1");
      assertTrue("P elements and all CSS", database.check("attributes", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void test_SPAN_Padding() throws IOException {
    final String url = ModularFactsOnWebDriverFF.class.getResource("inline.html").toString();
    Browser.navigate(url, true);
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    try {
      generator.factsFor().allElementsExcept(elem, "style").with().text(elem).withElementClob(elem);
      generator.generateFacts("d1");
      assertTrue("Padded span is separated", database.check("padded", elem.toString()));

    } finally {
      elem.close();
    }
  }

  @Test
  public void testOnly_P_and_Attributes() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().onlyElements(elem, "P").with().attributes().forAll(attr);
      generator.generateFacts("d1");
      assertTrue("all attributes", database.check("attributes", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void testOnly_P_and_Ids() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().onlyElements(elem, "P").with().text(attr).and().ids(attr);
      generator.generateFacts("d1");
      assertTrue("p_ids ", database.check("p_ids", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void testOnly_P_and_Class_Attribute() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().onlyElements(elem, "P").with().attributes().onlyFor(attr, "class");
      generator.generateFacts("d1");
      assertTrue("p elements and class attributes", database.check("p_class", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void testAllOnlyBoxes() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().allElements(elem).with().css().onlyBoundingBoxes(attr);
      generator.generateFacts("d1");
      assertTrue("all  boxes", database.check("all_boxes", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void test_A_and_OnlyBoxes() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());
    final DOMFactGenerator generator = getDOMFactGenerator();
    final Writer elem = new StringWriter();
    final Writer attr = new StringWriter();
    try {
      generator.factsFor().onlyElements(elem, "A").with().css().onlyBoundingBoxes(attr);
      generator.generateFacts("d1");
      assertTrue("a's boxes", database.check("a's boxes", attr.toString()));

    } finally {
      elem.close();
      attr.close();
    }
  }

  @Test
  public void testUnion() throws IOException {

    final DOMFactGenerator generator = getDOMFactGenerator();
    final StringWriter[] writers = new StringWriter[] { new StringWriter(), new StringWriter(), new StringWriter() };
    generator.factsFor().allElements(writers[0]).with().css().allProperties(writers[1]).and().attributes()
    .forAll(writers[2]);
    generator.generateFacts("d1");
    try {
      final Set<String> all = Sets.newTreeSet();
      for (final StringWriter writer : writers) {
        asStrings(writer, all);
        writer.getBuffer().setLength(0);
      }

      generator.factsFor().allElementsExcept(writers[0], "TD").with().css().allProperties(writers[1]).and()
      .attributes().forAll(writers[2]);
      generator.generateFacts("d1");
      final Set<String> except = Sets.newTreeSet();
      for (final StringWriter writer : writers) {
        asStrings(writer, except);
        writer.getBuffer().setLength(0);
      }
      generator.factsFor().onlyElements(writers[0], "TD").with().css().allProperties(writers[1]).and().attributes()
      .forAll(writers[2]);
      generator.generateFacts("d1");
      final Set<String> only = Sets.newTreeSet();
      for (final StringWriter writer : writers) {
        asStrings(writer, only);
        writer.getBuffer().setLength(0);
      }

      final Set<String> union = Sets.newTreeSet(Sets.union(except, only));
      Assert.assertTrue("Same Content", CollectionUtils.isEqualCollection(union, all));
      Assert.assertEquals("same size", all.size(), (except.size() + only.size()));
    } finally {
      for (final StringWriter writer : writers) {
        writer.close();
      }
    }
  }

  private void asStrings(final Writer el_writer, final Set<String> newFatcs) {
    final Iterable<String> split = Splitter.onPattern(IOUtils.LINE_SEPARATOR).split(el_writer.toString());
    for (final String string : split)
      if (!string.equals("")) {
        newFatcs.add(string);
      }
  }

  @Override
  public String toString() {
    return super.toString();
  }

  @Test
  public void testAllPointersOnSimplePage() throws IOException {
    setParametricMethodKeyPrefix();
    database.setMode(getTestMode());

    final DOMElement element = AbstractFactGeneratorTest.Browser.getContentDOMWindow().getDocument()
        .getElementById("id1");
    final XPathNodePointerRanking computeRobustPointers = DOMNodeFinderService.computeRobustPointers(element);
    // System.out.println(computeRobustPointers.size());
    assertTrue(database.check("title_robust_list", computeRobustPointers.toString()));
  }

}
