// $codepro.audit.disable closeWhereCreated, debuggingCode
/**
 * Header
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import org.junit.BeforeClass;

import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;

/**
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public class ModularFactsOnHTMLUnit extends AbstractFactGeneratorTest {

  @BeforeClass
  public static void oneTimesetUp() {

    Browser = BrowserFactory.newWebBrowser(Engine.HTMLUNIT, true);
  }

}
