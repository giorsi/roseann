package uk.ac.ox.cs.diadem.domFactsGenerator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.DiademRuntimeException;
import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.util.misc.EscapingUtils;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode.Type;

class StartEndNodeId {
  private static final Logger LOGGER = LoggerFactory.getLogger(StartEndNodeId.class);

  private String formattedId = null;
  private String ident = null;
  protected final long start;
  protected long end = Long.MAX_VALUE;
  protected final long parent;
  protected final Configuration config = ConfigurationFacility.getConfiguration();
  protected String idElementPrefix = config.getString("facts.dom.html-element.idprefix");
  protected String idTextPrefix = config.getString("facts.dom.html-text.idprefix");

  protected StartEndNodeId(final long parent, final long start) {

    this.start = start;
    this.parent = parent;
  }

  protected StartEndNodeId(final long parent, final long start, final long end) {

    this.start = start;
    this.end = end;
    this.parent = parent;
  }

  protected StartEndNodeId(final String formattedId, final long parent, final long start, final long end) {
    this(parent, start, end);
    this.formattedId = formattedId;

  }

  @Override
  public String toString() {

    return start + ", " + end + ", " + parent;
  }

  String formatElementIdentifier(final DOMNode node) {

    if (ident != null)
      return ident;

    ident = idElementPrefix + start + "_" + EscapingUtils.escapeForDLVConstants(node.getLocalName());
    return ident;
  }

  String formatTextIdentifier(final DOMNode node) {

    if (ident != null)
      return ident;
    String prefix = idTextPrefix;
    if (EscapingUtils.isGarbageText(node.getNodeValue()))
      prefix = config.getString("facts.dom.html-text.idprefix-garbage");
    ident = prefix + start;
    return ident;
  }

  public String id(final DOMNode node) {
    // lazy
    if (formattedId == null)
      if (node.getNodeType() == Type.ELEMENT)
        formattedId = formatElementIdentifier(node);
      else if (node.getNodeType() == Type.TEXT)
        formattedId = formatTextIdentifier(node);
      else {
        LOGGER.error("Unexpected call to formatIdentifier for a node of type {}", node.getNodeType());
        throw new DiademRuntimeException("Unexpected call to formatIdentifier() for a node of type {}", LOGGER);
      }
    return formattedId;
  }

  // can be null
  public String getFormattedId() {
    return formattedId;
  }

  // public String formatAttrIdentifier(DOMNode node, DOMNode attr,
  // Configuration config) {
  // return this.formatElementIdentifier(node, config) + "_"
  // + FormattingUtils.escapeForDLV(node.getLocalName());
  // }
  // public String formatAttrIdentifier(DOMNode node, DOMNode attr,
  // Configuration config) {
  // return this.formatIdentifier(node, config) + "_"
  // + FormattingUtils.escapeForDLV(node.getNodeName());
  // }
  public String formatAttrIdentifier(final DOMNode node, final String attrNodeName) {

    return formatElementIdentifier(node) + "_" + EscapingUtils.escapeForDLVConstants(attrNodeName);
  }

  public long getStart() {
    return start;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {

    final int prime = 31;
    int result = 1;
    result = (prime * result) + (int) (end ^ (end >>> 32));
    result = (prime * result) + (int) (parent ^ (parent >>> 32));
    result = (prime * result) + (int) (start ^ (start >>> 32));
    return result;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(final Object obj) {

    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    final StartEndNodeId other = (StartEndNodeId) obj;
    if (end != other.end)
      return false;
    if (parent != other.parent)
      return false;
    if (start != other.start)
      return false;
    return true;
  }

}