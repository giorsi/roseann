///**
// * Header
// */
//package uk.ac.ox.cs.diadem.domFactsGenerator;
//
//import java.io.Writer;
//import java.util.List;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import uk.ac.ox.cs.diadem.env.DiademRuntimeException;
//import uk.ac.ox.cs.diadem.env.ParserException;
//import uk.ac.ox.cs.diadem.model.Model;
//import uk.ac.ox.cs.diadem.model.Term;
//
//import com.google.common.collect.Lists;
//
///**
// * 
// * An implementation of {@link FactSerializer} to produce prolog-like facts.
// * 
// * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
// */
//class FactsAsModelSerializer implements FactSerializerOnWriter {
//
//  static final Logger LOGGER = LoggerFactory.getLogger(FactsAsModelSerializer.class);
//  private final Model facts;
//
//  FactsAsModelSerializer(final Model model) {
//    this.facts = model;
//  }
//
//  private void fillModel(final String relationName, final List<FactParameter> params) {
//    try {
//      final Term[] terms = new Term[params.size()];
//      int i = 0;
//      for (final FactParameter p : params) {
//        terms[i] = facts.parseTerm(p.value);
//        i++;
//      }
//      facts.add(relationName, terms);
//
//    } catch (final ParserException e) {
//      LOGGER.error("Cannot parse {} as Atom", relationName);
//      throw new DiademRuntimeException("Error in parsing dom facts", e, LOGGER);
//    }
//  }
//
//  @Override
//  public void outputFact(final Writer destination, final String relationName, final FactParameter... params) {
//    LOGGER.debug("BackCompabilty: writes only in the model, no stream");
//    fillModel(relationName, Lists.newArrayList(params));
//  }
//
//  @Override
//  public void outputFact(final Writer destination, final String relationName, final List<FactParameter> params) {
//    LOGGER.debug("BackCompabilty: writes only in the model, no stream");
//    fillModel(relationName, params);
//  }
//
//  @Override
//  public String toString() {
//    return super.toString();
//  }
// }
