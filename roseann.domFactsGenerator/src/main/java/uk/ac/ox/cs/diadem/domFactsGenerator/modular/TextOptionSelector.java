package uk.ac.ox.cs.diadem.domFactsGenerator.modular;

import java.io.Writer;

import uk.ac.ox.cs.diadem.domFactsGenerator.DOMFactGenerator;

/**
 * Configures the extraction for text facts. It is possible to specify whether to include an elementClob for the
 * selected elements along with its related predicates(not included by default).
 * 
 * For example, by enabling the element clob, it produces facts like:
 * 
 * <pre>
 * {@code
 * html_text( textnodeId, start, end, parent, pageId ).
 * 
 * clob_element( elementClobId,"text",pageId).
 * content( textnodeId, elementsClobId, startOffset, endOffset ).
 * content( elementId, elementsClobId, startOffset, endOffset ).
 * }
 * </pre>
 * 
 * 
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public interface TextOptionSelector extends AndOptions {

  // /**
  // * To enable facts relative to attributes text, like:
  // *
  // * <pre>
  // * {@code
  // * content( attributeNodeId, attrClobId, startOffset, endOffset ).
  // * clob_attribute( attrClobId, "attribute ~@~ text", pageId).
  // * }
  // * </pre>
  // *
  // * @param destination
  // * the destination writer
  // * @return the same object to continue the configuration
  // */
  // public TextOptionSelector withAttributeClob(Writer destination);

  /**
   * To enable facts relative to elements text, like:
   * 
   * <pre>
   * {@code
   * html_text( textnodeId, start, end, parent, pageId ).
   * clob_element( elementClobId,"text",pageId).
   * content( textnodeId, elementsClobId, startOffset, endOffset ).
   * content( elementId, elementsClobId, startOffset, endOffset ).
   * }
   * </pre>
   * 
   * @param destination
   *          the destination writer
   * @return the same object to continue the configuration
   */
  TextOptionSelector withElementClob(Writer destination);

  /**
   * Builds the configured fact extraction. Use it to terminate the configuration
   * 
   * @deprecated not necessary any more to call tihs method to terminate the configuration. It is added by default on
   *             {@link DOMFactGenerator#factsFor()}
   */
  @Deprecated
  public void build();

}