// $codepro.audit.disable lossOfPrecisionInCast
/*
 * Copyright (c)2011, DIADEM Team
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the DIADEM team nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DIADEM Team BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import java.io.Writer;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter;
import uk.ac.ox.cs.diadem.util.misc.EscapingUtils;
import uk.ac.ox.cs.diadem.webapi.dom.DOMBoundingClientRect;
import uk.ac.ox.cs.diadem.webapi.dom.DOMCSS2Properties.CssProperty;
import uk.ac.ox.cs.diadem.webapi.dom.DOMCSSStyleDeclaration;
import uk.ac.ox.cs.diadem.webapi.dom.DOMElement;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode;
import uk.ac.ox.cs.diadem.webapi.dom.DOMVisitor;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
class CSSVisitorImpl extends AbstractStartEndVisitor {

  static DOMVisitor createCSSVisitor(final FactSerializerOnWriter serializer, final String context,
      final Predicate<? super String> predicate, final PredicateWithWriter<? super String> cssSelectorPredicate) {
    return new CSSVisitorImpl(serializer, context, predicate, cssSelectorPredicate);
  }

  static final Logger LOGGER = LoggerFactory.getLogger(ElementVisitorImpl.class);
  final FactSerializerOnWriter serializer;
  final String pageid;
  private final boolean generateBoundingBoxes;
  private final Predicate<? super String> predicate;
  private final PredicateWithWriter<? super String> cssSelectorPredicate;

  private final String pageidRole = config.getString("facts.dom.css.box.pageid");
  private final String nodeRole = config.getString("facts.dom.css.box.node");
  private final String left = config.getString("facts.dom.css.box.left");
  private final String top = config.getString("facts.dom.css.box.top");
  private final String right = config.getString("facts.dom.css.box.right");
  private final String bottom = config.getString("facts.dom.css.box.bottom");
  private final String width = config.getString("facts.dom.css.box.width");
  private final String height = config.getString("facts.dom.css.box.height");
  private final String valueRole = config.getString("facts.dom.css.value");
  private final String cssPrefix = config.getString("facts.dom.relations.css-prefix");
  // use to canonicalize predicate names to avoid building strings
  private final Map<CssProperty, String> predicateMap;

  private CSSVisitorImpl(final FactSerializerOnWriter serializer, final String pageid,
      final Predicate<? super String> predicate, final PredicateWithWriter<? super String> cssSelectorPredicate) {
    this.serializer = serializer;
    this.pageid = pageid;
    this.predicate = predicate;
    this.cssSelectorPredicate = cssSelectorPredicate;
    generateBoundingBoxes = getBoxesWriter() != null;
    predicateMap = Maps.newEnumMap(CssProperty.class);
    // initialize the map of predicate names
    for (final CssProperty cssProp : CssProperty.values()) {
      if (cssSelectorPredicate.apply(cssProp.getPropertyName()))
        continue;
      predicateMap.put(cssProp, cssPrefix + cssProp.asLowerCamelCase());
    }
  }

  private Writer getBoxesWriter() {
    return cssSelectorPredicate.getWriterFor(config.getString("facts.dom.relations.box"));
  }

  @Override
  public boolean filterOut(final DOMNode node) {
    return predicate.apply(node.getNodeName().toUpperCase());
  }

  @Override
  public void startElement(final DOMNode node) {

    addStartLabel(node);

    // if the element is to filter, we don't produce any output
    if (filterOut(node)) {
      LOGGER.debug("CSS facts skipped for Element {}", node.getNodeName());
      return;
    }

    if ((node.getNodeType() != DOMNode.Type.ELEMENT) || node.getNodeName().equals("STYLE"))
      return;
    final DOMCSSStyleDeclaration style = ((DOMElement) node).getComputedStyle();
    for (final CssProperty cssProp : CssProperty.values()) {
      // filterout if not contained in the configured selection
      if (cssSelectorPredicate.apply(cssProp.getPropertyName()))
        continue;
      final String value = style.getPropertyValue(cssProp.getPropertyName());
      if (!canFilterOutValue(value))
        // dom__css__backgroundAttachment( pageId, e_1_html, "scroll" ).
        serializeCSSProperty(cssSelectorPredicate.getWriterFor(cssProp.getPropertyName()), node, cssProp, value);

    }

    if (generateBoundingBoxes) {
      LOGGER.debug("Generating bounding boxes for Element {}", node.getNodeName());
      final StartEndNodeId ident = getStartEndNodeId(node);
      final DOMBoundingClientRect rect = ((DOMElement) node).getBoundingClientRect();
      if (hasNegativeCoordinates(rect)) {
        CSSVisitorImpl.LOGGER.warn(
            "Found box with negative coordinates..skipped element {} ({}, {}, {}, {}, {})",
            new Object[] { ident.formatElementIdentifier(node), rect.getLeft(), rect.getTop(), rect.getRight(),
                rect.getBottom() });
        return;
      }

      final List<FactParameter> params = new LinkedList<FactParameter>();
      params.add(new FactParameter(pageidRole, pageid));
      params.add(new FactParameter(nodeRole, ident.formatElementIdentifier(node)));
      params.add(new FactParameter(left, Integer.toString((int) rect.getLeft())));
      params.add(new FactParameter(top, Integer.toString((int) rect.getTop())));
      params.add(new FactParameter(right, Integer.toString((int) rect.getRight())));
      params.add(new FactParameter(bottom, Integer.toString((int) rect.getBottom())));
      params.add(new FactParameter(width, Integer.toString((int) rect.getWidth())));
      params.add(new FactParameter(height, Integer.toString((int) rect.getHeight())));
      serializer.outputFact(getBoxesWriter(), config.getString("facts.dom.relations.box"), params);
    }
  }

  private boolean hasNegativeCoordinates(final DOMBoundingClientRect rect) {

    final float left = rect.getLeft();
    final float top = rect.getTop();
    final float width = rect.getWidth();
    final float height = rect.getHeight();
    final float bottom = rect.getBottom();
    final float right = rect.getRight();
    return (left < 0) | (top < 0) | (width < 0) | (height < 0) | (bottom < 0) | (right < 0);
  }

  private boolean canFilterOutValue(final String value) {

    // System.out.println(value);
    if (value.length() == 0)
      return true;
    if (value.contains("NOT_FOUND"))
      return true;
    return false;
  }

  void serializeCSSProperty(final Writer destination, final DOMNode node, final CssProperty property, final String value) {

    final StartEndNodeId ident = getStartEndNodeId(node);
    final List<FactParameter> params = Lists.newLinkedList();
    params.add(new FactParameter(pageidRole, pageid));
    params.add(new FactParameter(nodeRole, ident.formatElementIdentifier(node)));
    // params.add(new FactParameter(config.getString("facts.dom.css.property"), property));
    params.add(new FactParameter(valueRole, EscapingUtils.quoteIfNotNumberGreaterThenZero(value)));

    serializer.outputFact(destination, predicateMap.get(property), params);
  }

  @Override
  public String toString() {
    return super.toString();
  }
}
