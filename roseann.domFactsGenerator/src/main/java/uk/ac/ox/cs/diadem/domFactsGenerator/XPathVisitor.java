// $codepro.audit.disable cyclomaticComplexity
/**
 * Header
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import java.io.Writer;
import java.util.BitSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter;
import uk.ac.ox.cs.diadem.domFactsGenerator.modular.XPathOptionSelector.Axis;
import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode.Type;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
class XPathVisitor extends AbstractStartEndVisitor {

  private static final Logger LOGGER = LoggerFactory.getLogger(XPathVisitor.class);

  private final FactSerializerOnWriter serializer;
  private final String pageId;
  private final PredicateWithWriter<? super String> elementSelectorPredicate;

  private final PredicateWithWriter<? super String> xpathSelector;

  private final BitSet xpathrelations;

  private final boolean textNodes;

  /**
   * 
   * @param serializer
   * @param pageId
   * @param elementSelectorPredicate
   * @param options
   *          an array of boolean to enable/disable relations: [1] for child, [2] for descendant, [3] for
   *          descendant-or-self, [4] for following
   */
  XPathVisitor(final FactSerializerOnWriter serializer, final String pageId,
      final PredicateWithWriter<? super String> predicate, final PredicateWithWriter<? super String> xpathSelector,
      final boolean textNodes) {
    this.serializer = serializer;
    this.pageId = pageId;
    elementSelectorPredicate = predicate;
    this.xpathSelector = xpathSelector;
    // oxpathChild = !canFilterOutAxis(Axis.CHILD);
    // oxpathDescendant = !canFilterOutAxis(Axis.DESCENDANT);
    // xpathDescOrSelf = !canFilterOutAxis(Axis.DESCENDANT_OR_SELF);
    // oxpathFollowing = !canFilterOutAxis(Axis.FOLLOWING);
    // oxpathFollowingSibling = !canFilterOutAxis(Axis.FOLLOWING_SIBLING);
    this.textNodes = textNodes;

    xpathrelations = new BitSet(Axis.values().length);

    for (final Axis axis : Axis.values())
      if (!canFilterOutAxis(axis))
        xpathrelations.set(axis.getCode());

  }

  private boolean canFilterOutAxis(final Axis axis) {
    return xpathSelector.apply(axis.name());
  }

  @Override
  public boolean filterOut(final DOMNode node) {
    if (node.getNodeType() == Type.TEXT)
      return elementSelectorPredicate.apply(node.getParentNode().getNodeName().toUpperCase());
    else
      return elementSelectorPredicate.apply(node.getNodeName().toUpperCase());
  }

  private Writer getWriter(final Axis axis) {
    return xpathSelector.getWriterFor(axis.name());
  }

  @Override
  public void finish() {
    final Set<DOMNode> keySet = map.keySet();
    final Set<StartEndNodeId> nodes = Sets.newHashSet();
    for (final DOMNode node : keySet) {
      // if the element is marked to be filter out, we don't produce any relation for it
      if (filterOut(node)) {
        LOGGER.debug("XPath facts skipped for Element {}", node.getNodeName());
        continue;
      }
      // if text nodes are not requetsed
      if ((node.getNodeType() == Type.TEXT) && !textNodes)
        continue;

      nodes.add(getStartEndNodeId(node));
    }

    // // copy the map of writers
    // final Map<Axis, Writer> writerMap = Maps.newHashMap();
    // for (final Axis axes : Axis.values()) {
    // final Writer writerFor = xpathSelector.getWriterFor(axes.name());
    // if (writerFor != null)
    // writerMap.put(axes, writerFor);
    // }
    final Map<? super String, Writer> writerMap = xpathSelector.asMap();

    buildXPathRelations(nodes, xpathrelations, serializer, writerMap, pageId);
  }

  static void buildXPathRelations(final Set<StartEndNodeId> nodes, final BitSet xpathRelations,
      final FactSerializerOnWriter serializer, final Map<? super String, Writer> writers, final String pageId) {
    final Map<StartEndNodeId, Boolean> map = Maps.newHashMap();
    for (final StartEndNodeId v : nodes)
      map.put(v, Boolean.TRUE);
    buildXPathRelations(map, xpathRelations, serializer, writers, pageId);
  }

  @Override
  public String toString() {
    return super.toString();
  }

  public static void buildXPathRelations(final Map<StartEndNodeId, Boolean> nodes, final BitSet xpathRelations,
      final FactSerializerOnWriter serializer, final Map<? super String, Writer> writers, final String pageId) {
    final Configuration config = ConfigurationFacility.getConfiguration();
    // child(C,P) :- html_element(C,_,_,Pstart,_,_),
    // html_element(P,Pstart,_,_,_,_).
    for (final StartEndNodeId v : nodes.keySet()) {

      final Boolean skip = nodes.get(v); // node used only for shadow dom
      if (skip)
        continue;

      final String idForNodeV = v.getFormattedId();

      for (final StartEndNodeId c : nodes.keySet()) {

        final Boolean toskip = nodes.get(c);// node used only for shadow dom
        if (toskip)
          continue;

        final String idForNodeC = c.getFormattedId();

        // same node, descendantOrSelf(El,El)
        if (xpathRelations.get(Axis.DESCENDANT_OR_SELF.getCode()))
          if ((v.start >= c.start) && (v.start <= c.end) && (v.end >= c.start) && (v.end <= c.end)) {
            final Writer destination = writers.get(Axis.DESCENDANT_OR_SELF.name());

            serializer.outputFact(destination, config.getString("facts.dom.relations.xpath.descendant-or-self"),
                FactParameter.of(pageId), FactParameter.of(idForNodeV), FactParameter.of(idForNodeC));
          }

        // descendant(D,A) :- Astart < Dstart,Aend > Dend.
        if (xpathRelations.get(Axis.DESCENDANT.getCode()))
          if ((v.start > c.start) && (v.start < c.end) && (v.end > c.start) && (v.end < c.end)) {
            final Writer destination = writers.get(Axis.DESCENDANT.name());

            serializer.outputFact(destination, config.getString("facts.dom.relations.xpath.descendant"),
                FactParameter.of(pageId), FactParameter.of(idForNodeV), FactParameter.of(idForNodeC));
          }
        // child(C,P) :- C.parent == P.start
        //
        if (xpathRelations.get(Axis.CHILD.getCode()))
          if (v.parent == c.start) {
            final Writer destination = writers.get(Axis.CHILD.name());
            serializer.outputFact(destination, config.getString("facts.dom.relations.xpath.child"),
                FactParameter.of(pageId), FactParameter.of(idForNodeV), FactParameter.of(idForNodeC));
          }
        // only for elements: following(F,X) :- Xend<Fstart
        if (xpathRelations.get(Axis.FOLLOWING.getCode()))
          if ((v.start > c.start) && (v.end > c.end)) {
            final Writer destination = writers.get(Axis.FOLLOWING.name());
            serializer.outputFact(destination, config.getString("facts.dom.relations.xpath.following"),
                FactParameter.of(pageId), FactParameter.of(idForNodeV), FactParameter.of(idForNodeC));
          }
        if (xpathRelations.get(Axis.FOLLOWING_SIBLING.getCode()))
          if ((v.parent == c.parent) && (v.start > c.start) && (v.end > c.end)) {
            final Writer destination = writers.get(Axis.FOLLOWING_SIBLING.name());
            serializer.outputFact(destination, config.getString("facts.dom.relations.xpath.following-sibling"),
                FactParameter.of(pageId), FactParameter.of(idForNodeV), FactParameter.of(idForNodeC));
          }

      }

    }

  }
}
