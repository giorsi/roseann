/**
 * Header
 */
package uk.ac.ox.cs.diadem.domFactsGenerator.modular;

/**
 * Only to Continue the fluent interface returning a {@link GeneralOptionSelector} to continue the configuration
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public interface AndOptions {

  /**
   * Continues the fluent interface returning a general option selector
   * 
   * @return the {@link GeneralOptionSelector} object to continuing specify the configuration
   */
  GeneralOptionSelector and();

}
