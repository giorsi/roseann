package uk.ac.ox.cs.diadem.domFactsGenerator.modular;

public interface ConfBuilder {

  /**
   * Builds the configured fact extraction. Use it to terminate the configuration
   */
  public abstract void build();

}