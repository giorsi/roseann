/*
 * Copyright (c)2011, DIADEM Team
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the DIADEM team nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DIADEM Team BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter;
import uk.ac.ox.cs.diadem.util.misc.EscapingUtils;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNamedNodeMap;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode;
import uk.ac.ox.cs.diadem.webapi.dom.DOMVisitor;

/**
 * 
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
class AttributeVisitorImpl extends AbstractStartEndVisitor {

  private static final String NULL_ATTR_VALUE = "null";

  public static DOMVisitor createAttributeVisitor(final FactSerializerOnWriter serializer, final String context,
      final PredicateWithWriter<? super String> elementSelector,
      final PredicateWithWriter<? super String> attributeSelector) {
    return new AttributeVisitorImpl(serializer, context, elementSelector, attributeSelector);
  }

  @Override
  public boolean filterOut(final DOMNode node) {
    return elementSelectorPredicate.apply(node.getNodeName().toUpperCase());
  }

  final FactSerializerOnWriter serializer;
  static final Logger LOGGER = LoggerFactory.getLogger(AttributeVisitorImpl.class);
  final String pageId;
  private final PredicateWithWriter<? super String> elementSelectorPredicate;
  private final PredicateWithWriter<? super String> attributeSelector;

  private AttributeVisitorImpl(final FactSerializerOnWriter serializer, final String pageId,
      final PredicateWithWriter<? super String> elementSelectorPredicate,
      final PredicateWithWriter<? super String> attributeSelector) {

    this.serializer = serializer;
    this.pageId = pageId;
    this.elementSelectorPredicate = elementSelectorPredicate;
    this.attributeSelector = attributeSelector;
  }

  @Override
  public void startElement(final DOMNode node) {

    addStartLabel(node);
    // if the element is to filter, we don't produce any output
    if (filterOut(node)) {
      LOGGER.debug("Attribute facts skipped for Element {}", node.getNodeName());
      return;
    }

    final DOMNamedNodeMap attributes = node.getAttributes();
    if (attributes == null)
      return;
    final StartEndNodeId ident = getStartEndNodeId(node);
    for (int i = 0; i < attributes.getLength(); i++) {
      // skip the
      if (canFilterOutAttribute(attributes.item(i).getLocalName()))
        continue;

      final List<FactParameter> params = computeParametersForAttribute(node, ident, attributes.item(i));
      // dom__attribute__node(pageId, e_13_p_id, e_13_p, id, "id1" ).
      serializer.outputFact(attributeSelector.getWriterFor(node.getLocalName()),
          config.getString("facts.dom.relations.attribute"), params);
    }
  }

  private boolean canFilterOutAttribute(final String localName) {
    // attribute names are received in input in lowercase
    final String localNameL = localName.toLowerCase();
    return canSkipFakeAttribute(localNameL) || attributeSelector.apply(localNameL);
  }

  protected List<FactParameter> computeParametersForAttribute(final DOMNode node, final StartEndNodeId ident,
      final DOMNode someNodeMethods) {

    final List<FactParameter> params = new LinkedList<FactParameter>();
    params.add(new FactParameter(config.getString("facts.dom.attribute.pageid"), pageId));
    params.add(new FactParameter(config.getString("facts.dom.attribute.id"), ident.formatAttrIdentifier(node,
        someNodeMethods.getNodeName())));
    params.add(new FactParameter(config.getString("facts.dom.attribute.node"), ident.formatElementIdentifier(node)));
    params.add(new FactParameter(config.getString("facts.dom.attribute.tag"), EscapingUtils
        .escapeForDLVConstants(someNodeMethods.getLocalName())));
    final String nodeValue = (someNodeMethods.getNodeValue() == null) ? AttributeVisitorImpl.NULL_ATTR_VALUE
        : someNodeMethods.getNodeValue();
    params.add(new FactParameter(config.getString("facts.dom.attribute.value"), EscapingUtils
        .quoteUnquotedString(EscapingUtils.escapeStringContentForDLV(nodeValue))));
    return params;
  }

  @Override
  public String toString() {
    return super.toString();
  }
}
