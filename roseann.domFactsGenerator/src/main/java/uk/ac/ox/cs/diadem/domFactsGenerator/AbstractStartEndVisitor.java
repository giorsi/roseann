// $codepro.audit.disable checkTypeInEquals, emptyMethod, lossOfPrecisionInCast
/*
 * Copyright (c)2011, DIADEM Team
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the DIADEM team nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DIADEM Team BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.DiademRuntimeException;
import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode.Type;
import uk.ac.ox.cs.diadem.webapi.dom.DOMVisitor;

import com.google.common.collect.Maps;

abstract class AbstractStartEndVisitor implements DOMVisitor {
  /**
   * Logger
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractStartEndVisitor.class);
  protected final Configuration config = ConfigurationFacility.getConfiguration();
  // public static final String MY_ID = "my_id_123";
  // private Map<DOMNode, StartEndNodeId> textNodesLabels;
  // private Map<String, StartEndNodeId> nodeLabels;
  final protected Map<DOMNode, StartEndNodeId> map = Maps.newHashMap();
  long counter;

  @Override
  public void init() {

    // textNodesLabels = Maps.newHashMap();
    // nodeLabels = Maps.newHashMap();
    counter = 0;
  }

  @Override
  public void finish() {
    // do nothing
  }

  @Override
  public boolean filterOut(final DOMNode node) {
    return false;
  }

  @Override
  public void startElement(final DOMNode node) {

    addStartLabel(node);
  }

  protected boolean canSkipFakeAttribute(final String localName) {

    return false;// localName.equals(AbstractStartEndVisitor.MY_ID);
  }

  protected void addStartLabel(final DOMNode node) {

    final DOMNode parentNode = node.getParentNode();
    final StartEndNodeId identifier = getStartEndNodeId(parentNode);
    final long parent = ((parentNode == null) || (identifier == null)) ? getRootLabel() : identifier.start;
    counter = counter + 1;
    final long start = counter;
    final StartEndNodeId ident = new StartEndNodeId(parent, start);
    putStartEndNodeId(node, ident);
  }

  public void labelText(final DOMNode node) {

    final StartEndNodeId identifier = getStartEndNodeId(node.getParentNode());
    final long parent = ((node.getParentNode() == null) || (identifier == null)) ? getRootLabel() : identifier.start;
    counter = counter + 1;
    final long start = counter;
    counter = counter + 1;
    final long end = counter;
    final StartEndNodeId ident = new StartEndNodeId(parent, start, end);
    putStartEndNodeId(node, ident);
  }

  private void putStartEndNodeId(final DOMNode node, final StartEndNodeId ident) {

    if ((node.getNodeType() == Type.ELEMENT) || (node.getNodeType() == Type.DOCUMENT))
      // ((DOMElement) node).setAttribute(AbstractStartEndVisitor.MY_ID, domId + "");
      // nodeLabels.put(domId, ident);
      map.put(node, ident);
    else
      // textNodesLabels.put(node, ident);
      map.put(node, ident);
  }

  protected StartEndNodeId getStartEndNodeId(final DOMNode node) {

    if (node == null)
      return null;
    if (node.getNodeType() == Type.DOCUMENT)
      return null;
    if ((node.getNodeType() == Type.ELEMENT) || (node.getNodeType() == Type.TEXT)) {
      final StartEndNodeId startEndNodeId = map.get(node);
      if (startEndNodeId == null) {
        LOGGER.error("No StartEndNodeId found for node {} althugh it should exists, returning null;",
            node.getNodeName());
        return null;
      }
      // make sure lazy format id is created
      startEndNodeId.id(node);
      return startEndNodeId;
    }
    throw new DiademRuntimeException("Unexpected Node type " + node.getNodeName(), LOGGER);
  }

  private long getRootLabel() {

    // now returns 0... previously was -1
    return 0;
  }

  @Override
  public void endElement(final DOMNode node) {

    updateEndLabel(node);
  }

  public void updateEndLabel(final DOMNode node) {

    final StartEndNodeId ident = getStartEndNodeId(node);
    counter = counter + 1;
    ident.end = counter;
  }

  @Override
  public void visitComment(final DOMNode node) {

  }

  @Override
  public void visitProcessingInstruction(final DOMNode node) {

  }

  @Override
  public void visitText(final DOMNode node) {

    labelText(node);
  }

  @Override
  public String toString() {
    return super.toString();
  }
}
