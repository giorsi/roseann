/**
 * Header
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import java.io.Writer;
import java.util.List;

/**
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public interface FactSerializerOnWriter {

  /**
   * Builds a single fact given relation name and terms, and writes it in the provided writer
   * 
   * @param destination
   * @param relationName
   * @param params
   */
  void outputFact(Writer destination, String relationName, Object... params);

  /**
   * Builds a single fact given relation name and terms, and writes it in the provided writer
   * 
   * @param destination
   * @param relationName
   * @param params
   *          a list of params, ( using toString() )
   */
  void outputFact(Writer destination, String relationName, List<?> params);

}
