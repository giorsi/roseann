// $codepro.audit.disable cyclomaticComplexity
/*
 * Copyright (c)2011, DIADEM Team
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the DIADEM team nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DIADEM Team BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.util.misc.EscapingUtils;
import uk.ac.ox.cs.diadem.webapi.dom.DOMElement;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode.Type;
import uk.ac.ox.cs.diadem.webapi.dom.DOMVisitor;

import com.google.common.collect.Lists;

/**
 * 
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public class DOMWalkerImpl implements DOMWalker {

  /**
   * Creates a {@link DOMWalker} object to traverse the DOM
   * 
   * @param documentElement
   * @param enableGarbageTextNodes
   *          true to include also garbage nodes (whitespaces and <code>&nbsp;</code>)
   * 
   */
  public static DOMWalker createDOMWalker(final DOMElement documentElement, final boolean enableGarbageTextNodes) {

    return new DOMWalkerImpl(documentElement, enableGarbageTextNodes);
  }

  static final Logger LOGGER = LoggerFactory.getLogger(DOMWalkerImpl.class);
  final private List<DOMVisitor> visitors;
  private final DOMElement documentElement;
  private final boolean enableGarbageTextNodes;

  private DOMWalkerImpl(final DOMElement documentElement, final boolean enableGarbageTextNodes) {
    this.documentElement = documentElement;
    this.enableGarbageTextNodes = enableGarbageTextNodes;
    LOGGER.debug("Initializing DOMWalker for URI {} ...", "TODO provide URI");// TODO
    visitors = Lists.newLinkedList();
    LOGGER.debug("   ... DOMWalker initialized.");
  }

  @Override
  public void addVisitor(final DOMVisitor v) {

    visitors.add(v);
  }

  @Override
  public void doWalk() {

    init();
    this.walk(documentElement);
    finish();
  }

  private void walk(final DOMNode node) {

    this.walk(node, 0);
  }

  private void init() {

    for (final DOMVisitor vis : visitors)
      vis.init();
  }

  private void finish() {

    for (final DOMVisitor vis : visitors)
      vis.finish();
  }

  private final void walk(final DOMNode node, final int level) {

    // if (LOGGER.isDebugEnabled())
    // LOGGER.debug("{}> Walking node {} ...", EscapingUtils.indent(level), node.getNodeName() + ", parent:"
    // + node.getParentNode().getNodeName() + " level:" + level);

    final Type nodeType = node.getNodeType();
    switch (nodeType) {
    case ELEMENT:
      for (final DOMVisitor vis : visitors)
        vis.startElement(node);
      for (final DOMNode child : node.getChildNodes())
        walk(child, level + 1);
      for (final DOMVisitor vis : visitors)
        vis.endElement(node);
      break;
    case COMMENT:
      for (final DOMVisitor vis : visitors)
        vis.visitComment(node);
      break;
    case TEXT:
      for (final DOMVisitor vis : visitors)
        // we skip nodes for indentation and white values or nonBreakingSpace
        if (!EscapingUtils.isGarbageText(node.getNodeValue())) // if it's on;y garbage we skip it
          vis.visitText(node);
      break;
    case PROCESSING_INSTRUCTION:
      for (final DOMVisitor vis : visitors)
        vis.visitProcessingInstruction(node);
      break;
    default:
      LOGGER.error("{} unexpected node type {} at node {}", node.getNodeType(), node);
      break;
    }

    // if (LOGGER.isDebugEnabled())
    // LOGGER.debug("{}> ... done walking node {}", EscapingUtils.indent(level), node);
  }

  @Override
  public String toString() {
    return super.toString();
  }
}
