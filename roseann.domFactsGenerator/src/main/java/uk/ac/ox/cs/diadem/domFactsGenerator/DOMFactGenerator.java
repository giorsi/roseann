// $codepro.audit.disable checkTypeInEquals
/*
 * Copyright (c)2011, DIADEM Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the DIADEM team nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DIADEM Team BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.domFactsGenerator.modular.AndOptions;
import uk.ac.ox.cs.diadem.domFactsGenerator.modular.AndorBuildOptions;
import uk.ac.ox.cs.diadem.domFactsGenerator.modular.AttributeOptionSelector;
import uk.ac.ox.cs.diadem.domFactsGenerator.modular.ConfBuilder;
import uk.ac.ox.cs.diadem.domFactsGenerator.modular.CssOptionSelector;
import uk.ac.ox.cs.diadem.domFactsGenerator.modular.CssOptionSelector.CssMoreOptionSelector;
import uk.ac.ox.cs.diadem.domFactsGenerator.modular.GeneralOptionSelector;
import uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter;
import uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWriters;
import uk.ac.ox.cs.diadem.domFactsGenerator.modular.TextOptionSelector;
import uk.ac.ox.cs.diadem.domFactsGenerator.modular.WhatElements;
import uk.ac.ox.cs.diadem.domFactsGenerator.modular.WithOptions;
import uk.ac.ox.cs.diadem.domFactsGenerator.modular.XPathOptionSelector;
import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;
import uk.ac.ox.cs.diadem.util.functions.FunctionUtils.CapitalizeFunction;
import uk.ac.ox.cs.diadem.util.functions.FunctionUtils.UncapitalizeFunction;
import uk.ac.ox.cs.diadem.util.misc.ToStringUtils;
import uk.ac.ox.cs.diadem.webapi.WebAPIRuntimeException;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.DOMCSS2Properties.CssProperty;
import uk.ac.ox.cs.diadem.webapi.dom.finder.DOMNodeFinderService.Score;
import uk.ac.ox.cs.diadem.webapi.dom.finder.XPathFinderByCanonical;
import uk.ac.ox.cs.diadem.webapi.dom.finder.XPathNodePointer;
import uk.ac.ox.cs.diadem.webapi.dom.finder.XPathNodePointer.Type;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory.Engine;
import uk.ac.ox.cs.diadem.webapi.utils.XPathUtil;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * Class to produce modular facts for a web page. Such extraction is highly configurable. For instance, the following
 * produces facts for all dom elements, including facts for XPath, CSS, attributes and text node, all written in
 * separate streams:
 * 
 * <pre>
 * {@code
 *   m.factsFor().allElements(el_writer).with().
 *                      attributes().forAll(attrWriter).and().
 *                      css().allProperties(cssWriter).and().
 *                      text(textWriter).withAttributeClob(textWriter).withElementClob(textWriter);
 * 
 *  m.generateFacts();
 * 
 * }
 * </pre>
 * 
 * It's possible to specify multiple extraction configuration, e.g.:
 * 
 * <pre>
 * {@code
 *   m.factsFor().allElementsExcept("a").writeIn(elementWriter).xpath(xpathWriter);
 *   m.factsFor().onlyElement("a").writeIn(elementWriter).text(textWriter);
 *   m.generateFacts();
 * 
 * }
 * </pre>
 * 
 * After the {@link DOMFactGenerator#generateFacts()} method is executed, the current configuration is reset. This
 * enables to reuse the same object for successive invocations, as:
 * 
 * <pre>
 * {@code
 *   m.factsFor().allElementsExcept(elementWriter,"A");
 *   m.generateFacts();
 *   //here the previous configuration is deleted
 *   //we set a new one and generate the relative facts
 *   m.factsFor().onlyElement(elementWriter,"A");
 *   m.generateFacts();
 * 
 * }
 * </pre>
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public class DOMFactGenerator {

  static final Logger LOGGER = LoggerFactory.getLogger(DOMFactGenerator.class);
  final Configuration CONFIGURATION = ConfigurationFacility.getConfiguration();
  protected WebBrowser webBrowser = null;
  final protected List<ConfigurableFactExtractionItem> extractionSets = Lists.newArrayList();
  protected FactSerializerOnWriter serializer;

  /**
   * Construct a fact generator for the given browser. By default it uses a {@link PrologFactSerializer} to build the
   * facts
   * 
   * @param webBrowser
   * @deprecated use the factory method {@link DOMFactGenerator#newDOMFactGenerator(WebBrowser)} instead.
   */
  @Deprecated
  public DOMFactGenerator(final WebBrowser webBrowser) {

    init(webBrowser, null);
  }

  /**
   * Construct a fact generator for the given browser, using a custom {@link FactSerializerOnWriter} to build the facts
   * 
   * @param webBrowser
   * @param serializer
   * @deprecated use the factory method {@link DOMFactGenerator#newDOMFactGenerator(WebBrowser, FactSerializerOnWriter)}
   *             instead.
   */
  @Deprecated
  public DOMFactGenerator(final WebBrowser webBrowser, final FactSerializerOnWriter serializer) {

    this.webBrowser = webBrowser;
    this.serializer = serializer;
  }

  protected void init(final WebBrowser webBrowser, final FactSerializerOnWriter serializer) {
    this.webBrowser = webBrowser;
    if (serializer == null) {
      this.serializer = new PrologFactSerializer();
    } else {
      this.serializer = serializer;
    }
  }

  /**
   * Using this method to ensure that you get the proper {@link DOMFactGenerator} w.r.t. the webBrowser used.
   * 
   * @param webBrowser the browser to use
   * @return a {@link DOMFactGenerator} suitable for the given browser
   */
  public static DOMFactGenerator newDOMFactGenerator(final WebBrowser webBrowser) {
    if (webBrowser.getEngine() == Engine.WEBDRIVER_FF)
      return new DOMFactGeneratorOnJS(webBrowser);
    return new DOMFactGenerator(webBrowser);

  }

  /**
   * Using this method to ensure that you get the proper {@link DOMFactGenerator} w.r.t. the webBrowser used.
   * 
   * @param webBrowser the browser to use
   * @param serializer serializer for facts
   * @return a {@link DOMFactGenerator} suitable for the given browser
   */
  public static DOMFactGenerator newDOMFactGenerator(final WebBrowser webBrowser,
      final FactSerializerOnWriter serializer) {
    if (webBrowser.getEngine() == Engine.WEBDRIVER_FF)
      return new DOMFactGeneratorOnJS(webBrowser, serializer);
    return new DOMFactGenerator(webBrowser, serializer);

  }

  //
  /**
   * Generates the configured facts, for the current page in the {@link WebBrowser}. As a side effect ,it clears the
   * configuration to be reused in following extractions. It uses a random generator pageId
   */
  public void generateFacts() {

    generateFacts("d" + System.nanoTime());
  }

  /**
   * Utility method which produces ALL possible facts, including garbage nodes
   * 
   * @param destination
   */
  public void dumpAll(final Writer destination) {

    factsFor().allElements(destination).with().attributes().forAll(destination).and().css().allProperties(destination)
        .withBoxes(destination).and().ids(destination).and().text(destination).withElementClob(destination).and()
        .xpath().allRelations(destination).and().rawDOM(destination);
  }

  /**
   * Generates the configured facts, for the current page in the {@link WebBrowser}, using the given pageId. As a side
   * effect ,it clears the configuration to be reused in following extractions.
   */
  public void generateFacts(final String pageId) {
    XPathUtil.enableCache();
    XPathFinderByCanonical.enableCache();
    for (final ConfigurableFactExtractionItem item : extractionSets) {
      final DOMWalker walker = configureDomwalker(item, pageId);
      walker.doWalk();
    }
    extractionSets.clear();
    XPathUtil.invalidateCache();
    XPathFinderByCanonical.invalidateCache();
  }

  /**
   * Entry point to build a configurable fact extraction, using a fluent interface pattern, like:
   * 
   * <pre>
   * {@code
   *   m.factsFor().allElements(el_writer).with().
   *                      attributes().forAll(attrWriter).and().
   *                      css().allProperties(cssWriter).and().
   *                      text(textWriter).withAttributeClob(textWriter).withElementClob(textWriter).
   *                      build();
   * }
   * </pre>
   * 
   * @return an object {@link WhatElements} on which is possible to select which elements to include
   */
  public WhatElements factsFor() {
    final ConfigurableFactExtractionItem e = new ConfigurableFactExtractionItem(extractionSets);
    extractionSets.add(e);
    return e;
  }

  @Override
  public String toString() {
    return ToStringUtils.getInstance().toString(extractionSets);
  }

  // here come the whole logic of the configuration
  private DOMWalker configureDomwalker(final ConfigurableFactExtractionItem item, final String pageId) {
    final DOMWalker walker = DOMWalkerImpl.createDOMWalker(webBrowser.getContentDOMWindow().getDocument()
        .getDocumentElement(), item.enableGarbage);

    PredicateWithWriter<String> elementPredicate = null;

    if (item.allElements) {
      LOGGER.debug("Selected alway accepting predicate for all elements");
      elementPredicate = PredicateWriters.<String> getAlwaysAcceptingFilterOnSingleWriter(item.elementWriter);
    }
    if (item.onlySpecifiedElements) {
      LOGGER.debug("Selected filtering predicate for NOT contained elements");
      elementPredicate = PredicateWriters.<String> getFilterOutIfNotContainedOnSingleWriter(item.selectedElements,
          item.elementWriter);
    }
    if (item.allElementsExceptThoseSpecified) {
      LOGGER.debug("Selected filtering predicate for contained elements");
      elementPredicate = PredicateWriters.<String> getFilterOutIfContainedOnSingleWriter(item.selectedElements,
          item.elementWriter);
    }
    LOGGER.debug("Adding Visitor for elements");

    walker.addVisitor(DOMVisitors.createElementVisitor(serializer, pageId, elementPredicate));

    // walker.addVisitor(DOMVisitors.createFooVisitor());

    // deferred here to make sure text nodes are included
    if (item.generateIds) {
      item.configureIdsExtraction();
    }

    // the selected options
    final Set<FACT_OPTION> options = item.options.keySet();

    for (final FACT_OPTION fact_OPTION : options) {
      switch (fact_OPTION) {
      case IDS:
        LOGGER.debug("Adding Visitor for Ids");
        final PredicateWithWriter<String> idSelector = item.options.get(fact_OPTION);
        walker.addVisitor(DOMVisitors.createElementAndTextVisitor(serializer, pageId, idSelector, item.threshold,
            item.idTypes));
        break;

      case ATTRIBUTE:
        LOGGER.debug("Adding Visitor for attributes");
        final PredicateWithWriter<String> attributeSelector = item.options.get(fact_OPTION);
        walker.addVisitor(DOMVisitors.createAttributeVisitor(serializer, pageId, elementPredicate, attributeSelector));
        break;
      case TEXT:
        LOGGER.debug("Adding Visitor for text nodes");
        final PredicateWithWriter<String> clobSelector = item.options.get(fact_OPTION);
        walker.addVisitor(DOMVisitors.createTextVisitor(serializer, pageId, elementPredicate, clobSelector));
        break;
      case CSS:
        LOGGER.debug("Adding Visitor for CSS cssProperties");
        final PredicateWithWriter<String> cssSelector = item.options.get(fact_OPTION);
        walker.addVisitor(DOMVisitors.createCSSPropertyVisitor(serializer, pageId, elementPredicate, cssSelector));
        break;
      case XPATH:
        LOGGER.debug("Adding Visitor for xpath nodes");
        final PredicateWithWriter<String> xpathSelector = item.options.get(fact_OPTION);
        walker.addVisitor(DOMVisitors
            .createXPathVisitor(serializer, pageId, elementPredicate, xpathSelector, item.text));
        break;
      default:
        break;
      }
    }

    return walker;

  }

  /**
   * Possible MacroOptions
   * 
   * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
   */
  enum FACT_OPTION {
    XPATH, CSS, TEXT, ATTRIBUTE, IDS, JS_PROPERTIES

  }

  /**
   * Inner class to implement a single configurable fact extraction.
   * 
   * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
   */
  protected class ConfigurableFactExtractionItem implements WithOptions, AndOptions, AndorBuildOptions, WhatElements,
      GeneralOptionSelector, TextOptionSelector, XPathOptionSelector, AttributeOptionSelector, CssOptionSelector,
      CssMoreOptionSelector, ConfBuilder {

    /**
     * outer reference to add this object in the list of {@link DOMFactGenerator}
     */
    final List<ConfigurableFactExtractionItem> outerList;
    /**
     * Maps {@link FACT_OPTION} to {@link PredicateWithWriter} which both select what elements to select and where are
     * written
     */

    final Map<FACT_OPTION, PredicateWithWriter<String>> options = Maps.newEnumMap(FACT_OPTION.class);
    /**
     * contains the selected elements in case of {@link ConfigurableFactExtractionItem#allElementsExcept(String...)} and
     * {@link ConfigurableFactExtractionItem#onlyElements(String...)}. For all elements, the list is empty
     * 
     */
    List<String> selectedElements = null;
    /**
     * MAin writer for elements
     */
    Writer elementWriter;
    /**
     * if true, all dom elements are extracted
     */
    boolean allElements = false;
    /**
     * if true, all dom elements except those selected will be part of fact generation
     */
    boolean allElementsExceptThoseSpecified = false;
    /**
     * if true, all the specified elements are part of the fact generation
     */
    boolean onlySpecifiedElements = false;
    // CSS options
    final Map<String, Writer> cssWritersMap = Maps.newHashMap();
    // TEXt visitor Options
    final Map<String, Writer> textWriters = Maps.newHashMap();
    final boolean enableGarbage = false;
    // XPath options
    final Map<String, Writer> xpathWritersMap = Maps.newHashMap();
    // ids options
    boolean generateIds;
    // writer for identifiers
    Writer idsWriter;
    // true is text nodes are generated
    boolean text;
    // dafault is MAX
    Integer threshold = Score.MAX.getThresold();
    // default is the following sequence
    Type[] idTypes = new XPathNodePointer.Type[] { XPathNodePointer.Type.ATTRIBUTE, XPathNodePointer.Type.POSITION,
        XPathNodePointer.Type.ANCHOR, XPathNodePointer.Type.CANONICAL };
    Writer shadowDomWriter = null;
    Writer jsPropertiesWriter = null;

    ConfigurableFactExtractionItem(final List<ConfigurableFactExtractionItem> toProcess) {
      outerList = toProcess;

    }

    @Override
    public WithOptions allElements(final Writer destination) {
      elementWriter = destination;
      allElements = true;
      selectedElements = Collections.<String> emptyList();
      return this;
    }

    @Override
    public WithOptions allElementsExcept(final Writer destination, final String... strings) {
      elementWriter = destination;
      allElementsExceptThoseSpecified = true;
      // we normalize tag names in UPPERCASE
      selectedElements = Lists.transform(Lists.newArrayList(strings), CapitalizeFunction.INSTANCE);
      return this;
    }

    @Override
    public WithOptions onlyElements(final Writer destination, final String... strings) {
      elementWriter = destination;
      onlySpecifiedElements = true;
      // we normalize tag names in UPPERCASE
      selectedElements = Lists.transform(Lists.newArrayList(strings), CapitalizeFunction.INSTANCE);
      return this;
    }

    @Override
    public GeneralOptionSelector with() {
      return this;
    }

    @Override
    public XPathOptionSelector xpath() {
      return this;
    }

    @Override
    public AndorBuildOptions allRelations(final Writer destination) {
      // builds the map from all Axis to the same writer
      final Axis[] values = Axis.values();
      for (final Axis axis : values) {
        xpathWritersMap.put(axis.name(), destination);
      }
      updateXPathMap();
      return this;
    }

    @Override
    public AndorBuildOptions onlyRelations(final Map<Axis, Writer> xpathWriters) {
      final Set<Axis> keySet = xpathWriters.keySet();
      for (final Axis p : keySet) {
        xpathWritersMap.put(p.name(), xpathWriters.get(p));
      }
      updateXPathMap();
      return this;
    }

    @Override
    public AndorBuildOptions onlyRelations(final Writer destination, final Axis... relations) {
      for (final Axis p : relations) {
        xpathWritersMap.put(p.name(), destination);
      }

      updateXPathMap();
      return this;
    }

    private void updateXPathMap() {
      // it's safe to do only once, the inner xpathWritersMap will be updated anyway outside
      if (!options.containsKey(FACT_OPTION.XPATH)) {
        options.put(FACT_OPTION.XPATH,
            PredicateWriters.<String> getFilterOutIfNotContainedOnMultiWriters(xpathWritersMap));
      }
    }

    @Override
    public CssOptionSelector css() {
      return this;
    }

    @Override
    public CssMoreOptionSelector withBoxes(final Writer boxWriter) {
      // put the string as key og the map to indicate boundedboxes to be enabled
      cssWritersMap.put(CONFIGURATION.getString("facts.dom.relations.box"), boxWriter);
      updateCssMap();
      return this;
    }

    @Override
    public CssMoreOptionSelector withCoord(final Writer coordWriter) {
      // put the string as key og the map to indicate coordinates to be enabled
      cssWritersMap.put(CONFIGURATION.getString("facts.dom.relations.visual"), coordWriter);
      updateCssMap();
      return this;
    }

    @Override
    public CssMoreOptionSelector allProperties(final Writer destination) {
      final CssProperty[] values = CssProperty.values();
      for (final CssProperty p : values) {
        cssWritersMap.put(p.getPropertyName(), destination);
      }

      updateCssMap();
      return this;
    }

    private void updateCssMap() {
      // it's safe to do only once, the innner map will be updated anyway
      if (!options.containsKey(FACT_OPTION.CSS)) {
        options.put(FACT_OPTION.CSS, PredicateWriters.<String> getFilterOutIfNotContainedOnMultiWriters(cssWritersMap));
      }
    }

    @Override
    public CssMoreOptionSelector onlyProperties(final Writer destination, final CssProperty... properties) {
      for (final CssProperty p : properties) {
        cssWritersMap.put(p.getPropertyName(), destination);
      }

      updateCssMap();
      return this;
    }

    @Override
    public CssMoreOptionSelector onlyProperties(final Map<CssProperty, Writer> cssWriters) {
      final Set<CssProperty> keySet = cssWriters.keySet();
      for (final CssProperty p : keySet) {
        cssWritersMap.put(p.getPropertyName(), cssWriters.get(p));
      }

      updateCssMap();
      return this;
    }

    @Override
    public CssMoreOptionSelector allPropertiesExcept(final Writer destination, final CssProperty... properties) {
      final List<CssProperty> unwanted = Lists.newArrayList(properties);
      final CssProperty[] allProperties = CssProperty.values();
      for (final CssProperty p : allProperties) {
        if (unwanted.contains(p)) {
          continue;
        }
        cssWritersMap.put(p.getPropertyName(), destination);
      }
      updateCssMap();
      return this;
    }

    @Override
    public TextOptionSelector text(final Writer textwriter) {
      text = true;
      textWriters.put(CONFIGURATION.getString("facts.dom.relations.html-text"), textwriter);
      // it's safe to put and override, we can only add elements to the map
      updateTextMap();
      return this;
    }

    private void updateTextMap() {
      if (!options.containsKey(FACT_OPTION.TEXT)) {
        options.put(FACT_OPTION.TEXT, PredicateWriters.<String> getFilterOutIfNotContainedOnMultiWriters(textWriters));
      }
    }

    // unsupported in the new version, leave commented for backup if we change our mind
    // @Override
    // public TextOptionSelector withAttributeClob(final Writer attributeClobWriter) {
    // textWriters.put(ConfigurationFacility.getConfiguration().getString("facts.dom.relations.clob_attribute"),
    // attributeClobWriter);
    // updateTextMap();
    // return this;
    // }

    // @Override
    // public TextOptionSelector withGarbageNodes() {
    // enableGarbage = true;
    // return this;
    // }

    @Override
    public TextOptionSelector withElementClob(final Writer elementClobWriter) {
      textWriters.put(ConfigurationFacility.getConfiguration().getString("facts.dom.relations.clob-text"),
          elementClobWriter);
      updateTextMap();
      return this;
    }

    @Override
    public void build() {
      // termnates the single configuration and add to the inner list reference
      // outerList.add(this);
      LOGGER.warn("Build method is deprecated, not necessary ");
    }

    @Override
    public GeneralOptionSelector and() {
      return this;
    }

    @Override
    public AndorBuildOptions forAll(final Writer destination) {
      // for all we set an always accepting predicate
      options.put(FACT_OPTION.ATTRIBUTE, PredicateWriters.<String> getAlwaysAcceptingFilterOnSingleWriter(destination));
      return this;
    }

    @Override
    public AndorBuildOptions onlyFor(final Writer destination, final String... attributeNames) {
      // set a predicate to filter out attributes NOT contained in those selected. we normalize attribute names in
      // lowercase
      options.put(
          FACT_OPTION.ATTRIBUTE,
          PredicateWriters.<String> getFilterOutIfNotContainedOnSingleWriter(
              Lists.transform(Lists.newArrayList(attributeNames), UncapitalizeFunction.INSTANCE), destination));
      return this;
    }

    @Override
    public AndorBuildOptions forAllExcept(final Writer destination, final String... attributeNames) {
      // set a predicate to filter out attributes DO contained in those selected. we normalize attribute names in
      // lowercase
      options.put(
          FACT_OPTION.ATTRIBUTE,
          PredicateWriters.<String> getFilterOutIfContainedOnSingleWriter(
              Lists.transform(Lists.newArrayList(attributeNames), UncapitalizeFunction.INSTANCE), destination));
      return this;
    }

    @Override
    public AndorBuildOptions ids(final Writer idsWriter) {
      // deferred configuration to check if text is selected too
      this.idsWriter = idsWriter;
      generateIds = true;
      return this;
    }

    @Override
    public AndorBuildOptions ids(final Writer idsWriter, final Score threshold, final XPathNodePointer.Type... types) {
      return ids(idsWriter, threshold.getThresold(), types);
    }

    @Override
    public AndorBuildOptions ids(final Writer idsWriter, final Integer threshold, final Type... types) {

      // deferred configuration to check if text is selected too
      this.threshold = threshold;
      idTypes = types;
      return ids(idsWriter);
    }

    void configureIdsExtraction() {
      if (allElements) {
        // if also text is required it's a always accepting predicate
        if (text) {
          options.put(FACT_OPTION.IDS, PredicateWriters.<String> getAlwaysAcceptingFilterOnSingleWriter(idsWriter));
        } else {
          // we accept all but text nodes
          options.put(FACT_OPTION.IDS,
              PredicateWriters.<String> getFilterOutIfContainedOnSingleWriter(Lists.newArrayList("#text"), idsWriter));
        }
      } else if (allElementsExceptThoseSpecified) {
        // if also text is required it's ok
        if (text) {
          options.put(
              FACT_OPTION.IDS,
              PredicateWriters.<String> getFilterOutIfContainedOnSingleWriter(
                  Lists.transform(Lists.newArrayList(selectedElements), CapitalizeFunction.INSTANCE), idsWriter));
        } else {// otherwise we add #text in the list
          final ArrayList<String> copy = Lists.newArrayList(selectedElements);
          copy.add("#text");
          options.put(
              FACT_OPTION.IDS,
              PredicateWriters.<String> getFilterOutIfContainedOnSingleWriter(
                  Lists.transform(copy, CapitalizeFunction.INSTANCE), idsWriter));
        }
      } else if (onlySpecifiedElements) {

        final ArrayList<String> copy = Lists.newArrayList(selectedElements);
        // if text is wanted we add to the list
        if (text) {
          copy.add("#text");
        }
        options.put(
            FACT_OPTION.IDS,
            PredicateWriters.<String> getFilterOutIfNotContainedOnSingleWriter(
                Lists.transform(Lists.newArrayList(copy), CapitalizeFunction.INSTANCE), idsWriter));
      }
    }

    @Override
    public AttributeOptionSelector attributes() {
      return this;
    }

    @Override
    public String toString() {
      final StringBuilder b = new StringBuilder();
      b.append("Facts for ");
      if (allElements) {
        b.append("all elements");
      } else if (onlySpecifiedElements) {
        b.append("onlySpecifiedElements for elements: ");
        b.append(ToStringUtils.getInstance().toString(selectedElements));
      } else {
        b.append("all element allElementsExceptThoseSpecified: ");
        b.append(ToStringUtils.getInstance().toString(selectedElements));
      }
      b.append("with options: ");
      b.append(ToStringUtils.getInstance().toString(options.keySet()));

      return b.toString();
    }

    @Override
    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = (prime * result) + ((elementWriter == null) ? 0 : elementWriter.hashCode());
      result = (prime * result) + ((options == null) ? 0 : options.hashCode());
      return result;
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      final ConfigurableFactExtractionItem other = (ConfigurableFactExtractionItem) obj;
      if (elementWriter == null) {
        if (other.elementWriter != null)
          return false;
      } else if (!elementWriter.equals(other.elementWriter))
        return false;
      if (options == null) {
        if (other.options != null)
          return false;
      } else if (!options.equals(other.options))
        return false;
      return true;
    }

    @Override
    public AndorBuildOptions onlyBoundingBoxes(final Writer destination) {

      return withBoxes(destination);
    }

    @Override
    public AndorBuildOptions rawDOM(final Writer shadowDomWriter) {
      if (webBrowser.getEngine() != Engine.WEBDRIVER_FF) {
        LOGGER.error("Unsupported 'allTheRest' feature for the browser '{}'", webBrowser.getEngine());
        throw new WebAPIRuntimeException("Unsupported 'allTheRest' feature for the current browser", LOGGER);
      }
      this.shadowDomWriter = shadowDomWriter;
      return this;
    }

    @Override
    public AndorBuildOptions properties(final Writer jsPropertiesWriter) {
      if (webBrowser.getEngine() != Engine.WEBDRIVER_FF) {
        LOGGER.error("Unsupported 'jsProperties' feature for the browser '{}'", webBrowser.getEngine());
        throw new WebAPIRuntimeException("Unsupported 'jsProperties' feature for the current browser", LOGGER);
      }

      this.jsPropertiesWriter = jsPropertiesWriter;
      return this;
    }
  }

}
