package uk.ac.ox.cs.diadem.domFactsGenerator.modular;

import uk.ac.ox.cs.diadem.domFactsGenerator.DOMFactGenerator;

/**
 * to access further options for the selected elements
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public interface WithOptions extends ConfBuilder {

  /**
   * Continues the fluent interface to configure further
   * 
   * @return a {@link GeneralOptionSelector} to continue the configuration
   */
  GeneralOptionSelector with();

  /**
   * Builds the configured fact extraction. Use it to terminate the configuration
   * 
   * @deprecated not necessary any more to call this method to terminate the configuration. It is added by default on
   *             {@link DOMFactGenerator#factsFor()}
   */
  @Deprecated
  @Override
  public void build();

}