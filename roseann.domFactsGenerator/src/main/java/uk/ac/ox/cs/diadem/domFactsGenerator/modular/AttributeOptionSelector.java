package uk.ac.ox.cs.diadem.domFactsGenerator.modular;

import java.io.Writer;

/**
 * Enable the configuration of facts for attributes
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public interface AttributeOptionSelector {

  /**
   * Specifies the generation of facts for all elements in one single writer
   * 
   * @param destination
   *          destination writer
   * @return an instance of {@link AndOptions} to further configure facts
   */
  AndorBuildOptions forAll(Writer destination);

  /**
   * Specifies the generation of facts for only the provided elements, in one single writer. Attribute names are
   * internally normalized in lowercase (often in browser attributes are lowercase), therefore lowercase is the
   * preferred representation to avoid string transformation.
   * 
   * @param destination
   *          destination writer
   * @param attributeNames
   *          list of attribute names to include in the generation (lowercase).
   * @return an instance of {@link AndOptions} to further configure facts
   */
  AndorBuildOptions onlyFor(Writer destination, String... attributeNames);

  /**
   * Specifies the generation of facts for all elements except those provided, in one single writer. Attribute names are
   * internally normalized in lowercase (often in browser attributes are lowercase), therefore lowercase is the
   * preferred representation to avoid string transformation.
   * 
   * @param destination
   *          destination writer
   * @param attributeNames
   *          list of attribute names to include in the generation (lowercase).
   * @return an instance of {@link AndOptions} to further configure facts
   */
  AndorBuildOptions forAllExcept(Writer destination, String... attributeNames);

}