package uk.ac.ox.cs.diadem.domFactsGenerator.modular;

import java.io.Writer;

import uk.ac.ox.cs.diadem.webapi.dom.finder.DOMNodeFinderService.Score;
import uk.ac.ox.cs.diadem.webapi.dom.finder.XPathNodePointer;
import uk.ac.ox.cs.diadem.webapi.dom.finder.XPathNodePointerRanking;

/**
 * To specify the categories of facts to extract for each element selected.
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public interface GeneralOptionSelector {
  /**
   * To configure XPath facts for the element selected
   * 
   * @return the same object, to implement a fluent interface
   */
  XPathOptionSelector xpath();

  /**
   * To configure CSS facts for the element selected
   * 
   * @param csswriter the destination
   * @return a {@link CssOptionSelector} object, to configure CSS facts
   */
  CssOptionSelector css();

  /**
   * Produces facts for the text nodes of the element selected. In particular, it produces:
   * 
   * <pre>
   * {@code
   * html_text( textnodeId, start, end, parent, pageId ).
   * }
   * </pre>
   * 
   * @param textwriter the destination
   * @return a {@link TextOptionSelector} to further configure the fact extraction
   */
  TextOptionSelector text(final Writer textwriter);

  /**
   * Produces facts for the attributes of the selected element, like
   * 
   * <pre>
   * {@code
   * html_attr( e_13_p_id, e_13_p, id, "value", d1 ).
   * }
   * </pre>
   * 
   * @return a {@link AttributeOptionSelector} to further configure the fact extraction
   */
  AttributeOptionSelector attributes();

  // /**
  // * Generates facts relating opaque node ids (e.g., e_102_div) to more robust ids based on XPath {see @link
  // * {@link XPathNodePointerRanking} given and applied in order of preference
  // *
  // * @param idsWriters
  // * the destination writer
  // * @param types
  // * to apply in order of preference
  // * @return
  // */
  // AndorBuildOptions ids(final Writer idsWriters, XPathNodePointer.Type... types);

  /**
   * Generates facts relating opaque node ids (e.g., e_102_div) to more robust ids based on XPath {see @link
   * {@link XPathNodePointerRanking}. It uses default value for both the Score and the order of application of
   * {@link XPathNodePointer.Type}
   * 
   * @param idsWriters the destination writer
   * @return
   */
  AndorBuildOptions ids(final Writer idsWriters);

  /**
   * Same as {@link GeneralOptionSelector#ids(Writer)} but adding a threshold {@link Score} to specify the mimimum
   * threshold for robustness, and the sequence of types {@link XPathNodePointer.Type} to consider. These are processed
   * in order of preference.
   * 
   * @param idsWriter the destination writer
   * @param threshold
   * @param types
   * @return
   */

  AndorBuildOptions ids(Writer idsWriter, Score threshold, XPathNodePointer.Type... types);

  /**
   * Same as {@link GeneralOptionSelector#ids(Writer)} but adding a threshold to specify the mimimum threshold for
   * robustness, and the sequence of types {@link XPathNodePointer.Type} to consider. These are processed in order of
   * preference.
   * 
   * @param idsWriter the destination writer
   * @param threshold
   * @param types
   * @return
   */

  AndorBuildOptions ids(Writer idsWriter, Integer threshold, XPathNodePointer.Type... types);

  /**
   * Specifies a destination for a copy of all DOM facts, including garbage text nodes, without any normalization
   * 
   * @param rawDomWriter destination
   * @return
   */
  AndorBuildOptions rawDOM(Writer rawDomWriter);

  /**
   * Specifies a destination for facts relative to javascript properties of elements, such as value, href
   * 
   * @param jsPropertiesWriter destination
   * @return
   */
  AndorBuildOptions properties(Writer jsPropertiesWriter);

}