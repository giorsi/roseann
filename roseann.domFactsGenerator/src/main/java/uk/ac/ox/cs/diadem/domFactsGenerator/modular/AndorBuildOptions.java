package uk.ac.ox.cs.diadem.domFactsGenerator.modular;

import uk.ac.ox.cs.diadem.domFactsGenerator.DOMFactGenerator;

/**
 * Only to Continue the fluent interface returning a {@link GeneralOptionSelector} to continue the configuration
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public interface AndorBuildOptions {

  /**
   * Continues the fluent interface returning a general option selector
   * 
   * @return the {@link GeneralOptionSelector} object to continuing specify the configuration
   */
  GeneralOptionSelector and();

  /**
   * Builds the configured fact extraction. Use it to terminate the configuration
   * 
   * @deprecated not necessary any more to call tihs method to terminate the configuration. It is added by default on
   *             {@link DOMFactGenerator#factsFor()}
   */
  @Deprecated
  public void build();

}