/*
 * Copyright (c)2011, DIADEM Team
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the DIADEM team nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DIADEM Team BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import uk.ac.ox.cs.diadem.webapi.dom.DOMVisitor;

/**
 * Allows to walk a DOM tree by using a set {@link DOMVisitor}
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 * 
 */
public interface DOMWalker {

  /**
   * 
   * @param visitor
   */
  void addVisitor(DOMVisitor visitor);

  /**
   * Starts walking the DOM tree, following the algorithm:
   * 
   * <pre>
   * {@code
   * for (final DOMVisitor vis : visitors)
   *    vis.init();
   *    
   * walk(documentElement);
   * 
   * for (final DOMVisitor vis : visitors)
   *   vis.finish();
   *   
   * }
   * </pre>
   * 
   * where the walk function performs recursively:
   * 
   * <pre>
   * {@code
   *  void walk(final DOMNode node, final int level){
   *     switch (node.getNodeType()) {
   *     case ELEMENT:
   *       for (final DOMVisitor vis : visitors)
   *         vis.startElement(node);
   *       
   *       final DOMNodeList childNodes = node.getChildNodes();
   *       
   *       for (final DOMNode child : childNodes)
   *         walk(child, level + 1);
   *       
   *       for (final DOMVisitor vis : visitors)
   *         vis.endElement(node);
   *       break;
   *     
   *     case TEXT:
   *       for (final DOMVisitor vis : visitors)
   *         vis.visitText(node);
   *       break;
   *     
   *     case COMMENT:
   *       for (final DOMVisitor vis : visitors)
   *         vis.visitComment(node);
   *       break;
   *     
   *     case PROCESSING_INSTRUCTION:
   *       for (final DOMVisitor vis : visitors)
   *         vis.visitProcessingInstruction(node);
   *       break;
   *     
   *     default:...
   *    }
   * }
   * }
   * </pre>
   * 
   * 
   */
  void doWalk();
}
