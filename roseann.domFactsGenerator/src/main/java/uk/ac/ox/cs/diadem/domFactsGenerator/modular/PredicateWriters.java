/**
 * Header
 */
package uk.ac.ox.cs.diadem.domFactsGenerator.modular;

import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Predicate;
import com.google.common.collect.ForwardingMap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

/**
 * Factory class for predifined {@link PredicateWithWriter} objects.
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public class PredicateWriters {

  private static final Logger LOGGER = LoggerFactory.getLogger(PredicateWriters.class);

  private PredicateWriters() {
    // prevents instantiation
  }

  /**
   * Returns a {@link PredicateWithWriter} whose method {@link Predicate#apply(Object)} returns <code>true</code> if the
   * input object has no mapping in the given map, <code>false</code> otherwise. Also, it provides a {@link Writer} for
   * each contained object.
   * 
   * @param writers
   *          maps objects to writers and it's used to check contaiment too.
   * @return a {@link PredicateWithWriter} whose method {@link Predicate#apply(Object)} returns <code>true</code> if the
   *         input object has no mappings in the given map, <code>false</code> otherwise
   */
  public static <T> PredicateWithWriter<T> getFilterOutIfNotContainedOnMultiWriters(final Map<T, Writer> writers) {
    return new FilterIfNotContainedPredicateOnMultiWriters<T>(writers);
  }

  /**
   * Returns a {@link PredicateWithWriter} whose method {@link Predicate#apply(Object)} returns <code>true</code> if the
   * input object has a mapping in the given map, <code>false</code> otherwise. Also, it provides a {@link Writer} for
   * each contained object.
   * 
   * @param writers
   *          maps objects to writers and it's used to check contaiment too.
   * @return a {@link PredicateWithWriter} whose method {@link Predicate#apply(Object)} returns <code>true</code> if the
   *         input object has no mappings in the given map, <code>false</code> otherwise
   */
  public static <T> PredicateWithWriter<T> getFilterIfContainedOnMultiWriters(final Map<T, Writer> writers) {
    return new FilterIfContainedPredicateOnMultiWriters<T>(writers);
  }

  /**
   * Returns a {@link PredicateWithWriter} whose method {@link Predicate#apply(Object)} returns <code>true</code> if the
   * input object is contained in the provided list, <code>false</code> otherwise. Also, it provides a single
   * {@link Writer}
   * 
   * @param elements
   *          reference elements
   * @param writer
   *          the destination
   * @return a {@link PredicateWithWriter} whose method {@link Predicate#apply(Object)} returns <code>true</code> if the
   *         input object has no mappings in the given map, <code>false</code> otherwise
   */
  public static <T> PredicateWithWriter<T> getFilterOutIfContainedOnSingleWriter(final List<T> elements,
      final Writer writer) {
    return new FilterIfContainedPredicateOnSingleWriter<T>(elements, writer);
  }

  /**
   * Returns a {@link PredicateWithWriter} whose method {@link Predicate#apply(Object)} returns <code>true</code> if the
   * input object is NOT contained in the provided list, <code>false</code> otherwise. Also, it provides a single
   * {@link Writer}
   * 
   * @param elements
   *          reference elements
   * @param writer
   *          the destination
   * @return a {@link PredicateWithWriter} whose method {@link Predicate#apply(Object)} returns <code>true</code> if the
   *         input object has no mappings in the given map, <code>false</code> otherwise
   */
  public static <T> PredicateWithWriter<T> getFilterOutIfNotContainedOnSingleWriter(final List<T> selectedElements,
      final Writer writer) {
    return new FilterIfNotContainedPredicateOnSingleWriter<T>(selectedElements, writer);
  }

  /**
   * Returns a {@link PredicateWithWriter} whose method {@link Predicate#apply(Object)} returns always
   * <code>false</code>. Also, it provides a single {@link Writer}
   * 
   * @param writer
   *          the destination
   * @return a {@link PredicateWithWriter} whose method {@link Predicate#apply(Object)} returns always
   *         <code>false</code>.
   */
  public static <T> PredicateWithWriter<T> getAlwaysAcceptingFilterOnSingleWriter(final Writer writer) {
    return new AlwaysAcceptingOnSingleWriter<T>(writer);
  }

  /**
   * Returns a {@link PredicateWithWriter} whose method {@link Predicate#apply(Object)} returns always
   * <code>false</code>. Also, it provides a mapping for {@link Writer} objects.
   * 
   * @param writer
   *          maps objects to writers
   * @return a {@link PredicateWithWriter} whose method {@link Predicate#apply(Object)} returns always
   *         <code>false</code>.
   */
  public static <T> PredicateWithWriter<T> getAlwaysAcceptingFilterOnMultipleWriters(final Map<T, Writer> writers) {
    return new AlwaysAcceptingOnMultipleWriters<T>(writers);
  }

  // actual implementation
  private static final class FilterIfNotContainedPredicateOnMultiWriters<T> implements PredicateWithWriter<T> {
    private final Map<T, Writer> writers;

    private FilterIfNotContainedPredicateOnMultiWriters(final Map<T, Writer> writers) {
      this.writers = writers;
    }

    @Override
    public boolean apply(final T input) {
      // filter out is the specified elements do not contains the input node
      return !writers.containsKey(input);
    }

    @Override
    public Writer getWriterFor(final String name) {
      return writers.get(name);
    }

    @Override
    public Map<T, Writer> asMap() {
      return writers;
    }

    @Override
    public uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter.ACCEPTANCE type() {
      return ACCEPTANCE.INCLUDE;
    }
  }

  private static final class FilterIfNotContainedPredicateOnSingleWriter<T> implements PredicateWithWriter<T> {
    private final List<T> selectedElements;
    private final Writer singleWriter;

    private FilterIfNotContainedPredicateOnSingleWriter(final List<T> selectedElements, final Writer singleWriter) {
      this.selectedElements = selectedElements;

      this.singleWriter = singleWriter;
    }

    @Override
    public boolean apply(final T input) {
      // filter out is the specified elements do not contains the input node
      return !selectedElements.contains(input);
    }

    @Override
    public Writer getWriterFor(final String name) {
      return singleWriter;
    }

    @Override
    public Map<T, Writer> asMap() {
      final HashMap<T, Writer> map = Maps.newHashMap();
      for (final T t : selectedElements)
        map.put(t, singleWriter);
      return map;
    }

    @Override
    public uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter.ACCEPTANCE type() {
      return ACCEPTANCE.INCLUDE;
    }
  }

  private static final class FilterIfContainedPredicateOnMultiWriters<T> implements PredicateWithWriter<T> {
    private final Map<T, Writer> writers;

    private FilterIfContainedPredicateOnMultiWriters(final Map<T, Writer> writers) {
      this.writers = writers;
    }

    @Override
    public boolean apply(final T input) {
      // filter out is the specified elements does contain the input node
      return writers.containsKey(input);
    }

    @Override
    public Writer getWriterFor(final String name) {
      return writers.get(name);
    }

    @Override
    public Map<T, Writer> asMap() {
      return writers;
    }

    @Override
    public uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter.ACCEPTANCE type() {
      return ACCEPTANCE.EXCLUDE;
    }
  }

  private static final class FilterIfContainedPredicateOnSingleWriter<T> implements PredicateWithWriter<T> {
    private final List<T> selectedElements;
    private final Writer singleWriter;

    private FilterIfContainedPredicateOnSingleWriter(final List<T> selectedElements, final Writer singleWriter) {
      this.selectedElements = selectedElements;
      this.singleWriter = singleWriter;
    }

    @Override
    public boolean apply(final T input) {
      // filter out is the specified elements does contain the input node
      return selectedElements.contains(input);
    }

    @Override
    public Writer getWriterFor(final String name) {
      return singleWriter;
    }

    @Override
    public Map<T, Writer> asMap() {
      final HashMap<T, Writer> map = Maps.newHashMap();
      for (final T t : selectedElements)
        map.put(t, singleWriter);
      return map;
    }

    @Override
    public uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter.ACCEPTANCE type() {
      return ACCEPTANCE.EXCLUDE;
    }
  }

  private static final class AlwaysAcceptingOnSingleWriter<T> implements PredicateWithWriter<T> {

    private final Writer destination;

    private AlwaysAcceptingOnSingleWriter(final Writer destination) {
      this.destination = destination;
    }

    @Override
    public boolean apply(final T input) {
      // never filter out
      return false;
    }

    @Override
    public Writer getWriterFor(final String name) {
      // always the same writer
      return destination;
    }

    @Override
    public Map<T, Writer> asMap() {
      return new MapOnSingleElement<T, Writer>(destination);
    }

    @Override
    public uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter.ACCEPTANCE type() {
      return ACCEPTANCE.ALLIN;
    }

    private class MapOnSingleElement<K, V> extends ForwardingMap<K, V> {

      private final Map<K, V> delegate = ImmutableMap.of();
      private final V destinationWriter;

      public MapOnSingleElement(final V destinationWriter) {
        this.destinationWriter = destinationWriter;
      }

      @Override
      protected Map<K, V> delegate() {
        return delegate;
      }

      @Override
      public V get(final Object key) {
        return destinationWriter;
      }

      @Override
      public boolean containsKey(final Object key) {
        return true;
      }
    }
  }

  private static final class AlwaysAcceptingOnMultipleWriters<T> implements PredicateWithWriter<T> {

    private final Map<T, Writer> writers;

    private AlwaysAcceptingOnMultipleWriters(final Map<T, Writer> writers) {
      this.writers = writers;
    }

    @Override
    public boolean apply(final T input) {
      // never filter out
      return false;
    }

    @Override
    public Writer getWriterFor(final String name) {
      return writers.get(name);
    }

    @Override
    public Map<T, Writer> asMap() {
      return writers;
    }

    @Override
    public uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter.ACCEPTANCE type() {
      return ACCEPTANCE.ALLIN;
    }
  }

  /**
   * Only for backcompatibility, to be removed
   * 
   * @return
   */
  public static PredicateWithWriter<? super String> getAlwaysAcceptingPredicateOnDummyWriter() {
    return new PredicateWithWriter<String>() {

      @Override
      public boolean apply(final String input) {
        // no filter by default
        return false;
      }

      @Override
      public Writer getWriterFor(final String name) {
        LOGGER.debug("here only for backcompatibility");
        return null;
      }

      @Override
      public Map<String, Writer> asMap() {
        return ImmutableMap.of();
      }

      @Override
      public uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter.ACCEPTANCE type() {
        return ACCEPTANCE.ALLIN;
      }

    };
  }
}
