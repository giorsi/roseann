package uk.ac.ox.cs.diadem.domFactsGenerator.modular;

import java.io.Writer;
import java.util.Map;

import com.google.common.base.Predicate;

/**
 * Used as selector. It extends {@link Predicate} to provide a mapping for {@link Writer}
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public interface PredicateWithWriter<T> extends Predicate<T> {
  public enum ACCEPTANCE {
    ALLIN, INCLUDE, EXCLUDE
  }

  /**
   * Returns the writer associated with the given name,if any. It will return <code>null</code> if not found
   * 
   * @param name
   * @return the writer associated with the given name,if any, <code>null</code> otherwise
   */
  Writer getWriterFor(String name);

  Map<T, Writer> asMap();

  ACCEPTANCE type();
}