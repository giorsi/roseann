/**
 * Header
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode;
import uk.ac.ox.cs.diadem.webapi.dom.DOMVisitor;
import uk.ac.ox.cs.diadem.webapi.dom.finder.XPathNodePointer.Type;

/**
 * Factory for {@link DOMVisitor} objects
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public class DOMVisitors {

  private DOMVisitors() {
    // prevent instantiation
  }

  public static DOMVisitor createTextVisitor(final FactSerializerOnWriter serializer, final String context,
      final PredicateWithWriter<? super String> elementSelectorPredicate,
      final PredicateWithWriter<? super String> clobSelectorPredicate) {
    return TextVisitorImpl.createTextVisitor(serializer, context, elementSelectorPredicate, clobSelectorPredicate);
  }

  public static DOMVisitor createFooVisitor() {
    return new DOMVisitor() {

      @Override
      public void visitText(final DOMNode node) {

      }

      @Override
      public void visitProcessingInstruction(final DOMNode node) {

      }

      @Override
      public void visitComment(final DOMNode node) {

      }

      @Override
      public void startElement(final DOMNode node) {

      }

      @Override
      public void init() {

      }

      @Override
      public void finish() {

      }

      @Override
      public boolean filterOut(final DOMNode node) {
        return false;
      }

      @Override
      public void endElement(final DOMNode node) {

      }
    };
  }

  public static DOMVisitor createElementVisitor(final FactSerializerOnWriter serializer, final String context,
      final PredicateWithWriter<? super String> elementSelectorPredicate) {
    return ElementVisitorImpl.createElementVisitor(serializer, context, elementSelectorPredicate);
  }

  public static DOMVisitor createCSSPropertyVisitor(final FactSerializerOnWriter serializer, final String context,
      final PredicateWithWriter<? super String> elementSelectorPredicate,
      final PredicateWithWriter<? super String> cssSelectorPredicate) {
    return CSSVisitorImpl.createCSSVisitor(serializer, context, elementSelectorPredicate, cssSelectorPredicate);
  }

  public static DOMVisitor createAttributeVisitor(final FactSerializerOnWriter serializer, final String context,
      final PredicateWithWriter<? super String> elementSelector,
      final PredicateWithWriter<? super String> attributeSelector) {
    return AttributeVisitorImpl.createAttributeVisitor(serializer, context, elementSelector, attributeSelector);
  }

  public static DOMVisitor createXPathVisitor(final FactSerializerOnWriter serializer, final String context,
      final PredicateWithWriter<? super String> elementSelecto,
      final PredicateWithWriter<? super String> xpathSelector, final boolean textNodes) {
    return new XPathVisitor(serializer, context, elementSelecto, xpathSelector, textNodes);
  }

  public static DOMVisitor createElementAndTextVisitor(final FactSerializerOnWriter serializer, final String context,
      final PredicateWithWriter<? super String> elementSelectorPredicate, final Integer threshold,
      final Type[] xpathPointerTypes) {
    return NodeIdVisitorImpl.createElementVisitor(serializer, context, elementSelectorPredicate, threshold,
        xpathPointerTypes);
  }

}
