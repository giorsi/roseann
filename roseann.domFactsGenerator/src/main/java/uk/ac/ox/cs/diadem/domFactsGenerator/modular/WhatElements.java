package uk.ac.ox.cs.diadem.domFactsGenerator.modular;

import java.io.Writer;

/**
 * Offers the ability to select set of elements to produce facts for
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public interface WhatElements extends WithOptions {
  /**
   * Specifies that the extraction is related to all the (dom) elements.
   * 
   * @param destination
   *          the output destination
   * @return a {@link WithOptions} object to further configure facts generation
   */
  WithOptions allElements(Writer destination);

  /**
   * Specifies that facts are generated for all (dom) elements excluding those provided. Elements names are internally
   * normalized as upperCase (to match browser representation). Therefore, to avoid unnecessary string conversion, tags
   * should be listed in uppercase.
   * 
   * @param tags
   *          the list on tags to exclude (uppercase)
   * @return a {@link WithOptions} object to further configure facts generation
   */
  WithOptions allElementsExcept(Writer destination, final String... tags);

  /**
   * Specifies that facts are generated for only the provided (dom) elements. Elements names are internally normalized
   * as upperCase (to match browser representation). Therefore, to avoid unnecessary string conversion, tags should be
   * listed in uppercase.
   * 
   * @param tags
   *          the list on tags to exclude (uppercase)
   * @return a {@link WithOptions} object to further configure facts generation
   */
  WithOptions onlyElements(Writer destination, final String... tags);

}