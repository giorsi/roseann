/*
 * Copyright (c)2011, DIADEM Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the DIADEM team nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DIADEM Team BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter;
import uk.ac.ox.cs.diadem.env.DiademRuntimeException;
import uk.ac.ox.cs.diadem.util.misc.EscapingUtils;
import uk.ac.ox.cs.diadem.webapi.WebAPIRuntimeException;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode.Type;
import uk.ac.ox.cs.diadem.webapi.dom.DOMVisitor;
import uk.ac.ox.cs.diadem.webapi.dom.finder.DOMNodeFinderService;
import uk.ac.ox.cs.diadem.webapi.dom.finder.FinderUtils;
import uk.ac.ox.cs.diadem.webapi.dom.finder.XPathNodePointer;
import uk.ac.ox.cs.diadem.webapi.dom.finder.XPathNodePointerRanking;
import uk.ac.ox.cs.diadem.webapi.dom.finder.XPathNodePointerRankingOnSet;

import com.google.common.base.Stopwatch;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;

class NodeIdVisitorImpl extends AbstractStartEndVisitor {

  Cache<DOMNode, XPathNodePointer> cache = CacheBuilder.newBuilder().maximumSize(100000).build();

  public static DOMVisitor createElementVisitor(final FactSerializerOnWriter serializer, final String context,
      final PredicateWithWriter<? super String> predicate, final Integer threshold,
      final XPathNodePointer.Type[] xpathPointerTypes) {
    return new NodeIdVisitorImpl(serializer, context, predicate, threshold, xpathPointerTypes);
  }

  @Override
  public boolean filterOut(final DOMNode node) {
    return elementSelector.apply(node.getNodeName().toUpperCase());
  }

  final FactSerializerOnWriter serializer;
  static final Logger LOGGER = LoggerFactory.getLogger(NodeIdVisitorImpl.class);
  final String pageId;
  private final PredicateWithWriter<? super String> elementSelector;
  private final Integer threshold;
  private final XPathNodePointer.Type[] xpathPointerTypes;

  private NodeIdVisitorImpl(final FactSerializerOnWriter serializer, final String context,
      final PredicateWithWriter<? super String> elementSelectorPredicate, final Integer threshold,
      final XPathNodePointer.Type[] xpathPointerTypes) {
    super();
    this.serializer = serializer;
    pageId = context;
    elementSelector = elementSelectorPredicate;
    this.threshold = threshold;
    this.xpathPointerTypes = xpathPointerTypes;
  }

  @Override
  public void endElement(final DOMNode node) {
    updateEndLabel(node);
    final StartEndNodeId ident = getStartEndNodeId(node);
    final String identifier = ident.formatElementIdentifier(node);
    if (filterOut(node)) {
      LOGGER.debug("skipped Id generation Facts for Element {}", node.getNodeName());
      return;
    }
    serialize(node, identifier);
  }

  private void serialize(final DOMNode node, final String identifier) {
    LOGGER.debug("Serializing {} hashcode {}", node, node.hashCode());
    // if the element is to filter, we don't produce any output

    final List<FactParameter> params = Lists.newLinkedList();
    params.add(new FactParameter(config.getString("facts.dom.node-id.pageid"), pageId));
    params.add(new FactParameter(config.getString("facts.dom.node-id.idnode"), identifier));

    // If the key wasn't in the "easy to compute" group, we need to
    // do things the hard way.
    try {

      XPathNodePointer best = null;
      if (node.getNodeType() == Type.TEXT) {
        best = querycache(node.getParentNode());
        // manage text nodes
        best = FinderUtils.manageTextNodeIfAny(node, best);
      } else {
        best = querycache(node);
      }

      // final XPathNodePointer best = getTopAmongAll(node);
      // LOGGER.info("Best pointer {} of type {}, score {}",
      // new Object[] { best.getXPath(), best.getType(), best.getScore() });

      params.add(new FactParameter(config.getString("facts.dom.node-id.pointer"), EscapingUtils
          .quoteUnquotedString(EscapingUtils.escapeStringContentForDLV(best.getXPath()))));
      final String factName = config.getString("facts.dom.relations.node-id");
      // dom__element__node( pageId, e_13_p, 13, 16, 8, p ).
      serializer.outputFact(elementSelector.getWriterFor(node.getLocalName()), factName, params);
    } catch (final ExecutionException e) {
      LOGGER.debug("Error '{}' generating id for node {}.", e.getMessage(), node);
      throw new DiademRuntimeException("error  generating id for node: " + node, e, LOGGER);
    }
  }

  private XPathNodePointer querycache(final DOMNode node) throws ExecutionException {
    XPathNodePointer best;
    if (cache.getIfPresent(node) == null) {
      LOGGER.debug("no cached XPathNodePointer for {}", node);
    } else {
      LOGGER.debug("Found cached XPathNodePointer for {}", node);
    }
    best = cache.get(node, new Callable<XPathNodePointer>() {
      @Override
      public XPathNodePointer call() {
        final Stopwatch w = Stopwatch.createUnstarted();
        w.start();
        final XPathNodePointer bestEffort = getTopBestEffort(node);
        w.stop();
        LOGGER.debug("retrieved best pointer of type {}, for {}, score {}, in {}ms",
            new Object[] { bestEffort.getType(), node, bestEffort.getScore(), w.elapsed(TimeUnit.MILLISECONDS) });

        return bestEffort;
      }
    });

    return best;
  }

  // private XPathNodePointer getTopBestEffort(final DOMNode node) {
  //
  // final Stopwatch stopwatch = new Stopwatch();
  // XPathNodePointer bestSoFar = null;
  // stopwatch.start();
  //
  // XPathNodePointerRanking p = DOMNodeFinderService.getXPathPointersByAttribute(node, threshold);
  // stopwatch.stop();
  // if (FinderUtils.checkThreshold(p, threshold))
  // return p.first();
  // else {
  // bestSoFar = updateBest(p, bestSoFar);
  // stopwatch.start();
  // p = DOMNodeFinderService.getXPathPointersByPosition(node, threshold);
  // LOGGER.info("Retrieved {} xpath pointers for {} by POSITION in {} ms, best {}", new Object[] { p.size(), node,
  // stopwatch.elapsedMillis(), p.isEmpty() ? null : p.first().getScore() });
  // stopwatch.reset();
  // }
  //
  // if (FinderUtils.checkThreshold(p, threshold))
  // return p.first();
  // else {
  // bestSoFar = updateBest(p, bestSoFar);
  // stopwatch.start();
  // p = DOMNodeFinderService.getXPathPointersByAnchor(node, threshold);
  // stopwatch.stop();
  // LOGGER.info("Retrieved {} xpath pointers by ANCHOR in {} ms, best {}",
  // new Object[] { p.size(), stopwatch.elapsedMillis(), p.isEmpty() ? null : p.first().getScore() });
  // stopwatch.reset();
  // // store the best so far
  // }
  //
  // if (FinderUtils.checkThreshold(p, threshold))
  // return p.first();
  // else {
  // bestSoFar = updateBest(p, bestSoFar);
  // final XPathNodePointer canonicalXPath = DOMNodeFinderService.getCanonicalXPath(node);
  // if ((bestSoFar != null) && (bestSoFar.getScore() > canonicalXPath.getScore()))
  // return bestSoFar;
  // else
  // return canonicalXPath;
  // }
  // }

  private XPathNodePointer getTopBestEffort(final DOMNode node) {
    if (xpathPointerTypes.length == 0)
      throw new WebAPIRuntimeException("Specify at least one XPathNodePointer.Type to compute locators", LOGGER);

    final Stopwatch stopwatch = Stopwatch.createUnstarted();
    XPathNodePointer bestSoFar = null;
    for (final XPathNodePointer.Type type : xpathPointerTypes) {

      stopwatch.start();
      final XPathNodePointerRanking p = computePointersBy(type, node);
      stopwatch.stop();
      LOGGER.debug("Retrieved {} xpath pointers for {} by {} in {} ms, best {}", new Object[] { p.size(), node, type,
          stopwatch.elapsed(TimeUnit.MILLISECONDS), p.isEmpty() ? null : p.first().getScore() });
      stopwatch.reset();
      if (FinderUtils.checkThreshold(p, threshold))
        return p.first();
      else {
        bestSoFar = updateBest(p, bestSoFar);
      }
    }
    if (bestSoFar != null)
      return bestSoFar;
    else
      return DOMNodeFinderService.getCanonicalXPath(node);

  }

  private XPathNodePointerRanking computePointersBy(
      final uk.ac.ox.cs.diadem.webapi.dom.finder.XPathNodePointer.Type type, final DOMNode node) {
    switch (type) {
    case ATTRIBUTE:
      return DOMNodeFinderService.getXPathPointersByAttribute(node, threshold);
    case POSITION:
      return DOMNodeFinderService.getXPathPointersByPosition(node, threshold);
    case ANCHOR:
      return DOMNodeFinderService.getXPathPointersByAnchor(node, threshold);
    case TEXT:
      return DOMNodeFinderService.getXPathPointersByTextContent(node, threshold);
    case CANONICAL:
      final XPathNodePointerRankingOnSet rank = XPathNodePointerRankingOnSet.newRank();
      rank.add(DOMNodeFinderService.getCanonicalXPath(node));
      return rank;

    default:
      throw new WebAPIRuntimeException("Unsupported XPathNodePointer.Type " + type, LOGGER);
    }
  }

  private XPathNodePointer updateBest(final XPathNodePointerRanking p, final XPathNodePointer bestSoFar) {
    if (p.isEmpty())
      return bestSoFar;
    final XPathNodePointer first = p.first();
    if (bestSoFar == null)
      return first;
    if (bestSoFar.getScore() >= first.getScore())
      return bestSoFar;
    else
      return first;
  }

  @Override
  public void visitText(final DOMNode node) {
    labelText(node);
    final StartEndNodeId ident = getStartEndNodeId(node);
    final String identifier = ident.formatTextIdentifier(node);
    if (filterOut(node.getParentNode())) {
      LOGGER.debug("skipped Id generation Facts for Element {}", node.getNodeName());
      return;
    }

    serialize(node, identifier);
  }

  @Override
  public void finish() {
    // System.out.println(cache.stats());
  }

  @Override
  public String toString() {
    return super.toString();
  }
}
