// $codepro.audit.disable checkTypeInEquals
package uk.ac.ox.cs.diadem.domFactsGenerator;

/**
 * 
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
final class FactParameter {

  final String role;
  final String value;

  public FactParameter(final String r, final String v) {

    this.role = r;
    this.value = v;
  }

  public static FactParameter of(final String value) {
    return new FactParameter("", value);
  }

  public static FactParameter of(final String role, final String value) {
    return new FactParameter(role, value);
  }

  @Override
  public int hashCode() {

    final int prime = 31;
    int result = 1;
    result = (prime * result) + ((role == null) ? 0 : role.hashCode());
    result = (prime * result) + ((value == null) ? 0 : value.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {

    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    final FactParameter other = (FactParameter) obj;
    if (role == null) {
      if (other.role != null)
        return false;
    } else if (!role.equals(other.role))
      return false;
    if (value == null) {
      if (other.value != null)
        return false;
    } else if (!value.equals(other.value))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return role + " : " + value;
  }
}
