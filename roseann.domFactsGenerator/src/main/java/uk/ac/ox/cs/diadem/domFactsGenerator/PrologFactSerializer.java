package uk.ac.ox.cs.diadem.domFactsGenerator;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.env.DiademRuntimeException;
import uk.ac.ox.cs.diadem.env.configuration.Configuration;
import uk.ac.ox.cs.diadem.env.configuration.ConfigurationFacility;

import com.google.common.collect.Lists;

/**
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public class PrologFactSerializer implements FactSerializerOnWriter {

  private static final Logger LOGGER = LoggerFactory.getLogger(PrologFactSerializer.class);
  final Configuration config = ConfigurationFacility.getConfiguration();

  public PrologFactSerializer() {

  }

  private void writeInWriter(final Writer dest, final String relationName, final List<?> params) {
    if (dest == null) {
      LOGGER.error("No writer specified for outputting facts for predicate {}", relationName);
      throw new DiademRuntimeException("No writer specified for outputting facts for predicate " + relationName, LOGGER);
    }

    try {
      dest.write(config.getString("facts.dom.output.fact.prolog"));
      dest.write(relationName);
      dest.write(config.getString("facts.dom.output.fact.postname"));
      final boolean outputRoles = config.getBoolean("facts.dom.output.fact.roles");
      int counter = 0;
      for (final Object param : params) {
        if (outputRoles) {
          dest.write(getRole(param));
          dest.write(config.getString("facts.dom.output.fact.postrole"));
        }
        dest.write(getValue(param));
        if (++counter < params.size())
          dest.write(config.getString("facts.dom.output.fact.paramsep"));
      }
      dest.write(config.getString("facts.dom.output.fact.postparams"));
      dest.write(config.getString("facts.dom.output.fact.epilog"));
      dest.write(IOUtils.LINE_SEPARATOR);
    } catch (final IOException e) {
      LOGGER.error("Error in writing output facts {}", relationName);
      throw new DiademRuntimeException("Error in writing output facts", e, LOGGER);
    }

  }

  private String getRole(final Object param) {
    if (param instanceof FactParameter)
      return ((FactParameter) param).role;
    return "";
  }

  private String getValue(final Object param) {
    if (param instanceof FactParameter)
      return ((FactParameter) param).value;
    return param.toString();
  }

  @Override
  public void outputFact(final Writer destination, final String relationName, final Object... params) {
    writeInWriter(destination, relationName, Lists.newArrayList(params));
  }

  @Override
  public void outputFact(final Writer destination, final String relationName, final List<?> params) {
    writeInWriter(destination, relationName, params);
  }

  @Override
  public String toString() {
    return super.toString();
  }
}
