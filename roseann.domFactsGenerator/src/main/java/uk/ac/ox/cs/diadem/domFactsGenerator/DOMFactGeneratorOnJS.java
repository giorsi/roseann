// $codepro.audit.disable checkTypeInEquals
/*
 * Copyright (c)2011, DIADEM Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the DIADEM team nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DIADEM Team BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter;
import uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter.ACCEPTANCE;
import uk.ac.ox.cs.diadem.domFactsGenerator.modular.XPathOptionSelector.Axis;
import uk.ac.ox.cs.diadem.env.DiademRuntimeException;
import uk.ac.ox.cs.diadem.util.misc.EscapingUtils;
import uk.ac.ox.cs.diadem.webapi.WebBrowser;
import uk.ac.ox.cs.diadem.webapi.dom.DOMCSS2Properties.CssProperty;
import uk.ac.ox.cs.diadem.webapi.dom.impl.BrowserFactory;

import com.google.common.base.Charsets;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.io.Resources;

/**
 * Class to produce modular facts for a web page, by directly calling Javascript. Such extraction is highly
 * configurable. For instance, the following produces facts for all dom elements, including facts for XPath, CSS,
 * attributes and text node, all written in separate streams:
 *
 * <pre>
 * {@code
 *   m.factsFor().allElements(el_writer).with().
 *                      attributes().forAll(attrWriter).and().
 *                      css().allProperties(cssWriter).and().
 *                      text(textWriter).withAttributeClob(textWriter).withElementClob(textWriter);
 * 
 *  m.generateFacts();
 * 
 * }
 * </pre>
 *
 * It's possible to specify multiple extraction configuration, e.g.:
 *
 * <pre>
 * {@code
 *   m.factsFor().allElementsExcept("a").writeIn(elementWriter).xpath(xpathWriter);
 *   m.factsFor().onlyElement("a").writeIn(elementWriter).text(textWriter);
 *   m.generateFacts();
 * 
 * }
 * </pre>
 *
 * After the {@link DOMFactGeneratorOnJS#generateFacts()} method is executed, the current configuration is reset. This
 * enables to reuse the same object for successive invocations, as:
 *
 * <pre>
 * {@code
 *   m.factsFor().allElementsExcept(elementWriter,"A");
 *   m.generateFacts();
 *   //here the previous configuration is deleted
 *   //we set a new one and generate the relative facts
 *   m.factsFor().onlyElement(elementWriter,"A");
 *   m.generateFacts();
 * 
 * }
 * </pre>
 *
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public class DOMFactGeneratorOnJS extends DOMFactGenerator {

  private static final String SCRIPT_JS = "extract-facts-new.js";
  static final Logger LOGGER = LoggerFactory.getLogger(DOMFactGeneratorOnJS.class);
  private static final Joiner COMMA_JOINER = Joiner.on(',').skipNulls();

  /**
   * Construct a fact generator for the given browser. By default it uses a {@link PrologFactSerializer} to build the
   * facts
   *
   * @param webBrowser
   */
  public DOMFactGeneratorOnJS(final WebBrowser webBrowser) {

    super(webBrowser);
  }

  /**
   * Construct a fact generator for the given browser, using a custom {@link FactSerializerOnWriter} to build the facts
   *
   * @param webBrowser
   */
  public DOMFactGeneratorOnJS(final WebBrowser webBrowser, final FactSerializerOnWriter serializer) {

    super(webBrowser, serializer);
  }

  /**
   * Generates the configured facts, for the current page in the {@link WebBrowser}, using the given pageId. As a side
   * effect ,it clears the configuration to be reused in following extractions.
   */
  @Override
  public void generateFacts(final String pageId) {

    // XPathUtil.enableCache();
    // XPathFinderByCanonical.enableCache();
    final String locationURL = webBrowser.getLocationURL();
    String lastScript = null;
    try {
      for (final ConfigurableFactExtractionItem item : extractionSets) {

        lastScript = configureJS(item, pageId);
        LOGGER.debug("Evaluating on page <{}> script <{}>", locationURL, lastScript);
        final Object facts = webBrowser.evaluate(lastScript);
        postProcessAndWriteinWriters(item, pageId, facts);

      }
    } catch (final Throwable e) {
      // LOGGER.error("Error producing facts on page: <{}>", locationURL);
      // LOGGER.error("Extraction script used: <{}>", lastScript);
      // LOGGER.error("Throwable <{}> cause: <{}>", e.getClass(), StringUtils.left(e.getMessage(),500));
      throw new DiademRuntimeException("Failed DOM facts extraction " + locationURL + " , see logs for details", e,
          LOGGER);

    } finally {
      extractionSets.clear();
    }
    // XPathUtil.disableCache();
    // XPathFinderByCanonical.disableCache();
  }

  // here come the whole logic of the configuration
  private String configureJS(final ConfigurableFactExtractionItem item, final String pageId) {
    try {
      final Map<String, String> params = Maps.newHashMap();
      params.put("CONF_CURRENT_PAGE_ID", EscapingUtils.quoteUnquotedString(pageId));
      params.put("CONF_EXCL_ELEMENT", "[]");
      params.put("CONF_EXCL_ATTRIBUTE", "[]");
      params.put("CONF_EXCL_CSS", "[]");
      params.put("CONF_INCL_ELEMENT", "[]");
      params.put("CONF_INCL_ATTRIBUTE", "[]");
      params.put("CONF_INCL_CSS", "[]");
      params.put("CONF_ENABLE_TEXT", "false");
      params.put("CONF_ENABLE_ATTRIBUTE", "false");
      params.put("CONF_ENABLE_JSPROP", "false");
      params.put("CONF_ENABLE_CSS", "false");
      params.put("CONF_ENABLE_CLOB", "false");
      params.put("CONF_ENABLE_BOX", "false");
      params.put("CONF_ENABLE_LOCATOR", "false");
      params.put("CONF_ENABLE_USECLASS_FOR_LOCATOR", "true");
      params.put("CONF_ENABLE_USEID_FOR_LOCATOR", "true");
      params.put("CONF_ENABLE_REST", "false");
      params.put("CONF_PREFIX_ELEMENT",
          EscapingUtils.quoteUnquotedString(CONFIGURATION.getString("facts.dom.html-element.idprefix")));
      params.put("CONF_PREFIX_TEXT",
          EscapingUtils.quoteUnquotedString(CONFIGURATION.getString("facts.dom.html-text.idprefix")));
      params.put("CONF_PREFIX_BOX",
          EscapingUtils.quoteUnquotedString(CONFIGURATION.getString("facts.dom.relations.box")));
      params.put("CONF_PREFIX_CLOB",
          EscapingUtils.quoteUnquotedString(CONFIGURATION.getString("facts.dom.text.elementidprefix")));

      LOGGER.debug("Configuring JS script for fact extraction");

      final String commons_js = Resources.toString(Resources.getResource(BrowserFactory.class, "commons_mini.js"),
          Charsets.UTF_8);

      final String isshown_js = Resources.toString(Resources.getResource(BrowserFactory.class, "isShown_mini.js"),
          Charsets.UTF_8);

      String script = Joiner.on("\n").join(commons_js, isshown_js,
          Resources.toString(Resources.getResource(DOMFactGeneratorOnJS.class, SCRIPT_JS), Charsets.UTF_8));

      // String script = Resources.toString(Resources.getResource(DOMFactGeneratorOnJS.class, SCRIPT_JS),
      // Charset.defaultCharset());

      if (item.shadowDomWriter != null) {
        script = script.replace("CONF_ENABLE_REST", "true");
      }

      if (item.jsPropertiesWriter != null) {
        script = script.replace("CONF_ENABLE_JSPROP", "true");
      }

      if (item.onlySpecifiedElements) {
        final CharSequence include = getJsArrayOfQuotedString(item.selectedElements.iterator());
        script = script.replace("CONF_INCL_ELEMENT", include);
        LOGGER.debug("Including only {} elements", include);
      }
      if (item.allElementsExceptThoseSpecified) {
        final CharSequence exclude = getJsArrayOfQuotedString(item.selectedElements.iterator());
        script = script.replace("CONF_EXCL_ELEMENT", exclude);
        LOGGER.debug("Excluding {} elements", exclude);
      }

      // deferred here to make sure text nodes are included
      if (item.generateIds) {
        script = script.replace("CONF_ENABLE_LOCATOR", "true");
        script = script.replace("CONF_ENABLE_ID_FOR_LOCATOR", webBrowser.manageOptions()
            .useIdAttributeForXPathLocator().toString());
        script = script.replace("CONF_ENABLE_USECLASS_FOR_LOCATOR", webBrowser.manageOptions()
            .useClassAttributeForXPathLocator().toString());

      }
      // the selected options
      final Set<FACT_OPTION> options = item.options.keySet();

      for (final FACT_OPTION fact_OPTION : options) {
        final PredicateWithWriter<String> predicate = item.options.get(fact_OPTION);
        switch (fact_OPTION) {
        case ATTRIBUTE:
          script = script.replace("CONF_ENABLE_ATTRIBUTE", "true");
          LOGGER.debug("Enabling attributes");
          script = configureInclusionExclusion(script, predicate, "CONF_EXCL_ATTRIBUTE", "CONF_INCL_ATTRIBUTE");
          break;

        case TEXT:
          LOGGER.debug("Enabling text nodes");
          script = script.replace("CONF_ENABLE_TEXT", "true");

          // the existence of the clob implies it is requested
          final Writer clobWriter = predicate.getWriterFor(CONFIGURATION.getString("facts.dom.relations.clob-text"));
          if (clobWriter != null) {
            LOGGER.debug("Enabling clob nodes");
            script = script.replace("CONF_ENABLE_CLOB", "true");
          }

          break;
        case CSS:
          script = script.replace("CONF_ENABLE_CSS", "true");
          LOGGER.debug("Enabling css");
          script = configureInclusionExclusion(script, predicate, "CONF_EXCL_CSS", "CONF_INCL_CSS");
          final Writer boxwriter = item.cssWritersMap.get(CONFIGURATION.getString("facts.dom.relations.box"));
          if (boxwriter != null) {
            LOGGER.debug("Enabling bounding boxes");
            script = script.replace("CONF_ENABLE_BOX", "true");
          }

          break;
        case XPATH:
          // do nothing, we perform later in the output
          break;

        default:
          break;
        }
      }

      for (final String param : params.keySet()) {
        script = script.replace(param, params.get(param));
      }

      // System.out.println("Running:" + script);
      return script;
    } catch (final IOException e) {
      throw new DiademRuntimeException("Error reading resource file: " + SCRIPT_JS, e, LOGGER);
    }
  }

  private String configureInclusionExclusion(String script, final PredicateWithWriter<String> predicate,
      final String excludeParam, final String includeParam) {
    final Set<String> itemNames = Sets.newHashSet(predicate.asMap().keySet());
    if (predicate.type() == ACCEPTANCE.EXCLUDE) {
      final CharSequence exclude = getJsArrayOfQuotedString(itemNames.iterator());
      script = script.replace(excludeParam, exclude);
      LOGGER.debug("Excluding {}", exclude);
    }

    if (predicate.type() == ACCEPTANCE.INCLUDE) {
      final CharSequence include = getJsArrayOfQuotedString(itemNames.iterator());
      script = script.replace(includeParam, include);
      LOGGER.debug("including {}", include);
    }

    return script;
  }

  private CharSequence getJsArrayOfQuotedString(final Iterator<String> selectedElements) {
    return "[" + COMMA_JOINER.join(Iterators.transform(selectedElements, QuotingAndLowerCaseFunction.INSTANCE)) + "]";
  }

  private void postProcessAndWriteinWriters(final ConfigurableFactExtractionItem item, final String pageId,
      final Object facts) {

    if (facts instanceof Map) {
      final Map map = (Map) facts;

      final Boolean shadowDomRequested = item.shadowDomWriter != null;

      LOGGER.debug("Parsing elements");
      final Map<StartEndNodeId, Boolean> nodes = parseElements(castFacts(map.get("elements")), pageId,
          item.elementWriter, item.shadowDomWriter);

      if (shadowDomRequested || item.generateIds) {
        LOGGER.debug("Parsing Xpath ids");
        parseLocators(castFacts(map.get("locators")), pageId, item.idsWriter, item.shadowDomWriter);
        parseIdMapping(castFacts(map.get("idMapping")), pageId, item.idsWriter, item.shadowDomWriter);
      }

      if (item.jsPropertiesWriter != null) {
        parseJSProperties(castFacts(map.get("jsProperties")), pageId, item.jsPropertiesWriter);
      }

      // the selected options
      Set<FACT_OPTION> options = Sets.newTreeSet(item.options.keySet());

      if (shadowDomRequested) {// override as need to read all anyway
        options = Sets.newTreeSet(Arrays.asList(FACT_OPTION.values()));
        // both already processed above
        options.remove(FACT_OPTION.IDS);
        options.remove(FACT_OPTION.JS_PROPERTIES);

      }

      boolean deferredXpath = false;
      for (final FACT_OPTION fact_OPTION : options) {
        switch (fact_OPTION) {
        case ATTRIBUTE:
          LOGGER.debug("Parsing attributes");

          Writer writerForAttrs = null;
          if (item.options.containsKey(fact_OPTION)) {
            writerForAttrs = item.options.get(fact_OPTION).getWriterFor("anystringHere");
          }
          parseAttributes(castFacts(map.get("attributes")), pageId, writerForAttrs, item.shadowDomWriter);
          break;
        case TEXT:

          Writer textNodesWriter = null;
          if (item.options.containsKey(fact_OPTION)) {
            textNodesWriter = item.options.get(fact_OPTION).getWriterFor(
                CONFIGURATION.getString("facts.dom.relations.html-text"));
          }
          LOGGER.debug("Parsing text nodes");

          nodes.putAll(parseTextNodes(castFacts(map.get("texts")), pageId, textNodesWriter, item.shadowDomWriter));

          Writer clobWriter = null;
          if (item.options.containsKey(fact_OPTION)) {
            clobWriter = item.options.get(fact_OPTION).getWriterFor(
                CONFIGURATION.getString("facts.dom.relations.clob-text"));
          }

          if (clobWriter != null) { // if clob is requested is != null
            LOGGER.debug("Parsing clob spans and values");
            parseClobSpans(castFacts(map.get("clob_spans")), pageId, clobWriter, item.shadowDomWriter);
            parseClobValues(castFacts(map.get("clob_values")), pageId, clobWriter, item.shadowDomWriter);
            parseClobString(map.get("clob_text"), pageId, clobWriter);
            parseClobBreakingFlows(castFacts(map.get("clob_breaking_flows")), pageId, clobWriter);

          } else if (shadowDomRequested) {
            parseClobValues(castFacts(map.get("clob_values")), pageId, clobWriter, item.shadowDomWriter);
          }
          break;
        case CSS:
          LOGGER.debug("Parsing CSS properties");
          final Map<String, Writer> cssWritersMap = item.cssWritersMap;
          parseCSSProperties(castFacts(map.get("css")), pageId, cssWritersMap, item.shadowDomWriter,
              item.jsPropertiesWriter);
          final Writer boxwriter = cssWritersMap.get(CONFIGURATION.getString("facts.dom.relations.box"));
          if (boxwriter != null) {
            parseBoxes(castFacts(map.get("boxes")), pageId, boxwriter);
          }
          break;
        case XPATH:
          // xpath relations are deferred at the very end
          if (item.options.containsKey(fact_OPTION)) {
            deferredXpath = true;
          }
          break;

        default:
          LOGGER.warn("skipped parsing for fact {}", fact_OPTION);
          break;
        }
      }
      // ensure xpath relations are done at the very end
      if (deferredXpath) {
        LOGGER.debug("Adding Visitor for text nodes");
        final PredicateWithWriter<String> xpathSelector = item.options.get(FACT_OPTION.XPATH);

        final Map<String, Writer> stringMap = xpathSelector.asMap();

        final BitSet xpathRelations = getBitSetForXpathRelations(xpathSelector);

        XPathVisitor.buildXPathRelations(nodes, xpathRelations, serializer, stringMap, pageId);
      }

    } else {
      String error = "";
      if (facts == null) {
        error = "Cannot parse Javascript result, javascript returned null";
      } else {
        error = "Cannot parse Javascript result, Unexpected type returned by javascript <" + facts.getClass()
            + "> instead of expected <MAP>";
      }
      throw new DiademRuntimeException(error, LOGGER);
    }
  }

  private void parseLocators(final List<List<Object>> ids, final String pageId, final Writer idsWriter,
      final Writer shadowDomWriter) {
    final String relationName = CONFIGURATION.getString("facts.dom.relations.node-id");
    // tuples are [ id, locator, isExcluded ]
    for (final List<Object> tuple : ids) {

      final Boolean isExcluded = (Boolean) tuple.remove(2);

      final Boolean shadow = shadowDomWriter != null;

      if (isExcluded && !shadow) {
        continue;
      }

      // append page id
      tuple.add(0, pageId);
      tuple.set(1, EscapingUtils.escapeForDLVConstants(tuple.get(1).toString()));
      final String unqEscapedNodeValue = EscapingUtils.escapeStringContentForDLV(tuple.get(2).toString());
      final String string = EscapingUtils.quoteUnquotedString(unqEscapedNodeValue);
      tuple.set(2, string);

      // dom1__node0__id( d1, t_14, "//p[@id='id1']/text()[1]" ).
      if (!isExcluded) {
        serializer.outputFact(idsWriter, relationName, tuple);
      }
      if (shadow) {
        serializer.outputFact(shadowDomWriter, relationName, tuple);
      }
    }

  }

  private void parseIdMapping(final List<List<Object>> ids, final String pageId, final Writer idsWriter,
      final Writer shadowDomWriter) {
    final String relationName = CONFIGURATION.getString("facts.dom.relations.id-mapping");
    // tuples are [ currentid, oldId]
    for (final List<Object> tuple : ids) {
      // append page id
      tuple.add(0, pageId);
      tuple.set(1, EscapingUtils.escapeForDLVConstants(tuple.get(1).toString()));
      tuple.set(2, EscapingUtils.escapeForDLVConstants(tuple.get(2).toString()));
      tuple.set(3, EscapingUtils.escapeForDLVConstants(tuple.get(3).toString()));
      // tuple.set(4, EscapingUtils.escapeForDLVConstants(tuple.get(4).toString()));
      // mapTo(currentPageId, current_node_id, old_page_id, old_node_id ).
      // dom1__node0__mapTo( d2, e_9_p, d1, e_9_p, ).

      serializer.outputFact(idsWriter, relationName, tuple);

      if (shadowDomWriter != null) {
        serializer.outputFact(shadowDomWriter, relationName, tuple);
      }
    }

  }

  private BitSet getBitSetForXpathRelations(final PredicateWithWriter<String> xpathSelector) {
    final BitSet xpathRelations = new BitSet(Axis.values().length);
    for (final Axis axis : Axis.values())
      if (!xpathSelector.apply(axis.name())) {
        xpathRelations.set(axis.getCode());
      }
    return xpathRelations;
  }

  private void parseCSSProperties(final List<List<Object>> css, final String pageId,
      final Map<String, Writer> cssWritersMap, final Writer shadowWriter, final Writer jsWriter) {
    final String cssPrefix = CONFIGURATION.getString("facts.dom.relations.css-prefix");

    final Map<String, String> property2predicate = Maps.newHashMap();
    // initialize the map of predicate names
    for (final CssProperty cssProp : CssProperty.values())
      if (cssWritersMap.containsKey(cssProp.getPropertyName())) {
        property2predicate.put(cssProp.getPropertyName(), cssPrefix + cssProp.asLowerCamelCase());
      }

    // CSSRepresentative [ property, elId, value, isExcluded ];
    for (final List<Object> tuple : css) {

      final String property = tuple.get(0).toString();
      if (!property2predicate.containsKey(property)) {
        continue;
      }

      final Boolean isExcluded = (Boolean) tuple.remove(3);

      final Boolean shadow = shadowWriter != null;
      if (isExcluded && shadow) {
        continue;
      }

      final String elementId = tuple.get(1).toString();
      final String propertyValue = tuple.get(2).toString();
      // dom__css__backgroundAttachment( pageId, e_1_html, "scroll" ).
      tuple.set(0, pageId);
      tuple.set(1, EscapingUtils.escapeForDLVConstants(elementId));
      tuple.set(2, EscapingUtils.quoteIfNotNumberGreaterThenZero(propertyValue));

      ArrayList<String> additionalCSSTuple = null;
      ArrayList<String> additionalJSTuple = null;

      if (CssProperty.background_image.getPropertyName().equals(property)) {
        // special treatment for background_image
        final String parseUrl = parseUrl(propertyValue);
        if (parseUrl != null) {
          additionalCSSTuple = Lists.newArrayList();
          additionalCSSTuple.add(pageId);
          additionalCSSTuple.add(EscapingUtils.escapeForDLVConstants(elementId));
          additionalCSSTuple.add(EscapingUtils.quoteIfNotNumberGreaterThenZero(parseUrl));
          // necessary for the annotator in the js writer
          additionalJSTuple = Lists.newArrayList();
          additionalJSTuple.add(pageId);
          additionalJSTuple.add(EscapingUtils.escapeForDLVConstants(elementId + "_js_bgimg"));
          additionalJSTuple.add(EscapingUtils.escapeForDLVConstants(elementId));
          additionalJSTuple.add(EscapingUtils.escapeForDLVConstants("backgroundimagehref"));
          additionalJSTuple.add(EscapingUtils.quoteIfNotNumberGreaterThenZero(parseUrl));

        }
      }

      final String specialPropertyName = cssPrefix + CssProperty.background_image.asLowerCamelCase() + "Href";
      if (!isExcluded) {
        serializer.outputFact(cssWritersMap.get(property), property2predicate.get(property), tuple);

        if (additionalCSSTuple != null) {
          serializer.outputFact(cssWritersMap.get(property), specialPropertyName, additionalCSSTuple);
          // the annorator needs the css property background-image but redirected in the js bucket.
          //
          final String jsPredicate = CONFIGURATION.getString("facts.dom.relations.js-property");
          if (jsWriter != null) {
            serializer.outputFact(jsWriter, jsPredicate, additionalJSTuple);
          } else {
            LOGGER
            .warn("Cannot provide additional <background-image-href> as javascript property for the annotator. Javascript-properties are not configured.");
          }

        }
      }
      if (shadow) {
        serializer.outputFact(shadowWriter, property2predicate.get(property), tuple);

        if (additionalCSSTuple != null) {
          serializer.outputFact(shadowWriter, specialPropertyName, additionalCSSTuple);
        }
      }

    }

  }

  private String parseUrl(final String urlDef) {
    final int urlsInIt = StringUtils.countMatches(urlDef, "url(");
    if (urlsInIt != 1)
      return null;
    final String possiblyQuotedURL = StringUtils.substringBeforeLast(StringUtils.substringAfter(urlDef, "url("), ")");
    if (EscapingUtils.isQuotedString(possiblyQuotedURL))
      return EscapingUtils.stripQuotesFromQuotedString(possiblyQuotedURL);
    else
      return possiblyQuotedURL;
  }

  private static final String BOX_NEGATIVE_MESSAGE = "Element %s has a bounding box (%s,%s,%s,%s with %s width and %s height) with negative coordinates. No box fact created.";
  private static final String BOX_NEGATIVE_TITLE = "Element with negative coordinates in bounding box.";

  private void parseBoxes(final List<List<Object>> boxes, final String pageId, final Writer destination) {

    final String relationName = CONFIGURATION.getString("facts.dom.relations.box");

    for (final List<Object> singleBox : boxes) {
      boolean skip = false;
      // clean negative and non integer value
      for (int i = 0; i < singleBox.size(); i++) {
        if (singleBox.get(i).toString().startsWith("-")) {
          final String message = String.format(BOX_NEGATIVE_MESSAGE, singleBox.get(0), singleBox.get(1),
              singleBox.get(2), singleBox.get(3), singleBox.get(4), singleBox.get(5), singleBox.get(6));
          // TODO track down and improve
          // WebApiFailures.NEGATIVE_COORDINATE_BOX.failure().addDescription(BOX_NEGATIVE_TITLE, message)
          // .setParameter("element", singleBox.get(0).toString()).write();
          LOGGER.debug(message);
          skip = true;
          break;
        }
        // rude truncation
        if (singleBox.get(i).toString().contains(".")) {
          singleBox.set(i, Splitter.on(".").split(singleBox.get(i).toString()).iterator().next());
        }
      }

      if (skip) {
        continue;
      }
      // add page id
      singleBox.add(0, pageId);
      // escape element id
      singleBox.set(1, EscapingUtils.escapeForDLVConstants(singleBox.get(1).toString()));
      // dom1__css0__boundingBox( d1, e_51_p, 8, 240, 1010, 259, 1002, 19 ).
      serializer.outputFact(destination, relationName, singleBox);
    }

  }

  private void parseJSProperties(final List<List<Object>> jsProperties, final String pageId,
      final Writer jsPropertiesWriter) {
    final String relationName = CONFIGURATION.getString("facts.dom.relations.js-property");

    // JSPropertyRepresentative [ propId, elId, property, value];
    for (final List<Object> tuple : jsProperties) {

      tuple.add(0, pageId);
      // now is ( pageId, propId, elId, property, value ).
      tuple.set(1, EscapingUtils.escapeForDLVConstants(tuple.get(1).toString()));
      tuple.set(2, EscapingUtils.escapeForDLVConstants(tuple.get(2).toString()));
      final String prop = EscapingUtils.escapeForDLVConstants(tuple.get(3).toString());
      tuple.set(3, prop);
      if (!(prop.equals("width_num") || prop.equals("height_num"))) {
        // normally is a string quoted
        tuple.set(4,
            EscapingUtils.quoteUnquotedString(EscapingUtils.escapeStringContentForDLV(tuple.get(4).toString())));
      } else {
        // for some properties we want numbers
        tuple.set(4, (EscapingUtils.escapeStringContentForDLV(tuple.get(4).toString())));
      }
      serializer.outputFact(jsPropertiesWriter, relationName, tuple);

    }

  }

  private void parseClobString(final Object object, final String pageId, final Writer clobWriter) {
    if (object != null) {
      final String relationName = CONFIGURATION.getString("facts.dom.relations.clob-text");

      final String clobId = CONFIGURATION.getString("facts.dom.text.elementidprefix") + pageId;
      final String val = EscapingUtils.quoteUnquotedString(EscapingUtils.escapeStringContentForDLV(object.toString()));
      // writes the clob_element fact
      // dom1__clob0__text( d1, clob_d1, "Some Content separated by space" ).
      serializer.outputFact(clobWriter, relationName, new Object[] { pageId, clobId, val });

    } else {
      LOGGER.error("Cannot parse Javascript result, clob string is null");
      throw new DiademRuntimeException("Error parsing facts for clob from JS invocation", LOGGER);
    }

  }

  private Map<StartEndNodeId, Boolean> parseTextNodes(final List<List<Object>> textNodes, final String pageId,
      final Writer textNodeWriter, final Writer shadowDomWriter) {
    final Map<StartEndNodeId, Boolean> nodes = Maps.newHashMap();
    final String relationName = CONFIGURATION.getString("facts.dom.relations.html-text");
    // tuples are TextRepresentative[ id, start, end, parStart, isExcluded ];
    for (final List<Object> tuple : textNodes) {
      // is excluded if not asked or garbage
      final Boolean isExcluded = (Boolean) tuple.remove(4);
      final Boolean shadow = shadowDomWriter != null;

      if (isExcluded && !shadow) {
        continue;
      }

      tuple.add(0, pageId);

      // dom1__text0__node( d1, t_4, 4, 5, 3 ).
      if (!isExcluded) {
        serializer.outputFact(textNodeWriter, relationName, tuple);
      }

      if (shadow) {
        serializer.outputFact(shadowDomWriter, relationName, tuple);
      }

      nodes.put(
          new StartEndNodeId(tuple.get(1).toString(), Long.parseLong(tuple.get(4).toString()), Long.parseLong(tuple
              .get(2).toString()), Long.parseLong(tuple.get(3).toString())), isExcluded);
    }
    return nodes;
  }

  private void parseClobValues(final List<List<Object>> attributes, final String pageId, final Writer clobWriter,
      final Writer shadowDomWriter) {
    final String relationName = CONFIGURATION.getString("facts.dom.relations.text-value");
    // tuples are ClobValuesRepresentative[ id, value, isExcluded, isGarbage ]
    for (final List<Object> tuple : attributes) {

      // remove flags information
      final Boolean isGarbage = (Boolean) tuple.remove(3);
      final Boolean isExcluded = (Boolean) tuple.remove(2);

      final Boolean shadow = shadowDomWriter != null;
      final Boolean skip = (isGarbage || isExcluded);

      if (skip && !shadow) {
        continue;
      }

      // append page id, becomes [ pageId, id, value]
      tuple.add(0, pageId);
      final String originalValue = tuple.get(2).toString();

      if (!skip) {
        final String normalized = EscapingUtils.normalizeTextNodes(originalValue);
        // here the version normalized and trimmed
        final String unqEscapedNodeValue = EscapingUtils.escapeStringContentForDLV(normalized);
        tuple.set(2, EscapingUtils.quoteUnquotedString(unqEscapedNodeValue));
        // dom1__clob0__value( d1, t_4, "Title" ).
        serializer.outputFact(clobWriter, relationName, tuple);
      }
      if (shadow) // we print a copy
      {
        // else we produce the version without normalization that goes into clobValuesOriginalWriter
        final String escapedValue = EscapingUtils.escapeStringContentForDLV(originalValue);
        tuple.set(2, EscapingUtils.quoteUnquotedString(escapedValue));
        // dom1__clob0__value( d1, t_4, "Title /n/t" ).
        serializer.outputFact(shadowDomWriter, relationName, tuple);
      }

    }

  }

  private void parseClobSpans(final List<List<Object>> clobSpans, final String pageId, final Writer clobWriter,
      final Writer shadowDomWriter) {
    final String relationName = CONFIGURATION.getString("facts.dom.relations.clob-span");
    // SpanRepresentative[ id, Configuration.prefixes.clob, startClobSize, endClobSize,isExcluded ];
    for (final List<Object> tuple : clobSpans) {
      final Boolean isExcluded = (Boolean) tuple.remove(4);
      if (isExcluded) {
        continue;
      }
      // add page Id
      tuple.add(0, pageId);
      // escape element id
      tuple.set(1, EscapingUtils.escapeForDLVConstants(tuple.get(1).toString()));
      // append page id
      tuple.set(2, tuple.get(2).toString() + pageId);
      // dom1__clob0__span( d1, t_4, clob_d1, 0, 5 ).
      serializer.outputFact(clobWriter, relationName, tuple);
      if (shadowDomWriter != null) {
        serializer.outputFact(shadowDomWriter, relationName, tuple);
      }
    }

  }

  private void parseClobBreakingFlows(final List<List<Object>> separators, final String pageId, final Writer clobWriter) {
    final String relationName = CONFIGURATION.getString("facts.dom.relations.clob-breakflow");
    // BreakingFlowRepresentative[ Configuration.prefixes.clob, position ];
    for (final List<Object> tuple : separators) {
      // add page Id
      tuple.add(0, pageId);
      // append page id
      tuple.set(1, tuple.get(1).toString() + pageId);
      // dom1__clob0__breakflow( d1, clob_d1, 5 ).
      serializer.outputFact(clobWriter, relationName, tuple);
    }

  }

  // boolean isClobElementRequested() {
  // return clobSelectorPredicate.getWriterFor(config.getString("facts.dom.relations.clob-text")) != null;
  // }

  private void parseAttributes(final List<List<Object>> attributes, final String pageId, final Writer writerForAttrs,
      final Writer shadowDomWriter) {
    final String relationName = CONFIGURATION.getString("facts.dom.relations.attribute");
    // [ id, elId, name, value, isExcluded ];
    for (final List<Object> tuple : attributes) {

      if (tuple.get(2).toString().equals("webdriver")) {
        continue;
      }

      final Boolean isExcluded = (Boolean) tuple.remove(4);

      final Boolean shadow = shadowDomWriter != null;

      if (isExcluded && !shadow) {
        continue;
      }

      // becomes // [ pageId, id, elId, name, value, isExcluded ];
      tuple.add(0, pageId);

      final String attributeName = tuple.get(3).toString();

      // escape tag in Id and tag value and attributeName
      tuple.set(1, EscapingUtils.escapeForDLVConstants(tuple.get(1).toString()));
      tuple.set(2, EscapingUtils.escapeForDLVConstants(tuple.get(2).toString()));

      final String nameConstant = EscapingUtils.escapeForDLVConstants(tuple.get(3).toString());
      // if unchanged is a valid constant
      if (nameConstant.equals(attributeName)) {
        tuple.set(3, nameConstant);
      } else {
        tuple.set(3, EscapingUtils.quoteUnquotedString(EscapingUtils.escapeStringContentForDLV(attributeName)));
      }

      final String nodeValue = (tuple.get(4) == null) ? "null" : tuple.get(4).toString();
      // values as string
      tuple.set(4, EscapingUtils.quoteUnquotedString(EscapingUtils.escapeStringContentForDLV(nodeValue)));

      if (!isExcluded) {
        // dom__attribute__node(pageId, e_13_p_id, e_13_p, id, "id1" ).
        serializer.outputFact(writerForAttrs, relationName, tuple);
      }
      if (shadow) {
        // attribute name not escaped but as string
        tuple.set(3, EscapingUtils.quoteUnquotedString(EscapingUtils.escapeStringContentForDLV(attributeName)));
        serializer.outputFact(shadowDomWriter, relationName, tuple);
      }

    }

  }

  private Map<StartEndNodeId, Boolean> parseElements(final List<List<Object>> elements, final String pageId,
      final Writer elementWriter, final Writer shadowDomWriter) {
    final Map<StartEndNodeId, Boolean> nodes = Maps.newHashMap();
    final String factName = CONFIGURATION.getString("facts.dom.relations.html-element");
    // tuples arrive as: [id, start, end, parStart, tag, isExcluded];
    for (final List<Object> tuple : elements) {

      final Boolean isExcluded = (Boolean) tuple.remove(5);
      final Boolean shadow = shadowDomWriter != null;

      if (isExcluded && !shadow) {
        continue;
      }
      // now becomes [pageId, elementid, start, end, parStart, tag, isExcluded];
      tuple.add(0, pageId);
      // escape the id as it contain the tag name
      tuple.set(1, EscapingUtils.escapeForDLVConstants(tuple.get(1).toString()));
      final String tagName = tuple.get(5).toString();
      // escape the tag for DLV
      tuple.set(5, EscapingUtils.escapeForDLVConstants(tagName));

      // dom__element__node( pageId, e_13_p, 13, 16, 8, p ).
      if (!isExcluded) {
        serializer.outputFact(elementWriter, factName, tuple);
      }
      if (shadow) { // we print a copy
        // element name not escaped but as string
        tuple.set(5, EscapingUtils.quoteUnquotedString(EscapingUtils.escapeStringContentForDLV(tagName)));
        serializer.outputFact(shadowDomWriter, factName, tuple);
      }

      nodes.put(
          new StartEndNodeId(tuple.get(1).toString(), Long.parseLong(tuple.get(4).toString()), Long.parseLong(tuple
              .get(2).toString()), Long.parseLong(tuple.get(3).toString())), isExcluded);
    }
    return nodes;
  }

  @SuppressWarnings("unchecked")
  private List<List<Object>> castFacts(final Object facts) {

    if ((facts != null) && (facts instanceof List<?>))
      return (List<List<Object>>) facts;

    throw new DiademRuntimeException(
        "Cannot parse Javascript result for elements. Expected List<?> but actual type is "
            + (facts == null ? null : facts.getClass()), LOGGER);

  }

  // a singleton function to capitalize Strings
  protected enum QuotingAndLowerCaseFunction implements Function<String, String> {
    INSTANCE;

    QuotingAndLowerCaseFunction() {
    }

    @Override
    public String apply(final String input) {
      return "'" + input.toLowerCase() + "'";
    }

  }

}
