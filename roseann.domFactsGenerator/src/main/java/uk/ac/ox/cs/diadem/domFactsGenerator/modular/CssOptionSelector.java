package uk.ac.ox.cs.diadem.domFactsGenerator.modular;

import java.io.Writer;
import java.util.Map;

import uk.ac.ox.cs.diadem.webapi.dom.DOMCSS2Properties.CssProperty;

/**
 * Enables to specification of facts related to CSS
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public interface CssOptionSelector {
  /**
   * More detailed css configuration
   * 
   * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
   */
  interface CssMoreOptionSelector extends AndorBuildOptions {

    /**
     * To enable facts for bounding box,
     * 
     * @param boxes
     * @return the same object to continue the configuration
     */
    CssMoreOptionSelector withBoxes(Writer boxWriter);

    /**
     * To enable facts for produce facts for visual coordinates like, visual(e_1081_div,absolute_height,100)
     * 
     * @param visualAbsoluteCoordinates
     * @return the same object to continue the configuration
     */
    CssMoreOptionSelector withCoord(Writer coordWriter);
  }

  /**
   * To enable facts for bounding boxes
   * 
   * @param destination
   *          output writer
   * @return the same object to continue the configuration
   */
  AndorBuildOptions onlyBoundingBoxes(Writer destination);

  /**
   * Specifies the generation of facts for all css properties in one single writer.
   * 
   * @param destination
   *          output writer
   * @return the same object to continue the configuration
   */
  CssMoreOptionSelector allProperties(Writer destination);

  /**
   * Specifies the generation of facts for only the css properties provides, in one single writer.
   * 
   * @param destination
   *          destination output writer
   * @param properties
   *          the list of properties to include
   * @return the same object to continue the configuration
   */
  CssMoreOptionSelector onlyProperties(Writer destination, CssProperty... properties);

  /**
   * Specifies the generation of facts for only the css properties provides, each one written in the {@link Writer}
   * which is mapped to
   * 
   * @param properties
   *          a map from {@link CssProperty} to the writer in which the related fact are written.
   * @return the same object to continue the configuration
   */
  CssMoreOptionSelector onlyProperties(Map<CssProperty, Writer> cssWriters);

  /**
   * Specifies the generation of facts for all {@link CssProperty}, excluding those provided. The facts are written in
   * one single writer.
   * 
   * @param destination
   *          destination output writer
   * @param properties
   *          the list of properties to exclude
   * @return the same object to continue the configuration
   */
  CssMoreOptionSelector allPropertiesExcept(Writer destination, CssProperty... properties);

}