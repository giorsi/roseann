/*
 * Copyright (c)2011, DIADEM Team
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the DIADEM team nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DIADEM Team BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import java.io.Writer;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter;
import uk.ac.ox.cs.diadem.util.misc.EscapingUtils;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode;
import uk.ac.ox.cs.diadem.webapi.dom.DOMVisitor;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
class TextVisitorImpl extends AbstractStartEndVisitor {

  public static DOMVisitor createTextVisitor(final FactSerializerOnWriter serializer, final String context,
      final PredicateWithWriter<? super String> elementSelectorPredicate,
      final PredicateWithWriter<? super String> clobSelectorPredicate) {
    return new TextVisitorImpl(serializer, context, elementSelectorPredicate, clobSelectorPredicate);
  }

  @Override
  public boolean filterOut(final DOMNode node) {
    return predicate.apply(node.getNodeName().toUpperCase());
  }

  private FactSerializerOnWriter serializer;
  static final Logger LOGGER = LoggerFactory.getLogger(TextVisitorImpl.class);
  private String pageId;
  private StringBuilder elementClob;
  // private StringBuilder attributeClob;
  private Map<DOMNode, Integer> startPositions;
  private PredicateWithWriter<? super String> predicate;
  private PredicateWithWriter<? super String> clobSelectorPredicate;

  private TextVisitorImpl(final FactSerializerOnWriter serializer, final String context,
      final PredicateWithWriter<? super String> predicate,
      final PredicateWithWriter<? super String> clobSelectorPredicate) {
    this();
    this.predicate = predicate;
    this.clobSelectorPredicate = clobSelectorPredicate;
    startPositions = Maps.newHashMap();
    this.serializer = serializer;
    pageId = context;
    elementClob = new StringBuilder();
    // this.attributeClob = new StringBuilder();
  }

  private TextVisitorImpl() {
    // TODO Auto-generated constructor stub
  }

  @Override
  public void startElement(final DOMNode node) {

    addStartLabel(node);
    startPositions.put(node, elementClob.length());
  }

  @Override
  public void visitText(final DOMNode node) {

    labelText(node);

    // if the element's parent is to filter, we don't produce any output
    if (filterOut(node.getParentNode())) {
      LOGGER.debug("Text facts skipped for Element {}", node.getNodeName());
      return;
    }

    final StartEndNodeId ident = getStartEndNodeId(node);
    List<FactParameter> params = new LinkedList<FactParameter>();
    params = new LinkedList<FactParameter>();
    params.add(new FactParameter(config.getString("facts.dom.html-text.pageid"), pageId));
    params.add(new FactParameter(config.getString("facts.dom.html-text.id"), ident.formatTextIdentifier(node)));
    params.add(new FactParameter(config.getString("facts.dom.html-text.start"), Long.toString(ident.start)));
    params.add(new FactParameter(config.getString("facts.dom.html-text.end"), Long.toString(ident.end)));
    params.add(new FactParameter(config.getString("facts.dom.html-text.parent"), Long.toString(ident.parent)));

    final String relationName = config.getString("facts.dom.relations.html-text");

    // if the node contains valuable text, we write facts like
    // dom__text__node(pageId, t_505, 505, 506, 504). into elementWriter
    // otherwise is skipped in the DOMWALKER

    // if (enableGarbageNodes)
    // relationName = config.getString("facts.dom.relations.html-text-garbage");

    outputFact(getTextNodesWriter(), relationName, params);

    // produces elementclob only if configured
    if (!isClobElementRequested())
      return;
    final long startPos = elementClob.length();
    // writes stuff like dom__clob__span( pageId, t_24, elclob_d1, 0, 9 ).
    writeSpan(node, ident);
    final long endPos = elementClob.length();
    if ((endPos < 0) || (endPos < startPos))
      return;

    elementClob.append(config.getString("facts.dom.output.clob.endsep"));

  }

  // writes stuff like dom__clob__span( pageId, t_24, elclob_d1, 0, 9 ). FOR TEXT NODES
  private void writeSpan(final DOMNode node, final StartEndNodeId ident) {
    String nodeValue = node.getNodeValue();
    nodeValue = EscapingUtils.normalizeTextNodes(nodeValue);
    final long startPos = elementClob.length();
    elementClob.append(nodeValue);
    final long endPos = elementClob.length();
    List<FactParameter> params = new LinkedList<FactParameter>();
    params.add(new FactParameter(config.getString("facts.dom.text.pageid"), pageId));
    params.add(new FactParameter(config.getString("facts.dom.text.node"), ident.formatTextIdentifier(node)));
    params.add(new FactParameter(config.getString("facts.dom.text.clob"), config
        .getString("facts.dom.text.elementidprefix") + pageId));
    params.add(new FactParameter(config.getString("facts.dom.text.start"), Long.toString(startPos)));
    params.add(new FactParameter(config.getString("facts.dom.text.end"), Long.toString(endPos)));

    outputFact(getClobElementwriter(), config.getString("facts.dom.relations.clob-span"), params);
    // WORKAROUND TEXT NODE
    params = new LinkedList<FactParameter>();
    params.add(new FactParameter(config.getString("facts.dom.text.pageid"), pageId));
    params.add(new FactParameter(config.getString("facts.dom.text.node"), ident.formatTextIdentifier(node)));
    final String unqEscapedNodeValue = EscapingUtils.escapeStringContentForDLV(nodeValue);
    final String string = EscapingUtils.quoteUnquotedString(unqEscapedNodeValue);
    params.add(new FactParameter(config.getString("facts.dom.text.value"), string));

    outputFact(getClobElementwriter(), config.getString("facts.dom.relations.text-value"), params);
  }

  @Override
  public void endElement(final DOMNode node) {

    updateEndLabel(node);

    // if the element is to filter, we don't produce any output
    if (filterOut(node)) {
      LOGGER.debug("Facts for element's content skipped for Element {}", node.getNodeName());
      return;
    }
    // skip if configured
    if (!isClobElementRequested())
      return;
    // writes stuff like dom__clob__span( pageId, elementId, elclob_d1, 0, 9 ) BUT FOR ELEMENTS
    final int endpos = elementClob.length();
    final int startPos = startPositions.get(node);
    final StartEndNodeId ident = getStartEndNodeId(node);
    if ((endpos < 0) || (endpos < startPos))
      return;
    final List<FactParameter> params = new LinkedList<FactParameter>();
    params.add(new FactParameter(config.getString("facts.dom.text.pageid"), pageId));
    params.add(new FactParameter(config.getString("facts.dom.text.node"), ident.formatElementIdentifier(node)));
    params.add(new FactParameter(config.getString("facts.dom.text.clob"), config
        .getString("facts.dom.text.elementidprefix") + pageId));
    params.add(new FactParameter(config.getString("facts.dom.text.start"), startPositions.get(node).toString()));
    params.add(new FactParameter(config.getString("facts.dom.text.end"), Long.toString(endpos)));
    // content goes in clob_element writer
    outputFact(getClobElementwriter(), config.getString("facts.dom.relations.clob-span"), params);
  }

  @Override
  public void finish() {

    final List<FactParameter> params = Lists.newLinkedList();
    // for element elementClob, if configured
    if (isClobElementRequested()) {
      params.add(new FactParameter(config.getString("facts.dom.clob.pageid"), pageId));
      params.add(new FactParameter(config.getString("facts.dom.clob.id"), config
          .getString("facts.dom.text.elementidprefix") + pageId));
      params.add(new FactParameter(config.getString("facts.dom.clob.value"), EscapingUtils
          .quoteUnquotedString(EscapingUtils.escapeStringContentForDLV(elementClob.toString()))));
      // writes the clob_element fact
      outputFact(getClobElementwriter(), config.getString("facts.dom.relations.clob-text"), params);
    }
  }

  protected boolean isClobElementRequested() {
    return clobSelectorPredicate.getWriterFor(config.getString("facts.dom.relations.clob-text")) != null;
  }

  private Writer getClobElementwriter() {
    return clobSelectorPredicate.getWriterFor(config.getString("facts.dom.relations.clob-text"));
  }

  protected boolean isClobAttributeRequested() {
    return clobSelectorPredicate.getWriterFor(config.getString("facts.dom.relations.clob_attribute")) != null;
  }

  private Writer getTextNodesWriter() {
    return clobSelectorPredicate.getWriterFor(config.getString("facts.dom.relations.html-text"));
  }

  protected void outputFact(final Writer destination, final String relationName, final List<FactParameter> params) {
    serializer.outputFact(destination, relationName, params);
  }

  @Override
  public String toString() {
    return super.toString();
  }
}
