package uk.ac.ox.cs.diadem.domFactsGenerator.modular;

import java.io.Writer;
import java.util.Map;

/**
 * Enables the configuration of XPath relation to produce as facts for the elected elements.
 * 
 * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
 */
public interface XPathOptionSelector {

  // /**
  // * To enable/disable the relation <code>child(childnodeId,parentnodeId)</code> (including text nodes). Also, it
  // * produces <code>childTextOf(childTextNodeId,parentElementId)</code> in case of text nodes.
  // *
  // * @param xpathChild
  // * @return the same object to continue the configuration
  // */
  // public XPathOptionSelector child(boolean produceChild);
  //
  // /**
  // * To enable/disable the relation <code>following(followerNodeId, aNodeId)</code> (only for element nodes)
  // *
  // * @param produceFollowing
  // * @return the same object to continue the configuration
  // */
  // public XPathOptionSelector following(boolean produceFollowing);
  //
  // /**
  // * Configure whether to produce the relations <code>descendant(descnodeId,ancestorNodeId)</code> (also for text
  // nodes)
  // * and <code>descendantTextOf(descTextId,ancestorElementId)</code>. Also, it is possible to enable the relation
  // * <code>descendantOrSelf(descnodeId,ancestorNodeId).</code>
  // *
  // * @param produceDescendant
  // * true to produce descendant relations
  // * @param produceDescendantOrSelf
  // * true to produce descendantOrSelf
  // * @return the same object to continue the configuration
  // */
  // public XPathOptionSelector descendant(boolean produceDescendant, boolean produceDescendantOrSelf);
  /**
   * XPath axes to produce facts for
   * 
   * @author Giovanni Grasso (giovannigrasso@gmail.com) Oxford University, Department of Computer Science
   */
  public static enum Axis {
    /**
     * enables the relation <code>child(childnodeId,parentnodeId)</code> (including text nodes)
     */
    CHILD(0),
    /**
     * produces the relations <code>descendant(descnodeId,ancestorNodeId)</code> (including text nodes)
     */
    DESCENDANT(1),
    /**
     * produces the relations <code>descendantOrSelf(descnodeId,descnodeId)</code> (including text nodes)
     */
    DESCENDANT_OR_SELF(2),
    /**
     * enables the relation <code>following(followerNodeId, aNodeId)</code> (including text nodes)
     */
    FOLLOWING(3),
    /**
     * enables the relation <code>followingSibling(followerNodeId, aNodeId)</code> (including text nodes)
     */
    FOLLOWING_SIBLING(4);

    public int getCode() {
      return code;
    }

    private final int code;

    private Axis(final int code) {
      this.code = code;

    }
  }

  /**
   * Specifies the generation of facts for all {@link Axis}, in one single writer.
   * 
   * @param destination
   *          output writer
   * @return an {@link AndOptions} object to continue the configuration
   */
  AndorBuildOptions allRelations(Writer destination);

  /**
   * Specifies the generation of facts for only the provided {@link Axis}, in one single writer.
   * 
   * @param destination
   *          output writer
   * @return an {@link AndOptions} object to continue the configuration
   */
  AndorBuildOptions onlyRelations(Writer destination, Axis... relations);

  /**
   * Specifies the generation of facts for only the provided {@link Axis}, each one in the {@link Writer} whose is
   * mapped to.
   * 
   * @param xpathWriters
   *          map {@link Axis} to destination {@link Writer}
   * @return an {@link AndOptions} object to continue the configuration
   */
  AndorBuildOptions onlyRelations(Map<Axis, Writer> xpathWriters);

}