package uk.ac.ox.cs.diadem.domFactsGenerator;

import org.slf4j.Logger;

import uk.ac.ox.cs.diadem.env.DiademRuntimeException;

@SuppressWarnings("serial")
public class FailingBrowserFactGenerationException extends DiademRuntimeException {

    public FailingBrowserFactGenerationException(final String message, final Logger logger) {

        super(message, logger);
    }

    public FailingBrowserFactGenerationException(final String message, final Throwable cause, final Logger logger) {

        super(message, cause, logger);
    }
}
