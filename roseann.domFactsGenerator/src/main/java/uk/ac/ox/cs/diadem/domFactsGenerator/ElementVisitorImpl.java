/*
 * Copyright (c)2011, DIADEM Team
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the DIADEM team nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL DIADEM Team BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package uk.ac.ox.cs.diadem.domFactsGenerator;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.ox.cs.diadem.domFactsGenerator.modular.PredicateWithWriter;
import uk.ac.ox.cs.diadem.util.misc.EscapingUtils;
import uk.ac.ox.cs.diadem.webapi.dom.DOMNode;
import uk.ac.ox.cs.diadem.webapi.dom.DOMVisitor;

import com.google.common.collect.Lists;

class ElementVisitorImpl extends AbstractStartEndVisitor {

  final String factName = config.getString("facts.dom.relations.html-element");

  public static DOMVisitor createElementVisitor(final FactSerializerOnWriter serializer, final String context,
      final PredicateWithWriter<? super String> predicate) {
    return new ElementVisitorImpl(serializer, context, predicate);
  }

  @Override
  public boolean filterOut(final DOMNode node) {
    return elementSelector.apply(node.getNodeName().toUpperCase());
  }

  final FactSerializerOnWriter serializer;
  static final Logger LOGGER = LoggerFactory.getLogger(ElementVisitorImpl.class);
  final String pageId;
  private final PredicateWithWriter<? super String> elementSelector;

  private ElementVisitorImpl(final FactSerializerOnWriter serializer, final String context,
      final PredicateWithWriter<? super String> elementSelectorPredicate) {
    super();
    this.serializer = serializer;
    pageId = context;
    elementSelector = elementSelectorPredicate;
  }

  @Override
  public void endElement(final DOMNode node) {

    updateEndLabel(node);
    final StartEndNodeId ident = getStartEndNodeId(node);
    // if the element is to filter, we don't produce any output
    if (filterOut(node)) {
      LOGGER.debug("skipped Facts for Element {}", node.getNodeName());
      return;
    }

    final List<FactParameter> params = Lists.newLinkedList();
    params.add(FactParameter.of(pageId));
    params.add(FactParameter.of(ident.formatElementIdentifier(node)));
    params.add(FactParameter.of(Long.toString(ident.start)));
    params.add(FactParameter.of(Long.toString(ident.end)));
    params.add(FactParameter.of(Long.toString(ident.parent)));
    params.add(FactParameter.of(EscapingUtils.escapeForDLVConstants(node.getLocalName())));

    // dom__element__node( pageId, e_13_p, 13, 16, 8, p ).
    serializer.outputFact(elementSelector.getWriterFor(node.getLocalName()), factName, params);
  }

  @Override
  public String toString() {
    return super.toString();
  }
}
