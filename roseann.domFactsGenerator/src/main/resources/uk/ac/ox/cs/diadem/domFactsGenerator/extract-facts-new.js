// EXTRACTFACTS.JS 
//This file needs also Commons.js and isShown.js
/** Configuration for the extractor * */
var Configuration = {
	/** Excludes for elements, attributes, etc. */
	excludes : {
		/** @const */
		elements : CONF_EXCL_ELEMENT, // array ['p']
		/** @const */
		attributes : CONF_EXCL_ATTRIBUTE, // array ['src']
		/** @const */
		css : CONF_EXCL_CSS
	// array ['font']

	},
	/** Includes for elements, attributes, etc. */
	includes : {
		/** @const */
		elements : CONF_INCL_ELEMENT,
		/** @const */
		attributes : CONF_INCL_ATTRIBUTE,
		/** @const */
		css : CONF_INCL_CSS
	},
	/** @const */
	text : CONF_ENABLE_TEXT, // boolean
	/** @const */
	attribute : CONF_ENABLE_ATTRIBUTE,
	/** @const */
	jsProperties : CONF_ENABLE_JSPROP,
	/** @const */
	css : CONF_ENABLE_CSS,
	/** @const */
	clob : CONF_ENABLE_CLOB,
	/** @const */
	boxes : CONF_ENABLE_BOX,
	/** @const */
	locators : CONF_ENABLE_LOCATOR,
	/** @const */
	locators_useClass : CONF_ENABLE_USECLASS_FOR_LOCATOR,
	/** @const */
	locators_useId : CONF_ENABLE_USEID_FOR_LOCATOR,
	/** @const */
	allTheRest : CONF_ENABLE_REST,
	/** @const */
	skipXMLNSattr : true,
	/** @const */
	idMapping : '_diadem_id',
	/** @const */
	pageIdMapping : '_diadem_previous_page_id',
	/** @const */
	currentPageId : CONF_CURRENT_PAGE_ID,
	
	prefixes : {
		/** @const */
		element : CONF_PREFIX_ELEMENT, // "e_"
		/** @const */
		text : CONF_PREFIX_TEXT, // "t_"
		/** @const */
		box : CONF_PREFIX_BOX, // "boundingBox"
		/** @const */
		clob : CONF_PREFIX_CLOB,
		/** @const */
		separator : " ",
		/** @const */
		js_prop : "js_"
	}
};



/** @enum {number} */
var XPathAxis = {
	CHILD : 0,
	DESCENDANT : 1,
	FOLLOWING : 2,
	PRECEDING : 3,
	ANCESTOR : 4,
	FOLLOWING_SIBLING : 5,
	PRECEDING_SIBLING : 6,
	SELF : 8,
	ANCESTOR_OR_SELF : 9,
	DESCENDANT_OR_SELF : 10,
	PARENT : 11
};

var ElementRepresentative = function(id, start, end, parStart, tag, isExcluded) {
	return [ id, start, end, parStart, tag, isExcluded ];
};
var TextRepresentative = function(id, start, end, parStart, isExcluded) {
	return [ id, start, end, parStart, isExcluded ];
};
var ClobValuesRepresentative = function(id, value, isExcluded, isGarbage) {
	return [ id, value, isExcluded, isGarbage ];
};
var ClobBreakingFlowRepresentative = function(position) {

	return [ Configuration.prefixes.clob,position];
}
var SpanRepresentative = function(id, startClobSize, endClobSize, isExcluded) {
	return [ id, Configuration.prefixes.clob, startClobSize, endClobSize,isExcluded ];
};
var CSSRepresentative = function(property, elId, value, isExcluded) {
	return [ property, elId, value, isExcluded ];
};
var BoundingBoxRepresentative = function(elId, left, top, right, bottom, width,
		height) {
	return [ elId, left, top, right, bottom, width, height ];
};
var AttributeRepresentative = function(id, elId, name, value, isExcluded) {
	return [ id, elId, name, value, isExcluded ];
};
var LocatorRepresentative = function(id, locator, isExcluded) {
	return [ id, locator, isExcluded ];
}
var JSPropertyRepresentative = function(propId, elId, property, value) {

	return [ propId,elId, property, value];
}

var IdMappingRepresentative = function(currentId,oldId) {

	return [ currentId,document[Configuration.pageIdMapping], oldId];
}



var Extractor = function() {
	this.result = {
		elements : [],
		attributes : [],
		jsProperties : [],
		idMapping : [],
		texts : [],
		texts_garbage : [],
		clob_values : [],
		clob_spans : [],
		clob_text : "",
		clob_breaking_flows : [],
		css : [],
		boxes : [],
		xpaths : [],
		locators : []
	};
	this.helpers = {
		id_counter : 0,
		start_counter : 0,
		text_counter : 0,
		clob_size : 0,
		emptyBox : {
			left: Number.POSITIVE_INFINITY,
			top: Number.POSITIVE_INFINITY,
			right: 0,
			bottom: 0
		},
		merge: function(a, b) {
			if (!b) return a;
			var vleft = Math.min(a.left, b.left);
			var vtop = Math.min(a.top, b.top);
			var vright = Math.max(a.right, b.right);
			var vbottom = Math.max(a.bottom, b.bottom);
			return {
				left: vleft,
				top: vtop,
				right: vright,
				bottom: vbottom
			};
		},
		arrayContains : function(array, value) {
			for ( var i = 0; i < array.length; i++) {
				if (array[i] === value) {
					return true;
				}
			}
			return false;
		},
		isField : function(element){
			var tags = ["form", "input", "textarea", "label", "fieldset", "legend",
		      "select", "optgroup", "option", "button", "datalist", "keygen", "output"];
			for ( var i = 0; i < tags.length; i++) {
				if (tags[i] === element.nodeName.toLowerCase()) {
					return true;
				}
			}
			return false;
		},
		//isLinkSameHost(linkish.host,window.location.host);
		isLinkSameHost : function(linkHost,windowHost){
			//remove www. in front if any
			var pattern = /^www./;
			return linkHost.replace( pattern, "" ) === windowHost.replace( pattern, "" )
			
		},
		isLink : function(element){
			var tags = ["a", "link"];
			for ( var i = 0; i < tags.length; i++) {
				if (tags[i] === element.nodeName.toLowerCase()) {
					return true;
				}
			}
			return false;
		},
		isImg : function(element){
				return "img" === element.nodeName.toLowerCase();
		},
		isFrame : function(element){
			var tags = ["frame", "iframe"];
			for ( var i = 0; i < tags.length; i++) {
				if (tags[i] === element.nodeName.toLowerCase()) {
					return true;
				}
			}
			return false;
		},
		getOverflowProp: function(element){
		  if (element.nodeType !== Node.ELEMENT_NODE) 
			  return 'none';
 		  var computedStyle = document.defaultView.getComputedStyle(element, null);
	      return computedStyle.getPropertyValue('overflow');
		},
		hasOverflowXHidden: function(element) {
		  if (element.nodeType !== Node.ELEMENT_NODE) 
			  return false;
 		  var computedStyle = document.defaultView.getComputedStyle(element, null);
 		  return computedStyle.getPropertyValue('overflow') === 'hidden' || computedStyle.getPropertyValue('overflow-x') === 'hidden';
		},
		hasOverflowYHidden: function(element) {
		  if (element.nodeType !== Node.ELEMENT_NODE) 
			  return false;
 		  var computedStyle = document.defaultView.getComputedStyle(element, null);
 		  return computedStyle.getPropertyValue('overflow') === 'hidden' || computedStyle.getPropertyValue('overflow-y') === 'hidden';
		},
		getDisplayProp: function(element){
			if(element.nodeType === Node.TEXT_NODE){
				if(this.isGarbage(element))
					return 'none';
				else
				return this.getDisplayProp(element.parentNode);
			}

        if(element.nodeType !== Node.ELEMENT_NODE){
        return 'none';
        }
			var computedStyle = document.defaultView.getComputedStyle(element, null);
			return computedStyle.getPropertyValue('display');
		},

		isBreakingFlow : function(element){
			var status = {left:false,right:false}

			//br is inline but produces a break
			if ('br' === element.nodeName.toLowerCase()) {
				return {left:true,right:true};
			}
			var computedStyle = document.defaultView.getComputedStyle(element, null);

			var displayProp = computedStyle.getPropertyValue('display');
			if(displayProp !== 'inline' && displayProp !== 'none'){
			 return {left:true,right:true};
			 }

			//fixed width
			var width = computedStyle.getPropertyValue('width');
			if(width !== 'auto'){
			 return {left:true,right:true};
			 }


			var position = computedStyle.getPropertyValue('position');
			if(position === 'absolute')
				return  {left:true,right:true};


			var left = computedStyle.getPropertyValue('padding-left');
			if(left !== '0px')
				status.left = true;

			var right = computedStyle.getPropertyValue('padding-right');
			if(right !== '0px')
				status.right = true;


			return status;
		},


		/**
		 * @param {string}
		 *            value
		 * @param {array}
		 *            excludeArray
		 * @param {array}
		 *            includeArray
		 * @return {boolean}
		 */
		checkExclusion : function(value, excludeArray, includeArray) {
			// Check if excluded
			if (excludeArray.length !== 0
					&& this.arrayContains(excludeArray, value)) {
				return true;
			}
			// Check if included
			if (includeArray.length !== 0
					&& !this.arrayContains(includeArray, value)) {
				return true;
			}
			return false;
		},
		isGarbage : function(node) {
			// Node not-empy that contains no non-whitespace character
			// return !/\S/.test(node.textContent);
			return /^\s+$/.test(node.textContent);
		},
		escapeForDLVConstant : function(text){
		     
		    if(!text)
		    throw ("Expected not-empty string, cannot escape the value '" + text+ "'");
		    
			var c = text.replace(/\W/g, "_");
		    // }
		    if (c.startsWith("_")) {
		      c = "tag" + c;
		    }
		    // if is not a integer number, cannot start with a number
		    if (!isInteger(c) && startWithInteger(c)) {
		      c = "tag" + c;
		    }
		    return c.toLowerCase();	
		}
	};
	this.extract = function() {
		
		this.extractNode(document.documentElement, 0,false,false,false);
		//set the page id as jsProperty on the document for producing a mapping in the next calls
		document[Configuration.pageIdMapping] = Configuration.currentPageId;
		
	};
	/**
	 * @param {DOMNode}
	 *            root
	 * @param {number}
	 *            parStart
	 */
	this.extractNode = function(root, parStart, isFirstChild, isLastChild) {
		if (!(root instanceof Node))
			throw "extractNode called with non-Node argument: " + root;
		switch (root.nodeType) {
		case Node.ELEMENT_NODE:
			return this.extractElement(root, parStart,  isFirstChild, isLastChild);
			break;
		case Node.TEXT_NODE:
			// if(this.helpers.isGarbage(root))
			// return;
			return this.extractText(root, parStart, isFirstChild,isLastChild);
			break;
		default:
			return this.helpers.emptyBox;
			// ignore
			break;
		}
	};

	this.addClobSeparator = function(){
		var positionInClob=this.result.clob_text.length;
		this.result.clob_text += Configuration.prefixes.separator;
		this.helpers.clob_size = this.result.clob_text.length;
		this.result.clob_breaking_flows.push(new ClobBreakingFlowRepresentative(positionInClob)); 
	};
	
	this.mapElementIdInAny = function (element,currentId){
		
		var oldId = element[Configuration.idMapping];
		
		if(oldId){
			//write the fact 
			this.result.idMapping.push(new IdMappingRepresentative(currentId, oldId));
		}
		//and update with the current	
		element[Configuration.idMapping] = currentId;

	}
	
	/**
	 * @param {HTMLElement}
	 *            element
	 * @param {number}
	 *            parStart
	 */
	this.extractElement = function(element, parStart,  isFirstChild, isLastChild) {
		// Verify that it's an element
		if (!(element instanceof Node)
				&& element.nodeType === Node.ELEMENT_NODE)
			throw "extractNode called with non-Node argument: " + element;

		//current element break flow?
		var isBreakingFlow = this.helpers.isBreakingFlow(element);

		// Increment and safe start_counter
		var start = ++this.helpers.start_counter;
		var startClobSize = this.helpers.clob_size;

		// PSEUDO TRIM START
//		if(/^\s/.test(element.textContent))
//			startClobSize++;
		// PSEUDO TRIM END 
		
		var id = Configuration.prefixes.element + start + "_"
				+ element.localName;

		var firstVisibleChild = null;
		var lastVisibleChild = null;

		var children = element.childNodes;
		//finds the first and last index of children with display != none
		for ( var i = 0; i < children.length; i++) {
			if(this.helpers.getDisplayProp(children[i]) !== 'none' ){
				if(firstVisibleChild === null)
					firstVisibleChild = i;

				if(lastVisibleChild === null || i > lastVisibleChild)
					lastVisibleChild = i;

			}
		}
		//visit children
		var childBoxes = this.helpers.emptyBox;
		for ( var i = 0; i < children.length; i++) {
			var trimLeft = (i === firstVisibleChild) && (isFirstChild || isBreakingFlow.left);
			var trimRight = (i === lastVisibleChild) && (isLastChild || isBreakingFlow.right);
			var childBox = this.extractNode(children[i], start, trimLeft, trimRight);
		}
		//if no visible children we add the separator anyway is breaks the flow (e.g., the case of BR)
		//if(isBreakingFlow && children.length<=0)
		if(isBreakingFlow && firstVisibleChild === null) {
		  this.addClobSeparator();
//		  startClobSize = startClobSize + Configuration.prefixes.separator.length;
		}
		else if (isBreakingFlow.left || isFirstChild) {
//		  startClobSize = startClobSize + Configuration.prefixes.separator.length;
		}
		

		var endClobSize = this.helpers.clob_size;

		// spans in the clob, unless skipped
		if (!skipElement && Configuration.clob) {
			// PSEUDO TRIM START
//			if(/\s$/.test(element.textContent))
//				endClobSize--;
			// PSEUDO TRIM END
			this.result.clob_spans.push(new SpanRepresentative(id,
					startClobSize, endClobSize,false));

		}


		// update end encoding
		var end = ++this.helpers.start_counter;

		// it is necessary to produce the shadow dom
		var skipElement = false;

		// Check exclusion and inclusion
		if (this.helpers.checkExclusion(element.localName,
				Configuration.excludes.elements,
				Configuration.includes.elements))
			skipElement = true;
		// neither the element nor the shadow dom are requested
		if (skipElement && !Configuration.allTheRest)
			return;

		// Create element representative and add to return
		this.result.elements.push(new ElementRepresentative(id, start, end,
				parStart, element.localName, skipElement));

		// Treat attributes.
		var skipAttribute = skipElement || (!Configuration.attribute);

		if (Configuration.allTheRest || !skipAttribute) {// either attributes
			// or shadow are
			// requested
			this.extractAttributes(element.attributes,id,skipAttribute);
//			var attributes = element.attributes;
//			for ( var i = 0; i < attributes.length; i++) {
//				this.extractAttribute(attributes[i], id, skipAttribute);
//			}
		}

		// Treat CSS properties
		var skipCss = skipElement || (!Configuration.css);
		if (Configuration.allTheRest || !skipCss) {// either css or
			// shadow are requested
			var styles = document.defaultView.getComputedStyle(element, null);
			for ( var i = 0; i < styles.length; i++) {
				if (!(styles[i].indexOf("-") === 0)) {
					this.extractCSS(styles[i], styles
							.getPropertyValue(styles[i]), id, skipCss);
				}
			}
		}

		// Treat Boxes unless the element is to skip (not used for shadow dom
		if (!skipElement && Configuration.boxes) {
			childBoxes = this.extractBoundingBox(id, element);
		}

		// Treat Boxes unless the element is to skip (not used for shadow dom
		if (!skipElement && Configuration.jsProperties) {
			this.extractJSProperties(id, element);
		}


		var skipLocators = skipElement || (!Configuration.locators);
		if (Configuration.allTheRest || !skipLocators) {// either locators or
			// shadow are requested
			// we produce locators also for elements to skip

			var locator = _getXPathLocator(element,Configuration.locators_useId,Configuration.locators_useClass);
			// console.log(element.localName + ": " + start + ", " + parStart +
			// " " + locator);
			this.result.locators.push(new LocatorRepresentative(id, locator,
					skipLocators));

		}
		
		//pruduces a mapping among ids
		this.mapElementIdInAny(element,id);
		
		return childBoxes;
	};
	/**
	 * @param {Node}
	 *            node // text node
	 * @param {number}
	 *            parStart
	 */
	this.extractText = function(node, parStart, isFirstChild, isLastChild) {
 		// we increment start/end anyway to be consistent on the same document,
		// regardless whether text nodes are required or not.
		var start = ++this.helpers.start_counter;

		var end = ++this.helpers.start_counter;

		var toSkipNode = !Configuration.text;
		// Check exclusion and inclusion on the parent element
		if (this.helpers.checkExclusion(node.parentNode.localName,
				Configuration.excludes.elements,
				Configuration.includes.elements))
			toSkipNode = true;
		// either the text nodes or the shadow dom are requested
		if (toSkipNode && !Configuration.allTheRest)
			return;

		var id = Configuration.prefixes.text + start;
		this.helpers.text_counter++;

		var garbage = this.helpers.isGarbage(node);

		//console.log('node t_:'+start+" is garbage:"+garbage);

		// if garbage or skipped, we produce it for the shadow dom
		this.result.texts.push(new TextRepresentative(id, start, end, parStart,
				(garbage || toSkipNode)));
		
		if (!toSkipNode && !garbage && Configuration.boxes) {
			childBoxes = this.extractBoundingBoxTextNode(id, node);
		}

		//only if clob is requested
		var toSkipClob = toSkipNode || (!Configuration.clob);

		// either the shadow or the clob are requested
		if (Configuration.allTheRest || !toSkipClob) {

			// Create the clobvalue entry with the original text

				var originalText = node.textContent;

				// we pass the non-normalized text. The normalization it's done
				// in Java.
				this.result.clob_values.push(new ClobValuesRepresentative(id,
						originalText, toSkipClob, garbage));

				if (!toSkipClob) {// if requested we produce the stuff for the
					var text = originalText;
					//trim only the lefmost
					if(isFirstChild){
					    this.addClobSeparator();
						//disabled trimming in March
					    //text = text.trimLeft();
					}
					if(isLastChild){
						//disabled trimming in March
						//text = text.trimRight();
					}

					//now we normalize spaces
					var normalizedText = text.replace(/\s+/g,
					' ');

					// clob but skipping garbage nodes
					var startClobSize = this.helpers.clob_size;

					//take care of no trimming
					// PSEUDO TRIM START
//					if(normalizedText.startsWith(" "))
//						startClobSize++;
					// PSEUDO TRIM END

					this.result.clob_text += normalizedText;

					this.helpers.clob_size = this.result.clob_text.length;

					var endClobSize = this.helpers.clob_size;

					//take care of no trimming
					// PSEUDO TRIM START
//					if(normalizedText.endsWith(" "))
//						endClobSize--;
					// PSEUDO TRIM END

					if(!(garbage || toSkipNode)) //if not to skip
					this.result.clob_spans.push(new SpanRepresentative(id,
							startClobSize, endClobSize,(garbage||toSkipNode)));
					//adding separator to break text flow
					if(isLastChild)
						  this.addClobSeparator();

				}

		}

		var toSkipLocators = toSkipNode || (!Configuration.locators);
		// either the shadow or the locatores are requested
		if (Configuration.allTheRest || !toSkipLocators) {
			// gnrerate locators for text nodes
			var locator =  _getXPathLocator(node,Configuration.locators_useId,Configuration.locators_useClass);
			this.result.locators.push(new LocatorRepresentative(id, locator, (garbage || toSkipLocators)));
		}
		
		//pruduces a mapping among ids
		this.mapElementIdInAny(node,id);
		return this.helpers.emptyBox;
	};
	/**
	 * @param {Node}
	 *            attribute
	 * @param {string}
	 *            elId
	 */
	this.extractAttribute = function(attribute, elId, relativePosition, skipElement) {
		var skip = skipElement;
		// Check exclusion and inclusion
		if (this.helpers.checkExclusion(attribute.localName,
				Configuration.excludes.attributes,
				Configuration.includes.attributes))
			skip = true; // goes into the junk writer

		//additional skipping for xmlns attributes
		if(Configuration.skipXMLNSattr){
			if(attribute.nodeName.startsWith("xmlns"))
				skip=true;
		}
		//here the attributeId can clash in case the localName is not unique, there fore we add a trailing number 
		var attr_id = elId + "_"+ attribute.localName;
		if(WebUtils.containsNonWord(attribute.localName))
			 attr_id+="_"+relativePosition;
		
		this.mapElementIdInAny(attribute,attr_id);
		
		this.result.attributes.push(new AttributeRepresentative(attr_id, elId, attribute.localName,
				attribute.nodeValue, skip));
	};
	
	/**
	 * 
	 */
	this.extractAttributes= function(attributes, elId, skipElement){
		for ( var i = 0; i < attributes.length; i++) {
			this.extractAttribute(attributes[i], elId, i,skipElement);
		}
	};
	/**
	 * @param {string}
	 *            property
	 * @param {string}
	 *            value
	 * @param {string}
	 *            elId
	 */
	this.extractCSS = function(property, value, elId, skipElement) {
		var skip = skipElement;
		// Check exclusion and inclusion
		if (this.helpers.checkExclusion(property, Configuration.excludes.css,
				Configuration.includes.css))
			return;
		
		this.result.css
				.push(new CSSRepresentative(property, elId, value, skip));
		
	};
	/**
	 * Extracts a bounding box for the given element (with the given elId) assuming the given childrenBox represents a bounding box for the children.
	 * TODO: When computing the childrenBox we should skip invisible, overlay, ... children. 
	 */
	this.extractBoundingBox = function(elId, element) {
		var rect = element.getBoundingClientRect();
		var left = rect.left;
		var top = rect.top;
		var bottom = rect.bottom;
		var right = rect.right;
		// Check all ancestors whether they have smaller coordinates and hidden overflow
		var parent = element.parentNode;
		while (parent !== null) {
		  if (parent.getBoundingClientRect) {
			  parentRect = parent.getBoundingClientRect();
			  if (this.helpers.hasOverflowXHidden(parent)) {
				  left = Math.round(Math.max(left, parentRect.left));
				  right = Math.round(Math.min(right, parentRect.right));
			  }
			  if (this.helpers.hasOverflowYHidden(parent)) {
				  top = Math.round(Math.max(top, parentRect.top));
				  bottom = Math.round(Math.min(bottom, parentRect.bottom));
			  }
		  }
		  parent = parent.parentNode;
		}

		left = left+window.scrollX;
		right = right+window.scrollX;
		top = top+window.scrollY;
		bottom = bottom+window.scrollY;
		var width = right - left;
		var height = bottom - top;
		this.result.boxes.push(new BoundingBoxRepresentative(elId, left,
				top, right, bottom, width, height));
	};

	/**
	 * Extracts a bounding box for the given text node. 
	 */
	this.extractBoundingBoxTextNode = function(textId, textNode) {
		var range = document.createRange();
		range.selectNodeContents(textNode);
		if (range.getBoundingClientRect) {
		  var rect = range.getBoundingClientRect();
		  this.result.boxes.push(new BoundingBoxRepresentative(textId, rect.left,
				  rect.top, rect.right, rect.bottom, rect.width, rect.height));
		}		
	};

	/**
	 *
	 */
	this.extractJSProperties = function(elId, element) {
		
		//for all we report is  is Displayed
		var displId = elId + "_"+ Configuration.prefixes.js_prop + "displayed";
		this.result.jsProperties.push(new JSPropertyRepresentative(displId, elId, "displayed", bot_dom_isShown(element,false).toString()));
		
		if (element === document.documentElement){ //for document
			var docIdPageHref = elId + "_"+ Configuration.prefixes.js_prop + "pageHref";
			this.result.jsProperties.push(new JSPropertyRepresentative(docIdPageHref, elId, "pageHref", window.location.href));
			var docId = elId + "_"+ Configuration.prefixes.js_prop + "dimension";
			this.result.jsProperties.push(new JSPropertyRepresentative(docId, elId, "width_num", getDocumentDimension()[0]));
			this.result.jsProperties.push(new JSPropertyRepresentative(docId, elId, "height_num", getDocumentDimension()[1]));
		}
		
		var f_p=['value','checked','defaultValue','defaultChecked','selected'];
		var a_p = ['href'];
		var frame_p = ['src'];
		var p = [];

		if(this.helpers.isField(element)){
			p=f_p;
			//for all fields we report whether enabled 
			var enId = elId + "_"+ Configuration.prefixes.js_prop + "enabled";
			this.result.jsProperties.push(new JSPropertyRepresentative(enId, elId, "enabled", bot_dom_isEnabled(element).toString()));
		}
		else if (this.helpers.isLink(element))
			p=a_p;
		else if (this.helpers.isFrame(element) || this.helpers.isImg(element))
			p=frame_p;
		else return;

		for(var i = 0; i < p.length; i++ ){
			if(p[i] in element){
				
				var v = element[p[i]];//get the value for the property
				var propId = elId + "_"+ Configuration.prefixes.js_prop + p[i];
				this.result.jsProperties.push(new JSPropertyRepresentative(propId, elId, p[i], v.toString()));

				var linkish = element;
				if(p[i] === 'src'){ //if the property is src, it means wa are looking at an iframe
					linkish = document.createElement('a');
					linkish.href = v;
				}
				//for links (or iframe/img src) in general
				if (p[i] === 'href' || p[i] === 'src' ) {
					if (linkish.host) {
						propId = elId + "_"+ Configuration.prefixes.js_prop + "host";
						this.result.jsProperties.push(new JSPropertyRepresentative(propId, elId, "host", linkish.host));
						propId = elId + "_"+ Configuration.prefixes.js_prop + "samehost";
						var same = this.helpers.isLinkSameHost(linkish.host,window.location.host);
						this.result.jsProperties.push(new JSPropertyRepresentative(propId, elId, "samehost", same));
						propId = elId + "_"+ Configuration.prefixes.js_prop + "forbiddenhost";
						var host = linkish.host;
						var forbidden = (host.indexOf("google") != -1 || host.indexOf("twitter") != -1 || host.indexOf("facebook") != -1 || host.indexOf("vimeo") != -1 || host.indexOf("youtube") != -1 || host.indexOf("linkedin") != -1 || host.indexOf("bing") != -1 || host.indexOf("baidu") != -1 );
						this.result.jsProperties.push(new JSPropertyRepresentative(propId, elId, "forbiddenhost", forbidden));
					}
//					console.log(elId + ": " + linkish.protocol);
					if (linkish.protocol) {
						propId = elId + "_"+ Configuration.prefixes.js_prop + "protocol";
						this.result.jsProperties.push(new JSPropertyRepresentative(propId, elId, "protocol", linkish.protocol));
					}
//					console.log(elId + ": " + linkish.pathname);
					if (linkish.pathname) {
						propId = elId + "_"+ Configuration.prefixes.js_prop + "pathname";
						this.result.jsProperties.push(new JSPropertyRepresentative(propId, elId, "pathname", linkish.pathname));
					}
//					console.log(elId + ": " + linkish.hash);
					propId = elId + "_"+ Configuration.prefixes.js_prop + "type";
					if ((!(/^http/.test(linkish.href)) || /\.(pdf|js|png|jpeg|jpg|gif|css|ico)$/.test(linkish.href))
							|| (linkish.href.indexOf("download") != -1) || (linkish.href.indexOf("floorplan") != -1)
							|| (linkish.href.indexOf("map") != -1) || (linkish.href.indexOf("brochure") != -1)) {
						this.result.jsProperties.push(new JSPropertyRepresentative(propId, elId, "linktype", "other"));
					} else {
						this.result.jsProperties.push(new JSPropertyRepresentative(propId, elId, "linktype", "html"));
					}
					if (linkish.hash) {
						propId = elId + "_"+ Configuration.prefixes.js_prop + "hash";
						this.result.jsProperties.push(new JSPropertyRepresentative(propId, elId, "hash", linkish.hash));
					}
				}
			}
		}

	};


}


var extractor = new Extractor();
//if we aren't in a html document we stop
if("html" !== document.documentElement.nodeName.toLowerCase())
	return extractor.result;

extractor.extract();
return extractor.result;
