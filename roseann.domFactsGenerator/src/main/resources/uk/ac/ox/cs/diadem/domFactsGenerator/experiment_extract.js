/** Configuration for the extractor * */
var Configuration = {
	/** Excludes for elements, attributes, etc. */
	excludes : {
		/** @const */
		elements : [],
		/** @const */
		attributes : [],
		/** @const */
		css : []
	// array ['font']

	},
	/** Includes for elements, attributes, etc. */
	includes : {
		/** @const */
		elements : [],
		/** @const */
		attributes : [],
		/** @const */
		css : []
	},
	/** @const */
	text : true,
	/** @const */
	attribute : false,
	/** @const */
	css : false,
	/** @const */
	clob : true,
	/** @const */
	boxes : false,
	/** @const */
	locators : false,
	/** @const */
	allTheRest : false,
	prefixes : {
		/** @const */
		element : "e_",
		/** @const */
		text : "t_",
		/** @const */
		box : "boundingBox",
		/** @const */
		clob : "clob_",
		/** @const */
		separator : " ",
		/** @const */
		js_prop : "js_"
	}
};

/*
 * var getElementTreeXPath = function(element) { var paths = []; // Use nodeName
 * (instead of localName) so namespace prefix is included (if any). for (;
 * element && element.nodeType === 1; element = element.parentNode) { var index =
 * 0; for (var sibling = element.previousSibling; sibling; sibling =
 * sibling.previousSibling) { // Ignore document type declaration. if
 * (sibling.nodeType === Node.DOCUMENT_TYPE_NODE) continue; if (sibling.nodeName
 * === element.nodeName) ++index; } var tagName =
 * element.nodeName.toLowerCase(); var pathIndex = (index ? "[" + (index+1) +
 * "]" : ""); paths.splice(0, 0, tagName + pathIndex); }
 *
 * return paths.length ? "/" + paths.join("/") : null; };
 */

/**
 * Gets an XPath for an element which describes its hierarchical location.
 */
/*
 * var getElementXPath = function(element) { if (element && element.id) return
 * '//*[@id="' + element.id + '"]'; elseabout:startpage return
 * getElementTreeXPath(element); };
 */

var createXPathFromElement = function(elm) {




	var allNodes = document.getElementsByTagName('*');
	for (segs = []; elm && elm.nodeType === 1; elm = elm.parentNode) {
		//for html we don't bother
		if(elm === document.documentElement ){
			segs.unshift('/'+elm.nodeName.toLowerCase()+'[1]');
					return segs.join('/');
		}

		if (elm.hasAttribute('id')) {
			var uniqueIdCount = 0;
			for ( var n = 0; n < allNodes.length; n++) {
				if (allNodes[n].hasAttribute('id') && allNodes[n].id === elm.id)
					uniqueIdCount++;
				if (uniqueIdCount > 1)
					break;
			}
			;
			if (uniqueIdCount === 1) {
				segs.unshift("id('" + elm.getAttribute('id') + "')");
				return segs.join('/');
			} else {
				segs.unshift(elm.localName.toLowerCase() + "[@id='"
						+ elm.getAttribute('id') + "']");
			}
		} else if (elm.hasAttribute('class')) {
			for (i = 1, sib = elm.previousSibling; sib; sib = sib.previousSibling) {
				if ((sib.localName === elm.localName)
						&& (sib.hasAttribute('class'))
						&& (sib.getAttribute('class') === elm
								.getAttribute('class')))
					i++;
			}
			;
			segs.unshift(elm.localName.toLowerCase() + "[@class='"
					+ elm.getAttribute('class') + "']" + "[" + i + "]");
		} else {
			for (i = 1, sib = elm.previousSibling; sib; sib = sib.previousSibling) {
				if (sib.localName === elm.localName)
					i++;
			}
			;
			segs.unshift(elm.localName.toLowerCase() + '[' + i + ']');
		}
		;
	}
	;
	return segs.length ? '/' + segs.join('/') : null;
};

/** @enum {number} */
var XPathAxis = {
	CHILD : 0,
	DESCENDANT : 1,
	FOLLOWING : 2,
	PRECEDING : 3,
	ANCESTOR : 4,
	FOLLOWING_SIBLING : 5,
	PRECEDING_SIBLING : 6,
	SELF : 8,
	ANCESTOR_OR_SELF : 9,
	DESCENDANT_OR_SELF : 10,
	PARENT : 11
};

var ElementRepresentative = function(id, start, end, parStart, tag, isExcluded) {
	return [ id, start, end, parStart, tag, isExcluded ];
};
var TextRepresentative = function(id, start, end, parStart, isExcluded) {
	return [ id, start, end, parStart, isExcluded ];
};
var ClobValuesRepresentative = function(id, value, isExcluded, isGarbage) {
	return [ id, value, isExcluded, isGarbage ];
};
var SpanRepresentative = function(id, startClobSize, endClobSize) {
	return [ id, Configuration.prefixes.clob, startClobSize, endClobSize ];
};
var CSSRepresentative = function(property, elId, value, isExcluded) {
	return [ property, elId, value, isExcluded ];
};
var BoundingBoxRepresentative = function(elId, left, top, right, bottom, width,
		height) {
	return [ elId, left, top, right, bottom, width, height ];
};
var AttributeRepresentative = function(id, elId, name, value, isExcluded) {
	return [ id, elId, name, value, isExcluded ];
};
var LocatorRepresentative = function(id, locator, isExcluded) {
	return [ id, locator, isExcluded ];
}
var Extractor = function() {
	this.result = {
		elements : [],
		attributes : [],
		texts : [],
		texts_garbage : [],
		clob_values : [],
		clob_spans : [],
		clob_text : "",
		css : [],
		boxes : [],
		xpaths : [],
		locators : []
	};
	this.helpers = {
		id_counter : 0,
		start_counter : 0,
		text_counter : 0,
		clob_size : 0,
		arrayContains : function(array, value) {
			for ( var i = 0; i < array.length; i++) {
				if (array[i] === value) {
					return true;
				}
			}
			return false;
		},
		addSeparator : function(node){


			var foll=document.evaluate("following::node()[1]", node, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;

			if(foll){
			console.log(node.textContent+" followed by: " +foll.textContent);
	         //if it's a node for indentation, we get the next
				if(foll.nodeType === Node.TEXT_NODE && ((/^\s+$/.test(foll.textContent)))){
				 console.log(node.textContent+" WRONGLY followed by: " +foll.textContent);
					foll=document.evaluate("following::*[1]", node, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;
					if(foll)
					console.log(node.textContent+" followed NOW by: " +foll);
					else console.log(node.textContent+" NOT followed");
			}
			//recheck if defined

			if(foll){
			if(foll.nodeType === Node.ELEMENT_NODE){
				var displSibling = document.defaultView.getComputedStyle(foll, null).getPropertyValue('display');
				if(displSibling !== 'inline'){
				 console.log(node.textContent+" ADD separators");
				 return true;
				 }
	        }
			}

	        }
	        else{console.log(node.textContent+" NOT followed");}
	        console.log(node.textContent+" NOT add separators");
			return false;


		},

		/**
		 * @param {string}
		 *            value
		 * @param {array}
		 *            excludeArray
		 * @param {array}
		 *            includeArray
		 * @return {boolean}
		 */
		checkExclusion : function(value, excludeArray, includeArray) {
			// Check if excluded
			if (excludeArray.length !== 0
					&& this.arrayContains(excludeArray, value)) {
				return true;
			}
			// Check if included
			if (includeArray.length !== 0
					&& !this.arrayContains(includeArray, value)) {
				return true;
			}
			return false;
		},
		isGarbage : function(node) {
			// Node not-empy that contains no non-whitespace character
			// return !/\S/.test(node.textContent);
			return /^\s+$/.test(node.textContent);
		}
	};
	this.extract = function() {
		this.extractNode(document.documentElement, 0);
	};
	/**
	 * @param {DOMNode}
	 *            root
	 * @param {number}
	 *            parStart
	 */
	this.extractNode = function(root, parStart) {
		if (!(root instanceof Node))
			throw "extractNode called with non-Node argument: " + root;
		switch (root.nodeType) {
		case Node.ELEMENT_NODE:
			this.extractElement(root, parStart);
			break;
		case Node.TEXT_NODE:
			// if(this.helpers.isGarbage(root))
			// return;
			this.extractText(root, parStart);
			break;
		default:
			// ignore
			break;
		}
	};
	/**
	 * @param {HTMLElement}
	 *            element
	 * @param {number}
	 *            parStart
	 */
	this.extractElement = function(element, parStart) {
		// Verify that it's an element
		if (!(element instanceof Node)
				&& element.nodeType === Node.ELEMENT_NODE)
			throw "extractNode called with non-Node argument: " + element;

		// Increment and safe start_counter
		var start = ++this.helpers.start_counter;
		var startClobSize = this.helpers.clob_size;
		var id = Configuration.prefixes.element + start + "_"
				+ element.localName;
		// Treat children
		var children = element.childNodes;
		for ( var i = 0; i < children.length; i++) {
			this.extractNode(children[i], start);
		}
		// update end encoding
		var end = ++this.helpers.start_counter;

		// it is necessary to produce the shadow dom
		var skipElement = false;

		// Check exclusion and inclusion
		if (this.helpers.checkExclusion(element.localName,
				Configuration.excludes.elements,
				Configuration.includes.elements))
			skipElement = true;
		// neither the element nor the shadow dom are requested
		if (skipElement && !Configuration.allTheRest)
			return;

		// Create element representative and add to return
		this.result.elements.push(new ElementRepresentative(id, start, end,
				parStart, element.localName, skipElement));

		// Treat attributes.
		var skipAttribute = skipElement || (!Configuration.attribute);

		if (Configuration.allTheRest || !skipAttribute) {// either attributes
			// or shadow are
			// requested
			var attributes = element.attributes;
			for ( var i = 0; i < attributes.length; i++) {
				this.extractAttribute(attributes[i], id, skipAttribute);
			}
		}

		// Treat CSS properties
		var skipCss = skipElement || (!Configuration.css);
		if (Configuration.allTheRest || !skipCss) {// either css or
			// shadow are requested
			var styles = document.defaultView.getComputedStyle(element, null);
			for ( var i = 0; i < styles.length; i++) {
				if (!(styles[i].indexOf("-") === 0)) {
					this.extractCSS(styles[i], styles
							.getPropertyValue(styles[i]), id, skipCss);
				}
			}
		}

		// Treat Boxes unless the element is to skip (not used for shadow dom
		if (!skipElement && Configuration.boxes) {
			this.extractBoundingBox(id, element);
		}
		// spans in the clob, unless skipped
		if (!skipElement && Configuration.clob) {
			var endClobSize = this.helpers.clob_size;
			this.result.clob_spans.push(new SpanRepresentative(id,
					startClobSize, endClobSize));
		}

		var skipLocators = skipElement || (!Configuration.locators);
		if (Configuration.allTheRest || !skipLocators) {// either locators or
			// shadow are requested
			// we produce locators also for elements to skip

			var locator = createXPathFromElement(element);
			// console.log(element.localName + ": " + start + ", " + parStart +
			// " " + locator);
			this.result.locators.push(new LocatorRepresentative(id, locator,
					skipLocators));

		}
	};
	/**
	 * @param {Node}
	 *            node // text node
	 * @param {number}
	 *            parStart
	 */
	this.extractText = function(node, parStart) {

		// we increment start/end anyway to be consistent on the same document,
		// regardless whether text nodes are required or not.
		var start = ++this.helpers.start_counter;

		var end = ++this.helpers.start_counter;

		var toSkipNode = !Configuration.text;
		// Check exclusion and inclusion on the parent element
		if (this.helpers.checkExclusion(node.parentNode.localName,
				Configuration.excludes.elements,
				Configuration.includes.elements))
			toSkipNode = true;
		// either the text nodes or the shadow dom are requested
		if (toSkipNode && !Configuration.allTheRest)
			return;

		var id = Configuration.prefixes.text + start;
		this.helpers.text_counter++;

		var garbage = this.helpers.isGarbage(node);

		console.log('node t_:'+start+" is garbage:"+garbage);

		// if garbage or skipped, we produce it for the shadow dom
		this.result.texts.push(new TextRepresentative(id, start, end, parStart,
				(garbage || toSkipNode)));

		//only if clob is requested
		var toSkipClob = toSkipNode || (!Configuration.clob);

		// either the shadow or the clob are requested
		if (Configuration.allTheRest || !toSkipClob) {

			// Create the clobvalue entry with the original text

				var originalText = node.textContent;
//				var normalizedText = originalText.trim().replace(/\s+/g,
//						' ');
				//we don't trim for the moment
				var normalizedText = originalText.replace(/\s+/g,
						' ');

				// we pass the non-normalized text. The normalization it's done
				// in Java.
				this.result.clob_values.push(new ClobValuesRepresentative(id,
						originalText, toSkipClob, garbage));

				if (!toSkipClob && !garbage) {// if requested we produce the stuff for the
					// clob but skipping garbage nodes
					var startClobSize = this.helpers.clob_size;
					this.result.clob_text += normalizedText;
					this.helpers.clob_size = this.result.clob_text.length;
					var endClobSize = this.helpers.clob_size;
					this.result.clob_spans.push(new SpanRepresentative(id,
							startClobSize, endClobSize));
					// add separator if it's a block element

					var addSep = false;
					var displ = document.defaultView.getComputedStyle(node.parentNode, null).getPropertyValue('display');
					if(displ !== 'inline')
						addSep = true;
					else
					addSep = this.helpers.addSeparator(node);

					if(addSep)
					this.result.clob_text += " ";


					this.helpers.clob_size = this.result.clob_text.length;
				}

		}

		var toSkipLocators = toSkipNode || (!Configuration.locators);
		// either the shadow or the locatores are requested
		if (Configuration.allTheRest || !toSkipLocators) {
			// gnrerate locators for text nodes
			var locator = createXPathFromElement(node.parentNode);
			// find the position among siblings
			for (i = 1, sib = node.previousSibling; sib; sib = sib.previousSibling) {
				if (sib.nodeName === node.nodeName)
					i++;
			}

			// console.log(element.localName + ": " + start + ", " +
			// parStart + " " + locator);
			this.result.locators.push(new LocatorRepresentative(id, locator
					+ "/text()[" + i + "]", (garbage || toSkipLocators)));
		}
	};
	/**
	 * @param {Node}
	 *            attribute
	 * @param {string}
	 *            elId
	 */
	this.extractAttribute = function(attribute, elId, skipElement) {
		var skip = skipElement;
		// Check exclusion and inclusion
		if (this.helpers.checkExclusion(attribute.localName,
				Configuration.excludes.attributes,
				Configuration.includes.attributes))
			skip = true; // goes into the junk writer

		this.result.attributes.push(new AttributeRepresentative(elId + "_"
				+ attribute.localName, elId, attribute.localName,
				attribute.nodeValue, skip));
	};
	/**
	 * @param {string}
	 *            property
	 * @param {string}
	 *            value
	 * @param {string}
	 *            elId
	 */
	this.extractCSS = function(property, value, elId, skipElement) {
		var skip = skipElement;
		// Check exclusion and inclusion
		if (this.helpers.checkExclusion(property, Configuration.excludes.css,
				Configuration.includes.css))
			return;

		this.result.css
				.push(new CSSRepresentative(property, elId, value, skip));
	};
	/**
	 * @param {string}
	 *            elId
	 * @param {HTMLElement}
	 *            element
	 */
	this.extractBoundingBox = function(elId, element) {
		var rect = element.getBoundingClientRect();
		var left = rect.left+window.scrollX;
		var top = rect.top+window.scrollY;
		var bottom = rect.bottom+window.scrollY;
		this.result.boxes.push(new BoundingBoxRepresentative(elId, left,
				top, rect.right, bottom, rect.width, rect.height));
	};

}
var extractor = new Extractor();
extractor.extract();
var after = new Date();
// console.log(after-before);
return extractor.result;
